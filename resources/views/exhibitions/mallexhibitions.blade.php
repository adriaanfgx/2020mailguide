@extends('layouts.pages')
@section('title')
    @parent
    Exhibitions
@stop

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            @include('partials.headers.search')
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Exhibitions: </span>The latest <span>Exhibitions</span>
                    </h1>
                    <p class="animated fadeInDown delay2">See what is on show near you!</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->

@stop

@section('content')

    <div class="row-fluid hidden-phone" id="filterSection_menu">
        <div class="span12"><!--sort wrap -->
        </div><!--span12 -->
    </div><!--row -->

    <div id="filterSection" data-perrow="4" class="row-fluid">


        <div class="span3 pull-left">
            <h4 class="hborder">Listed Exhibitions</h4>
            <!--                <p class="legend">Listing Gauteng events</p>-->
        </div>

        <div class="span3 pull-right">
            <div class="overlay-wrapper"></div>
            <div class="blog-content border_top">

                <dl>
                    <dd>
                    <dt>Keyword Search</dt>

                    <input name="Search" type="text">
                    <input class="submit reg-btn" type="submit" value="Search">
                    </dd>

                </dl>
            </div>
        </div>  <!-- span6 -->


        <div class="span3 pull-right">
            <div class="overlay-wrapper"></div>
            <div class="blog-content border_top">

                <dl>
                    <dt>Mall Selection</dt>
                    <dd>
                        {{ Form::select('mall_id', $malls,$mallID,array('id' => 'mall_id')) }}
                    </dd>
                    <br>

                </dl>
            </div>
        </div>  <!-- span6 -->

        <div class="span3 pull-right">
            <div class="overlay-wrapper"></div>
            <div class="blog-content border_top">

                <dl>
                    <dt>Province Select</dt>
                    <dd>
                        {{ Form::select('mall_id', $provinces,null,array('id' => 'provinces')) }}
                    </dd>
                    <br>


                </dl>
            </div>
        </div>  <!-- span6 -->

        <div class="clear"></div>

        @if( count($exhibitions) > 0 )
            <div class="row-fluid">
                <div class="span12">
                    <p class="legend">&nbsp;</p>

                    <table class="table table-border">
                        <thead>
                        <tr>
                            <th>Mall</th>
                            <th>Exhibition Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                        </tr>
                        </thead>
                        <tbody id="event_data">
                        @foreach($exhibitions as $exhibition)
                            <tr>
                                <td>{{ $exhibition->mall }}</td>
                                <td>
                                    <a href='{{ route('exhibitions.view_exhibition',$exhibition->exhibitionID) }}'>{{ $exhibition->name }}</a>
                                </td>
                                <td>{{ date('j F Y', strtotime($exhibition->startDate)) }}</td>
                                <td>{{ date('j F Y', strtotime($exhibition->endDate)) }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!--span12 -->
            </div>
        @else
            <br/>
            <div class="alert alert-info span6">There are no currently no listed exhibitions</div>
        @endif
    </div>
@stop

<!-- extra javascript -->

@section('exScript')
    </script>
@stop
