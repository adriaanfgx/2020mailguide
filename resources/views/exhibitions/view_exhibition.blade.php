@extends('layouts.pages')

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            @include('partials.headers.search')
            <div class="row-fluid">
                <div class="span112">
                    <h1 class="animated fadeInDown delay1"><span>Exhibitions: </span>The latest <span>Exhibitions</span>
                    </h1>
                    <p class="animated fadeInDown delay2">See what is on show near you!</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
@stop

@section('content')

    <div class="row-fluid">

        <h4 class="hborder">Exhibition @ {{$mall->name}}</h4>
        <div class="row-fluid">
            <div class="span12">
                <div class="span3">
                    <?php
                    $_image = (!empty($exhibition->thumbnail1)) ? 'uploadimages/mall_' . $mall->mallID . '/' . $exhibition->thumbnail1 : 'img/noexhibition.png';
                    ?>
                    <div class="zoom-gallery">
                        <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $exhibition->mallID }}/{{ $exhibition->image1 }}"
                           title="{{$exhibition->name}}">
                            <img src="{{ Config::get('app.url') }}/{{ $_image }}" alt="{{$exhibition->name}}"/>
                        </a>
                        @if( !empty($exhibition->altImage1))
                            <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $exhibition->mallID }}/{{ $exhibition->altImage1 }}"
                               title="{{$exhibition->name}}" class="gallery-image">
                                <img
                                    src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $exhibition->mallID }}/{{ $exhibition->altImage1 }}"
                                    alt="{{$exhibition->name}}"/>
                            </a>
                        @endif
                        @if( !empty($exhibition->altImage2))
                            <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $exhibition->mallID }}/{{ $exhibition->altImage2 }}"
                               title="{{$exhibition->name}}" class="gallery-image">
                                <img
                                    src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $exhibition->mallID }}/{{ $exhibition->altImage2 }}"
                                    alt="{{$exhibition->name}}"/>
                            </a>
                        @endif
                        @if( !empty($exhibition->altImage3))
                            <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $exhibition->mallID }}/{{ $exhibition->altImage3 }}"
                               title="{{$exhibition->name}}" class="gallery-image">
                                <img
                                    src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $exhibition->mallID }}/{{ $exhibition->altImage3 }}"
                                    alt="{{$exhibition->name}}"/>
                            </a>
                        @endif
                    </div>
                    <br/><br/>
                    <!--/my carousel-->
                </div><!--span3-->
                <div class="span9">
                    <div class="overlay-wrapper">

                        <div class="overlay"></div>
                    </div>
                    <div class="blog-content border_top">
                        <div>
                            <h4 class="hborder">{{ $exhibition->name }}</h4>
                            <dl>
                                @if($exhibition->exhibition)
                                    <dd>{{ $exhibition->exhibition }}</dd>
                                @endif
                                <dt>Start Date</dt>
                                <dd>{{ date('j F Y', strtotime($exhibition->startDate)) }}</dd>
                                <dt> End Date</dt>
                                <dd>{{ date('j F Y', strtotime($exhibition->endDate)) }}</dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
            <p>&nbsp;</p>
            <div class="text-right">
                <button class="btn btn-inverse go_back">&laquo; Go back</button>
            </div>
        </div>
    </div>
@stop

<!-- extra javascript -->

@section('exScript')
    <script type="text/javascript">
        jQuery(document).ready(function () {

            $('.zoom-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: true,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function (element) {
                        return element.find('img');
                    }
                }

            });
        });
    </script>
@stop
