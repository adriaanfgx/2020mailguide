@extends('layouts.pages')

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        @include('partials.headers.search')
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Exhibitions: </span>The latest <span>Exhibitions</span>
                    </h1>
                    <p class="animated fadeInDown delay2">See what is on show near you!</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->

@stop

@section('content')



    <div class="row-fluid">

        <div class="hborder">
            <h4 class="paddingBottom  ">
                Exhibitions
            </h4>
            <div class="row-fluid">
                <div class="span3 offset4">
                    <div class="overlay-wrapper"></div>
                    <select name="province_id" id="provinces">
                        <option value="" selected="selected">Please select ...</option>
                        @foreach($provinces as $key=>$value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="span3">
                    <?php
                    $mallSelected = session()->get('selected_mall', '');
                    //if(!$mallSelected) $mallSelected = 0;
                    ?>
                    <div class="overlay-wrapper"></div>
                    {{ Form::select('mall_id',$malls,$mallSelected, ['placeholder'=>'Please select a province ...'], array('id'=>'mall_id','disabled')) }}
                    <?php session()->forget('selected_mall'); ?>
                </div>
            </div>
        </div>  <!-- span6 -->
        <table class="table table-border table-striped responsive exhibitions-table">
            <thead>
            <tr>
                <th>Mall</th>
                <th>Exhibition Name</th>
                <th>Start Date</th>
                <th>End Date</th>
            </tr>
            </thead>
            <tbody id="exhibition_data">
            <tr>
                <td colspan="4">Please Select a province to start...</td>
            </tr>
            </tbody>
        </table>

    </div>
@stop

<!-- extra javascript -->

@section('exScript')
    <script type="text/javascript">
        jQuery(document).ready(function () {

            if ($("#provinces").val() != '') {
                $('#mall_id').attr('disabled', false);

                //Get shops for the pre-selected mall
                if ($('#mall_id').val()) {
                    getExhibitionsForSelectedMall();
                } else {
                    reloadProvinces($("#provinces").val());
                }
            }

            $("#mall_id").change(function () {
                getExhibitionsForSelectedMall();
            });

            $("#provinces").change(function () {

                var $province = $('#provinces').val();
                reloadProvinces($province);
            });

            allExhibitions();
        });

        function allExhibitions() {
            $.ajax({
                url: 'exhibitions/mallExhibitions/0',
                data: {},
                dataType: 'json',
                success: function (data) {

//                console.log(data.data);

                    $("#exhibition_data").empty();
                    var newData = data.data;

                    listExhibitions(newData);

                }

            });
        }

        function reloadProvinces(_province) {
            if (_province == '') {
                $('#mall_id').empty().append('<option value="">Please Select a Province...</option>');
                $('#mall_id').attr('disabled', 'disabled');

                allExhibitions();
            } else {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: '/exhibitions/provincefilter/' + _province,
                    success: function (data) {
                        if (data.malls.length) {
                            $('#mall_id').empty().append('<option value="">Please select a Mall...</option>');
                            if (data.malls) {
                                $.each(data.malls, function (i, val) {
                                    $('#mall_id').append('<option value="' + val.mallID + '">' + val.name + ' (' + val.totalExhibitions + ')</option>');
                                });
                            }

                            $('#exhibition_data').html('<tr><td colspan="5">Please select a mall to see a list of Exhibitions!</td></tr>');
                            if (data.exhibitions.total) {
                                listExhibitions(data.exhibitions.data);
                            }

                            $('#mall_id').removeAttr('disabled');
                        } else {
                            $('#exhibition_data').html('<tr><td colspan="5">There are no Malls in this Province with current Exhibitions!</td></tr>');
                            $('#mall_id').empty().append('<option value="">Please Select a Province...</option>');
                            $('#mall_id').attr('disabled', 'disabled');
                        }

                    }
                });
            }
        }

        function getExhibitionsForSelectedMall() {

            var mallID = $("#mall_id").val();

            $.ajax({
                url: 'exhibitions/mallExhibitions/' + mallID,
                data: {},
                dataType: 'json',
                success: function (data) {

                    $("#exhibition_data").empty();
                    var newData = data.data;

                    listExhibitions(newData);

                }

            });
        }

        function listExhibitions(data) {
            $("#exhibition_data").empty();
            var _html = '';
            $.each(data, function (j, val) {
                var m_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

                var start = new Date(val.startDate.replace(/-/g, "/"));
                var end = new Date(val.endDate.replace(/-/g, "/"));
                var start_date = start.getDate();
                var get_start_month = start.getMonth();
                var start_month = m_names[get_start_month];
                var start_year = start.getFullYear();
                var fin_date = end.getDate();
                var get_month = end.getMonth();
                var fin_month = m_names[get_month];
                var fin_year = end.getFullYear();

                _html = "<tr><td><a href='/malls/view/" + val.mallID + "/" + slugify(val.mall) + "'>" + val.mall + "</a>";

                if (val.mallCity && val.mallProvince) {
                    _html += '<br><a href="/malls/?city=' + val.mallCityID + '-' + slugify(val.mallCity) + '" class="dark-link">' + val.mallCity + '</a>, <a href="/malls/provinceID/' + val.mallProvinceID + '" class="dark-link">' + val.mallProvince + '</a>';
                }
                _html += "</td><td><a href='exhibitions/view/" + val.exhibitionID + "'>" + val.name + "</a></td><td>" + start_date + ' ' + start_month + ' ' + start_year + "</td><td>" + fin_date + ' ' + fin_month + ' ' + fin_year + "</td></tr>";
                $("#exhibition_data").append(_html);

            });
        }
    </script>
@stop
