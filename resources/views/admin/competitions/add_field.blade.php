<div class="container">
    @if(isset($field))
        {{ Form::model($field, array('method' => 'post' ,'name' => 'addField', 'id' => 'addField', 'data-id'=>"$field->formFieldID",'class' => 'form-horizontal my_form')) }}
    @else
        {{ Form::open(array( 'method' => 'post', 'name' => 'addField', 'id' => 'addField','data-id'=>'new', 'class' => 'themed form-horizontal')) }}
    @endif
    <h4>{{$heading}}</h4>

    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="name">Field Name</label>
            {{ Form::text('name',NULL, array('id'=>'name','class' => 'form-control form-control-sm')) }}
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="mandatory">Is The Field Mandatory</label>
            <br/>
            <div class="form-check form-check-inline">
                <?php
                $selectedY = '';
                $selectedN = '';
                if (isset($field['mandatory'])) {
                    if ($field->mandatory === 'Y') {
                        $selectedY = true;
                    } else {
                        $selectedN = true;
                    }
                }
                ?>
                Yes &nbsp;{{ Form::radio('mandatory','Y',$selectedY) }}&nbsp;&nbsp;
                No &nbsp;{{ Form::radio('mandatory','N',$selectedN) }}
            </div>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-12">
            <label for="typeOfField">Type of Field</label>
            <div class="form-group col-md-12">
                {{ Form::select('typeOfField',$fieldTypes,NULL, array('id'=>'typeOfField','class' => 'form-control form-control-sm')) }}
                @if(isset($prev_type) )
                    {{ Form::hidden('prev_type',$prev_type,array('id'=>'prev_type'))}}
                @else
                    {{ Form::hidden('prev_type','none',array('id'=>'prev_type'))}}
                @endif
            </div>
        </div>
    </div>
    <div class="" id="textfields">
        <div class="form-row" id="defaultValue">
            <div class="form-group col-md-12">
                <label for="defaultValue">Default Value</label>
                <div class="controls">
                    <?php
                    $class = "";
                    if (isset($field) && $prev_type == "dob") {
                        $class = "datepicker";
                    }
                    ?>
                    @if(isset($field->formFieldSpecs->defaultValue))
                        {{ Form::text('defaultValue',$field->formFieldSpecs->defaultValue,array('class'=>"txtbar $class span4")) }}
                    @else
                        {{ Form::text('defaultValue',NULL,array('class'=>"form-control form-control-sm")) }}
                    @endif
                </div>
            </div>
        </div>
        <div class="form-row" id="size">
            <div class="form-group col-md-12">
                <label for="size">Text Field Size</label>
                <div class="form-group col-md-12">
                    @if(isset($field->formFieldSpecs->size))
                        {{ Form::select('size',$size,$field->formFieldSpecs->size,array('class'=>'form-control form-control-sm')) }}
                    @else
                        {{ Form::select('size',$size,200,array('class'=>'form-control form-control-sm')) }}
                    @endif
                </div>
            </div>
        </div>
        <div class="form-row" id="maxlength">
            <div class="form-group col-md-12">
                <label for="maxlength">Maxlength</label>
                <div class="form-group col-md-12">
                    @if(isset($field->formFieldSpecs->defaultValue))
                        {{ Form::text('maxlength',$field->formFieldSpecs->defaultValue,array('class'=>'form-control form-control-sm')) }}
                    @else
                        {{ Form::text('maxlength',null,array('class'=>'form-control form-control-sm')) }}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="form-row" id="filefields">
        <div class="form-group col-md-12" id="fileTypes">
            <label for="fileTypes">File Types</label>
            <div class="form-group col-md-12">
                @if(isset($field->formFieldSpecs->fileTypes))
                    <?php
                    dd($field->formFieldSpecs->fileTypes);
                    $selectedTypes = explode(',', $field->formFieldSpecs->fileTypes);
                    ?>
                    {{ Form::select('fileTypes[]',$fileTypes,$selectedTypes, array('multiple'=>'true')) }}
                @else
                    {{ Form::select('fileTypes[]',$fileTypes,NULL, array('multiple'=>'true')) }}
                @endif
            </div>
        </div>
    </div>
    <div id="textareafields">
        <div class="form-row">
            <div class="form-group col-md-12" id="numRows">
                <label for="rows">Number of Rows</label>
                <div class="form-group col-md-12">
                    @if(isset($field->formFieldSpecs->rows))
                        {{ Form::select('rows',$rows,$field->formFieldSpecs->rows,array('class'=>'form-control form-control-sm','id'=>'rows')) }}
                    @else
                        {{ Form::select('rows',$rows,'4',array('class'=>'form-control form-control-sm','id'=>'rows')) }}
                    @endif
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12" id="numCols">
                <label for="cols">Number of Columns (Textarea width)</label>
                <div class="form-group col-md-12">
                    @if(!isset($field->formFieldSpecs->cols))
                        {{ Form::select('cols',$width,'30',array('class'=>'form-control form-control-sm','id'=>'cols')) }}
                    @else
                        {{ Form::select('cols',$width,NULL,array('class'=>'form-control form-control-sm','id'=>'cols')) }}
                    @endif
                </div>
            </div>
        </div>
        <div class="form-row">
            <div class="form-group col-md-12" id="wordWrap">
                <label for="wrap">Word Wrapping</label>
                <div class="form-group col-md-12">
                    @if(!isset($field->formFieldSpecs->wrap))
                        {{ Form::select('wrap',$wrap,'soft',array('class'=>'form-control form-control-sm','id'=>'wrap')) }}
                    @else
                        {{ Form::select('wrap',$wrap,NULL,array('class'=>'form-control form-control-sm','id'=>'wrap')) }}
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div id="radiofields">
        <div class="form-row">
            <div class="form-group col-md-12" id="numRadioOptions">
                <label for="rows">Number of Radio Button Options</label>
                <div class="form-group col-md-12">
                    @if(isset($field->formFieldElements) && sizeof($field->formFieldElements)>0)
                        {{ Form::select('numRadioOptions',$rows,sizeof($field->formFieldElements),array('class'=>'form-control form-control-sm','id'=>'numRadioOptions')) }}
                    @else
                        {{ Form::select('numRadioOptions',$rows,2,array('class'=>'form-control form-control-sm','id'=>'numRadioOptions')) }}
                    @endif
                </div>
            </div>
            <table class="table names" id="radios">

            </table>
        </div>
    </div>
    <div id="checkfields">
        <div class="form-row">
            <div class="form-group col-md-12" id="numCheckboxOptions">
                <label for="numCheckboxOptions">Number of Checkbox Options</label>
                <div class="form-group col-md-12">
                    {{ Form::select('numCheckboxOptions',$rows,'2',array('class'=>'form-control form-control-sm','id'=>'numCheckboxOptions')) }}
                </div>
            </div>
            <table class="table names" id="checks">

            </table>
        </div>
    </div>
    <div class="form-row" id="numSelectOptions">
        <div class="form-group col-md-12">
            <label for="numSelectOptions">Number of Drop Down List Options</label>
            <div class="form-group col-md-12">
                @if(isset($field->name) && sizeof($field->formFieldElements)>0)
                    {{ Form::select('numSelectOptions',$rows,sizeof($field->formFieldElements),array('class'=>'form-control form-control-sm','id'=>'numSelectOptions')) }}
                @else
                    {{ Form::select('numSelectOptions',$rows,'2',array('class'=>'form-control form-control-sm','id'=>'numSelectOptions')) }}
                @endif
            </div>
        </div>
    </div>
    <div class="form-row" id="multiple">
        <div class="form-group col-md-12">
            <label for="multiple">Allow Multiple Selections</label>
            <div class="form-group col-md-12">
                Yes &nbsp;{{ Form::radio('multiple','Y') }}&nbsp;&nbsp;
                No &nbsp;{{ Form::radio('multiple','N') }}
            </div>
            <table class="table names" id="selects">

            </table>
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-12">
            <button class="submit reg-btn btn btn-primary btn-sm" id="btn_submit">Submit Details</button>
        </div>
    </div>
    {{ Form::close() }}
</div>