<div class="form-group mt-2" style="width: 80%;">
    <table class="table table-striped table-responsive">
        <thead>
        <tr>
            <th style="width: 15%" scope="col">Field Name</th>
            <th style="width: 15%" scope="col">What the field will look like</th>
            <th style="width: 5%"></th>
            <th style="width: 15%"></th>
        </tr>
        </thead>
        <tbody id="the_fields">
        @if( is_object($formFields) )
            @foreach($formFields as $key=>$field)

                <tr class="{{$field->name}}">
                    <td>
                        {{$field->name}}{{$field->mandatory == "Y"?'*':''}}
                    </td>
                    <td class="field">
                        <?php
                        if (sizeof($field->formFieldElements) > 0) {
                            $elems = $field->formFieldElements;
                            $class = $field['typeOfField'];
                            if ($class != "select") {
                                foreach ($elems as $elem) {
                                    if ($elem->checkSelected == 'Y') {
                                        $selected = true;
                                    } else {
                                        $selected = false;
                                    }
                                    echo '<label class="radio inline" style="margin-right: 3px; margin-left: 3px;"> ';
                                    echo $elem->name;
                                    echo '</label>';
                                    echo Form::$class('field[]', $selected, array('data-id' => $field->formFieldID));
                                }
                            } else {
                                $selected = $field->formFieldSelectedElements->lists('formElementID');
                                $elems = $field->formFieldElements->lists('name', 'formElementID');
                                $array['data-id'] = $field->formFieldID;
                                if ($field->formFieldSpecs->multiple == "Y") {
                                    $array['multiple'] = 'true';
                                }
                                echo Form::$class('field', $elems, $selected, $array);
                            }
                        } else {
                            $class = $field['typeOfField'];
                            $specs = array();
                            //dd('hi')
                            if ($class == "textarea") {
                                $specs = array('class' => 'span4', 'rows' => $field->formFieldSpecs->rows, 'cols' => $field->formFieldSpecs->cols, 'wrap' => $field->formFieldSpecs->wrap);
                            }
                            if ($class == "text" || $class == "dob") {
                                $specs = array('size' => $field->formFieldSpecs->size);
                            }
                            $specs['data-id'] = $field->formFieldID;
                            $specs['class'] = 'form-control form-control-sm';
                            if ($class != "dob") {
                                if ($class == 'file') {
                                    echo Form::$class('field', [], $specs);
                                } else {
                                    echo Form::$class('field', $field->formFieldSpecs->defaultValue, $specs);
                                }
                            } else {
                                $specs['class'] = 'datepicker';
                                echo Form::text('field', $field->formFieldSpecs->defaultValue, $specs);
                            }
                        }
                        ?>
                    </td>

                    <td class="move">{{ Form::select('move',$moves,NULL, array('id'=>'move','data-id'=>$field->formFieldID,'class' => 'with-search custom-select custom-select-sm to_move')) }}</td>
                    <td>
                        <a class="btn btn-info btn-mini data-field action_edit" data-function="{{$field['function']}}"
                           data-id="{{$field->formFieldID}}"><i class="fa fa-pencil"></i> Update</a>
                        <a class="btn btn-danger btn-mini confirm action_remove" href="#"
                           data-id="{{$field->formFieldID}}"><i
                                    class="fa fa-trash-o"></i> Remove</a>
                    </td>
                </tr>
            @endforeach
        @else

        @endif
        </tbody>
    </table>
</div>
