@extends('layouts.admin')

@section('content')

    <div id="create-competition" class="container" style="padding-bottom: 150px">
        @include('notifications')

        <div class="card shadow mb-4">
            @if(isset($competition))
                {{ Form::model($competition, array('route' => ['competitions.update.post', $competition->formID], 'method' => 'post','files'=>true ,'name' => 'bookingForm', 'id' => 'competitionForm', 'data-id'=>"$competition->formID",'class' => 'my_form')) }}
                {{ Form::hidden('competitionID',$competition->formID,array('id'=>'competitionID'))}}
            @else
                {{ Form::open(array('route' => ['competitions.create.post'], 'method' => 'post','files'=>true , 'name' => 'bookingForm', 'id' => 'competitionForm','data-id'=>'new', 'class' => 'themed')) }}
                {{ Form::hidden('competitionID',null,array('id'=>'competitionID'))}}
            @endif

            @csrf
            <h5 class="card-header">{{ $heading }}</h5>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="mallID">Mall</label>
                        <select name="mallID" id="mallID" class="form-control form-control-sm">
                            <option value="">Select a Mall</option>
                            @foreach($malls as $mall)
                                <option value="{{ $mall->mallID }}"
                                        @if(isset($mallID) && $mall->mallID == $mallID) selected="selected" @endif >
                                    {{ $mall->name }}
                                </option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">
                            {{ ($errors->has('mallID') ? $errors->first('mallID') : '') }}
                        </div>
                    </div>
                </div>
                <h4>Details</h4>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="recipient">Form Recipient*</label>
                        {{ Form::text('recipient',NULL, array('id'=>'recipient','placeholder'=>'email', 'class' => 'form-control form-control-sm')) }}
                        <div class="invalid-feedback">
                            {{ ($errors->has('recipient') ? $errors->first('recipient') : '') }}
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="subject">Title
                            <small> Also appears in the subject line of the e-mail</small>
                        </label>
                        {{ Form::text('subject',NULL, array('id'=>'subject', 'placeholder'=>'title', 'class' => "form-control form-control-sm")) }}
                        <div class="invalid-feedback">
                            {{ ($errors->has('subject') ? $errors->first('subject') : '') }}
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="startDate">Start Date*</label>
                        {{ Form::text('startDate',NULL, array('id'=>'startDate','class' => 'datepicker', 'placeholder'=>'start date', 'class'=>"form-control form-control-sm")) }}
                        <div class="invalid-feedback">
                            {{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="endDate">End Date *</label>
                        {{ Form::text('endDate',NULL, array('id'=>'endDate','class' => 'form-control form-control-sm datepicker', 'placeholder'=>'end date')) }}
                        <div class="invalid-feedback">
                            {{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="description">Description
                            <small class="help-inline"> Optional text describing the competition</small>
                        </label>
                        {{ Form::textarea('description',NULL, array('id'=>'description','class' => 'form-control wysi-text-area','rows'=>'5')) }}
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="image1">Image</label>
                        <div class="control-group">
                            <div class="file-field">
                                @if(isset($competition->thumbnail ))
                                    <img class='thumbnail'
                                         src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mallID . '/' . $competition->thumbnail }}"/>
                                    <br/>
                                @endif
                                <div class="btn btn-light">
                                    {{ Form::file('image1',array('id'=>'image1')) }}
                                    <br>
                                    <br>
                                    <button id="clearImage" class="btn btn-mini btn-warning btn-sm float-left"
                                            type="button">Clear
                                        Upload
                                    </button>
                                </div>
                                <small class="form-text text-muted">Allowed filetypes: .jpg, .png, .gif
                                    <br/>
                                    Max filesize: 1MB
                                </small>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image1') ? $errors->first('image1') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="TsandCsFile">Terms & Conditions Upload</label>
                        <div class="control-group">
                            <div class="file-field">
                                <div class="btn btn-light">
                                    {{ Form::file('TsandCsFile', array('id'=>'TsandCsFile')) }}
                                    @if(isset($filePath))
                                        <br>
                                        <br>
                                        <span class="terms"><?php echo "<a target='_blank' href='" . $filePath . "' />" . $competition->TsandCsFile . "</a><br/>" ?></span>
                                    @endif
                                    <br>
                                    <br>
                                    <button id="clearPDF" class="btn btn-mini btn-warning btn-sm float-left"
                                            type="button">Clear Upload
                                    </button>
                                </div>
                                <small class="form-text text-muted">Allowed filetypes: .pdf, .doc
                                    <br/>
                                    Max filesize: 1MB
                                </small>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('TsandCsFile') ? $errors->first('TsandCsFile') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="activated">Display the Competition*</label>
                        <br/>
                        <div class="form-check form-check-inline">
                            {{ Form::radio('activated', 'Y', ['class' => 'form-check-input']) }}
                            <label class="form-check-label label-inline" for="activated1"> &nbsp;Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            {{ Form::radio('activated', 'N', true, ['class' => 'form-check-input']) }}
                            <label class="form-check-label label-inline" for="activated"> No</label>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="facebook">Display on Facebook
                            <br/>
                            <small>"Yes" means the Competition will NOT display on Mallguide or the Mall site
                            </small>
                        </label>
                        <div class="form-check form-check-inline">
                            {{ Form::radio('facebook', 'Y', ['class' => 'form-check-input']) }}
                            <label class="form-check-label label-inline" for="facebook1">&nbsp;Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            {{ Form::radio('facebook', 'N', true, ['class' => 'form-check-input']) }}
                            <label class="form-check-label label-inline" for="facebook">&nbsp;No</label>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="noForm">Display Online Form</label>
                        <br/>
                        @if(!isset($competition))
                            {{ Form::radio('noForm','N',true, ['class' => 'form-check-input']) }}Yes
                        @else
                            {{ Form::radio('noForm','N', ['class' => 'form-check-input']) }}Yes
                        @endif
                        {{ Form::radio('noForm','Y', ['class' => 'form-check-input']) }} No
                        <br/>
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="thanksMsg">Competition Confirmation Message*
                            <br/>
                            <small> Message displayed to the user after the form is submitted</small>
                        </label>
                        <br/>
                        {{ Form::textarea('thanksMsg',NULL, array('id'=>'thanksMsg','rows'=>'2', 'class' => 'form-control')) }}
                    </div>
                    <div class="form-group col-md-4">
                        <label for="subject">Submit Button Text*
                            <small>Text appearing on the form's submit button</small>
                        </label>
                        @if(isset($competition))
                            {{ Form::text('submitBtnText',NULL, array('id'=>'submitBtnText','placeholder'=>'email', 'class' => 'form-control form-control-sm')) }}
                        @else
                            {{ Form::text('submitBtnText','Send', array('id'=>'submitBtnText','placeholder'=>'email', 'class' => 'form-control form-control-sm')) }}
                        @endif
                        <div class="invalid-feedback">
                            {{ ($errors->has('submitBtnText') ? $errors->first('submitBtnText') : '') }}
                        </div>
                    </div>
                </div>
                <h4>Add competition fields</h4>
                <button id="add_field" type="button" class="btn btn-info">
                    Add Field
                </button>
                <div class="form-row form_fields">
                    <div class="form-group">
                        <br/>
                        <table class="table table-striped table-responsive">
                            <thead>
                            <tr>
                                <th>Field Description</th>
                                <th>What the field will look like</th>
                                <th></th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="the_fields">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                * Required fields
                <button type="submit" class="btn btn-primary float-right">
                    @if(isset($competition))
                        Update Competition
                    @else
                        Create Competition
                    @endif
                </button>
            </div>
            </form>
        </div>
    </div>
@endsection
@section('javascript')
    <script src="{{ asset('js/create_competition.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/gijgo.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/bs-custom-file-input.min.js') }}"></script>
    <script src="{{ asset('js/tinymce/js/tinymce/tinymce.js') }}"></script>
    <script src="{{ asset('js/jquery.colorbox-min.js') }}"></script>
    <script src="{{ asset('js/fields.js') }}"></script>
    <script>

        if (tempFields === undefined) {
            var tempFields = [];
        }
        // Register plugin with a short name
        $(document).ready(function () {
            tinymce.init({
                selector: '.wysi-text-area',
                plugins: "image link -mailto ",
                image_advtab: true,
                menubar: false,
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: ''
            });

            $('#startDate').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy-mm-dd'
            });

            $('#endDate').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy-mm-dd'
            });

            bsCustomFileInput.init()

            if ($("input[type='radio'][name=noForm]:checked").val() === 'Y') {
                $("#this_comp_fields").hide();
            }

            $("input[name=noForm]:radio").click(function () {

                if ($(this).val() === 'Y') {
                    $("#this_comp_fields").hide();
                } else {
                    $("#this_comp_fields").show();
                }
            });
            var form_id = $("#competitionID").val();

            if (form_id !== undefined && form_id !== "") {
                get_fields(form_id);
            } else {
                //GEt Json form fields
                if (tempFields.length > 0) {
                    show_jsonFields();
                } else {
                    get_jsonFields();
                }
            }


            activate_date();
            //activate_dob();
            $('#clearImage').on('click', function (evt) {
                var control = $('#image1');

                var cloned = '<input id="image1" name="image1" type="file">';
                control.replaceWith(cloned);
            });


            $('#clearPDF').on('click', function (evt) {
                var control = $('#TsandCsFile');
                var cloned = '<input id="TsandCsFile" name="TsandCsFile" type="file">';
                control.replaceWith(cloned);
                @if(isset($competition))
                $.ajax(
                    {
                        type: 'GET',
                        url: '/competitions/clearTerms/' + form_id,
                        success: function () {
                            $(".terms").html("")
                        }
                    });
                @endif
            });
            var form_id = $("#competitionForm").data('id');

            if ($('input[value="N"][name="noForm"]:radio').attr('checked')) {
                $('.formFields').show();
            } else {
                $('.formFields').hide();
            }

            $("input[name='facebook']:radio").change(function () {
                if ($(this).val() == "Y") {
                    $('input[value="N"][name="activated"]:radio').prop('checked', true);
                    //$('input[value="Y"][name="noForm"]:radio').prop('checked', true);
                }
            });

            $("input[name='noForm']:radio").change(function () {
                if ($(this).val() == "N") {
                    $('.formFields').show();

                } else {
                    $('.formFields').hide();
                }
            });
        })

        $('#formSubmit').on('click', function (evt) {
            //alert('hi');
            evt.preventDefault();
            //alert(form_id);
            $('legend').addClass('dvLoading');

            if (form_id != "new") {
                var the_url = "/admin/updateCompetition/" + form_id;
            } else {
                var the_url = "/admin/createCompetition";
            }

            tinyMCE.triggerSave();

            var formElement = document.getElementById("competitionForm");
            var formData = new FormData(formElement);
            formData.append('image1', $('input[type=file]')[0].files[0]);
            formData.append('TsandCsfile', $('input[type=file]')[1].files[0]);
            $.ajax(
                {
                    url: the_url,
                    type: "POST",
                    //data: $(this).serialize(),
                    data: formData,
                    async: false,
                    contentType: false,
                    processData: false,
                    datatype: "json",
                    beforeSend: function () {
                        $(".validation-error-inline").hide();
                    }
                })
                .done(function (data) {
                    $('legend').removeClass('dvLoading');
                    if (data.validation_failed === 1) {
                        var arr = data.errors;
                        $.each(arr, function (index, value) {
                            if (value.length !== 0) {
                                $("#" + index).after('<br/><span class="text-error validation-error-inline">' + value + '</span>');
                                $("#" + index).parent().parent().addClass('error');
                                var last = index;
                            }
                        });
                        scroll_top();
                    }
                    if (data.validation_passed === 1) {
                        tempFields = [];
                        window.location = '/admin/competitions/' + data.mallID;
                    }
                })
                .fail(function (jqXHR, ajaxOptions, thrownError) {
                    console.log(jqXHR);
                    alert('Error, could not save');
                });
            return false;
        });

        $("#add_field").click(function (e) {
            var formID = $("#competitionID").val();
            var url = "/admin/addField/" + formID;

            e.preventDefault();

            if (formID == '') {
                $("#the_fields tr").remove();
                $.get(url, function (data) {
                    $.colorbox(
                        {
                            html: data,
                            width: "45%",
                            maxHeight: "95%",
                            onComplete: function () {
                                hide_fields();

                                $('#btn_submit').click(function (e) {

                                    e.preventDefault();

                                    var fieldForm = $('#addField').serializeArray();
                                    var fieldString = {};
                                    for (var i = 0; i < fieldForm.length; i++) {
                                        //console.log(fieldForm[i]);

                                        if (fieldForm[i].name != "_token") {
                                            fieldForm[i].name = fieldForm[i].name.replace("[]", "")
                                            if (fieldString[fieldForm[i].name] != undefined) {
                                                fieldString[fieldForm[i].name] += "," + fieldForm[i].value;
                                            } else {
                                                fieldString[fieldForm[i].name] = fieldForm[i].value;
                                            }
                                        }
                                    }
                                    var order = tempFields.length + 1;
                                    fieldString["contentOrder"] = order;
                                    //$fieldForm.push(order)
                                    tempFields.push(fieldString);
                                    //console.log(tempFields);
                                    show_jsonFields();
                                    $.colorbox.close();

                                });
                                $("#bnt_close").click(function () {
                                    $.colorbox.close();

                                });

                                $("input[name=defaultValue]").addClass('timepicker');
                            }
                        });
                });

            } else {
                $.get(url, function (data) {
                    $.colorbox(
                        {
                            html: data,
                            width: "45%",
                            maxHeight: "95%",
                            onComplete: function () {
                                hide_fields();

                                $('#btn_submit').click(function (e) {
                                    e.preventDefault();

                                    $.ajax(
                                        {
                                            type: 'POST',
                                            url: '/admin/createField/' + formID,
                                            data: $('#addField').serialize(),
                                            success: function () {
                                                //location.reload();
                                                get_fields(formID);
                                                $.colorbox.close();
                                            }
                                        });

                                });
                                $("#bnt_close").click(function () {
                                    $.colorbox.close();
                                });
                                $("input[name=defaultValue]").addClass('timepicker');
                            }
                        });
                });
            }

        });

        function activate_date() {
            $('.datepicker').datepicker({dateFormat: "yy-mm-dd"});
        }

        function scroll_top() {
            $("html").scrollTop(120);
        }

        function activate_dob() {
            $('.datepicker').datepicker({
                dateFormat: "yy-mm-dd",
                defaultDate: "-25y-m-d",
                yearRange: "-100:-0",
                changeMonth: true,
                changeYear: true
            });
        }

        function activate_edit_field() {
            $(".action_edit").click(function (e) {
                var fieldID = $(this).data('id');
                var url = "/admin/editField/" + fieldID;

                e.preventDefault();
                $.get(url, function (data) {
                    $.colorbox(
                        {
                            html: data,
                            width: "50%",
                            maxHeight: "95%",
                            onComplete: function () {
                                hide_fields();

                                $('#btn_submit').click(function (e) {
                                    e.preventDefault();

                                    $.ajax(
                                        {
                                            type: 'POST',
                                            url: '/admin/updateField/' + fieldID,
                                            data: $('#addField').serialize(),
                                            success: function () {
                                                location.reload();
                                                $.colorbox.close();
                                            }
                                        });
                                });
                                $("#bnt_close").click(function () {
                                    $.colorbox.close();
                                });
                            }
                        });
                });
            });
        }

        function to_confirm() {
            $('.confirm').click(function () {

                var answer = confirm("Are you sure you want to delete this item?");
                if (answer) {
                    var fieldID = $(this).data('id');

                    deleteField(fieldID);
                    return true;
                } else {
                    return false;
                }
            });
        }

        function deleteField(fieldID) {
            var the_url = "/admin/deleteField/" + fieldID;

            $.ajax(
                {
                    type: 'GET',
                    url: the_url,
                    success: function (data) {
                        var formID = $('#competitionForm').data('id');
                        get_fields(formID);

                    }
                });
        }

        function hide_fields() {
            change_field_types($('#typeOfField').val());

            $("#typeOfField").change(function () {
                change_field_types($(this).val());
            });

        }

        /*functions for add field modal********************/
        function get_edit_elems(IDname, fieldID, type) {
            var url = "/admin/getElements/" + fieldID;

            $.getJSON(url,

                function (the_data) {
                    if (the_data) {
                        var amount = Object.keys(the_data).length;
                        $.each(the_data, function (i, item) {
                            var ii = i + 1;
                            var row = getRow(ii, amount, type, item);
                            if (ii == 1) {
                                $(IDname).html(row);
                            } else {
                                $(IDname + ' tr:last').after(row);
                            }

                        });

                        activateMove();

                    }

                });

        }

        function get_check_forms() {
            //alert("hi");
            var type = "checkbox";
            var amount = $("#numRadioOptions option:selected").val();
            //alert(amount);
            //var stop=amount;
            var the_id = "#checks";
            var fieldID = $('#addField').data('id');

            //alert(fieldID);
            getRows(fieldID, amount, type, the_id);

            $("#numCheckboxOptions").change(function () {

                var old_amount = $('#checks tr').length;
                var new_amount = $("#numCheckboxOptions option:selected").val();

                if (old_amount > new_amount) {
                    for (var ii = old_amount; ii > new_amount; ii--) {
                        $('#checks tr:last').remove();
                    }
                } else {
                    for (var ii = old_amount + 1; ii <= new_amount; ii++) {
                        var row = getRow(ii, new_amount, type);

                        $('#checks tr:last').after(row);
                    }
                }

                activateMove();
                //hideMovements();
            });
        }

        function get_select_form() {
            var type = "select";
            var the_id = "#selects";
            var amount = $("#numSelectOptions option:selected").val();
            var fieldID = $('#addField').data('id');

            getRows(fieldID, amount, type, the_id);

            $("#numSelectOptions").change(function () {

                var old_amount = $(the_id + ' tr').length;
                var new_amount = $("#numSelectOptions option:selected").val();

                if (old_amount > new_amount) {
                    for (var ii = old_amount; ii > new_amount; ii--) {
                        $(the_id + ' tr:last').remove();
                    }
                } else {
                    for (var ii = old_amount + 1; ii <= new_amount; ii++) {
                        var row = getRow(ii, new_amount, type);

                        $(the_id + ' tr:last').after(row);
                    }
                }
                activateMove();
            });
        }

        function getRows(fieldID, amount, type, the_id) {
            var prev_type = $('#prev_type').val();

            //alert(prev_type);
            if (fieldID == 'new' || prev_type != type) {
                var first_row = getRow(1, amount, type);
                $(the_id).html(first_row);
                for (var ii = 2; ii <= amount; ii++) {
                    var row = getRow(ii, amount, type);
                    $(the_id + ' tr:last').after(row);
                }
                activateMove();
            } else {
                var url = "/admin/getElements/" + fieldID;

                $.getJSON(url,
                    function (the_data) {
                        if (the_data) {
                            amount = Object.keys(the_data).length;
                            $.each(the_data, function (i, item) {
                                var ii = i + 1;
                                var row = getRow(ii, amount, type, item);
                                if (ii == 1) {
                                    $(the_id).html(row);
                                } else {
                                    $(the_id + ' tr:last').after(row);
                                }
                            });
                            activateMove();
                        }

                    });
            }

        }

        function getRow(num, total, the_type, item) {
            var sel = "<select class='form-control form-control-sm' data-num='" + num + "'>";
            sel += "<option value=''>Move Option...</option>";
            sel += "<option value='up'>Move up one position</option>";
            sel += "<option value='down'>Move down one position</option>";
            sel += "<option value='top'>Move to top</option>";
            sel += "<option value='bottom'>Move to bottom</option>";

            sel += "</select>"

            var selected = ''
            var value = ''
            if (item) {
                if (item.checkedSelected == 'Y') {
                    var selected = "checked='checked'";
                }
                var value = item.name;
            }


            var row = '<tr><td><label class="col-form-label col-form-label-sm" data-num="' + num + '"  for="add_field">Option Name (' + num + ')</label><input class="form-control form-control-sm" type="text" id="name' + num + '" name="checkName[]" placeholder="name" value="' + value + '"></td>' +
                '<td>' +
                '<label class="col-form-label col-form-label-sm" for="checkedSelected">set checked by default?</label> ' +
                '<input type="checkbox" class="form-control form-control-sm" data-num="' + num + '" value="' + num + '" id="check' + num + '" ' + selected + ' name="checkedSelected' + num + '">' +
                '</td>' +

                '<td class="move1">' + sel + '</td>' +
                '</tr>';
            return row;
        }

        function shopDropdown(form_id) {
            //alert("hi");
            $("#mallID").change(function () {
                var the_val = $("input[name=requestedBy]:checked").val();
                if (the_val == "Shop") {
                    var $mall = $('#mallID').val();
                    $('.shop_drop').load('/admin/shopDropdown/' + $mall + '/' + form_id);
                    $('.shop_drop').show();
                }
            });
        }

        function get_fields(form_id) {

            $('.form_fields').load('/admin/getFields/' + form_id, function () {
                to_confirm();
                activate_edit_field();
                hideMovements(".move select");
                activateFieldMove();
                activate_dob();
            });

        }

        function get_jsonFields() {
            $.ajax({
                url: '/admin/createpageformfields',
                data: '',
                dataType: 'json',
                success: function (data) {
                    var mandatory = '';
                    $.each(data, function (index, value) {

                        $.each(value, function (ind, val) {
                            tempFields.push(val);
                            mandatory = '';
                            if (val.mandatory === 'Y') {
                                mandatory = '*';
                            }

                            var fieldClass = val.typeOfField;
                            var specs = [];

                            if (fieldClass == 'textarea') {
                                specs['class'] = 'span4';
                                specs['rows'] = 3;
                                specs['cols'] = 5;
                                specs['wrap'] = 'soft';
                            }
                            var field = "<input class='form-control form-control-sm' type=" + fieldClass + " name=" + val.name + " >";

                            $("#the_fields").append("<tr class=" + val.name + " id='" + ind + "'><td>" + val.name + "&nbsp;" + mandatory + "</td><td class='field'>" + field + "</td><td></td><td><input type='hidden' name='formCustomFields[]' value='" + JSON.stringify(val) + "'><a class='btn btn-danger btn-mini confirm action_remove' href='#' id='row_" + ind + "'><i class='fa fa-trash-o'></i> Remove</a></td></tr>");


                            $("#row_" + ind).click(function () {
                                var index = jQuery.inArray(val, tempFields);
                                if (index > -1) {
                                    tempFields.splice(index, 1);
                                }
                                $("#" + ind).remove();

                            });
                        });

                    });
                }
            });
        }

        function show_jsonFields() {
            $.each(tempFields, function (a, b) {
                mandatory = '';
                if (b.mandatory === 'Y') {
                    mandatory = '*';
                }
                var fieldClass = b.typeOfField;
                if (fieldClass == 'text') {

                    field = "<input class='form-control form-control-sm' type='" + fieldClass + "' name='" + b.name + "' >";
                }

                if (fieldClass == 'textarea') {

                    field = "<textarea name='" + b.name + "'></textarea>";
                }

                if (fieldClass == 'radio' || fieldClass == 'checkbox') {
                    field = "";
                    var fieldSubNames = b.checkName.split(',');
                    for (var i = 1; i <= fieldSubNames.length; i++) {

                        var buttonChecked = '';

                        if (b['checkedSelected' + i] != undefined) {
                            buttonChecked = 'checked';
                        }

                        var radioOptionText = fieldSubNames[i - 1];
                        //  radioOptions.push(radioOptionText);

                        field += "<input type='" + fieldClass + "' name='" + b.name + "' value='" + radioOptionText + "' " + buttonChecked + ">" + radioOptionText + "&nbsp;&nbsp;";
                    }
                }

                if (fieldClass == 'select') {
                    var fieldSubNames = b.checkName.split(',');
                    var selectOptions = [];
                    var multipleSelect = b.multiple;
                    var multiple = '';
                    if (multipleSelect == 'Y') {
                        multiple = 'multiple';
                    }

                    var selectName = $("#name").val();
                    field = "<select name='" + selectName + "' " + multiple + "><option>Select</option>";

                    for (var i = 1; i <= fieldSubNames.length; i++) {
                        var selected = "";
                        var selectOptionText = fieldSubNames[i - 1];
                        var checked = "checkedSelected" + i;

                        if (b['checkedSelected' + i] != undefined) {
                            selected = "selected";
                        }
                        field += "<option value=" + selectOptionText + " " + selected + ">" + selectOptionText + "</option>";
                    }
                }

                if (fieldClass == 'file') {
                    field = "<input type='" + fieldClass + "' name='" + b.name + "' >";
                }

                if (fieldClass == 'dob') {
                    field = "<input id='dob' class='datepicker hasDatepicker' type='text' name='dob' placeholder='Date of birth'>";
                }

                $("#the_fields").append("<tr class='" + b.name + "' id='" + a + "'><td>" + b.name + "&nbsp;" + mandatory + "</td><td class='field'>" + field + "</td><input type='hidden' name='formCustomFields[]' value='" + JSON.stringify(b) + "'><td></td><td><a class='btn btn-danger btn-mini confirm action_remove' href='#' id='row_" + a + "'><i class='fa fa-trash-o'></i> Remove</a></td></tr>");

                $("#row_" + a).click(function () {
                    var index = jQuery.inArray(a, tempFields);
                    if (index > -1) {
                        tempFields.splice(index, 1);
                    }
                    $("#" + a).remove();

                });
            });

        }

        function mallDropdown(form_id) {
            var the_val = $("input[name=requestedBy]:checked").val();
            if (the_val == "Shop") {
                //alert("hi");
                $('.mall_drop').load('/admin/mallDropdown/' + "Yes" + '/' + form_id, function () {
                    shopDropdown(form_id);
                });
            } else {
                $('.mall_drop').load('/admin/mallDropdown' + '/No' + '/' + form_id, function () {
                    $('.shop_drop').hide();
                });
            }
        }
    </script>
@endsection
