@extends('layouts.admin')

@section('content')
<div class="container" style="padding-bottom: 20px">
    <div id="competition_response_answers" class="mt-2">
        <div class="shadow rounded" style="background-color: #fff; padding-top: 8px; padding-bottom: 1px;">
            <b-col class="my-2 col-3 col float-right">
                <button
                    v-if="winner"
                    v-on:click="toggleWinner"
                    class="btn btn-danger float-right">
                    Remove Winner
                </button>
                <button v-else v-on:click="toggleWinner" class="btn btn-info float-right">Choose as Winner</button>
            </b-col>

            <b-table
                striped
                show-empty
                hover
                :items="answers"
                :fields="fields"
                :busy="isBusy"
            >
                <div slot="table-busy" class="text-center text-danger my-2">
                    <b-spinner class="align-middle"></b-spinner>
                    <strong>Loading...</strong>
                </div>

            </b-table>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('js/competition_response_answers.js') }}" type="text/javascript"></script>
@endsection
