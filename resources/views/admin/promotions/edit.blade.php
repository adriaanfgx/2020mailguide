@extends('layouts.admin')

@section('content')

    <div class="container" style="padding-bottom: 150px">
        @include('notifications')
        @if (isset($promotion))
            <form method="post" action="/admin/promotions/post-edit/{{ $promotion->promotionsMGID }}" enctype="multipart/form-data">
                @else
                    <form method="post" action="/admin/promotions/post-create" enctype="multipart/form-data">
                        <input type="hidden" name="shopMGID" value="{{ $shop->shopMGID }}" />
                        <input type="hidden" name="mallID" value="{{ $shop->mallID }}" />
                        @endif
                        @csrf
                        <div class="card shadow mb-4">
                            <h5 class="card-header">{{ $shop->name }} >> Promotions</h5>
                            <div class="card-body">
                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="title">Title *</label>
                                        <input name="title" type="text" class="form-control form-control-sm" id="title"
                                               value="{{ isset($promotion) ? $promotion->title: old('title') }}">
                                        <div class="invalid-feedback">
                                            {{ ($errors->has('title') ? $errors->first('title') : '') }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-4">
                                        <label for="startDate">Start Date *</label>
                                        <input name="startDate" type="text"
                                               readonly="readonly"
                                               class="form-control form-control-sm date-field"
                                               id="startDate"
                                               value="@if(isset($promotion)) {{ $promotion->startDate }} @else {{ old('startDate') }} @endif">
                                        <div class="invalid-feedback">
                                            {{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="endDate">End Date *</label>
                                        <input name="endDate" type="text"
                                               readonly="readonly"
                                               class="form-control form-control-sm date-field"
                                               id="endDate"
                                               value="@if(isset($promotion)) {{ $promotion->endDate }} @else {{ old('endDate') }} @endif">
                                        <div class="invalid-feedback">
                                            {{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-12">
                                        <label for="promotion">Promotion</label>
                                        <textarea class="wysi-text-area" name="promotion"
                                                  rows="7">{{ isset($promotion) ? $promotion->promotion : old('promotion') }}</textarea>
                                        {{ ($errors->has('promotion') ? $errors->first('promotion') : '') }}
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <label for="image1">Image 1</label>
                                        @if (isset($promotion) && $promotion->image1 !== null)
                                            <br/>
                                            <img
                                                src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $promotion->image1 }}" style="max-width: 400px; max-height: 400px;"/>
                                        @endif
                                        <div class="file-field">
                                            <div class="btn btn-light">
                                                <input type="file" name="image1" accept="image/png, image/jpeg"/>
                                            </div>
                                            <div class="invalid-feedback">
                                                {{ ($errors->has('image1') ? $errors->first('image1') : '') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="image2">Image 2</label>
                                        @if (isset($promotion) && $promotion->image2 !== null)
                                            <br/>
                                            <img
                                                src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $promotion->image2 }}" style="max-width: 400px; max-height: 400px;"/>
                                        @endif
                                        <div class="file-field">
                                            <div class="btn btn-light">
                                                <input type="file" name="image2" accept="image/png, image/jpeg"/>
                                            </div>
                                            <div class="invalid-feedback">
                                                {{ ($errors->has('image2') ? $errors->first('image2') : '') }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                * Required fields
                                <button type="submit" class="btn btn-primary float-right">Save details</button>
                            </div>
                        </div>
                    </form>

    </div>
@endsection

@section('javascript')
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
    <script src="{{ asset('js/tinymce/js/tinymce/tinymce.js') }}"></script>
    <script>
        // Register plugin with a short name
        $(document).ready(function () {
            tinymce.init({
                selector: '.wysi-text-area',
                plugins: "image link -mailto ",
                image_advtab: true,
                menubar: false,
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: ''
            });

            $('#startDate').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy-mm-dd'
            });

            $('#endDate').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy-mm-dd'
            });
        })

    </script>
@endsection
