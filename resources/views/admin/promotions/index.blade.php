@extends('layouts.admin')

@section('content')
    <div class="container" style="padding-bottom: 20px">
        <div id="promotions" class="mt-2">
            <div class="mb-4 filters">
                <div class="" style="padding-left: 0px; padding-right: 10px;">
                    <div class="select">
                        <b-form-select v-model="province" :options="provinces" style="background-image:none" class="mt-0" @change="provinceChange">
                            <template slot="first">
                                <option :value="null">Select a province</option>
                            </template>
                        </b-form-select>
                    </div>
                </div>
                <div class="" style="padding-left: 0px; padding-right: 0px; max-width: 200px;">
                    <div class="select">
                        <b-form-select v-model="mall" :options="mallDropdown" style="background-image:none" class="select" @change="mallChange">
                            <template slot="first">
                                <option :value="null">Select a mall</option>
                            </template>
                        </b-form-select>
                    </div>
                </div>
                <div class="col-6" style="padding-left: 0px; padding-right: 0px;"></div>
            </div>

            <div class="shadow rounded" style="background-color: #fff; padding-top: 8px; padding-bottom: 1px;">

                <b-col class="my-2 col-3 col float-right">
                    <b-input-group size="sm">
                        <b-form-input v-model="filter" placeholder="Filter shops..."></b-form-input>
                        <b-input-group-append>
                            <b-button :disabled="!filter" @click="filter = ''">Clear</b-button>
                        </b-input-group-append>
                    </b-input-group>
                </b-col>

                <b-table
                    striped
                    hover
                    :items="shops"
                    :fields="fields"
                    :filter="filter"
                    :current-page="currentPage"
                    :per-page="perPage"
                    :busy="isBusy"
                    @filtered="onFiltered">

                    <div slot="table-busy" class="text-center text-danger my-2">
                        <b-spinner class="align-middle"></b-spinner>
                        <strong>Loading...</strong>
                    </div>

                    <template slot="actions" slot-scope="row">
                        <b-dropdown class="actions-button" variant="link" size="lg" no-caret>
                            <b-button slot="button-content" class="actions-button" variant="light">
                                <svg data-icon="more" viewBox="0 0 16 16"
                                     style="fill: rgb(102, 120, 138); width: 12px; height: px;">
                                    <path
                                        d="M2 6.03a2 2 0 1 0 0 4 2 2 0 1 0 0-4zM14 6.03a2 2 0 1 0 0 4 2 2 0 1 0 0-4zM8 6.03a2 2 0 1 0 0 4 2 2 0 1 0 0-4z"
                                        fill-rule="evenodd"></path>
                                </svg>
                            </b-button>
                            <b-dropdown-item :href="addPromotionUrl(row)">Add Promotion</b-dropdown-item>
                            <b-dropdown-item :href="viewPromotionsUrl(row)">List Promotions</b-dropdown-item>
                        </b-dropdown>
                    </template>

                </b-table>
                <b-col md="6" class="mb-4">
                    <b-pagination
                        v-model="currentPage"
                        :total-rows="totalRows"
                        :per-page="perPage"
                        class="my-0"
                    ></b-pagination>
                </b-col>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="{{ asset('js/promotions.js') }}" type="text/javascript"></script>
@endsection
