@extends('layouts.admin')

@section('content')
<div class="container" style="padding-bottom: 20px">
    <div id="applicant" class="mt-2">
        <div class="shadow rounded" style="background-color: #fff; padding: 16px; font-size: .8rem;">
            <label style="font-size: 1.2rem; margin-bottom: 28px;">Applicant</label>
            <div class="form-group row">
                <label class="col-2 col-form-label">First name</label>
                <div class="col-sm-10">
                    {{ $applicant->name }}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 col-form-label">Surname</label>
                <div class="col-sm-10">
                    {{ $applicant->surname }}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 col-form-label">Home Tel</label>
                <div class="col-sm-10">
                    {{ $applicant->telephone }}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 col-form-label">Cell</label>
                <div class="col-sm-10">
                    {{ $applicant->cell }}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    {{ $applicant->email }}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-2 col-form-label">CV</label>
                <div class="col-sm-10">
                    <a href="" target="_blank" class="btn btn-primary">Download CV</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

