@extends('layouts.admin')

@section('content')

<div class="container" style="padding-bottom: 150px">
    @include('notifications')

    <div class="card shadow mb-4">
        <form method="post" action="{{ route('jobs.edit.post', $job->jobID) }}" enctype="multipart/form-data">
        @csrf
            <h5 class="card-header">Edit Job</h5>

            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="title">Position *</label>
                        <input name="title"
                               type="text"
                               class="form-control form-control-sm"
                               id="title"
                               value="{{ old('title', $job->title) }}">
                        <div class="invalid-feedback">
                            {{ ($errors->has('title') ? $errors->first('title') : '') }}
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="salary">Salary</label>
                        <input name="salary"
                               type="text"
                               class="form-control form-control-sm"
                               id="salary"
                               value="{{ old('salary', $job->salary) }}">
                        <div class="invalid-feedback">
                            {{ ($errors->has('salary') ? $errors->first('salary') : '') }}
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="refNum">Ref No</label>
                        <input name="refNum"
                               type="text"
                               class="form-control form-control-sm"
                               id="refNum"
                               value="{{ old('refNum', $job->refNum) }}">
                        <div class="invalid-feedback">
                            {{ ($errors->has('refNum') ? $errors->first('refNum') : '') }}
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="startDate">Start Date *</label>
                        <input name="startDate" type="text"
                               readonly="readonly"
                               class="form-control form-control-sm date-field"
                               id="startDate"
                               value="{{ old('startDate', $job->startDate) }}">
                        <div class="invalid-feedback">
                            {{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="endDate">End Date *</label>
                        <input name="endDate" type="text"
                               readonly="readonly"
                               class="form-control form-control-sm date-field"
                               id="endDate"
                               value="{{ old('endDate', $job->endDate) }}">
                        <div class="invalid-feedback">
                            {{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}
                        </div>
                    </div>
                </div>
                <h4>Contact Details</h4>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="contactPerson">Contact Person</label>
                        <div class="controls">
                            <input name="contactPerson"
                                   type="text"
                                   class="form-control form-control-sm" id="contactPerson"
                                   value="{{ old('contactPerson', $job->contactPerson) }}">
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="cell">Cell</label>
                        <div class="controls">
                            <input name="cell" type="text"
                                   class="form-control form-control-sm" id="cell"
                                   value="{{ old('cell', $job->cell) }}">
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="leasingTelephone">Tel</label>
                        <div class="controls">
                            <input name="telephone" type="text"
                                   class="form-control form-control-sm" id="telephone"
                                   value="{{ old('leasingTelephone', $job->leasingTelephone) }}">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="fax">Fax</label>
                        <div class="controls">
                            <input name="fax" type="text"
                                   class="form-control form-control-sm" id="fax"
                                   value="{{ old('fax', $job->fax) }}">
                        </div>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="leasingEmail">Email</label>
                        <div class="controls">
                            <input name="email"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="email"
                                   value="{{ old('email', $job->email) }}">
                            <div class="invalid-feedback">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="description">Description*</label>
                        <textarea
                            name="description"
                            class="form-control wysi-text-area"
                            id="description"
                            rows="3">{{ old('description', $job->description) }}</textarea>
                        <div class="invalid-feedback">
                            {{ ($errors->has('description') ? $errors->first('description') : '') }}
                        </div>
                    </div>
                </div>
                <h4>Settings</h4>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="showInfo">Must contact details for job be displayed?</label>
                        <br/>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input"
                                   type="radio"
                                   name="showInfo"
                                   id="showInfo"
                                   value="Y" @if($job->showInfo == 'Y') checked @endif>
                            <label class="label-inline" for="showInfo">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input"
                                   type="radio"
                                   name="showInfo"
                                   id="showInfo1"
                                   value="N" @if($job->showInfo == 'N') checked @endif>
                            <label class="label-inline" for="showInfo1">No</label>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="display">Must job be displayed?</label>
                        <br/>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input"
                                   type="radio"
                                   name="display"
                                   id="display"
                                   value="Y" @if($job->display == 'Y') checked @endif>
                            <label class="label-inline" for="display">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input"
                                   type="radio"
                                   name="display"
                                   id="display1"
                                   value="N" @if($job->display == 'N') checked @endif>
                            <label class="label-inline" for="display1">No</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                * Required fields
                <button type="submit" class="btn btn-primary float-right">Save Job</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('js/gijgo.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/tinymce/js/tinymce/tinymce.js') }}"></script>
<script>
    // Register plugin with a short name
    $(document).ready(function () {
        tinymce.init({
            selector: '.wysi-text-area',
            plugins: "image link -mailto ",
            image_advtab: true,
            menubar: false,
            force_br_newlines: true,
            force_p_newlines: false,
            forced_root_block: ''
        });

        $('#startDate').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd'
        });

        $('#endDate').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd'
        });
    })

</script>
@endsection
