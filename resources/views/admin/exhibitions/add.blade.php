@extends('layouts.admin')

@section('content')

<div class="container" style="padding-bottom: 150px">
    @include('notifications')

    <div class="card shadow mb-4">
        <form method="post" action="/admin/exhibitions/create/{{ $mall->mallID }}" enctype="multipart/form-data">
            @csrf
            <h5 class="card-header">{{ $mall->name }} > Add an exhibition</h5>

            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="name">Exhibition Name *</label>
                        <input name="name" type="text" class="form-control form-control-sm" id="name"
                               value="{{ old('name') }}">
                        <div class="invalid-feedback">
                            {{ ($errors->has('name') ? $errors->first('name') : '') }}
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="exhibition">Exhibition Description</label>
                        <textarea name="exhibition" class="form-control wysi-text-area" id="exhibition"
                                  rows="3">{{ old('exhibition') }}</textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="startDate">Start Date *</label>
                        <input name="startDate"
                               type="text"
                               readonly="readonly"
                               class="form-control form-control-sm date-field"
                               id="startDate"
                               value="{{ old('startDate') }}">
                        <div class="invalid-feedback">
                            {{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="endDate">End Date *</label>
                        <input name="endDate" type="text"
                               readonly="readonly"
                               class="form-control form-control-sm date-field"
                               id="endDate" value="{{ old('endDate') }}">
                        <div class="invalid-feedback">
                            {{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="category">Category</label>
                        <select name="category" id="category" class="form-control form-control-sm">
                            <option value="">Please Select...</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->category }}">
                                {{ $category->category }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="location">Venue</label>
                        <select name="location" id="location" class="form-control form-control-sm">
                            <option value="">Please Select...</option>
                            @foreach($locations as $location)
                            <option value="{{ $location->location }}">
                                {{ $location->location }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="location">Location</label>
                        <input
                            name="location"
                            type="text"
                            class="form-control form-control-sm"
                            id="location"
                            value="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="shoppingCouncilMember">Must exhibition be displayed?</label>
                        <br/>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="display"
                                   id="display1" value="Y" checked>
                            <label class="form-check-label label-inline" for="display1">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="display"
                                   id="display" value="N">
                            <label class="form-check-label label-inline" for="display">No</label>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="image3">Exhibition Image (A thumbnail image will be created.)</label>
                        <div class="control-group">
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="thumbnail1" accept="image/png, image/jpeg" />
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('thumbnail1') ? $errors->first('thumbnail1') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="image3">
                            AVAILABLE TO FINEGRAFIX ONLY
                            Alternate images which will not display on Mallguide - Only on the remote mall site
                            Alternate Image 1:(No resizing of image will be done)
                        </label>
                        <div class="control-group">
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="altImage1"  accept="image/png, image/jpeg" />
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('altImage1') ? $errors->first('altImage1') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="altImage2">Alternate Image 2:(No resizing of image will be done)</label>
                        <div class="control-group">
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="altImage2"  accept="image/png, image/jpeg" />
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('altImage2') ? $errors->first('altImage2') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="altImage3">Alternate Image 3(No resizing of image will be done)</label>
                        <div class="control-group">
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="altImage3"  accept="image/png, image/jpeg" />
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('altImage3') ? $errors->first('altImage3') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                * Required fields
                <button type="submit" class="btn btn-primary float-right">Create exhibition</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('js/gijgo.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bs-custom-file-input.min.js') }}"></script>
<script src="{{ asset('js/tinymce/js/tinymce/tinymce.js') }}"></script>
<script>
    // Register plugin with a short name
    $(document).ready(function () {
        tinymce.init({
            selector: '.wysi-text-area',
            plugins: "image link -mailto ",
            image_advtab: true,
            menubar: false,
            force_br_newlines: true,
            force_p_newlines: false,
            forced_root_block: ''
        });

        $('#startDate').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd'
        });

        $('#endDate').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd'
        });

        bsCustomFileInput.init()
    })

</script>
@endsection
