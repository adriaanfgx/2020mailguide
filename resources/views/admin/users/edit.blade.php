@extends('layouts.admin')

@section('content')

    <div id="create_user" class="container" style="padding-bottom: 150px">
        @include('notifications')

        <form method="post" action="/admin/users/edit/{{ $user->id }}">
            @csrf
            <div class="card shadow mb-4">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h5 class="card-header">Update User Details</h5>
                <div class="card-body">
                    <h4>Personal Info</h4>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="name">Email*</label>
                            <input name="email"
                                   type="email"
                                   class="form-control form-control-sm"
                                   id="email"
                                   value="{{ old('email', $user->email) }}"/>
                            <div class="invalid-feedback">
                                {{ ($errors->has('email') ? $errors->first('email') : '') }}
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="admin_level">Admin Level</label>
                            <select class="form-control"
                                    name="admin_level"
                                    id="admin_level">
                                <option value="">Please select ...</option>
                                @foreach($adminLevel as $level)
                                    <option value="{{ $level }}" @if($level == $user->admin_level)
                                    selected="selected" @endif>
                                        {{ $level }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4 form-check" id="mall_select">
                            <input type="checkbox"
                                   name="activated"
                                   class="form-check-input"
                                   id="activated"
                                   @if($user->activated) checked="checked" @endif>
                            <label class="form-check-label" for="activated">Activated User</label>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            @if( $user->admin_level == 'Mall Manager')
                                <div class="control-group">
                                    <label class="control-label" for="mallID">Please select from the list below
                                        to change the user's mall:</label>
                                    <select class="form-control"
                                            name="mallID"
                                            id="mallID">
                                        <option value="">Please select ...</option>
                                        @foreach($malls as $mall)
                                            <option value="{{ $mall->mallID }}"
                                                    @if(isset($selectMall) && $mall->mallID == $selectMall)  selected="selected" @endif>
                                                {{ $mall->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            @elseif( $user->admin_level == 'Marketing Manager' )
                                <div class="control-group">
                                    <label class="control-label" for="user_malls">Please select one or more malls from the list below:</label>
                                    <select class="multi-mall"
                                            name="mallID[]"
                                            multiple="multiple"
                                            id="mallID">
                                        <option value="">Please select ...</option>
                                        @foreach($malls as $mall)
                                            <option value="{{ $mall->mallID }}"
                                                    @if(isset($selectMall) && in_array($mall->mallID, $selectMall)) selected="selected" @endif>
                                                {{ $mall->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            @elseif( $user->admin_level == 'Shop Manager' )
                                <div class="control-group" id="shop_mall_select">
                                    <label for="shop_mall">Select a mall for the user</label>
                                    <select class="multi-shop_mall"
                                            name="shop_mallID[]"
                                            multiple="multiple"
                                            id="shop_mall">
                                        <option value="">Please select ...</option>
                                        @foreach($malls as $mall)
                                            <option value="{{ $mall->mallID }}" @if(isset($selectedMalls) && in_array($mall->mallID, $selectedMalls))
                                            selected="selected" @endif>
                                                {{ $mall->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="control-group"  style="margin-top: 16px;" >
                                    <label class="control-label"for="shopMGID">Update user shops:</label>
                                    <select class="multi-shop"
                                            name="shopMGID[]"
                                            multiple="multiple"
                                            id="shopMGID">
                                        <option value="">Please select...</option>
                                        @foreach($shops as $shop)
                                            <option value="{{ $shop->shopMGID }}" @if(isset($selectedShops) && in_array($shop->shopMGID, $selectedShops))
                                            selected="selected" @endif>
                                                {{ $shop->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                            @if( $user->admin_level == 'Chain Manager' )
                                <div class="control-group" id="chain_name">
                                    <label for="chain_name">Enter a shop name</label>
                                    <select class="multi-chain"
                                            name="chain_name[]"
                                            multiple="multiple"
                                            id="chain">
                                        <option value="">Please select...</option>
                                        @foreach($chains as $key => $name)
                                            <option value="{{ $key}}" @if(isset($userchain) && in_array($key, $userchain->toArray())) selected="selected" @endif>
                                                {{ $name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-md-4">
                            @if( $user->admin_level == 'Mall Manager' || $user->admin_level == 'Marketing Manager' )
                                <div class="control-group">
                                    <label><strong>Malls Selected</strong></label>
                                    <div class="controls">
                                        <ul>
                                            @if(isset($selectedMalls) && count($selectedMalls))
                                                @foreach( $selectedMalls as $selected )
                                                    <li>{{ $selected }}</li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            @elseif( $user->admin_level == 'Chain Manager' )
                                @if(isset($userchain))
                                    <br/>
                                    <div class="control-group">
                                        <label><strong>Chain Store</strong></label>
                                        <div class="controls">
                                            @foreach($userchain as $chain)
                                                {{ $chain }}<br/>
                                            @endforeach
                                        </div>
                                    </div>
                                @endif
                            @elseif( $user->admin_level == 'Shop Manager' )
                                <br/>
                                <div class="control-group">
                                    <label><strong>User Shop(s)</strong></label>
                                    <div class="controls">
                                        <ul>
                                            @if(isset($userShops) && count($userShops))
                                                @foreach( $userShops as $shopID =>$shopName )
                                                    <li>{{ $shopName }}</li>
                                                @endforeach
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <h4>Personal Details</h4>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="first_name">First Name *</label>
                            <input name="first_name"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="first_name"
                                   value="{{ old('first_name', $user->first_name ) }}"/>
                            <div class="invalid-feedback">
                                {{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="last_name">Last Name</label>
                            <input name="last_name"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="last_name"
                                   value="{{ old('last_name', $user->last_name ) }}" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="cell">Cell Phone</label>
                            <input name="cell"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="cell"
                                   value="{{ old('cell', $user->cell) }}"/>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="tel_home">Home phone</label>
                            <input name="tel_home"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="tel_home"
                                   value="{{ old('tel_home', $user->tel_home) }}"/>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="tel_work">Work phone</label>
                            <input name="tel_work"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="tel_work"
                                   value="{{ old('tel_work', $user->tel_work) }}"/>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="fax">Fax</label>
                            <input name="fax"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="fax"
                                   value="{{ old('fax', $user->fax) }}"/>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="postal_address">Postal Address</label>
                            <textarea name="postal_address"
                                      rows="3"
                                      class="form-control"
                                      cols="50">{{ old('postal_address', $user->postal_address) }}</textarea>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="postal_code">Postal Code</label>
                            <input name="postal_code"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="postal_code"
                                   value="{{ old('postal_code', $user->postal_code) }}"/>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="birth_date">Date of Birth</label>
                            <input name="birth_date" type="text"
                                   readonly="readonly"
                                   class="form-control form-control-sm date-field"
                                   id="birth_date"
                                   value="{{ old('birth_date', $user->birth_date) }}"/>
                            <div class="invalid-feedback">
                                {{ ($errors->has('birth_date') ? $errors->first('birth_date') : '') }}
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="province_id">Province</label>
                            <select class="form-control"
                                    name="province_id"
                                    id="province_id"
                                    @change="provinceChange($event)">
                                <option value="">Please select ...</option>
                                @foreach($provinces as $key => $value)
                                    <option value="{{ $key }}" @if($user->province_id == $key) selected="selected" @endif>
                                        {{ $value }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="city">City</label>
                            <select class="form-control"
                                    name="city_id"
                                    id="city">
                                <option value="">Please select ...</option>
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}" @if($user->city_id == $city->id) selected="selected" @endif>
                                        {{ $city->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <h4>Company Details</h4>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="company">Company List</label>
                            <select class="form-control" name="company" id="company">
                                <option value="">Please select ...</option>
                                @foreach($companyList as $value)
                                    <option value="{{ $value->company }}">
                                        {{ $value->company }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="company">Company</label>
                            <input name="company"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="company"
                                   value="{{ old('company', $user->company) }}"/>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="vat_number">Vat Number</label>
                            <input name="vat_number"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="vat_number"
                                   value="{{ old('vat_number', $user->vat_number) }}" />
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save details</button>
                </div>
            </div>
        </form>

    </div>
@endsection

@section('javascript')
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="{{ asset('js/create_user.js') }}"></script>

    <script>
        $(document).ready(function () {
            // alert(1);
            var usertype = $('#user_type').val();

            showHideFields(usertype);
            multiShop();
            // $(".collapse").collapse();
            $(".numeric").mask('0b99999999');

            $(".multi-mall").select2({});
            $(".multi-shop_mall").select2({});
            $(".multi-chain").select2({});
            $(".multi-shop").select2({
                allowClear: true
            });

            $('#sub').attr('disabled', 'disabled');

            $('#birth_date').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy-mm-dd'
            });

            $("#user_type").change(function () {
                usertype = $(this).val();
                showHideFields(usertype);
            });

            $('#password').keyup(function () {
                var _parent = $(this).parent().parent();
                var level = 0;
                var mesg = '';
                var input = $(this).val();

                if (/(.*[A-Z])/.test(input) == false) {
                    level = 1;
                    mesg += ' No uppercase letters.<br/>';
                }
                if (/(.*[\!|\@|\#|\$|\_])/.test(input) == false) {
                    level = 2;
                    mesg += ' No special character (!@#$_).<br/>';

                }
                if (/(.*[\d])/.test(input) == false) {
                    level = 3;
                    mesg += ' No numbers.<br/>';
                }
                if (/(.*[a-z])/.test(input) == false) {
                    level = 4;
                    mesg += ' No lowercase letters.<br/>';
                }
                if (input.length < 6) {
                    level = 5;
                    mesg += ' Not long enough.<br/>';
                }

                $('#password_help').html(mesg);

                if (level != 0) {
                    _parent.addClass('error');
                    $('#sub').attr('disabled', 'disabled');
                } else {
                    $('#sub').removeAttr('disabled');
                }
            });
        });

        function showHideFields(usertype) {
            if (usertype == 'Mall Manager') {
                $("#mall_select").show();
                $("#multi_mall").hide();
                $("#chain_name").hide();
                $("#shop_mall_select").hide();
                $("#shop_multiselect").hide();
                $("#chainList").hide();
            }

            if (usertype == 'Marketing Manager') {
                $("#multi_mall").show();
                $("#mall_select").hide();
                $("#shop_mall_select").hide();
                $("#chain_name").hide();
                $("#chainList").hide();
                $("#shop_multiselect").hide();
            }

            if (usertype == 'Shop Manager') {

                $("#chain_name").hide();
                $("#shop_multiselect").hide();
                $("#chainList").hide();
                $("#shop_mall_select").show();
                $("#mall_select").hide();
                $("#multi_mall").hide();
            }

            if (usertype == 'Chain Manager') {

                $("#chain_name").show();
                $("#mall_select").hide();
                $("#multi_mall").hide();
                $("#shop_mall_select").hide();
                $("#shop_multiselect").hide();
                $("#chainList").hide();
            }
            if (usertype == 'FGX Staff' || usertype == 'Master' || usertype == "") {

                $("#mall_select").hide();
                $("#multi_mall").hide();
                $("#chain_name").hide();
                $("#shop_mall_select").hide();
                $("#shop_multiselect").hide();
                $("#chainList").hide();
            }
        }

        function multiShop(mall) {
            $("#shop_mall").change(function () {
                var mall = $(this).val();
                console.log("Mall is " + mall);
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    data: {mallIDs: mall},
                    url: '/admin/shops/getbymall?mallIds=' + mall,
                    success: function (data) {

                        $("#shop_multiselect").show();
                        $('#multishopMGID').empty();
                        //clear shops
                        $(".multi-shop").select2('data', null)
                        $.each(data, function (key, value) {
                            $('#multishopMGID').append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                });
            });
        }

    </script>
@endsection
