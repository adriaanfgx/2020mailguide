@extends('layouts.admin')

@section('content')
<div class="container" style="padding-bottom: 20px">
    @include('notifications')
    <div id="users" class="mt-2">
        <div class="d-flex mb-4">
            <div class="p-2" style="padding-left: 0px !important;">
                <div class="select">
                    <b-form-select v-model="selected_user_type" :options="user_types" style="background-image:none">
                        <template slot="first">
                            <option :value="null">Type of User</option>
                        </template>
                    </b-form-select>
                </div>
            </div>
            <div class="p-2" style="padding-left: 0px; padding-right: 0px; max-width: 200px;">
                <div class="select">
                    <b-form-select v-model="selected_user_state" :options="user_states" style="background-image:none">
                    </b-form-select>
                </div>
            </div>
            <div class="ml-auto p-2" style="padding-right: 0px !important;">
                <a href="{{ route('users.create.get' ) }}" class="btn btn-primary float-right">Add New</a>
            </div>
        </div>

        <div class="shadow rounded" style="background-color: #fff; padding-top: 8px; padding-bottom: 1px;">
            <b-col class="my-2 col-3 float-right">
                <b-input-group size="sm">
                    <b-form-input v-model="filter" placeholder="Filter users..."></b-form-input>
                    <b-input-group-append>
                        <b-button :disabled="!filter" @click="filter = ''">Clear</b-button>
                    </b-input-group-append>
                </b-input-group>
            </b-col>

            <b-table
                striped
                show-empty
                hover
                :items="filtered_users"
                :fields="fields"
                :filter="filter"
                :current-page="currentPage"
                :per-page="perPage"
                :busy="isBusy"
                @filtered="onFiltered">

                <div slot="table-busy" class="text-center text-danger my-2">
                    <b-spinner class="align-middle"></b-spinner>
                    <strong>Loading...</strong>
                </div>

                <template slot="actions" slot-scope="row">
                    <b-dropdown class="actions-button" variant="link" size="lg" no-caret>
                        <b-button slot="button-content" class="actions-button" variant="light">
                            <svg data-icon="more" viewBox="0 0 16 16"
                                 style="fill: rgb(102, 120, 138); width: 12px; height: 12px;">
                                <path
                                    d="M2 6.03a2 2 0 1 0 0 4 2 2 0 1 0 0-4zM14 6.03a2 2 0 1 0 0 4 2 2 0 1 0 0-4zM8 6.03a2 2 0 1 0 0 4 2 2 0 1 0 0-4z"
                                    fill-rule="evenodd"></path>
                            </svg>
                        </b-button>
                        <b-dropdown-item :href="editUserUrl(row.item)">Edit</b-dropdown-item>
                        <b-dropdown-item v-if="row.item.status != 'suspended'" :href="suspendUserUrl(row.item)">Suspend</b-dropdown-item>
                        <b-dropdown-item v-if="row.item.status == 'suspended'" :href="unSuspendUserUrl(row.item)">Un-Suspend</b-dropdown-item>
                        <b-dropdown-item v-if="row.item.activated != '1'" :href="resendActivationUrl(row.item)">Resend Activation</b-dropdown-item>
                        <b-dropdown-item v-if="row.item.fgx_approved == '0'" :href="approveUserUrl(row.item)">Approve User</b-dropdown-item>
                        <b-dropdown-item v-on:click="confirmDeleteUser(row.item)">Delete</b-dropdown-item>
                    </b-dropdown>
                </template>

            </b-table>

            <b-col>
                <b-pagination
                    v-model="currentPage"
                    :total-rows="totalRows"
                    :per-page="perPage">
                </b-pagination>
            </b-col>
        </div>

    </div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('js/users.js') }}" type="text/javascript"></script>
@endsection
