@extends('layouts.admin')

@section('content')

    <div id="create_user" class="container" style="padding-bottom: 150px">
        @include('notifications')

        <form method="post" action="/admin/users/suspend/{{ $user->id }}">
            @csrf
            <div class="card shadow mb-4">
                <h5 class="card-header">Suspend User</h5>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="suspendTime">Suspend Time</label>
                            <input name="suspendTime"
                                   type="suspendTime"
                                   class="form-control form-control-sm"
                                   id="suspendTime"
                                   value="{{ old('suspendTime') }}"/>
                            <div class="invalid-feedback">
                                {{ ($errors->has('suspendTime') ? $errors->first('suspendTime') : '') }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Suspend User</button>
                </div>
            </div>
        </form>

    </div>
@endsection
