@extends('layouts.admin')

@section('content')

    <div id="create_user" class="container" style="padding-bottom: 150px">
        @include('notifications')

        <form method="post" action="/admin/users/create">
            @csrf
            <div class="card shadow mb-4">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h5 class="card-header">Add a User</h5>
                <div class="card-body">
                    <h4>Login Details</h4>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="name">Email*</label>
                            <input name="email"
                                   type="email"
                                   class="form-control form-control-sm"
                                   id="email"
                                   value="{{ old('email') }}" />
                            <div class="invalid-feedback">
                                {{ ($errors->has('email') ? $errors->first('email') : '') }}
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="admin_level">Type of User</label>
                            <select class="form-control" name="admin_level" id="user_type">
                                <option value="">Please select ...</option>
                                @foreach($adminLevel as $level)
                                    <option value="{{ $level }}">
                                        {{ $level }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4" id="mall_select">
                            <label for="mallID">Select a mall for the user</label>
                            <select class="form-control" name="mallID" id="mall">
                                <option value="">Please select ...</option>
                                @foreach($malls as $key => $value)
                                    <option value="{{ $key }}">
                                        {{ $value }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4" id="shop_mall_select">
                            <label for="shop_mallID">Find shops by mall</label>
                            <select class="multi-shop_mall" name="shop_mallID[]" id="shop_mall" multiple>
                                <option value="">Please select ...</option>
                                @foreach($malls as $key => $value)
                                    <option value="{{ $key }}">
                                        {{ $value }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4" id="shop_multiselect">
                            <label for="multishopMGID">Please select one or more shops from the list below</label>
                            <select class="multi-shop" name="shopMGID[]" id="multishopMGID" multiple>
                                <option value="">Please select ...</option>
                                @foreach($shops as $key => $value)
                                    <option value="{{ $key }}">
                                        {{ $value }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4" id="chain_name">
                            <label for="chain_name">Enter a shop Name</label>
                            <select class="multi-chain" name="chain_name[]" id="chain" multiple>
                                <option value="">Please select ...</option>
                                @foreach($chains as $key => $value)
                                    <option value="{{ $key }}">
                                        {{ $value }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4" id="multi_mall">
                            <label for="multimallID">Please select one or more malls from the list below</label>
                            <select class="multi-mall" name="mallIDs[]" id="multimallID" multiple>
                                <option value="">Please select ...</option>
                                @foreach($malls as $key => $value)
                                    <option value="{{ $key }}">
                                        {{ $value }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <br/>
                    <br/>
                    <br/>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <strong>Password needs to have :</strong>
                            <br/>
                            <ul>
                                <li>At least one upper case english letter</li>
                                <li>At least one lower case english letter</li>
                                <li>At least one digit</li>
                                <li>At least one special character (!@#$_)</li>
                                <li>Minimum 6 characters in length</li>
                            </ul>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="password">Password *</label>
                            <input name="password"
                                   type="password"
                                   class="form-control form-control-sm"
                                   id="password"
                                   value="">
                            <div class="invalid-feedback">
                                {{ ($errors->has('password') ? $errors->first('password') : '') }}
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="password_confirmation">Confirm Password *</label>
                            <input name="password_confirmation"
                                   type="password"
                                   class="form-control form-control-sm"
                                   id="password_confirmation"
                                   value="">
                            <div class="invalid-feedback">
                                {{ ($errors->has('password_confirmation') ? $errors->first('password_confirmation') : '') }}
                            </div>
                        </div>
                    </div>
                    <h4>Personal Details</h4>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="first_name">First Name *</label>
                            <input name="first_name"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="first_name"
                                   value="{{ old('first_name') }}" />
                            <div class="invalid-feedback">
                                {{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="last_name">Last Name</label>
                            <input name="last_name"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="last_name"
                                   value="{{ old('last_name') }}" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="cell">Cell Phone</label>
                            <input name="cell"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="cell"
                                   value="{{ old('cell') }}" />
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="tel_home">Home phone</label>
                            <input name="tel_home"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="tel_home"
                                   value="{{ old('tel_home') }}" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="tel_work">Work phone</label>
                            <input name="tel_work"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="tel_work"
                                   value="{{ old('tel_work') }}" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="fax">Fax</label>
                            <input name="fax"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="fax"
                                   value="{{ old('fax') }}" />
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="postal_address">Postal Address</label>
                            <textarea name="postal_address"
                                      rows="3"
                                      class="form-control"
                                      cols="50">{{ old('postal_address') }}</textarea>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="postal_code">Postal Code</label>
                            <input name="postal_code"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="postal_code"
                                   value="{{ old('postal_code') }}" />
                        </div>
                        <div class="form-group col-md-4">
                            <label for="gender">Gender</label>
                            <br/>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input"
                                       type="radio"
                                       name="gender"
                                       id="gender"
                                       value="Female">
                                <label class="form-check-label label-inline" for="gender">Female</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input"
                                       type="radio"
                                       name="gender"
                                       id="gender1"
                                       value="Male">
                                <label class="form-check-label label-inline" for="gender1">Male</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="birth_date">Date of Birth</label>
                            <input name="birth_date" type="text"
                                   readonly="readonly"
                                   class="form-control form-control-sm date-field"
                                   id="birth_date" value="">
                            <div class="invalid-feedback">
                                {{ ($errors->has('birth_date') ? $errors->first('birth_date') : '') }}
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="province_id">Province</label>
                            <select class="form-control"
                                    name="province_id"
                                    id="province_id"
                                    @change="provinceChange($event)">
                                <option value="">Please select ...</option>
                                @foreach($provinces as $key => $value)
                                    <option value="{{ $key }}">
                                        {{ $value }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="city">City</label>
                            <select class="form-control"
                                    name="city_id"
                                    id="city">
                                <option value="">Please select ...</option>
                                @foreach($cities as $key => $value)
                                    <option value="{{ $key }}">
                                        {{ $value }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <h4>Company Details</h4>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="company">Company List</label>
                            <select class="form-control" name="company" id="company">
                                <option value="">Please select ...</option>
                                @foreach($companyList as $value)
                                    <option value="{{ $value->company }}">
                                        {{ $value->company }}
                                    </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="vat_number">Vat Number</label>
                            <input name="vat_number"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="vat_number"
                                   value=""/>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save details</button>
                </div>
            </div>
        </form>

    </div>
@endsection

@section('javascript')
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="{{ asset('js/create_user.js') }}"></script>

    <script>
        $(document).ready(function () {
            // alert(1);
            var usertype = $('#user_type').val();

            showHideFields(usertype);
            multiShop();
            // $(".collapse").collapse();
            $(".numeric").mask('0b99999999');

            $(".multi-mall").select2({});
            $(".multi-shop_mall").select2({});
            $(".multi-chain").select2({});
            $(".multi-shop").select2({
                allowClear: true
            });

            $('#sub').attr('disabled', 'disabled');

            $('#birth_date').datepicker({
                uiLibrary: 'bootstrap4',
                format: 'yyyy-mm-dd'
            });

            $("#user_type").change(function () {
                usertype = $(this).val();
                showHideFields(usertype);
            });

            $('#password').keyup(function () {
                var _parent = $(this).parent().parent();
                var level = 0;
                var mesg = '';
                var input = $(this).val();

                if (/(.*[A-Z])/.test(input) == false) {
                    level = 1;
                    mesg += ' No uppercase letters.<br/>';
                }
                if (/(.*[\!|\@|\#|\$|\_])/.test(input) == false) {
                    level = 2;
                    mesg += ' No special character (!@#$_).<br/>';

                }
                if (/(.*[\d])/.test(input) == false) {
                    level = 3;
                    mesg += ' No numbers.<br/>';
                }
                if (/(.*[a-z])/.test(input) == false) {
                    level = 4;
                    mesg += ' No lowercase letters.<br/>';
                }
                if (input.length < 6) {
                    level = 5;
                    mesg += ' Not long enough.<br/>';
                }

                $('#password_help').html(mesg);

                if (level != 0) {
                    _parent.addClass('error');
                    $('#sub').attr('disabled', 'disabled');
                } else {
                    $('#sub').removeAttr('disabled');
                }
            });
        });

        function showHideFields(usertype) {
            if (usertype == 'Mall Manager') {
                $("#mall_select").show();
                $("#multi_mall").hide();
                $("#chain_name").hide();
                $("#shop_mall_select").hide();
                $("#shop_multiselect").hide();
                $("#chainList").hide();
            }

            if (usertype == 'Marketing Manager') {
                $("#multi_mall").show();
                $("#mall_select").hide();
                $("#shop_mall_select").hide();
                $("#chain_name").hide();
                $("#chainList").hide();
                $("#shop_multiselect").hide();
            }

            if (usertype == 'Shop Manager') {

                $("#chain_name").hide();
                $("#shop_multiselect").hide();
                $("#chainList").hide();
                $("#shop_mall_select").show();
                $("#mall_select").hide();
                $("#multi_mall").hide();
            }

            if (usertype == 'Chain Manager') {

                $("#chain_name").show();
                $("#mall_select").hide();
                $("#multi_mall").hide();
                $("#shop_mall_select").hide();
                $("#shop_multiselect").hide();
                $("#chainList").hide();
            }
            if (usertype == 'FGX Staff' || usertype == 'Master' || usertype == "") {

                $("#mall_select").hide();
                $("#multi_mall").hide();
                $("#chain_name").hide();
                $("#shop_mall_select").hide();
                $("#shop_multiselect").hide();
                $("#chainList").hide();
            }
        }

        function multiShop(mall) {
            $("#shop_mall").change(function () {
                var mall = $(this).val();
                console.log("Mall is " + mall);
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    data: {mallIDs: mall},
                    url: '/admin/shops/getbymall?mallIds=' + mall,
                    success: function (data) {

                        $("#shop_multiselect").show();
                        $('#multishopMGID').empty();
                        //clear shops
                        $(".multi-shop").select2('data', null)
                        $.each(data, function (key, value) {
                            $('#multishopMGID').append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                });
            });
        }

    </script>
@endsection
