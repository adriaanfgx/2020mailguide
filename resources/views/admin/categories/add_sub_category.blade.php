@extends('layouts.admin')

@section('content')

    <div class="container" style="padding-bottom: 150px">
        @include('notifications')
        @if (isset($subCategory))
            <form method="post" action="/admin/sub-category/update/{{ $subCategory->id }}">
                @else
            <form method="post" action="/admin/sub-category/post-create">
                @endif
                <input type="hidden" name="category_id" value="{{ $category->id }}" />
                @csrf
                <div class="card shadow mb-4">
                    <h5 class="card-header">{{ $heading }}</h5>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="name">Name *</label>
                                <input name="name" type="text" class="form-control form-control-sm" id="name"
                                       value="@if(isset($subCategory)) {{ $subCategory->name }} @else {{ old('name') }} @endif">
                                <div class="invalid-feedback">
                                    {{ ($errors->has('name') ? $errors->first('name') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        * Required fields
                        <button type="submit" class="btn btn-primary float-right">Save details</button>
                    </div>
                </div>
            </form>
    </div>
@endsection

@section('javascript')
@endsection
