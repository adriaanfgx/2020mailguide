@extends('layouts.admin')

@section('content')

    <div class="container" style="padding-bottom: 150px">
        @include('notifications')
        @if (isset($category))
            <form method="post" action="/admin/categories/post-update/{{ $category->id }}">
                @else
            <form method="post" action="/admin/categories/post-create">
                @endif
                @csrf
                <div class="card shadow mb-4">
                    <h5 class="card-header"><a href="/admin/categories">Categories</a> >> {{ $heading }}</h5>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="name">Name *</label>
                                <input name="name" type="text" class="form-control form-control-sm" id="name"
                                       value="@if(isset($category)) {{ $category->name }} @else {{ old('name') }} @endif">
                                <div class="invalid-feedback">
                                    {{ ($errors->has('name') ? $errors->first('name') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        * Required fields
                        <button type="submit" class="btn btn-primary float-right">Save details</button>
                    </div>
                </div>
            </form>
    </div>
@endsection

@section('javascript')
@endsection
