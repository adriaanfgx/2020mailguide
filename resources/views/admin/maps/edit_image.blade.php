@extends('layouts.map')


@section('content')
    <div>

        <img src="{{ asset('img/mallmap_point.png') }}" id="mapPointer" />

        <img src="http://mallguide.co.za/uploadimages/mall_{{ $map->mallID }}/{{ $map->imageSrc }}" id="map_img">

    </div>

        <button id="saveLoc" type="button">Save & Close</button>

@stop

@section('exScript')

    <script>

        var mapPointer = $("#mapPointer");
        var mapPointerWidth = mapPointer.width();
        var mapPointerHeight = mapPointer.height();

        $(document).ready(function(){

            var  xCoord = parseInt(window.opener.$("#mapX").val()) - (mapPointerWidth / 2);
            var  yCoord = parseInt(window.opener.$("#mapY").val()) - mapPointerHeight;

            var imageX = $('#map_img').width();
            var imageY = $('#map_img').height();
            window.resizeTo(imageX,imageY+100);

            if( xCoord == '' || yCoord == ''){
                $("#mapPointer").hide();
            }

            $("#mapPointer").show().css({'top':(yCoord)+'px','left':(xCoord)+'px'});

            $("#rm_btn").click(function(){
                $("#mapPointer").detach();
                $("#mapX",opener.document).val('');
                $("#mapY",opener.document).val('');
            });

            var bodyRect,elemRect,offsetX,offsetY;

            $( "#mapPointer" ).draggable({

                stop: function() {

                    bodyRect = document.body.getBoundingClientRect();
                    elemRect = document.getElementById('mapPointer').getBoundingClientRect();
                    offsetX   = (elemRect.left - bodyRect.left) + (mapPointerWidth / 2);
                    offsetY   = (elemRect.top - bodyRect.top) + mapPointerHeight;
                }
            });

            $('#saveLoc').click(function(){

                $("#mapX",opener.document).val(offsetX);
                $("#mapY",opener.document).val(offsetY);

                window.close();
            });
        });

    </script>
@stop
