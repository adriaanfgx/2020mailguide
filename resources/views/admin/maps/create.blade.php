@extends('layouts.admin')

@section('content')

    <div class="container" style="padding-bottom: 150px">
        @include('notifications')
        @if (isset($map))
            <form method="post" action="/admin/maps/post-update/{{ $map->mapImageMGID }}" enctype="multipart/form-data">
                @else
            <form method="post" action="/admin/maps/post-create/{{ $mall->mallID }}" enctype="multipart/form-data">
                @endif
                @csrf
                <div class="card shadow mb-4">
                    <h5 class="card-header">{{ $heading }}</h5>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="location">Location * (this is the reference text used by the shops)</label>
                                <input name="location" type="text" class="form-control form-control-sm" id="location"
                                       value="@if(isset($map)) {{ $map->location }} @endif">
                                <div class="invalid-feedback">
                                    {{ ($errors->has('location') ? $errors->first('location') : '') }}
                                </div>
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="imageSrc">Map Image *</label>
                                @if (isset($map))
                                    <br />
                                    <a href="#" id="mapImage">{{ $map->imageSrc }}</a>
                                @endif
                                <div class="control-group">
                                    <div class="file-field">
                                        <div class="btn btn-light">
                                            <input type="file" name="imageSrc" accept="image/png, image/jpeg" />
                                        </div>
                                        <div class="invalid-feedback">
                                            {{ ($errors->has('imageSrc') ? $errors->first('imageSrc') : '') }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        * Required fields
                        <button type="submit" class="btn btn-primary float-right">Save details</button>
                    </div>
                </div>
            </form>
    </div>
@endsection

@section('javascript')
    @if (isset($map))
    <script>
        $(document).ready(function(){
            $("#mapImage").click(function(e){
                PopupCenter("{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $map->imageSrc }}","test",600,600);
            });
        });

        function PopupCenter(pageURL, title,w,h) {
            var left = (screen.width/2)-(w/2);
            var top = (screen.height/2)-(h/2);
            var targetWin = window.open (pageURL, title, 'width='+w+', height='+h+', top='+top+', left='+left);
        }
    </script>
    @endif
@endsection
