@extends('layouts.admin')

@section('content')

    <div class="container" style="padding-bottom: 150px">
        @include('notifications')

        <div class="card shadow mb-4">

            <form method="post" action="/admin/shops/post-edit/{{ $shop->shopMGID }}">
                @csrf
                <h5 class="card-header">{{ $shop->name }} >> Edit</h5>
                <a href="/admin/mall-shops/{{ $mall->mallID }}"><<  {{ $mall->name }} shops</a>
                <div id="add_shop" class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="name">Shop Name *</label>
                            <input name="name" type="text" class="form-control form-control-sm" id="name"
                                   value="{{ $shop->name }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('name') ? $errors->first('name') : '') }}
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="category_id">Category</label>
                            <select name="category_id" id="category_id" class="form-control form-control-sm">
                                <option value="">Please Select...</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" @if($category->id == $shop->category_id) selected @endif>{{$category->name }}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="category"  id="category_text"/>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="subcategory">Subcategory</label>
                            <select name="subcategory_id" id="subcategory" class="form-control form-control-sm">
                                <option value="">Please Select...</option>
                                @foreach($subcategories as $key => $value)
                                    <option value="{{ $key }}" @if($key == $shop->subcategory_id) selected @endif>{{$value }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="newShop">Is the shop new?</label><br/>
                            <div class="form-check form-check-inline">
                                @if (isset($shop))
                                    <input class="form-check-input" type="radio" name="newShop" value="Y" @if( $shop->newShop === 'Y' || $shop->newShop === '') checked @endif />
                                @else
                                    <input class="form-check-input" type="radio" name="newShop" value="Y" checked/>
                                @endif
                                <label class="form-check-label" for="newShop">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="newShop" value="N" @if(isset($shop)
                                       && $shop->newShop === 'N') checked @endif />
                                <label class="form-check-label" for="newShop">No</label>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="display">Must Shop be displayed?</label><br/>
                            <div class="form-check form-check-inline">
                                @if (isset($shop))
                                    <input class="form-check-input" type="radio" name="display" value="Y" @if( $shop->display
                                === 'Y') checked @endif />
                                @else
                                    <input class="form-check-input" type="radio" name="display" value="Y" checked/>
                                @endif
                                <label class="form-check-label" for="display">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="display" value="N" @if(isset($shop)
                                       && $shop->display === 'N') checked @endif />
                                <label class="form-check-label" for="display">No</label>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="blog">Does shop have a blog?</label><br/>
                            <div class="form-check form-check-inline">
                                @if (isset($shop))
                                    <input class="form-check-input" type="radio" name="blog" value="1" @if( $shop->blog
                                == 1) checked @endif />
                                @else
                                    <input class="form-check-input" type="radio" name="blog" value="1" checked/>
                                @endif
                                <label class="form-check-label" for="blog">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="blog" value="0" @if(isset($shop)
                                       && $shop->blog == 0) checked @endif />
                                <label class="form-check-label" for="blog">No</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="tradingHours">Trading Hours</label>
                            <textarea class="wysi-text-area" name="tradingHours"
                                      rows="7">{{ $shop->tradingHours }}</textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="description">Description</label>
                            <textarea class="wysi-text-area" name="description"
                                      rows="7">{{ $shop->description }}</textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="keywords">Keywords</label>
                            <textarea class="wysi-text-area" name="keywords"
                                      rows="7">{{ $shop->keywords }}</textarea>
                        </div>
                    </div>


                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save details</button>
                </div>
            </form>
        </div>

        <div class="card shadow mb-4" id="shop_products">
            <form method="post" action="/admin/shops/post-product-details">
                @csrf
                <input type="hidden" name="shopId" value="{{ $shop->shopMGID }}" />
                <h5 class="card-header">Products</h5>

                @if ($message = Session::get('product_success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Success:</strong> {{ $message }}
                    </div>
                    {{ Session::forget('product_success') }}
                @endif

                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="product1">Product 1</label>
                            <input name="product1" type="text" class="form-control form-control-sm" id="product1"
                                   value="{{ $shop->product1 }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="product2">Product 2</label>
                            <input name="product2" type="text" class="form-control form-control-sm" id="product2"
                                   value="{{ $shop->product2 }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="product3">Product 3</label>
                            <input name="product3" type="text" class="form-control form-control-sm" id="product3"
                                   value="{{ $shop->product3 }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="product4">Product 4</label>
                            <input name="product4" type="text" class="form-control form-control-sm" id="product4"
                                   value="{{ $shop->product4 }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="product5">Product 5</label>
                            <input name="product5" type="text" class="form-control form-control-sm" id="product5"
                                   value="{{ $shop->product5 }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="product6">Product 6</label>
                            <input name="product6" type="text" class="form-control form-control-sm" id="product6"
                                   value="{{ $shop->product6 }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="product7">Product 7</label>
                            <input name="product7" type="text" class="form-control form-control-sm" id="product7"
                                   value="{{ $shop->product7 }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="product8">Product 8</label>
                            <input name="product8" type="text" class="form-control form-control-sm" id="product8"
                                   value="{{ $shop->product8 }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="product9">Product 9</label>
                            <input name="product9" type="text" class="form-control form-control-sm" id="product9"
                                   value="{{ $shop->product9 }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="product10">Product 10</label>
                            <input name="product10" type="text" class="form-control form-control-sm" id="product10"
                                   value="{{ $shop->product10 }}">
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save Product Details</button>
                </div>
            </form>
        </div>

        <div class="card shadow mb-4" id="brand_details">
            <form method="post" action="/admin/shops/post-shop-brands">
                @csrf
                <input type="hidden" name="shopId" value="{{ $shop->shopMGID }}" />
                <h5 class="card-header">Shop Brands</h5>
                @if ($message = Session::get('brand_success'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Success:</strong> {{ $message }}
                    </div>
                    {{ Session::forget('brand_success') }}
                @endif
                <div class="card-body">
                    <div class="controls">
                        <?php

                        if(isset($shop->brands))
                        {
                            $shop_brands = $shop->brands->pluck('id')->toArray();
                        }
                        $count = 1;
                        ?>
                        @foreach($brands as $key => $value)
                            <span for="{{ 'brand_'.$key }}">
                                <input type="checkbox" name="brands[]" value="{{ $key }}" @if(isset($shop_brands) && in_array($key, $shop_brands)) checked @endif>
                                 {{ $value }}
                            </span> &nbsp;
                            @if(($count %12.5) == 0 && $count > 1)
                                <div class="clearfix">&nbsp;</div>
                            @endif
                            <?php $count++; ?>
                        @endforeach

                    </div>
                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save Shop Brands</button>
                </div>
            </form>
        </div>

        <div class="card shadow mb-4" id="contact_details">
            <form method="post" action="/admin/shops/post-contact-details">
                @csrf
                <input type="hidden" name="shopId" value="{{ $shop->shopMGID }}" />
                <h5 class="card-header">Contact Details</h5>

                @if ($message = Session::get('success_contact'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Success:</strong> {{ $message }}
                    </div>
                    {{ Session::forget('success_contact') }}
                @endif

                @if ($message = Session::get('error_contact'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Error:</strong> {{ $message }}
                    </div>
                    {{ Session::forget('error_contact') }}
                @endif

                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="shopNumber">Shop Number</label>
                            <input name="shopNumber"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="shopNumber"
                                   value="{{ old('shopNumber', $shop->shopNumber) }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="telephone">Telephone</label>
                            <input name="telephone"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="telephone"
                                   value="{{ old('telephone', $shop->telephone) }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="cell">Cell</label>
                            <input name="cell" type="text"
                                   class="form-control form-control-sm numeric"
                                   id="cell"
                                   value="{{ old('cell', $shop->cell) }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="email">Email Address</label>
                            <input name="email"
                                   type="email"
                                   class="form-control form-control-sm"
                                   id="email"
                                   value="{{ old('email', $shop->email) }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('email') ? $errors->first('email') : '') }}
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="googleMapX">Google Map Co-ords(latitude)</label>
                            <input name="googleMapX"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="googleMapX"
                                   value="{{ old('googleMapX', $shop->googleMapX) }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="googleMapY">Google Map Co-ords(longitude)</label>
                            <input name="googleMapY"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="googleMapY"
                                   value="{{ old('googleMapY', $shop->googleMapY) }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="fax">Fax</label>
                            <input name="fax"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="fax"
                                   value="{{ old('fax', $shop->fax) }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="contactPerson">Contact Person</label>
                            <input name="contactPerson"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="contactPerson"
                                   value="{{ old('contactPerson', $shop->contactPerson) }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="url">Web Address <small>Must contain http: or https:</small></label>
                            <input name="url"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="url"
                                   value="{{ old('url', $shop->url) }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('url') ? $errors->first('url') : '') }}
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="facebookURL">Facebook Page</label>
                            <input name="facebookURL"
                                   type="text"
                                   class="form-control form-control-sm url"
                                   id="facebookURL"
                                   value="{{ old('facebookURL', $shop->facebookURL) }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="landmark">Landmark</label>
                            <input name="landmark"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="landmark"
                                   value="{{ old('landmark', $shop->landmark) }}">
                        </div>

                    </div>
                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save Contact Details</button>
                </div>
            </form>
        </div>

        <div class="card shadow mb-4" id="card_details">
            <form method="post" action="/admin/shops/post-card-details">
                @csrf
                <input type="hidden" name="shopId" value="{{ $shop->shopMGID }}" />
                <h5 class="card-header">Shop Cards</h5>

                @if ($message = Session::get('success_card'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Success:</strong> {{ $message }}
                    </div>
                    {{ Session::forget('success_card') }}
                @endif

                <div class="card-body">
                    <div class="form-row">
                        <div class="control-group">
                            <label><strong>Cards accepted:</strong></label>
                            <div class="controls">
                                <input type="checkbox" name="visa" value="Y" @if($shop->visa === 'Y') checked @endif>Visa &nbsp;
                                <input type="checkbox" name="mastercard" value="Y" @if($shop->mastercard === 'Y') checked @endif>Mastercard &nbsp;
                                <input type="checkbox" name="amex" value="Y" @if($shop->amex === 'Y') checked @endif>American Express &nbsp;
                                <input type="checkbox" name="diners" value="Y" @if($shop->diners === 'Y') checked @endif>Diners Club &nbsp;
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="control-group">
                            <label for="otherCard"><strong>Additional cards:</strong></label>
                            <div class="controls">
                                @foreach($otherCards as $other)
                                    <input type="checkbox" name="otherCard[]" value="{{ $other->cardID }}" @if(in_array($other->cardID, $otherCardsSelected)) checked @endif>{{$other->title}} &nbsp;&nbsp;
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save Card Details</button>
                </div>
            </form>
        </div>

        <div class="card shadow mb-4" id="map_details">
            <form method="post" action="/admin/shops/post-map-details">
                @csrf
                <input type="hidden" name="shopId" value="{{ $shop->shopMGID }}" />
                <h5 class="card-header">Mall Site Map</h5>
                @if ($message = Session::get('success_map'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Success:</strong> {{ $message }}
                    </div>
                    {{ Session::forget('success_map') }}
                @endif
                <div class="card-body">
                    <div class="form-row">

                        <div class="form-group col-md-4">
                            <label for="shopNumber">Select a map (click on the name to view the image and place the shop marker)</label> <br/>
                            <br />
                            @if(count($mapImage) > 0)
                                @foreach($mapImage as $map)
                                    <?php
                                    $checked = '';
                                    if(isset($shop))
                                    {
                                        if($shop->map == $map->location){
                                            $checked = 'checked';
                                        }
                                    }
                                    ?>
                                        <label class="radio-inline">
                                            <input type="radio" name="map" class="show_img" id="{{ $map->mapImageMGID }}" value="{{ $map->location }}" {{ $checked }}>
                                            {{ $map->location }}
                                        </label> &nbsp;&nbsp;
                                @endforeach
                            @endif

                                <input type="hidden" name="mapX" id="mapX" />
                                <input type="hidden" name="mapY" id="mapY" />

                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save Map Details</button>
                </div>
            </form>
        </div>

        <div class="card shadow mb-4" id="image_info">
            <form method="post" action="/admin/shops/post-image-details" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="shopId" value="{{ $shop->shopMGID }}" />
                <h5 class="card-header">Shop Images <strong style="color: red;">(Images that are 2MB or more will NOT uploaded)</strong></h5>

                @if ($message = Session::get('success_imageupload'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Success:</strong> {{ $message }}
                    </div>
                    {{ Session::forget('success_imageupload') }}
                @endif

                @if ($message = Session::get('error_imageupload'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Error:</strong> {{ $message }}
                    </div>
                    {{ Session::forget('error_imageupload') }}
                @endif

                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="logo">Logo</label>
                            @if ($shop->logo !== null)
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $shop->mallID . '/' . $shop->logo }}"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="logo" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('logo') ? $errors->first('logo') : '') }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="image1">Store Front Image</label>
                            @if ($shop->image1 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $shop->mallID . '/' . $shop->image1 }}"/>
                                <a class="btn btn-danger btn-mini action_remove" href="#" id="image1" data-id="imageBig1"><i class="fa fa-trash"></i></a>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image1" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image1') ? $errors->first('image1') : '') }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="image2">Image 2</label>
                            @if ($shop->image2 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $shop->mallID . '/' . $shop->image2 }}"/>
                                <a class="btn btn-danger btn-mini action_remove" href="#" id="image2" data-id="imageBig2"><i class="fa fa-trash"></i></a>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image2" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image2') ? $errors->first('image2') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="image3">Image 3</label>
                            @if ($shop->image3 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $shop->mallID . '/' . $shop->image3 }}"/>
                                <a class="btn btn-danger btn-mini action_remove" href="#" id="image3" data-id="imageBig3"><i class="fa fa-trash"></i></a>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image3" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image3') ? $errors->first('image3') : '') }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="image4">Image 4</label>
                            @if ($shop->image4 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $shop->mallID . '/' . $shop->image4 }}"/>
                                <a class="btn btn-danger btn-mini action_remove" href="#" id="image4" data-id="imageBig4"><i class="fa fa-trash"></i></a>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image4" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image4') ? $errors->first('image4') : '') }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="image5">Image 5</label>
                            @if ($shop->image5 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $shop->mallID . '/' . $shop->image5 }}"/>
                                <a class="btn btn-danger btn-mini action_remove" href="#" id="image5" data-id="imageBig5"><i class="fa fa-trash"></i></a>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image5" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image5') ? $errors->first('image5') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="image6">Image 6</label>
                            @if ($shop->image6 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $shop->mallID . '/' . $shop->image6 }}"/>
                                <a class="btn btn-danger btn-mini action_remove" href="#" id="image6" data-id="imageBig6"><i class="fa fa-trash"></i></a>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image6" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image6') ? $errors->first('image6') : '') }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="image7">Image 7</label>
                            @if ($shop->image7 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $shop->mallID . '/' . $shop->image7 }}"/>
                                <a class="btn btn-danger btn-mini action_remove" href="#" id="image7" data-id="imageBig7"><i class="fa fa-trash"></i></a>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image7" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image7') ? $errors->first('image7') : '') }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="image8">Image 8</label>
                            @if ($shop->image8 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $shop->mallID . '/' . $shop->image8 }}"/>
                                <a class="btn btn-danger btn-mini action_remove" href="#" id="image8" data-id="imageBig8"><i class="fa fa-trash"></i></a>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image8" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image8') ? $errors->first('image8') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="image9">Image 9</label>
                            @if ($shop->image9 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $shop->mallID . '/' . $shop->image9 }}"/>
                                <a class="btn btn-danger btn-mini action_remove" href="#" id="image9" data-id="imageBig9"><i class="fa fa-trash"></i></a>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image9" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image9') ? $errors->first('image9') : '') }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="image10">Image 10</label>
                            @if ($shop->image10 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $shop->mallID . '/' . $shop->image10 }}"/>
                                <a class="btn btn-danger btn-mini action_remove" href="#" id="image10" data-id="imageBig10"><i class="fa fa-trash"></i></a>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image10" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image10') ? $errors->first('image10') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save Shop Images Details</button>
                </div>
            </form>
        </div>

        <div class="card shadow mb-4" id="owner_info">
            <form method="post" action="/admin/shops/post-owner-details" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="shopId" value="{{ $shop->shopMGID }}" />
                <h5 class="card-header">Owner and Manager Details</h5>

                @if ($message = Session::get('success_owner'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Success:</strong> {{ $message }}
                    </div>
                    {{ Session::forget('success_owner') }}
                @endif

                @if ($message = Session::get('error_owner'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Error:</strong> {{ $message }}
                    </div>
                    {{ Session::forget('error_owner') }}
                @endif

                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="ownerName">Owner's first name</label>
                            <input name="ownerName"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="ownerName"
                                   value="{{ old('ownerName', $shop->ownerName) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="ownerSurname">Owner's surname</label>
                            <input name="ownerSurname"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="ownerSurname"
                                   value="{{ old('ownerSurname', $shop->ownerSurname) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="ownerCell">Owner's Cell</label>
                            <input name="ownerCell"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="ownerCell"
                                   value="{{ old('ownerCell', $shop->ownerCell) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="ownerEmail">Owner's Email</label>
                            <input name="ownerEmail"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="ownerEmail"
                                   value="{{ old('ownerEmail', $shop->ownerEmail) }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('ownerEmail') ? $errors->first('ownerEmail') : '') }}
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="managerName">Manager 1: first name</label>
                            <input name="managerName"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="managerName"
                                   value="{{ old('managerName', $shop->managerName) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="managerSurname">Manager 1:surname</label>
                            <input name="managerSurname"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="managerSurname"
                                   value="{{ old('managerSurname', $shop->managerSurname) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="managerCell">Manager 1: Cell</label>
                            <input name="managerCell"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="managerCell"
                                   value="{{ old('managerCell', $shop->managerCell) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="managerEmail">Manager 1: Email</label>
                            <input name="managerEmail"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="managerEmail"
                                   value="{{ old('managerEmail', $shop->managerEmail) }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('managerEmail') ? $errors->first('managerEmail') : '') }}
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="managerName2">Manager 2: first name</label>
                            <input name="managerName2"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="managerName2"
                                   value="{{ old('managerName2', $shop->managerName2) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="managerSurname2">Manager 2:surname</label>
                            <input name="managerSurname2"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="managerSurname2"
                                   value="{{ old('managerSurname2', $shop->managerSurname2) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="managerCell2">Manager 2: Cell</label>
                            <input name="managerCell2"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="managerCell2"
                                   value="{{ old('managerCell2', $shop->managerCell2) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="managerEmail2">Manager 2: Email</label>
                            <input name="managerEmail2"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="managerEmail2"
                                   value="{{ old('managerEmail2', $shop->managerEmail2) }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('managerEmail2') ? $errors->first('managerEmail2') : '') }}
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="managerName2">Manager 3: first name</label>
                            <input name="managerName2"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="managerName2"
                                   value="{{ old('managerName2', $shop->managerName2) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="managerSurname3">Manager 3:surname</label>
                            <input name="managerSurname3"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="managerSurname3"
                                   value="{{ old('managerSurname3', $shop->managerSurname3) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="managerCell3">Manager 3: Cell</label>
                            <input name="managerCell3"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="managerCell3"
                                   value="{{ old('managerCell3', $shop->managerCell3) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="managerEmail3">Manager 3: Email</label>
                            <input name="managerEmail3"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="managerEmail3"
                                   value="{{ old('managerEmail3', $shop->managerEmail3) }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('managerEmail3') ? $errors->first('managerEmail3') : '') }}
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="headOfficeName">Head Office: first name</label>
                            <input name="headOfficeName"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="headOfficeName"
                                   value="{{ old('headOfficeName', $shop->headOfficeName) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="headOfficeSurname">Head Office:surname</label>
                            <input name="headOfficeSurname"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="headOfficeSurname"
                                   value="{{ old('headOfficeSurname', $shop->headOfficeSurname) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="headOfficeCell">Head Office: Cell</label>
                            <input name="headOfficeCell"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="headOfficeCell"
                                   value="{{ old('headOfficeCell', $shop->headOfficeCell) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="headOfficeEmail">Head Office: Email</label>
                            <input name="headOfficeEmail"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="headOfficeEmail"
                                   value="{{ old('headOfficeEmail', $shop->headOfficeEmail) }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('headOfficeEmail') ? $errors->first('headOfficeEmail') : '') }}
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="financialName">Financial Manager: first name</label>
                            <input name="financialName"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="financialName"
                                   value="{{ old('financialName', $shop->financialName) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="financialSurname">Financial Manager:surname</label>
                            <input name="financialSurname"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="financialSurname"
                                   value="{{ old('financialSurname', $shop->financialSurname) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="financialCell">Financial Manager: Cell</label>
                            <input name="financialCell"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="financialCell"
                                   value="{{ old('financialCell', $shop->financialCell) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="financialEmail">Financial Manager: Email</label>
                            <input name="financialEmail"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="financialEmail"
                                   value="{{ old('financialEmail', $shop->financialEmail) }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('financialEmail') ? $errors->first('financialEmail') : '') }}
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="areaManagerName">Area Manager: first name</label>
                            <input name="areaManagerName"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="areaManagerName"
                                   value="{{ old('areaManagerName', $shop->areaManagerName) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="areaManagerSurname">Area Manager:surname</label>
                            <input name="areaManagerSurname"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="areaManagerSurname"
                                   value="{{ old('areaManagerSurname', $shop->areaManagerSurname) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="areaManagerCell">Area Manager: Cell</label>
                            <input name="areaManagerCell"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="areaManagerCell"
                                   value="{{ old('areaManagerCell', $shop->areaManagerCell) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="areaManagerEmail">Area Manager: Email</label>
                            <input name="areaManagerEmail"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="areaManagerEmail"
                                   value="{{ old('areaManagerEmail', $shop->areaManagerEmail) }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('areaManagerEmail') ? $errors->first('areaManagerEmail') : '') }}
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="opsManagerName">Ops Manager:surname</label>
                            <input name="opsManagerName"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="opsManagerName"
                                   value="{{ old('opsManagerName', $shop->opsManagerName) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="opsManagerSurname">Ops Manager:surname</label>
                            <input name="opsManagerSurname"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="opsManagerSurname"
                                   value="{{ old('opsManagerSurname', $shop->opsManagerSurname) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="opsManagerCell">Ops Manager: Cell</label>
                            <input name="opsManagerCell"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="opsManagerCell"
                                   value="{{ old('opsManagerCell', $shop->opsManagerCell) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="opsManagerEmail">Ops Manager: Email</label>
                            <input name="opsManagerEmail"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="opsManagerEmail"
                                   value="{{ old('opsManagerEmail', $shop->opsManagerEmail) }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('opsManagerEmail') ? $errors->first('opsManagerEmail') : '') }}
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="emergencyName">Emergency Contact: first name</label>
                            <input name="emergencyName"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="emergencyName"
                                   value="{{ old('emergencyName', $shop->emergencyName) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="emergencySurname">Emergency contact:surname</label>
                            <input name="emergencySurname"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="emergencySurname"
                                   value="{{ old('emergencySurname', $shop->emergencySurname) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="emergencyCell">Emergency Contact: Cell</label>
                            <input name="emergencyCell"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="emergencyCell"
                                   value="{{ old('emergencyCell', $shop->emergencyCell) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="emergencyEmail">Emergency Contact: Email</label>
                            <input name="emergencyEmail"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="emergencyEmail"
                                   value="{{ old('emergencyEmail', $shop->emergencyEmail) }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('emergencyEmail') ? $errors->first('emergencyEmail') : '') }}
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label for="emergencyName2">Emergency Contact (2): first name</label>
                            <input name="emergencyName2"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="emergencyName2"
                                   value="{{ old('emergencyName2', $shop->emergencyName2) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="emergencySurname2">Emergency contact (2):surname</label>
                            <input name="emergencySurname2"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="emergencySurname2"
                                   value="{{ old('emergencySurname2', $shop->emergencySurname2) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="emergencyCell2">Emergency Contact (2): Cell</label>
                            <input name="emergencyCell2"
                                   type="text"
                                   class="form-control form-control-sm numeric"
                                   id="emergencyCell2"
                                   value="{{ old('emergencyCell2', $shop->emergencyCell2) }}">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="emergencyEmail2">Emergency Contact (2): Email</label>
                            <input name="emergencyEmail2"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="emergencyEmail2"
                                   value="{{ old('emergencyEmail2', $shop->emergencyEmail2) }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('emergencyEmail2') ? $errors->first('emergencyEmail2') : '') }}
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save Owner & Manager Details</button>
                </div>
            </form>
        </div>

        <div class="card shadow mb-4" id="res_info">
            <form method="post" action="/admin/shops/post-reservation-details">
                @csrf
                <input type="hidden" name="shopId" value="{{ $shop->shopMGID }}" />
                <h5 class="card-header">Reservation Details</h5>

                @if ($message = Session::get('success_res'))
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Success:</strong> {{ $message }}
                    </div>
                    {{ Session::forget('success_res') }}
                @endif

                @if ($message = Session::get('error_res'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <strong>Error:</strong> {{ $message }}
                    </div>
                    {{ Session::forget('error_res') }}
                @endif

                <div class="card-body">
                    <div class="control-group">
                        <label for="reserveWith"><strong>If reservation notifications are sent who must receive them?</strong></label>
                        <div class="controls">
                            <?php
                            $checked = '';
                            if(isset($shop))
                            {
                                $selectedArray = explode(',',$shop->reserveWith);
                            }
                            ?>
                            <input type="checkbox" name="reserveWith[]" value="1"  @if(in_array(1, $selectedArray)) checked @endif />Owner &nbsp;
                            <input type="checkbox" name="reserveWith[]" value="2"  @if(in_array(2, $selectedArray)) checked @endif />Manager 1 &nbsp;
                            <input type="checkbox" name="reserveWith[]" value="3"  @if(in_array(3, $selectedArray)) checked @endif />Manager 2 &nbsp;
                            <input type="checkbox" name="reserveWith[]" value="4"  @if(in_array(4, $selectedArray)) checked @endif/>Manager 3 &nbsp;
                            <input type="checkbox" name="reserveWith[]" value="5"  @if(in_array(5, $selectedArray)) checked @endif/>Head Office &nbsp;
                            <input type="checkbox" name="reserveWith[]" value="6"  @if(in_array(6, $selectedArray)) checked @endif />Financial Manager &nbsp;
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save Reservation Details</button>
                </div>
            </form>
        </div>


        <div class="card shadow mb-4" id="shop_downloads">
            @csrf
            <input type="hidden" name="shopId" value="{{ $shop->shopMGID }}" />
            <h5 class="card-header">Shop Downloads</h5>

            @if ($message = Session::get('success_downloads'))
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Success:</strong> {{ $message }}
                </div>
                {{ Session::forget('success_downloads') }}
            @endif

            @if ($message = Session::get('error_downloads'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>Error:</strong> {{ $message }}
                </div>
                {{ Session::forget('error_downloads') }}
            @endif

            <div class="card-body">
                <div class="control-group">
                    <label for="reserveWith" style="color: #3a87ad;">Please Note: Shop Downloads are uploaded and saved independently of the details above.</label>
                    <div class="row">
                        <table class="table">
                            <thead>
                            <tr>
                                <td width="50">Type</td>
                                <td>Name</td>
                                <td width="100">Size</td>
                                <td width="100">&nbsp;</td>
                            </tr>
                            </thead>
                            <tbody id="downloadsDiv">
                            @foreach($shop->downloads as $download)
                                <tr>
                                    <td width="50" class="text-center"><img src="{{ asset('img/docs/'.$download->filetype.'.png') }}"/></td>
                                    <td><a href="/download/mall_{{ $shop->mallID }}/{{ $download->filename }}" target="_blank"> {{ $download->name }}</a></td>
                                    <td width="100">{{ $download->filesize }}kb</td>
                                    <td width="100">
                                        <button id="{{ $download->id }}" class="btn btn-sm btn-danger upload_delete" type="button"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                        </table>
                    </div>
                    <div class="row mt-5 ml-5">
                        <form id="uploadForm" name="uploadForm" action="/admin/shops/file-upload" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="shopMGID" value="{{ $shop->shopMGID }}" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="upload_name">Name (To appear on link. eg. Menu)</label>
                                        <input name="upload_name" type="text" class="form-control form-control-sm" id="upload_name"
                                               value="" >
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="fileUpload"><strong>File</strong></label>
                                    <div class="file-field">
                                        <div class="btn btn-light">
                                            <input type="file" name="fileUpload" accept="image/png, image/jpeg, application/pdf"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row-fluid ml-5">
                                    <div class="span12">
                                        <button type="submit" class="btn btn-primary float-right">Upload File</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>




    </div>
@endsection

@section('javascript')
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
    <script src="{{ asset('js/tinymce/js/tinymce/tinymce.js') }}"></script>
    <script>
        // Register plugin with a short name
        $(document).ready(function () {
            tinymce.init({
                selector: '.wysi-text-area',
                plugins: "image link -mailto ",
                image_advtab: true,
                menubar: false,
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: ''
            });

            $(".numeric").mask('0b99999999');

            $(".url").formatUrl({
                protocol:'http://',
                alwaysShow:false,
                minimumInputLength:3
            });

            $(".show_img").click(function(){
                $(this).prop('checked',true);
                var imgSrc = $(this).attr("id");
                var openWin = PopupCenter('/admin/maps/editimage/'+imgSrc, 'test',600,600);
            });

            if($("#category_id").val() == ''){
                $('#subcategory').empty();
            }else{
                $("#category_text").val($("#category_id").children("option").filter(":selected").text());
            }

            $("#category_id").change(function () {
                refreshSubcategoryList();
                $("#category_text").val($(this).children("option").filter(":selected").text());
            })

            $(".action_remove").click(function(e){

                var image = $(this).attr('id');
                var imageBig = $(this).attr('data-id');
                var route = window.location.href;
                var resultArr = route.split('/');
                var shop = <?php echo  $shop->shopMGID; ?>

                e.preventDefault();

                if (confirm("Are you sure you want to remove this image ? ")) {
                    $.ajax({
                        type: "GET",
                        url: '/admin/shops/remove-image/'+shop+'/'+image+'/'+imageBig,
                        data: '',
                        success: function(){
                            location.reload();
                        }
                    });
                } else  {
                    return false;
                }
            });

            $(".upload_delete").click(function () {
                let id = $(this).attr('id');
                if (confirm("Are you sure you want to remove this upload ? ")) {
                    $.ajax({
                        type: "GET",
                        url: '/admin/shops/delete-file/'+id,
                        data: '',
                        success: function(){
                            location.reload();
                        }
                    });
                } else  {
                    return false;
                }
            })
        })

        function PopupCenter(pageURL, title,w,h) {
            var left = (screen.width/2)-(w/2);
            var top = (screen.height/2)-(h/2);
            var targetWin = window.open (pageURL, title, 'width='+w+', height='+h+', top='+top+', left='+left);
        }

        function refreshSubcategoryList()
        {
            var category = $('#category_id').val();

            $.ajax({
                type: 'GET',
                url: '/shops/subcategory',
                data:{category : category},
                success: function (data) {

                    $('#subcategory').empty().append('<option value="">Please Select...</option>');

                    $.each(data, function(key, value){
                        $('#subcategory').append('<option value="'+key+'">'+value+'</option>');
                    })
                }
            });
        }

    </script>
@endsection
