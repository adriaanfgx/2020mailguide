@extends('layouts.admin')

@section('content')

    <div class="container" style="padding-bottom: 150px">
        @include('notifications')

        <div class="card shadow mb-4">

            <form method="post" action="/admin/shops/update-generic/{{ $generic->shopGenericID }}">
                @csrf
                <h5 class="card-header">Shop Details</h5>
                <div id="add_shop" class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="name">Shop Name *</label>
                            <input name="name" type="text" class="form-control form-control-sm" id="name"
                                   value="{{ $generic->name }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('name') ? $errors->first('name') : '') }}
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="category_id">Category</label>
                            <select name="category" id="category" class="form-control form-control-sm">
                                <option value="">Please Select...</option>
                                @foreach($categories as $key => $value)
                                    <option value="{{ $key }}" @if($key == $generic->category) selected @endif>{{$value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="url">Web Address</label>
                            <input name="url" type="text" class="form-control form-control-sm" id="url"
                                   value="{{ $generic->url }}">
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="tradingHours">Trading Hours</label>
                            <textarea class="wysi-text-area" name="tradingHours"
                                      rows="7">{{ $generic->tradingHours }}</textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="description">Description</label>
                            <textarea class="wysi-text-area" name="description"
                                      rows="7">{{ $generic->description }}</textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <label for="keywords">Keywords</label>
                            <input name="keywords" type="text" class="form-control form-control-sm" id="keywords"
                                   value="{{ $generic->keywords }}">
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save details</button>
                </div>
            </form>
        </div>

        <div class="card shadow mb-4">
            <form method="post" action="/admin/shops/update-generic-products">
                @csrf
                <input type="hidden" name="shopGenericID" value="{{ $generic->shopGenericID }}" />
                <h5 class="card-header">Products</h5>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="product1">Product 1</label>
                            <input name="product1" type="text" class="form-control form-control-sm" id="product1"
                                   value="{{ $generic->product1 }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="product2">Product 2</label>
                            <input name="product2" type="text" class="form-control form-control-sm" id="product2"
                                   value="{{ $generic->product2 }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="product3">Product 3</label>
                            <input name="product3" type="text" class="form-control form-control-sm" id="product3"
                                   value="{{ $generic->product3 }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="product4">Product 4</label>
                            <input name="product4" type="text" class="form-control form-control-sm" id="product4"
                                   value="{{ $generic->product4 }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="product5">Product 5</label>
                            <input name="product5" type="text" class="form-control form-control-sm" id="product5"
                                   value="{{ $generic->product5 }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="product6">Product 6</label>
                            <input name="product6" type="text" class="form-control form-control-sm" id="product6"
                                   value="{{ $generic->product6 }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="product7">Product 7</label>
                            <input name="product7" type="text" class="form-control form-control-sm" id="product7"
                                   value="{{ $generic->product7 }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="product8">Product 8</label>
                            <input name="product8" type="text" class="form-control form-control-sm" id="product8"
                                   value="{{ $generic->product8 }}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="product9">Product 9</label>
                            <input name="product9" type="text" class="form-control form-control-sm" id="product9"
                                   value="{{ $generic->product9 }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="product10">Product 10</label>
                            <input name="product10" type="text" class="form-control form-control-sm" id="product10"
                                   value="{{ $generic->product10 }}">
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save Product Details</button>
                </div>
            </form>
        </div>

        <div class="card shadow mb-4">
            <form method="post" action="/admin/shops/update-generic-images" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="shopGenericID" value="{{ $generic->shopGenericID }}" />
                <h5 class="card-header">Shop Images</h5>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="logo">Logo</label>
                            @if ($generic->logo !== null)
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/generic/' . $generic->logo }}"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="logo" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('logo') ? $errors->first('logo') : '') }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="image1">Store Front Image</label>
                            @if ($generic->image1 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/generic/' . $generic->image1 }}"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image1" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image1') ? $errors->first('image1') : '') }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="image2">Image 2</label>
                            @if ($generic->image2 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/generic/' . $generic->image2 }}"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image2" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image2') ? $errors->first('image2') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="image3">Image 3</label>
                            @if ($generic->image3 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/generic/' . $generic->image3 }}"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image3" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image3') ? $errors->first('image3') : '') }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="image4">Image 4</label>
                            @if ($generic->image4 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/generic/' . $generic->image4 }}"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image4" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image4') ? $errors->first('image4') : '') }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="image5">Image 5</label>
                            @if ($generic->image5 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/generic/' . $generic->image5 }}"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image5" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image5') ? $errors->first('image5') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="image6">Image 6</label>
                            @if ($generic->image6 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/generic/' . $generic->image6 }}"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image6" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image6') ? $errors->first('image6') : '') }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="image7">Image 7</label>
                            @if ($generic->image7 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/generic/' . $generic->image7 }}"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image7" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image7') ? $errors->first('image7') : '') }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="image8">Image 8</label>
                            @if ($generic->image8 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/generic/' . $generic->image8 }}"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image8" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image8') ? $errors->first('image8') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="image9">Image 9</label>
                            @if ($generic->image9 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/generic/' . $generic->image9 }}"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image9" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image9') ? $errors->first('image9') : '') }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="image10">Image 10</label>
                            @if ($generic->image10 !== '')
                                <br/>
                                <img
                                    src="{{ env('MALLGUIDE_URL').'/uploadimages/generic/' . $generic->image10 }}"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="image10" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('image10') ? $errors->first('image10') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save Shop Images Details</button>
                </div>
            </form>
        </div>

        <div class="card shadow mb-4">
            <form method="post" action="/admin/shops/post-generic-card-details">
                @csrf
                <input type="hidden" name="shopGenericID" value="{{ $generic->shopGenericID }}" />
                <h5 class="card-header">Shop Cards</h5>
                <div class="card-body">
                    <div class="form-row">
                        <div class="control-group">
                            <label><strong>Cards accepted:</strong></label>
                            <div class="controls">
                                <input type="checkbox" name="visa" value="Y" @if($generic->visa === 'Y') checked @endif>Visa &nbsp;
                                <input type="checkbox" name="mastercard" value="Y" @if($generic->mastercard === 'Y') checked @endif>Mastercard &nbsp;
                                <input type="checkbox" name="amex" value="Y" @if($generic->amex === 'Y') checked @endif>American Express &nbsp;
                                <input type="checkbox" name="diners" value="Y" @if($generic->diners === 'Y') checked @endif>Diners Club &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save Card Details</button>
                </div>
            </form>
        </div>

    </div>
@endsection

@section('javascript')
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
    <script src="{{ asset('js/tinymce/js/tinymce/tinymce.js') }}"></script>
    <script>
        // Register plugin with a short name
        $(document).ready(function () {
            tinymce.init({
                selector: '.wysi-text-area',
                plugins: "image link -mailto ",
                image_advtab: true,
                menubar: false,
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: ''
            });

            $(".url").formatUrl({
                protocol:'http://',
                alwaysShow:false,
                minimumInputLength:3
            });

            $(".show_img").click(function(){
                $(this).prop('checked',true);
                var imgSrc = $(this).attr("id");
                var openWin = PopupCenter('/admin/maps/editimage/'+imgSrc, 'test',600,600);
            });
        })

        function PopupCenter(pageURL, title,w,h) {
            var left = (screen.width/2)-(w/2);
            var top = (screen.height/2)-(h/2);
            var targetWin = window.open (pageURL, title, 'width='+w+', height='+h+', top='+top+', left='+left);
        }

    </script>
@endsection
