@extends('layouts.admin')

@section('content')

    <div class="container" style="padding-bottom: 150px">
        @include('notifications')

        <div class="card shadow mb-4">
            <form method="post" action="/admin/shops/post-create">
                @csrf
                <input type="hidden" name="mallID" value="{{ $mall->mallID }}" />
                <h5 class="card-header">{{ $mall->name }} >> Add Shop</h5>

                <div id="add_shop" class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="name">Shop Name *</label>
                            <input name="name" type="text" class="form-control form-control-sm" id="name"
                                   value="{{ old('name') }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('name') ? $errors->first('name') : '') }}
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="category_id">Category</label>
                            <select name="category_id" id="category_id" class="form-control form-control-sm">
                                <option value="">Please Select...</option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" @if (old('category_id') == $category->id) selected @endif>{{$category->name }}</option>
                                @endforeach
                            </select>

                            <input type="hidden" name="category"  id="category_text"/>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="subcategory">Subcategory</label>
                            <select name="subcategory_id" id="subcategory" class="form-control form-control-sm">
                                <option value="">Please Select...</option>
                                @foreach($subcategories as $key => $value)
                                    <option value="{{ $key }}">{{$value }}</option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="tradingHours">Trading Hours</label>
                            <textarea class="wysi-text-area" name="tradingHours"
                                      rows="7">{{ old('tradingHours') }}</textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="description">Description</label>
                            <textarea class="wysi-text-area" name="description"
                                      rows="7">{{ old('description') }}</textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="keywords">Keywords</label>
                            <textarea class="wysi-text-area" name="keywords"
                                      rows="7">{{ old('keywords') }}</textarea>
                        </div>
                    </div>


                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save details</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('javascript')
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    <script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
    <script src="{{ asset('js/tinymce/js/tinymce/tinymce.js') }}"></script>
    <script>
        // Register plugin with a short name
        $(document).ready(function () {
            tinymce.init({
                selector: '.wysi-text-area',
                plugins: "image link -mailto ",
                image_advtab: true,
                menubar: false,
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: ''
            });

            if($("#category_id").val() == ''){
                $('#subcategory').empty();
            }

            $("#category_id").change(function () {
                refreshSubcategoryList();
                $("#category_text").val($(this).children("option").filter(":selected").text());
            })
        })

        function refreshSubcategoryList()
        {
            var category = $('#category_id').val();

            $.ajax({
                type: 'GET',
                url: '/shops/subcategory',
                data:{category : category},
                success: function (data) {

                    $('#subcategory').empty().append('<option value="">Please Select...</option>');

                    $.each(data, function(key, value){
                        $('#subcategory').append('<option value="'+key+'">'+value+'</option>');
                    })
                }
            });
        }

    </script>
@endsection
