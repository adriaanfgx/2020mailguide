@extends('layouts.admin')

@section('content')

<div class="container" style="padding-bottom: 150px">
    @include('notifications')

    <div class="card shadow mb-4">
        <form method="post" action="/admin/malls/add">
            @csrf
            <h5 class="card-header">Add Mall</h5>

            <div id="add_mall" class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="name">Mall Name *</label>
                        <input name="name" type="text" class="form-control form-control-sm" id="name"
                               value="">
                        <div class="invalid-feedback">
                            {{ ($errors->has('name') ? $errors->first('name') : '') }}
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="country_id">Country</label>
                        <select name="country_id"
                                id="country"
                                class="form-control form-control-sm"
                                @change="countryChange($event)">
                            <option value="">Please Select...</option>
                            @foreach($countries as $country)
                            <option value="{{ $country->id }}">
                                {{ $country->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="province_id">Province</label>
                        <select name="province_id"
                                id="province"
                                class="form-control form-control-sm"
                                @change="provinceChange($event)">
                            <option value="">Please Select...</option>
                            <option v-for="province in provinces" v-bind:value="province.id">
                                @{{ province.name }}
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputState">City</label>
                        <select name="city_id"
                                id="city_id"
                                class="form-control form-control-sm">
                            <option value="">Please Select...</option>
                            <option v-for="city in cities" v-bind:value="city.id">
                                @{{ city.name }}
                            </option>
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="category_id">Classification List</label>
                        <select name="category_id" id="category_id" class="form-control form-control-sm">
                            <option value="">Please Select...</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{
                                $category->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="physicalAddress">Physical address</label>
                        <textarea name="physicalAddress" class="form-control" id="physicalAddress"
                                  rows="3"></textarea>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="postalAddress">Postal address</label>
                        <textarea name="postalAddress" class="form-control" id="postalAddress" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="glaRetailM2">GLA Retail (m2)</label>
                        <input name="glaRetailM2" type="text" class="form-control form-control-sm" id="glaRetailM2"
                               value="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="website">Website</label>
                        <input name="website" type="text" class="form-control form-control-sm" id="website"
                               value="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="numRetailFloors">Number of retail floors in centre</label>
                        <input name="numRetailFloors" type="text" class="form-control form-control-sm"
                               id="numRetailFloors"
                               value="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="numShops">Number Of Shops</label>
                        <input name="numShops" type="text" class="form-control form-control-sm" id="numShops"
                               value="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="coveredParkingBays">Covered Parking Bays</label>
                        <input name="coveredParkingBays" type="text" class="form-control form-control-sm"
                               id="coveredParkingBays"
                               value="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="openParkingBays">Open Parking Bays</label>
                        <input name="openParkingBays" type="text" class="form-control form-control-sm"
                               id="openParkingBays"
                               value="">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="totalParkingBays">Total Parking Bays</label>
                        <input name="totalParkingBays" type="text" class="form-control form-control-sm"
                               id="totalParkingBays"
                               value="">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="anchorTenants">Anchor Tenants</label>
                        <input name="anchorTenants" type="text" class="form-control form-control-sm" id="anchorTenants"
                               value="">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                * Required fields
                <button type="submit" class="btn btn-primary float-right">Save details</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('js/add_mall.js') }}" type="text/javascript"></script>
@endsection
