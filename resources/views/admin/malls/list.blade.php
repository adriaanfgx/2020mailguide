@extends('layouts.admin')

@section('content')
<div class="container">
    <div id="malls" class="mt-2">
        <div class="mb-4 filters">
            <div class="col-3" style="padding-left: 0px; padding-right: 0px;">
                <vue-bootstrap-typeahead
                    v-model="query"
                    :serializer="mall => mall.name"
                    placeholder="Type a mall..."
                    @hit="mallSelected"
                    :data="autocomplete"
                />
            </div>
            <div class="col-3">
                <div class="select">
                    <select class="" v-model="province" @change="provinceChange">
                        <option value=""> Select a province</option>
                        <option v-for="option in provinces" v-bind:value="option.value">
                            @{{ option.text }}
                        </option>
                    </select>
                </div>
            </div>
            <div class="col-6" style="padding-left: 0px; padding-right: 0px;">
                <a href="{{ route('mall.add.get') }}" class="btn btn-primary float-right">Add a Mall</a>
            </div>
        </div>

        <div class="shadow rounded" style="background-color: #fff; padding-top: 8px; padding-bottom: 1px;">
            <b-col class="my-2 col-3 col float-right">
                <b-input-group size="sm">
                    <b-form-input v-model="filter" placeholder="Filter malls..."></b-form-input>
                    <b-input-group-append>
                        <b-button :disabled="!filter" @click="filter = ''">Clear</b-button>
                    </b-input-group-append>
                </b-input-group>
            </b-col>

            <b-table
                show-empty
                striped
                hover
                :items="malls"
                :fields="fields"
                :filter="filter"
                :current-page="currentPage"
                :per-page="perPage"
                :busy="isBusy"
                @filtered="onFiltered">

                <div slot="name" slot-scope="data" v-html="data.value"></div>
                <span slot="centre_manager_telephone" slot-scope="data" v-html="data.value"></span>
                <span slot="website" slot-scope="data" v-html="data.value"></span>
                <span slot="centre_manager_telephone" slot-scope="data" v-html="data.value"></span>
                <span slot="location" slot-scope="data" v-html="data.value"></span>
                <span slot="facebook" slot-scope="data" v-html="data.value"></span>
                <span slot="activate" slot-scope="data" v-html="data.value"></span>
                <span slot="mallguide_display" slot-scope="data" v-html="data.value"></span>
                <span slot="coordinates" slot-scope="data" v-html="data.value"></span>
                <span slot="images" slot-scope="data" v-html="data.value"></span>
                <div slot="table-busy" class="text-center text-danger my-2">
                    <b-spinner class="align-middle"></b-spinner>
                    <strong>Loading...</strong>
                </div>

                <template slot="actions" slot-scope="row">
                    <b-dropdown class="actions-button" variant="link" size="lg" no-caret>
                        <b-button slot="button-content" class="actions-button" variant="light">
                            <svg data-icon="more" viewBox="0 0 16 16"
                                 style="fill: rgb(102, 120, 138); width: 12px; height: 12px;">
                                <path
                                    d="M2 6.03a2 2 0 1 0 0 4 2 2 0 1 0 0-4zM14 6.03a2 2 0 1 0 0 4 2 2 0 1 0 0-4zM8 6.03a2 2 0 1 0 0 4 2 2 0 1 0 0-4z"
                                    fill-rule="evenodd"></path>
                            </svg>
                        </b-button>
                        <b-dropdown-item :href="editUrl(row.item)">Edit</b-dropdown-item>
                        <b-dropdown-item :href="editUrl(row.item)">Delete</b-dropdown-item>
                    </b-dropdown>
                </template>

            </b-table>

            <b-col class="mt-4">
                <b-pagination
                    v-model="currentPage"
                    :total-rows="totalRows"
                    :per-page="perPage"
                ></b-pagination>
            </b-col>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('js/malls.js') }}" type="text/javascript"></script>
@endsection
