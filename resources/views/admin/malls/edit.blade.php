@extends('layouts.admin')

@section('content')

<div id="edit_mall" class="container" style="padding-bottom: 150px">
    @include('notifications')

    <div class="card shadow mb-4">
        <form method="post" action="/admin/malls/edit/{{ $mall->mallID }}/mall-details">
            @csrf
            <h5 class="card-header">Mall Details</h5>

            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="name">Mall Name *</label>
                        <input name="name"
                               type="text" class="form-control form-control-sm" id="name"
                               value="{{ old('name', $mall->name) }}">
                        <div class="invalid-feedback">
                            {{ ($errors->has('name') ? $errors->first('name') : '') }}
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="country_id">Country</label>
                        <select name="country_id"
                                id="country"
                                class="form-control form-control-sm"
                                @change="countryChange($event)">
                            <option value="">Please Select...</option>
                            @foreach($countries as $country)
                            <option value="{{ $country->id }}" @if($country->id == $mall->country_id)
                                selected="selected" @endif>{{ $country->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="province_id">Province</label>
                        <select name="province_id"
                                id="province"
                                class="form-control form-control-sm"
                                @change="provinceChange($event)">
                            <option value="">Please Select...</option>
                            @foreach($provinces as $province)
                            <option value="{{ $province->id }}" @if($mall->province_id == $province->id)
                                selected="selected" @endif>{{ $province->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="city">City</label>
                        <select name="city_id" id="city" class="form-control form-control-sm">
                            <option value="">Please Select...</option>
                            @foreach($cities as $city)
                            <option value="{{ $city->id }}" @if($mall->city_id == $city->id) selected="selected" @endif>
                                {{ $city->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="category_id">Classification List</label>
                        <select name="category_id" id="category_id" class="form-control form-control-sm">
                            <option value="">Please Select...</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}" @if($mall->category_id == $category->id)
                                selected="selected" @endif>{{
                                $category->name }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="physicalAddress">Physical address</label>
                        <textarea name="physicalAddress"
                                  class="form-control"
                                  id="physicalAddress"
                                  rows="3">{{ old('physicalAddress', $mall->physicalAddress) }}</textarea>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="postalAddress">Postal address</label>
                        <textarea name="postalAddress"
                                  class="form-control"
                                  id="postalAddress"
                                  rows="3">{{ old('postalAddress', $mall->postalAddress) }}</textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="glaRetailM2">GLA Retail (m2)</label>
                        <input name="glaRetailM2"
                               type="text"
                               class="form-control form-control-sm"
                               id="glaRetailM2"
                               value="{{ old('glaRetailM2', $mall->glaRetailM2) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="website">Website</label>
                        <input name="website"
                               type="text"
                               class="form-control form-control-sm"
                               id="website"
                               value="{{ old('website', $mall->website) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="numRetailFloors">Number of retail floors in centre</label>
                        <input name="numRetailFloors"
                               type="text"
                               class="form-control form-control-sm"
                               id="numRetailFloors"
                               value="{{ old('numRetailFloors', $mall->numRetailFloors) }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="numShops">Number Of Shops</label>
                        <input name="numShops"
                               type="text"
                               class="form-control form-control-sm"
                               id="numShops"
                               value="{{ old('numShops', $mall->numShops) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="coveredParkingBays">Covered Parking Bays</label>
                        <input name="coveredParkingBays"
                               type="text"
                               class="form-control form-control-sm"
                               id="coveredParkingBays"
                               value="{{ old('coveredParkingBays', $mall->coveredParkingBays) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="openParkingBays">Open Parking Bays</label>
                        <input name="openParkingBays"
                               type="text"
                               class="form-control form-control-sm"
                               id="openParkingBays"
                               value="{{ old('openParkingBays', $mall->openParkingBays) }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="totalParkingBays">Total Parking Bays</label>
                        <input name="totalParkingBays"
                               type="text"
                               class="form-control form-control-sm"
                               id="totalParkingBays"
                               value="{{ old('totalParkingBays', $mall->totalParkingBays) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="anchorTenants">Anchor Tenants</label>
                        <input name="anchorTenants"
                               type="text"
                               class="form-control form-control-sm"
                               id="anchorTenants"
                               value="{{ old('anchorTenants', $mall->anchorTenants) }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="mapX">Latitude</label>
                        <input name="mapX"
                               type="text"
                               class="form-control form-control-sm"
                               id="mapX"
                               value="{{ old('mapX', $mall->mapX) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="mapY">Longitude</label>
                        <input name="mapY"
                               type="text"
                               class="form-control form-control-sm"
                               id="mapY"
                               value="{{ old('mapY', $mall->mapY) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="inputEmail4">Ask Google</label>
                        <a href="/admin/maps/position/{{ $mall->mallID  }}" class="form-control btn btn-light"
                           style="cursor: pointer !important;">Open location
                        </a>
                    </div>
                </div>

            </div>
            <div class="card-footer">
                * Required fields
                <button type="submit" class="btn btn-primary float-right">Save details</button>
            </div>
        </form>
    </div>
    <div class="card shadow mb-4">
        <form method="post" action="/admin/malls/edit/{{ $mall->mallID }}/information">
            @csrf
            <h5 class="card-header">Information</h5>
            <div class="card-body">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-description-tab" data-toggle="tab"
                           href="#nav-description" role="tab" aria-controls="nav-description"
                           aria-selected="true">Description</a>
                        <a class="nav-item nav-link" id="nav-management-tab" data-toggle="tab"
                           href="#nav-management" role="tab" aria-controls="nav-management"
                           aria-selected="false">Management</a>
                        <a class="nav-item nav-link" id="nav-marketing-tab" data-toggle="tab"
                           href="#nav-marketing" role="tab" aria-controls="nav-marketing"
                           aria-selected="false">Marketing</a>
                        <a class="nav-item nav-link" id="nav-parking-tab" data-toggle="tab"
                           href="#nav-parking" role="tab" aria-controls="nav-parking"
                           aria-selected="false">Parking</a>
                        <a class="nav-item nav-link" id="nav-security-tab" data-toggle="tab"
                           href="#nav-security" role="tab" aria-controls="nav-security"
                           aria-selected="false">Security</a>
                        <a class="nav-item nav-link" id="nav-facilities-tab" data-toggle="tab"
                           href="#nav-facilities" role="tab" aria-controls="nav-facilities"
                           aria-selected="false">Facilities</a>
                        <a class="nav-item nav-link" id="nav-leasing-tab" data-toggle="tab"
                           href="#nav-leasing" role="tab" aria-controls="nav-leasing"
                           aria-selected="false">Leasing</a>
                        <a class="nav-item nav-link" id="nav-gift-cards-tab" data-toggle="tab"
                           href="#nav-gift-cards" role="tab" aria-controls="nav-gift-cards"
                           aria-selected="false">Gift Cards</a>
                        <a class="nav-item nav-link" id="nav-tourism-tab" data-toggle="tab"
                           href="#nav-tourism" role="tab" aria-controls="nav-tourism"
                           aria-selected="false">Tourism</a>
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">

                    <?php
                    $obj = $mall->details;
                    if ($obj) {
                        $details = $obj->toArray();
                    } else {
                        $details = array(
                            'management' => '',
                            'marketing' => '',
                            'parking' => '',
                            'security' => '',
                            'facilities' => '',
                            'leasing' => '',
                            'giftcards' => '',
                            'tourism' => ''
                        );
                    }
                    ?>
                    <div class="tab-pane fade pt-4 show active" id="nav-description" role="tabpanel"
                         aria-labelledby="nav-description-tab">
                    <textarea class="wysi-text-area" name="mallDescription"
                              rows="7">{{ $mall->mallDescription }}</textarea>
                    </div>
                    <div class="tab-pane fade pt-4" id="nav-management" role="tabpanel"
                         aria-labelledby="nav-management-tab">
                    <textarea class="wysi-text-area" name="details_management"
                              rows="7">{{ $details['management'] }}</textarea>
                    </div>
                    <div class="tab-pane fade pt-4" id="nav-marketing" role="tabpanel"
                         aria-labelledby="nav-marketing-tab">
                    <textarea class="wysi-text-area" name="details_marketing"
                              rows="7">{{ $details['marketing'] }}</textarea>
                    </div>
                    <div class="tab-pane fade pt-4" id="nav-parking" role="tabpanel"
                         aria-labelledby="nav-parking-tab">
                    <textarea class="wysi-text-area" name="details_parking"
                              rows="7">{{ $details['parking'] }}</textarea>
                    </div>
                    <div class="tab-pane fade pt-4" id="nav-security" role="tabpanel"
                         aria-labelledby="nav-security-tab">
                    <textarea class="wysi-text-area" name="details_security"
                              rows="7">{{ $details['security'] }}</textarea>
                    </div>
                    <div class="tab-pane fade pt-4" id="nav-facilities" role="tabpanel"
                         aria-labelledby="nav-facilities-tab">
                    <textarea class="wysi-text-area" name="details_facilities"
                              rows="7">{{ $details['facilities'] }}</textarea>
                    </div>
                    <div class="tab-pane fade pt-4" id="nav-leasing" role="tabpanel"
                         aria-labelledby="nav-leasing-tab">
                    <textarea class="wysi-text-area" name="details_leasing"
                              rows="7">{{ $details['leasing'] }}</textarea>
                    </div>
                    <div class="tab-pane fade pt-4" id="nav-gift-cards" role="tabpanel"
                         aria-labelledby="nav-gift-cards-tab">
                    <textarea class="wysi-text-area" name="details_giftcards"
                              rows="7">{{ $details['giftcards'] }}</textarea>
                    </div>
                    <div class="tab-pane fade pt-4" id="nav-tourism" role="tabpanel"
                         aria-labelledby="nav-tourism-tab">
                    <textarea class="wysi-text-area" name="details_tourism"
                              rows="7">{{ $details['tourism'] }}</textarea>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary float-right">Save Information</button>
            </div>
        </form>
    </div>
    <div class="card shadow mb-4">
        <form method="post" action="/admin/malls/edit/{{ $mall->mallID }}/images" enctype="multipart/form-data">
            @csrf
            <h5 class="card-header">Images</h5>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="logo">Logo&nbsp;(Only gif, jpg or png files)</label>
                        <br/>
                        @if(!empty($mall->logo))
                        <button class="btn btn-danger btn-sm action_remove"
                                id="logo"
                                v-on:click="deleteImage"
                                style="margin-bottom: 6px;">
                            <i class="fa fa-trash-o"></i>
                            Remove Image
                        </button>
                        <br/>
                        <img
                            src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $mall->logo }}"
                            alt="Logo"/>
                        @endif
                        <div class="file-field">
                            <div class="btn btn-light">
                                <input type="file" name="logo" accept="image/png, image/jpeg"/>
                            </div>
                            <div class="invalid-feedback">
                                {{ ($errors->has('logo') ? $errors->first('logo') : '') }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="image1">Mall Image 1</label>
                        <br/>
                        @if(!empty($mall->image1))
                        <button class="btn btn-danger btn-sm action_remove"
                                id="image1"
                                v-on:click="deleteImage"
                                style="margin-bottom: 6px;">
                            <i class="fa fa-trash-o"></i>
                            Remove Image
                        </button>
                        <br/>
                        <img
                            src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $mall->thumbnail1 }}"
                            alt="image1"/>
                        @endif
                        <div class="file-field">
                            <div class="btn btn-light">
                                <input type="file" name="thumbnail1" accept="image/png, image/jpeg"/>
                            </div>
                            <div class="invalid-feedback">
                                {{ ($errors->has('image1') ? $errors->first('image1') : '') }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="image2">Mall Image 2</label>
                        <br/>
                        @if(!empty($mall->image2))
                        <button class="btn btn-danger btn-sm action_remove"
                                id="image2"
                                v-on:click="deleteImage"
                                style="margin-bottom: 6px;">
                            <i class="fa fa-trash-o"></i>
                            Remove Image
                        </button>
                        <br/>
                        <img
                            src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $mall->thumbnail2 }}"
                            alt="image2"/>
                        @endif
                        <div class="file-field">
                            <div class="btn btn-light">
                                <input type="file" name="image2" accept="image/png, image/jpeg"/>
                            </div>
                            <div class="invalid-feedback">
                                {{ ($errors->has('image2') ? $errors->first('image2') : '') }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="image3">Mall Image 3</label>
                        <br/>
                        @if(!empty($mall->image3))
                        <button class="btn btn-danger btn-sm action_remove"
                                id="image3"
                                v-on:click="deleteImage"
                                style="margin-bottom: 6px;">
                            <i class="fa fa-trash-o"></i>
                            Remove Image
                        </button>
                        <br/>
                        <img
                            src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $mall->thumbnail3 }}"
                            alt="image3"/>
                        @endif
                        <div class="file-field">
                            <div class="btn btn-light">
                                <input type="file" name="image3" accept="image/png, image/jpeg"/>
                            </div>
                            <div class="invalid-feedback">
                                {{ ($errors->has('image3') ? $errors->first('image3') : '') }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="image4">Mall Image 4</label>
                        <br/>
                        @if(!empty($mall->image4))
                        <button class="btn btn-danger btn-sm action_remove"
                                id="image4"
                                v-on:click="deleteImage"
                                style="margin-bottom: 6px;">
                            <i class="fa fa-trash-o"></i>
                            Remove Image
                        </button>
                        <br/>
                        <img
                            src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $mall->thumbnail4 }}"
                            alt="image4"/>
                        @endif
                        <div class="file-field">
                            <div class="btn btn-light">
                                <input type="file" name="image4" accept="image/png, image/jpeg"/>
                            </div>
                            <div class="invalid-feedback">
                                {{ ($errors->has('image4') ? $errors->first('image4') : '') }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="image5">Mall Image 5</label>
                        <br/>
                        @if(!empty($mall->image5))
                        <button class="btn btn-danger btn-sm action_remove"
                                id="image5"
                                v-on:click="deleteImage"
                                style="margin-bottom: 6px;">
                            <i class="fa fa-trash-o"></i>
                            Remove Image
                        </button>
                        <br/>
                        <img
                            src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $mall->thumbnail5 }}"
                            alt="image5"/>
                        @endif
                        <div class="file-field">
                            <div class="btn btn-light">
                                <input type="file" name="image5" accept="image/png, image/jpeg"/>
                            </div>
                            <div class="invalid-feedback">
                                {{ ($errors->has('image5') ? $errors->first('image5') : '') }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="image6">Mall Image 6</label>
                        <br/>
                        @if(!empty($mall->image6))
                        <button class="btn btn-danger btn-sm action_remove"
                                id="image6"
                                v-on:click="deleteImage"
                                style="margin-bottom: 6px;">
                            <i class="fa fa-trash-o"></i>
                            Remove Image
                        </button>
                        <br/>
                        <img
                            src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $mall->thumbnail6 }}"
                            alt="image6"/>
                        @endif
                        <div class="file-field">
                            <div class="btn btn-light">
                                <input type="file" name="image6" accept="image/png, image/jpeg"/>
                            </div>
                            <div class="invalid-feedback">
                                {{ ($errors->has('image6') ? $errors->first('image6') : '') }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="image7">Mall Image 7</label>
                        <br/>
                        @if(!empty($mall->image7))
                        <button class="btn btn-danger btn-sm action_remove"
                                id="image7"
                                v-on:click="deleteImage"
                                style="margin-bottom: 6px;">
                            <i class="fa fa-trash-o"></i>
                            Remove Image
                        </button>
                        <br/>
                        <img
                            src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $mall->thumbnail7 }}"
                            alt="image7"/>
                        @endif
                        <div class="file-field">
                            <div class="btn btn-light">
                                <input type="file" name="image7" accept="image/png, image/jpeg"/>
                            </div>
                            <div class="invalid-feedback">
                                {{ ($errors->has('image7') ? $errors->first('image7') : '') }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="image8">Mall Image 8</label>
                        <br/>
                        @if(!empty($mall->image8))
                        <button class="btn btn-danger btn-sm action_remove"
                                id="image8"
                                v-on:click="deleteImage"
                                style="margin-bottom: 6px;">
                            <i class="fa fa-trash-o"></i>
                            Remove Image
                        </button>
                        <br/>
                        <img
                            src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $mall->thumbnail8 }}"
                            alt="image8"/>
                        @endif
                        <div class="file-field">
                            <div class="btn btn-light">
                                <input type="file" name="image8" accept="image/png, image/jpeg"/>
                            </div>
                            <div class="invalid-feedback">
                                {{ ($errors->has('image8') ? $errors->first('image8') : '') }}
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="image9">Mall Image 9</label>
                        <br/>
                        @if(!empty($mall->image9))
                        <button class="btn btn-danger btn-sm action_remove"
                                id="image9"
                                v-on:click="deleteImage"
                                style="margin-bottom: 6px;">
                            <i class="fa fa-trash-o"></i>
                            Remove Image
                        </button>
                        <br/>
                        <img
                            src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $mall->thumbnail9 }}"
                            alt="image9"/>
                        @endif
                        <div class="file-field">
                            <div class="btn btn-light">
                                <input type="file" name="image9" accept="image/png, image/jpeg"/>
                            </div>
                            <div class="invalid-feedback">
                                {{ ($errors->has('image9') ? $errors->first('image9') : '') }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="image10">Mall Image 10</label>
                        <br/>
                        @if(!empty($mall->image10))
                        <button class="btn btn-danger btn-sm action_remove"
                                id="image10"
                                v-on:click="deleteImage"
                                style="margin-bottom: 6px;">
                            <i class="fa fa-trash-o"></i>
                            Remove Image
                        </button>
                        <br/>
                        <img
                            src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $mall->thumbnail10 }}"
                            alt="image10"/>
                        @endif
                        <div class="file-field">
                            <div class="btn btn-light">
                                <input type="file" name="image10" accept="image/png, image/jpeg"/>
                            </div>
                            <div class="invalid-feedback">
                                {{ ($errors->has('image10') ? $errors->first('image10') : '') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary float-right">Save Images</button>
            </div>
        </form>
    </div>

    <div class="card shadow mb-4">
        <form method="post" action="/admin/malls/edit/{{ $mall->mallID }}/special-features"
              enctype="multipart/form-data">
            @csrf
            <h5 class="card-header">Special Features</h5>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="specialFeature1">Special Feature 1</label>
                        <input name="specialFeature1"
                               type="text"
                               class="form-control form-control-sm"
                               id="specialFeature1"
                               value="{{ old('specialFeature1', $mall->specialFeature1) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="specialFeature2">Special Feature 2</label>
                        <input name="specialFeature2"
                               type="text"
                               class="form-control form-control-sm"
                               id="specialFeature2"
                               value="{{ old('specialFeature2', $mall->specialFeature2) }}">

                    </div>
                    <div class="form-group col-md-4">
                        <label for="specialFeature3">Special Feature 3</label>
                        <input name="specialFeature3"
                               type="text"
                               class="form-control form-control-sm"
                               id="specialFeature3"
                               value="{{ old('specialFeature3', $mall->specialFeature3) }}">

                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="specialFeature4">Special Feature 4</label>
                        <input name="specialFeature4"
                               type="text"
                               class="form-control form-control-sm"
                               id="specialFeature4"
                               value="{{ old('specialFeature4', $mall->specialFeature4) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="specialFeature5">Special Feature 5</label>
                        <input name="specialFeature5"
                               type="text"
                               class="form-control form-control-sm"
                               id="specialFeature5"
                               value="{{ old('specialFeature5', $mall->specialFeature5) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="specialFeature6">Special Feature 6</label>
                        <input name="specialFeature6"
                               type="text"
                               class="form-control form-control-sm"
                               id="specialFeature6"
                               value="{{ old('specialFeature6', $mall->specialFeature6) }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="specialFeature7">Special Feature 7</label>
                        <input name="specialFeature7"
                               type="text"
                               class="form-control form-control-sm"
                               id="specialFeature7"
                               value="{{ old('specialFeature7', $mall->specialFeature7) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="specialFeature8">Special Feature 8</label>
                        <input name="specialFeature8"
                               type="text"
                               class="form-control form-control-sm"
                               id="specialFeature8"
                               value="{{ old('specialFeature8', $mall->specialFeature8) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="specialFeature9">Special Feature 6</label>
                        <input name="specialFeature9"
                               type="text"
                               class="form-control form-control-sm"
                               id="specialFeature9"
                               value="{{ old('specialFeature9', $mall->specialFeature9) }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="specialFeature10">Special Feature 10</label>
                        <input name="specialFeature10"
                               type="text"
                               class="form-control form-control-sm"
                               id="specialFeature10"
                               value="{{ old('specialFeature10', $mall->specialFeature10) }}">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary float-right">Save special features</button>
            </div>
        </form>
    </div>

    <div class="card shadow mb-4">
        <form method="post" action="/admin/malls/edit/{{ $mall->mallID }}/centre-management"
              enctype="multipart/form-data">
            @csrf
            <h5 class="card-header">Centre Management</h5>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="centreManager">Centre Manager</label>
                        <input name="centreManager"
                               type="text"
                               class="form-control form-control-sm"
                               id="centreManager"
                               value="{{ old('centreManager', $mall->centreManager) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="centreManagerTelephone">Tel</label>
                        <input name="centreManagerTelephone"
                               type="text"
                               class="form-control form-control-sm"
                               id="centreManagerTelephone"
                               value="{{ old('centreManagerTelephone', $mall->centreManagerTelephone) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="centreManagerFax">Fax</label>
                        <input name="centreManagerFax"
                               type="text"
                               class="form-control form-control-sm"
                               id="centreManagerFax"
                               value="{{ old('centreManagerFax', $mall->centreManagerFax) }}">

                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="centreManagerCell">Cell</label>
                        <input name="centreManagerCell"
                               type="text"
                               class="form-control form-control-sm"
                               id="centreManagerCell"
                               value="{{ old('centreManagerCell', $mall->centreManagerCell) }}">

                    </div>
                    <div class="form-group col-md-4">
                        <label for="centreManagerEmail">Centre Manager Email</label>
                        <input name="centreManagerEmail"
                               type="text"
                               class="form-control form-control-sm"
                               id="centreManagerEmail"
                               value="{{ old('centreManagerEmail', $mall->centreManagerEmail) }}">

                        <div class="invalid-feedback">
                            {{ ($errors->has('centreManagerEmail') ? $errors->first('centreManagerEmail') : '') }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary float-right">Save Special Features</button>
            </div>
        </form>
    </div>

    <div class="card shadow mb-4">
        <form method="post" action="/admin/malls/edit/{{ $mall->mallID }}/marketing-consultant"
              enctype="multipart/form-data">
            @csrf
            <h5 class="card-header">Marketing Consultant</h5>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="marketingConsultant">Marketing Company Name</label>
                        <input name="marketingConsultant"
                               type="text"
                               class="form-control form-control-sm"
                               id="marketingConsultant"
                               value="{{ old('marketingConsultant', $mall->marketingConsultant) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="marketingTelephone">Tel</label>
                        <input name="marketingTelephone"
                               type="text"
                               class="form-control form-control-sm"
                               id="marketingTelephone"
                               value="{{ old('marketingTelephone', $mall->marketingTelephone) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="marketingCell">Cell</label>
                        <input name="marketingCell"
                               type="text"
                               class="form-control form-control-sm"
                               id="marketingCell"
                               value="{{ old('marketingCell', $mall->marketingCell) }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="marketingFax">Fax</label>
                        <input name="marketingFax"
                               type="text"
                               class="form-control form-control-sm"
                               id="marketingFax"
                               value="{{ old('marketingFax', $mall->marketingFax) }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="marketingManager">Marketing Manager</label>
                        <input name="marketingManager"
                               type="text"
                               class="form-control form-control-sm"
                               id="marketingManager"
                               value="{{ old('marketingManager', $mall->marketingManager) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="marketingManagerEmail">Marketing Manager Email Address</label>
                        <input name="marketingManagerEmail"
                               type="text"
                               class="form-control form-control-sm"
                               id="marketingManagerEmail"
                               value="{{ old('marketingManagerEmail', $mall->marketingManagerEmail) }}">
                        <div class="invalid-feedback">
                            {{ ($errors->has('marketingManagerEmail') ? $errors->first('marketingManagerEmail') : '') }}
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="marketingAssistant">Marketing Assistant</label>
                        <input name="marketingAssistant"
                               type="text"
                               class="form-control form-control-sm"
                               id="marketingAssistant"
                               value="{{ old('marketingAssistant', $mall->marketingAssistant) }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="marketingAssistantEmail">Marketing Assistant Email Address</label>
                        <input name="marketingAssistantEmail"
                               type="text"
                               class="form-control form-control-sm"
                               id="marketingAssistantEmail"
                               value="{{ old('marketingAssistantEmail', $mall->marketingAssistantEmail) }}">
                        <div class="invalid-feedback">
                            {{ ($errors->has('marketingAssistantEmail') ? $errors->first('marketingAssistantEmail') :
                            '') }}
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="marketingContractStart">Start Date</label>
                        <input name="marketingContractStart"
                               type="text"
                               readonly="readonly"
                               class="form-control form-control-sm date-field" id="marketingContractStart"
                               value="{{ old('marketingContractStart', $mall->marketingContractStart) }}">
                    </div>
                    <div class="form-group col-md-4">
                        <label for="marketingContractEnd">End Date</label>
                        <input name="marketingContractEnd"
                               type="text"
                               readonly="readonly"
                               class="form-control form-control-sm date-field" id="marketingContractEnd"
                               value="{{ old('marketingContractEnd', $mall->marketingContractEnd) }}">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary float-right">Save Marketing Consultant</button>
            </div>
        </form>
    </div>

    <div class="card shadow mb-4">
        <form method="post" action="/admin/malls/edit/{{ $mall->mallID }}/leasing-agent"
              enctype="multipart/form-data">
            @csrf
            <h5 class="card-header">Leasing Agent</h5>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="leasingAgent">Leasing agent</label>
                        <div class="controls">
                            <input name="leasingAgent" type="text"
                                   class="form-control form-control-sm"
                                   id="leasingAgent"
                                   value="{{ old('leasingAgent', $mall->leasingAgent) }}">
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="leasingContactPerson">Leasing contact person</label>
                        <div class="controls">
                            <input name="leasingContactPerson"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="leasingContactPerson"
                                   value="{{ old('leasingContactPerson', $mall->leasingContactPerson) }}">
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="leasingTelephone">Tel</label>
                        <div class="controls">
                            <input name="leasingTelephone" type="text"
                                   class="form-control form-control-sm"
                                   id="leasingTelephone"
                                   value="{{ old('leasingTelephone', $mall->leasingTelephone) }}">
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="leasingFax">Fax</label>
                        <div class="controls">
                            <input name="leasingFax"
                                   type="text"
                                   class="form-control form-control-sm"
                                   id="leasingFax"
                                   value="{{ old('leasingFax', $mall->leasingFax) }}">
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="leasingCell">Cell</label>
                        <div class="controls">
                            <input name="leasingCell" type="text"
                                   class="form-control form-control-sm"
                                   id="leasingCell"
                                   value="{{ old('leasingCell', $mall->leasingCell) }}">
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="leasingEmail">Email</label>
                        <div class="controls">
                            <input name="leasingEmail" type="text"
                                   class="form-control form-control-sm"
                                   id="leasingEmail"
                                   value="{{ old('leasingEmail', $mall->leasingEmail) }}">
                            <div class="invalid-feedback">
                                {{ ($errors->has('leasingEmail') ? $errors->first('leasingEmail') : '') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary float-right">Save Leasing Agent</button>
            </div>
        </form>
    </div>

    <div class="card shadow mb-4">
        <form method="post" action="/admin/malls/edit/{{ $mall->mallID }}/shopping-council"
              enctype="multipart/form-data">
            @csrf
            <h5 class="card-header">Shopping Council Details</h5>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="shoppingCouncilMember">Are you a shopping council member?</label>
                        <br/>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="shoppingCouncilMember"
                                   id="shoppingCouncilMember1" value="Y" @if($mall->shoppingCouncilMember == 'Y')
                            checked @endif>
                            <label class="form-check-label label-inline" for="shoppingCouncilMember1">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="shoppingCouncilMember"
                                   id="shoppingCouncilMember" value="N" @if($mall->shoppingCouncilMember == 'N') checked
                            @endif>
                            <label class="form-check-label label-inline" for="shoppingCouncilMember">No</label>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="shoppingCouncilNumber">If so, what is your shopping council member
                            number?</label>
                        <input name="shoppingCouncilNumber" type="text"
                               class="form-control form-control-sm" id="shoppingCouncilNumber"
                               value="{{ $mall->shoppingCouncilNumber }}">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary float-right">Save Shopping Council Details</button>
            </div>
        </form>
    </div>

    <div class="card shadow mb-4">
        <form method="post" action="/admin/malls/edit/{{ $mall->mallID }}/trading-hours"
              enctype="multipart/form-data">
            @csrf
            <h5 class="card-header">Trading Hours</h5>
            <div class="card-body">
                <h6>Trading Hours</h6>
                <?php

                $startTimeMonday = isset($mall->tradinghour->startHoursMon) ? $mall->tradinghour->startHoursMon : '';
                $endTimeMonday = isset($mall->tradinghour->endHoursMon) ? $mall->tradinghour->endHoursMon : '';
                $startTimeTues = isset($mall->tradinghour->startHoursTues) ? $mall->tradinghour->startHoursTues : '';
                $endTimeTues = isset($mall->tradinghour->endHoursTues) ? $mall->tradinghour->endHoursTues : '';
                $startTimeWed = isset($mall->tradinghour->startHoursWed) ? $mall->tradinghour->startHoursWed : '';
                $endTimeWed = isset($mall->tradinghour->endHoursWed) ? $mall->tradinghour->endHoursWed : '';
                $startTimeThurs = isset($mall->tradinghour->startHoursThurs) ? $mall->tradinghour->startHoursThurs : '';
                $endTimeThurs = isset($mall->tradinghour->endHoursThurs) ? $mall->tradinghour->endHoursThurs : '';
                $startTimeFri = isset($mall->tradinghour->startHoursFri) ? $mall->tradinghour->startHoursFri : '';
                $endTimeFri = isset($mall->tradinghour->endHoursFri) ? $mall->tradinghour->endHoursFri : '';
                $startTimeSat = isset($mall->tradinghour->startHoursSat) ? $mall->tradinghour->startHoursSat : '';
                $endTimeSat = isset($mall->tradinghour->endHoursSat) ? $mall->tradinghour->endHoursSat : '';
                $startTimeSun = isset($mall->tradinghour->startHoursSun) ? $mall->tradinghour->startHoursSun : '';
                $endTimeSun = isset($mall->tradinghour->endHoursSun) ? $mall->tradinghour->endHoursSun : '';
                $startTimePublicHolidays = isset($mall->tradinghour->startHoursPublicHolidays) ? $mall->tradinghour->startHoursPublicHolidays : '';
                $endTimePublicHolidays = isset($mall->tradinghour->endHoursPublicHolidays) ? $mall->tradinghour->endHoursPublicHolidays : '';
                ?>
                <ul class="list-group col-md-8 list-group-flush">
                    <li class="list-group-item">
                        <div class="form-row">
                            <div class="col-md-4">
                                Monday
                            </div>
                            <div class="col-md-6">
                                <select class="custom-select custom-select-sm with-drop-down" id="startHoursMon"
                                        name="startHoursMon">
                                    <option value="">Select ...</option>
                                    @foreach($startTimeRanges as $key => $startTimeRange)
                                    <option value="{{ $key }}" @if($key== $startTimeMonday)
                                            selected="selected" @endif>
                                        {{ $startTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                                &nbsp;-&nbsp;
                                <select class="custom-select custom-select-sm" id="endHoursMon" name="endHoursMon">
                                    <option value="">Select ...</option>
                                    @foreach($endTimeRanges as $key => $endTimeRange)
                                    <option value="{{ $key }}" @if($key== $endTimeMonday)
                                            selected="selected" @endif>
                                        {{ $endTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="form-row">
                            <div class="col-md-4">
                                Tuesday
                            </div>
                            <div class="col-md-6">
                                <select class="custom-select custom-select-sm" id="startHoursTues"
                                        name="startHoursTues">
                                    <option value="">Select ...</option>
                                    @foreach($startTimeRanges as $key => $startTimeRange)
                                    <option value="{{ $key }}" @if($key== $startTimeTues)
                                            selected="selected" @endif>
                                        {{ $startTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                                &nbsp;-&nbsp;
                                <select class="custom-select custom-select-sm" id="endHoursTues" name="endHoursTues">
                                    <option value="">Select ...</option>
                                    @foreach($endTimeRanges as $key => $endTimeRange)
                                    <option value="{{ $key }}" @if($key== $endTimeTues)
                                            selected="selected" @endif>
                                        {{ $endTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="form-row">
                            <div class="col-md-4">
                                Wednesday
                            </div>
                            <div class="col-md-6">
                                <select class="custom-select custom-select-sm" id="startHoursWed" name="startHoursWed">
                                    <option value="">Select ...</option>
                                    @foreach($startTimeRanges as $key => $startTimeRange)
                                    <option value="{{ $key }}" @if($key== $startTimeWed)
                                            selected="selected" @endif>
                                        {{ $startTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                                &nbsp;-&nbsp;
                                <select class="custom-select custom-select-sm" id="endHoursWed" name="endHoursWed">
                                    <option value="">Select ...</option>
                                    @foreach($endTimeRanges as $key => $endTimeRange)
                                    <option value="{{ $key }}" @if($key== $endTimeWed)
                                            selected="selected" @endif>
                                        {{ $endTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="form-row">
                            <div class="col-md-4">
                                Thursday
                            </div>
                            <div class="col-md-6">
                                <select class="custom-select custom-select-sm" id="startHoursThurs"
                                        name="startHoursThurs">
                                    <option value="">Select ...</option>
                                    @foreach($startTimeRanges as $key => $startTimeRange)
                                    <option value="{{ $key }}" @if($key== $startTimeThurs)
                                            selected="selected" @endif>
                                        {{ $startTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                                &nbsp;-&nbsp;
                                <select class="custom-select custom-select-sm" id="endHoursThurs" name="endHoursThurs">
                                    <option value="">Select ...</option>
                                    @foreach($endTimeRanges as $key => $endTimeRange)
                                    <option value="{{ $key }}" @if($key== $endTimeThurs)
                                            selected="selected" @endif>
                                        {{ $endTimeRange }}
                                    </option>
                                    @endforeach
                                </select></div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="form-row">
                            <div class="col-md-4">
                                Friday
                            </div>
                            <div class="col-md-6">
                                <select class="custom-select custom-select-sm" id="startHoursFri" name="startHoursFri">
                                    <option value="">Select ...</option>
                                    @foreach($startTimeRanges as $key => $startTimeRange)
                                    <option value="{{ $key }}" @if($key== $startTimeFri)
                                            selected="selected" @endif>
                                        {{ $startTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                                &nbsp;-&nbsp;
                                <select class="custom-select custom-select-sm" id="endHoursFri" name="endHoursFri">
                                    <option value="">Select ...</option>
                                    @foreach($endTimeRanges as $key => $endTimeRange)
                                    <option value="{{ $key }}" @if($key== $endTimeFri)
                                            selected="selected" @endif>
                                        {{ $endTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="form-row">
                            <div class="col-md-4">
                                Saturday
                            </div>
                            <div class="col-md-6">
                                <select class="custom-select custom-select-sm" id="startHoursSat" name="startHoursSat">
                                    <option value="">Select ...</option>
                                    @foreach($startTimeRanges as $key => $startTimeRange)
                                    <option value="{{ $key }}" @if($key== $startTimeSat)
                                            selected="selected" @endif>
                                        {{ $startTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                                &nbsp;-&nbsp;
                                <select class="custom-select custom-select-sm" id="endHoursSat" name="endHoursSat">
                                    <option value="">Select ...</option>
                                    @foreach($endTimeRanges as $key => $endTimeRange)
                                    <option value="{{ $key }}" @if($key== $endTimeSat)
                                            selected="selected" @endif>
                                        {{ $endTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="form-row">
                            <div class="col-md-4">
                                Sunday
                            </div>
                            <div class="col-md-6">
                                <select class="custom-select custom-select-sm" id="startHoursSun" name="startHoursSun">
                                    <option value="">Select ...</option>
                                    @foreach($startTimeRanges as $key => $startTimeRange)
                                    <option value="{{ $key }}" @if($key== $startTimeSun)
                                            selected="selected" @endif>
                                        {{ $startTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                                &nbsp;-&nbsp;
                                <select class="custom-select custom-select-sm" id="endHoursSun" name="endHoursSun">
                                    <option value="">Select ...</option>
                                    @foreach($endTimeRanges as $key => $endTimeRange)
                                    <option value="{{ $key }}" @if($key== $endTimeSun)
                                            selected="selected" @endif>
                                        {{ $endTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="form-row">
                            <div class="col-md-4">
                                Public Holidays
                            </div>
                            <div class="col-md-6">
                                <select class="custom-select custom-select-sm" id="startHoursPublicHolidays"
                                        name="startHoursPublicHolidays">
                                    <option value="">Select ...</option>
                                    @foreach($startTimeRanges as $key => $startTimeRange)
                                    <option value="{{ $key }}" @if($key== $startTimePublicHolidays)
                                            selected="selected" @endif>
                                        {{ $startTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                                &nbsp;-&nbsp;
                                <select class="custom-select custom-select-sm" id="endHoursPublicHolidays"
                                        name="endHoursPublicHolidays">
                                    <option value="">Select ...</option>
                                    <option value="Till Late">Until late</option>
                                    <option value="optional">Optional</option>
                                    @foreach($endTimeRanges as $key => $endTimeRange)
                                    <option value="{{ $key }}" @if($key== $endTimePublicHolidays)
                                            selected="selected" @endif>
                                        {{ $endTimeRange }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item"></li>
                </ul>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary float-right">Save Trading Hours</button>
            </div>
        </form>
    </div>

    <div class="card shadow mb-4">
        <form method="post" action="/admin/malls/edit/{{ $mall->mallID }}/settings"
              enctype="multipart/form-data">
            @csrf
            <h5 class="card-header">Settings</h5>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="activate">Activate mall?</label>
                        <br/>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="activate"
                                   id="activate" value="Y" @if($mall->activate == 'Y') checked @endif>
                            <label class="label-inline" for="activate">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="activate"
                                   id="activate1" value="N" @if($mall->activate == 'N') checked @endif>
                            <label class="label-inline" for="activate1">No</label>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="activate">Must mall be linked to Facebook</label>
                        <br/>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="facebook"
                                   id="facebook" value="Y" @if($mall->facebook == 'Y') checked @endif>
                            <label class="label-inline" for="facebook">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="facebook"
                                   id="facebook1" value="N" @if($mall->facebook == 'N') checked @endif>
                            <label class="label-inline" for="facebook1">No</label>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="activate">Display mall on Mallguide?</label>
                        <br/>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="mallguideDisplay"
                                   id="mallguideDisplay" value="Y" @if($mall->mallguideDisplay == 'Y') checked @endif>
                            <label class="label-inline" for="mallguideDisplay">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="mallguideDisplay"
                                   id="mallguideDisplay1" value="N" @if($mall->mallguideDisplay == 'N') checked @endif>
                            <label class="label-inline" for="mallguideDisplay1">No</label>
                        </div>
                    </div>
                </div>
                <div class="form-row" style="margin-top: 16px;">
                    <div class="form-group col-md-4">
                        <label for="activate">RSS Enabled?</label>
                        <br/>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="rss_enabled"
                                   id="rss_enabled" value="1" @if($mall->rss_enabled == '1') checked @endif>
                            <label class="label-inline" for="rss_enabled">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="rss_enabled"
                                   id="rss_enabled1" value="0" @if($mall->rss_enabled == '0') checked @endif>
                            <label class="label-inline" for="rss_enabled1">No</label>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="activate">Does mall have a blog?</label>
                        <br/>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="blog"
                                   id="blog" value="1" @if($mall->blog == '1') checked @endif>
                            <label class="label-inline" for="blog">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="blog"
                                   id="blog1" value="0" @if($mall->blog == '0') checked @endif>
                            <label class="label-inline" for="blog1">No</label>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="blog_url">Blog Url</label>
                        <input name="blog_url" type="text"
                               class="form-control form-control-sm" id="centreManagerEmail"
                               value="{{ $mall->blog_url }}">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary float-right">Save Settings</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('javascript')
<script src="{{ asset('js/gijgo.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bs-custom-file-input.min.js') }}"></script>
<script src="{{ asset('js/tinymce/js/tinymce/tinymce.js') }}"></script>
<script src="{{ asset('js/edit_mall.js') }}"></script>
<script>
    // Register plugin with a short name
    $(document).ready(function () {
        tinymce.init({
            selector: '.wysi-text-area',
            plugins: "image link -mailto ",
            image_advtab: true,
            menubar: false,
            force_br_newlines: true,
            force_p_newlines: false,
            forced_root_block: ''
        });

        tinymce.create('tinymce.plugins.MailToPlugin', {

            init: function (ed, url) {
                ed.addCommand('mceMailTo', function () {
                    var linkText = ed.selection.getContent({format: 'text'});
                    var newText = "<a href='mailto:foo@bar.com?subject=testing'>" + linkText + "</a>"
                    ed.execCommand('mceInsertContent', false, newText);
                });

                // Register example button
                ed.addButton('mailto', {
                    title: 'MailTo',
                    cmd: 'mceMailTo',
                    image: url + '/images/mailto.gif'
                });
            }
        });

        tinymce.PluginManager.add('mailto', tinymce.plugins.MailToPlugin);

        $('#marketingContractStart').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd'
        });

        $('#marketingContractEnd').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd',
        });
    })

</script>
@endsection
