@extends('layouts.admin')

@section('content')

<div class="container" style="padding-bottom: 150px">
    @include('notifications')
    @if (isset($event))
    <form method="post" action="/admin/events/post-update/{{ $event->eventsMGID }}" enctype="multipart/form-data">
        @else
        <form method="post" action="/admin/events/post-create/{{ $mall->mallID }}" enctype="multipart/form-data">
            @endif
            @csrf
            <div class="card shadow mb-4">
                <h5 class="card-header">{{ $mall->name }} >> Add Event</h5>
                <div class="card-body">
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="name">Event Name *</label>
                            <input name="name" type="text" class="form-control form-control-sm" id="name"
                                   value="@if(isset($event)) {{ $event->name }} @else {{ old('name') }} @endif">
                            <div class="invalid-feedback">
                                {{ ($errors->has('name') ? $errors->first('name') : '') }}
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="event">Event description</label>
                            <textarea class="wysi-text-area" name="event"
                                      rows="7">{{ isset($event) ? $event->event : old('event') }}</textarea>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="startDate">Start Date *</label>
                            <input name="startDate" type="text"
                                   readonly="readonly"
                                   class="form-control form-control-sm date-field"
                                   id="startDate"
                                   value="@if(isset($event)) {{ $event->startDate }} @else {{ old('startDate') }} @endif">
                            <div class="invalid-feedback">
                                {{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="endDate">End Date *</label>
                            <input name="endDate" type="text"
                                   readonly="readonly"
                                   class="form-control form-control-sm date-field"
                                   id="endDate"
                                   value="@if(isset($event)) {{ $event->endDate }} @else {{ old('endDate') }} @endif">
                            <div class="invalid-feedback">
                                {{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}
                            </div>
                        </div>
                    </div>
                    <span>
                    <strong>
                        To specify the category either select an existing one from the category list below or enter a new one into the field to the right:
                    </strong>
                </span>
                    <br/>
                    <br/>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="category">Select a category</label>
                            <select class="form-control" name="category">
                                <option value="">Please select ...</option>
                                @foreach($categories as $key => $value)
                                <option value="{{ $key }}" @if(isset($event) && $event->category == $key) selected
                                    @endif>
                                    {{ $value }}
                                </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="category_text">Category
                                <small>(Please ensure that category does not already exist and check for typos.)</small>
                            </label>
                            <input name="category_text" type="text" class="form-control form-control-sm"
                                   value="@if(isset($event)) {{ $event->category }} @else {{ old('category') }} @endif"/>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="location">Venue List</label>
                            <select class="form-control" name="location">
                                <option value="">Please select ...</option>
                                @foreach($locations as $key => $value)
                                <option value="{{ $key }}" @if(isset($event) && $event->location == $key) selected
                                    @endif>
                                    {{ $value }}
                                </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="location">Venue</label>
                            <input name="location" type="text" class="form-control form-control-sm"
                                   value="@if(isset($event)) {{ $event->location }} @else {{ old('location') }} @endif"/>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="display">Must Event be displayed?</label><br/>

                            <div class="form-check form-check-inline">
                                @if (isset($event))
                                <input class="form-check-input" type="radio" name="display" value="Y" @if( $event->display
                                === 'Y') checked @endif />
                                @else
                                <input class="form-check-input" type="radio" name="display" value="Y" checked/>
                                @endif
                                <label class="form-check-label" for="yes">Yes</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="display" value="N" @if(isset($event)
                                       && $event->display === 'N') checked @endif />
                                <label class="form-check-label" for="display">No</label>
                            </div>
                        </div>

                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="thumbnail1">Event Image</label>
                            @if (isset($event) && $event->thumbnail1 !== null)
                            <br/>
                            <img
                                src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $event->thumbnail1 }}"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="thumbnail1" accept="image/png, image/jpeg"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('thumbnail1') ? $errors->first('thumbnail1') : '') }}
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="fileName">Terms & Conditions File</label>
                            @if (isset($event) && $event->fileName !== '')
                            <br/>
                            <a href="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $event->fileName }}">
                                {{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $event->fileName
                                }}
                            </a>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="fileName"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('fileName') ? $errors->first('fileName') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <h4>Images &nbsp;&nbsp; (AVAILABLE TO FINEGRAFIX ONLY) </h4>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="altImage1">Alternate Image 1 (No resizing of Image will be done)</label>
                            @if (isset($event) && $event->altImage1 !== null)
                            <br/>
                            <img
                                src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $event->altImage1 }}"
                                style="max-width: 300px;max-height: 300px;"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="altImage1"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('altImage1') ? $errors->first('altImage1') : '') }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="altImage2">Alternate Image 2 (No resizing of Image will be done)</label>
                            @if (isset($event) && $event->altImage1 !== null)
                            <br/>
                            <img
                                src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $event->altImage2 }}"
                                style="max-width: 300px;max-height: 300px;"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="altImage2"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('altImage2') ? $errors->first('altImage2') : '') }}
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-4">
                            <label for="altImage3">Alternate Image 2 (No resizing of Image will be done)</label>
                            @if (isset($event) && $event->altImage1 !== null)
                            <br/>
                            <img
                                src="{{ env('MALLGUIDE_URL').'/uploadimages/mall_' . $mall->mallID . '/' . $event->altImage3 }}"
                                style="max-width: 300px;max-height: 300px;"/>
                            @endif
                            <div class="file-field">
                                <div class="btn btn-light">
                                    <input type="file" name="altImage3"/>
                                </div>
                                <div class="invalid-feedback">
                                    {{ ($errors->has('altImage3') ? $errors->first('altImage3') : '') }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    * Required fields
                    <button type="submit" class="btn btn-primary float-right">Save details</button>
                </div>
            </div>
        </form>

</div>
@endsection

@section('javascript')
<script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/bs-custom-file-input/dist/bs-custom-file-input.min.js"></script>
<script src="{{ asset('js/tinymce/js/tinymce/tinymce.js') }}"></script>
<script>
    // Register plugin with a short name
    $(document).ready(function () {
        tinymce.init({
            selector: '.wysi-text-area',
            plugins: "image link -mailto ",
            image_advtab: true,
            menubar: false,
            force_br_newlines: true,
            force_p_newlines: false,
            forced_root_block: ''
        });

        $('#startDate').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd'
        });

        $('#endDate').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd'
        });
    })

</script>
@endsection
