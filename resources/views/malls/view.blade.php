@extends('layouts.pages')
@section('seo_meta')
@section('title')

@section('heading')
    <div class="slim-inner-heading">
        <div class="container-fluid">

            <div class="row-fluid">
                @include('partials.headers.search')
                <div class="span8">
                    <h1 class="animated fadeInDown delay1"><span>Malls: </span>Listed information</h1>

                    <p class="animated fadeInDown delay2">All Mall information, all in one place.</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->
@stop


@section('content')

    @if ($mall->mapX != '' && $mall->mapY != '')
    <?php
//        $weather_info = json_decode(file_get_contents("https://api.forecast.io/forecast/08ee3f806b2b29847a9a86a86f1b5d02/" . $mall->mapX ."," . $mall->mapY ."?units=si" ))
    ?>
    @endif
    <div class="row-fluid">
        <div class="span12">


            <div class="span3">

                <div class="blog-content border_top">
                    <div class="mall-caption">
                        <div class="hborder"><h4>{{ $mall->name }}</h4></div>

                        <div id="mallCarousel" class="carousel slide opacity-fade text-center">

                            <div class="zoom-gallery carousel-inner mall-slides">
                                <!-- Width/height ratio of thumbnail and the main image must match to avoid glitches.

                                If ratios are different, you may add CSS3 opacity transition to the main image to make the change less noticable.

                                 -->
                                <?php $logo =  !empty($mall->logo) ? 'uploadimages/mall_'.$mall->mallID .'/'. $mall->logo : 'img/nomalllogo.png'; ?>
                                <a href="{{ Config::get('app.url') }}/{{ $logo }}" title="{{ $mall->name }}" class="item carousel-item active">
                                    <img src="{{ Config::get('app.url') }}/{{ $logo }}" alt="{{ $mall->name }}" />
                                </a>
                                @if( !empty($mall->image1))
                                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image1 }}" title="{{ $mall->name }}" class="gallery-image item">
                                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image1 }}" alt="{{ $mall->name }}" class="carousel-item" />
                                    </a>
                                @endif
                                @if( !empty($mall->image2))
                                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image2 }}" title="{{ $mall->name }}" class="gallery-image item">
                                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image2 }}" alt="{{ $mall->name }}" class="carousel-item" />
                                    </a>
                                @endif
                                @if( !empty($mall->image3))
                                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image3 }}" title="{{ $mall->name }}" class="gallery-image item">
                                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image3 }}" alt="{{ $mall->name }}" class="carousel-item" />
                                    </a>
                                @endif
                                @if( !empty($mall->image4))
                                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image4 }}" title="{{ $mall->name }}" class="gallery-image item">
                                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image4 }}" alt="{{ $mall->name }}" class="carousel-item" />
                                    </a>
                                @endif
                                @if( !empty($mall->image5))
                                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image5 }}" title="{{ $mall->name }}" class="gallery-image item">
                                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image5 }}" alt="{{ $mall->name }}" class="carousel-item" />
                                    </a>
                                @endif
                                @if( !empty($mall->image6))
                                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image6 }}" title="{{ $mall->name }}" class="gallery-image item">
                                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image6 }}" alt="{{ $mall->name }}" class="carousel-item" />
                                    </a>
                                @endif
                                @if( !empty($mall->image7))
                                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image7 }}" title="{{ $mall->name }}" class="gallery-image item">
                                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image7 }}" alt="{{ $mall->name }}" class="carousel-item" />
                                    </a>
                                @endif
                                @if( !empty($mall->image8))
                                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image8 }}" title="{{ $mall->name }}" class="gallery-image item">
                                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image8 }}" alt="{{ $mall->name }}" class="carousel-item" />
                                    </a>
                                @endif
                                @if( !empty($mall->image9))
                                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image9 }}" title="{{ $mall->name }}" class="gallery-image item">
                                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image9 }}" alt="{{ $mall->name }}" class="carousel-item" />
                                    </a>
                                @endif
                                @if( !empty($mall->image10))
                                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image10 }}" title="{{ $mall->name }}" class="gallery-image item">
                                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->image10 }}" alt="{{ $mall->name }}" class="carousel-item" />
                                    </a>
                                @endif
                            </div>
                        </div>
                        <!--/my carousel-->

                        <div>
                            @if(!empty($mall->centreManagerTelephone))
                                <?php
                                $tel = str_replace('+27','0', $mall->centreManagerTelephone );
                                $tel = str_replace('0 ','0', $tel );
                                $tel2 = str_replace(' ','', $tel );
                                $tel2 = str_replace('(','', $tel2 );
                                $tel2 = str_replace(')','', $tel2 );
                                ?>

                                <i class="fa fa-phone-square fa-lg"></i>&nbsp;
                                <a class="tel" href="tel:{{ $tel2 }}">{{ $tel }}</a>
                            @endif
                        </div>

                        <?php $website = removeURLProtocol($mall->website); ?>
                        @if(!empty($mall->website))
                            <a href="http://{{ $website }}" target="_blank">{{ $website }}</a>
                        @endif

                    </div>
                </div>
            </div>

            <!--span3-->

            <div class="span3">
                <div class="blog-content border_top">
                    <div class="mall-caption">
                        <div class="hborder"><h4>At the Mall</h4></div>
                        {{--@if(isset($weather_info))--}}
                            {{--<div id="weatherDiv" class="ui-corner-all">--}}
                                {{--<div id="weatherIcon">--}}
                                    {{--<img src="/img/weather/{{ $weather_info->currently->icon }}.png" />--}}
                                {{--</div>--}}
                                {{--<div id="weatherTextHolder">--}}
                                    {{--<div id="weatherText">{{ intval($weather_info->currently->temperature) }}&deg;</div>--}}
                                {{--</div>--}}
                                {{--<div class="clearfix"></div>--}}
                            {{--</div>--}}
                        {{--@endif--}}

                        <ul>
                            <li><a href="#mallShops">{{ $listedShops->total() }} shops.</a> </li>
                            <li>
                                <a href="/events/mall/{{ $mall->mallID }}" class="dark-link">
                                    @if($mall->curEvents != null) {{ $mall->curEvents->count() }} @else 0 @endif events.
                                </a>
                                @if($mall->curEvents != null)
                                    @if($mall->curEvents->count() > 0)
                                        <a href="/events/mall/{{ $mall->mallID }}">View</a>
                                    @endif
                                @endif
                            </li>
                            <li>
                                <a href="/competitions/mall/{{ $mall->mallID }}" class="dark-link">
                                    @if($mall->curCompetitions != null)  {{ $mall->curCompetitions->count() }} @else 0 @endif competitions.
                                </a>
                                @if($mall->curCompetitions != null)
                                    @if($mall->curCompetitions->count())
                                        <a href="/competitions/mall/{{ $mall->mallID }}">Enter Now
                                        </a>
                                    @endif
                                @endif
                            </li>
                            <li>
                                <a href="/jobs/mall/{{ $mall->mallID }}" class="dark-link">
                                    @if($mall->curJob != null) {{ $mall->curJobs->count() }} @else 0 @endif job vacancies.
                                </a>
                                @if($mall->curJobs != null)
                                    @if($mall->curJobs->count())
                                        <a href="/jobs/mall/{{ $mall->mallID }}">Apply Now</a>
                                    @endif
                                @endif
                            </li>
                            <li>
                                <a href="/promotions/mall/{{ $mall->mallID }}" class="dark-link">
                                    @if($mall->curPromotions != null) {{ $mall->curPromotions->count() }} @else 0 @endif promotions.
                                </a>
                                @if($mall->promotions != null)
                                    @if($mall->promotions->count())
                                        <a href="/promotions/mall/{{ $mall->mallID }}">View</a>
                                    @endif
                                @endif
                            </li>
                            <li>
                                <a href="/movies/mallmovies/{{ $mall->mallID }}" class="dark-link">
                                    @if($mall->whatsShowing != null) {{ $mall->whatsShowing->count() }} @else 0 @endif movies.
                                </a>
                                @if($mall->whatsShowing != null)
                                    @if($mall->whatsShowing->count())
                                        <a href="/movies/mallmovies/{{ $mall->mallID }}">Write a review</a>
                                    @endif
                                @endif
                            </li>
                            <li>
                                <a href="/exhibitions/mall/{{ $mall->mallID }}" class="dark-link">
                                    @if($mall->exhibitions != null) {{ $mall->exhibitions->count() }}  @else 0 @endif exhibitions.
                                </a>
                                @if($mall->exhibitions != null)
                                    @if($mall->exhibitions->count())
                                        <a href="/exhibitions/mall/{{ $mall->mallID }}">View</a>
                                    @endif
                                @endif
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="span3">

                <div class="blog-content border_top">
                    <div class="mall-caption">
                        <div class="hborder"><h4>Info</h4></div>
                        @if($mall->tradinghour)
                            @if($mall->tradinghour->startHoursMon != '' && $mall->tradinghour->endHoursMon != '')
                                <div class="mall-trading-hours">
                                    <table class="table table-condensed table-hover" cellpadding="0" cellspacing="0">
                                        <thead>
                                        <th colspan="2" class="capital-uppercase">{{ $mall->name }} Trading Hours</th>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>Monday - Friday</td>
                                            <td>{{ $mall->tradinghour->startHoursMon }} - {{ $mall->tradinghour->endHoursMon }}</td>
                                        </tr>
                                        @if($mall->tradinghour->startHoursSat != '' && $mall->tradinghour->endHoursSat != '')
                                            <tr>
                                                <td>Saturday</td>
                                                <td>{{ $mall->tradinghour->startHoursSat }} - {{ $mall->tradinghour->endHoursSat }}</td>
                                            </tr>
                                        @endif
                                        @if($mall->tradinghour->startHoursSun != '' && $mall->tradinghour->endHoursSun != '')
                                            <tr>
                                                <td>Sunday</td>
                                                <td>{{ $mall->tradinghour->startHoursSun }} - {{ $mall->tradinghour->endHoursSun }}</td>
                                            </tr>
                                        @endif
                                        @if($mall->tradinghour->startHoursPublicHolidays != '' && $mall->tradinghour->endHoursPublicHolidays != '')
                                            <tr>
                                                <td>Public Holidays</td>
                                                <td>{{ $mall->tradinghour->startHoursPublicHolidays }} - {{ $mall->tradinghour->endHoursPublicHolidays }}</td>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            @endif
                        @endif
                        <div class="clearfix"></div>
                        @if($mall->openParkingBays || $mall->totalParkingBays)
                            @if($mall->openParkingBays)
                                <div>
                                    <table class="table table-condensed table-hover">
                                        <tr>
                                            <td class="text-thirteen text-center"><strong>Open Parking:</strong></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                {{ $mall->openParkingBays }} bays
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            @endif
                            @if($mall->totalParkingBays)
                                <div>
                                    <table class="table table-condensed table-hover">
                                        <tr>
                                            <td class="text-thirteen text-center"><strong>Total Parking:</strong></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">
                                                {{ $mall->totalParkingBays }} bays
                                            </td>
                                        </tr>
                                    </table>

                                </div>
                            @endif
                        @endif

                        <dl class="no-margin">
                            <dt>Classification</dt>
                            <dd>{{ $mall->classification }}</dd>
                        </dl>
                        <dl class="no-margin">
                            @if($mall->glaRetailM2)
                                <dt>GLA Retail (m<sup>2</sup>)</dt>
                                <dd>{{ $mall->glaRetailM2 }}</dd>
                            @endif
                            <a name="mallShops"></a>
                        </dl>
                        @if($mall->anchorTenants)
                            <dl class="no-margin">
                                <dt>
                                    Anchor Tenants</dt>
                                <dd>{{ $mall->anchorTenants }}</dd>
                            </dl>
                        @endif
                    </div>
                </div>
                <!-- caption -->
            </div>

            <div class="span3">
                <div class='blog-content border_top'>
                    <div class="mall-caption">
                        <div id="map_canvas" style="height:300px;frameborder:0px;"></div>

                        <div class="mall-info">
                            @if(isset($mall->physicalAddress) && $mall->physicalAddress != '')
                                <strong>Physical Address:</strong>
                                <address>{{ $mall->physicalAddress }}</address>
                            @endif
                            @if(isset($mall->postalAddress) && $mall->postalAddress != '')
                                <strong>Postal Address:</strong>
                                <address>{{ $mall->postalAddress }}</address>
                            @endif


                            <dl>
                                <dt>
                                    @if(isset($mall_city))<strong>City: </strong>  <a href="/malls?city={{ slugify($mall_city['name']) }}-{{ $mall_city['id'] }}">{{ $mall_city['name'] }}</a> @endif<br/>
                                    @if(isset($mall->province_id) && isset($provinces[$mall->province_id]))<strong>Province: </strong>  <a href="/malls/provinceID/{{ $mall->province_id }}">{{ $provinces[$mall->province_id] }}</a> @endif
                                </dt>
                            </dl>

                            <div class="text-right">
                                last updated: @if($mall->lastUpdate != '0000-00-00 00:00:00'){{ date('d/m/Y', strtotime($mall->lastUpdate)) }} @else never @endif
                            </div>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </div>
        </div>

        <!--span3-->
    </div>
    <div class="clearfix">&nbsp;</div>
    <!-- Mall details -->
    <ul class="nav nav-tabs" id="mallTabs">
        @if($mall->mallDescription != '')
            <li class="active mall-tabs"><a href="#mallDescription" data-toggle="tab">Description</a></li>
        @endif
        @if(isset($mall->details))
            @if(trim($mall->details->management) != '')
                <li class="mall-tabs"><a href="#centreManagement" data-toggle="tab">Centre Management</a></li>
            @endif
            @if(trim($mall->details->marketing) != '')
                <li class="mall-tabs"><a href="#mallMarketing" data-toggle="tab">Marketing</a></li>
            @endif
            @if(trim($mall->details->security) != '')
                <li class="mall-tabs"><a href="#mallParking" data-toggle="tab">Parking &amp; Security</a></li>
            @endif
            @if(trim($mall->details->facilities) != '')
                <li class="mall-tabs"><a href="#mallFacilities" data-toggle="tab">Unique Facilities</a></li>
            @endif
            @if(trim($mall->details->leasing) != '')
                <li class="mall-tabs"><a href="#mallLeasing" data-toggle="tab">Leasing</a></li>
            @endif
            @if(trim($mall->details->giftcards) != '')
                <li class="mall-tabs"><a href="#mallGiftCards" data-toggle="tab">Gift Cards</a></li>
            @endif
            @if(trim($mall->details->tourism) != '')
                <li class="mall-tabs"><a href="#mallTourism" data-toggle="tab">Tourism</a></li>
            @endif
            @if(trim($mall->details->contact) != '')
                <li class="mall-tabs"><a href="#mallContact" data-toggle="tab">Contact</a></li>
            @endif
        @endif
    </ul>

    <div class="tab-content">
        @if($mall->mallDescription != '')
            <div class="tab-pane active" id="mallDescription">{{ $mall->mallDescription }}</div>
        @endif
        @if(isset($mall->details))
            @if(trim($mall->details->management) != '')
                <div class="tab-pane" id="centreManagement">{{ $mall->details->management }}</div>
            @endif
            @if(trim($mall->details->marketing) != '')
                <div class="tab-pane" id="mallMarketing">{{ $mall->details->marketing }}</div>
            @endif
            @if(trim($mall->details->security) != '')
                <div class="tab-pane" id="mallParking">{{ $mall->details->security }}</div>
            @endif
            @if(trim($mall->details->facilities) != '')
                <div class="tab-pane" id="mallFacilities">{{ $mall->details->facilities }}</div>
            @endif
            @if(trim($mall->details->leasing) != '')
                <div class="tab-pane" id="mallLeasing">{{ $mall->details->leasing }}</div>
            @endif
            @if(trim($mall->details->giftcards) != '')
                <div class="tab-pane" id="mallGiftCards">{{ $mall->details->giftcards }}</div>
            @endif
            @if(trim($mall->details->tourism) != '')
                <div class="tab-pane" id="mallTourism">{{ $mall->details->tourism }}</div>
            @endif
            @if(trim($mall->details->contact) != '')
                <div class="tab-pane" id="mallContact">{{ $mall->details->contact }}</div>
            @endif
        @endif
    </div>
    <!-- Mall details -->
    <div class="clearfix">&nbsp;</div>

    <div class="row-fluid">
        <div class="span12">
            <h4 class="hborder">Listed Stores</h4>

            @if( sizeof($listedShops) > 0 )
                {{--@if($alpha != '')--}}
                @include('partials.alphasorter')
                {{--@endif--}}
                <table class="table table-border">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>
                            <span class="hidden-phone">Name</span>
                            <span class="hidden-desktop">Shop</span>
                        </th>
                        <th class="hidden-phone">Category</th>
                        <th class="hidden-phone">Shop Number</th>
                        <th class="hidden-phone">&nbsp;</th>
                        <th><span class="hidden-phone">Contact Number</span></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($listedShops as $shop)
                        <tr>
                            <td>
                                @if(!empty($shop->logo))
                                    <?php $logo = 'uploadimages/mall_' . $mall->mallID . '/' . $shop->logo ?>
                                @else
                                    <?php $logo = 'img/nomalllogo.gif'; ?>
                                @endif
                                <img class="img-responsive" width="100" src="{{ Config::get('app.url') }}/{{ $logo }}" alt="{{ $shop->name }}" />
                            </td>
                            <td>
                                <a href='{{ route('shops.view_shop',array($shop->shopID,slugify($mall->name),slugify($shop->name) )) }}'>{{ $shop->name }}</a><br>
                                <span class="hidden-desktop">{{ $shop->category }}</span>
                                <span class="hidden-desktop">&nbsp;/&nbsp;{{ $shop->shopNumber }}</span>
                            </td>
                            <td class="hidden-phone">{{ $shop->category }}</td>
                            <td class="hidden-phone text-center">@if($shop->shopNumber) {{ $shop->shopNumber }} @else N/A @endif</td>
                            <td class="hidden-phone">
                                @if($shop->mapX && $shop->mapY)
                                    <a href="{{ route('shop.mapImage',array($shop->shopID)) }}" data-x-pos="{{ $shop->mapX }}" data-y-pos="{{ $shop->mapY }}" data-fancybox-title="{{ $shop->name }} @ {{ $mall->name }}" class="shop-map">
                                        <i class="fa fa-map-marker fa-lg fa-fw"></i>
                                    </a>
                                @endif
                            </td>
                            <td>
                                {{--<div class="text-center hidden-desktop">--}}
                                @if($shop->mapX && $shop->mapY)
                                    <a href="{{ route('shop.mapImage',array($shop->shopID)) }}" data-x-pos="{{ $shop->mapX }}" data-y-pos="{{ $shop->mapY }}"  class="shop-map hidden-desktop">
                                        <i class="fa fa-map-marker fa-lg fa-fw"></i>
                                    </a>
                                    <div class="clearfix"></div>
                                @endif
                                {{--</div>--}}
                                @if($shop->telephone)
                                    <a href="tel:{{ $shop->telephone }}" class="hidden-phone">{{ $shop->telephone }}</a>
                                    <a class="btn btn-inverse btn-small hidden-desktop" href="tel:{{ $shop->telephone }}">Call</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            @else
                <div class="clearfix">&nbsp;</div>
                <p>&nbsp;</p>
                <div class="text-center">
                    @if($alpha != '')
                        There are no shops listed in this mall for the selected criteria.
                    @else
                        There are no shops listed in this mall.
                    @endif
                </div>
            @endif
            <p class="legend">

            <div class="pagination"><?php echo $listedShops->appends(array('alpha' => request()->get('alpha')))->links(); ?></div>
            </p>

        </div>
        <!--span12 -->
    </div><!--row-fluid-->

    <div class="row-fluid">
        <div class="span12">
            <div class="text-right">
                <button class="btn btn-inverse go_back">&laquo; Go back</button>
            </div>
        </div>
    </div>

@stop

@section('exScript')
    <script src="{{ asset('https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("a.shop-map").fancybox({
                type:'image',
                afterLoad: function(current, previous){
                    var _mapHolder = $('.fancybox-inner');
                    // shop location marker
                    var _callerEl = $(this.element);
                    var _pointer = $('<div></div>').html('<span id="mapPointer"><img src="{{ asset('img/dot_03.gif') }}"/></span>');

                    _mapHolder.html(_mapHolder.html()+_pointer.html());
                    $('#mapPointer').attr('style', 'position: absolute; left: '+_callerEl.attr('data-x-pos')+'px; top: '+_callerEl.attr('data-y-pos')+'px');
                }
            });

            initGMap('<?php echo $mall->mapX; ?>', '<?php echo $mall->mapY; ?>', 'map_canvas');
//        getWeather();

            $('.zoom-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: true,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function(element) {
                        return element.find('img');
                    }
                }

            });

            $("#provinces").change(function () {
                province = $(this).val();

                $.ajax({
                    url: '../provinceMalls/' + province,
                    data: {},
                    dataType: 'json',
                    success: function () {

                        window.location = '../province/' + province;
                    }

                });
            });


            $('.carousel').carousel({interval: 5000});
            $('#mallTabs').tab();

        });


        function getWeather()
        {
            var url = "https://api.forecast.io/forecast/08ee3f806b2b29847a9a86a86f1b5d02/{{ $mall->mapX }},{{ $mall->mapY }}";
            $.ajax({
                url: url,
                success: function(data){

                }
            });
        }
    </script>
@stop
