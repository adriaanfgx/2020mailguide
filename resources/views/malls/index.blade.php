@extends('layouts.pages')

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">

        <div class="container-fluid content_img">
            <div class="row-fluid">
                @include('partials.headers.search')
                <div class="span12">
                    <h1 class="animated fadeInDown delay1"><span>Malls: </span>Shopping<span> Delight</span> </h1>
                    <p class="animated fadeInDown delay2">Find listed malls</p>
                </div><!--/span12-->

            </div><!--/row-->
            <div class="row">
                <div class="span12">
                    <div id="breadcrumbs" >
                        {{ generateBreadcrumbs() }}
                    </div><!--/span6-->
                </div>
            </div>
        </div><!--/container-->
    </div><!--/three-->
@stop

@section('content')

    <div class="row-fluid">

        <div class="hborder">
            <h4 class="paddingBottom">Malls</h4>
            <div class="row-fluid">
                <div class="span3 offset5">
                    <div class="overlay-wrapper"></div>

                    <select name="provinces" id="provinces">
                        <option value="">Please Select a Province...</option>
                        @foreach($provinces as $key=>$value)
                            <option value="{{ $key }}" @if(isset($id) && $id == $key)selected="selected"@endif>{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="span3">
                    <div class="overlay-wrapper"></div>
                    <div class="input-append">
                        <input type="text"   name="Search" value="" id="search_string" placeholder="Keyword Search" />
                        <button type="submit" id="search" class="btn btn-info" id="shopsearch"><i class="icon-search"></i></button>
                    </div>
                </div>
                @include('partials.alphasorter')
            </div>
        </div>

        <!-- span6 -->

        <div class="clear"></div>
        @if((isset($city_id)) && ($city_id && $province_id))
            <div class="alert alert-info">
                Browsing malls in {{ $city }}, <a href="/malls/provinceID/{{ $province_id }}" class="dark-link">{{ $province }}</a>
            </div>
        @endif
        <div class="clear"></div>
        <div class="pull-right span3">
            Show
            <select name="items_per_page" id="slt_items_per_page" class="span4">
                <?php
                $items_counter = range(50, 250, 50);
                foreach($items_counter as $item_count){
                    echo('<option value="'.$item_count.'"');
                    if(isset($items_per_page) && $item_count == $items_per_page) echo ' selected="selected"';
                    echo('>'.$item_count.'</option>');
                }
                ?>
            </select>
            records per page
        </div>
        <div class="clear"></div>


        <div class="row-fluid">
            <table class="table table-border table-striped responsive malls-table">
                <thead>
                <tr>
                    <th width="100">&nbsp;</th>
                    <th>Mall</th>
                    <th width="" class="hidden-phone">Area</th>
                    <th width="">&nbsp;</th>
                    <th width="">&nbsp;</th>
                    <th width="" class="hidden-phone">No of Shops</th>
                    <th width="">&nbsp;</th>
                </tr>
                </thead>
                <tbody id="mall_table">
                <tr>
                    <td colspan="7">Please select a province first...</td>
                </tr>
                </tbody>
            </table>



        </div>

    </div>
@stop
@section('exScript')
    <script type="text/javascript">
        var _alpha = '<?php echo request()->get('alpha'); ?>';
        var _city = '<?php echo request()->get('city'); ?>';
        $(document).ready(function()
        {

            if( $("#provinces").val() != '' ){
                var url="";
                getMalls($("#provinces").val(),url);
            }

            $("#slt_items_per_page").change(function()
            {
                var url="/malls";
                getMalls('',url);
            });

            $("#provinces").change(function()
            {
                var url="";
                getMalls($(this).val(),url);
            });

            if($("#provinces").val())
            {
                getMalls($("#provinces").val(),"");
            } else {
                getMalls('', '');
            }

            $("#search_string").keypress(function(e){

                if (e.keyCode == 13){
                    findMalls("");
                }
            });

            $("#search").click(function(){
                findMalls("");
            });

            $('.alpha-sorter a').on('click', function(e){
                e.preventDefault();
                var _dst = $(this).attr('href');
                if(_dst){
                    _dst += '&per_page=' + $("#slt_items_per_page").val();
                }
                window.location = '/malls/' + _dst;
                //return false;
            })
        });

        function getMalls(province, pageURL){
            $("#mall_table").empty().html('<tr><td colspan="7">Loading Malls....</td></tr>');

            var ajaxURL="";
            if(pageURL=="")
            {
                ajaxURL= '/malls/province-malls/' + province;
            }
            else
            {
                ajaxURL=pageURL;
            }

            /*if( province == '')
            {
                $("#mall_table").empty().html('<tr><td colspan="7">Please select a province first...</td></tr>');

            }
            else
            {*/
            //does the querystring already has alpha in it?
            if(ajaxURL.indexOf('alpha=') == -1){
                ajaxURL += '?alpha='+_alpha;
            }

            if(ajaxURL.indexOf('city=') == -1){
                ajaxURL += '&city='+_city;
            }

            ajaxURL += '&per_page='+$('#slt_items_per_page').val();

            $.ajax({
                url: ajaxURL,
                dataType: 'html',
                success: function(data)
                {
                    $("#mall_table").empty().append(data);
                    paginationFix();
                }

            });
//        }

        }

        function paginationFix()
        {
            $('.pagination a').click( function (evt)
            {
                //alert('please');
                evt.preventDefault();
                var page_url = $(this).attr('href');
//			console.log(page_url);
                getMalls($("#provinces").val(), page_url);
            });
        }
        function searchPaginationFix()
        {
            $('.pagination a').click( function (evt)
            {
                //alert('please');
                evt.preventDefault();
                var page_url = $(this).attr('href');
                findMalls(page_url);
            });
        }
        function findMalls(pageURL)
        {
            if( $("#search_string").val() != ''){
                var url="";
                if(pageURL=="")
                {
                    var string = $("#search_string").val();
                    var url = '/malls/search/'+string;
                }
                else
                {
                    url=pageURL;
                }

                url += '/?&per_page='+$('#slt_items_per_page').val();

                $("#mall_table").empty();
                $.ajax({
                    url: url,
                    data: '',
                    success: function(data)
                    {
                        //alert("hi");
                        $("#mall_table").empty().append(data);
                        searchPaginationFix();
                    }
                });
            }
        }
    </script>
@stop

