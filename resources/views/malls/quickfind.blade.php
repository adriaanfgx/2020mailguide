@extends('layouts.pages')

@section('title')
    @parent
    Mall Search Results
@stop


@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                @include('partials.headers.search')
                <div class="span6">
                    <h1 class="animated fadeInDown delay1">Mall Search <span>Results</span></h1>

                    <p class="animated fadeInDown delay2">Search for Malls, Shops or Movies</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a> <span class="divider">/</span></li>
                        <li class="active">Mall Search</li>
                    </ul>
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->
@stop

@section('content')
    <div class="row-fluid hidden-phone" id="filterSection_menu">
        <div class="span12"><!--sort wrap -->
        </div>
        <!--span12 -->
    </div><!--row -->

    <div data-perrow="4" class="row-fluid">


        <div class="row-fluid">
            <div class="span12">
                <?php
                $total_results = $malls->total();
                ?>
                @if( $total_results > 0 )

                    <h4 class="hborder">Mall Matches</h4>
                    <div class="alert alert-info">
                        @if($total_results == 1)
                            Your search returned 1 result.
                        @else
                            Showing results.
                        @endif
                    </div>
                    <table class="table table-border">
                        <thead>
                        <tr>

                            <th width="100">&nbsp;</th>
                            <th>Mall</th>
                            <th width="" class="hidden-phone">Area</th>
                            <th width="">&nbsp;</th>
                            <th width="">&nbsp;</th>
                            <th width="" class="hidden-phone">No of Shops</th>
                            <th width="">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody id="malls">
                        @foreach( $malls as $mall )
                            <tr>
                                <td>
                                    @if(!empty($mall->logo))
                                        <?php $logo = 'uploadimages/mall_' . $mall->mallID . '/' . $mall->logo ?>
                                    @else
                                        <?php $logo = 'img/nomalllogo.gif'; ?>
                                    @endif
                                    <img class="img-responsive" width="100" src="{{ Config::get('app.url') }}/{{ $logo }}" alt="{{ $mall->name }}" />
                                </td>
                                <td class="vertical-middle">
                                    @if(isset($mall->mallCity->id))
                                        <a href="/malls/view/{{ $mall->mallID }}/{{ slugify($mall->name) }}">{{ $mall->name }}</a><br/>
                                        <span class="hidden-desktop">{{ $mall->mallCity->name }}</span>
                                    @else
                                        <a href="/malls/view/{{ $mall->mallID }}/{{slugify($mall->name)}}">{{ $mall->name }}</a><br/>
                                    @endif
                                </td>
                                <td class="hidden-phone vertical-middle">
                                    @if(isset($mall->mallCity->id))
                                        <a href="/malls?city=<?php echo $mall->mallCity->id; ?>-<?php echo slugify($mall->mallCity->name); ?>" class="dark-link">
                                            {{ $mall->mallCity->name }}
                                        </a>
                                    @endif

                                </td>

                                <td class="vertical-middle">
                                    @if(!empty($mall->centreManagerTelephone))
                                        <?php
                                        $tel = str_replace('+27','0', $mall->centreManagerTelephone );
                                        $tel = str_replace('0 ','0', $tel );
                                        $tel2 = str_replace(' ','', $tel );
                                        $tel2 = str_replace('(','', $tel2 );
                                        $tel2 = str_replace(')','', $tel2 );
                                        ?>

                                        <i class="fa fa-phone-square fa-lg hidden-phone"></i>&nbsp;
                                        <a class="tel hidden-phone" href="tel:{{ $tel2 }}">{{ $tel }}</a>
                                        <a class="btn btn-inverse btn-small hidden-desktop" href="tel:{{ $tel2 }}">Call</a>
                                    @endif
                                </td>
                                <td class="vertical-middle">
                                    <a href="http://maps.google.com/?ie=UTF&hq=&ll={{ $mall->mapX }},{{ $mall->mapY }}&z=13" target="_blank">
                                        <i class="fa fa-map-marker fa-lg fa-fw"></i>
                                    </a>
                                </td>
                                <?php

                                $count = $mall->shops->count();
                                ?>
                                <td style="overflow:hidden;" class="hidden-phone vertical-middle">{{ ($count == 0) ? '0' : $count }}</td>
                                <td class="vertical-middle">
                                    @if($count != 0)
                                        <a href="/malls/view/{{ $mall->mallID }}/{{ slugify($mall->name) }}#mallShops" class="btn btn-mini btn-info">View Shops</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $malls->links() }}
                @else
                    <div class="alert alert-info">Sorry, there are no matches for your search...</div>
                @endif

            </div>
            <!--span12 -->
        </div>
    </div>
@stop

