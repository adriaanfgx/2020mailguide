@extends('layouts.pages')

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Malls - </span>Get listed </h1>
                    <p class="animated fadeInDown delay2">Register your mall on mallguide</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
@stop

@section('content')
    <div class="row-fluid">
        <div class="span12 blog-content">
            <h3 class="dotted-border"> Malls - Get Listed</h3>
            <div class="row-fluid">
                <div class="span6">
                    Mallguide offers the local and international visitor a free resource portal,
                    to overview everything current in retail shopping. With this free tool,
                    you will be able to manage your tenants, promotions, events,
                    etc and also drive higher volumes of traffic to your site and premise...
                </div>
                <div class="well-small well span6">
                    <h6>Want a slice of the action ?</h6>
                    Please complete the registration form. Mallguide will notify you once your listing has been
                    approved.
                </div>
            </div>
            {{ Form::open(array('route' => 'malls.postRegister', 'method' => 'post','files'=> true,'name' => 'mallSignUp','id' => 'mallSignUp', 'class' => 'form-inline mallSignUp')) }}
                <div class="row-fluid">
                    <div class="span12">
                        <h4 class="dotted-border">Login Info</h4>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span5">
                        <div class="control-group {{ ($errors->has('admin_level') ? 'error' : '') }}">
                            <label for="classification"><strong>You are the?*</strong></label>
                            <div class="controls">
                                {{ Form::select('admin_level',$admin_level,null,array('class'=>'txtbar')) }}
                            </div>
                            <span class="help-block">{{ ($errors->has('admin_level') ? $errors->first('admin_level') : '') }}</span>
                        </div>
                    </div>
                    <div class="span5">
                        <br/>
                        All info will be overseen before account will be made active.</a>
                    </div>
                </div>
                @include('/partials/minLoginInfo')
                <div class="row-fluid">
                    <div class="span12">
                        <h4 class="dotted-border">Mall Info</h4>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('name') ? 'error' : '') }}">
                            <label for="name" class="control-label"><strong>Mall Name *</strong></label>
                            <div class="controls">
                                {{ Form::text('name',null,array('class'=>'txtbar')) }}
                            </div>
                            <span class="help-block">{{ ($errors->has('name') ? $errors->first('name') : '') }}</span>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="classification"><strong>Classification List</strong></label>
                            <div class="controls">
                                {{ Form::select('classification',$classification,null) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <div class="control-group">
                            <label for="classification"><strong>Mall description</strong></label>
                            <div class="controls">
                                {{ Form::textarea('classification',null,array('class'=>'txtbar','rows'=>3)) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="physicalAddress"><strong>Physical Address</strong></label>
                            <div class="controls">
                                {{ Form::textarea('physicalAddress',null,array('rows'=>'3')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="province"><strong>Province List</strong></label>
                            <div class="controls">
                                {{ Form::select('province', $provinces, null) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="city"><strong>City List</strong></label>
                            <div class="controls">
                                {{ Form::select('city', $cities, null) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">

                    <div class="span4">
                        <div class="control-group">
                            <label for="postalAddress"><strong>Postal Address</strong></label>
                            <div class="controls">
                                {{ Form::textarea('postalAddress',null,array('rows'=>'3')) }}
                            </div>
                        </div>
                    </div>

                    <div class="span4">
                        <div class="control-group">
                            <label for="website"><strong>Website</strong></label>
                            <div class="controls">
                                {{ Form::text('website',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>

                    <div class="span2">
                        <div class="control-group">
                            <label for="numShops"><strong>Nr. of shops</strong></label>
                            <div class="controls">
                                {{ Form::text('numShops',null,array('class'=>'txtbar span8')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span2">
                        <div class="control-group">
                            <label for="glaRetailM2"><strong>GLA Retail (m2)</strong></label>
                            <div class="controls">
                                {{ Form::text('glaRetailM2',null,array('class'=>'txtbar span8')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span2">
                        <div class="control-group">
                            <label for="openParkingBays"><strong>Nr. Open Parking bays</strong></label>
                            <div class="controls">
                                {{ Form::text('openParkingBays',null,array('class'=>'txtbar span8')) }}
                            </div>
                        </div>
                    </div>


                    <div class="span2">
                        <div class="control-group">
                            <label for="coveredParkingBays"><strong>Nr. Covered Parking bays</strong></label>
                            <div class="controls">
                                {{ Form::text('coveredParkingBays',null,array('class'=>'txtbar span8')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span2">
                        <div class="control-group">
                            <label for="totalParkingBays"><strong>Total Parking bays</strong></label>
                            <div class="controls">
                                {{ Form::text('totalParkingBays',null,array('class'=>'txtbar span8')) }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span8">
                        <div class="control-group">
                            <label for="anchorTenants"><strong>Anchor Tenants</strong></label>
                            <div class="controls">
                                {{ Form::text('anchorTenants',null) }}
                            </div>
                        </div>
                    </div>


                    <div class="span2">
                    </div>

                </div>
                <div class="row" style="padding-left: 30px;">

                </div>

                <div class="row-fluid">
                <span class="well-small well span12"> Only jpg, jpeg, gif or png files. Smaller than 1 Mb.</span>
        </div>
        <div class="row-fluid">
        <div class="span3">
            <div class="control-group {{ ($errors->has('logo') ? 'error' : '') }}">
                <label for="logo"><strong>Logo</strong></label>
                <div class="controls">
                    {{ Form::file('logo') }}<br />
                    <span class="help-block">{{ ($errors->has('logo') ? $errors->first('logo') : '') }}</span>
                </div>
            </div>
        </div>
        <div class="span3">
            <div class="control-group {{ ($errors->has('image2') ? 'error' : '') }}">
                <label for="image2"><strong>Mall Image 2</strong> </label><br/>
                <div class="controls">
                    {{ Form::file('image2') }}<br />
                    <span class="help-block">{{ ($errors->has('image2') ? $errors->first('image2') : '') }}</span>
                </div>
            </div>
        </div>


        <div class="span3">

            <div class="control-group {{ ($errors->has('image1') ? 'error' : '') }}">
                <label for="image1"><strong>Mall Image 1</strong></label><br/>
                <div class="controls">
                    {{ Form::file('image1') }}
                    <span class="help-block">{{ ($errors->has('image1') ? $errors->first('image1') : '') }}</span>
                </div>
            </div>
        </div>
        <div class="span3">

            <div class="control-group {{ ($errors->has('image3') ? 'error' : '') }}">
                <label for="image3"><strong>Mall Image 3</strong></label><br/>
                <div class="controls">
                    {{ Form::file('image3') }}
                    <span class="help-block">{{ ($errors->has('image3') ? $errors->first('image3') : '') }}</span>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="padding-left: 30px;">
        <div class="span12">
            <h4 class="dotted-border">Special Features &nbsp;<small><strong>(eg. Ice rink, cinemas)</strong></small>
            </h4>
        </div>
    </div>

    <div class="row" style="padding-left: 30px;">
        <div class="span4">
            <div class="control-group">
                <label for="specialFeature1"><strong>Special Feature 1</strong></label>
                <div class="controls">
                    {{ Form::text('specialFeature1',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="specialFeature2"><strong>Special Feature 2</strong></label>
                <div class="controls">
                    {{ Form::text('specialFeature2',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="specialFeature3"><strong>Special Feature 3</strong></label>
                <div class="controls">
                    {{ Form::text('specialFeature3',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding-left: 30px;">

        <div class="span4">
            <div class="control-group">
                <label for="specialFeature4"><strong>Special Feature 4</strong></label>
                <div class="controls">
                    {{ Form::text('specialFeature4',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="specialFeature5"><strong>Special Feature 5</strong></label>
                <div class="controls">
                    {{ Form::text('specialFeature5',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="specialFeature6"><strong>Special Feature 6</strong></label>
                <div class="controls">
                    {{ Form::text('specialFeature6',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="padding-left: 30px;">
        <div class="span4">
            <div class="control-group">
                <label for="specialFeature7"><strong>Special Feature 7</strong></label>
                <div class="controls">
                    {{ Form::text('specialFeature7',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="specialFeature8"><strong>Special Feature 8</strong></label>
                <div class="controls">
                    {{ Form::text('specialFeature8',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="specialFeature9"><strong>Special Feature 9</strong></label>
                <div class="controls">
                    {{ Form::text('specialFeature9',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="padding-left: 30px;">

        <div class="span4">
            <div class="control-group">
                <label for="specialFeature10"><strong>Special Feature 10</strong></label>
                <div class="controls">
                    {{ Form::text('specialFeature10',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="padding-left: 30px;">
        <div class="span12">
            <h4 class="dotted-border"><strong>Centre Management</strong></small></h4>
        </div>
    </div>
    <div class="row" style="padding-left: 30px;">
        <div class="span4">
            <div class="control-group">
                <label for="centreManager"><strong>Center Manager</strong></label>
                <div class="controls">
                    {{ Form::text('centreManager',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="centreManagerTelephone"><strong>Tel</strong></label>
                <div class="controls">
                    {{ Form::text('centreManagerTelephone',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="centreManagerCell"><strong>Cell</strong></label>
                <div class="controls">
                    {{ Form::text('centreManagerCell',null,array('class'=>'txtbar numeric')) }}
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding-left: 30px;">
        <div class="span4">
            <div class="control-group {{ ($errors->has('centreManagerEmail') ? 'error' : '') }}">
                <label for="centreManagerEmail"><strong>Email</strong></label>
                <div class="controls">
                    {{ Form::text('centreManagerEmail',null,array('class'=>'txtbar')) }}
                    {{ ($errors->has('centreManagerEmail') ? $errors->first('centreManagerEmail') : '') }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="centreManagerFax"><strong>Fax</strong></label>
                <div class="controls">
                    {{ Form::text('centreManagerFax',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>

    </div>
    <div class="row" style="padding-left: 30px;">
        <div class="span12">
            <h4 class="dotted-border"><strong>Marketing Consultant</strong></small></h4>
        </div>
    </div>
    <div class="row" style="padding-left: 30px;">
        <div class="span4">
            <div class="control-group">
                <label for="marketingConsultant"><strong>Marketing Company Name</strong></label>
                <div class="controls">
                    {{ Form::text('marketingConsultant',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>

    </div>
    <div class="row" style="padding-left: 30px;">
        <div class="span4">
            <div class="control-group">
                <label for="marketingCell"><strong>Cell</strong></label>
                <div class="controls">
                    {{ Form::text('marketingCell',null,array('class'=>'txtbar numeric')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="marketingTelephone"><strong>Tel</strong></label>
                <div class="controls">
                    {{ Form::text('marketingTelephone',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="marketingFax"><strong>Fax</strong></label>
                <div class="controls">
                    {{ Form::text('marketingFax',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding-left: 30px;">
        <div class="span4">
            <div class="control-group">
                <label for="marketingManager"><strong>Marketing Manager</strong></label>
                <div class="controls">
                    {{ Form::text('marketingManager',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group {{ ($errors->has('marketingManagerEmail') ? 'error' : '') }}">
                <label for="marketingManagerEmail"><strong>Marketing Manager Email Address</strong></label>
                <div class="controls">
                    <input class="txtbar" name="marketingManagerEmail" type="text">
                    {{ ($errors->has('marketingManagerEmail') ? $errors->first('marketingManagerEmail') : '') }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group {{ ($errors->has('marketingContractStart') ? 'error' : '') }}">
                <label for="marketingContractStart" class="control-label"><strong>Contract Start Date</strong></label>
                <div class="controls">
                    <div class="input-append datetimepicker4">
                        {{ Form::text('marketingContractStart',null,array('data-format'=>'yyyy-MM-dd','readonly')) }}
                        <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                          </i>
                        </span>
                    </div>
                </div>
                <span class="help-block">{{ ($errors->has('marketingContractStart') ? $errors->first('marketingContractStart') : '') }}</span>
            </div>
        </div>
    </div>
    <div class="row" style="padding-left: 30px;">
        <div class="span4">
            <div class="control-group">
                <label for="marketingAssistant"><strong>Marketing Assistant</strong></label>
                <div class="controls">
                    {{ Form::text('marketingAssistant',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group {{ ($errors->has('marketingAssistantEmail') ? 'error' : '') }}">
                <label for="marketingAssistantEmail"><strong>Marketing Assistant Email Address</strong></label>
                <div class="controls">
                    {{ Form::text('marketingAssistantEmail',null,array('class'=>'txtbar')) }}
                    {{ ($errors->has('marketingAssistantEmail') ? $errors->first('marketingAssistantEmail') : '') }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group {{ ($errors->has('marketingContractEnd') ? 'error' : '') }}">
                <label for="marketingContractEnd" class="control-label"><strong>Contract End Date</strong></label>
                <div class="controls">
                    <div class="input-append datetimepicker4">
                        {{ Form::text('marketingContractEnd',null,array('data-format'=>'yyyy-MM-dd','readonly')) }}
                        <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                          </i>
                        </span>
                    </div>
                </div>
                <span class="help-block">{{ ($errors->has('marketingContractEnd') ? $errors->first('marketingContractEnd') : '') }}</span>
            </div>
        </div>
    </div>

    <div class="row" style="padding-left: 30px;">
        <div class="span12">
            <h4 class="dotted-border">Leasing Agent</h4>
        </div>
    </div>
    <div class="row" style="padding-left: 30px;">
        <div class="span4">
            <div class="control-group">
                <label for="leasingAgent"><strong>Leasing agent</strong></label>
                <div class="controls">
                    {{ Form::text('leasingAgent',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="leasingContactPerson"><strong>Leasing contact person</strong></label>
                <div class="controls">
                    {{ Form::text('leasingContactPerson',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="leasingTelephone"><strong>Tel</strong></label>
                <div class="controls">
                    {{ Form::text('leasingTelephone',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="padding-left: 30px;">
        <div class="span4">
            <div class="control-group">
                <label for="leasingCell"><strong>Cell</strong></label>
                <div class="controls">
                    {{ Form::text('leasingCell',null,array('class'=>'txtbar numeric')) }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group {{ ($errors->has('leasingEmail') ? 'error' : '') }}">
                <label for="leasingEmail"><strong>Email</strong></label>
                <div class="controls">
                    {{ Form::text('leasingEmail',null,array('class'=>'txtbar')) }}
                    {{ ($errors->has('leasingEmail') ? $errors->first('leasingEmail') : '') }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="leasingFax"><strong>Fax</strong></label>
                <div class="controls">
                    {{ Form::text('leasingFax',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="padding-left: 30px;">
        <div class="span12">
            <h4 class="dotted-border">Shopping Council Member Details</h4>
        </div>
    </div>
    <div class="row" style="padding-left: 30px;">
        <div class="span4">
            <div class="control-group">
                <label for="shoppingCouncilMember"><strong>Are you a shopping council member?</strong></label>
                <div class="controls">
                    {{ Form::radio('shoppingCouncilMember','Y') }}Yes &nbsp;&nbsp;{{ Form::radio('shoppingCouncilMember','N') }}No
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="shoppingCouncilNumber"><strong>If so, what is your shopping council member number?</strong></label>
                <div class="controls">
                    {{ Form::text('shoppingCouncilNumber',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
    </div>
    <button class="submit btn btn-info" type="submit" id="update_user">Submit Details</button>
    </form>
    </div>
    </div>
@stop
@section('exScript')
    <script type="text/javascript">
        $(document).ready(function () {

            $(".numeric").mask('27b99999999');

            $('.datetimepicker4').datetimepicker({
                pickTime: false
            }).on('changeDate', function (e) {
                $(this).datetimepicker('hide');
            });

            $('#password').keyup(function () {
                var _parent = $(this).parent().parent();


                var level = 0;
                var mesg = '';
                var input = $(this).val();

                if (/(.*[A-Z])/.test(input) == false) {
                    level = 1;
                    mesg += ' No uppercase letters.<br/>';
                }

                if (/(.*[\!|\@|\#|\$|\_])/.test(input) == false) {
                    level = 2;
                    mesg += ' No special character (!@#$_).<br/>';
                }
                if (/(.*[\d])/.test(input) == false) {
                    level = 3;
                    mesg += ' No numbers.<br/>';
                }
                if (/(.*[a-z])/.test(input) == false) {
                    level = 4;
                    mesg += ' No lowercase letters.<br/>';
                }

                if (input.length < 6) {
                    level = 5;
                    mesg += ' Not long enough.<br/>';
                }

                $('#password_help').html(mesg);

                if (level != 0) {
                    _parent.addClass('error');

                    $('#sub').attr('disabled', 'disabled');
                } else {
                    $('#sub').removeAttr('disabled');
                }
            });

        });
    </script>
@stop