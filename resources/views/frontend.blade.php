@extends('layouts.master')
@section('snippets')
    <div class="col-md-12 col-lg-5 p-t-20">
        <span class="highlight">MALLGUIDE IS A DIRECTORY LISTING FOR MALLS</span>
        <div class="clearfix p-b-20"></div>
        <div class="col-md-6 col-lg-4">
            <span class="whitebold">25, 000</span>
            <hr style="margin:5px 0" />
            <span class="purplebold">SHOPS</span>
        </div>
        <div class="col-md-6 col-lg-4">
            <span class="whitebold">1, 400</span>
            <hr style="margin:5px 0" />
            <span class="purplebold">MALLS</span>
        </div>
        <div class="col-md-12 col-lg-4"></div>
        <div class="clearfix"></div>
    </div>
@endsection
@section('content')
    <div class="p-50"></div>
    <div class="row">
        <div class="col-md-4 col-lg-2">
            <div class="bluebocks">
                <div class="iconwrap">
                    <div class="iconblock">
                        <a href="#" class="hvr-push"><img src="/assets/images/entertainment.png" class="img-fluid" alt="Mallguide Entertainment" /></a>
                    </div>
                </div>
                ENTERTAINMENT
            </div>
        </div>
        <div class="col-md-4 col-lg-2">
            <div class="bluebocks">
                <div class="iconwrap">
                    <div class="iconblock">
                        <a href="/shops.php" class="hvr-push"><img src="/assets/images/shops.png" class="img-fluid" alt="Mallguide Shops" /></a>
                    </div>
                </div>
                SHOPS
            </div>
        </div>
        <div class="col-md-4 col-lg-2">
            <div class="bluebocks">
                <div class="iconwrap">
                    <div class="iconblock">
                        <a href="/restaurants.php" class="hvr-push"><img src="/assets/images/restaurants.png" class="img-fluid" alt="Mallguide Restaurants" /></a>
                    </div>
                </div>
                RESTAURANTS
            </div>
        </div>
        <div class="col-md-4 col-lg-2">
            <div class="bluebocks">
                <div class="iconwrap">
                    <div class="iconblock">
                        <a href="/promotions.php" class="hvr-push"><img src="/assets/images/promotions.png" class="img-fluid" alt="Mallguide Promotions" /></a>
                    </div>
                </div>
                PROMOTIONS
            </div>
        </div>
        <div class="col-md-4 col-lg-2">
            <div class="bluebocks">
                <div class="iconwrap">
                    <div class="iconblock">
                        <a href="/competitions.php" class="hvr-push"><img src="/assets/images/competitions.png" class="img-fluid" alt="Mallguide Competitions" /></a>
                    </div>
                </div>
                COMPETITIONS
            </div>
        </div>
        <div class="col-md-4 col-lg-2">
            <div class="bluebocks">
                <div class="iconwrap">
                    <div class="iconblock">
                        <a href="#" class="hvr-push"><img src="/assets/images/featured.png" class="img-fluid" alt="Mallguide Featured Stores" /></a>
                    </div>
                </div>
                FEATURE MALLS
            </div>
        </div>
    </div>
    <div class="p-50"></div>
    <div class="row">
        <div class="col-md-4">
            <div class="darkblueblocks">
                <span class="bluetext">LISTING 1400+ MALLS</span><br />
                All the Mall info in one place. Get Mall or Store information in one destination...
            </div>
        </div>
        <div class="col-md-4">
            <div class="darkblueblocks">
                <span class="bluetext">WHAT'S HAPPENING?</span><br />
                We keep up to date with events, competitions, movies & exhibitions. Mallguide, for people in the know...
            </div>
        </div>
        <div class="col-md-4">
            <div class="darkblueblocks">
                <span class="bluetext">FOLLOW US ON SOCIAL</span> <a href="https://www.facebook.com/mallguide/" target="_blank"><i class="fa fa-facebook-f" style="color:white"></i></a> <a href="https://twitter.com/mallguide" target="_blank"><i class="fa fa-twitter" style="color:white"></i></a><br />
                Get all things to you, in one location. Why not show us some love and like us on Facebook.
            </div>
        </div>
    </div>
    <div class="p-50"></div>
    <div class="row">
        <div class="col-md-12 col-lg-6">
            <span class="highlight">WHO ARE WE?</span><br />
            <h2>MALLGUIDE IS THE ULTIMATE LISTING FOR MALLS</h2>
            This is the amalgamation of years of loving work and ideas, developed with all possible users in mind. Mallguide will service malls, shops, leasing agents and even independent movie houses who until now have not been able to provide their customers with a free resource to all relevant information.
            <br /><br />
            For Mall or Store contact details please search in Mallguide's Directory.
            <br /><br />
        </div>
        <div class="col-md-12 col-lg-6">
            <div class="owl-carousel owl-theme">
                <div class="item scroll01">
                    <h3>FEATURE <b>MALLS</b></h3>
                    <div class="p-100"></div>
                    <b>HYDE PARK CORNER</b><br />
                    Sandton - Johannesburg<br /><br />
                    <button type="button" class="btn btn-primary">View More</button>
                </div>
                <div class="item scroll02">
                    <h3>FEATURE <b>MALLS</b></h3>
                    <div class="p-100"></div>
                    <b>HYDE PARK CORNER</b><br />
                    Sandton - Johannesburg<br /><br />
                    <button type="button" class="btn btn-primary">View More</button>
                </div>
                <div class="item scroll01">
                    <h3>FEATURE <b>MALLS</b></h3>
                    <div class="p-100"></div>
                    <b>HYDE PARK CORNER</b><br />
                    Sandton - Johannesburg<br /><br />
                    <button type="button" class="btn btn-primary">View More</button>
                </div>
                <div class="item scroll02">
                    <h3>FEATURE <b>MALLS</b></h3>
                    <div class="p-100"></div>
                    <b>HYDE PARK CORNER</b><br />
                    Sandton - Johannesburg<br /><br />
                    <button type="button" class="btn btn-primary">View More</button>
                </div>
            </div>
        </div>
    </div>
@endsection
