@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Create Competition</h1>

                    <p class="animated fadeInDown delay2">Manage your Mall, Tenant Information &amp; Communication</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')
@stop

@section('content')

    <div class="row-fluid">
        <div class="blog-content">
            <h4 class="dotted-border">Build your own competition</h4>
            {{ Form::open(array('route' => 'competitions.postInsert', 'method' => 'post','files'=>'true', 'name' => 'compCreate','id' =>'compCreate', 'class' => 'form-inline')) }}
            {{ Form::hidden('referer',URL::previous()) }}
            <input type="hidden" name="thanksMsg" value="Thank you, your entry was successful!"/>

            @if(Session::get('activeChain'))
                <div class="control-group {{ ($errors->has('mallID') ? 'error' : '') }}">
                    <label class="control-label" for="mallID"><strong>Select a mall</strong></label>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                    <a href="#" id="deselect_all"><strong>Deselect All</strong></a>
                    <div class="controls">
                        {{ Form::select('mallID[]',$chainMalls,$chainMallIDs,array('class'=>'span10 multi-mall','id'=>'mallID[]','multiple')) }}

                        <div class="help-block">{{ ($errors->has('mallID') ? $errors->first('mallID') : '') }}</div>
                    </div>
                </div>
                <br/>
                <br/>
            @endif

            <div class="span5">
                <h4 class="dotted-border">Form Recipient</h4>

                <div class="control-group {{ ($errors->has('recipient') ? 'error' : '') }}">
                    <label for="recipient" class="control-label">Mandatory: E-mail address of person receiving the
                        competition entries.</label>
                    <div class="controls">
                        {{ Form::text('recipient',null,array('class'=>'txtbar','id'=>'recipient')) }}
                        <div
                            class="help-block text-error">{{ ($errors->has('recipient') ? $errors->first('recipient') : '') }}</div>
                    </div>
                </div>

                <h4 class="dotted-border">Competition Title</h4>

                <div class="control-group {{ ($errors->has('subject') ? 'error' : '') }}">
                    <label for="subject" class="control-label">Mandatory: Also appears on the subject line of the
                        email.</label>

                    <div class="controls">
                        {{ Form::text('subject',null,array('class'=>'txtbar','id'=>'subject')) }}

                    </div>
                    <span class="help-block">{{ ($errors->has('subject') ? $errors->first('subject') : '') }}</span>
                </div>

                <div class="control-group">
                    <label for="description">Competition description</label>
                    <div class="controls">
                        <textarea rows="3" class="txtbox" name="description"></textarea>
                    </div>
                </div>

            </div>
            <!--span4-->

            <div class="span6">

                <div class="control-group {{ ($errors->has('startDate') ? 'error' : '') }}">
                    <label for="startDate" class="control-label"><strong>Start Date*</strong></label>
                    <div class="controls">
                        <div class="input-append datetimepicker4">
                            {{ Form::text('startDate',null,array('class'=>'span12','id'=>'startDate','data-format'=>'yyyy-MM-dd','readonly')) }}
                            <span class="add-on">
			                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
			                  </i>
			                </span>
                        </div>
                        <span
                            class="help-block text-error">{{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}</span>
                    </div>
                </div>
                <br/>
                <div class="control-group {{ ($errors->has('endDate') ? 'error' : '') }}">
                    <label for="endDate" class="control-label"><strong>End Date*</strong></label>
                    <div class="controls">
                        <div class="input-append datetimepicker4">
                            {{ Form::text('endDate',null,array('class'=>'span12','id'=>'endDate','data-format'=>'yyyy-MM-dd','readonly')) }}
                            <span class="add-on">
			                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
			                  </i>
			                </span>

                        </div>
                        <span class="help-block">{{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}</span>
                    </div>
                </div>

                <br/>
                <br/>
                <label for="activated"><strong>Display competition</strong></label>
                Yes &nbsp;<input name="activated" type="radio" value="Y" checked="checked"/>
                No &nbsp;<input name="activated" type="radio" value="N"/>

                <br/>

                <!-- Vincent Park -->
                @if( $mallID == 5 )
                    <div class="control-group">
                        <label for="displayFor">Display On :</label>
                        <div class="controls">
                            Mallguide &nbsp;<input type="radio" name="displayFor" value="M" selected="selected"/> &nbsp;&nbsp;
                            Vicinity Loyalty Rewards &nbsp;<input type="radio" name="displayFor" value="L"/> &nbsp;&nbsp;
                            Both &nbsp;<input type="radio" name="displayFor" value="B"/> &nbsp;&nbsp;

                        </div>
                    </div>
                @endif
                <h4 class="dotted-border">Uploads</h4>
                <p class="text-danger"><small>Please make sure that any files uploaded do not contain spaces or special
                        characters in the name.</small></p>
                <div class="control-group {{ ($errors->has('image1') ? 'error' : '') }}">
                    <label><strong>Upload an image</strong></label>
                    <div class="controls">
                        <span class="btn btn-file">{{ Form::file('image1') }}</span>
                        <br/>
                        <div
                            class="help-block text-error">{{ ($errors->has('image1') ? $errors->first('image1') : '') }}</div>
                    </div>
                </div>
                <br/>
                <br/>
                @if(isset($filePath))
                    <?php echo "<label>Uploaded Terms & Conditions:</label> <br /><a target='_blank' href='" . $filePath . "' />" . $form->TsandCsFile . "</a><br/>" ?>
                    <br>
                @endif

                <div class="control-group {{ ($errors->has('TsandCsFile') ? 'error' : '')}}">
                    <label><strong>Terms and Conditions &nbsp;<small>Only PDF files</small></strong></label>
                    <div class="controls">
                        <span class="btn btn-file">{{ Form::file('TsandCsFile', array('id'=>'TsandCsFile')) }}</span>
                        <br/>
                        <div
                            class="help-block">{{ ($errors->has('TsandCsFile') ? $errors->first('TsandCsFile') : '') }}</div>
                    </div>
                </div>

                <button class="submit reg-btn" id="add_review">Submit Details</button>
                <input type="hidden" name="submitBtnText" value="Submit"/>
            </div>

            <!--span4-->
            {{ Form::close() }}
            <div class="clear"></div>

        </div><!--/span12-->
    </div><!--/span12-->

@stop

@section('exScript')
    <script>

        $(function () {

            $('.datetimepicker4').datetimepicker({
                pickTime: false
            }).on('changeDate', function (e) {
                $(this).datetimepicker('hide');
            });
            $(".multi-mall").select2({});

            $("#deselect_all").click(function () {

                $(".multi-mall").select2('val', '');
            });
        });
    </script>
@stop
