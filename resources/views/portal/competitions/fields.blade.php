@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Edit Competition </h1>

                    <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp;
                        Communication</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')

@stop


@section('content')
    <div class="row-fluid">
        <div class="blog-content">
            <h4 class="dotted-border">Edit the Existing Competition Details</h4>
            {{ Form::model($form, array('route' => array('competitions.postFields', $form->formID),'method'=>'post','files'=>true ,'name' => 'formFieldEdit', 'id' => 'formFieldEdit', 'class' => 'form-inline')) }}
            {{ Form::hidden('referer',URL::previous()) }}

            <div class="row-fluid">
                <div class="span4">
                    <h4 class="dotted-border">Form Recipient</h4>

                    <div class="control-group {{ ($errors->has('recipient') ? 'error' : '') }}">
                        <label for="recipient">Mandatory: E-mail address of person receiving the competition
                            entries.</label>

                        <div class="controls">
                            {{ Form::text('recipient',null,array('class'=>'txtbar')) }}
                            <span
                                class="help-block">{{ ($errors->has('recipient') ? $errors->first('recipient') : '') }}</span>
                        </div>
                    </div>

                    <h4 class="dotted-border">Competition Title</h4>

                    <div class="control-group {{ ($errors->has('subject') ? 'error' : '') }}">
                        <label for="subject">Mandatory: Also appears on the subject line of the email.</label>

                        <div class="controls">
                            {{ Form::text('subject',null,array('class'=>'txtbar')) }}
                            <span
                                class="help-inline">{{ ($errors->has('subject') ? $errors->first('subject') : '') }}</span>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="description">Competition description</label>

                        <div class="controls">
                            {{ Form::textarea('description',null,array('class'=>'txtbox','rows'=>'3')) }}
                        </div>
                    </div>

                </div>
                <!--span4-->

                <div class="span4">
                    <h4 class="dotted-border">Competition Dates</h4>
                    <div class="control-group {{ ($errors->has('startDate') ? 'error' : '') }}">
                        <div class="input-append datetimepicker4">
                            <div class="span3">
                                <label for="startDate">Start</label>
                            </div>
                            <!--span3-->
                            {{ Form::text('startDate',null,array('class'=>'span12','id'=>'startDate','data-format'=>'yyyy-MM-dd','readonly')) }}
                            <span class="add-on">
                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                  </i>
                    {{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}
                </span>
                        </div>
                    </div>

                    <div class="clear"></div>
                    <br/>

                    <div class="control-group {{ ($errors->has('endDate') ? 'error' : '') }}">
                        <div class="input-append datetimepicker4">
                            <div class="span3">
                                <label for="endDate">End</label>
                            </div>
                            <!--span3-->
                            {{ Form::text('endDate',null,array('class'=>'span12','id'=>'endDate','data-format'=>'yyyy-MM-dd','readonly')) }}
                            <span class="add-on">
                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                  </i>
                    <span class="help-inline">{{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}</span>
                </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="activated">Display competition</label>
                        <br/>
                        <label class="radio inline">
                            {{ Form::radio('activated','Y') }}Yes
                        </label>
                        <label class="radio inline">
                            {{ Form::radio('activated','N') }} No
                        </label>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="noForm">Display Online Form:</label>
                        <br/>
                        <label class="radio inline">
                            {{ Form::radio('noForm', 'N') }} Yes
                        </label>
                        <label class="radio inline">
                            {{ Form::radio('noForm', 'Y') }} No
                        </label>
                    </div>
                    <br/>
                    <div class="clear"></div>

                    <h4 class="dotted-border">Uploads</h4>
                    <div class="control-group" {{ ($errors->has('image1') ? 'error' : '') }}>
                        @if(isset($imagePath))
                            <img src="{{$imagePath}}" alt="Current Image"/><br/><br/>
                        @endif
                        <label>Select an Image</label>
                        <div class="controls">
                            <span class="btn btn-file">{{ Form::file('image1',array('id'=>'img')) }}</span>
                            <br/>
                            <span
                                class="help-inline">{{ ($errors->has('image1') ? $errors->first('image1') : '') }}</span>
                        </div>
                        <br/>

                    </div>
                    <br/>
                    @if(isset($filePath))
                        <label>Uploaded Terms & Conditions:</label> <br/><a target='_blank' href="{{$filePath}}"/> {{$form->TsandCsFile }}</a><br/>
                        <br>
                    @endif
                    <div class="control-group" {{ ($errors->has('TsandCsFile') ? 'error' : '') }}>
                        <label><strong>Terms and Conditions &nbsp;<small>Only PDF files</small></strong></label>
                        <div class="controls">
                            <span class="btn btn-file">{{ Form::file('TsandCsFile') }}</span>
                            <br/>
                            <span
                                class="help-inline">{{ ($errors->has('TsandCsFile') ? $errors->first('TsandCsFile') : '') }}</span>

                        </div>
                    </div>

                    <button class="submit reg-btn" id="add_review">Submit Details</button>
                    <input type="hidden" name="submitBtnText" value="Submit"/>
                </div>
                <div class="span4">
                    <h4>Competition Fields and Questions</h4>
                    <h6>Add a competition Field</h6>
                    Clicking the "Add Competition Field" button below will allow you to add a new field to the
                    competition.
                    The options available to you include the ability to add text fields, text areas, radio buttons,
                    checkboxes and drop down lists.
                    <a class="btn btn-mini btn-info" href="{{ route('admin.addfield',$form->formID) }}" type="button"
                       data-id="{{ $form->formID }}">Add Competition Field</a>
                    <table class="table table-bordered">
                        <h6>Edit Existing Competition Fields</h6>
                        <thead>
                        <th>Field Description</th>
                        <th>Actions</th>
                        </thead>
                        <tbody>
                        @foreach( $formFields as $field )
                            <tr>
                                <td>
                                    {{ $field->name }}
                                </td>
                                <td class="move">
                                    <a href="{{ route('admin.editField',$field->formFieldID) }}"
                                       class="btn btn-info btn-mini" id="{{ $field->formFieldID }}"><i
                                            class="fa fa-pencil"></i>Edit</a>&nbsp;|&nbsp;<a href="#"
                                                                                             class="btn btn-danger btn-mini action_delete"
                                                                                             id="{{ $field->formFieldID }}"><i
                                            class="fa fa-trash-o"></i>Remove</a>
                                    {{ Form::select('move',$moves,NULL, array('id'=>'move','data-id'=>$field->formFieldID,'class' => 'span6 with-search to_move')) }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>

                {{ Form::close() }}
            </div><!--/span12-->
        </div>
        @stop

        @section('exScript')
            <script src="{{ asset('js/fields.js') }}"></script>
            <script type="text/javascript">
                $(function () {
                    hideMovements(".move select");
                    activatePortalMove();
                    $('.datetimepicker4').datetimepicker({
                        pickTime: false,
                        autoclose: true
                    });

                    $(".action_delete").click(function (e) {

                        e.preventDefault();
                        var field = $(this).attr("id");

                        bootbox.confirm("Are you sure you want to remove this field ? ");

                        $(".modal a.btn-primary").click(function () {
                            $.ajax({
                                type: "GET",
                                url: '/admin/deleteField/' + field,
                                data: '',
                                success: function (data) {
                                    location.reload();
                                }
                            });
                        });


                    });

                });

                function activatePortalMove() {
                    $(".to_move").change(function () {
                        var move = $(this).val();
                        var fieldID = $(this).data('id');
                        var the_url = "/admin/changeOrder/" + fieldID + '/' + move;
                        $.ajax(
                            {
                                type: 'GET',
                                url: the_url,
                                success: function (data) {
                                    location.reload();
                                }
                            });
                    });
                }
            </script>
@stop
