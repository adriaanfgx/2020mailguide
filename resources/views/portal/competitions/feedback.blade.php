@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
			<div class="inner-heading">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span6">
    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Competition Entry</h1>
    <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
                        </div><!--/span6-->
                        <div id="breadcrumbs" class="span6">
                            <ul class="breadcrumb">
                                <li><a href="/">Home</a><span class="divider">/</span></li>
                                <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                                <li class="active">Competitions</li>
                            </ul>
                        </div><!--/span6-->
                    </div><!--/row-->
                </div><!--/container-->
            </div><!--/three-->
    @include('portal.partials.submenu')
@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div>
    <!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">

    <div class="span12">
	<h4 class="hborder">Competitions Entry Details</h4>

    </div>
    <!-- span6 -->

    <div class="clear"></div>

    <div class="row-fluid">
        <div class="span12">
            <table class="table table-border">
                <thead>
                <tr>
                    <th>Competition Field/ Question</th>
                    <th>Response / Answer</th>
                </tr>
                </thead>
                <tbody id="response_table">
                @if(sizeof($feedbackdata) > 0 )
                @foreach($feedbackdata as $data)
                <tr>
                    <td>{{ $data->fieldName }}</td>
                    <td>{{ $data->value }}</td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="2">No data.....</td>
                </tr>
                @endif
                </tbody>
            </table>

        </div>
        <!--span12 -->
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $(".action_delete").click(function (e) {

            e.preventDefault();
            var event = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

//            $(".modal a.btn-primary").click(function () {
//                $.ajax({
//                    type: "POST",
//                    url: '/events/delete/' + event,
//                    data: '',
//                    success: function () {
////                        alert('deleted');
//                    },
//                    dataType: 'json'
//                });
//            });


        });
    });
</script>
@stop

