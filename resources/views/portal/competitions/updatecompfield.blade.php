@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Edit Competition </h1>

                <p class="animated fadeInDown delay2">Manage your Mall, Tenant Information &amp; Communication</p>
            </div>
            <!--/span6-->
            <div id="breadcrumbs" class="span6">
                      {{ generateBreadcrumbs() }}
            </div>
            <!--/span6-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</div><!--/three-->
	@include('portal.partials.submenu')
@stop


@section('content')

<div class="row-fluid">
	<div class="blog-content">
		{{$add_field}}
	</div><!--/span12-->
</div><!--/span12-->

@stop

@section('exScript')
<script src="{{ asset('js/fields.js'); }}"></script>
<script type="text/javascript">
    $(document).ready(function ()
	{
        @if(isset($field))
            formID={{$field->formID}};
            var fieldID={{$field->formFieldID}};
		@else
			@if(isset($formID))
                formID ={{$formID}};
            @else
                formID ="";
			@endif
            var fieldID='';

		@endif
		//alert(formID)
		change_field_types($('#typeOfField').val());
		$("#typeOfField").change(function()
        {
            //console.log("hide_fields");
            change_field_types($(this).val());
        });

        $('#btn_submit').click(function(e)
        {
            e.preventDefault();
			if(fieldID!="")
			{
				var the_url='/admin/updateField/'+fieldID;
			}
			else
			{
				var the_url='/admin/createField/'+formID;
			}
            $.ajax(
            {
                type: 'POST',
                url: the_url,
                data: $('#addField').serialize(),
                success: function ()
                {
                    window.location.assign("/competitions/fields/"+formID)
                }
            });

        });


    });
</script>
@stop
