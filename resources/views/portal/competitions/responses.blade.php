@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
			<div class="inner-heading">
                <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span6">
    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Competition Entries</h1>
    <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
                        </div><!--/span6-->
                        <div id="breadcrumbs" class="span6">
                            <ul class="breadcrumb">
                                <li><a href="/">Home</a><span class="divider">/</span></li>
                                <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                                <li class="active">Competitions</li>
                            </ul>
                        </div><!--/span6-->
                    </div><!--/row-->
                </div><!--/container-->
            </div><!--/three-->
	@include('portal.partials.submenu')
@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div>
    <!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">
    <div class="span12 text-right">
        <a href="{{ route('feedback.export', $formID) }}" class="btn btn-info btn-mini" ><i class="fa fa-download"></i>Export to Excel</a>&nbsp;|&nbsp;<a href="{{ route('feedback.randomize', $formID) }}" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i>Select a Random Winner</a>
    </div>
    <div class="span12">
        <h4 class="hborder">Listed Competitions</h4>

    </div>
    <!-- span6 -->

    <div class="clear"></div>

    <div class="row-fluid">
        <div class="span12">
            <table class="table table-border">
                <thead>
                <tr>
                    <th>Date Received</th>
                    <th>Email Address</th>
                    <th>Winner</th>
                    <th>Status</th>
                    <th class="align-right">Actions</th>
                </tr>
                </thead>
                <tbody id="response_table">
                @if(sizeof($respondents) > 0 )
                @foreach($respondents as $respondent)
                <tr>
                    <td>{{ $respondent->date }}</td>
                    <td>{{ $respondent->email }}</td>
                    <td>
                        @if($respondent->winner == 1)
                            <span class='label label-success'>WINNER</span>
                        @endif
                    </td>
                    <td>
                        @if($respondent->winner == 1)
                        <a href="{{ route('winner.disqualify',$respondent->feedbackID) }}" class="btn btn-warning btn-mini" ><i class="fa fa-cancel"></i>Not a Winner</a>
                        @endif
                    </td>
                    <td class="align-right">
                        <a href="{{ route('feedback.view',$respondent->feedbackID) }}" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i>View</a>&nbsp;|&nbsp;<a class="btn btn-danger btn-mini action_delete" href="#" id="#"><i class="fa fa-trash-o"></i> Delete</a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5">There are currently no entrants for this competition...</td>
                </tr>
                @endif
                </tbody>
            </table>

        </div>
        <!--span12 -->
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $(".action_delete").click(function (e) {

            e.preventDefault();
            var event = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

//            $(".modal a.btn-primary").click(function () {
//                $.ajax({
//                    type: "POST",
//                    url: '/events/delete/' + event,
//                    data: '',
//                    success: function () {
////                        alert('deleted');
//                    },
//                    dataType: 'json'
//                });
//            });


        });
    });
</script>
@stop

