@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Competition Listing</h1>

                    <p class="animated fadeInDown delay2">Manage your Mall,Tenant Information &amp; Communication</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')
@stop
@section('content')

    <div class="container-fluid biggerPadding blog-content">


        <!-- span6 -->

        <div class="clear"></div>

        <div class="row-fluid">
            <div class="span12">
                <h4 class="hborder">Competitions<a id="addComp" href="/competitions/create"
                                                   class="btn btn-info pull-right"><i class="fa fa-plus"></i> Add
                        New</a></h4>

                <h5>Current Competitions</h5>
                <table class="table table-border">
                    <thead>
                    <tr>
                        <th>Subject</th>
                        <th>Start Date</th>
                        <th>Close Date</th>
                        <th>Displayed?</th>
                        <th>Responses</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody id="comp_table">
                    @if(sizeof($currentComp) > 0 )

                        @foreach($currentComp as $current)
                            <tr>
                                <td>{{ $current->subject }}</td>
                                <td>{{ substr($current->startDate,0,10) }}</td>
                                <td>{{ substr($current->endDate,0,10) }}</td>
                                <td>{{ displayText($current->activated) }}</td>
                                <td>
                                    <span>{{ $current->count_resp }}</span><br/>
                                    <span>({{ $current->winners }} {{ pluralize($current->winners,'Winner') }})</span>
                                </td>
                                <td>
                                    <a href="{{ route('competitions.fields',$current->formID) }}"
                                       class="btn btn-info btn-mini"><i class="fa fa-pencil"></i> Edit</a>&nbsp;&nbsp;
                                    @if($current->count_resp > 0 )
                                        <a href="{{ route('competitions.responses',$current->formID) }}"
                                           class="btn btn-info btn-mini"><i class="fa fa-eye"> Responses</i></a>&nbsp;
                                        &nbsp;
                                    @endif
                                    <a class="btn btn-danger btn-mini action_delete"
                                       href="{{ route('competitions.responses',$current->formID) }}"
                                       id="{{ $current->formID }}"><i class="fa fa-trash-o"></i>
                                        Delete</a>
                                </td>
                            </tr>

                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">There are no current competitions.....</td>
                        </tr>
                    @endif

                    </tbody>
                </table>

                <h5>Past Competitions</h5>
                <table class="table table-border">
                    <thead>
                    <tr>
                        <th>Subject</th>
                        <th>Start Date</th>
                        <th>Close Date</th>
                        <th>Displayed?</th>
                        <th>Responses</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody id="comp_table">
                    @if(sizeof($pastComp) > 0 )
                        @foreach($pastComp as $past)
                            <tr>
                                <td>{{ $past->subject }}</td>
                                <td>{{ substr($past->startDate,0,10) }}</td>
                                <td>{{ substr($past->endDate,0,10) }}</td>
                                <td>{{ displayText($past->activated) }}</td>
                                <td>
                                    <span>{{ $past->count_resp }}</span><br/>
                                    <span>({{ $past->winners }} {{ pluralize($past->winners,'Winner') }})</span>
                                </td>
                                <td>
                                    <a href="{{ route('competitions.fields',$past->formID) }}"
                                       class="btn btn-info btn-mini"><i class="fa fa-pencil"></i> Edit</a>&nbsp;&nbsp;
                                    @if($past->count_resp > 0 )
                                        <a href="{{ route('competitions.responses',$past->formID) }}"
                                           class="btn btn-info btn-mini"><i class="fa fa-eye"> Responses</i></a>&nbsp;
                                        &nbsp;
                                    @endif
                                    <a class="btn btn-danger btn-mini action_delete"
                                       href="{{ route('competitions.responses',$past->formID) }}"
                                       id="{{ $past->formID }}"><i class="fa fa-trash-o"></i>
                                        Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">There are no current competitions.....</td>
                        </tr>
                    @endif

                    </tbody>
                </table>

            </div>
            <!--span12 -->
        </div>
    </div>
@stop
@section('exScript')
    <script src="{{ asset('js/bootbox.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".action_delete").click(function (e) {

                e.preventDefault();
                var competition = $(this).attr("id");

//            bootbox.confirm("Are you sure you want to delete this entry ? ");

//            $(".modal a.btn-primary").click(function () {
                $.ajax({
                    type: "POST",
                    url: '/competitions/delete/' + competition,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function (data) {
                        location.reload();
                    }
                });
//            });


            });
        });
    </script>
@stop

