@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Promotions Listing</h1>
                    <p class="animated fadeInDown delay2">Manage your Mall, Tenant Information &amp; Communication</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')
@stop

@section('content')

    <div class="row-fluid">
        <div class="blog-content">
            <?php

            $ADDURL = "/promotions/portalAdd";

            ?>
            <h4 class="hborder">Promotions<a id="addComp" href="{{$ADDURL}}" class="btn btn-info pull-right"><i
                        class="fa fa-plus"></i> Add New</a></h4>
            <h5>Current Promotions</h5>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Shop</th>
                    <th>Title</th>
                    <th width="125">Start Date</th>
                    <th width="125">End Date</th>
                    <th width="185">Actions</th>
                </tr>
                </thead>
                <tbody id="event_table">
                @if(sizeof($currentPromo) > 0)

                    @foreach($currentPromo as $current)
                        <tr>
                            <td>{{ $current->shop->name }}</td>
                            <td>{{ Str::limit($current->title, 50) }}</td>
                            <td width="125">{{ $current->startDate }}</td>
                            <td width="125">{{ $current->endDate }}</td>
                            <td width="185">
                                <a href="{{ route('promotions.update',$current->promotionsMGID) }}"
                                   class="btn btn-info btn-mini"><i class="fa fa-edit"></i>Edit</a>
                                <a href="#" id="{{ $current->promotionsMGID }}"
                                   class="btn btn-default btn-mini btn_copy"><i class="fa fa-copy"></i>Copy</a>
                                <a class="btn btn-danger btn-mini action_delete" href="#"
                                   id="{{ $current->promotionsMGID }}"><i class="fa fa-trash"></i>Delete</a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">There are no current promotions .........</td>
                    </tr>
                @endif
                </tbody>
            </table>

            <h5>Past Promotions</h5>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Shop</th>
                    <th>Promotion</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if( sizeof($pastPromo)> 0 )

                    @foreach($pastPromo as $past)
                        <tr>
                            <td>{{ $past->shop->name }}</td>
                            <td>{{ Str::limit($past->promotion, 50) }}</td>
                            <td>{{ $past->startDate }}</td>
                            <td>{{ $past->endDate }}</td>
                            <td colspan="3">
                                <a href="{{ route('promotions.update',$past->promotionsMGID) }}"
                                   class="btn btn-info btn-mini"><i class="icon-white icon-pencil"></i> Edit</a>&nbsp;|&nbsp;
                                <a href="#" id="{{ $past->promotionsMGID }}" class="btn btn-info btn-mini btn_copy"><i
                                        class="icon-white icon-eye-open"></i> Copy</a>&nbsp;|&nbsp;
                                <a class="btn btn-danger btn-mini action_delete" href="#"
                                   id="{{ $past->promotionsMGID }}"><i class="icon-white icon-trash"></i> Delete</a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5">There are no past promotions .........</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
@section('exScript')
    <script src="{{ asset('js/bootbox.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".action_delete").click(function (e) {

                e.preventDefault();
                var promotion = $(this).attr("id");
//           alert($(this).attr("id"));
                bootbox.confirm("Are you sure you want to delete this entry ? ");

                $(".modal a.btn-primary").click(function () {
                    $.ajax({
                        type: "POST",
                        url: '/promotions/delete/' + promotion,
                        data: {
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function () {
                            location.reload();
                        }
                    });
                });

            });

            $(".btn_copy").click(function () {

                var promotion = $(this).attr("id");

                $.ajax({
                    type: "POST",
                    url: '/promotions/copy/' + promotion,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function (data) {
                        location.reload();
                    }
                });

            });
        });
    </script>
@stop

