@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Create Promotion </h1>

                    <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp;
                        Communication</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')
@stop
@section('content')
    <div class=" row-fluid ">
        <div class="blog-content">
            <h3>Add A Promotion</h3>
            Get the exposure your promotion deserves, list it for free on Mallguide...
            <h4 class="dotted-border">How Does It Work?</h4>
            <ul>
                <li>You add a new promotion online</li>
                <li>Your Mall's Marketing team are notified of this submission</li>
                <li>The submission is verified and approved by your Mall's Marketing team</li>
                <li>Your promotion is automatically added to Mallguide</li>
                <li>Your promotion gets lots of exposure, and is a great success!</li>
            </ul>

            <form action="{{ route('promotions.postCreate') }}" method="post" enctype="multipart/form-data"
                  name="promotionCreate" id="promotionCreate" class="form-inline">
                @csrf
                <h4 class="dotted-border">Add Promotion Details</h4>
                @if(!Session::has('activeShopID'))

                    <div class="control-group span12">
                        <label for="mall">Mall*</label>
                        <div class="controls">
                            {{ Form::text('mall',$mall->name,array('readonly','class'=>'span6')) }}
                            {{ Form::hidden('mallID',$mall->mallID,array('readonly')) }}
                            {{ Form::hidden('referer',URL::previous()) }}
                            <span class="help-inline"></span>
                        </div>
                    </div>

                    <div class="control-group span12">
                        <label for="shopMGID">Shop*</label>
                        <div class="controls">
                            <select name="shopMGID" class="span6">
                                @foreach($shops as $shop)
                                    <option value="{{ $shop->shopMGID }}">{{ $shop->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @endif

                <div class="control-group   {{ ($errors->has('title') ? 'error' : '') }}">
                    <label for="title">Title*</label>
                    <div class="controls">
                        {{ Form::text('title',null,array('class'=>'span6')) }}
                    </div>
                    <span class="help-block">{{ ($errors->has('title') ? $errors->first('title') : '') }}</span>
                </div>

                <div class=" span3 ">

                    <div class="control-group  {{ ($errors->has('startDate') ? 'error' : '') }}">
                        <label for="startDate" class="control-label">Start*</label>
                        <div class="controls">
                            <div class="input-append datetimepicker4">
                                {{ Form::text('startDate',null,array('id'=>'startDate','data-format'=>'yyyy-MM-dd','readonly','class'=>'span10')) }}
                                <span class="add-on">
	                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                          </i>
	                        </span>
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}</span>
                        </div>
                    </div>
                    <div class="control-group  {{ ($errors->has('endDate') ? 'error' : '') }}">
                        <label for="endDate">End*</label>
                        <div class="controls">
                            <div class="input-append datetimepicker4">
                                {{ Form::text('endDate',null,array('id'=>'endDate','data-format'=>'yyyy-MM-dd','readonly','class'=>'span10')) }}
                                <span class="add-on">
	                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                          </i>
	                        </span>
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}</span>
                        </div>
                    </div>
                </div>
                <div class=" span8 ">

                    <div class="control-group {{ ($errors->has('promotion') ? 'error' : '') }}">
                        <label for="promotion ">Description*</label>
                        <div class="controls">
                            <textarea rows="3" class="txtbox" name="promotion"></textarea>
                            <span
                                class="help-block">{{ ($errors->has('promotion') ? $errors->first('promotion') : '') }}</span>
                        </div>
                    </div>
                    <div class="control-group {{ ($errors->has('image1') ? 'error' : '') }}">
                        <label>Upload Image</label>
                        <div class="controls">
                            <span class="btn btn-file">{{ Form::file('image1') }}</span>
                            {{ ($errors->has('image1') ? $errors->first('image1') : '') }}
                        </div>
                    </div>
                </div>

                <br/>
                <div class=" span12 ">
                    <button class="submit reg-btn" id="add_review">Submit Details</button>
                </div>
                <!--span4-->
            </form>
            <div class="clear"></div>
        </div><!--/span12-->
    </div><!--/span12-->
@stop

@section('exScript')
    <script type="text/javascript">
        $(function () {
            $('.datetimepicker4').datetimepicker({
                pickTime: false
            }).on('changeDate', function (e) {
                $(this).datetimepicker('hide');
            });
        });
    </script>
@stop
