@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Create Promotion </h1>
                    <p class="animated fadeInDown delay2">Manage your Promotions</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')
@stop
@section('content')

    <div class="row-fluid">
        <div class="blog-content">
            <h3>Edit Promotion</h3>

            Get the exposure your promotion deserves, list it for free on Mallguide...

            <h4 class="dotted-border">How Does It Work?</h4>
            <ul>
                <li>You add a new promotion online</li>
                <li>Your Mall's Marketing team are notified of this submission</li>
                <li>The submission is verified and approved by your Mall's Marketing team</li>
                <li>Your promotion is automatically added to Mallguide</li>
                <li>Your promotion gets lots of exposure, and is a great success!</li>
            </ul>

            {{ Form::model($promotion, array('route' => array('promotions.postUpdate', $promotion->promotionsMGID),'method'=>'post','files'=>true ,'name' => 'promotionEdit', 'id' => 'promotionEdit', 'class' => 'form-inline')) }}

            <h4 class="dotted-border">Update Promotion Details</h4>

            <div class="span12">
                <div class="control-group  {{ ($errors->has('title') ? 'error' : '') }}">
                    <label for="title">Title*</label>
                    <div class="controls">
                        {{ Form::text('title',$promotion->title, array('class'=>'span6')) }}
                    </div>
                    <span class="help-block">{{ ($errors->has('title') ? $errors->first('title') : '') }}</span>
                </div>
            </div>

            <div class="span3">
                <div class="input-append datetimepicker4">
                    <div class="control-group">
                        <label for="startDate">Start Date*</label>
                        <div class="controls">
                            {{ Form::text('startDate',null,array('data-format'=>'yyyy-MM-dd','class'=>'span12','readonly')) }}
                            <span class="add-on">
	                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                          </i>
	                        </span>
                        </div>
                    </div>
                </div>
                <div class="input-append datetimepicker4">
                    <div class="control-group">
                        <label for="endDate">End Date*</label>
                        <div class="controls">
                            {{ Form::text('endDate',null,array('data-format'=>'yyyy-MM-dd','class'=>'span12','readonly')) }}
                            <span class="add-on">
	                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                          </i>
	                        </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span8">
                <div class="control-group {{ ($errors->has('promotion') ? 'error' : '') }}">
                    <label for="promotion" class="control-label">Description*</label>
                    <div class="controls">
                        {{ Form::textarea('promotion',null,array('class'=>'txtbox','rows'=>'3')) }}
                    </div>
                    <span class="help-block">{{ ($errors->has('promotion') ? $errors->first('promotion') : '') }}</span>
                </div>
                @if(!empty($promotion->thumbnail1))
                    <img
                        src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $promotion->mallID }}/{{ $promotion->thumbnail1 }}"
                        alt="Image"/><br/>
                @else
                    <img src="" alt="No Image" class="overlay-image" data-overlaytext="Movie Info"/><br/>
                @endif
                <span class="btn btn-file">{{ Form::file('image1') }}</span>
            </div>
            <br/>
            <div class="span12">
                <button class="submit reg-btn" id="add_review">Submit Details</button>
            </div>
            <!--span4-->
            {{ Form::close() }}
            <div class="clear"></div>
        </div><!--/span12-->
    </div><!--/span12-->
@stop

@section('exScript')
    <script type="text/javascript">
        $(function () {
            $('.datetimepicker4').datetimepicker({
                pickTime: false
            });

        });
    </script>
@stop
