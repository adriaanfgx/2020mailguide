@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Logged In </h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a> <span class="divider">/</span></li>
                    <li class="active">Memberzone</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')
@stop


@section('content')

<div class="span12 middle-headings dotted-border white-bg">
    <div class="span6 pull-left" style="padding-left: 5px;">Hi {{ $user->first_name }}<br /> Listed on this page is a host of link shortcuts that will help you make the most of your listing on Mallguide. Please make use of our range of <strong>free services and tools</strong> at your disposal to help market and promote your mall to all of South Africa.</div>

</div><!--/span12-->



<div class="row-fluid">
    <div class="span7">
        <h4 class="dotted-border">How does it work?</h4>

        Mallguide lists your Mall and stores to more than 300,000 visitors a month, at no cost to you.
        It's our way at FGX Studios to give back to the retail community, that we have been servicing for the past decade.
        Keep your content current, and make use of the extra exposure and feet in your Mall.


        <h4 class="dotted-border">Approving Information on Mallguide</h4>

        Mallguide relies on you as the Mall's Marketing team or Centre management to check, and approve all store information posted from the website.
        Please ensure that all information is correct, as this will ensure your listing stays current and helpful to the South African shoppers.

        <h4 class="dotted-border">Approval hold</h4>

        Your Shops can request you to release the "Approval hold" on any updates made by them. All their Shop updates will feature automatically once posted on Mallguide.




        <h4 class="dotted-border">Email &amp; SMS Communication</h4>

        As part of the Mallguide toolkit, you are able to keep in touch with your tenants and shoppers using bulk emails or SMS's. To make use of this service, you will require
        3 Modules to be installed for your Mall;

        <h4 class="dotted-border">Member Module</h4>

        Manage and create various communication groups by adding member contact information. Reast easy knowing that all your CPA requirements are being taken care of by making
        allowances for member database unsubscribes in real time. Export your members to an Excel spreadsheet at any time. Keep in touch with your base using the Member Module.

        <h4 class="dotted-border">Communication Module</h4>

        The Communication Module works in tandem with the Member Module. Once you have loaded all your members to your various groups, you can start communicating to them by
        buying Communication Credits. Send personalised Emails and SMS's to your database. Commision email templates from FGX Studios, that once loaded will enable you to compose
        monthly, regular communication or a news flash with just a couple of clicks.

        <h4 class="dotted-border">Email Templates</h4>

        Make your communication attractive with a Email Template. Order these to be customed designed for your needs, in your individual look and feel. There are no limits to the
        amount of templates you can load.

    </div><!-- span6 -->

    <div class="span5">
        <h4>Your Control Panel</h4>

        <div class="control-panel">
            <?php
                $user = Sentry::getUser();
                $userID = $user->id;
            ?>
            <h4><span class="control-ico-about">About You</span></h4>
            <ul>
                <li><a href="{{ route('user.updatepassword',$userID) }}">Update your password</a></li>
                <li><a href="{{ route('user.updatedetails',$userID) }}">Update your personal information</a></li>
                <li><a href="{{ route('portal.marketingmalls') }}">Update your current mall details</a></li>
            </ul>

            <!-- Mall And User update section -->

            <h4><span class="control-ico-comm">Communication with Tenants</span></h4>
            <ul>
                <li>You have &nbsp; {{ $smsCredits }} &nbsp; SMS credits left, &nbsp;<a href="{{ route('manager.buysmscredits') }}"><strong>order more</strong></a></li>
                <li><a href="{{ route('manager.getsendsms') }}">Compose SMS Message for Tenants</a></li>
                <li><a href="{{ route('manager.smsreports') }}">Tenant SMS Message Reports</a></li>
            </ul>

            <h4><span class="control-ico-links">Quick Links</span></h4>
            <ul>
                <li>
                    <a href="{{ route('portal.addshop') }}">Add a Shop</a>&nbsp; | &nbsp;
                    <a href="{{ route('portal.marketingshops') }}">List Shops</a>
                </li>
                <li>
                    <a href="{{ route('portal.addmarketinguser') }}">Add a User </a>&nbsp; | &nbsp;
                    <a href="{{ route('portal.listusers') }}"> List Users</a>
                </li>
                <li>
                    <a href="{{ route('portal.addmarketingevent') }}">Add an Event</a>&nbsp; | &nbsp;
                    <a href="{{ route('portal.marketingevents') }}">List Events</a>
                </li>
                <li>
                    <a href="{{ route('portal.exhibitions') }}">Add an Exhibition</a>&nbsp; | &nbsp;
                    <a href="{{ route('portal.mexhibitions') }}">List Exhibitions</a>
                </li>
                <li>
                    <a href="{{ route('portal.malls') }}">Add a Competition</a>&nbsp; | &nbsp;
                    <a href="{{ route('portal.mcomps') }}">List Competitions</a>
                </li>
                <li>
                    <a href="{{ route('portal.promotionmalls') }}">Add a Promotion</a>&nbsp; | &nbsp;
                    <a href="{{ route('portal.mpromotions') }}">List Promotions</a>
                </li>
                <li>
                    <a href="{{ route('portal.jobmalls') }}">Add a Job</a>&nbsp; | &nbsp;
                    <a href="{{ route('portal.mjobs') }}">List Jobs</a>
                </li>
                <!--                <li><a href="#">Billing - Issued Invoices</a></li>-->
            </ul>

            <div>
                <h4><span class="control-ico-posts">Information posted by Mallguide</span></h4>
                <ul>
                    <li><a href="{{ route('portal.marketinginfo') }}">Want to send an electronic Newsletter to your Shopper/Client Database?</a></li>
                    <li><a href="{{ route('portal.competitioninfo') }}">Standard Terms and Conditions for Competitions and Adding Entrants into Databases for further Communications</a></li>
                </ul>
            </div>
        </div><!-- control panel -->
    </div><!--span6 -->
</div><!--row -->









@stop
