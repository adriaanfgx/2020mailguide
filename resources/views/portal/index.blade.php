@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Logged In </h1>
                    <p class="animated fadeInDown delay2">Manage your Mall, Tenant Information &amp; Communication</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')
@stop


@section('content')

    <div class=" blog-content container-fluid ">
        <div class="row-fluid">
            <h3 class="biggerLeftMargin">{{ $headingName }} Control Panel</h3>
            <div class="well biggerLeftMargin biggerRightMargin well-small">Hi {{ $user->first_name }},<br/> Listed on
                this page is a host of link shortcuts that will help you make the most of your listing on Mallguide.
                Please make use of our range of <strong>free services and tools</strong> at your disposal to help market
                and promote your {{$message}} to all of South Africa.
            </div>

            <div class="span7">
                @if((isset($unapprovedShops) && $unapprovedShops > 0)||(isset($shopUsers) && $shopUsers > 0) || (isset($subscribed) && $subscribed > 0))
                    <div class="yellow control-panel">
                        <h4><i class="fa fa-user"></i> Pending</strong> - Awaiting approval</h4>

                        <ul>
                            @if(isset($unapprovedShops) && $unapprovedShops > 0)
                                <li>
                                    <?php $params = array('display' => 'N');
                                    $queryString = http_build_query($params);
                                    ?>
                                    <a href="{{ route('shops.list').'?'.$queryString }}">{{ $unapprovedShops }}</a>
                                    shop(s) need to be set to display.<a
                                        href="{{ route('shops.list').'?'.$queryString }}"> Approve here</a>
                                </li>
                            @endif
                            @if(isset($shopUsers) && $shopUsers > 0)
                                <li>
                                    <a href="{{ route('portal.listusers') }}">{{ $shopUsers }}</a> shop user(s) have
                                    requested to be approved.<a href="{{ route('portal.listusers') }}"> Approve here</a>
                                </li>
                            @endif
                            @if(isset($tenants) && $tenants > 0)
                                <li>
                                    <a href="{{ route('portal.listusers') }}">{{ $tenants }}</a> tenant(s) have
                                    requested to be approved.<a href="{{ route('portal.list-tenants') }}"> Approve
                                        here</a>
                                </li>
                            @endif
                            @if(isset($mallUsers) && $mallUsers > 0)
                                <li>
                                    <a href="{{ route('portal.listusers') }}">{{ $mallUsers }}</a> mall user(s) have
                                    requested to be approved. <a href="{{ route('portal.listusers') }}"> Approve
                                        here</a>
                                </li>
                            @endif
                            @if(isset($subscribed))
                                <li><strong>{{ $subscribed }}</strong> &nbsp;&nbsp; Mallguide member(s) have subscribed
                                    to your movie schedule newsletter
                                </li>
                            @endif
                        </ul>
                    </div>
                @endif

                <div class="yellow control-panel">
                    <h4><i class="fa fa-bar-chart-o"></i> Your Quick Stats</h4>
                    <ul>
                        @if(isset($countShops))
                            <li>
                                <a href="{{ session()->has('activeChain') ? route('chainshop.chainstores') : route('shops.list') }}">{{ $countShops }}</a>
                                shop(s) are listed, <a
                                    href="{{ session()->has('activeChain') ? route('chainshop.create') : route('shops.register') }}">add
                                    a new one</a> or <a
                                    href="{{ session()->has('activeChain') ? route('chainshop.chainstores') : route('shops.list') }}">change
                                    existing details</a>.
                            </li>
                        @endif
                        @if(isset($countSchedules))
                            <li><strong>{{ $countSchedules }}</strong> &nbsp;&nbsp; Movie Schedule displayed for today ,
                                &nbsp;<a href="{{ route('moviehouse.addmovieschedule') }}">add schedule</a>&nbsp; or
                                &nbsp;<a href="{{ route('moviehouse.listmovieschedules') }}">list schedules</a>.
                            </li>
                        @endif
                        @if(isset($countEvents))
                            <li><a href="{{ route('mallevents.list') }}">{{ $countEvents }}</a> event(s) are currently
                                displayed. <a href="{{ route('mallevents.create') }}">Add a new one</a></li>
                        @endif
                        @if(isset($countExhibitions))
                            <li><a href="{{ route('exhibitions.list') }}">{{ $countExhibitions }}</a> exhibition(s) are
                                currently displayed. <a href="{{ route('exhibitions.create') }}">Add a new one</a></li>
                        @endif
                        @if(isset($countPromotions))
                            <li>
                                <a href="{{ session()->has('activeChain')? route('chainshop.promotions'):route('mallpromotions.list') }}">{{ $countPromotions }}</a>
                                promotion(s) are currently displayed. <a
                                    href="{{ session()->has('activeChain')? route('chainshop.createpromotion'):route('mallpromotions.create') }}">Add
                                    a new one</a></li>
                        @endif
                        @if(isset($countCompetitions))
                            <li><a href="{{ route('competitions.list') }}">{{ $countCompetitions }}</a> competition(s)
                                are currently displayed. <a href="{{ route('competitions.create') }}">Add a new one</a>
                            </li>
                        @endif
                        @if(isset($countJobs) && Sentinel::hasAccess('manager'))
                            <li><a href="{{ route('jobs.index') }}">{{ $countJobs }}</a> job(s) are currently available.
                                <a href="{{ route('jobs.create') }}">Add a new one</a></li>
                        @elseif(isset($countJobs) && Sentinel::hasAccess('user'))
                            <li>
                                <a href="{{ session()->has('activeChain')? route('chainshop.jobs'):route('store.jobs') }}">{{ $countJobs }}</a>
                                job(s) are currently available. <a href="{{ route('jobs.create') }}">Add a new one</a>
                            </li>
                        @endif
                        @if(isset($countGiftIdeas) && isset($activeShopID))
                            <li><a href="{{ route('store.giftideas') }}">{{ $countGiftIdeas }}</a> gift idea(s) are
                                currently available. <a href="{{ route('jobs.create') }}">Add a new one</a></li>
                        @endif
                    </ul>

                </div><!--yellow -->

                @if(Sentinel::hasAccess('manager') && isset($activeMallID))


                    <h4 class="dotted-border">How does it work?</h4>


                    Mallguide lists your Mall and stores to more than 300,000 visitors a month, at no cost to you.
                    It's our way at FGX Studios to give back to the retail community, that we have been servicing for
                    the past decade.
                    Keep your content current, and make use of the extra exposure and feet in your Mall.

                    <h4 class="dotted-border">Approving Information on Mallguide</h4>

                    Mallguide relies on you as the Mall's Marketing team or Centre management to check, and approve all
                    store information posted from the website.
                    Please ensure that all information is correct, as this will ensure your listing stays current and
                    helpful to the South African shoppers.

                    <h4 class="dotted-border">Approval hold</h4>

                    Your Shops can request you to release the "Approval hold" on any updates made by them. All their
                    Shop updates will feature automatically once posted on Mallguide.

                    <h4 class="dotted-border">Email &amp; SMS Communication</h4>

                    As part of the Mallguide toolkit, you are able to keep in touch with your tenants and shoppers using
                    bulk emails or SMS's. To make use of this service, you will require
                    3 Modules to be installed for your Mall;

                    <h4 class="dotted-border">Member Module</h4>

                    Manage and create various communication groups by adding member contact information. Reast easy
                    knowing that all your CPA requirements are being taken care of by making
                    allowances for member database unsubscribes in real time. Export your members to an Excel
                    spreadsheet at any time. Keep in touch with your base using the Member Module.

                    <h4 class="dotted-border">Communication Module</h4>

                    The Communication Module works in tandem with the Member Module. Once you have loaded all your
                    members to your various groups, you can start communicating to them by
                    buying Communication Credits. Send personalised Emails and SMS's to your database. Commision email
                    templates from FGX Studios, that once loaded will enable you to compose
                    monthly, regular communication or a news flash with just a couple of clicks.

                    <h4 class="dotted-border">Email Templates</h4>

                    Make your communication attractive with a Email Template. Order these to be customed designed for
                    your needs, in your individual look and feel. There are no limits to the
                    amount of templates you can load.
                @endif

            </div><!-- span7 -->

            <div class="span4">

                <div class="control-panel">
                    <?php
                    $user = Sentinel::getUser();
                    $userID = $user->id;
                    ?>
                    <h4><i class="fa fa-user"></i> About You</h4>
                    <ul>
                        <li><a href="{{ route('user.updatepassword',$userID) }}">Update your password</a></li>
                        <li><a href="{{ route('user.updatedetails') }}">Update your personal information</a></li>
                        @if(isset($activeMallID) && Sentinel::hasAccess('manager'))
                            <li><a href="{{ route('portal.malldetails',$activeMallID) }}">Update your current mall
                                    details</a></li>
                        @endif
                    </ul>

                    <!-- Mall And User update section -->
{{--                    @if(isset($smsCredits))--}}
{{--                        <h4><i class="fa fa-email-o"></i><i class="fa fa-phone"></i> Communication with Tenants</h4>--}}
{{--                        <ul>--}}
{{--                            <li>You have &nbsp; {{ $smsCredits }} &nbsp; SMS credits left, &nbsp;<a--}}
{{--                                    href="{{ route('manager.buysmscredits') }}"><strong><u>order more</u></strong></a>--}}
{{--                            </li>--}}
{{--                            <li><a href="{{ route('manager.getfiltermall') }}">Compose SMS Message for Tenants</a></li>--}}
{{--                            <li><a href="{{ route('manager.smsreports') }}">Tenant SMS Message Reports</a></li>--}}
{{--                        </ul>--}}
{{--                    @endif--}}

                    <h4><i class="fa fa-share-alt"></i> Quick Links</h4>
                    <ul>
                        @if( isset($activeShopID))
                            <li>
                                <a href="{{ route('shops.update',$activeShopID) }}">Update Shop Details</a>&nbsp;
                            </li>
                        @endif
                        @if(isset($activeMallID) && Sentinel::hasAccess('manager'))
                            <li style="margin-bottom: 20px;">
                                <a href="{{ route('shops.export',$activeMallID) }}">Download Shop Excel Sheet</a>
                            </li>

                            <li>
                                <a href="{{ route('exhibitions.create') }}">Add an Exhibition</a>&nbsp; | &nbsp;
                                <a href="{{ route('exhibitions.list') }}">List Exhibitions</a>
                            </li>
                            <li>
                                <a href="{{ route('mallevents.create') }}">Add an Event</a>&nbsp; | &nbsp;
                                <a href="{{ route('mallevents.list') }}">List Events</a>
                            </li>

                            <li>
                                <a href="{{ route('shops.register') }}">Add a Shop</a>&nbsp; | &nbsp;
                                <a href="{{ route('shops.list') }}">List Shops</a>
                            </li>
                        @endif

                        <li>
                            <a href="{{ route('competitions.create') }}">Add a Competition</a>&nbsp; | &nbsp;
                            <a href="{{ route('competitions.list') }}">List Competitions</a>
                        </li>

                        @if(isset($activeMallID) && Sentinel::hasAccess('manager'))
                            <li>
                                <a href="{{ route('mallpromotions.create') }}">Add a Promotion</a>&nbsp; | &nbsp;
                                <a href="{{ route('mallpromotions.list') }}">List Promotions</a>
                            </li>
                            <li>
                                <a href="{{ route('jobs.create') }}">Add a Job</a>&nbsp; | &nbsp;
                                <a href="{{ route('jobs.index') }}">List Jobs</a>
                            </li>
                            <li>
                                <a href="{{ route('store.addgiftidea') }}">Add a Gift Idea</a>&nbsp; | &nbsp;
                                <a href="{{ route('store.giftideas') }}">List Gift Ideas</a>
                            </li>
                        @endif
                        @if(Sentinel::hasAccess('independent'))
                            <li>
                                <a href="{{ route('moviehouse.addmovieschedule') }}">Add a Movie Schedule</a>&nbsp; |
                                &nbsp;
                                <a href="{{ route('moviehouse.listmovieschedules') }}">List Movie Schedules</a>
                            </li>
                        @endif
                        @if(isset($activeChain))
                            <li>
                                <a href="{{ route('chainshop.create') }}">Add a Shop</a>&nbsp; | &nbsp;
                                <a href="{{ route('chainshop.chainstores') }}">List Shops</a>
                            </li>
                            <li>
                                <a href="{{ route('chainshop.createjob') }}">Add a Job</a>&nbsp; | &nbsp;
                                <a href="{{ route('chainshop.jobs') }}">List Jobs</a>
                            </li>
                            <li>
                                <a href="{{ route('chainshop.createpromotion') }}">Add a Promotion</a>&nbsp; | &nbsp;
                                <a href="{{ route('chainshop.promotions') }}">List Promotions</a>
                            </li>
                            <li>
                                <a href="{{ route('store.addgiftidea') }}">Add a Gift Idea</a>&nbsp; | &nbsp;
                                <a href="{{ route('store.giftideas') }}">List Gift Ideas</a>
                            </li>
                        @endif

                        @if(isset($activeShopID))
                            <li>
                                <a href="{{ route('mallpromotions.create') }}">Add a Promotion</a>&nbsp; | &nbsp;
                                <a href="{{ route('mallpromotions.list') }}">List Promotions</a>
                            </li>
                            <li>
                                <a href="{{ route('store.createStoreJob') }}">Add a Job</a>&nbsp; | &nbsp;
                                <a href="{{ route('store.jobs') }}">List Jobs</a>
                            </li>
                            <li>
                                <a href="{{ route('store.addgiftidea') }}">Add a Gift Idea</a>&nbsp; | &nbsp;
                                <a href="{{ route('store.giftideas') }}">List Gift Ideas</a>
                            </li>
                        @endif
                        <li>
                            <a href="{{ route('booking.featured') }}">Book & View Featured Spots</a>
                        </li>
                    </ul>
                    <h4><i class="fa fa-user"></i> Users</h4>
                    <ul>
                        <li>
                            @if(isset($activeShopID))
                                <a href="{{ route('store.adduser') }}">Add a User </a>&nbsp; | &nbsp;
                            @endif
                            @if(isset($activeChain))
                                <a href="{{ route('store.adduser') }}">Add a User </a>&nbsp; | &nbsp;
                            @endif
                            @if(isset($activeMallID))
                                <a href="{{ route('portal.adduser') }}">Add a User </a>&nbsp; | &nbsp;
                            @endif
                            @if(isset($activeMallID))
                                <a href="{{ route('portal.listusers') }}"> List Users</a>
                            @endif
                            @if(isset($activeChain))
                                <a href="{{ route('store.userlist') }}"> List Users</a>
                            @endif
                            @if(isset($activeShopID))
                                <a href="{{ route('store.userlist') }}"> List Users</a>
                            @endif
                        </li>
                    </ul>

                    <div>
                        <h4><i class="fa fa-question"></i> Information posted by Mallguide</h4>
                        <ul>
                            @if(Sentinel::hasAccess('manager'))
                                <li><a href="{{ route('portal.marketinginfo') }}">Send electronic Newsletter to your
                                        Shopper/Client Database?</a></li>
                            @endif
                            <li><a href="{{ route('portal.competitioninfo') }}">Standard Terms and Conditions for
                                    Competitions and Entrant information</a></li>
                        </ul>
                    </div>
                </div><!-- control panel -->
            </div><!--span6 -->
        </div><!--row -->
    </div>









@stop
