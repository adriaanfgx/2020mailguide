@extends('layouts.backpages')
@section('title')
    @parent
    Booking Schedule
@stop

@section('content')
    <link href="{{ asset('css/fullcalendar.css') }}" rel="stylesheet">

    <h2>Booking Schedule</h2>

    <div class="row-fluid">
        <div class="blog-content span12">
            <div class="span4">
                <div class="control-group {{ ($errors->has('bookingWhat') ? 'error' : '') }}">
                    <label for="bookingWhat"><strong>What featured spot would you like to book?</strong></label>
                    <div class="controls">
                        {{ Form::select('bookingWhat',$featuredOptionsDropdown,'Mall',array('id'=>'bookingWhat')) }}
                    </div>
                    <span
                        class="help-block">{{ ($errors->has('bookingWhat') ? $errors->first('bookingWhat') : '') }}</span>
                </div>
            </div>
            <div class="span4" id="shop_div">
                <div class="control-group">
                    <label for="shopMGID"><strong>Select Shop: </strong></label>
                    <div class="controls">
                        @if(isset($shopList))
                            {{ Form::select('shopMGID',$shopList, NULL,array('id'=>'theShopID')) }}
                        @elseif(isset($activeShopID))
                            {{ Form::hidden('shopMGID',$activeShopID,array('id'=>'theShopID')) }}

                        @else
                            <select name="shopMGID" id="theShopID">
                            </select>
                        @endif
                    </div>
                </div>
            </div>

            <div class="row-fluid">

            </div>
            <div class="row-fluid">

                <div class="span10">
                    <div id="eventCalendarDefault"></div>
                </div>
                <div class="span2">
                    <div class="alert-info alert">Bookings work in weeks. Click on any open day and you will book the
                        next 7days including the day clicked on if this week doesn't already have a booking
                    </div>
                    <div class="alert-info alert">Other people's bookings
                        <div class="label-inverse label smallerPadding"></div>
                    </div>
                    <div class="alert-info alert">Your un-approved bookings
                        <div class="label-info label smallerPadding"></div>
                    </div>
                    <div class="alert-info alert">Your approved bookings
                        <div class="label-success label smallerPadding"></div>
                    </div>
                </div>
            </div>


        </div>
        <!--remove booking modal-->
        <div class="modal hide fade" id="removeBooking">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Remove Booking</h3>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to remove this booking?</p>
            </div>
            <div class="modal-footer">
                <a id="deleteButton" data-link="" class="btn btn-warning">Remove Booking</a>
                <a href="#" class="btn" data-dismiss="modal">Close</a>
            </div>
        </div>

        <!--choose what to book modal-->
        <div class="modal hide fade" id="chooseBooking">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Add Booking For</h3>
            </div>
            <div class="modal-body">
                <div class="row-fluid" id="the_malls_drop">
                    <div class='span12' id='choose_div'>
                        <div class='control-group'>
                            <label for='shopMGID'><strong>Select Shop</strong></label>
                            <div class='controls'>
                                <select name="shopMGID" id="subShopID">

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid" id="the_div">
                </div>

                <div class="modal-body">
                    <div class="row-fluid">
                        <div class='span11' id='mallDiv'>
                            <div class='control-group'>
                                <label><strong>Mall Name</strong></label>
                                <div class='mallName'>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class='span11' id='typeDiv'>
                            <div class='control-group'>
                                <label><strong class="type"></strong></label>
                                <div class='typeLabel'>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class='span6' id='choose_div'>
                            <div class='control-group'>
                                <label><strong>Start Date</strong></label>
                                <div class='startDateDisplay'>
                                </div>
                            </div>
                        </div>
                        <div class='span5' id='choose_div'>
                            <div class='control-group'>
                                <label><strong>End Date</strong></label>
                                <div class='endDateDisplay'>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class='span6' id='choose_div'>
                            <div class='control-group'>
                                <label><strong>Price excl vat</strong></label>
                                <div class='bookingPrice'>
                                </div>

                            </div>
                        </div>
                        <div class='span5' id='choose_div'>
                            <div class='control-group'>
                                <label><strong>Final Price</strong></label>
                                <div class='taxPrice'>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::hidden('the_start',NULL,array('id'=>'the_start')) }}
                {{ Form::hidden('the_end',NULL,array('id'=>'the_end')) }}
            </div>
            <div class="modal-footer">
                <a id="addBooking" data-link="" class="btn btn-warning">Add Booking</a>
                <a href="#" class="btn" data-dismiss="modal">Close</a>
            </div>
        </div>
        <div class="modal hide fade" id="bookingInfo">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Booking Info</h3>
            </div>
            <div class="modal-body">
                <div class="row-fluid">
                    <div class='span11' id='mallDiv'>
                        <div class='control-group'>
                            <label><strong>Mall Name</strong></label>
                            <div class='mallName'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class='span11' id='eventDiv'>
                        <div class='control-group'>
                            <label><strong class="type"></strong></label>
                            <div class='typeLabel'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class='span6' id='choose_div'>
                        <div class='control-group'>
                            <label><strong>Start Date</strong></label>
                            <div class='startDateDisplay'>
                            </div>
                        </div>
                    </div>
                    <div class='span5' id='choose_div'>
                        <div class='control-group'>
                            <label><strong>End Date</strong></label>
                            <div class='endDateDisplay'>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class='span6' id='choose_div'>
                        <div class='control-group'>
                            <label><strong>Price excl vat</strong></label>
                            <div class='bookingPrice'>
                            </div>

                            {{ Form::hidden('price',NULL,array('id'=>'price')) }}
                        </div>
                    </div>
                    <div class='span5' id='choose_div'>
                        <div class='control-group'>
                            <label><strong>Final Price</strong></label>
                            <div class='taxPrice'>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a data-link="" class="preDeleteButton btn btn-warning">Remove Booking</a>
                <a href="#" class="btn" data-dismiss="modal">Close</a>
            </div>
        </div>
        <!--choose what to book modal-->
        <div class="modal hide fade" id="chooseBooking">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3>Add Booking For</h3>
            </div>
            <div class="modal-body">
                <div class="row-fluid" id="the_malls">
                    <div class='span12' id='choose_div'>
                        <div class='control-group'>
                            <label for='mallID'><strong>Select Mall</strong></label>
                            <div class='controls'>
                                <select name="mallID" id="theMallID">

                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid" id="the_div">
                </div>
                {{ Form::hidden('the_start',NULL,array('id'=>'the_start')) }}
                {{ Form::hidden('the_end',NULL,array('id'=>'the_end')) }}
            </div>
            <div class="modal-footer">
                <a id="addBooking" data-link="" class="btn btn-warning">Add Booking</a>
                <a href="#" class="btn" data-dismiss="modal">Close</a>
            </div>
        </div>
    </div>

@stop

@section('exScript')
    <script src="{{ asset('js/moment.min.js')}}"></script>


    <script src="{{ asset('js/fullcalendar.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#mall_div").hide();
            $("#shop_div").hide();

            //create calendar
            $("#eventCalendarDefault").fullCalendar(
                {
                    //add the events
                    events:
                        {
                            url: '/admin/bookingdata',
                            data: function () {
                                return {
                                    bookingWhat: $("#bookingWhat").val()
                                };
                            },
                            type: "GET"
                        },
                    //on click event
                    eventClick: function (calEvent) {
                        //if this is my booking
                        if (calEvent.color == '#G1G' || calEvent.color == '#393') {
                            //if booking is still editable
                            $.get("/admin/checkStart",
                                {
                                    start: calEvent.start.toDate() / 1000,
                                    end: calEvent.end.toDate() / 1000 - 1
                                }).done(function (data) {
                                if (data == "valid") {
                                    var bookingID = calEvent.id
                                    $("#deleteButton").data("link", "/admin/bookings/remove/" + bookingID);
                                    $("#the_id").val(bookingID);

                                    $('#mallDiv').hide()

                                    $.get("/booking/info/" + bookingID).done(function (data) {
                                        var json = JSON.parse(data);
                                        $('.startDateDisplay').html(json.startDate);
                                        $('.endDateDisplay').html(json.endDate);
                                        $('.bookingPrice').html('R' + json.bookingPrice);
                                        $('.taxPrice').html('R' + json.taxPrice);
                                        $('.type').html(json.type);
                                        if (json.mallName) {
                                            $('.mallName').html(json.mallName)
                                            $('#mallDiv').show()
                                        }
                                        $('.typeLabel').html(json.typeLabel);
                                        $('#bookingInfo').modal('show');

                                    });

                                } else {
                                    alert(data);
                                }
                            });
                        }
                    },
                    selectable: true,
                    allDayDefault: true,
                    selectHelper: true,
                    //on selecting a date
                    select: function (start) {
                        var the_start = start.toDate().getTime() / 1000
                        $('#the_start').val(the_start);
                        //check if there is no conflict with other bookings
                        $.get("/admin/checkDates",
                            {
                                start: the_start,
                                bookingWhat: $("#bookingWhat").val()

                            }).done(function (data) {
                            if (data == "valid") {
                                var bookingWhat = $("#bookingWhat").val();

                                //if booking needs an extra dropdown
                                if (bookingWhat != 'Mall' && bookingWhat != 'Shop') {

                                    @if(Session::has('activeChain'))
                                    $.get("/admin/bookingShopsList",
                                        {
                                            start: the_start,
                                            bookingWhat: $("#bookingWhat").val()
                                        }).done(function (data1) {
                                        //alert('hi');
                                        $('#chooseBooking').modal('show');
                                        $('#subShopID').html(data1);
                                    });
                                    @else
                                    $('#the_malls_drop').hide();

                                    @endif
                                    getDropdown(the_start);
                                    newEventDisplay(the_start);
                                    $('#chooseBooking').modal('show');
                                } else {
                                    newEventDisplay(the_start);
                                    $('#the_malls_drop').hide();
                                    $('#chooseBooking').modal('show');
                                }
                            } else {
                                alert(data)
                            }
                        });


                    },
                    editable: false,
                    draggable: false,
                    ignoreTimezone: true,
                    eventLimit: true // allow "more" link when too many events
                });

            @if(!isset($activeShopID))
            if ($("#bookingWhat").val() == 'Shop') {
                $("#mall_div").show();
                $("#shop_div").show();
            }
            @endif

            $("#bookingWhat").change(function () {
                if ($(this).val() == "Shop") {
                    @if(!isset($shopList) && !isset($activeShopID))
                    $.get("/admin/bookingShops",
                        {
                            mallID: $('#mallID').val()
                        }).done(function (data) {
                        //alert('hi');
                        $('#theShopID').html(data);

                    });
                    $("#mall_div").show();
                    $("#shop_div").show();
                    $("#mallID").change(function () {
                        $.get("/admin/bookingShops",
                            {
                                mallID: $(this).val()
                            }).done(function (data) {
                            //alert('hi');
                            $('#theShopID').html(data);

                        });
                    });
                    @elseif(!isset($activeShopID))
                    $("#shop_div").show();
                    @endif

                } else {
                    $("#shop_div").hide();
                    $("#mall_div").hide();
                }
                $('#eventCalendarDefault').fullCalendar('refetchEvents');
            });

            $('#deleteButton').click(function () {
                var href = $("#deleteButton").data('link');
                $.post(href).done(function (data) {
                    data = JSON.parse(data);

                    $('#removeBooking').modal('hide');
                    $('#eventCalendarDefault').fullCalendar('refetchEvents'); // stick? = true

                    if (data.removed != 1) {
                        var message = data.error;
                        alert(message);
                    }
                });
            });
            $("#subShopID").change(function () {
                //alert($(this).val());
                getDropdown($('#the_start').val());
            });
            if ($("#bookingWhat").val() == "Shop") {
                @if(!isset($shopList) && !isset($activeShopID))
                $.get("/admin/bookingShops",
                    {
                        mallID: $("#mallID").val()
                    }).done(function (data) {
                    //alert('hi');
                    $('#theShopID').html(data);

                });
                $("#mallID").change(function () {
                    $.get("/admin/bookingShops",
                        {
                            mallID: $(this).val()
                        }).done(function (data) {
                        //alert('hi');
                        $('#theShopID').html(data);

                    });
                });
                @elseif(!isset($activeShopID))
                $("#shop_div").show();
                @endif
            }
            $('#addBooking').click(function () {
                addEvent($('#the_start').val());
            });

            var today = new Date().getTime() / 1000;
            $('.datetimepicker4').datetimepicker(
                {
                    pickTime: false,
                    startDate: today
                }).on('changeDate', function (e) {
                $(this).datetimepicker('hide');
            });

        });

        function addEvent(start) {
            $.post("/admin/bookings/create",
                {
                    startDate: start,
                    bookingWhat: $("#bookingWhat").val(),
                    mallID: $("#mallID").val(),
                    eventsMGID: $("#eventsMGID").val(),
                    shopMGID: $("#theShopID").val(),
                    price: $('#price').val(),
                    promotionsMGID: $("#promotionsMGID").val(),
                    formID: $("#formID").val()
                }
            ).done(function (data) {
                data = JSON.parse(data);
                //console.log(data);
                if (data.valid == 0) {
                    var message = data.error;
                    alert(message);
                } else if (data.valid == 1) {
                    $('#eventCalendarDefault').fullCalendar('refetchEvents');
                }
                $('#chooseBooking').modal('hide');
            })
                .fail(function (jqXHR, ajaxOptions, thrownError) {
                    alert('No response from server');
                });
        }

        function newEventDisplay(start) {
            $('#mallDiv').hide()
            $('#typeDiv').hide()

            $.get("/booking/displayNew",
                {
                    startDate: start,
                    bookingWhat: $("#bookingWhat").val(),
                    mallID: $("#mallID").val(),
                    shopMGID: $("#theShopID").val()
                }
            ).done(function (data) {
                var json = JSON.parse(data);
                $('.startDateDisplay').html(json.startDate);
                $('.endDateDisplay').html(json.endDate);
                $('.bookingPrice').html('R' + json.bookingPrice);
                $('.taxPrice').html('R' + json.taxPrice);
                $('#price').val(json.bookingPrice)
                if (json.mallName) {
                    $('.mallName').html(json.mallName)
                    $('#mallDiv').show()
                }
                if (json.type) {
                    $('.type').html(json.type);
                    $('.typeLabel').html(json.typeLabel);
                    $('#typeDiv').show()

                }
                $('.typeLabel').html(json.typeLabel);
            })
                .fail(function (jqXHR, ajaxOptions, thrownError) {
                    alert('No response from server');
                });
        }

        $('.preDeleteButton').click(function () {
            $('#bookingInfo').modal('hide');
            $('#removeBooking').modal('show');
        });

        function getDropdown(start) {
            //alert($("#subShopID").val());
            $.get("/admin/bookingDropdown",
                {
                    start: start,
                    bookingWhat: $("#bookingWhat").val(),
                    mallID: $("#theMallID").val(),
                    shopMGID: $("#subShopID").val()
                }).done(function (data) {
                $('#the_div').html(data);
            });
        }
    </script>
@stop
