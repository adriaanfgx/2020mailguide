@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('content')

    <div class="row-fluid">
        <div class="blog-content">
            <h4 class="hborder">Job Listings
                <a id="addJob" href="/jobs/portalCreate" class="btn btn-info pull-right"><i class="fa fa-plus" "=""></i>
                    Add New</a>
            </h4>

            <div class="span3 pull-right">
                <div class="overlay-wrapper"></div>

            </div>  <!-- span6 -->


            <div class="row-fluid">
                <div class="span12">
                    <table class="table table-border">
                        <thead>
                        <tr>
                            <th>Shop Name</th>
                            <th>Job Title</th>
                            <th>Closing Date</th>
                            <th>Show Contact Info</th>
                            <th>Display</th>
                            <th>Applicants</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody id="shop_table">
                        @foreach($jobs as $job)
                            <tr>
                                <td>{{ $job->name }}</td>
                                <td>{{ $job->title }}</td>
                                <td>{{ $job->endDate }}</td>
                                <td>{{ displayText($job->showInfo) }}</td>
                                <td>{{ displayText($job->display) }}</td>
                                <td>
                                    {{ $job->applicants }} &nbsp;

                                </td>
                                <td>
                                    @if( $job->applicants > 0 )
                                        <a href="{{ route('jobs.applicants',$job->jobID) }}"
                                           class="btn btn-info btn-mini"><i class="icon-white icon-eye-open"></i>
                                            Applicants</a>&nbsp; |&nbsp;
                                    @endif
                                    <a href="{{ route('jobs.edit',$job->jobID) }}" class="btn btn-info btn-mini"><i
                                            class="icon-white icon-pencil"></i> Edit</a>&nbsp; | &nbsp;
                                    <a class="btn btn-danger btn-mini action_delete"
                                       href="{{ route('jobs.delete',$job->jobID) }}" id="{{ $job->jobID }}"><i
                                            class="icon-white icon-trash"></i> Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div><!--span12 -->
            </div>
        </div>
    </div>
@stop
@section('exScript')
    <script src="{{ asset('js/bootbox.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".action_delete").click(function (e) {

                e.preventDefault();
                var job = $(this).attr("id");

                bootbox.confirm("Are you sure you want to delete this entry ? ");

                $(".modal a.btn-primary").click(function () {
                    $.ajax({
                        type: "POST",
                        url: '/jobs/delete/' + job,
                        data: {
                            '_token': "{{ csrf_token()  }}"
                        },
                        success: function () {
                            location.reload();
                        }
                    });
                });
            });
        });
    </script>
@stop

