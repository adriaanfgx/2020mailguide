@extends('layouts.backpages')
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Featured Jobs</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li><a href="/jobs/index/{{$job->mall->mallID}}">Jobs</a><span class="divider">/</span></li>
                    <li class="active">Job Applicants</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div><!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">

    <div class="span12 pull-left">
        <h4 class="hborder">Job Applicants for {{$job->title}} @ {{$job->shop->name}}</h4>
    </div>


    <div class="row-fluid">
        <div class="span12">
            <table class="table table-border">
                <thead>
                <tr>
                    <th>Applicant</th>
                    <th>Cell</th>
                    <th>E-mail</th>
                    <th>Date Applied</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach($applicants as $applicant)
                <tr>
                    <td>{{ $applicant->name }} &nbsp; {{ $applicant->surname }}</td>
                    <td>{{ $applicant->cell }}</td>
                    <td>{{ $applicant->email }}</td>
                    <td>{{ $applicant->dateAdded }}</td>
                    <td>
                       <a href="{{ route('applicant.view',$applicant->jobApplicantID) }}" class="btn btn-info btn-mini"><i class="icon-white icon-eye-open"></i> View</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

        </div><!--span12 -->
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">

</script>
@stop

