@extends('layouts.backpages')
@section('title')
@parent
Member Zone
@stop

@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div><!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">

    <div class="span12 pull-left">
        <h4 class="hborder">Applicant for {{$applicant->application->job->title}} @ {{$applicant->application->job->shop->name}}</h4>

    </div>


    <div class="clear"></div>

    <div class="row-fluid">
        <div class="span12">

            <div class="row my_group">
                <label class="my_label">First name</label>
                {{ $applicant->name }}
            </div>

            <div class="row my_group">
                <label class="my_label">Surname</label>
                {{ $applicant->surname }}
            </div>
            <div class="row my_group">
                <label class="my_label">Home Tel</label>
                    {{ $applicant->telephone }}
            </div>
            <div class="row my_group">
                <label class="my_label">Cell</label>
                <div class="controls">
                {{ $applicant->cell }}
                </div>
            </div>
            <div class="row my_group">
                <label class="my_label">Email</label>
                {{ $applicant->email }}
            </div>
            <div class="row my_group">
                <label class="my_label2">CV</label>
                @if(!empty( $applicant->cv ) )
                <a class="btn btn-info" target="_blank" href="{{ route('jobs.cv',$applicant->cv) }}"><i class="icon-download icon-white"></i> Download CV</a>
                @endif
            </div>

        </div><!--span12 -->
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">

</script>
@stop

