@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Edit Job </h1>

                    <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp;
                        Communication</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')

@stop

@section('content')

    <div class="row-fluid">
        <div class="blog-content">
            <h3>Mall</h3>
            {{ Form::model($job, array('route' => array('jobs.postEdit', $job->jobID),'method'=>'post','name' => 'jobEdit', 'id' => 'jobEdit', 'class' => 'form-inline')) }}
            {{ Form::hidden('mallID',$job->mallID) }}
            {{ Form::hidden('referer',URL::previous()) }}
            <div class="span4">
                <h4 class="dotted-border">Update Job Details</h4>
                <div class="control-group">
                    <label for="title">Position*</label>

                    <div class="controls">
                        {{ Form::text('title',null) }}
                        <span class="help-inline"></span>
                        <span class="text-error">{{ ($errors->has('title') ? $errors->first('title') : '') }}</span>

                    </div>
                </div>
                <div class="control-group">
                    <label for="salary">Salary</label>

                    <div class="controls">
                        {{ Form::text('salary') }}
                        <span class="help-inline"></span>
                    </div>
                </div>
                <div class="input-append datetimepicker4">
                    <div class="control-group {{ ($errors->has('startDate') ? 'error' : '') }}">
                        <label for="startDate" class="control-label">Starting Date</label>
                        <div class="controls">
                            {{ Form::text('startDate',null,array('class' => 'span12','data-format'=>'yyyy-MM-dd','readonly')) }}
                            <span class="add-on">
	                            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                            </i>
	                        </span>
                            <span
                                class="help-block">{{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}</span>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
                <div class="input-append datetimepicker4">
                    <div class="control-group">
                        <label for="endDate">Closing Date*</label>
                        <div class="controls">
                            {{ Form::text('endDate',null,array('class' => 'span12','data-format'=>'yyyy-MM-dd','readonly')) }}
                            <span class="add-on">
	                            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
	                            </i>
	                        </span>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label for="refNum">Ref No</label>

                    <div class="controls">
                        {{ Form::text('refNum') }}
                        <span class="help-inline"></span>
                    </div>
                </div>
                <div class="control-group">
                    <label for="description">Description*</label>

                    <div class="controls">
                        {{ Form::textarea('description',null,array('class'=>'txtbox','rows'=>'3')) }}
                    </div>
                    <span
                        class="text-error">{{ ($errors->has('description') ? $errors->first('description') : '') }}</span>

                </div>
            </div>
            <!--span4-->

            <div class="span4">
                <h4 class="dotted-border">Contact Details</h4>

                <div class="control-group">
                    <label for="contactPerson">Contact Person</label>

                    <div class="controls">
                        {{ Form::text('contactPerson') }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="cell">Cell</label>

                    <div class="controls">
                        {{ Form::text('cell') }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="telephone">Tel</label>

                    <div class="controls">
                        {{ Form::text('telephone') }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="fax">Fax</label>

                    <div class="controls">
                        {{ Form::text('fax') }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="email">Email</label>

                    <div class="controls">
                        {{ Form::text('email') }}
                    </div>
                </div>
            </div>
            <!--span4-->

            <div class="span3">
                <h4 class="dotted-border">Settings</h4>

                <div class="control-group">
                    <label>Must contact details for job be displayed?</label>

                    <div class="controls">
                        Yes {{ Form::radio('showInfo','Y') }} &nbsp;
                        No {{ Form::radio('showInfo','N') }}
                    </div>
                </div>

                <div class="control-group">
                    <label>Must job be displayed?</label>

                    <div class="controls">
                        Yes {{ Form::radio('display','Y') }} &nbsp;
                        No {{ Form::radio('display','N') }}
                    </div>
                </div>

                <br/>
                <button class="submit reg-btn" id="add_review">Submit Details</button>

            </div>
            <!--span4-->
            </form>
            <div class="clear"></div>

        </div><!--/span12-->
    </div><!--/span12-->

@stop

@section('exScript')

    <script type="text/javascript">

        $(function () {
            $('.datetimepicker4').datetimepicker({
                pickTime: false
            });
        });
    </script>
@stop
