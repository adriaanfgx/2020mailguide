@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Add Job </h1>

                    <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp;
                        Communication</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')
@stop

@section('content')

    <div class="row-fluid ">
        <div class="blog-content">
            <div class="white-bg">
                <h3>Shops - Add A Job</h3>

                Looking for the best staff to fill your available positions?
                Let Mallguide assist you by matching our database of job seekers to your location and required skills.
                Have the pick
                of the crop with this free online service...

                <h4 class="dotted-border">How Does It Work?</h4>
                <ul>
                    <li>You add a new job online</li>
                    <li>Job seekers register with Mallguide, and upload their CV's</li>
                    <li>The job seekers browse the list of available positions and apply online</li>
                    <li>You are notified of applications, click on the "<a href="{{ route('portal.index') }}">Your
                            Member Page</a>"
                        button above
                    </li>
                    <li>You screen and approve applicants based on their submissions and CV's</li>
                    <li>Approved applicants are sent your direct contact details for an interview</li>
                    <li>Your store prospers under fine management and capable staff!</li>
                </ul>


                <form action="{{ route('jobs.postCreate') }}" method="post" name="jobCreate" id="jobCreate"
                      class="form-inline">
                    @csrf
                    <div class="span4">
                        <h4 class="dotted-border">Job Details</h4>

                        <div class="control-group">
                            <label for="mall">Mall*</label>

                            <div class="controls">
                                {{ Form::text('mall',isset($mall->name) ? $mall->name: '' ,array('readonly')) }}
                                {{ Form::hidden('mallID', isset($mall->mallID) ? $mall->mallID: '' ) }}
                                {{ Form::hidden('referer',URL::previous()) }}
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="shopMGID">Shop*</label>

                            <div class="controls">
                                {{ Form::select('shopMGID', isset($shops) ? $shops: [] ) }}
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="title">Position*</label>

                            <div class="controls">
                                {{ Form::text('title') }}
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="salary">Salary</label>

                            <div class="controls">
                                {{ Form::text('salary') }}
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="input-append datetimepicker4">
                            <div class="control-group">
                                <label for="startDate">Starting Date</label>
                                <div class="controls">
                                    {{ Form::text('startDate',null,array('class' => 'span12','data-format'=>'yyyy-MM-dd','readonly')) }}
                                    <span class="add-on">
		                            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
		                            </i>
		                        </span>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="input-append datetimepicker4">
                            <div class="control-group">
                                <label for="endDate">End Date*</label>
                                <div class="controls">
                                    {{ Form::text('endDate',null,array('class' => 'span12','data-format'=>'yyyy-MM-dd','readonly')) }}
                                    <span class="add-on">
		                            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
		                            </i>
		                        </span>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="refNum">Ref No</label>

                            <div class="controls">
                                {{ Form::text('refNum') }}
                                <span class="help-inline"></span>
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="description">Description*</label>

                            <div class="controls">
                                {{ Form::textarea('description',null,array('class'=>'txtbox','rows'=>'3')) }}
                            </div>
                        </div>
                    </div>
                    <!--span4-->

                    <div class="span4">
                        <h4 class="dotted-border">Contact Details</h4>

                        <div class="control-group">
                            <label for="contactPerson">Contact Person</label>

                            <div class="controls">
                                {{ Form::text('contactPerson') }}
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="cell">Cell</label>

                            <div class="controls">
                                {{ Form::text('cell') }}
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="telephone">Tel</label>

                            <div class="controls">
                                {{ Form::text('telephone') }}
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="fax">Fax</label>

                            <div class="controls">
                                {{ Form::text('fax') }}
                            </div>
                        </div>
                        <div class="control-group">
                            <label for="email">Email</label>

                            <div class="controls">
                                {{ Form::text('email') }}
                            </div>
                        </div>
                    </div>
                    <!--span4-->

                    <div class="span3">
                        <h4 class="dotted-border">Location Details</h4>

                        <div class="control-group">
                            <label>Must shop name and contact details for job be displayed?</label>
                            <div class="controls">
                                Yes {{ Form::radio('showInfo','Y',true) }} &nbsp;
                                No {{ Form::radio('showInfo','N') }}
                            </div>
                        </div>

                        <div class="control-group">
                            <label>Must Job be displayed ?</label>
                            <div class="controls">
                                Yes {{ Form::radio('display','Y',true) }} &nbsp;
                                No {{ Form::radio('display','N') }}
                            </div>
                        </div>
                        <br/>
                        <button class="submit reg-btn" id="add_review">Submit Details</button>

                    </div>
                    <!--span4-->
                </form>
                <div class="clear"></div>
            </div>
        </div>
    </div><!--/span12-->

@stop

@section('exScript')

    <script type="text/javascript">

        $(function () {

            $('.datetimepicker4').datetimepicker({
                pickTime: false
            });

        });
    </script>
@stop
