@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Markerting Information</h1>
                    <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp;
                        Communication</p>
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')
@stop

@section('content')

    <div class="row-fluid">
        <div class="span12 blog-content">

            <div class="hborder">
                <h4>Standard Terms and Conditions for Competitions & Entrant Information</h4>
            </div>

            <div class="row-fluid">
                <div class="span12">
                    <p>
                        Mallguide provides you with the functionality for running competitions online, for your mall or
                        shop FOR FREE.
                        You can also customise your entry form so you can get the exact information you want from your
                        entrants.
                        You can also view all your entrants and their info when you login.
                    </p>
                    <p>
                        If you want to add competition entrants to your communications database,
                        it's important to include something that TELLS them that this is what you're going to do with
                        their info.
                    </p>
                    <p>
                        We’ve written up some standard terms and conditions that you can copy and paste into your
                        competition description and edit according to your needs.
                        By these terms and conditions, entrants who enter your competition automatically agree to
                        receive further communications from you,
                        from which they can choose to be unsubscribed at any time. If you have any questions regarding
                        this, be sure to send us an email with your queries.
                    </p>
                    <strong>
                        Download:&nbsp;<a href="{{ route('portal.compterms') }}">Competition_Standard_Terms_and_Conditions.pdf
                            (23 Kb)</a>
                    </strong>
                </div><!--span12 -->
            </div>
        </div>
    </div>
@stop
@section('exScript')

    <script type="text/javascript">
        $(document).ready(function () {

        });
    </script>
@stop

