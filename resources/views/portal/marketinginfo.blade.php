@extends('layouts.backpages')
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Markerting Information</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')
@stop

@section('content')
<div class="row-fluid">
	<div class="blog-content span12">
	    <div class="hborder">
			<h4>Want to send an electronic Newsletter to your Shopper/Client Database?</h4>
	    </div>

	    <div class="row-fluid">
	        <div class="span12">
	            <p>
	                Increase customer loyalty by providing relevant information to your customer database in a cost effective manner.
	                E-mail is probably the most cost effective method of advertising in getting your message across directly to your target audience.
	                Thus also creating brand awareness and brand loyalty.
	                In short, MESSAGE4U will provide a monthly or ad hoc service whereby a personalised Newsletter will be sent out to your customer database.
	            </p>
	            <p>
	                View our standard quotation for more details.
	            </p>
	            <strong>
	                Download:&nbsp;<a href="{{ route('portal.smsterms') }}">STANDARD_MALLS_Newsletter__SMS.pdf (206 Kb)</a>
	            </strong>
	        </div><!--span12 -->
	    </div>
    </div>
</div>
@stop
@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

    });
</script>
@stop

