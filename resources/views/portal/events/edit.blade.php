@extends('layouts.backpages')
@section('seo_meta')
    <meta name="description"
          content="">
    <meta name="keywords" content="">
@stop
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Edit Event </h1>

                    <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp;
                        Communication</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')
@stop

@section('content')
    <div class="blog-content container-fluid">
        <div class="row-fluid">
            <div class="biggerLeftMargin">
                <h3>Edit event</h3>

                Does your Mall have a current or upcoming Event that you would like to give greater exposure? Mallguide
                is just the
                place for you to do that,
                simply complete your Event details below and include it on the site for free...

                <h4 class="dotted-border">How Does It Work?</h4>
                <ul>
                    <li>Only the Marketing team or Centre Management can add an Event online</li>
                    <li>Your Event is automatically added to Mallguide on submission</li>
                    <li>You spread awareness of current Events, and drive more shoppers to your Mall</li>
                </ul>

                {{ Form::model($event, array('route' => array('mallevents.postEdit', $event->eventsMGID),'method'=>'post','files'=>true ,'name' => 'eventEdit', 'id' => 'eventEdit', 'class' => 'form-inline')) }}
                <input type="hidden" name="mall" value="{{ $event->mallID }}">
                {{ Form::hidden('referer',URL::previous()) }}
                <div class="span4">
                    <h4 class="dotted-border">Event Details</h4>

                    <div class="control-group {{ ($errors->has('name') ? 'error' : '') }}">
                        <label for="mall" class="control-label"><strong>Event Name</strong></label>
                        <div class="controls">
                            {{ Form::text('name',null,array('class' => 'txtbar required')) }}
                        </div>
                        <span class="help-block">{{ ($errors->has('name') ? $errors->first('name') : '') }}</span>
                    </div>
                    <span>To specify a category select an existing one from the list below</span>
                    <div class="control-group">
                        <label for="category"><strong>Category List</strong></label>
                        <div class="controls">
                            {{ Form::select('category',$categories,null,array('id'=>'category')) }}
                        </div>
                    </div>
                </div>
                <!--span4-->

                <div class="span4">

                    <h4 class="dotted-border">Location Details</h4>
                    <div class="control-group">
                        <label for="location"><strong>Location</strong></label>
                        <div class="controls">
                            {{ Form::text('location',null,array('class'=>'txtbar','id'=>'location')) }}
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="event"><strong>Description</strong></label>

                        <div class="controls">
                            {{ Form::textarea('event',null,array('class' => 'txtbox','rows'=>'3')) }}
                        </div>
                    </div>
                </div>
                <!--span4-->

                <div class="span3">
                    <h4 class="dotted-border">Dates &amp; Image</h4>
                    <div class="control-group">
                        <label for="startDate" class="control-label"><strong>Start</strong></label>
                        <div class="controls">
                            <div class="input-append datetimepicker4">
                                {{ Form::text('startDate',null,array('class' => 'span12','data-format'=>'yyyy-MM-dd','readonly')) }}
                                <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                    </span>
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}</span>
                        </div>
                    </div>


                    <div class="clear"></div>
                    <br/>

                    <div class="control-group">
                        <label for="endDate" class="control-label"><strong>End</strong></label>
                        <div class="controls">
                            <div class="input-append datetimepicker4">
                                {{ Form::text('endDate',null,array('class' => 'span12','data-format'=>'yyyy-MM-dd','readonly')) }}
                                <span class="add-on">
                    <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                    </i>
                    </span>
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}</span>
                        </div>
                    </div>

                    <div class="clear"></div>
                    <br/>

                    <div class="control-group {{ ($errors->has('image1') ? 'error' : '') }}">
                        @if(!empty($event->thumbnail1))
                            <img
                                src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $event->mallID }}/{{ $event->thumbnail1 }}"
                                alt="Image"/>
                        @else
                            <img src="" alt="Image" class="overlay-image"/>
                        @endif
                        <span class="btn btn-file">{{ Form::file('image1') }}</span>
                        {{ ($errors->has('image1') ? $errors->first('image1') : '') }}
                    </div>
                    <div class="control-group">
                        <label for="display"><strong>Must Event be displayed</strong></label>
                        <div class="controls">
                            Yes {{ Form::radio('display','Y') }} &nbsp;
                            No {{ Form::radio('display','N') }}
                        </div>
                    </div>
                    <div class="clear"></div>
                    <br/>
                    <button class="submit reg-btn" id="add_review">Submit Details</button>
                </div>
                <!--span4-->
                {{ Form::close() }}

            </div>
        </div>
    </div>
@stop

@section('exScript')
    <script type="text/javascript">
        $(function () {
            $("#category").change(function () {
                $("#category_text").val($(this).val());
            });

            $('.datetimepicker4').datetimepicker({
                pickTime: false
            });
        });
    </script>
@stop
