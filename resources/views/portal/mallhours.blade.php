@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Mall Hours</h1>

                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div>
            <!--/span6-->
			{{ generateBreadcrumbs() }}
            <!--/span6-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div>
    <!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>
    </div>
    <!-- span6 -->
    <div class="pull-left">
        <h4>Update mall Hours</h4>
    </div>
    <div class="clear"></div>
    <hr class="hborder"/>

    <div class="row-fluid">
        <div class="span12">
            @if(empty($mallHours))
            <form action="{{ route('portal.insertmallhours') }}" method="post" name="mallHoursInsert"
                  id="mallHoursInsert">
                @else
                {{ Form::model($mallHours, array('route' => array('portal.updatemallhours', $mallHours->hoursID),'method'=>'post','name' => 'mallHoursEdit', 'id' => 'mallHoursEdit', 'class' =>'form-inline')) }}
                @endif
                {{ Form::hidden('mallID',$mallID) }}
                <div class="accordion" id="accordion2">
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a data-toggle="collapse" class="accordion-toggle collapsed" data-parent="#accordion2"
                               href="#collapse1">
                                Normal Hours
                            </a>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse">
                            <div class="panel-body">
                                <table class="table" style="width: auto;">
                                    <thead></thead>
                                    <tbody>
                                    <tr>
                                        <td>Monday</td>
                                        <td>
                                            {{ Form::text('startHoursMon',null,array('class'=>'timepicker ')) }}
                                        </td>
                                        <td>
                                            {{ Form::text('endHoursMon',null,array('class'=>'timepicker ')) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tuesday</td>
                                        <td>
                                            {{ Form::text('startHoursTues',null,array('class'=>'timepicker ')) }}
                                        </td>
                                        <td>
                                            {{ Form::text('endHoursTues',null,array('class'=>'timepicker ')) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Wednesday</td>
                                        <td>
                                            {{ Form::text('startHoursWed',null,array('class'=>'timepicker ')) }}
                                        </td>
                                        <td>
                                            {{ Form::text('endHoursWed',null,array('class'=>'timepicker ')) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Thursday</td>
                                        <td>
                                            {{ Form::text('startHoursThurs',null,array('class'=>'timepicker ')) }}
                                        </td>
                                        <td>
                                            {{ Form::text('endHoursThurs',null,array('class'=>'timepicker ')) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Friday</td>
                                        <td>
                                            {{ Form::text('startHoursFri',null,array('class'=>'timepicker ')) }}
                                        </td>
                                        <td>
                                            {{ Form::text('endHoursFri',null,array('class'=>'timepicker ')) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Saturday</td>
                                        <td>
                                            {{ Form::text('startHoursSat',null,array('class'=>'timepicker ')) }}
                                        </td>
                                        <td>
                                            {{ Form::text('endHoursSat',null,array('class'=>'timepicker ')) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sunday</td>
                                        <td>
                                            {{ Form::text('startHoursSun',null,array('class'=>'timepicker ')) }}
                                        </td>
                                        <td>
                                            {{ Form::text('endHoursSun',null,array('class'=>'timepicker ')) }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a data-toggle="collapse" class="accordion-toggle" data-parent="#accordion2"
                               href="#collapse2">
                                Extended Hours
                            </a>
                        </div>
                        <div id="collapse2" class="accordion-collapse collapse">
                            <div class="accordion-body">
                                <table class="table" style="width: auto;">
                                    <thead></thead>
                                    <tbody>
                                    <tr>
                                        <td>Monday</td>
                                        <td>
                                            {{ Form::text('startHoursMon',null,array('class'=>'timepicker ')) }}
                                        </td>
                                        <td>
                                            {{ Form::text('endHoursMon',null,array('class'=>'timepicker ')) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Tuesday</td>
                                        <td>
                                            {{ Form::text('startHoursTues',null,array('class'=>'timepicker ')) }}
                                        </td>
                                        <td>
                                            {{ Form::text('endHoursTues',null,array('class'=>'timepicker ')) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Wednesday</td>
                                        <td>
                                            {{ Form::text('startHoursWed',null,array('class'=>'timepicker ')) }}
                                        </td>
                                        <td>
                                            {{ Form::text('endHoursWed',null,array('class'=>'timepicker ')) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Thursday</td>
                                        <td>
                                            {{ Form::text('startHoursThurs',null,array('class'=>'timepicker ')) }}
                                        </td>
                                        <td>
                                            {{ Form::text('endHoursThurs',null,array('class'=>'timepicker ')) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Friday</td>
                                        <td>
                                            {{ Form::text('startHoursFri',null,array('class'=>'timepicker ')) }}
                                        </td>
                                        <td>
                                            {{ Form::text('endHoursFri',null,array('class'=>'timepicker ')) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Saturday</td>
                                        <td>
                                            {{ Form::text('startHoursSat',null,array('class'=>'timepicker ')) }}
                                        </td>
                                        <td>
                                            {{ Form::text('endHoursSat',null,array('class'=>'timepicker ')) }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Sunday</td>
                                        <td>
                                            {{ Form::text('startHoursSun',null,array('class'=>'timepicker ')) }}
                                        </td>
                                        <td>
                                            {{ Form::text('endHoursSun',null,array('class'=>'timepicker ')) }}
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="submit reg-btn" id="add_review">Submit Details</button>
                <br/><br/>
                {{ Form::close() }}
        </div>
        <!--span12 -->
    </div>
</div>
@stop
@section('exScript')

<script type="text/javascript">
    $(document).ready(function () {
        $('.timepicker').timepicker();
    });
</script>
@stop

