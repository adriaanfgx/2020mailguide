@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('content')
<div class="row-fluid">
	<div class="blog-content">
    {{ Form::model($user, array('route' => array('user.postEdit', $user->id),'method' => 'post','name' => 'userEdit', 'id' => 'userEdit', 'class' => 'form-inline')) }}
   <div class="row-fluid">
	    <div class="span12">
	        <h4 class="dotted-border">Update User Info</h4>
	    </div>
    </div>
   <div class="row-fluid">
	    <div class="span12">
	        <h6 class="dotted-border">Login and admin status information</h6>
	    </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}">
                <label for="email"><strong>Email*</strong></label>
                <div class="controls">
                    {{ Form::text('email',null,array('class'=>'txtbar','readonly')) }}
                    {{ ($errors->has('email') ? $errors->first('email') : '') }}
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label class="control-label" for="admin_level"><strong>Admin Level</strong></label>
                <div class="controls">
                    {{ Form::text('admin_level',null,array('class'=>'txtbar','readonly')) }}
                </div>
            </div>
        </div>

            @if( $user->admin_level == 'Mall Manager' )
            <div class="control-group span4">
                <label class="control-label" for="user_malls"><strong>User's mall: </strong></label>
                <div class="controls">
                @if(sizeof($malls)>0)

                    {{ Form::select('mallID',$portalMalls,$selectMall,array('id'=>'mallID')) }}
                @else
                    {{ Form::select('mallID',array(""=>"No malls found"),$selectMall,array('id'=>'mallID')) }}
                @endif
                </div>
            </div>
            @elseif( $user->admin_level == 'Marketing Manager' && sizeof($portalMalls)>1)
	            <div class="control-group span4">
	                <label class="control-label" for="user_malls"><strong>Please select one or more malls from the list below: </strong></label>
	                <div class="controls">
	                    {{ Form::select('mallID[]',$portalMalls,isset($selectedMalls) ? $selectedMalls : array(),array('class'=>'multi-mall','id'=>'mallID','multiple')) }}
	                </div>
	            </div>
            @elseif( $user->admin_level == 'Shop Manager' )
            <div class="control-group span4">
                <label class="control-label" for="user_shops"><strong>Select a store from the list below to change the user's store: </strong></label>
                <div class="controls">
                    {{ Form::select('shopMGID[]',$shopList,$selectedShops,array('class'=>'multi-mall','id'=>'mallID','multiple')) }}
                </div>
            </div>
            @endif

            @if( $user->admin_level == 'Mall Manager' || $user->admin_level == 'Marketing Manager' )
            <div class="control-group span4">
                <label><strong>Malls Selected</strong></label>
                <div class="controls">
                    <ul>
                        @foreach( $selectedMalls as $selected )
                        <li>{{ $selected }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @elseif( $user->admin_level == 'Chain Manager' )
            <br />
            <div class="control-group">
                <label><strong>Chain Store</strong></label>
                <div class="controls">
                    {{ $userchain }}
                </div>
            </div>
            @elseif( $user->admin_level == 'Shop Manager' )
            <br />
            <div class="control-group">
                <label><strong>User Shop(s)</strong></label>
                <div class="controls">
                    <ul>
                        @foreach( $userShops as $shop )
                        <li>{{ $shop }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
    </div>
        <h6 class="dotted-border">
            Personal Details
        </h6>
    <div class="row-fluid">
        <div class="span4">
            <div class="control-group {{ $errors->first('first_name', ' error') }}">
                <label for="first_name"><strong>First Name</strong></label>
                <div class="controls">
                    {{ Form::text('first_name',null) }}
                </div>
				<span class="help-block">{{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}</span>
            </div>
        </div>
        <div class="span4">
            <div class="control-group {{ $errors->first('last_name', ' error') }}">
                <label for="last_name"><strong>Last Name</strong></label>
                <div class="controls">
                    {{ Form::text('last_name',null) }}
                </div>
				<span class="help-block">{{ ($errors->has('last_name') ? $errors->first('last_name') : '') }}</span>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="cell" class="control-label"><strong>Cell Phone</strong></label>
                <div class="controls">
                    {{ Form::text('cell',null,array('class'=>'numeric')) }}
                </div>
            </div>
        </div>
        </div>
        <div class="row-fluid">
            <div class="span4">
                <div class="control-group">
                    <label for="tel_home" class="control-label"><strong>Home phone</strong></label>
                    <div class="controls">
                        {{ Form::text('tel_home',null) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="tel_work" class="control-label"><strong>Work Phone</strong></label>
                    <div class="controls">
                        {{ Form::text('tel_work',null) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="fax" class="control-label"><strong>Fax</strong></label>
                    <div class="controls">
                        {{ Form::text('fax',null) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span4">
                <div class="control-group">
                    <label for="postal_address" class="control-label"><strong>Postal Address</strong></label>
                    <div class="controls">
                        {{ Form::textarea('postal_address',null,array('rows'=>3)) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="postal_code" class="control-label"><strong>Postal Code</strong></label>
                    <div class="controls">
                        {{ Form::text('postal_code',null,array('class'=>'span2')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="controls-group">
                    <label for="birth_date" class="control-label"><strong>Date of Birth</strong></label>
                    <div class="controls">
                        <div class="input-append datetimepicker4">
                            {{ Form::text('birth_date',null,array('data-format'=>'yyyy-MM-dd','readonly')) }}
                            <span class="add-on">
                              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                              </i>
                            </span>
                        </div>
                        <span class="help-block">{{ ($errors->has('birth_date') ? $errors->first('birth_date') : '') }}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span4">
                <div class="control-group">
                    <label for="province"><strong>Province </strong></label>
                    <div class="controls">
                        {{ Form::select('province_id',$provinces,null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="city"><strong>City</strong></label>
                    <div class="controls">
                        {{ Form::select('city_id',$cities,null,array('class'=>'txtbar','id'=>'city_text')) }}
                    </div>
                </div>
            </div>
            <div class="span4"></div>
        </div>

        <h6 class="dotted-border">
            Company Details
        </h6>
	    <div class="row-fluid">
	        <div class="span4">
	            <div class="control-group">
	                <label for="company"><strong>Company List : </strong></label>
	                <div class="controls">
	                    {{ Form::select('company',$companyList,null) }}
	                </div>
	            </div>
	        </div>
	        <div class="span4">
	            <div class="control-group">
	                <label for="company"><strong>Company</strong></label>
	                <div class="controls">
	                    {{ Form::text('company',null,array('class'=>'txtbar')) }}
	                </div>
	            </div>
	        </div>
	        <div class="span4">
	            <div class="control-group">
	                <label for="vat_number"><strong>Vat Number</strong></label>
	                <div class="controls">
	                    {{ Form::text('vat_number',null,array('class'=>'txtbar')) }}
	                </div>
	            </div>
	        </div>
	    </div>

	    <div class="row-fluid">
		    <div class="span5">
		        <div class="control-group">
		            <button class="submit btn btn-info" id="update_user">Submit Details</button>
		        </div>
		    </div>
		</div>
    {{ Form::close() }}
	</div>
</div>
@stop
@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });
        $(".multi-mall").select2({});
        $(".numeric").mask('0b99999999')
    });
</script>
@stop

