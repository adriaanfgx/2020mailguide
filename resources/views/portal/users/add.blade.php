@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Add User </h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                {{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
	@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid">
	<div class="blog-content">
	    <h4 class="dotted-border">Add Mall Or Shop User</h4>
		<!--    <div class="span4">-->
		<span>Users added here will be automatically approved, login details will be sent through to them via email.</span><br /><br />
		<ul>
		    <li><a href="/portal/addmalluser/{{ $mallID }}">Add a Mall User</a> -  These users can help manage mall information</li>
		    <li><a href="/portal/addstoreuser/{{ $mallID }}">Add a Shop User</a> -  These users can manage their shop information</li>
		</ul>
		<!--    </div>-->
	</div>
</div>
@stop

@section('exScript')
<script>

    $(document).ready(function(){

    });

</script>
@stop
