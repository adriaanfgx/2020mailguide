@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Mall Member</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
				{{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid  blog-content">

	<div class="span12">
	    <div class="row-fluid">
	        <h4 class="dotted-border">Registration Form</h4>
	    </div>
        <div class="leftMargin">
                {{ Form::open(array('route' => 'portal.postAddmalluser', 'method' => 'post','name' => 'addMallUser','id' => 'addMallUser', 'class' => 'form-inline shopCreate')) }}
                {{ Form::hidden('mallID',$mall->mallID) }}
                {{ Form::hidden('admin_level','Mall Manager') }}

                @include('partials/memberzone/adduserfields')

                <div class="row-fluid">
                    <div class="span4 offset2">
                        <div class="control-group">
                            <div class="controls">
                                <button class="submit reg-btn">Submit Details</button>
                            </div>
                        </div>
                    </div>
                </div>
                {{ Form::close() }}
        </div>
    </div>
</div>
@stop
@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

        $(".numeric").mask('27b99999999');

        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });
        $('#province').change(function(){

        	$('#city').empty();
            $.ajax({
                type: "GET",
                url: '/admin/provinces/cities/'+$(this).val(),
                data: '',
                success: function (data) {
                    //console.log(data);
                    $('#city').html('<option value="">Please Select a City..</option>');
                    $.each(data,function(key,val){

                        $('#city').append('<option value="'+key+'">'+val+'</option>');
                    });

                }
            });
        });
        $('#password').keyup(function() {
	        var _parent = $(this).parent().parent();
			var level = 0;
			var mesg = '';
			var input = $(this).val();

			if(/(.*[A-Z])/.test(input) == false)
			{
				level = 1;
				mesg += ' No uppercase letters.<br/>';

			}

			if(/(.*[\!|\@|\#|\$|\_])/.test(input) == false) {
				level = 2;
				mesg += ' No special character (!@#$_).<br/>';

			}
			if(/(.*[\d])/.test(input) == false)
			{
				level = 3;
				mesg += ' No numbers.<br/>';

			}
			if(/(.*[a-z])/.test(input) == false)
			{
				level = 4;
				mesg += ' No lowercase letters.<br/>';

			}

			if(input.length < 6)
			{
				level = 5;
				mesg += ' Not long enough.<br/>';

			}



			$('#password_help').html(mesg);

	        if(level != 0){
	            _parent.addClass('error');

				$('#sub').attr('disabled','disabled');
	        } else {
				$('#sub').removeAttr('disabled');
			}
	    });
    });

</script>
@stop

