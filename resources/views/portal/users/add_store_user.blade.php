@extends('layouts.backpages')
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Mall Member</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Add Mall User</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid  blog-content">

	<div class="span12">
	    <div class="row-fluid">
	        <h4 class="dotted-border">Registration Form</h4>
	    </div>
	    <div class="leftMargin">

		    <div class="row-fluid">
		        <div class="span12">
		            {{ Form::open(array('route' => 'portal.postAddmalluser', 'method' => 'post','name' => 'addMallUser','id' => 'addMallUser', 'class' => 'form-inline shopCreate')) }}
		            <div>
		                <div id="user_info">
		                    <div class="row" style="padding-left: 30px;">
		                        <div class="span4">
		                            <div class="control-group">
		                                <label for="mall" class="control-label"><strong>Mall</strong></label>
		                                <div class="controls">
		                                    {{ Form::text('mall',$mall->name,array('class'=>'txtbar','readonly')) }}
		                                    {{ Form::hidden('mallID',$mall->mallID) }}
		                                    {{ Form::hidden('admin_level','Shop Manager') }}
		                                </div>
		                            </div>
	                            </div>
		                        <div class="span4">
		                            <div class="control-group">
		                                <label for="shopID"><strong>Shop</strong></label>
		                                <div class="controls">
		                                    {{ Form::select('shopMGID',$shops) }}
		                                </div>
		                            </div>
		                        </div>
		                    </div>
		                    @include('partials/memberzone/adduserfields')
		                </div>
		            </div>
		            <button class="submit reg-btn" id="update_user">Submit Details</button>
		            {{ Form::close() }}
		        </div><!--span12 -->
		    </div>
	    </div>
    </div>
</div>
@stop
@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });
        $('#province').change(function(){

        	$('#city').empty();
            $.ajax({
                type: "GET",
                url: '/admin/provinces/cities/'+$(this).val(),
                data: '',
                success: function (data) {
                    //console.log(data);
                    $('#city').html('<option value="">Please Select a City..</option>');
                    $.each(data,function(key,val){

                        $('#city').append('<option value="'+key+'">'+val+'</option>');
                    });

                }
            });
        });
        $('#password').keyup(function() {
	        var _parent = $(this).parent().parent();
			var level = 0;
			var mesg = '';
			var input = $(this).val();

			if(/(.*[A-Z])/.test(input) == false)
			{
				level = 1;
				mesg += ' No uppercase letters.<br/>';

			}

			if(/(.*[\!|\@|\#|\$|\_])/.test(input) == false) {
				level = 2;
				mesg += ' No special character (!@#$_).<br/>';

			}
			if(/(.*[\d])/.test(input) == false)
			{
				level = 3;
				mesg += ' No numbers.<br/>';

			}
			if(/(.*[a-z])/.test(input) == false)
			{
				level = 4;
				mesg += ' No lowercase letters.<br/>';

			}

			if(input.length < 6)
			{
				level = 5;
				mesg += ' Not long enough.<br/>';

			}



			$('#password_help').html(mesg);

	        if(level != 0){
	            _parent.addClass('error');

				$('#sub').attr('disabled','disabled');
	        } else {
				$('#sub').removeAttr('disabled');
			}
	    });
    });
</script>
@stop

