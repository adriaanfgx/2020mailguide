@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>List users </h1>

                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div>
            <!--/span6-->
            <?php
            $activeUser = Sentry::getUser();
            $user = User::find($activeUser->id);
            $mallID = null;
            $userMalls = $user->malls;
            foreach ($userMalls as $malls) {
                $mallID = $malls->mallID;
            }

            ?>
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">List Users</li>
                </ul>
            </div>
            <!--/span6-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid">
    <!--    <div class="span4">-->
    <h4 class="dotted-border">Site Users</h4>
    <table class="table table-bordered">
        <thead>
        <th>Username / Email</th>
        <th>Name</th>
        <th>User Admin Level</th>
        <th>Date Added</th>
        <th>Active</th>
        <th>Actions</th>
        </thead>
        <tbody>
        @foreach( $users as $user )
        <tr>
            <td>{{ $user->email }}</td>
            <td>{{ $user->first_name }} &nbsp; {{ $user->last_name }}</td>
            <td>{{ $user->admin_level }}</td>
            <td>{{ $user->created_at }}</td>
            <td>{{ displayAsText($user->activated) }}</td>
            <td>
                <a href="{{ route('portal.edituser',$user->id) }}" class="btn btn-info btn-mini"><i
                        class="fa fa-pencil"></i> Edit</a>&nbsp;|&nbsp;<a class="btn btn-danger btn-mini action_delete"
                                                                          href="#" id="{{ $user->id }}"><i
                        class="fa fa-trash-o"></i> Delete</a>
                @if( $user->activated == 0 )
                &nbsp;|&nbsp;<a class="send_activation btn btn-success btn-mini" href="#" id="{{ $user->id }}"><i
                        class="fa fa-envelope"></i> Resend Activation</a>
                @endif
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
    @foreach( $users as $user )

    @endforeach
</div>
@stop

@section('exScript')
<script>

    $(document).ready(function () {

        $(".action_delete").click(function () {

            var $user = $(this).attr('id');

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $.ajax({
                type: "POST",
                url: '/managerdeleteuser/' + $user,
                data: {
                    '_token': "{{ csrf_token() }}"
                },
                success: function () {
                    location.reload();
                }
            });

        });

        $(".send_activation").click(function () {

            var $user = $(this).attr('id');

            $.ajax({
                type: "POST",
                url: '/resendactivation/' + $user,
                data: {
                    '_token': "{{ csrf_token() }}"
                },
                success: function () {
                    location.reload();
                }
            });

        });
    });

</script>
@stop
