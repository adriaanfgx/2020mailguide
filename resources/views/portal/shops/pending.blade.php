@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Shops Listing</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Shops</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div><!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">

    <div class="span9 pull-left">
        <h4 class="hborder">Shops to be approved</h4>

    </div>
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>

    </div>  <!-- span6 -->

    <div class="clear"></div>
<!--    <hr class="hborder" />-->

    <div class="row-fluid">
        <div class="span12">
            @if( sizeof($pendingShops) > 0 )
            <table class="table table-border">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Mall</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody id="shop_table">
                @foreach($pendingShops as $shop)
                <tr>
                    <td>{{ $shop->name }}</td>
                    <td>{{ $shop->mall }}</td>
                    <td>
                        <a href="{{ route('portal.unapprovedshop',$shop->shopMGID) }}" class="btn btn-success"><i class="fa fa-eye"></i>View Details</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            @else
            <span>There are no listed shops that need approval ...</span>
            @endif
        </div><!--span12 -->
    </div>
</div>
@stop
@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

    });
</script>
@stop

