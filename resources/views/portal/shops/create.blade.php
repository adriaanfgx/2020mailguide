@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Shops Registration </h1>
                    <p class="animated fadeInDown delay2">Manage your mall, Tenant Information &amp; Communication</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')

@stop

@section('content')
    <div class="blog-content container-fluid">
        <div class="row-fluid">
            <div class="biggerLeftMargin">
                <h3>Add shop</h3>

                {{ Form::open(array('route' => 'shops.postCreateShop', 'method' => 'post','files'=> true,'name' => 'shops.postRegister','id' => 'shopCreate', 'class' => 'form-inline shopCreate')) }}
                {{ Form::hidden('referer',URL::previous()) }}
                {{ Form::hidden('display','N') }}
                <div class="row-fluid">
                    <div class="control-group">
                        <label for="mall">Mall</label>
                        <div class="controls">
                            @if(isset($mall))
                                <input class="txtbar span3" type="text" name="mall" id="mall" value="{{ $mall->name }}"
                                       readonly/>
                                <input class="txtbar span3" type="hidden" name="mallID" id="mall"
                                       value="{{ $mall->mallID }}" readonly/>
                            @elseif(Sentinel::hasAccess('user'))
                                {{ Form::select('mallID', $malls, array('id'=>'mallID', 'class'=>'span6')) }}
                                {{ ($errors->has('mallID') ? $errors->first('mallID') : '') }}
                            @endif
                            <span class="help-inline"></span>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <h4 class="dotted-border">Shop Information</h4>
                    <div class="span6">
                        <div class="control-group {{ ($errors->has('name') ? 'error' : '') }}">
                            <label for="name" class="control-label"><strong>Shop Name</strong></label>
                            <div>
                                @if(Sentinel::hasAccess('user') && isset($activeChain))
                                    {{ Form::text('name',$activeChain,array('class'=>'txtbar span6','readonly')) }}
                                @else
                                    {{ Form::text('name',null,array('class'=>'txtbar span6')) }}
                                @endif
                            </div>
                            <span class="help-block">{{ ($errors->has('name') ? $errors->first('name') : '') }}</span>
                        </div>
                        <div class="control-group {{ ($errors->has('tradingHours') ? 'error' : '') }}">
                            <label for="tradingHours"><strong>Trading Hours</strong></label>
                            <div class="controls">
                                {{Form::textarea('tradingHours', null,array('class'=>'txtbox span6','rows'=>'3')) }}
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('tradingHours') ? $errors->first('tradingHours') : '') }}</span>
                        </div>
                    </div>
                    <div class="span6"></div>
                </div>
                <br/>
                <div class="row-fluid">
                    <h4 class="dotted-border ">Shop Category</h4>
                    <div class="span4">
                        <div class="control-group">
                            <label for="category">Category</label>
                            <div class="controls">
                                <select id="category" name="category_id" onchange="refreshSubcategoryList()">
                                    <option value="">Select category ...</option>
                                    @foreach( $categories as $id=>$category)
                                        <option value="{{ $id }}">{{ $category }}</option>
                                    @endforeach
                                </select>
                                {{ ($errors->has('category_id') ? $errors->first('category_id') : '') }}
                            </div>
                        </div>
                    </div>

                    <div class="span4">
                        <div class="control-group">
                            <label for="subcategory">Sub Category</label>
                            <div class="controls">
                                <select id="subcategory" name="subcategory_id">
                                    <option value="">Select category ...</option>
                                    @foreach( $subcategories as $id=>$subcategory)
                                        <option value="{{ $id }}">{{ $subcategory }}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <h4 class="dotted-border">Shop Description</h4>
                    <div class="span4">
                        <div class="control-group">
                            <label for="description"><strong>Description</strong></label>
                            <div class="controls">
                                {{Form::textarea('description', null,array('class'=>'txtbox ','rows'=>'3')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="keywords"><strong>Keywords</strong></label>
                            <div class="controls">
                                {{Form::textarea('keywords', null,array('class'=>'txtbox ','rows'=>'3')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <h4 class="dotted-border">Products</h4>
                    <div class="span4">
                        <div class="control-group">
                            <label for="product1"><strong>Product 1</strong></label>
                            <div class="controls">
                                {{Form::text('product1',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="product2"><strong>Product 2</strong></label>
                            <div class="controls">
                                {{Form::text('product2', null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="product3"><strong>Product 3</strong></label>
                            <div class="controls">
                                {{Form::text('product3', null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="product4"><strong>Product 4</strong></label>
                            <div class="controls">
                                {{Form::text('product4', null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <div class="controls">
                                <label for="product5"><strong>Product 5</strong></label>
                                {{Form::text('product5', null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <div class="controls">
                                <label for="product6"><strong>Product 6</strong></label>
                                {{Form::text('product6', null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <div class="controls">
                                <label for="product7"><strong>Product 7</strong></label>
                                {{Form::text('product7', null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <div class="controls">
                                <label for="product8"><strong>Product 8</strong></label>
                                {{Form::text('product8', null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <div class="controls">
                                <label for="product9"><strong>Product 9</strong></label>
                                {{Form::text('product9', null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <div class="controls">
                                <label for="product10"><strong>Product 10</strong></label>
                                {{Form::text('product10', null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row-fluid">
                    <h4 class="dotted-border">Contact Details</h4>
                    <div class="span4">
                        <div class="control-group">
                            <div class="controls">
                                <label for="shopNumber"><strong>Shop Number</strong></label>
                                {{Form::text('shopNumber', null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <div class="controls">
                                <label for="landmark"><strong>Landmark</strong></label>
                                {{Form::text('landmark', null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="telephone"><strong>Telephone</strong></label>
                            <div class="controls">
                                {{Form::text('telephone', null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <div class="controls">
                                <label for="fax"><strong>Fax</strong></label>
                                {{Form::text('fax', null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="cell"><strong>Cell</strong></label>
                            <div class="controls">
                                {{Form::text('cell',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="contactPerson"><strong>Contact Person</strong></label>
                            <div class="controls">
                                {{Form::text('contactPerson', null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}">
                            <label for="email"><strong>Email</strong></label>
                            <div class="controls">
                                {{Form::text('email', null,array('class'=>'txtbar')) }}
                            </div>
                            <span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('url') ? 'error' : '') }}">
                            <div class="controls">
                                <label for="url" class="control-label"><strong>Web</strong> &nbsp; <small>( Must start
                                        with http:// or https:// )</small></label>
                                {{Form::text('url',null,array('class'=>'txtbar')) }}
                            </div>
                            <span class="help-block">{{ ($errors->has('url') ? $errors->first('url') : '') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <h4 class="dotted-border">Store Images</h4>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('image1') ? 'error' : '') }}">
                            <label for="image1"><strong>Store front Image (Image 1)</strong>(Only gif, jpg or png files)</label>
                            <div class="controls">
                                <span class="btn btn-file">{{ Form::file('image1') }}</span><br/>
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('image1') ? $errors->first('image1') : '') }}</span>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('image2') ? 'error' : '') }}">
                            <label for="image2"><strong>Image 2</strong>(Only gif, jpg or png files)</label>
                            <div class="controls">
                                <span class="btn btn-file">{{ Form::file('image2') }}</span><br/>
                            </div>
                            <div class="help-block">{{ ($errors->has('image2') ? $errors->first('image2') : '') }}</div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('image3') ? 'error' : '') }}">
                            <label for="image3"><strong>Image 3 </strong>(Only gif, jpg or png files)</label>
                            <div class="controls">
                                <span class="btn btn-file">{{ Form::file('image3') }}</span><br/>
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('image3') ? $errors->first('image3') : '') }}</span>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('image4') ? 'error' : '') }}">
                            <label for="image4"><strong>Image 4</strong>(Only gif, jpg or png files)</label>
                            <div class="controls">
                                <span class="btn btn-file">{{ Form::file('image4') }}</span><br/>
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('image4') ? $errors->first('image4') : '') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('image5') ? 'error' : '') }}">
                            <label for="image5"><strong>Image 5</strong>(Only gif, jpg or png files)</label>
                            <div class="controls">
                                <span class="btn btn-file">{{ Form::file('image5') }}</span><br/>
                                {{ ($errors->has('image5') ? $errors->first('image5') : '') }}
                            </div>
                        </div>
                    </div>
                    <div class="span4"></div>
                </div>

                <div class="row-fluid">
                    <h4 class="dotted-border">Credit Cards</h4>
                    Mastercard&nbsp;{{Form::checkbox('mastercard','Y',array('class'=>'pull-left')) }}&nbsp;&nbsp;
                    Visa&nbsp;{{Form::checkbox('visa', 'Y',array('class'=>'pull-left')) }}&nbsp;&nbsp;
                    AMEX&nbsp;{{Form::checkbox('amex','Y',array('class'=>'pull-left')) }}&nbsp;&nbsp;
                    Diners club&nbsp{{Form::checkbox('diners', 'Y',array('class'=>'pull-left')) }}
                </div>
                <br/>
                <div class="clearfix"></div>
                <div class="control-group">
                    <label for="newShop"><strong>Is the shop new ?</strong></label>
                    <div class="controls">
                        {{Form::radio('newShop','Y', 'checked') }}&nbsp;Yes
                        &nbsp;&nbsp;{{Form::radio('newShop','N', 'checked') }}No
                    </div>
                </div>
                <br/>
                <div class="control-group">
                    <label for="display"><strong>Must your store be displayed?</strong></label>
                    <div class="controls">
                        {{Form::radio('display','Y', 'checked') }}&nbsp;Yes
                        &nbsp;&nbsp;{{Form::radio('display','N', 'checked') }}No
                    </div>
                </div>

                <div class="row-fluid">
                    <h4 class="dotted-border">Owner and Manager Contact Details</h4>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="ownerName"><strong>Owner's first name</strong></label>
                            <div class="controls">
                                {{ Form::text('ownerName',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="ownerSurname"><strong>Owner's surname</strong></label>
                            <div class="controls">
                                {{ Form::text('ownerSurname',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="ownerCell"><strong>Owner's Cell</strong></label>
                            <div class="controls">
                                {{ Form::text('ownerCell',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('ownerEmail') ? 'error' : '') }}">
                            <label class="control-label" for="ownerEmail"><strong>Owner's Email</strong></label>
                            <div class="controls">
                                {{ Form::text('ownerEmail',null,array('class'=>'txtbar')) }}
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('ownerEmail') ? $errors->first('ownerEmail') : '') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="managerName"><strong>Marketing Manager: first
                                    name</strong></label>
                            <div class="controls">
                                {{ Form::text('managerName',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="managerSurname"><strong>Marketing Manager
                                    :surname</strong></label>
                            <div class="controls">
                                {{ Form::text('managerSurname',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="managerCell"><strong>Marketing Manager:
                                    Cell</strong></label>
                            <div class="controls">
                                {{ Form::text('managerCell',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('managerEmail') ? 'error' : '') }}">
                            <label class="control-label" for="managerEmail"><strong>Marketing Manager:
                                    Email</strong></label>
                            <div class="controls">
                                {{ Form::text('managerEmail',null,array('class'=>'txtbar')) }}
                            </div>
                            <div
                                class="help-block">{{ ($errors->has('managerEmail') ? $errors->first('managerEmail') : '') }}</div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="managerName2"><strong>Manager 2: first
                                    name</strong></label>
                            <div class="controls">
                                {{ Form::text('managerName2',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="managerSurname2"><strong>Manager
                                    2:surname</strong></label>
                            <div class="controls">
                                {{ Form::text('managerSurname2',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="managerCell2"><strong>Manager 2: Cell</strong></label>
                            <div class="controls">
                                {{ Form::text('managerCell2',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('managerEmail2') ? 'error' : '') }}">
                            <label class="control-label" for="managerEmail2"><strong>Manager 2: Email</strong></label>
                            <div class="controls">
                                {{ Form::text('managerEmail2',null,array('class'=>'txtbar')) }}
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('managerEmail2') ? $errors->first('managerEmail2') : '') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="managerName3"><strong>Manager 3:first
                                    name</strong></label>
                            <div class="controls">
                                {{ Form::text('managerName3',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="managerSurname3"><strong>Manager
                                    3:surname</strong></label>
                            <div class="controls">
                                {{ Form::text('managerSurname3',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="managerCell3"><strong>Manager 3: Cell</strong></label>
                            <div class="controls">
                                {{ Form::text('managerCell3',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('managerEmail3') ? 'error' : '') }}">
                            <label class="control-label" for="managerEmail3"><strong>Manager 3: Email</strong></label>
                            <div class="controls">
                                {{ Form::text('managerEmail3',null,array('class'=>'txtbar')) }}
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('managerEmail3') ? $errors->first('managerEmail3') : '') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="headOfficeName"><strong>Head Office: first
                                    name</strong></label>
                            <div class="controls">
                                {{ Form::text('headOfficeName',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="headOfficeSurname"><strong>Head
                                    Office:surname</strong></label>
                            <div class="controls">
                                {{ Form::text('headOfficeSurname',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="headOfficeCell"><strong>Head Office: Cell</strong></label>
                            <div class="controls">
                                {{ Form::text('headOfficeCell',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('headOfficeEmail') ? 'error' : '') }}">
                            <label class="control-label" for="headOfficeEmail"><strong>Head Office:
                                    Email</strong></label>
                            <div class="controls">
                                {{ Form::text('headOfficeEmail',null,array('class'=>'txtbar')) }}
                            </div>
                            <div
                                class="help-block">{{ ($errors->has('headOfficeEmail') ? $errors->first('headOfficeEmail') : '') }}</div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="areaManagerName"><strong>Area Manager: first name</strong></label>
                            <div class="controls">
                                {{ Form::text('areaManagerName',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="areaManagerSurname"><strong>Area Manager:surname</strong></label>
                            <div class="controls">
                                {{ Form::text('areaManagerSurname',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="areaManagerCell"><strong>Area Manager:
                                    Cell</strong></label>
                            <div class="controls">
                                {{ Form::text('areaManagerCell',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('areaManagerEmail') ? 'error' : '') }}">
                            <label class="control-label" for="areaManagerEmail"><strong>Area Manager:
                                    Email</strong></label>
                            <div class="controls">
                                {{ Form::text('areaManagerEmail',null,array('class'=>'txtbar')) }}
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('areaManagerEmail') ? $errors->first('areaManagerEmail') : '') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="opsManagerName"><strong>Ops Manager: first
                                    name</strong></label>
                            <div class="controls">
                                {{ Form::text('opsManagerName',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="opsManagerSurname"><strong>Ops
                                    Manager:surname</strong></label>
                            <div class="controls">
                                {{ Form::text('opsManagerSurname',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="opsManagerCell"><strong>Ops Manager: Cell</strong></label>
                            <div class="controls">
                                {{ Form::text('opsManagerCell',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('opsManagerEmail') ? 'error' : '') }}">
                            <label class="control-label" for="opsManagerEmail"><strong>Ops Manager:
                                    Email</strong></label>
                            <div class="controls">
                                {{ Form::text('opsManagerEmail',null,array('class'=>'txtbar')) }}
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('opsManagerEmail') ? $errors->first('opsManagerEmail') : '') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="emergencyName"><strong>Emergency Contact: first
                                    name</strong></label>
                            <div class="controls">
                                {{ Form::text('emergencyName',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="emergencySurname"><strong>Emergency
                                    contact:surname</strong></label>
                            <div class="controls">
                                {{ Form::text('emergencySurname',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="emergencyCell"><strong>Emergency Contact:
                                    Cell</strong></label>
                            <div class="controls">
                                {{ Form::text('emergencyCell',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('emergencyEmail') ? 'error' : '') }}">
                            <label class="control-label" for="emergencyEmail"><strong>Emergency Contact: Email</strong></label>
                            <div class="controls">
                                {{ Form::text('emergencyEmail',null,array('class'=>'txtbar')) }}
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('emergencyEmail') ? $errors->first('emergencyEmail') : '') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="emergencyName2"><strong>Emergency Contact(2): first
                                    name</strong></label>
                            <div class="controls">
                                {{ Form::text('emergencyName2',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="emergencySurname2"><strong>Emergency contact
                                    (2):surname</strong></label>
                            <div class="controls">
                                {{ Form::text('emergencySurname2',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="clearfix"></div>
                    <div class="span4">
                        <div class="control-group">
                            <label class="control-label" for="emergencyCell2"><strong>Emergency Contact(2):
                                    Cell</strong></label>
                            <div class="controls">
                                {{ Form::text('emergencyCell2',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('emergencyEmail2') ? 'error' : '') }}">
                            <label class="control-label" for="emergencyEmail2"><strong>Emergency Contact (2):
                                    Email</strong></label>
                            <div class="controls">
                                {{ Form::text('emergencyEmail2',null,array('class'=>'txtbar')) }}
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('emergencyEmail2') ? $errors->first('emergencyEmail2') : '') }}</span>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <button class="submit reg-btn" id="add_review">Submit Details</button>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>

@stop

@section('exScript')
    <script>

        $(document).ready(function () {

            if ($("#category").val() == '') {
                $('#subcategory').empty();
            }
        });

        function refreshSubcategoryList() {
            var category = $('#category').val();

            $.ajax({
                type: 'GET',
                url: '/shops/subcategory/' + category,
                data: '',
                success: function (data) {
                    $('#subcategory').empty().append('<option value="">Please Select...</option>');

                    $.each(data, function (key, value) {
                        $('#subcategory').append('<option value="' + key + '">' + value + '</option>');
                    })
                }
            });
        }


    </script>
@stop
