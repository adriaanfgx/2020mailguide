@extends('layouts.backpages')
@section('heading')
    @include('portal.partials.submenu')
@stop

@section('content')
<?php //print_r(Session::all());exit(); ?>
<div class="container-fluid">
    <div class="container-fluid biggerPadding blog-content">
        <div class="clearfix"></div>
        <div class="row-fluid">
            <div class="hborder">
                <h4 class="paddingBottom">{{$heading}}</h4>
                <div class="span3 pull-right pull-top">
                    <div class="overlay-wrapper"></div>
                    @if($display!="N")
                    <select id="category" name="category" class="span10">
                        <option value="">Select existing category..</option>
                        <option value="all">All Categories</option>
                        @foreach( $categories as $category)
                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                    @endif
                </div>  <!-- span3 -->
            </div>
            <div class="row-fluid">
                <div class="span12">
                    <table class="table table-border">
                        <?php $activeMall = Session::get('activeMallID'); ?>
                        {{ Form::hidden('activeMall',$activeMall,array('id'=>'activeMall')) }}
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                            <th>Shop Number</th>
                            <th>Telephone</th>
                            <th>Display</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody id="shop_table">
                        @foreach($shops as $shop)
                        <tr>
                            <td>{{ $shop->name }}</td>
                            <td>{{ $shop->shopCategory? $shop->shopCategory->name:"" }}</td>
                            <td>{{ $shop->shopSubCategory? $shop->shopSubCategory->name:"" }}</td>
                            <td>{{ $shop->shopNumber }}</td>
                            <td>{{ $shop->telephone }}</td>
                            <td>{{ displayText($shop->display) }}</td>
                            <td>
                                <a href="{{ route('shops.update',$shop->shopMGID) }}" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp;&nbsp;|
                                &nbsp;&nbsp;<a class="btn btn-danger btn-mini action_delete" href="{{ route('shops.delete',$shop->shopMGID) }}" id="{{ $shop->shopMGID }}"><i class="fa fa-trash-o"></i> Delete</a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div><!--span12 -->
            </div>
        </div>
    </div>
</div>

@stop
@section('exScript')
<script type="text/javascript">
    $(document).ready(function(){

        $("#category").change(function(){

            $("#shop_table").empty();

            var route = window.location.href;
            var resultArr = route.split('/');
            var mallID = $("#activeMall").val();
            var category = $(this).val();

            $.ajax({
                url: '/shops/category/'+mallID+'/'+category,
                data:{},
                dataType: 'json',
                success: function(data){



                    $.each(data, function(j,val){
                        if(val.category==null)
						{
							val.category="";
						}
						if(val.subcategory==null)
						{
							val.subcategory="";
						}
                        $("#shop_table").append('<tr><td>'+val.name+'</td><td>'+val.category+'</td><td>'+val.subcategory+'</td><td>'+val.shopNumber+'</td><td>'+val.telephone+'</td><td>'+val.display+'</td><td><a href="/shops/update/'+val.shopMGID+'" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp;|&nbsp;<a class="btn btn-danger btn-mini action_delete" href="#" id="'+val.shopMGID+'"><i class="fa fa-trash-o"></i> Delete</a></td></tr>');
                    });

                    if( data.length === 0 ){
                        $('#shop_table').append('<tr><td">There is no data for selected categoty...</td></tr>');
                    } else {

                    }

                    $(".action_delete").click(function(e){
                        e.preventDefault();
                        var shop = $(this).attr("id");

                        bootbox.confirm("Are you sure you want to delete this entry ? ");

                        $(".modal a.btn-primary").click(function(){
                            $.ajax({
                                type: "POST",
                                url: '/shops/delete/'+shop,
                                data: {
                                    '_token': "{{ csrf_token() }}"
                                },
                                success: function(){
                                    location.reload();
                                }
                            });
                        });
                    });

                }

            });

        });

        $(".action_delete").click(function(e){
            e.preventDefault();
            var shop = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: '/shops/delete/'+shop,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });
        });
    });
</script>
@stop

