@foreach($shop->downloads as $download)
    <tr>
        <td width="50" class="text-center"><img src="{{ asset('img/docs/'.$download->filetype.'.png') }}"/></td>
        <td><a href="/download/mall_{{ $shop->shopMGID }}/{{ $download->filename }}"> {{ $download->name }}</a></td>
        <td width="100">{{ $download->filesize }}kb</td>
        <td width="100">
            <a href="/shops/{{ $shop->shopMGID }}/delete/{{ $download->id }}" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></a>
        </td>
    </tr>
@endforeach
 