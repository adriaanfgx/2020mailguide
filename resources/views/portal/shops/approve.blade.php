@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Shops Registration </h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Edit Shop</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')

<div class="span12 middle-headings dotted-border white-bg">
    <h3>Shops - Edit</h3>
</div><!--/span12-->

<div class="row-fluid">

<div>
{{ Form::model($shop) }}
<div id="shop_info">
    <h4 class="dotted-border">Shop Information</h4>
    <div class="control-group">
        <label for="name"><strong>Shop Name</strong></label>
        <div>
            {{ Form::text('name',null,array('class'=>'txtbar span4','readonly')) }}
        </div>
    </div>
    <div class="control-group">
        <label for="tradingHours"><strong>Trading Hours</strong></label>
        <div class="controls">
            {{Form::textarea('tradingHours', Input::old('tradingHours'),array('class'=>'txtbox span4','rows'=>'3','readonly')) }}
        </div>
    </div>
</div>
<br />
<h4 class="dotted-border">Shop Category</h4>
<div class="control-group">
    <label for="category"><strong>Category</strong></label>
    <div class="controls">
        {{ Form::text('category',null,array('class'=>'txtbar span4','readonly')) }}
    </div>
</div>

<div class="control-group">
    <label for="subcategory"><strong>Sub Category</strong></label>
    <div class="controls">
        {{ Form::text('subcategory',null,array('class'=>'txtbar span4','readonly')) }}
    </div>
</div>
<div id="shop_description">
    <h4 class="dotted-border">Shop Description</h4>
    <div class="control-group">
        <label for="description"><strong>Description</strong></label>
        <div class="controls">
            {{Form::textarea('description', Input::old('description'),array('class'=>'txtbox span4','rows'=>'3','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label for="keywords"><strong>Keywords</strong></label>
        <div class="controls">
            {{Form::textarea('keywords', Input::old('keywords'),array('class'=>'txtbox span4','rows'=>'3','readonly')) }}
        </div>
    </div>
</div>
<h4 class="dotted-border">Products</h4>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product1"><strong>Product 1</strong></label>
                {{Form::text('product1', Input::old('product1'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product2"><strong>Product 2</strong></label>
                {{Form::text('product2', Input::old('product2'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
    </div>
</div>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product3"><strong>Product 3</strong></label>
                {{Form::text('product3', Input::old('product3'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product4"><strong>Product 4</strong></label>
                {{Form::text('product4', Input::old('product4'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
    </div>
</div>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product5"><strong>Product 5</strong></label>
                {{Form::text('product5', Input::old('product5'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product6"><strong>Product 6</strong></label>
                {{Form::text('product6', Input::old('product6'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
    </div>
</div>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product7"><strong>Product 7</strong></label>
                {{Form::text('product7', Input::old('product7'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product8"><strong>Product 8</strong></label>
                {{Form::text('product8', Input::old('product8'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
    </div>
</div>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product9"><strong>Product 9</strong></label>
                {{Form::text('product9', Input::old('product9'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product10"><strong>Product 10</strong></label>
                {{Form::text('product10', Input::old('product10'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
    </div>
</div>
<h4 class="dotted-border">Contact Details</h4>
<div id="contact-details" class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="shopNumber"><strong>Shop Number</strong></label>
                {{Form::text('shopNumber', Input::old('shopNumber'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
        <div class="control-group">
            <label for="telephone"><strong>Tel</strong></label>
            <div class="controls">
                {{Form::text('telephone', Input::old('telephone'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
        <div class="control-group">
            <label for="cell"><strong>Cell</strong></label>
            <div class="controls">
                {{Form::text('cell', Input::old('cell'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
        <div class="control-group">
            <label for="email"><strong>Email</strong></label>
            <div class="controls">
                {{Form::text('email', Input::old('email'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="landmark"><strong>Landmark</strong></label>
                {{Form::text('landmark', Input::old('landmark'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <label for="fax"><strong>Fax</strong></label>
                {{Form::text('fax', Input::old('fax'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <label for="contactPerson"><strong>Contact Person</strong></label>
                {{Form::text('contactPerson', Input::old('contactPerson'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
        <div class="control-group">
            <div class="controls">
                <label for="url"><strong>Web</strong></label>
                {{Form::text('url', Input::old('url'),array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
    </div>

</div><!-- Contact Details -->
<h4 class="dotted-border">Store Images</h4>
<div id="contact-details" class="row" style="padding-left: 30px;">
    <div class="span6">
        <div class="control-group">
            <label for="image1">Store front Image 1</label><br />
            <div class="controls">
                @if(!empty($shop->image1))
                <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image1 }}" alt="Image" />
                @else
                <img src="" alt="No Image" class="overlay-image"/>
                @endif
            </div>
        </div>
        <br />
        <div class="control-group">
            <label for="image2"><strong>Image 2</strong>(Only gif, jpg or png files)</label><br />
            <div class="controls">
                @if(!empty($shop->image2))
                <a class="btn btn-danger btn-mini action_remove" href="#" id="1"><i class="fa fa-trash-o"></i> Remove Image</a><br />
                <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image2 }}" alt="Image" />
                @else
                <img src="" alt="No Image" class="overlay-image"/>
                @endif
            </div>
        </div>
        <br />
        <div class="control-group">
            <label for="image3"><strong>Image 3</strong>(Only gif, jpg or png files)</label><br />
            <div class="controls">
                @if(!empty($shop->image3))
                <a class="btn btn-danger btn-mini action_remove" href="#" id="1"><i class="fa fa-trash-o"></i> Remove Image</a><br />
                <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image3 }}" alt="Image" />
                @else
                <img src="" alt="No Image" class="overlay-image"/>
                @endif
            </div>
        </div>
        <br />
        <div class="control-group">
            <label for="image4"><strong>Image 4</strong>(Only gif, jpg or png files)</label><br />
            <div class="controls">
                @if(!empty($shop->image4))
                <a class="btn btn-danger btn-mini action_remove" href="#" id="1"><i class="fa fa-trash-o"></i> Remove Image</a><br />
                <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image4 }}" alt="Image" />
                @else
                <img src="" alt="No Image" class="overlay-image"/>
                @endif
            </div>
        </div>
        <br />
        <div class="control-group">
            <label for="image5"><strong>Image 5</strong>(Only gif, jpg or png files)</label><br />
            <div class="controls">
                @if(!empty($shop->image5))
                <a class="btn btn-danger btn-mini action_remove" href="#" id="1"><i class="fa fa-trash-o"></i> Remove Image</a><br />
                <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image5 }}" alt="Image" />
                @else
                <img src="" alt="No Image" class="overlay-image"/>
                @endif
            </div>
        </div>
    </div>
    <div class="span6"></div>
</div>


<h4 class="dotted-border">Credit Cards</h4>
<div class="row" style="padding-left: 30px;">
    Mastercard&nbsp;{{ Form::checkbox('mastercard','Y',array('readonly')) }}&nbsp;&nbsp;
    Visa&nbsp;{{ Form::checkbox('visa','Y',array('readonly')) }}&nbsp;&nbsp;
    AMEX&nbsp;{{ Form::checkbox('amex','Y',array('readonly')) }}&nbsp;&nbsp;
    Diners club&nbsp{{ Form::checkbox('diners','Y',array('readonly')) }}
</div>

<!--shop_description-->
</div><!--creditcards-->

<h4 class="dotted-border">New Store?</h4>
<div class="row" style="padding-left: 30px;">
    <div class="span2">
        <div class="control-group">
            <div class="controls">
                <label for="newShop" class="pull-left">Yes</label>&nbsp; &nbsp;
                {{Form::radio('newShop','Y',null,array('readonly')) }}
            </div>
        </div>
    </div>
    <div class="span2">
        <div class="control-group">
            <div class="controls">
                <label for="newShop" class="pull-left">No</label>&nbsp; &nbsp;
                {{Form::radio('newShop','N',null,array('readonly')) }}
            </div>
        </div>
    </div>
</div>
<span><strong>Owner and Manager Contact Details</strong></span>
<div class="row" style="padding-left: 30px">
<div class="span6">
    <div class="span6">
        <div class="control-group">
            <label class="control-label" for="ownerName"><strong>Owner's first name</strong></label>
            <div class="controls">
                {{ Form::text('ownerName',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="ownerCell"><strong>Owner's Cell</strong></label>
            <div class="controls">
                {{ Form::text('ownerCell',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="managerName"><strong>Marketing Manager: first name</strong></label>
            <div class="controls">
                {{ Form::text('managerName',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="managerCell"><strong>Marketing Manager: Cell</strong></label>
            <div class="controls">
                {{ Form::text('managerCell',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="managerName2"><strong>Manager 2: first name</strong></label>
            <div class="controls">
                {{ Form::text('managerName2',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="managerCell2"><strong>Manager 2: Cell</strong></label>
            <div class="controls">
                {{ Form::text('managerCell2',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="managerName3"><strong>Manager 3:first name</strong></label>
            <div class="controls">
                {{ Form::text('managerName3',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="managerCell3"><strong>Manager 3: Cell</strong></label>
            <div class="controls">
                {{ Form::text('managerCell3',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="headOfficeName"><strong>Head Office: first name</strong></label>
            <div class="controls">
                {{ Form::text('headOfficeName',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="headOfficeCell"><strong>Head Office: Cell</strong></label>
            <div class="controls">
                {{ Form::text('headOfficeCell',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="areaManagerName"><strong>Area Manager: first name</strong></label>
            <div class="controls">
                {{ Form::text('areaManagerName',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="areaManagerCell"><strong>Area Manager: Cell</strong></label>
            <div class="controls">
                {{ Form::text('areaManagerCell',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="opsManagerName"><strong>Ops Manager: first name</strong></label>
            <div class="controls">
                {{ Form::text('opsManagerName',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="opsManagerCell"><strong>Ops Manager: Cell</strong></label>
            <div class="controls">
                {{ Form::text('opsManagerCell',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="emergencyName"><strong>Emergency Contact: first name</strong></label>
            <div class="controls">
                {{ Form::text('emergencyName',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="emergencyCell"><strong>Emergency Contact: Cell</strong></label>
            <div class="controls">
                {{ Form::text('emergencyCell',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="emergencyName2"><strong>Emergency Contact(2): first name</strong></label>
            <div class="controls">
                {{ Form::text('emergencyName2',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" for="emergencyCell2"><strong>Emergency Contact(2): Cell</strong></label>
            <div class="controls">
                {{ Form::text('emergencyCell2',null,array('class'=>'txtbar','readonly')) }}
            </div>
        </div>
    </div>
</div>
<div class="span4">
    <div class="control-group">
        <label class="control-label" for="ownerSurname"><strong>Owner's surname</strong></label>
        <div class="controls">
            {{ Form::text('ownerSurname',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="ownerEmail"><strong>Owner's Email</strong></label>
        <div class="controls">
            {{ Form::text('ownerEmail',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="managerSurname"><strong>Marketing Manager :surname</strong></label>
        <div class="controls">
            {{ Form::text('managerSurname',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="managerEmail"><strong>Marketing Manager: Email</strong></label>
        <div class="controls">
            {{ Form::text('managerEmail',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="managerSurname2"><strong>Manager 2:surname</strong></label>
        <div class="controls">
            {{ Form::text('managerSurname2',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="managerEmail2"><strong>Manager 2: Email</strong></label>
        <div class="controls">
            {{ Form::text('managerEmail2',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="managerSurname3"><strong>Manager 3:surname</strong></label>
        <div class="controls">
            {{ Form::text('managerSurname3',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="managerEmail3"><strong>Manager 3: Email</strong></label>
        <div class="controls">
            {{ Form::text('managerEmail3',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="headOfficeSurname"><strong>Head Office:surname</strong></label>
        <div class="controls">
            {{ Form::text('headOfficeSurname',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="headOfficeEmail"><strong>Head Office: Email</strong></label>
        <div class="controls">
            {{ Form::text('headOfficeEmail',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="areaManagerSurname"><strong>Area Manager:surname</strong></label>
        <div class="controls">
            {{ Form::text('areaManagerSurname',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="areaManagerEmail"><strong>Area Manager: Email</strong></label>
        <div class="controls">
            {{ Form::text('areaManagerEmail',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="opsManagerSurname"><strong>Ops Manager:surname</strong></label>
        <div class="controls">
            {{ Form::text('opsManagerSurname',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group {{ ($errors->has('opsManagerEmail') ? 'error' : '') }}">
        <label class="control-label" for="opsManagerEmail"><strong>Ops Manager: Email</strong></label>
        <div class="controls">
            {{ Form::text('opsManagerEmail',null,array('class'=>'txtbar')) }}
            {{ ($errors->has('opsManagerEmail') ? $errors->first('opsManagerEmail') : '') }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="emergencySurname"><strong>Emergency contact:surname</strong></label>
        <div class="controls">
            {{ Form::text('emergencySurname',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="emergencyEmail"><strong>Emergency Contact: Email</strong></label>
        <div class="controls">
            {{ Form::text('emergencyEmail',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="emergencySurname2"><strong>Emergency contact (2):surname</strong></label>
        <div class="controls">
            {{ Form::text('emergencySurname2',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="emergencyEmail2"><strong>Emergency Contact (2): Email</strong></label>
        <div class="controls">
            {{ Form::text('emergencyEmail2',null,array('class'=>'txtbar','readonly')) }}
        </div>
    </div>
</div>
</div>
<span>
        <strong>If reservation notifications are sent who must receive them? (If applicable)</strong>
</span><br />
<?php
$vals = explode(',',$shop->reserveWith);
//        dd($vals);
$owner = '1';
$manager1 = '2';
$manager2 = '3';
$manager3 = '4';
$headoffice = '5';
$finmanager = '6';
$checked = '';
$checked1 = '';
$checked2 = '';
$checked3 = '';
$checked4 = '';
$checked5 = '';
if( in_array($owner,$vals)){
    $checked = ' checked ="checked"';
}
if( in_array($manager1,$vals)){
    $checked1 = ' checked ="checked"';
}
if( in_array($manager2,$vals)){
    $checked2 = ' checked ="checked"';
}
if( in_array($manager3,$vals)){
    $checked3 = ' checked ="checked"';
}
if( in_array($headoffice,$vals)){
    $checked4 = ' checked ="checked"';
}
if( in_array($finmanager,$vals)){
    $checked5 = ' checked ="checked"';
}

?>
Owner&nbsp;{{ Form::checkbox('reserveWith[]','1',$checked,array('readonly')) }}&nbsp;
Manager 1&nbsp;{{ Form::checkbox('reserveWith[]','2',$checked1,array('readonly')) }}&nbsp;
Manager 2&nbsp;{{ Form::checkbox('reserveWith[]','3',$checked2,array('readonly')) }}&nbsp;
Manager 3&nbsp;{{ Form::checkbox('reserveWith[]','4',$checked3,array('readonly')) }}&nbsp;
Head Office&nbsp;{{ Form::checkbox('reserveWith[]','5',$checked4,array('readonly')) }}&nbsp;
Financial Manager&nbsp;{{ Form::checkbox('reserveWith[]','6',$checked5,array('readonly')) }}&nbsp;<br /><br />
{{ Form::close() }}
<hr />
<div class="row" style="padding-left: 30px">
    <div class="span4">
        <span>
            If approved the shop will automatically display on mall guide and your mall's site<br />
            An Email will be sent to the store manager informing them of this<br />
            <br />
            <button class="submit btn btn-success approve-btn" id="approve"><i class="fa fa-thumbs-up"></i> Approve Store</button>
        </span><br /><br />
    </div>
    <div class="span4">
        <span>
            <button class="submit btn btn-danger" id="decline"><i class="fa fa-thumbs-down"></i> Decline Store</button>
        </span><br /><br />
        <div id="decline_rsn">
            <form action="#">
                <span>Please enter a message below, for the store manager explaining why the store has not been approved.</span>
                {{Form::textarea('decline_msg',null,array('class'=>'txtbox','id'=>'decline_msg','rows'=>'3')) }}<br />
            </form>

            <button class="submit btn-mini btn-info" id="confirm_decline">Confirm</button>
        </div>
    </div>
</div>
<!--<button class="submit reg-btn" id="add_review">Submit Details</button>-->
</div><!--row-fluid -->


@stop

@section('exScript')
<script>

    $(document).ready(function(){
        $("#decline_rsn").hide();
        var shop = <?php echo $shop->shopMGID; ?>

        $("#approve").click(function(){

            $.ajax({
                type: "POST",
                url: '/portal/approveshop/'+shop,
                data: {
                    '_token': "{{ csrf_token() }}"
                },
                success: function(){
                    location.reload();
                }
            });

        });

        $("#decline").click(function(e){

            e.preventDefault();
            $("#decline_rsn").show();

            var shop = <?php echo $shop->shopMGID; ?>;
            $("#confirm_decline").click(function(){

                var msg = $("#decline_msg").val();
//                console.log(msg);

                $.ajax({
                    type: "POST",
                    url: '/portal/declineshop/'+shop+'/'+msg,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });


            });

        });
    });

</script>
@stop
