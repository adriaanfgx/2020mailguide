@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Shops Registration </h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                {{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="blog-content container-fluid">
    <div class="row-fluid">
        <div class="biggerLeftMargin">

        <h3>Update shop details for {{ $shop->name }}</h3>

        {{ Form::model($shop, array('route' => array('shops.postEdit', $shop->shopMGID),'method' => 'post','files'=>true ,'name' => 'shopEdit', 'id' => 'shopEdit', 'class' => 'form-inline')) }}
        <input type="hidden" name="mall" value="{{ $shop->mallID }}" />
        {{ Form::hidden('referer',URL::previous()) }}
        <div id="shop_info">
            <div class="control-group {{ ($errors->has('name') ? 'error' : '') }}">
                <label for="name">Shop Name</label>
                <div>
                    {{ Form::text('name',null,array('class'=>'txtbar span4')) }}
                </div>
                <span class="help-block">{{ ($errors->has('name') ? $errors->first('name') : '') }}</span>
            </div>
            <div class="control-group {{ ($errors->has('tradingHours') ? 'error' : '') }}">
                <label for="tradingHours">Trading Hours</label>
                <div class="controls">
                    {{Form::textarea('tradingHours', null,array('class'=>'txtbox span4','rows'=>'3')) }}
                    {{ ($errors->has('tradingHours') ? $errors->first('tradingHours') : '') }}
                </div>
            </div>
        </div>
        <br />
        <div class="row-fluid">
            <h4 class="dotted-border {{ ($errors->has('tradingHours') ? 'error' : '') }}">Shop Category</h4>
            <div class="span4">
                <div class="control-group">
                    <label for="category_id"><strong>Category</strong></label>
                    <div class="controls">
                        {{ Form::select('category_id',$categories,null, ['placeholder' => 'Select category ...'], array('id'=>'category')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="subcategory_id"><strong>Sub Category</strong></label>
                    <div class="controls">
                        {{ Form::select('subcategory_id',$subcategories,null, ['placeholder' => 'Select category ...'], array('id'=>'subcategory')) }}
                    </div>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <h4 class="dotted-border">Shop Description</h4>
            <div class="span4">
                <div class="control-group">
                    <label for="description"><strong>Description</strong></label>
                    <div class="controls">
                        {{Form::textarea('description', null,array('class'=>'txtbox ','rows'=>'3')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="keywords"><strong>Keywords</strong></label>
                    <div class="controls">
                        {{Form::textarea('keywords', null,array('class'=>'txtbox ','rows'=>'3')) }}
                    </div>
                </div>
            </div>
        </div>



        <div class="row-fluid">
            <h4 class="dotted-border">Products</h4>
            <div class="span4">
                <div class="control-group">
                    <label for="product1"><strong>Product 1</strong></label>
                    <div class="controls">
                        {{Form::text('product1',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="product2"><strong>Product 2</strong></label>
                    <div class="controls">
                        {{Form::text('product2', null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label for="product3"><strong>Product 3</strong></label>
                    <div class="controls">
                        {{Form::text('product3', null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="product4"><strong>Product 4</strong></label>
                    <div class="controls">
                        {{Form::text('product4', null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <div class="controls">
                        <label for="product5"><strong>Product 5</strong></label>
                        {{Form::text('product5', null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <div class="controls">
                        <label for="product6"><strong>Product 6</strong></label>
                        {{Form::text('product6', null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <div class="controls">
                        <label for="product7"><strong>Product 7</strong></label>
                        {{Form::text('product7', null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <div class="controls">
                        <label for="product8"><strong>Product 8</strong></label>
                        {{Form::text('product8', null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <div class="controls">
                        <label for="product9"><strong>Product 9</strong></label>
                        {{Form::text('product9', null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <div class="controls">
                        <label for="product10"><strong>Product 10</strong></label>
                        {{Form::text('product10', null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <h4 class="dotted-border">Contact Details</h4>
            <div class="span4">
                <div class="control-group">
                    <div class="controls">
                        <label for="shopNumber"><strong>Shop Number</strong></label>
                        {{Form::text('shopNumber', null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <div class="controls">
                        <label for="landmark"><strong>Landmark</strong></label>
                        {{Form::text('landmark', null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label for="telephone"><strong>Telephone</strong></label>
                    <div class="controls">
                        {{Form::text('telephone', null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <div class="controls">
                        <label for="fax"><strong>Fax</strong></label>
                        {{Form::text('fax', null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label for="cell"><strong>Cell</strong></label>
                    <div class="controls">
                        {{Form::text('cell',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="contactPerson"><strong>Contact Person</strong></label>
                    <div class="controls">
                        {{Form::text('contactPerson', null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}">
                    <label for="email"><strong>Email</strong></label>
                    <div class="controls">
                        {{Form::text('email', null,array('class'=>'txtbar')) }}
                    </div>
                    <span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('url') ? 'error' : '') }}">
                    <div class="controls">
                        <label for="url" class="control-label"><strong>Web</strong> &nbsp; <small>( Must start with http:// or https:// )</small></label>
                        {{Form::text('url',null,array('class'=>'txtbar')) }}
                    </div>
                    <span class="help-block">{{ ($errors->has('url') ? $errors->first('url') : '') }}</span>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <h4 class="dotted-border">Store Images</h4>
            <div class="span4">
                <div class="control-group {{ ($errors->has('image1') ? 'error' : '') }}">
                    <label for="image1" class="control-label"><strong>Store front Image 1</strong></label><br />
                    <div class="controls">
                        @if(!empty($shop->image1))
                        <a class="btn btn-danger btn-mini action_remove" href="#" id="1"><i class="fa fa-trash-o"></i> Remove Image</a><br />
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image1 }}" alt="Image" />
                        @else
                        <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image1') }}</span><br />
                        <span class="help-block">{{ ($errors->has('image1') ? $errors->first('image1') : '') }}</span>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('image2') ? 'error' : '') }}">
                    <label for="image2"><strong>Image 2</strong>(Only gif, jpg or png files)</label><br />
                    <div class="controls">
                        @if(!empty($shop->image2))
                        <a class="btn btn-danger btn-mini action_remove" href="#" id="2"><i class="fa fa-trash-o"></i> Remove Image</a><br />
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image2 }}" alt="Image" />
                        @else
                        <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image2') }}</span><br />

                        <span class="help-block">{{ ($errors->has('image2') ? $errors->first('image2') : '') }}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('image3') ? 'error' : '') }}">
                    <label for="image3"><strong>Image 3</strong>(Only gif, jpg or png files)</label><br />
                    <div class="controls">
                        @if(!empty($shop->image3))
                        <a class="btn btn-danger btn-mini action_remove" href="#" id="3"><i class="fa fa-trash-o"></i> Remove Image</a><br />
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image3 }}" alt="Image" />
                        @else
                        <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image3') }}</span><br />
                        <span class="help-block">{{ ($errors->has('image3') ? $errors->first('image3') : '') }}</span>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('image4') ? 'error' : '') }}">
                    <label for="image4"><strong>Image 4</strong>(Only gif, jpg or png files)</label><br />
                    <div class="controls">
                        @if(!empty($shop->image4))
                        <a class="btn btn-danger btn-mini action_remove" href="#" id="4"><i class="fa fa-trash-o"></i> Remove Image</a><br />
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image4 }}" alt="Image" />
                        @else
                        <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image4') }}</span><br />
                        <span class="help-block">{{ ($errors->has('image4') ? $errors->first('image4') : '') }}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('image5') ? 'error' : '') }}">
                    <label for="image5"><strong>Image 5</strong>(Only gif, jpg or png files)</label><br />
                    <div class="controls">
                        @if(!empty($shop->image5))
                        <a class="btn btn-danger btn-mini action_remove" href="#" id="5"><i class="fa fa-trash-o"></i> Remove Image</a><br />
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image5 }}" alt="Image" />
                        @else
                        <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image5') }}</span><br />
                        <span class="help-block">{{ ($errors->has('image5') ? $errors->first('image5') : '') }}</span>
                    </div>
                </div>
            </div>
            <div class="span4"></div>
        </div>

        <div class="row-fluid">
            <h4 class="dotted-border">Credit Cards</h4>
            Mastercard&nbsp;{{Form::checkbox('mastercard','Y',array('class'=>'pull-left')) }}&nbsp;&nbsp;
            Visa&nbsp;{{Form::checkbox('visa', 'Y',array('class'=>'pull-left')) }}&nbsp;&nbsp;
            AMEX&nbsp;{{Form::checkbox('amex','Y',array('class'=>'pull-left')) }}&nbsp;&nbsp;
            Diners club&nbsp{{Form::checkbox('diners', 'Y',array('class'=>'pull-left')) }}
        </div>
        <br />

        <div class="control-group">
            <label for="newShop"><strong>Is the shop new ?</strong></label>
            <div class="controls">
                {{Form::radio('newShop','Y', 'checked') }}&nbsp;Yes &nbsp;&nbsp;{{Form::radio('newShop','N', 'checked') }}No
            </div>
        </div>

        <div class="control-group">
            <label for="display"><strong>Must your store be displayed?</strong></label>
            <div class="controls">
                {{Form::radio('display','Y', 'checked') }}&nbsp;Yes &nbsp;&nbsp;{{Form::radio('display','N', 'checked') }}No
            </div>
        </div>
        <div class="row-fluid">
            <h4 class="dotted-border">Owner and Manager Contact Details</h4>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="ownerName"><strong>Owner's first name</strong></label>
                    <div class="controls">
                        {{ Form::text('ownerName',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="ownerSurname"><strong>Owner's surname</strong></label>
                    <div class="controls">
                        {{ Form::text('ownerSurname',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="ownerCell"><strong>Owner's Cell</strong></label>
                    <div class="controls">
                        {{ Form::text('ownerCell',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('ownerEmail') ? 'error' : '') }}">
                    <label class="control-label" for="ownerEmail"><strong>Owner's Email</strong></label>
                    <div class="controls">
                        {{ Form::text('ownerEmail',null,array('class'=>'txtbar')) }}
                    </div>
                    <span class="help-block">{{ ($errors->has('ownerEmail') ? $errors->first('ownerEmail') : '') }}</span>
                </div>
            </div>
        </div>

			<div class="row-fluid">
				<h4 class="dotted-border">Financial Manager Contact Details</h4>
				<div class="span4">
					<div class="control-group">
						<label class="control-label" for="financialName"><strong>Financial Manager's first name</strong></label>
						<div class="controls">
							{{ Form::text('financialName',null,array('class'=>'txtbar')) }}
						</div>
					</div>
				</div>
				<div class="span4">
					<div class="control-group">
						<label class="control-label" for="financialSurname"><strong>Financial Manager's surname</strong></label>
						<div class="controls">
							{{ Form::text('financialSurname',null,array('class'=>'txtbar')) }}
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="clearfix"></div>
				<div class="span4">
					<div class="control-group">
						<label class="control-label" for="financialCell"><strong>Financial Manager's Cell</strong></label>
						<div class="controls">
							{{ Form::text('financialCell',null,array('class'=>'txtbar')) }}
						</div>
					</div>
				</div>
				<div class="span4">
					<div class="control-group {{ ($errors->has('financialEmail') ? 'error' : '') }}">
						<label class="control-label" for="financialEmail"><strong>Financial Manager's Email</strong></label>
						<div class="controls">
							{{ Form::text('financialEmail',null,array('class'=>'txtbar')) }}
						</div>
						<span class="help-block">{{ ($errors->has('financialEmail') ? $errors->first('financialEmail') : '') }}</span>
					</div>
				</div>
			</div>

        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="managerName"><strong>Marketing Manager: first name</strong></label>
                    <div class="controls">
                        {{ Form::text('managerName',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="managerSurname"><strong>Marketing Manager :surname</strong></label>
                    <div class="controls">
                        {{ Form::text('managerSurname',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="managerCell"><strong>Marketing Manager: Cell</strong></label>
                    <div class="controls">
                        {{ Form::text('managerCell',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('managerEmail') ? 'error' : '') }}">
                    <label class="control-label" for="managerEmail"><strong>Marketing Manager: Email</strong></label>
                    <div class="controls">
                        {{ Form::text('managerEmail',null,array('class'=>'txtbar')) }}
                    </div>
                    <div class="help-block">{{ ($errors->has('managerEmail') ? $errors->first('managerEmail') : '') }}</div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="managerName2"><strong>Manager 2: first name</strong></label>
                    <div class="controls">
                        {{ Form::text('managerName2',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="managerSurname2"><strong>Manager 2:surname</strong></label>
                    <div class="controls">
                        {{ Form::text('managerSurname2',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="managerCell2"><strong>Manager 2: Cell</strong></label>
                    <div class="controls">
                        {{ Form::text('managerCell2',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('managerEmail2') ? 'error' : '') }}">
                    <label class="control-label" for="managerEmail2"><strong>Manager 2: Email</strong></label>
                    <div class="controls">
                        {{ Form::text('managerEmail2',null,array('class'=>'txtbar')) }}
                    </div>
                    <span class="help-block">{{ ($errors->has('managerEmail2') ? $errors->first('managerEmail2') : '') }}</span>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="managerName3"><strong>Manager 3:first name</strong></label>
                    <div class="controls">
                        {{ Form::text('managerName3',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="managerSurname3"><strong>Manager 3:surname</strong></label>
                    <div class="controls">
                        {{ Form::text('managerSurname3',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="managerCell3"><strong>Manager 3: Cell</strong></label>
                    <div class="controls">
                        {{ Form::text('managerCell3',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('managerEmail3') ? 'error' : '') }}">
                    <label class="control-label" for="managerEmail3"><strong>Manager 3: Email</strong></label>
                    <div class="controls">
                        {{ Form::text('managerEmail3',null,array('class'=>'txtbar')) }}
                    </div>
                    <span class="help-block">{{ ($errors->has('managerEmail3') ? $errors->first('managerEmail3') : '') }}</span>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="headOfficeName"><strong>Head Office: first name</strong></label>
                    <div class="controls">
                        {{ Form::text('headOfficeName',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="headOfficeSurname"><strong>Head Office:surname</strong></label>
                    <div class="controls">
                        {{ Form::text('headOfficeSurname',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="headOfficeCell"><strong>Head Office: Cell</strong></label>
                    <div class="controls">
                        {{ Form::text('headOfficeCell',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('headOfficeEmail') ? 'error' : '') }}">
                    <label class="control-label" for="headOfficeEmail"><strong>Head Office: Email</strong></label>
                    <div class="controls">
                        {{ Form::text('headOfficeEmail',null,array('class'=>'txtbar')) }}
                    </div>
                    <div class="help-block">{{ ($errors->has('headOfficeEmail') ? $errors->first('headOfficeEmail') : '') }}</div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="areaManagerName"><strong>Area Manager: first name</strong></label>
                    <div class="controls">
                        {{ Form::text('areaManagerName',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="areaManagerSurname"><strong>Area Manager:surname</strong></label>
                    <div class="controls">
                        {{ Form::text('areaManagerSurname',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="areaManagerCell"><strong>Area Manager: Cell</strong></label>
                    <div class="controls">
                        {{ Form::text('areaManagerCell',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('areaManagerEmail') ? 'error' : '') }}">
                    <label class="control-label" for="areaManagerEmail"><strong>Area Manager: Email</strong></label>
                    <div class="controls">
                        {{ Form::text('areaManagerEmail',null,array('class'=>'txtbar')) }}
                    </div>
                    <span class="help-block">{{ ($errors->has('areaManagerEmail') ? $errors->first('areaManagerEmail') : '') }}</span>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="opsManagerName"><strong>Ops Manager: first name</strong></label>
                    <div class="controls">
                        {{ Form::text('opsManagerName',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="opsManagerSurname"><strong>Ops Manager:surname</strong></label>
                    <div class="controls">
                        {{ Form::text('opsManagerSurname',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="opsManagerCell"><strong>Ops Manager: Cell</strong></label>
                    <div class="controls">
                        {{ Form::text('opsManagerCell',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('opsManagerEmail') ? 'error' : '') }}">
                    <label class="control-label" for="opsManagerEmail"><strong>Ops Manager: Email</strong></label>
                    <div class="controls">
                        {{ Form::text('opsManagerEmail',null,array('class'=>'txtbar')) }}
                    </div>
                    <span class="help-block">{{ ($errors->has('opsManagerEmail') ? $errors->first('opsManagerEmail') : '') }}</span>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="emergencyName"><strong>Emergency Contact: first name</strong></label>
                    <div class="controls">
                        {{ Form::text('emergencyName',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="emergencySurname"><strong>Emergency contact:surname</strong></label>
                    <div class="controls">
                        {{ Form::text('emergencySurname',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="emergencyCell"><strong>Emergency Contact: Cell</strong></label>
                    <div class="controls">
                        {{ Form::text('emergencyCell',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('emergencyEmail') ? 'error' : '') }}">
                    <label class="control-label" for="emergencyEmail"><strong>Emergency Contact: Email</strong></label>
                    <div class="controls">
                        {{ Form::text('emergencyEmail',null,array('class'=>'txtbar')) }}
                    </div>
                    <span class="help-block">{{ ($errors->has('emergencyEmail') ? $errors->first('emergencyEmail') : '') }}</span>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="clearfix"></div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="emergencyName2"><strong>Emergency Contact(2): first name</strong></label>
                    <div class="controls">
                        {{ Form::text('emergencyName2',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="emergencySurname2"><strong>Emergency contact (2):surname</strong></label>
                    <div class="controls">
                        {{ Form::text('emergencySurname2',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">

            <div class="span4">
                <div class="control-group">
                    <label class="control-label" for="emergencyCell2"><strong>Emergency Contact(2): Cell</strong></label>
                    <div class="controls">
                        {{ Form::text('emergencyCell2',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('emergencyEmail2') ? 'error' : '') }}">
                    <label class="control-label" for="emergencyEmail2"><strong>Emergency Contact (2): Email</strong></label>
                    <div class="controls">
                        {{ Form::text('emergencyEmail2',null,array('class'=>'txtbar')) }}
                    </div>
                    <span class="help-block">{{ ($errors->has('emergencyEmail2') ? $errors->first('emergencyEmail2') : '') }}</span>
                </div>
            </div>
        </div>

        <div class="row-fluid">

            <div class="span4">
                <button class="submit reg-btn" id="add_review">Submit Details</button>
            </div>
            <div class="span4"></div>
        </div>
        <br />
        <div class="clearfix"></div>
        {{ Form::close() }}


        <div class="row-fluid">

            <div class="span-9">
                <h4 class="dotted-border">Shop Downloads</h4>
                <table class="table">
                    <thead>
                    <tr>
                        <td width="50">Type</td>
                        <td>Name</td>
                        <td width="100">Size</td>

                    </tr>
                    </thead>
                    <tbody id="downloadsDiv">
                        @foreach($shop->downloads as $download)
                            <tr>
                                <td width="50" class="text-center"><img src="{{ asset('img/docs/'.$download->filetype.'.png') }}"/></td>
                                <td><a href="/download/mall_{{ $shop->shopMGID }}/{{ $download->filename }}"> {{ $download->name }}</a></td>
                                <td width="100">{{ $download->filesize }}kb</td>

                            </tr>
                        @endforeach
                    </tbody>

                </table>
            </div>
        </div>
        <br/>
        <br/>
        <form id="uploadForm" name="uploadForm" action="/shops/files" method="POST" enctype="multipart/form-data">
            {{ Form::token() }}
            <input type="hidden" name="shopMGID" value="{{ $shop->shopMGID }}" />
            <div class="row-fluid">
                <div class="span4">
                    <div class="control-group">
                        <label for="name"><strong>Name (To appear on link. eg. Menu)</strong></label>
                        <div class="controls">
                            <input  type="text" value="" name="upload_name" id="upload_name" >
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="control-group">
                        <label for="fileUpload"><strong>File</strong></label>
                        <div class="controls">
                            {{Form::file('fileUpload') }}
                        </div>
                        <span class="help-block">Only Images and PDF files are allowed.</span>
                    </div>
                </div>

                <div class="row-fluid">
                    <div class="span12">
                        <button class="btn btn-info" type="button" id="downloadBtn">Upload File</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <br/>
    <br/>
    </div>
</div>
@stop

@section('exScript')
    <script src="{{ asset('js/jquery.form.min.js') }}"></script>
<script>

    $(document).ready(function(){

        $('#uploadForm').ajaxForm();

        var options = {
            target:        '#downloadsDiv',
            success:       showResponse,
            clearForm:  true
        };

        $('#downloadBtn').click(function(){

//            console.log('here');
            $('#uploadForm').ajaxSubmit(options);

        });

        if( $("#category").val() == ''){
            $('#subcategory').empty();
        }

        $("#category").change(function(){
            refreshSubcategoryList();
        });

        $(".action_remove").click(function(e){
            var imageID = $(this).attr('id');
            var route = window.location.href;
            var resultArr = route.split('/');
            var shop = resultArr[5];
            var image = 'image'+imageID;
//            console.log(image);

            e.preventDefault();

            bootbox.confirm("Are you sure you want to remove this entry ? ");

            $(".modal a.btn-primary").click(function(){

                $.ajax({
                    type: "POST",
                    url: '/shops/removeimage/'+shop+'/'+image,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });
        })
    });

    function showResponse(data)
    {
//        console.log('return');
        $('#downloadsDiv').empty().html(data);
    }

    function refreshSubcategoryList()
    {
        if( $('#category').val() != '' ){
            var category = $('#category').val();

            $.ajax({
                type: 'GET',
                url: '/shops/subcategory/'+category,
                data:'',
                success: function (data) {
//                console.log(data);
                    $('#subcategory').empty().append('<option value="">Please Select...</option>');

                    $.each(data, function(key, value){
                        $('#subcategory').append('<option value="'+key+'">'+value+'</option>');
                    })
                }
            });
        }
    }
</script>
@stop
