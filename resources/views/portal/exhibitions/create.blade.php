@extends('layouts.backpages')
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Create Exhibition </h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                {{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="blog-content container-fluid">
    <div class="row-fluid">
        <div class="biggerLeftMargin">

            <h3>Add an exhibition</h3>

            Does your Mall have a current or upcoming Exhibition that you would like to give greater exposure? Mallguide is just the place for you to do that,
            simply complete your Exhibition details below and include it on the site for free...

            <h4 class="dotted-border">How Does It Work?</h4>
            <ul>
                <li>Only the Marketing team or Centre Management can add an Exhibition online </li>
                <li>Your Exhibition is automatically added to Mallguide on submission </li>
                <li>You spread awareness of current Exhibition, and drive more shoppers to your Mall </li>
            </ul>
            <h4>Good To Know</h4>
            <ul>
                <li>Exhibitions are automatically removed on expiry </li>
                <li>Booking a Featured Exhibition ensures even greater exposure.</li>
            </ul>
            {{ Form::open(array('route' => 'exhibitions.postCreate', 'method' => 'post','files'=> true,'name' => 'exhCreate','id' => 'exhCreate', 'class' => 'form-inline')) }}
            <input type="hidden" name="mall" value="{{ $mallID }}">
            {{ Form::hidden('referer',URL::previous()) }}
            <div class="span4">
                <h4 class="dotted-border">Exhibition Details</h4>
                <div class="control-group {{ ($errors->has('name') ? 'error' : '') }}">
                    <label for="name" class="control-label"><strong>Exhibition Name</strong></label>
                    <div class="controls">
                        {{ Form::text('name',null,array('class'=>'txtbar')) }}
                    </div>
                    <span class="help-block">{{ ($errors->has('name') ? $errors->first('name') : '') }}</span>
                </div>
                <span>To specify the category select an existing one from the list below :</span>

                <div class="control-group">
                    <label for="category"><strong>Category List</strong></label>
                    <div class="controls">
                        {{ Form::select('category',$categories,null, ['placeholder' => 'Select a category'], ['id'=>'category']) }}
                    </div>
                </div>
            </div><!--span4-->

            <div class="span4">
                <h4 class="dotted-border">Location Details</h4>
                <span>To specify the location enter a new one into the field below</span>
                <div class="control-group">
                    <label for="location"><strong>Location</strong></label>
                    <div class="controls">
                        <input class="txtbar" type="text" name="location" />
                    </div>
                </div>

                <div class="control-group">
                    <label for="exhibition" class="control-label"><strong>Description</strong></label>
                    <div class="controls">
                        {{ Form::textarea('exhibition',null,array('class'=>'txtbox','rows'=>'3')) }}
                    </div>
                </div>
            </div><!--span4-->


            <div class="span3">
                <h4 class="dotted-border">Dates &amp; Images</h4>
                <div class="control-group {{ ($errors->has('startDate') ? 'error' : '') }}">
                    <label for="startDate" class="control-label"><strong>Start</strong></label>
                    <div class="controls">
                        <div class="input-append datetimepicker4">
                            <input data-format="yyyy-MM-dd" type="text" name="startDate" class="span12" readonly/>
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                      </i>
                    </span>
                        </div>
                        <span class="help-block">{{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}</span>
                    </div>
                </div>
                <div class="clear"></div>
                <br />
                <div class="control-group {{ ($errors->has('endDate') ? 'error' : '') }}">
                    <label for="endDate" class="control-label"><strong>End</strong></label>
                    <div class="controls">
                        <div class="input-append datetimepicker4">
                            <input data-format="yyyy-MM-dd" type="text" name="endDate" class="span12" readonly/>
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                      </i>
                    </span>
                        </div>
                        <span class="help-block">{{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}</span>
                    </div>
                </div>

                <div class="control-group {{ ($errors->has('image') ? 'error' : '') }}">
                    <label for="image" class="control-label"><strong>Select an Image</strong></label>
                    <div class="controls">
                        <span class="btn btn-file">{{ Form::file('image') }}</span>
                    </div>
                    <span class="help-block">{{ ($errors->has('image') ? $errors->first('image') : '') }}</span>
                </div>

                <div class="clear"></div>
                <br />
                <button class="submit reg-btn" id="add_review">Submit Details</button>

            </div><!--span4-->

            {{ Form::close() }}

        </div>
    </div>
</div>
@stop

@section('exScript')
<script>

    $(function () {

        $('.datetimepicker4').datetimepicker({
            pickTime: false
        }).on('changeDate', function(e){
                $(this).datetimepicker('hide');
            });

        $("#category").change(function(){
            $("#category2").val($("#category").val());
        });
    });
</script>
@stop
