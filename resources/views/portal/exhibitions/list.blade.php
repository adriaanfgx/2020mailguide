@extends('layouts.backpages')
@section('seo_meta')
    <meta name="description"
          content="">
    <meta name="keywords" content="">
@stop
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Exhibition Listing</h1>
                    <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp;
                        Communication</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')

@stop

@section('content')
    <div class="container-fluid">
        <div class="container-fluid biggerPadding blog-content">
            <div class="clearfix"></div>
            <div class="row-fluid">
                <div class="span12">
                    <h4 class="hborder">Exhibitions</h4>
                    <table class="table table-border">
                        <thead>
                        <tr>
                            <th>Exhibition Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Display</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(sizeof($exhibitions) > 0)
                            @foreach($exhibitions as $exhibition)
                                <tr>
                                    <td>{{ $exhibition->name }}</td>
                                    <td>{{ date_format(date_create($exhibition->startDate),'jS F Y') }}</td>
                                    <td>{{ date_format(date_create($exhibition->endDate),'jS F Y') }}</td>
                                    <td>{{ displayText($exhibition->display) }}</td>
                                    <td>
                                        <a href="{{ route('exhibitions.edit',$exhibition->exhibitionID) }}"
                                           class="btn btn-info btn-mini"><i class="fa fa-pencil"></i> Edit</a>&nbsp;|&nbsp;<a
                                            class="btn btn-danger btn-mini action_delete"
                                            href="{{ route('events.delete',$exhibition->exhibitionID) }}"
                                            id="{{ $exhibition->exhibitionID }}"><i class="fa fa-trash-o"></i>
                                            Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">There are currently no exhibitions.....</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@section('exScript')
    <script src="{{ asset('js/bootbox.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".action_delete").click(function (e) {

                e.preventDefault();
                var event = $(this).attr("id");

                bootbox.confirm("Are you sure you want to delete this entry ? ");

                $(".modal a.btn-primary").click(function () {
                    $.ajax({
                        type: "POST",
                        url: '/exhibitions/delete/' + event,
                        data: {
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function () {
                            location.reload();
                        }
                    });
                });
            });
        });
    </script>
@stop

