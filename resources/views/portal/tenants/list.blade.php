@extends('layouts.backpages')
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>List Tenants </h1>
                <p class="animated fadeInDown delay2">Manage your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                {{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')


<div class="row-fluid">
	<div class="blog-content">
    <!--    <div class="span4">-->
    <h4 class="dotted-border">Mall Tenants</h4>
    <table class="table table-bordered">
        <thead>
            <th>Email</th>
            <th>Name</th>
            <th>Date Created</th>
            <th>Active</th>
            <th width="150">Actions</th>
        </thead>
        <tbody>
        @foreach( $tenants as $user )
        <tr>
            <td>{{ $user->email }}</td>
            <td>{{ $user->first_name }} {{ $user->last_name }}</td>

            <td>{{ date_format(date_create($user->created_at),'jS F Y') }}</td>
            <td>{{ displayAsText($user->activated) }}</td>

            <td width="150">

                <a class="btn btn-danger btn-mini action_delete" href="/" id="{{ $user->id }}"><i class="fa fa-trash-o"></i> Delete</a>

                @if( !$user->activated)
                <a class="send_activation btn btn-success btn-mini" href="#" id="{{ $user->id }}"><i class="fa fa-envelope"></i> Resend Activation</a>
                @else
                <a class="de_activate btn btn-warning btn-mini" href="#" id="{{ $user->id }}"><i class="fa fa-user-times"></i> De-activate</a>
                @endif

            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

	</div>
</div>
@stop

@section('exScript')
<script>

    $(document).ready(function(){

        $(".action_delete").click(function(){

            var $user = $(this).attr('id');

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $.ajax({
                type: "POST",
                url: '/delete-tenant/'+$user,
                data: {
                    '_token': "{{ csrf_token() }}"
                },
                success: function(){
                    location.reload();
                }
            });

        });

        $(".send_activation").click(function(){

            var $user = $(this).attr('id');

            $.ajax({
                type: "POST",
                url: '/activate-tenant/'+$user,
                data: {
                    '_token': "{{ csrf_token() }}"
                },
                success: function(){
                    location.reload();
                }
            });

        });

    });

</script>
@stop
