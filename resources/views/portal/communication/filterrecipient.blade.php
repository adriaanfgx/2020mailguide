@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Compose SMS Message</h1>

                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div>
            <!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Compose SMS Message</li>
                </ul>
            </div>
            <!--/span6-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')
@stop


@section('content')

<div class="span12 middle-headings white-bg">


    <h4>Compose SMS Message For Tenants</h4>

    You have <strong>{{ $credits }} </strong> SMS credits left, <a href="{{ route('manager.buysmscredits') }}">order more.</a>
    <br />
    <br />
    <div class="row" style="padding-left: 40px;">
        <div class="span4">
            <div class="control-group">
                <label><strong>Select a mall</strong></label>
                <div class="controls">
                    {{ Form::select('mallID',$malls,$mall,array('id'=>'mallID')) }}
                </div>
            </div>
            <div class="control-group">
                {{ Form::select('smsRecipient',$smsRecipients,$type,array('id'=>'smsRecipient')) }}
            </div>
        </div>
    </div>
    <strong>Additional SMS recipients:</strong><br />
    You can send the SMS message to additional cell numbers (up to 10). Simply add the extra numbers in the fields provided below (Please start all cell numbers with a <strong>0</strong> eg. <strong>0</strong>81234567):
    <br />
    <br />
    <form action="{{ route('manager.postSendSMS') }}" method="post" name="postSendSMS" id="postSendSMS" class="form-inline">
        {{ Form::hidden('type',$type,array('id'=>'type')) }}
        {{ Form::hidden('mallID',$mall) }}
        <div class="row" style="padding-left: 40px;">
            <div class="span2">
                <div class="control-group">
                    <input type="text"  name="cell_number[]" class="numeric" />
                </div>
            </div>
            <div class="span2">
                <div class="control-group">
                    <input type="text"  name="cell_number[]" class="numeric" />
                </div>
            </div>
        </div>
        <div class="row" style="padding-left: 40px;">
            <div class="span2">
                <div class="control-group">
                    <input type="text"  name="cell_number[]" class="numeric" />
                </div>
            </div>
            <div class="span2">
                <div class="control-group">
                    <input type="text"  name="cell_number[]" class="numeric" />
                </div>
            </div>
        </div>
        <div class="row" style="padding-left: 40px;">
            <div class="span2">
                <div class="control-group">
                    <input type="text"  name="cell_number[]" class="numeric" />
                </div>
            </div>
            <div class="span2">
                <div class="control-group">
                    <input type="text"  name="cell_number[]" class="numeric" />
                </div>
            </div>
        </div>
        <div class="row" style="padding-left: 40px;">
            <div class="span2">
                <div class="control-group">
                    <input type="text"  name="cell_number[]" class="numeric" />
                </div>
            </div>
            <div class="span2">
                <div class="control-group">
                    <input type="text"  name="cell_number[]" class="numeric" />
                </div>
            </div>
        </div>
        <div class="row" style="padding-left: 40px;">
            <div class="span2">
                <div class="control-group">
                    <input type="text"  name="cell_number[]" class="numeric" />
                </div>
            </div>
            <div class="span2">
                <div class="control-group">
                    <input type="text"  name="cell_number[]" class="numeric" />
                </div>
            </div>
        </div>

        <br />
        <div class="row" style="padding-left: 40px;">
            <div class="span4">
                <div class="control-group {{ ($errors->has('title') ? 'error' : '') }}">
                    <label for="message" class="control-label"><strong>Compose message</strong>&nbsp;&nbsp;<small>(This Field is compulsory)</small></label>
                    <textarea type="text" value="" name="title" id="title" maxlength="160" onkeyup="limitTextCount('title', 'divcount', 139);" onkeydown="limitTextCount('title', 'divcount', 139);"></textarea>
                    <span class="help-block">{{ ($errors->has('title') ? $errors->first('title') : '') }}</span>
                    <br />
                    <small>Please note "REPLY STOP TO OPT OUT" Will be appended to the end of your messsage.</small>
                    <br>
                    <div id="divcount">139 charachter(s) to go..</div>
                </div>
            </div>
        </div>
        <br />
        Select the checkboxes next to the tenants you wish to send the SMS to.<br />
        <br />
        <br />
        <a href="#" class="checkall">Select All</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="#" class="uncheckall">Unselect All</a>
        <span class="pull-right" id="num_recipient"></span>
        {{ Form::hidden('creditsSending',null,array('id'=>'creditsSending')) }}
        <br />
        <div id="recipients">
            <span class="text text-error" id="warning_text"><strong>The highlighted numbers are invalid, Click &nbsp;&nbsp;<a href="#" id="remove_invalid"><strong>Here</strong></a> &nbsp;&nbsp;to remove them.Please ensure that update your tenant information with correct data..</strong></span>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Select All</th>
                    <th>Shop Name</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <span class="text text-error"><strong>{{ ($errors->has('recipientList') ? $errors->first('recipientList') : '') }}</strong></span>
                @if( sizeof($recipientList) > 0 )
                @foreach( $recipientList as $list )
                <?php
                    $id = explode("/",$list->recipient);
                ?>
                <tr class="{{ preg_replace('/\s+/', '',$id[0]) }}">
                    <td>
                        {{ Form::checkbox('recipientList[]',$list->recipient,false,array('class'=>'check_recipient')) }}
                    </td>
                    <td>
                        <a href="{{ route('shops.update',$list->shopMGID) }}">{{ $list->name }}</a>

                    </td>
                    <td nowrap id="{{ $list->shopMGID }}">
                        {{ $list->recipient }} &nbsp;&nbsp;&nbsp;
                        <input type="text" name="updateContact" value="{{ $list->recipient }}" class="edit_contact span3 numeric" id="{{ preg_replace('/\s+/', '',$id[0]) }}" />&nbsp;&nbsp;&nbsp;&nbsp;
                        <button class="submit reg-btn update_contact" id="user_{{ preg_replace('/\s+/', '',$id[0]) }}" data-id="{{ $list->shopMGID }}">Update Contact</button>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="3">There are no entries for your selection ....</td>
                </tr>
                @endif
                </tbody>
            </table>
            <button class="submit reg-btn" id="send_sms">Validate</button>
            <button class="submit reg-btn" id="send_valid">Send SMS</button>
        </div>
    </form>

</div><!--/span12-->

@stop

@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){
//        alert($("#type").val());
        $(".edit_contact").hide();
        $(".update_contact").hide();
        var totalRecipients = $(':checkbox:checked').size();
        $("#num_recipient").html('Selected &nbsp;<strong>'+totalRecipients+'</strong>&nbsp; recipients..');

        $("#warning_text").hide();
        $("#send_valid").hide();
        $(".numeric").mask('0b99999999');

        $(".check_recipient").click(function(){

            totalRecipients = $(':checkbox:checked').size();

            $("#num_recipient").html('Selected &nbsp;<strong>'+totalRecipients+'</strong>&nbsp; recipients..');
            $("#creditsSending").val(totalRecipients);
        });

        $("#smsRecipient").change(function(){

            var userType = $(this).val();
            var mall = $("#mallID").val();

            if( userType == 'all' ){
                window.location.href = '/manager/sendsms';
            }else{
                window.location.href = '/manager/sendsms/'+userType+'/'+mall;
            }
        });

        $(".checkall").click(function(){
            $('input:checkbox').prop('checked',true);

            var totalRecipients = $(':checkbox:checked').size();
            $("#num_recipient").html('Selected &nbsp;<strong>'+totalRecipients+'</strong>&nbsp; recipients..');
            $("#creditsSending").val(totalRecipients);


            $(".check_recipient").click(function(){
                totalRecipients = $(':checkbox:checked').size();
                $("#num_recipient").html('Selected &nbsp;<strong>'+totalRecipients+'</strong>&nbsp; recipients..');
                $("#creditsSending").val(totalRecipients);
            });

        });

        $("#send_sms").click(function(e){

            e.preventDefault();

            $.ajax({
                type: "POST",
                url: '/manager/validaterecipient',
                data: $("#postSendSMS").serialize(),
                success: function (data) {

                    $.each(data, function(key,value){

                        var newVal = value.replace(/\/.*$/, "");

                        $('.'+newVal).css({"backgroundColor":"#FF33FF;"});
                        $('#'+newVal).show();
                        $("#warning_text").show();
                        $("#user_"+newVal).show();
                        $('html, body').scrollTop(500);

                        $("#remove_invalid").click(function(){

                            $("."+newVal+" input:checkbox").prop('checked',false);

                            totalRecipients = $(':checkbox:checked').size();
                            $("#num_recipient").html('Selected &nbsp;<strong>'+totalRecipients+'</strong>&nbsp; recipients..');
                            $("#creditsSending").val(totalRecipients);

                        });

                    });

                    $("#send_sms").hide();
                    $("#send_valid").show();
                }
            });
        });

        $(".uncheckall").click(function(){
            $('input:checkbox').prop('checked',false);
//            alert($('input:checkbox :checked').size());
        });

        $(".update_contact").click(function(e){
            e.preventDefault();

            var userType = $("#type").val();
            var shop = $(this).attr('data-id');
            var row = $(this).attr('id');
//            console.log(row.split('_'));
            var cell = row.split('_')[1];
            var newCell = $("#"+cell).val();
            $(".numeric").mask('0b99999999');
            var type = '';
            if( userType == 'owners' ){
                type = 'ownerCell';
            }if( userType == 'manager1' ){
                type = 'managerCell';
            }if( userType == 'manager2' ){
                type = 'managerCell2';
            }if( userType == 'manager3' ){
                type = 'managerCell3';
            }if( userType == 'head' ){
                type = 'headOfficeCell';
            }if( userType == 'financial' ){
                type = 'financialCell';
            }if( userType == 'area' ){
                type = 'areaManagerCell';
            }if( userType == 'ops' ){
                type = 'opsManagerCell';
            }if( userType == 'emergency1' ){
                type = 'emergencyCell';
            }if( userType == 'emergency2' ){
                type = 'emergencyCell2';
            }

//            console.log($(this).attr('class'));
//            console.log(newCell);

            $.ajax({
                type: "POST",
                url: '/manager/updateusercell/'+shop+'/'+type+'/'+newCell,
                data: {
                    '_token': "{{ csrf_token() }}"
                },
                success: function (data) {

                    if( type == 'ownerCell' ){
                        $("#"+data.shopMGID).html(data.ownerCell);
                    }if( type == 'managerCell' ){
                        $("#"+data.shopMGID).html(data.managerCell);
                    }if( type == 'managerCell2' ){
                        $("#"+data.shopMGID).html(data.managerCell2);
                    }if( type == 'managerCell3' ){
                        $("#"+data.shopMGID).html(data.managerCell3);
                    }if( type == 'headOfficeCell' ){
                        $("#"+data.shopMGID).html(data.headOfficeCell);
                    }if( type == 'financialCell' ){
                        $("#"+data.shopMGID).html(data.financialCell);
                    }if( type == 'areaManagerCell' ){
                        $("#"+data.shopMGID).html(data.areaManagerCell);
                    }if( type == 'opsManagerCell' ){
                        $("#"+data.shopMGID).html(data.opsManagerCell);
                    }if( type == 'emergencyCell' ){
                        $("#"+data.shopMGID).html(data.emergencyCell);
                    }if( type == 'emergencyCell2' ){
                        $("#"+data.shopMGID).html(data.emergencyCell2);
                    }
//
//                    console.log(type);
                }
            });

//
        });

    });

    function limitTextCount(limitField_id, limitCount_id, limitNum)
    {
        var limitField = document.getElementById(limitField_id);
        var limitCount = document.getElementById(limitCount_id);
        var fieldLEN = limitField.value.length;

        if (fieldLEN > limitNum)
        {
            limitField.value = limitField.value.substring(0, limitNum);
        }
        else
        {
            limitCount.innerHTML = (limitNum - fieldLEN) + ' charachter(s) to go.';
        }
    }
</script>
@stop
