@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
@include('portal.partials.submenu')
@stop


@section('content')

<div class="row-fluid">
	<div class="span12 blog-content">

	    <form action="{{ route('manager.postBuysmscredits') }}" method="post" name="buySMSCredits" id="buySMSCredits" class="form-inline">
	        <h3>Order SMS Credits</h3>

			<div class="well ">
		        <strong>Please note: communication credits will only be activated once payment has been received.</strong>
	        </div>
	        <span>Required Fields are marked with * </span><br />
	        <div class="span4">
	            <h4 class="dotted-border">Invoice Details</h4>

	            <div class="control-group">
	                <label for="mallID"><strong>Mall &nbsp;</strong></label>
	                    <b>{{ $activeMall }}</b>
	            </div>
	            <div class="control-group {{ ($errors->has('accountName') ? 'error' : '') }}">
	                <label for="accountName" class="control-label"><strong>Name of person receiving invoice *</strong></label>
	                <div class="controls">
	                    {{ Form::text('accountName',$name,array('class'=>'txtbar','id'=>'accountName')) }}
	                    <span class="help-block">{{ ($errors->has('accountName') ? $errors->first('accountName') : '') }}</span>
	                </div>
	            </div>
	            <div class="control-group {{ ($errors->has('accountEmail') ? 'error' : '') }}">
	                <label for="accountEmail" class="control-label"><strong>Email of person receiving invoice *</strong></label>
	                <div class="controls">
	                    {{ Form::text('accountEmail',$user->email,array('class'=>'txtbar','id'=>'accountEmail')) }}
	                    <span class="help-block">{{ ($errors->has('accountEmail') ? $errors->first('accountEmail') : '') }}</span>
	                </div>
	            </div>


	        </div>

	        <!--span4-->

	        <div class="span7">
	            <h4 class="dotted-border">Package Options</h4>
	            {{ Form::hidden('numCredits',null,array('id'=>'num_credits')) }}
	            {{ Form::hidden('mallID',Session::get('activeMallID')) }}
	            <br />
	            <table class="table">
	                <tbody>
	                    <tr>
	                        <td style="border-top: none;"><span class="radioH">{{ Form::radio('description','SMS Credits - Feather package (250 credits)',false,array('class'=>'credit_price','id'=>250)) }} Feather </span></td>
	                        <td style="border-top: none;">{{ $featherCount }} sms's</td>
	                        <td style="border-top: none;">Valid for 12 months</td>
	                        <td style="border-top: none;">R{{ $featherCost }} excl. VAT</td>
	                    </tr>
	                    <tr>
	                        <td style="border-top: none;">{{ Form::radio('description','SMS Credits - Lite package (500 credits)',false,array('class'=>'credit_price','id'=>500)) }} Lite  </td>
	                        <td style="border-top: none;">{{ $liteCount }} sms's</td>
	                        <td style="border-top: none;">Valid for 12 months</td>
	                        <td style="border-top: none;">R{{ $liteCost }} excl. VAT</td>
	                    </tr>
	                    <tr>
	                        <td style="border-top: none;">{{ Form::radio('description','SMS Credits - Middle package (1500 credits)',false,array('class'=>'credit_price','id'=>1000)) }} Middle </td>
	                        <td style="border-top: none;">{{ $middleCount }} sms's</td>
	                        <td style="border-top: none;">Valid for 12 months</td>
	                        <td style="border-top: none;">R{{ $middleCost }} excl. VAT</td>
	                    </tr>
	                    <tr>
	                        <td style="border-top: none;">{{ Form::radio('description','SMS Credits - Heavy package (1500 credits)',false,array('class'=>'credit_price','id'=>2000)) }} Heavy</td>
	                        <td style="border-top: none;">{{ $heavyCount }} sms's</td>
	                        <td style="border-top: none;">Valid for 12 months</td>
	                        <td style="border-top: none;">R{{ $heavyCost }} excl. VAT</td>

	                    </tr>
						<tr> <td colspan="4" style="border-top: none;"><span class="help-block error">{{ ($errors->has('description') ? $errors->first('description') : '') }}</span></td></tr>
	                </tbody>
	            </table>


	        </div>
	        <!--span4-->

            <div class="span4">
                <div class="control-group">
	                <label for="poNumber" class="control-label"><strong>PO Number</strong></label>
	                <div class="controls">
	                    {{ Form::text('poNumber',null,array('class'=>'txtbar ','id'=>'poNumber')) }}
	                    <span class="help-block">{{ ($errors->has('poNumber') ? $errors->first('poNumber') : '') }}</span>
	                </div>
	            </div>
	            <div class="control-group">
	                <label for="postal_code" class="control-label"><strong>Postal Code</strong></label>
	                <div class="controls">
	                    {{ Form::text('postal_code',$user->postal_code,array('class'=>'txtbar ','id'=>'postal_code')) }}
	                    <span class="help-block">{{ ($errors->has('postal_code') ? $errors->first('postal_code') : '') }}</span>
	                </div>
	            </div>
            </div>
        <div class="span6">

	        <div class="control-group span8">
	            <label for="accountAddress" class="control-label"><strong>Postal Address</strong></label>
	            <div class="controls">
	                {{ Form::textarea('accountAddress',$user->postal_address,array('class'=>'txtbar ','id'=>'accountAddress','rows'=>'3')) }}
	                <span class="help-block">{{ ($errors->has('accountAddress') ? $errors->first('accountAddress') : '') }}</span>
	            </div>
	        </div>
        </div>

		<div class="span12">
            <button type="submit" class="btn btn-info">Submit Details</button>
            <a type="button" href="{{ route('portal.index') }}" class="btn btn-warning">Cancel</a>
		</div>


	        <!--span4-->
	    </form>

	</div><!--/span12-->
</div><!--/span12-->

@stop

@section('exScript')

<script type="text/javascript">

    $(function () {

        $(".credit_price").click(function(){
//            alert();
            $("#num_credits").val($(this).attr('id'));
        });

    });
</script>
@stop
