@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span10">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>SMS Reports</h1>

                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div>
            <!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">View SMS reports</li>
                </ul>
            </div>
            <!--/span6-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')
@stop


@section('content')
<div class="row-fluid">
<div class="span12 middle-headings blog-content">

    <h4 class="dotted-border">SMS Messages sent to tenants <small>(Click to open)</small></h4>
	<p><small>Please note: Reports are only viewable 24 hours after sending to allow for delivery.</small></p>
    
    
            @if( isset($messages ) )
			
			<div class="accordion" id="accordion2">
				
			<?php
			$title = '';
			$count=false;
                $end = '';
			foreach( $messages as $message ){

				$mess = $message->smsBody;
				$mess = str_replace(' REPLY STOP TO OPT OUT','', $mess);
				if($mess != $title) { 
					if($count == true) { 
						echo '</table></div></div></div>';
					}
					$count = true;
			?>
					
					<div class="accordion-group">
						<div class="accordion-heading">
							
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse_{{ $message->smsMessageID }}">
								{{ $mess }}
								
								<span class="pull-right">{{ date('F j Y H:i:s', strtotime($message->dateAdded)) }}</span>
							</a>
						</div>
						<div id="collapse_{{ $message->smsMessageID }}" class="accordion-body collapse">
							<div class="accordion-inner">
								<table class="table table-condensed table-hover">
			<?php
					
					$title = $mess;
				} else {
//					$count=true;
				}
			
			?>
				
				<tr>
					<td>Delivered: {{ $message->dateAdded }}</td>
					<td>Cell: {{ $message->recipients }}</td>
					<td>Status: {{ $message->submissionStatus }}</td>

					
				</tr>
				<?php
				}

				echo $end;
			 
				?>
            @else
            <p>No reports for this User.</p>
          
            @endif
			</table>
			</div>
		</div>
		</div>
	</div>  
</div><!--/span12-->
</div>
@stop

@section('exScript')

<script type="text/javascript">

    $(function () {

    });
</script>
@stop
