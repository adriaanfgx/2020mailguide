@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>SMS Reports</h1>

                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div>
            <!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">View SMS reports</li>
                </ul>
            </div>
            <!--/span6-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')
@stop


@section('content')

<div class="span12 middle-headings white-bg">

    <h4 class="dotted-border">SMS Message sent to tenants</h4>
    <div class="row" style="padding-left: 40px;">
        <div class="span4">
            You have &nbsp;<strong>{{ $userCredits }}</strong> SMS credits left, &nbsp;<a href="{{ route('manager.buysmscredits') }}">order more.</a><br />
        </div>
        <div class="span4"></div>
    </div>
    <div class="row" style="padding-left: 40px;">
        <div class="span4">
            <div class="control-group">
                <label for="dataMessage"><strong>Message</strong></label>
                <div class="controls">
                    {{ Form::textarea('dataMessage',$campaign->sentMessage,array('readonly','rows'=>'3')) }}
                </div>
            </div>
        </div>
        <div class="span4"></div>
    </div>
    <br />
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Cell Number</th>
            <th>Date Sent</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody>
        @if( isset($campaign->recipients ) )
        @foreach( $campaign->recipients as $campaign )
        <tr>
            <td>{{ $campaign->number }}</td>
            @if( $campaign->status != 'invalid' && $campaign->delivered !='')
            <td>{{ date('F j Y H:i:s', strtotime($campaign->delivered)) }}</td>
            @else
            <td>{{ "Not delivered" }}</td>
            @endif
            <td>
                {{ $campaign->status }}
            </td>
        </tr>
        @endforeach
        @else
        <tr>
            <td colspan="4">{{ $campaign->message }}.....</td>
        </tr>
        @endif
        </tbody>
    </table>
</div><!--/span12-->

@stop

@section('exScript')

<script type="text/javascript">

    $(function () {

    });
</script>
@stop
