@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span10">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Compose SMS Message</h1>

                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div>
            <!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Compose SMS Message</li>
                </ul>
            </div>
            <!--/span6-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')
@stop


@section('content')
<div class="row-fluid">
<div class="span12 middle-headings blog-content">



	<form action="{{ route('manager.postSendSMS') }}" method="post" name="sendSMS" id="sendSMS" class="form-inline" onsubmit="return canIsend()">
		
	<div class="row-fluid">
		<div class="span6">
			<h4>Compose SMS Message For Tenants</h4>
            <?php
            if($credits == 0)
            {
                $class = 'badge-warning';
            } else {
                $class='badge-success';
            }

                ?>
			You have <span id="numCredits" class="badge {{ $class }}">{{ $credits }}</span> SMS credits left, <a href="{{ route('manager.buysmscredits') }}">order more.</a>
			
			{{ Form::hidden('mallID',$mall->mallID,array('id'=>'mallID')) }}
			<input type="hidden" name="shopIDarray" id="shopIDarray" value="" />
			
				
					
			<div class="control-group {{ ($errors->has('title') ? 'error' : '') }}">
				<label for="message" class="control-label"><strong>Message</strong></label>
				<textarea type="text" value="" name="title" id="title" maxlength="160" onkeyup="limitTextCount('title', 'divcount', 139);" onkeydown="limitTextCount('title', 'divcount', 139);"></textarea>

				<span class="help-block">{{ ($errors->has('title') ? $errors->first('title') : '') }}</span>
				<br />
				<small>Please note "REPLY STOP TO OPT OUT" Will be appended to the end of your messsage.</small>
				<br>
				<div id="divcount">139 charachter(s) to go..</div>
			</div>

			

					
			
		</div>
		
		<div class="span6">
			<div class="well">
				<h5>Send a test</h5>
				<p><small>Before sending the message to all your tenants you can send yourself a test message. (<strong>Please note:</strong> a credit will be deducted for the test message.)</small></p>
				<div class="control-group {{ ($errors->has('cell_number[]') ? 'error' : '') }}">
					<label for="cell_number" class="control-label"><strong>Cell Number</strong>&nbsp;&nbsp;<small>(e.g&nbsp;&nbsp;27729957007)</small></label>
					<div class="input-append">
						<input type="text"  name="cell_number" id="cell_number" class="extras2" />
						<button class="btn" id="testBtn" type="button">Test</button>
					</div>
					<span class="help-block">{{ ($errors->has('cell_number') ? $errors->first('cell_number') : '') }}</span>
					
					
				</div>
				<div id="testResults"></div>
			</div>
		</div>
	</div>

		<div class="row-fluid">
			<div class="span12">
				<h4 class="dotted-border">Additional recipients</h4>
				<small>You can send the SMS message to additional cell numbers (up to 12). Simply add the extra numbers in the fields provided below.</small>
			</div>
		</div>

		<div class="row-fluid">	
			<div class="span12">
				<input type="text" name="extras[]" class="extras span2" value="" />
				<input type="text" name="extras[]" class="extras span2" value="" />
				<input type="text" name="extras[]" class="extras span2" value="" />
				<input type="text" name="extras[]" class="extras span2" value="" />
				<input type="text" name="extras[]" class="extras span2" value="" />
				<input type="text" name="extras[]" class="extras span2" value="" />
				<input type="text" name="extras[]" class="extras span2" value="" />
				<input type="text" name="extras[]" class="extras span2" value="" />
				<input type="text" name="extras[]" class="extras span2" value="" />
				<input type="text" name="extras[]" class="extras span2" value="" />
				<input type="text" name="extras[]" class="extras span2" value="" />
				<input type="text" name="extras[]" class="extras span2" value="" />
			</div>
		</div>
          
    <div class="row-fluid">
        <div class="span12">
            <h4 class="dotted-border">Select you recipients</h4>
			<table class="table table-condensed table-striped table-hover">
				<thead>
					<tr>
						<th colspan="3">
							<div class="span2 pull-left">
								<label class="checkbox spaced">
									<input type="checkbox" class="selectAll" id="selectAll" name="selectAll" value="1"> Select All
								</label>
							</div>
							<div class="span10 pull-right text-right">
								<label class="spaced">Send to: </label>
								<label class="checkbox spaced">
									<input type="checkbox"  class="selAll" id="all" name="all" value="all" checked="checked"> All Contacts
								</label>
								<label class="checkbox spaced">
									<input type="checkbox" class="recipientGroup selOne" id="owners" name="owners" value="owners"> Owners
								</label>
								<label class="checkbox spaced">
									<input type="checkbox" class="recipientGroup selOne" id="managers" name="managers" value="managers"> Managers
								</label>
								<label class="checkbox spaced">
									<input type="checkbox" class="recipientGroup selOne" id="headoffice" name="headoffice" value="headoffice"> Head Office
								</label>
								<label class="checkbox spaced">
									<input type="checkbox" class="recipientGroup selOne" id="financial" name="financial" value="financial"> Financial Officers
								</label>
							</div>
						</th>
					</tr>
				</thead>
				<tbody id="recipientsBody">
					<tr>
						<td colspan="3">Please select one or more recipient groups...</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">
							<input type="hidden" name="count" id="count" value="" />
							<input type="hidden" name="number_array" id="number_array" value="" />
							<div class="pull-right">Selected recipients: <span class="badge badge-inverse" id="totalRecipients">0</span></div>
						</td>
					</tr>
				</tfoot>
			</table>
        </div>
		<div class="span12">
			<button type="submit" class="btn btn-success" name="submit" id="sendMail">Submit</button>
		</div>

    </div>
	</form>
</div><!--/span12-->
</div>
@stop

@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

		$(".extras").mask('27b99999999');
		$(".extras2").mask('27b99999999');
		$('.extras').blur(function(){
			triggerCount();
		});
		
		$('#selectAll').change(function(){
			if($(this).is(':checked'))
			{
				$('.chooseShop').prop('checked', true);
			} else {
				$('.chooseShop').prop('checked', false);
			}
			$('#recipientsBody .chooseShop').trigger('change');
		});

		$('#all').change(function(){
			if($(this).is(':checked'))
			{
				$('.selOne').prop('checked', false);
				getContacts();
			} 
		});

		
		
		var numbers_array = [];
		
        $('.recipientGroup').click(function(){
			$('#all').prop('checked', false);
			getContacts();
		});
		
		//$('#sendMail').click(function(){
		//	var res = canIsend();
		//	
		//	if(res)
		//	{
		//		$('#sendSMS').submit();
		//	}
		//});

		$('#recipientsBody').on('change', '.chooseShop', function(){
			triggerCount();
			var total = parseInt($('#totalRecipients').text());
			var shops = [];
			var nums = [];
			$('.chooseShop').each(function(){
				if($(this).is(':checked'))
				{
					var add = parseInt($(this).attr('data-count'));
					var id = $(this).val();
					shops.push($(this).val());
					var n = $('#numbers_'+id).val();
					var n_str = n.split(',');
					$.each(n_str,function(i,value){
						nums.push(value);
					})
					total += add;
					
				} 
			});
			$('#totalRecipients').text(total);
			$('#count').val(total);
			
			$('#number_array').val(nums.toString());
			
		});
		
		$('#testBtn').click(function(){
			if($('#title').val() == '')
			{
				alert('Please enter a message!');
				$('#title').focus();
				
			} else if($('#cell_number').val() == ''){
				alert('Please enter a test number!');
				$('#cell_number').focus();
			} else {
				$(this).attr('disabled','disabled');
				$('#testResults').empty().html('Sending message...');

				var cell = $('#cell_number').val();
				var cell2 = cell.replace(' ','');


				var clean = cell2;
				if(cell2.substr(0,1)== '0')
				{
					clean = '27' + cell2.substr(1);
				}

				var message = $('#title').val();

				$.ajax({
					url: '/manager/sendtestsms',
					data: {cell_number: clean, title: message},
					method: 'post',
					dataType: 'html',
					success: function(data) {
						$('#testResults').empty().text(data);
						$('#testBtn').removeAttr('disabled');
					},
					error: function(xhr, textStatus, errorThrown){
						$('#testResults').empty().text( errorThrown ? errorThrown : xhr.status );
						$('#testBtn').removeAttr('disabled');
					}
				});
			}
            
			
			
		})

		getContacts();
    });
	
	function canIsend()
	{
		if($('#title').val() == '')
		{
			alert('Please enter a message!');
			$('#title').focus();
			return false;
		}
		var toSend = parseInt($('#totalRecipients').text())
		if(toSend < 1)
		{
			alert('You have no recipients selected!');
			return false;
		}
		return true;
	}
	
	function getContacts()
	{
		
		$('#recipientsBody').empty().html('<tr><td colspan="3">Loading contacts...</td></tr>');
		var groups = [];
		$('#totalRecipients').text('0');
		$('#count').val('0');
		$('.recipientGroup').each(function(){
			if($(this).is(':checked'))
			{
				groups.push($(this).attr('id'));
			}
		});
//		console.log(groups);
		if(groups.length == 0)
		{
			groups.push('all');
//				$('#recipientsBody').empty().html('<tr><td colspan="3">Please select one or more recipient groups...</td></tr>');
		}
		$.ajax({
			url: '/manager/getrecipients',
			data: {groups: groups},
			method: 'post',
			dataType: 'html',
			success: function(data) {
				$('#recipientsBody').empty().html(data);

			}
		});

		triggerCount();
	}
	
	function triggerCount()
	{
		
		var count = 0;
		$('.extras').each(function(){
			if($(this).val() != '')
			{
				count++;
			}
		});

		$('#totalRecipients').text(count);
		$('#count').val(count);
	}

    function limitTextCount(limitField_id, limitCount_id, limitNum)
    {
        var limitField = document.getElementById(limitField_id);
        var limitCount = document.getElementById(limitCount_id);
        var fieldLEN = limitField.value.length;

        if (fieldLEN > limitNum)
        {
            limitField.value = limitField.value.substring(0, limitNum);
        }
        else
        {
            limitCount.innerHTML = (limitNum - fieldLEN) + ' charachter(s) to go.';
        }
    }
</script>
@stop
