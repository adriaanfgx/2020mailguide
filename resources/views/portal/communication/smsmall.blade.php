@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Compose SMS Message</h1>

                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div>
            <!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Compose SMS Message</li>
                </ul>
            </div>
            <!--/span6-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')
@stop


@section('content')

<div class="span12 middle-headings white-bg">


    <h4>Compose SMS Message For Tenants</h4>

    You have <strong>{{ $credits }} </strong> SMS credits left, <a href="{{ route('manager.buysmscredits') }}">order more.</a>
    <br />
    <br />
    <div class="row" style="padding-left: 40px;">
        <div class="span4">
            <div class="control-group">
                <label><strong>Select a mall</strong></label>
                <div class="controls">
                    {{ Form::text('mallID',$mall->name,array('readonly')) }}
                </div>
            </div>
        </div>
    </div>
    <strong>Send a test</strong><br />
    Before sending the message to all your tenants you can send yourself a test message. (<strong>Please note:</strong> a credit will be deducted for the test message.)
    <br />
    <br />
    <form action="{{ route('manager.sendtestsms') }}" method="post" name="sendTestSMS" id="sendTestSMS" class="form-inline">
        {{ Form::hidden('mallID',$mall->mallID,array('id'=>'mallID')) }}
        <div class="row" style="padding-left: 40px;">
            <div class="span4">
                <div class="control-group {{ ($errors->has('title') ? 'error' : '') }}">
                    <label for="message" class="control-label"><strong>Test message</strong></label>
                    <textarea type="text" value="" name="title" id="title" maxlength="160" onkeyup="limitTextCount('title', 'divcount', 139);" onkeydown="limitTextCount('title', 'divcount', 139);"></textarea>
                    <span class="help-block">{{ ($errors->has('title') ? $errors->first('title') : '') }}</span>
                    <br />
                    <small>Please note "REPLY STOP TO OPT OUT" Will be appended to the end of your messsage.</small>
                    <br>
                    <div id="divcount">139 charachter(s) to go..</div>
                </div>
                <div class="control-group {{ ($errors->has('cell_number[]') ? 'error' : '') }}">
                    <label for="cell_number" class="control-label"><strong>Cell Number</strong>&nbsp;&nbsp;<small>(e.g&nbsp;&nbsp;0729957007)</small></label>
                    <input type="text"  name="cell_number[]" class="numeric" />
                    <span class="help-block">{{ ($errors->has('cell_number') ? $errors->first('cell_number') : '') }}</span>
                    <br />
                    <br />
                    <button class="submit reg-btn" id="send_test">Send test SMS</button><br /><br />
                </div>
            </div>
            <div class="span4"></div>
        </div>
    </form>
    <div class="row" style="padding-left: 40px;">
        <div class="span4">
            <h4 class="dotted-border">Select you recipients</h4>
            <div class="control-group">
                {{ Form::select('smsRecipient',$smsRecipients,null,array('id'=>'smsRecipient')) }}
            </div>
        </div>

    </div>

</div><!--/span12-->

@stop

@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

        var totalRecipients = $(':checkbox:checked').size();
        $("#num_recipient").html('Selected &nbsp;<strong>'+totalRecipients+'</strong>&nbsp; recipients..');

        $("#warning_text").hide();
        $("#send_valid").hide();
        $(".numeric").mask('0b99999999');

        $(".check_recipient").click(function(){

            totalRecipients = $(':checkbox:checked').size();

            $("#num_recipient").html('Selected &nbsp;<strong>'+totalRecipients+'</strong>&nbsp; recipients..');
            $("#creditsSending").val(totalRecipients);
        });

        $("#smsRecipient").change(function(){

            var userType = $(this).val();
            var mall = $("#mallID").val();

            if( userType == 'all' ){
                window.location.href = '/manager/sendsms';
            }else{
                window.location.href = '/manager/sendsms/'+userType+'/'+mall;
            }
        });

        $(".checkall").click(function(){
            $('input:checkbox').prop('checked',true);

            var totalRecipients = $(':checkbox:checked').size();
            $("#num_recipient").html('Selected &nbsp;<strong>'+totalRecipients+'</strong>&nbsp; recipients..');
            $("#creditsSending").val(totalRecipients);


            $(".check_recipient").click(function(){
                totalRecipients = $(':checkbox:checked').size();
                $("#num_recipient").html('Selected &nbsp;<strong>'+totalRecipients+'</strong>&nbsp; recipients..');
                $("#creditsSending").val(totalRecipients);
            });

        });

        $("#send_sms").click(function(e){

            e.preventDefault();

            $.ajax({
                type: "POST",
                url: '/manager/validaterecipient',
                data: $("#postSendSMS").serialize(),
                success: function (data) {

                    $.each(data, function(key,value){

                        var newVal = value.replace(/\/.*$/, "");

                        $('.'+newVal).css({"backgroundColor":"#FF0066;"});
                        $("#warning_text").show();
                        $('html, body').scrollTop(500);

                        $("#remove_invalid").click(function(){

                            $("."+newVal+" input:checkbox").prop('checked',false);

                            totalRecipients = $(':checkbox:checked').size();
                            $("#num_recipient").html('Selected &nbsp;<strong>'+totalRecipients+'</strong>&nbsp; recipients..');
                            $("#creditsSending").val(totalRecipients);

                        });

                    });

                    $("#send_sms").hide();
                    $("#send_valid").show();
                }
            });
        });

        $(".uncheckall").click(function(){
            $('input:checkbox').prop('checked',false);
//            alert($('input:checkbox :checked').size());
        });

    });

    function limitTextCount(limitField_id, limitCount_id, limitNum)
    {
        var limitField = document.getElementById(limitField_id);
        var limitCount = document.getElementById(limitCount_id);
        var fieldLEN = limitField.value.length;

        if (fieldLEN > limitNum)
        {
            limitField.value = limitField.value.substring(0, limitNum);
        }
        else
        {
            limitCount.innerHTML = (limitNum - fieldLEN) + ' charachter(s) to go.';
        }
    }
</script>
@stop
