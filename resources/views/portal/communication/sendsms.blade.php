@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Compose SMS Message</h1>

                <p class="animated fadeInDown delay2">Manager your Mall, Tenant Information &amp; Communication</p>
            </div>
            <!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Compose SMS Message</li>
                </ul>
            </div>
            <!--/span6-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</div><!--/three-->
@include('portal/partials/submenu')
@stop


@section('content')


@stop

@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){
        $(".numeric").mask('0b99999999');

        $("#mallID").change(function(){

            var mall = $(this).val();

            window.location.href = '/manager/sendsms/'+mall;

        });


    });

    function limitTextCount(limitField_id, limitCount_id, limitNum)
    {
        var limitField = document.getElementById(limitField_id);
        var limitCount = document.getElementById(limitCount_id);
        var fieldLEN = limitField.value.length;

        if (fieldLEN > limitNum)
        {
            limitField.value = limitField.value.substring(0, limitNum);
        }
        else
        {
            limitCount.innerHTML = (limitNum - fieldLEN) + ' charachter(s) to go.';
        }
    }
</script>
@stop
