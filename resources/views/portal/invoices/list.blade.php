@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Promotions Listing</h1>
                    <p class="animated fadeInDown delay2">Manage your Mall, Tenant Information &amp; Communication</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')
@stop

@section('content')

    <div class="row-fluid">
        <div class="blog-content">

            <h4 class="hborder">Invoices</h4>
            <h5>Unpaid Invoices</h5>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Number</th>
                    <th>Ref</th>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Cost</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody id="event_table">
                @if(sizeof($invoices) > 0)
                    <?php
                    $count1 = 0;
                    ?>
                    @foreach($invoices as $invoice)
                        @if($invoice->accountPaid == 'N')
                            <?php
                            $count1 = $count1 + 1;
                            ?>
                            <tr>
                                <td>{{ $invoice->invoiceDoc }}</td>
                                <td>{{ $invoice->ref }}</td>
                                <td>{{ $invoice->invoiceDate }}</td>
                                <td>{{ $invoice->description }}</td>
                                <td>{{ $invoice->price }}</td>
                                <td colspan="3">
                                    <a href="{{ route('admin.getPDF',$invoice->bookingAccountID) }}"
                                       class="btn btn-info btn-mini"><i class="icon-white icon-eye-open"></i>View</a>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    @if($count1==0)
                        <tr>
                            <td colspan="6">There are no other invoices...</td>
                        </tr>
                    @endif
                @else
                    <tr>
                        <td colspan="5">There are no unpaid invoices, thanks!</td>
                    </tr>
                @endif
                </tbody>
            </table>

            <h5>Other Invoices</h5>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Number</th>
                    <th>Ref</th>
                    <th>Date</th>
                    <th>Description</th>
                    <th>Cost</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @if( sizeof($invoices)> 0 )
                    <?php
                    $count1 = 0;
                    ?>
                    @foreach($invoices as $other)
                        @if($invoice->accountPaid != 'N')
                            <?php
                            $count1 = $count1 + 1;
                            ?>
                            <tr>
                                <td>{{ $invoice->invoiceDoc }}</td>
                                <td>{{ $invoice->ref }}</td>
                                <td>{{ $invoice->invoiceDate }}</td>
                                <td>{{ $invoice->description }}</td>
                                <td>{{ $invoice->price }}</td>
                                <td colspan="3">
                                    <a href="{{ route('admin.getPDF',$invoice->bookingAccountID) }}"
                                       class="btn btn-info btn-mini"><i class="icon-white icon-eye-open"></i>View</a>
                                </td>
                            </tr>
                        @endif
                    @endforeach
                    @if($count1==0)
                        <tr>
                            <td colspan="6">There are no other invoices...</td>
                        </tr>
                    @endif
                @else
                    <tr>
                        <td colspan="6">There are no other invoices...</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
@section('exScript')
    <script src="{{ asset('js/bootbox.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".action_delete").click(function (e) {

                e.preventDefault();
                var promotion = $(this).attr("id");
//           alert($(this).attr("id"));
                bootbox.confirm("Are you sure you want to delete this entry ? ");

                $(".modal a.btn-primary").click(function () {
                    $.ajax({
                        type: "POST",
                        url: '/promotions/delete/' + promotion,
                        data: {
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function () {
                            location.reload();
                        }
                    });
                });
            });

            $(".btn_copy").click(function () {

                var promotion = $(this).attr("id");

                $.ajax({
                    type: "POST",
                    url: '/promotions/copy/' + promotion,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function (data) {
                        location.reload();
                    }
                });

            });
        });
    </script>
@stop

