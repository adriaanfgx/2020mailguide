@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Promotions Listing</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Promotions</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div><!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">

    <div class="span9 pull-left">
        <h4 class="hborder">Listed Promotions</h4>

    </div>
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>
        <div class="blog-content border_top">
            <dl>
                <dt>Filter by Mall</dt>
                {{ Form::select('mall',$mallList,null,array('id'=>'mall')) }}
                <br />
            </dl>
        </div>
    </div>  <!-- span6 -->

    <div class="clear"></div>

    <div class="row-fluid">
        <div class="span12">
            @if(sizeof($promotions) > 0)
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Shop</th>
                    <th>Promotion</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody id="promo_table">
                @foreach($promotions as $current)
                <tr>
                    <td>{{ $current->shop }}</td>
                    <td>{{ Str::limit($current->promotion, 50) }}</td>
                    <td>{{ $current->startDate }}</td>
                    <td>{{ $current->endDate }}</td>
                    <td colspan="3">
                        <a href="{{ route('promotions.update',$current->promotionsMGID) }}" class="btn btn-info btn-mini"><i class="fa fa-pencil"></i>Edit</a>&nbsp;|&nbsp;
                        <a href="#" id="{{ $current->promotionsMGID }}" class="btn btn-info btn-mini btn_copy"><i class="fa fa-eye">Copy</i></a>&nbsp;|&nbsp;
                        <a class="btn btn-danger btn-mini action_delete" href="#" id="{{ $current->promotionsMGID }}"><i class="fa fa-trash-o"></i>Delete</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            @else
            <span>There are no listed promotions .........</span>
            @endif
        </div><!--span12 -->
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $(".action_delete").click(function(e){

            e.preventDefault();
            var promotion = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: '/promotions/delete/'+promotion,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });

        });

        $(".btn_copy").click(function(){

            var promotion = $(this).attr("id");

            $.ajax({
                type: "POST",
                url: '/promotions/copy/'+promotion,
                data: {
                    '_token': "{{ csrf_token() }}"
                },
                success: function(){
                    location.reload();
                }
            });

        });

        $("#mall").change(function(){

            $("#promo_table").empty();
            var mall = $(this).val();
//            alert(mall);
            $.ajax({
                url: '/portal/mpromotions/'+mall,
                data: '',
                success:function(data){
//                    console.log( data.length);
                    $.each(data, function(j,val){
                        $("#promo_table").append('<tr><td>'+val.shop+'</td><td>'+val.promotion+'</td><td>'+val.startDate+'</td><td>'+val.endDate+'</td><td><a href="/shops/update/'+val.shopMGID+'" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp;|&nbsp;<a class="btn btn-danger btn-mini action_delete" href="#" id="'+val.shopMGID+'"><i class="fa fa-trash-o"></i> Delete</a></td></tr>');
                    });

                    if( data.length == 0 ){
                        $('#promo_table').append('<tr><td colspan="5">There are no promotions for selected mall...</td></tr>');
                    }
                }
            });


        })
    });
</script>
@stop

