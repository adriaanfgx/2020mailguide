@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Mall Member</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Add Mall User</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div><!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>
    </div>  <!-- span6 -->
    <div class="pull-left">
        <h4 class="dotted-border">Registration Form</h4>
    </div>
    <div class="clear"></div>
    <!--    <hr class="hborder" />-->

    <div class="row-fluid">
        <div class="span12">
            {{ Form::open(array('route' => 'portal.postAddmalluser', 'method' => 'post','name' => 'addMallUser','id' => 'addMallUser', 'class' => 'form-inline shopCreate')) }}
            <div>
                <div id="user_info">
                    <h4 class="dotted-border">Mall Information</h4>
                    <div class="row" style="padding-left: 30px;">
                        <div class="span4">
                            <div class="control-group">
                                <label for="mall" class="control-label"><strong>Mall</strong></label>
                                &nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                                <a href="#" id="deselect_all"><strong>Deselect All</strong></a>
                                <div class="controls">
                                    {{ Form::select('mallID[]',$usermalls,$malls,array('class'=>'span10 multi-mall','id'=>'mallID','multiple')) }}
                                    {{ Form::hidden('admin_level','Mall Manager') }}
                                </div>
                            </div>
                        </div>
                        <div class="span4"></div>
                    </div>
                    @include('partials/memberzone/adduserfields')
                </div>
            </div>
            <button class="submit reg-btn" id="update_user">Submit Details</button>
            {{ Form::close() }}
        </div><!--span12 -->
    </div>
</div>
@stop
@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });

        $("#mallID").select2({});

        $("#deselect_all").click(function(){

            $(".multi-mall").select2('val', '');
        });
    });
</script>
@stop

