@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Exhibition Listing</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Exhibitions</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div>
    <!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">

    <div class="span12">
        <h4 class="hborder">Listed Exhibition</h4>

    </div>
    <!-- span6 -->
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>
        <div class="blog-content border_top">
            <dl>
                <dt>Filter by Mall</dt>
                {{ Form::select('mall',$mallList,null,array('id'=>'mall')) }}
                <br>
            </dl>
        </div>
    </div>

    <div class="clear"></div>

    <div class="row-fluid">
        <div class="span12">
            <table class="table table-border">
                <thead>
                <tr>
                    <th>Exhibition Name</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Display</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody id="exbtn_table">
                @if(sizeof($exhibitions) > 0)
                @foreach($exhibitions as $exhibition)
                <tr>
                    <td>{{ $exhibition->name }}</td>
                    <td>{{ $exhibition->startDate }}</td>
                    <td>{{ $exhibition->endDate }}</td>
                    <td>{{ displayText($exhibition->display) }}</td>
                    <td>
                        <a href="{{ route('exhibitions.edit',$exhibition->exhibitionID) }}" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp;|&nbsp;<a class="btn btn-danger btn-mini action_delete" href="{{ route('exhibitions.delete',$exhibition->exhibitionID) }}" id="{{ $exhibition->exhibitionID }}"><i class="fa fa-trash-o"></i> Delete</a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5">There are currently no exhibitions.....</td>
                </tr>
                @endif
                </tbody>
            </table>

        </div>
        <!--span12 -->
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $(".action_delete").click(function (e) {

            e.preventDefault();
            var event = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function () {
                $.ajax({
                    type: "POST",
                    url: '/exhibitions/delete/' + event,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function () {
                        location.reload();
                    }
                });
            });
        });

        $("#mall").change(function(){
            var mallID = $(this).val();

            $.ajax({
                url: '/portal/filterexhibitions/'+mallID,
                data: '',
                success: function(data){

                    $("#exbtn_table").empty();
                    $.each(data, function(j,val){

                        $("#exbtn_table").append('<tr><td>'+val.name+'</td><td>'+val.startDate+'</td><td>'+val.endDate+'</td><td>'+val.display+'</td><td><a href="/exhibitions/edit/'+val.exhibitionID+'" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp;|&nbsp;<a class="btn btn-danger btn-mini action_delete" href="#" id="'+val.exhibitionID+'"><i class="fa fa-trash-o"></i> Delete</a></td></tr>');
                    });

                    if( data.length == 0 ){
                        $('#exbtn_table').append('<tr><td colspan="5">There is no data for selected mall...</td></tr>');
                    }

                    $(".action_delete").click(function (e) {

                        e.preventDefault();
                        var event = $(this).attr("id");

                        bootbox.confirm("Are you sure you want to delete this entry ? ");

                        $(".modal a.btn-primary").click(function () {
                            $.ajax({
                                type: "POST",
                                url: '/exhibitions/delete/' + event,
                                data: {
                                    '_token': "{{ csrf_token() }}"
                                },
                                success: function () {
                                    location.reload();
                                }
                            });
                        });
                    });
                }
            });

        });
    });
</script>
@stop

