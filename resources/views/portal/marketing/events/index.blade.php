@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Events Listing</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">List Events</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div>
    <!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">

    <div class="span12">
        <h4 class="hborder">Listed Events</h4>

    </div>
    <!-- span6 -->
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>
        <div class="blog-content border_top">
            <dl>
                <dt>Filter by Mall</dt>
                {{ Form::select('mall',$mallList,null,array('id'=>'mall')) }}
                <br>
            </dl>
        </div>
    </div>

    <div class="clear"></div>

    <div class="row-fluid">
        <div class="span12">
            @if(sizeof($events) > 0)
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Event Name</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Display</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody id="event_table">
                @foreach($events as $current)
                <tr>
                    <td>{{ $current->name }}</td>
                    <td>{{ $current->startDate }}</td>
                    <td>{{ $current->endDate }}</td>
                    <td>{{ displayText($current->display) }}</td>
                    <td>
                        <a href="{{ route('mallevents.edit',$current->eventsMGID) }}" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp;|&nbsp;<a class="btn btn-danger btn-mini action_delete" href="{{ route('events.delete',$current->eventsMGID) }}" id="{{ $current->eventsMGID }}"><i class="fa fa-trash-o"></i> Delete</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            @else
            <span>There are no current events .........</span>
            @endif

        </div>
        <!--span12 -->
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $(".action_delete").click(function (e) {

            e.preventDefault();
            var event = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function () {
                $.ajax({
                    type: "POST",
                    url: '/events/delete/' + event,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function () {
                        location.reload();
                    },
                    dataType: 'json'
                });
            });
        });

        $("#mall").change(function(){
//           alert($(this).val());
            var mall = $(this).val();

            if( $(this).val() == ''){
                mall = 'all';
            }
//
            $.ajax({
                url: '/portal/filterevents/'+mall,
                data: '',
                success: function(data){

                    $("#event_table").empty();
                    $.each(data, function(j,val){

                        $("#event_table").append('<tr><td>'+val.name+'</td><td>'+val.startDate+'</td><td>'+val.endDate+'</td><td>'+val.display+'</td><td><a href="events/edit/'+val.eventsMGID+'" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp;|&nbsp;<a class="btn btn-danger btn-mini action_delete" href="#" id="'+val.eventsMGID+'"><i class="fa fa-trash-o"></i> Delete</a></td></tr>');
                    });

                    if( data.length == 0 ){
                        $('#event_table').append('<tr><td colspan="5">There is no data for selected mall...</td></tr>');
                    }

                    $(".action_delete").click(function (e) {

                        e.preventDefault();
                        var event = $(this).attr("id");

                        bootbox.confirm("Are you sure you want to delete this entry ? ");

                        $(".modal a.btn-primary").click(function () {
                            $.ajax({
                                type: "POST",
                                url: '/events/delete/' + event,
                                data: {
                                    '_token': "{{ csrf_token() }}"
                                },
                                success: function () {
                                    location.reload();
                                },
                                dataType: 'json'
                            });
                        });
                    });

                }
            });

        });
    });
</script>
@stop

