@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Shops Listing</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Shops</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div><!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">

    <div class="span9 pull-left">
        <h4 class="hborder">Listed Shops</h4>

    </div>
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>
        <div class="blog-content border_top">
            <dl>
                <dt>Filter by Mall</dt>
                    {{ Form::select('mall',$malls,null,array('id'=>'mall')) }}
                <br>
            </dl>
        </div>
    </div>  <!-- span6 -->

    <div class="clear"></div>
    <hr class="hborder" />

    <div class="row-fluid">
        <div class="span12">
            <table class="table table-border">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Categoty</th>
                    <th>Sub Category</th>
                    <th>Shop Number</th>
                    <th>Telephone</th>
                    <th>Display</th>
                    <th colspan="2">Actions</th>
                </tr>
                </thead>
                <tbody id="shop_table">
                @foreach($shopList as $shop)
                <tr>
                    <td>{{ $shop->name }}</td>
                    <td>{{ $shop->category }}</td>
                    <td>{{ $shop->subcategory }}</td>
                    <td>{{ $shop->shopNumber }}</td>
                    <td>{{ $shop->telephone }}</td>
                    <td>{{ displayText($shop->display) }}</td>
                    <td colspan="2">
                        <a href="{{ route('shops.update',$shop->shopMGID) }}" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp;&nbsp;|
                        &nbsp;&nbsp;<a class="btn btn-danger btn-mini action_delete" href="{{ route('shops.delete',$shop->shopMGID) }}" id="{{ $shop->shopMGID }}"><i class="fa fa-trash-o"></i> Delete</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

        </div><!--span12 -->
    </div>
</div>
@stop
@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

        $("#mall").change(function(){

            $("#shop_table").empty();

            var mallID = $(this).val();
            $.ajax({
                url: '/portal/shops/filter/'+mallID,
                data:{},
                dataType: 'json',
                success: function(data){

                    $.each(data, function(j,val){
                        $("#shop_table").append('<tr><td>'+val.name+'</td><td>'+val.category+'</td><td>'+val.subcategory+'</td><td>'+val.shopNumber+'</td><td>'+val.telephone+'</td><td>'+val.display+'</td><td><a href="/shops/update/'+val.shopMGID+'" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp;|&nbsp;<a class="btn btn-danger btn-mini action_delete" href="#" id="'+val.shopMGID+'"><i class="fa fa-trash-o"></i> Delete</a></td></tr>');
                    });

                    if( data.length === 0 ){
                        $('#shop_table').append('<tr><td">There is no data for selected mall...</td></tr>');
                    } else {

                    }

                    $(".action_delete").click(function(e){
                        e.preventDefault();
                        var shop = $(this).attr("id");

                        bootbox.confirm("Are you sure you want to delete this entry ? ");

                        $(".modal a.btn-primary").click(function(){
                            $.ajax({
                                type: "POST",
                                url: '/shops/delete/'+shop,
                                data: {
                                    '_token': "{{ csrf_token() }}"
                                },
                                success: function(){
                                    location.reload();
                                }
                            });
                        });
                    });

                }

            });

        });

        $(".action_delete").click(function(e){
            e.preventDefault();
            var shop = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: '/shops/delete/'+shop,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });
        });
    });
</script>
@stop

