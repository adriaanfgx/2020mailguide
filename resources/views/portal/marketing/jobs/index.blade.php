@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Job Listing</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Job</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div><!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">

    <div class="span12 pull-left">
        <h4 class="hborder">Job Listings</h4>

    </div>
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>
        <div class="blog-content border_top">
            <dl>
                <dt>Filter by Mall</dt>
                {{ Form::select('mall',$mallList,null,array('id'=>'mall')) }}
                <br>
            </dl>
        </div>
    </div>
    <br />
    <br />
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>
    </div>  <!-- span6 -->
    <br />
    <br />
    <div class="row-fluid">
        <div class="span12">
            <table class="table table-border">
                <thead>
                <tr>
                    <th>Shop Name</th>
                    <th>Job Title</th>
                    <th>Closing Date</th>
                    <th>Show Contact Info</th>
                    <th>Display</th>
                    <th>Applicants</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody id="job_table">
                @if( sizeof($jobs)>0 )
                @foreach($jobs as $job)
                <tr>
                    <td>{{ $job->name }}</td>
                    <td>{{ $job->title }}</td>
                    <td>{{ $job->endDate }}</td>
                    <td>{{ displayText($job->showInfo) }}</td>
                    <td>{{ displayText($job->display) }}</td>
                    <td>
                        {{ $job->applicants }} &nbsp;
                        @if( ($job->applicants) > 0 )
                        <a href="{{ route('jobs.applicants',$job->jobID) }}" class="btn btn-info btn-mini" style="float:right;margin-top:1px;"><i class="fa fa-eye">View</i></a>
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('jobs.edit',$job->jobID) }}" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp; | &nbsp;
                        <a class="btn btn-danger btn-mini action_delete" href="{{ route('jobs.delete',$job->jobID) }}" id="{{ $job->jobID }}"><i class="fa fa-trash-o"></i> Delete</a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="7">There are no listed Jobs</td>
                </tr>
                @endif
                </tbody>
            </table>

        </div><!--span12 -->
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $(".action_delete").click(function(e){

            e.preventDefault();
            var job = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: '/jobs/delete/'+job,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });
        });

        $("#mall").change(function(){

            var mall = $(this).val();

            $.ajax({
                url: '/portal/filterjobs/'+mall,
                data: '',
                success: function(data){
//                    console.log(data);
                    $("#job_table").empty();
                    $.each(data, function(j,val){

                        var applicnts = '';
                        if( val.applicants > 0){
                            applicnts = '<a href="jobs/applicants/'+val.jobID+'" class="btn btn-info btn-mini" style="float:right;margin-top:1px;"><i class="fa fa-eye">View</i></a>';
                        }
                        $("#job_table").append('<tr><td>'+val.name+'</td><td>'+val.name+'</td><td>'+val.endDate+'</td><td>'+val.showInfo+'</td><td>'+val.display+'</td><td>'+val.applicants+''+applicnts+'</td><td><a href="/jobs/edit/'+val.jobID+'" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp;|&nbsp;<a class="btn btn-danger btn-mini action_delete" href="#" id="'+val.jobID+'"><i class="fa fa-trash-o"></i> Delete</a></td></tr>');
                    });

                    if( data.length == 0 ){
                        $('#job_table').append('<tr><td colspan="5">There is no data for selected mall...</td></tr>');
                    }

                    $(".action_delete").click(function (e) {

                        e.preventDefault();
                        var event = $(this).attr("id");

                        bootbox.confirm("Are you sure you want to delete this entry ? ");

                        $(".modal a.btn-primary").click(function () {
                            $.ajax({
                                type: "POST",
                                url: '/jobs/delete/' + event,
                                data: {
                                    '_token': "{{ csrf_token() }}"
                                },
                                success: function () {
                                    location.reload();
                                }
                            });
                        });
                    });
                }
            });

        });
    });
</script>
@stop

