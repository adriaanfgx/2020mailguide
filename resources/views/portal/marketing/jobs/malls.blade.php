@extends('layouts.pages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Mall Listing</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">List Malls</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div>
    <!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">

    <div class="span12">
        <h4 class="hborder">Listed Malls</h4>

    </div>
    <!-- span6 -->

    <div class="clear"></div>

    <div class="row-fluid">
        <div class="span12">
            @if(sizeof($mallList) > 0)
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Add a Job</th>
                </tr>
                </thead>
                <tbody id="event_table">
                @foreach($mallList as $mall)
                <tr>
                    <td>{{ $mall->name }}</td>
                    <td>
                        <a href="{{ route('jobs.create',$mall->mallID) }}" class="btn btn-info btn-mini" ><i class="fa fa-plus"></i> Add a Job</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>
            @endif
        </div>
        <!--span12 -->
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {

    });
</script>
@stop

