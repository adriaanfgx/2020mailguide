@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Mall Details </h1>
                    <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a><span class="divider">/</span></li>
                        <li><a href="/portal">Memberzone</a><span class="divider">/</span></li>
                        <li class="active">Update Mall Details</li>
                    </ul>
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')
@stop

@section('content')
    <div class="row-fluid">
        <div class="span12 middle-headings dotted-border white-bg">
            <h3>Update Mall Details</h3>
        </div><!--/span12-->

        {{ Form::model($mall, array('route' => array('malls.postUpdate', $mall->mallID),'method' => 'post','files'=>true ,'name' => 'mallUpdate', 'id' => 'mallUpdate', 'class' => 'form-inline')) }}
        <div>
            <div id="mall_info">
                <h4 class="dotted-border">Mall Details</h4>
                <div class="control-group {{ ($errors->has('name') ? 'error' : '') }}">
                    <label for="name">Mall Name</label>
                    <div class="controls">
                        {{ Form::text('name',null,array('class'=>'txtbar')) }}
                        {{ ($errors->has('name') ? $errors->first('name') : '') }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="mallDescription">Mall Description</label>
                    <div class="controls">
                        {{ Form::textarea('mallDescription',null,array('class'=>'txtbox','rows'=>'5')) }}
                    </div>
                </div>
            </div>
        </div>
        <span>To specify the province either select an existing one from the list below or enter a new one directly into the field to the right:</span>
        <div class="row" style="padding-left: 30px;">
            <div class="span6">
                <div class="control-group">
                    <label for="province">Province List</label>
                    <div class="controls">
                        {{ Form::select('province',$provinces,null) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="city_id">City List</label>
                    <div class="controls">
                        {{ Form::select('city_id',$cities,null) }}
                    </div>
                </div>
{{--                <div class="control-group">--}}
{{--                    <label for="classification">Classification List</label>--}}
{{--                    <div class="controls">--}}
{{--                        {{ Form::select('classification',$classifications,null) }}--}}
{{--                    </div>--}}
{{--                </div>--}}
                <div class="control-group">
                    <label for="physicalAddress">Physical Address</label>
                    <div class="controls">
                        {{ Form::text('physicalAddress',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="glaRetailM2">GLA Retail (m2)</label>
                    <div class="controls">
                        {{ Form::text('glaRetailM2',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="numRetailFloors">Number Of retail floors in center</label>
                    <div class="controls">
                        {{ Form::text('numRetailFloors',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="coveredParkingBays">Covered Parking bays</label>
                    <div class="controls">
                        {{ Form::text('coveredParkingBays',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="totalParkingBays">Total Parking bays</label>
                    <div class="controls">
                        {{ Form::text('totalParkingBays',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <label for="postalAddress">Postal Address</label>
                    <div class="controls">
                        {{ Form::text('postalAddress',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group {{ ($errors->has('website') ? 'error' : '') }}">
                    <label for="website">Website</label>
                    <div class="controls">
                        {{ Form::text('website',null,array('class'=>'txtbar')) }}
                        {{ ($errors->has('website') ? $errors->first('website') : '') }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="numShops">Number of shops</label>
                    <div class="controls">
                        {{ Form::text('numShops',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="openParkingBays">Open Parking bays</label>
                    <div class="controls">
                        {{ Form::text('openParkingBays',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="anchorTenants">Anchor Tenants</label>
                    <div class="controls">
                        {{ Form::text('anchorTenants',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <h4 class="dotted-border">Images</h4>
        <div class="row" style="padding-left: 30px;">
            <div class="span6">

                <div class="control-group">
                    <label for="logo">Mall Logo</label>
                    <div class="controls">
                        @if(!empty($mall->logo))
                            <a class="btn btn-danger btn-mini action_remove" href="#" id="logo"><i class="fa fa-trash-o"></i>Remove Image</a><br />
                            <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->logo }}" alt="Image" />
                        @else
                            <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('logo') }}</span>
                    </div>
                </div>
                <div class="control-group">
                    <label for="thumbnail2">Mall Image 2</label>
                    <div class="controls">
                        @if(!empty($mall->image2))
                            <a class="btn btn-danger btn-mini action_remove" href="#" id="image2"><i class="fa fa-trash-o"></i>Remove Image</a><br />
                            <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->thumbnail2 }}" alt="Image" />
                        @else
                            <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image2') }}</span>
                    </div>
                </div>
                <!--New add missing images-->
                <div class="control-group">
                    <label for="thumbnail4">Mall Image 4</label>
                    <div class="controls">
                        @if(!empty($mall->thumbnail4))
                            <a class="btn btn-danger btn-mini action_remove" href="#" id="image4"><i class="fa fa-trash-o"></i>Remove Image</a><br />
                            <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->thumbnail4 }}" alt="Image" />
                        @else
                            <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image4') }}</span>
                    </div>
                </div>
                <div class="control-group">
                    <label for="thumbnail6">Mall Image 6</label>
                    <div class="controls">
                        @if(!empty($mall->thumbnail6))
                            <a class="btn btn-danger btn-mini action_remove" href="#" id="image6"><i class="fa fa-trash-o"></i>Remove Image</a><br />
                            <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->thumbnail6 }}" alt="Image" />
                        @else
                            <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image6') }}</span>
                    </div>
                </div>
                <div class="control-group">
                    <label for="thumbnail8">Mall Image 8</label>
                    <div class="controls">
                        @if(!empty($mall->thumbnail8))
                            <a class="btn btn-danger btn-mini action_remove" href="#" id="image8"><i class="fa fa-trash-o"></i>Remove Image</a><br />
                            <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->thumbnail8 }}" alt="Image" />
                        @else
                            <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image8') }}</span>
                    </div>
                </div>
                <div class="control-group">
                    <label for="thumbnail10">Mall Image 10</label>
                    <div class="controls">
                        @if(!empty($mall->thumbnail10))
                            <a class="btn btn-danger btn-mini action_remove" href="#" id="image10"><i class="fa fa-trash-o"></i>Remove Image</a><br />
                            <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->thumbnail10 }}" alt="Image" />
                        @else
                            <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image10') }}</span>
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <label for="thumbnail1">Mall Image 1</label>
                    <div class="controls">
                        @if(!empty($mall->image1))
                            <a class="btn btn-danger btn-mini action_remove" href="#" id="image1"><i class="fa fa-trash-o"></i>Remove Image</a><br />
                            <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->thumbnail1 }}" alt="Image" />
                        @else
                            <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image1') }}</span>
                    </div>
                </div>
                <div class="control-group">
                    <label for="thumbnail3">Mall Image 3</label>
                    <div class="controls">
                        @if(!empty($mall->image3))
                            <a class="btn btn-danger btn-mini action_remove" href="#" id="image3"><i class="fa fa-trash-o"></i>Remove Image</a><br />
                            <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->thumbnail3 }}" alt="Image" />
                        @else
                            <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image3') }}</span>
                    </div>
                </div>
                <!--New add missing images-->
                <div class="control-group">
                    <label for="thumbnail5">Mall Image 5</label>
                    <div class="controls">
                        @if(!empty($mall->thumbnail5))
                            <a class="btn btn-danger btn-mini action_remove" href="#" id="image5"><i class="fa fa-trash-o"></i>Remove Image</a><br />
                            <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->thumbnail5 }}" alt="Image" />
                        @else
                            <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image5') }}</span>
                    </div>
                </div>
                <div class="control-group">
                    <label for="thumbnail7">Mall Image 7</label>
                    <div class="controls">
                        @if(!empty($mall->thumbnail7))
                            <a class="btn btn-danger btn-mini action_remove" href="#" id="image7"><i class="fa fa-trash-o"></i>Remove Image</a><br />
                            <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->thumbnail7 }}" alt="Image" />
                        @else
                            <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image7') }}</span>
                    </div>
                </div>
                <div class="control-group">
                    <label for="thumbnail9">Mall Image 9</label>
                    <div class="controls">
                        @if(!empty($mall->thumbnail9))
                            <a class="btn btn-danger btn-mini action_remove" href="#" id="image9"><i class="fa fa-trash-o"></i>Remove Image</a><br />
                            <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $mall->thumbnail9 }}" alt="Image" />
                        @else
                            <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image9') }}</span>
                    </div>
                </div>
            </div>
            <div class="span6">

            </div>
        </div>
        <h4 class="dotted-border">Special Features (eg. ice rink, cinemas etc.)</h4>
        <div class="row" style="padding-left: 30px">
            <div class="span6">
                <div class="control-group">
                    <label for="specialFeature1">Special Feature 1</label>
                    <div class="controls">
                        {{ Form::text('specialFeature1',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="specialFeature3">Special Feature 3</label>
                    <div class="controls">
                        {{ Form::text('specialFeature3',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="specialFeature5">Special Feature 5</label>
                    <div class="controls">
                        {{ Form::text('specialFeature5',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="specialFeature7">Special Feature 7</label>
                    <div class="controls">
                        {{ Form::text('specialFeature7',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="specialFeature9">Special Feature 9</label>
                    <div class="controls">
                        {{ Form::text('specialFeature9',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <label for="specialFeature2">Special Feature 2</label>
                    <div class="controls">
                        {{ Form::text('specialFeature2',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="specialFeature4">Special Feature 4</label>
                    <div class="controls">
                        {{ Form::text('specialFeature4',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="specialFeature6">Special Feature 6</label>
                    <div class="controls">
                        {{ Form::text('specialFeature6',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="specialFeature8">Special Feature 8</label>
                    <div class="controls">
                        {{ Form::text('specialFeature8',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="specialFeature10">Special Feature 10</label>
                    <div class="controls">
                        {{ Form::text('specialFeature10',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <h4 class="dotted-border">Centre Management</h4>
        <div class="row" style="padding-left: 30px;">
            <div class="span6">
                <div class="control-group">
                    <label for="centreManager">Centre Manager</label>
                    <div class="controls">
                        {{ Form::text('centreManager',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="centreManagerFax">Centre Manager Fax</label>
                    <div class="controls">
                        {{ Form::text('centreManagerFax',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group {{ ($errors->has('centreManagerEmail') ? 'error' : '') }}">
                    <label for="centreManagerEmail">Centre Manager Email</label>
                    <div class="controls">
                        {{ Form::text('centreManagerEmail',null,array('class'=>'txtbar')) }}
                        {{ ($errors->has('centreManagerEmail') ? $errors->first('centreManagerEmail') : '') }}
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <label for="centreManagerTelephone">Tel</label>
                    <div class="controls">
                        {{ Form::text('centreManagerTelephone',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="centreManagerCell">Cell</label>
                    <div class="controls">
                        {{ Form::text('centreManagerCell',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <h4 class="dotted-border">Markerting Consultant</h4>
        <div class="row" style="padding-left: 30px;">
            <div class="span6">
                <div class="control-group">
                    <label for="marketingConsultant">Marketing Company Name</label>
                    <div class="controls">
                        {{ Form::text('marketingConsultant',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="marketingCell">Cell</label>
                    <div class="controls">
                        {{ Form::text('marketingCell',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="marketingManager">Marketing Manager</label>
                    <div class="controls">
                        {{ Form::text('marketingManager',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="marketingAssistant">Marketing Assistant</label>
                    <div class="controls">
                        {{ Form::text('marketingAssistant',null,array('class'=>'txtbar')) }}
                    </div>
                </div>

                <div class="control-group">
                    <label for="marketingContractStart">Start Date</label>
                    <div class="controls">
                        <div class="input-append date dp3" data-date="12-02-2012" data-date-format="dd-mm-yyyy">
                            {{ Form::text('marketingContractStart',null,array('class'=>'span8','readonly')) }}
                            <span class="add-on"><i class="icon-th"></i></span>
                            {{ ($errors->has('marketingContractStart') ? $errors->first('marketingContractStart') : '') }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <label for="marketingTelephone">Tel</label>
                    <div class="controls">
                        {{ Form::text('marketingTelephone',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="marketingFax">Fax</label>
                    <div class="controls">
                        {{ Form::text('marketingFax',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group {{ ($errors->has('marketingManagerEmail') ? 'error' : '') }}">
                    <label for="marketingManagerEmail">Marketing Manager Email Address</label>
                    <div class="controls">
                        {{ Form::text('marketingManagerEmail',null,array('class'=>'txtbar')) }}
                        {{ ($errors->has('marketingManagerEmail') ? $errors->first('marketingManagerEmail') : '') }}
                    </div>
                </div>
                <div class="control-group {{ ($errors->has('marketingAssistantEmail') ? 'error' : '') }}">
                    <label for="marketingAssistantEmail">Marketing Assistant Email Address</label>
                    <div class="controls">
                        {{ Form::text('marketingAssistantEmail',null,array('class'=>'txtbar')) }}
                        {{ ($errors->has('marketingAssistantEmail') ? $errors->first('marketingAssistantEmail') : '') }}
                    </div>
                </div>
                <div class="control-group {{ ($errors->has('marketingContractEnd') ? 'error' : '') }}">
                    <label for="marketingContractEnd">End Date</label>
                    <div class="controls">
                        <div class="input-append date dp3" data-date="12-02-2012" data-date-format="dd-mm-yyyy">
                            {{ Form::text('marketingContractEnd',null,array('class'=>'span8','readonly')) }}
                            <span class="add-on"><i class="icon-th"></i></span>
                            {{ ($errors->has('marketingContractEnd') ? $errors->first('marketingContractEnd') : '') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <h4 class="dotted-border">Leasing Agent</h4>
        <div class="row" style="padding-left: 30px;">
            <div class="span6">
                <div class="control-group">
                    <label for="leasingAgent">Leasing agent</label>
                    <div class="controls">
                        {{ Form::text('leasingAgent',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="leasingTelephone">Tel</label>
                    <div class="controls">
                        {{ Form::text('leasingTelephone',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="leasingCell">Cell</label>
                    <div class="controls">
                        {{ Form::text('leasingCell',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <label for="leasingContactPerson">Leasing contact person</label>
                    <div class="controls">
                        {{ Form::text('leasingContactPerson',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group">
                    <label for="leasingFax">Fax</label>
                    <div class="controls">
                        {{ Form::text('leasingFax',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
                <div class="control-group {{ ($errors->has('leasingEmail') ? 'error' : '') }}">
                    <label for="leasingEmail">Email</label>
                    <div class="controls">
                        {{ Form::text('leasingEmail',null,array('class'=>'txtbar')) }}
                        {{ ($errors->has('leasingEmail') ? $errors->first('leasingEmail') : '') }}
                    </div>
                </div>
            </div>
        </div>
        <h4 class="dotted-border">Shopping Council Member Details</h4>
        <div class="row" style="padding-left: 30px;">
            <div class="span6">
                <div class="control-group">
                    <label for="shoppingCouncilMember">Are you a shopping council member?</label>
                    <div class="controls">
                        {{ Form::radio('shoppingCouncilMember','Y') }}Yes &nbsp;&nbsp;{{ Form::radio('shoppingCouncilMember','N') }}No
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="control-group">
                    <label for="shoppingCouncilNumber">If so, what is your shopping council member number?</label>
                    <div class="controls">
                        {{ Form::text('shoppingCouncilNumber',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <button class="submit reg-btn" id="add_review">Submit Details</button>
    {{ Form::close() }}
    <!--    </div>-->
    </div>
@stop

@section('exScript')
    <script>

        $(document).ready(function(){
            $('.dp3').datepicker({

            });
        });

        $(".action_remove").click(function(e){

            var mallID = <?php echo $mall->mallID; ?>;
            var image = $(this).attr('id');

            e.preventDefault();

            bootbox.confirm("Are you sure you want to remove image ? ");

            $(".modal a.btn-primary").click(function(){

                $.ajax({
                    type: "POST",
                    url: '/admin/malls/removeimage/'+mallID+'/'+image,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });
        });

    </script>
@stop
