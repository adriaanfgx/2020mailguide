@extends('layouts.pages')
@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            @include('partials.headers.search')
            <div class="row-fluid">
                <div class="span12">
                    <h1 class="animated fadeInDown delay1"><span>Jobs: </span>New and current</h1>
                    <p class="animated fadeInDown delay2">Easy access to jobs at Mallguide malls!</p>
                </div><!--/span6-->
                <div class="span12">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->

@stop

@section('content')

    <div class="row-fluid">

        <div class="hborder">

            <h4>Latest Jobs</h4>

            <div class="row-fluid">
                <div class="span3 offset5">
                    <?php $mallSelected = session()->get('selected_mall', ''); ?>
                    <div class="overlay-wrapper"></div>
                        {{ Form::select('mallID',$malls,null, ['placeholder'=>'Please select a province ...'], ['id'=>'mallID']) }}
                    <?php session()->forget('selected_mall'); ?>
                </div>
                <div class="span3">
                    <div class="input-append">
                        <input name="Search" type="text" id="search_string"/>
                        <button type="submit" id="search" class="btn btn-info" id="search"><i class="icon-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <table class="table table-bordered table-striped jobs-table">
            <thead>
            <tr>
                <th>Location</th>
                <th>Mall</th>
                <th>Shop</th>
                <th>Job</th>
                <th>Job Description</th>
                <th>Start Date</th>
                <th>Closing Date</th>
            </tr>
            </thead>
            <tbody id="job_data">
            @if(isset($jobs) && count($jobs))
                @foreach($jobs as $job)
                    <tr>
                        <td>
                            <a href="/malls/?city={{ $job->mall->city_id }}-{{ slugify($job->mall->mallCity->name) }}"
                               class="dark-link">{{ $job->mall->mallCity->name }}</a>, <a
                                    href="/malls/provinceID/{{ $job->mall->province_id }}"
                                    class="dark-link">{{ $job->mall->mallProvince->name }}</a>
                        </td>
                        <td>
                            <a href="/malls/view/{{ $job->mall->mallID }}/{{ slugify($job->mall->name) }}">{{ $job->mall->name }}</a>
                        </td>
                        <td>
                            <a href="/shops/view/{{ $job->shop->shopMGID }}/{{ slugify($job->shop->mall->name) }}/{{ slugify($job->shop->name) }}">{{$job->shop->name }}</a>
                        </td>
                        <td><a href="{{ route('job.view',$job->jobID) }}">{{$job->title }}</a></td>
                        <td>{{ Str::limit($job->description, 30) }}</td>
                        <td>{{ date('j F Y', strtotime($job->startDate)) }}</td>
                        <td>{{ date('j F Y', strtotime($job->endDate)) }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="6">Please select a Mall first...</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>
@stop

@section('exScript')
    <script>
        $(document).ready(function () {
            if ($("#mallID").val() != '') {
                populateTable();
            }
            $("#mallID").change(function () {
                populateTable();
            });

            $("#search").click(function () {
                if ($("#search_string").val() !== '') {
                    var string = $("#search_string").val();
                    var url = '/jobs/search/' + string;
                    $("#job_data").empty();
                    $.ajax(
                        {
                            url: url,
                            data: '',
                            success: function (data) {
                                $.each(data, function (index, elem) {
                                    $("#job_data").append($('<tr>').html("<td>" + elem.mall + "</td><td>" + elem.province + "</td><td></td><td>" + elem.title + "</td><td><a href='view/" + elem.jobID + "'>" + (elem.description).substring(0, 50) + "</a></td><td>" + elem.dateAdded + "</td>"));
                                });
                                $(".pagination").hide();

                                if (data.length == 0) {
                                    $('#job_data').html('<tr><td colspan="5">There is no data that matches your search...</td></tr>');
                                }

                            }
                        });
                }

            });

            function populateTable() {
                var $mall = $('#mallID').val();

                $('#job_data').load('/jobs/populateTable/' + $mall);

            }
        });
    </script>
@stop
