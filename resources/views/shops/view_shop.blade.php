@extends('layouts.pages')
@section('seo_meta')
@section('title')

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            <div class="row-fluid">
                @include('partials.headers.search')
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Shops: </span>Store listing <span>Information</span>
                    </h1>
                    <p class="animated fadeInDown delay2">Looking for store details, we have it here.</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
@stop

@section('content')
    <div class="inner-page">
        @include('partials.googleads')
        <div class="container-fluid"><!--row --><!--row -->
            <div class="row-fluid">
                <h4 class="hborder">{{ $shop->name }} / <a class="header-link"
                                                           href="/malls/view/{{ $mall->mallID }}/{{ slugify($mall->name) }}">{{ $mall->name }}</a>
                </h4>
            </div><!--row -->

            <div class="row-fluid">
                <div class="span12">

                    <div class="span3">
                        <div class="blog-content border_top">
                            <div class="mall-caption carousel">
                                <div class="zoom-gallery carousel-inner mall-slides">
                                    <!--
                                    Width/height ratio of thumbnail and the main image must match to avoid glitches.
                                    If ratios are different, you may add CSS3 opacity transition to the main image to make the change less noticable.
                                     -->
                                    <?php
                                    $_image = (!empty($shop->image1)) ? 'uploadimages/mall_' . $mall->mallID . '/' . $shop->image1 : 'img/noshop.png';
                                    ?>
                                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig1 }}"
                                       title="{{ $shop->name }}" class="item carousel-item active">
                                        <img src="{{ Config::get('app.url') }}/{{ $_image }}" alt="{{ $shop->name }}"/>
                                    </a>
                                    @if( !empty($shop->image2))
                                        <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig2 }}"
                                           title="{{ $shop->name }}" class="gallery-image item carousel-item">
                                            <img
                                                src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig2 }}"
                                                alt="{{ $shop->name }}" class="carousel-item"/>
                                        </a>
                                    @endif
                                    @if( !empty($shop->image3))
                                        <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig3 }}"
                                           title="{{ $shop->name }}" class="gallery-image item carousel-item">
                                            <img
                                                src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig3 }}"
                                                alt="{{ $shop->name }}" class="carousel-item"/>
                                        </a>
                                    @endif
                                    @if( !empty($shop->image4))
                                        <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig4 }}"
                                           title="{{ $shop->name }}" class="gallery-image item carousel-item">
                                            <img
                                                src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig4 }}"
                                                alt="{{ $shop->name }}" class="carousel-item"/>
                                        </a>
                                    @endif
                                    @if( !empty($shop->image5))
                                        <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig5 }}"
                                           title="{{ $shop->name }}" class="gallery-image item carousel-item">
                                            <img
                                                src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig5 }}"
                                                alt="{{ $shop->name }}" class="carousel-item"/>
                                        </a>
                                    @endif
                                    @if( !empty($shop->image6))
                                        <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig6 }}"
                                           title="{{ $shop->name }}" class="gallery-image item carousel-item">
                                            <img
                                                src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig6 }}"
                                                alt="{{ $shop->name }}" class="carousel-item"/>
                                        </a>
                                    @endif
                                    @if( !empty($shop->image7))
                                        <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig7 }}"
                                           title="{{ $shop->name }}" class="gallery-image item carousel-item">
                                            <img
                                                src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig7 }}"
                                                alt="{{ $shop->name }}" class="carousel-item"/>
                                        </a>
                                    @endif
                                    @if( !empty($shop->image8))
                                        <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig8 }}"
                                           title="{{ $shop->name }}" class="gallery-image item carousel-item">
                                            <img
                                                src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig8 }}"
                                                alt="{{ $shop->name }}" class="carousel-item"/>
                                        </a>
                                    @endif
                                    @if( !empty($shop->image9))
                                        <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig9 }}"
                                           title="{{ $shop->name }}" class="gallery-image item carousel-item">
                                            <img
                                                src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig9 }}"
                                                alt="{{ $shop->name }}" class="carousel-item"/>
                                        </a>
                                    @endif
                                    @if( !empty($shop->image10))
                                        <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig10 }}"
                                           title="{{ $shop->name }}" class="gallery-image item carousel-item">
                                            <img
                                                src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->imageBig10 }}"
                                                alt="{{ $shop->name }}" class="carousel-item"/>
                                        </a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <br/>
                        <?php
                        $website = removeURLProtocol($shop->url);
                        ?>
                        <div class="blog-content">
                            <dl>
                                @if($shop->telephone)
                                    <dt>Telephone</dt>
                                    <dd><a href="tel:{{ $shop->telephone }}">{{ $shop->telephone }}</a></dd>
                                @endif
                                @if($shop->email)
                                    <dd><a href="mailto:{{ $shop->email }}">Email Us</a></dd>
                                @endif
                                @if($website)
                                    <dd><a href="http://{{ $website }}" target="_blank">{{ $website }}</a></dd>
                                @endif
                            </dl>
                        </div>
                        <!--/my carousel-->
                    </div><!--span3-->
                    <div class="span3">
                        <div class="blog-content border_top">
                            <div class="mall-caption">
                                <h4 class="hborder">At {{ $shop->name }}</h4>
                                <ul>
                                    <li>
                                        @if($shop->curPromotions->count())<a
                                            href="{{ Route('promotions.mallIndex', $shop->mallID) }}">@endif
                                            {{ $shop->curPromotions->count(). ' promotion' }}@if($shop->promotions->count() > 1){{ 's' }}@endif
                                            .
                                            @if($shop->curPromotions->count())</a>@endif
                                    </li>
                                    <li>
                                        {{--								@if($shop->curJobs->count())<a href="{{ Route('promotions.mallIndex', $shop->mallID) }}">@endif--}}
                                        {{ $shop->curJobs->count() }} job vacancies.
                                        {{--								@if($shop->curJobs->count())</a>@endif--}}
                                    </li>
                                    <li>
                                        {{--@if($shop->giftIdeas->count())<a href="{{ Route('promotions.mallIndex', $shop->mallID) }}">@endif--}}
                                        {{ $shop->giftIdeas->count() }} Gift Ideas.
                                        {{--@if($shop->giftIdeas->count())</a>@endif--}}
                                    </li>
                                    <li>
                                        @if($shop->curCompetitions->count())<a
                                            href="{{ Route('competitions.mallIndex', $shop->mallID) }}">@endif
                                            {{ $shop->curCompetitions->count() }} Competitions
                                            @if($shop->curCompetitions->count())</a>@endif
                                    </li>
                                </ul>

                                @if($shop->cell != '' || $shop->fax != '' || $shop->contactPerson != '')
                                    Additional Information:
                                    <table class="table table-condensed table-hover">
                                        @if($shop->cell != '')
                                            <tr>
                                                <td><strong>Cell No.:</strong></td>
                                                <td>
                                                    <div class="text-right">{{ $shop->cell }}</div>
                                                </td>
                                            </tr>
                                        @endif
                                        @if($shop->fax != '')
                                            <tr>
                                                <td><strong>Fax:</strong></td>
                                                <td>
                                                    <div class="text-right">{{ $shop->fax }}</div>
                                                </td>
                                            </tr>
                                        @endif
                                        @if($shop->contactPerson != '')
                                            <tr>
                                                <td><strong>Contact Person:</strong></td>
                                                <td>
                                                    <div class="text-right">{{ $shop->contactPerson }}</div>
                                                </td>
                                            </tr>
                                        @endif
                                    </table>
                                @endif

                                <?php
                                $products = array();
                                $shopArray = $shop->toArray();
                                //dd($shopArray['product1']);
                                for ($x = 1; $x <= 10; $x++) {

                                    $num = 'product' . $x;
                                    if (!empty($shopArray[$num])) {
                                        $products[] = trim($shopArray[$num]);
                                    }

                                }
                                $prod_text = implode(', ', $products);
                                ?>
                                @if(count($products))
                                    <h6>Products</h6>
                                    <div class="productsDiv">
                                        {{ $prod_text }}
                                    </div>
                                @endif

                                <?php $brands = ($shop->brands->count()) ? $shop->brands->lists('name') : array(); ?>
                                @if(count($brands))
                                    <h6>Brands</h6>
                                    <div class="brandsDiv">
                                        {{ implode(', ', $brands) }}
                                    </div>
                                @endif

                                <?php
                                $shop_cards = array();
                                if ($shop->amex == 'Y') $shop_cards[] = 'amex';
                                if ($shop->diners == 'Y') $shop_cards[] = 'diners';
                                if ($shop->mastercard == 'Y') $shop_cards[] = 'mastercard';
                                if ($shop->visa == 'Y') $shop_cards[] = 'visa';
                                ?>
                                @if(count($shop_cards))
                                    <h6>Cards / Payment methods accepted</h6>
                                    <div class="cardsDiv text-center">
                                        <?php
                                        foreach ($shop_cards as $card) {
                                            echo '<img class="creditCard" src="/img/cards/' . $card . '.png" width="32" height="32" />';
                                        }
                                        ?>
                                    </div>
                                @endif

                                @if(!empty($shop->faceBookURL))
                                    <h6 class="hborder">Follow us on Social Media</h6>
                                    <div class="socialIcons">
                                        <a href="http://{{ removeURLProtocol($shop->faceBookURL) }}"
                                           target="_blank"><i class="fa fa-facebook-square fa-lg fa-fw"></i></a>
                                    </div>
                                @endif
                                @if(!empty($shop->blog_url))
                                    <h6 class="hborder">View our Blog articles</h6>
                                    <div class="socialIcons">
                                        <a href="http://{{ removeURLProtocol($blog_url) }}"
                                           target="_blank">{{ $blog_url }}</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div><!--span4-->
                    <div class="span3">
                        <div class="overlay-wrapper">

                            <div class="overlay"></div>
                        </div>
                        <div class="blog-content border_top">
                            <div class="mall-caption">
                                <h4 class="hborder">Store Information</h4>
                                <dl>
                                    @if($shop->description != '')
                                        <dt>Description</dt>
                                        <dd>{{ $shop->description }}</dd>
                                    @endif
                                    @if(isset($shop->category) && $shop->category != '')
                                        <dt>Category</dt>
                                        <dd>{{ $shop->category }}</dd>
                                    @endif
                                    @if(isset($shop->tradingHours) && $shop->tradingHours != '')
                                        <dt>Trading Hours</dt>
                                        <dd>{{ $shop->tradingHours }}</dd>
                                    @endif

                                </dl>
                            </div>
                        </div>
                    </div>

                    <div class="span3">
                        <div class='blog-content border_top'>
                            <div class="mall-caption">
                                @if($shop->mapSrc && $shop->mapSrc != '')
                                    <div id="mall_map" style="height:300px;">
                                        <div id="mall-image-map"
                                             style="height: 100%; background-image: url('{{ route('shop.mapImage',array($shop->shopID)) }}');">
                                            <div class="left-scroll-arrow">&nbsp;</div>
                                            <div class="right-scroll-arrow">&nbsp;</div>
                                            <div class="up-scroll-arrow">&nbsp;</div>
                                            <div class="down-scroll-arrow">&nbsp;</div>
                                        </div>
                                        {{--<img src="{{ route('shop.mapImage',array($shop->shopID)) }}" width="100%" height="100%">--}}
                                    </div>
                                @endif
                                <div class="clearfix"></div>
                                <div class="mall-info">
                                    <dl>
                                        @if(isset($shop->shopNumber) && $shop->shopNumber != '')
                                            <dt>
                                                <strong>Shop Number:</strong>
                                                {{ $shop->shopNumber }}
                                            </dt>
                                        @endif
                                        @if(isset($shop->landmark) && $shop->landmark != '')
                                            <dt>
                                                <strong>Landmark:</strong>
                                                {{ $shop->landmark }}
                                            </dt>
                                        @endif
                                        @if(isset($shop->level) && $shop->level != '')
                                            <dt>
                                                <strong>Level:</strong>
                                                {{ $shop->level }}
                                            </dt>
                                        @endif
                                    </dl>
                                    <div class="clearfix"></div>
                                    <div class="clearfix"></div>

                                    <div class="text-right">
                                        last
                                        updated: @if($shop->lastUpdate != '0000-00-00 00:00:00'){{ date('d/m/Y', strtotime($shop->lastUpdate)) }} @else
                                            never @endif
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="text-right">
                        <button class="btn btn-small btn-inverse go_back">&laquo; Go back</button>
                    </div>

                </div>
            </div><!--row fluid -->
        </div><!--/container-->
    </div>
@stop

@section('exScript')
    <script src="{{ asset('https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false') }}"></script>
    <script src="/js/jquery.draggable.background.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.zoom-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: true,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function (element) {
                        return element.find('img');
                    }
                }

            });

            if ($('#mall-image-map').length) {
                $('#mall-image-map').backgroundDraggable({
                    done: function () {
                        /*backgroundPosition = $('#mall-image-map').css('background-position');
                        console.log(backgroundPosition);*/
                    }
                });

                $('#mall-image-map').css('background-position', '<?php echo $shop->mapX;?>px <?php echo $shop->mapY;?>px ');

                /* var url = $('#mall-image-map').css('background-image').replace('url(', '').replace(')', '').replace("'", '').replace('"', '');
                 var bgImg = $('<img />');
                 bgImg.hide();
                 bgImg.bind('load', function()
                 {
 //                    var height = $(this).height();
                     var _width = bgImg[0].width;
                     var _height = bgImg[0].height;
                 });

                 var _pointer = $('<div></div>').html('<span id="mapPointer"><img src="{{ asset('img/dot_03.gif') }}"/></span>');
                $('#mall-image-map').append(_pointer);
                $('#mall-image-map').append(bgImg);
                bgImg.attr('src', url);

                $('#mapPointer').css('top', '<?php echo $shop->mapY;?>px').css('left', '<?php echo $shop->mapX;?>px');*/

//            console.log($('#mall-image-map').width());
//            console.log($('#mall-image-map').height());
            }

            $('.right-scroll-arrow').on('click', function (e) {
                scrollShopMap($(this), 'left');
            });

            $('.left-scroll-arrow').on('click', function (e) {
                scrollShopMap($(this), 'right');
            });

            $('.up-scroll-arrow').on('click', function (e) {
                scrollShopMap($(this), 'up');
            });

            $('.down-scroll-arrow').on('click', function (e) {
                scrollShopMap($(this), 'down');
            });

            $('.carousel').carousel({interval: 5000});

        });


        function scrollShopMap(el, _dir) {
            var _parent = el.parent();
            //  var _pid = _parent.attr('id');
            var _posXY = getMapPOS(_parent);
            var _newPOS, _newX, _newY;


            _posXY[0] = parseInt(_posXY[0]);
            _posXY[1] = parseInt(_posXY[1]);

            _newX = _posXY[0];
            _newY = _posXY[1];

            if (_dir == 'left') {
                _newX = _posXY[0] - 20;
                _parent.css('background-position', _newX + 'px ' + _newY + 'px');
                _newPOS = getMapPOS(_parent);
                if (parseInt(_newPOS[0]) < 1) {
                    $('.left-scroll-arrow').removeClass('left-scroll-arrow-disabled');
                } else {
                    $('.left-scroll-arrow').addClass('left-scroll-arrow-disabled');
                }
            } else if (_dir == 'right') {
                _newX = _posXY[0] + 20;
                _parent.css('background-position', _newX + 'px ' + _newY + 'px');

                _newPOS = getMapPOS(_parent);
                if (parseInt(_newPOS[0]) > 1) {
                    $('.left-scroll-arrow').addClass('left-scroll-arrow-disabled');
                } else {
                    $('.left-scroll-arrow').removeClass('left-scroll-arrow-disabled');
                }
            } else if (_dir == 'down') {
                _newY = _posXY[1] - 20;
                _parent.css('background-position', _newX + 'px ' + _newY + 'px');
                _newPOS = getMapPOS(_parent);
                if (parseInt(_newPOS[1]) < 1) {
                    $('.up-scroll-arrow').removeClass('up-scroll-arrow-disabled');
                } else {
                    $('.up-scroll-arrow').addClass('up-scroll-arrow-disabled');
                }
            } else if (_dir == 'up') {
                _newY = _posXY[1] + 20;
                _parent.css('background-position', _newX + 'px ' + _newY + 'px');

                _newPOS = getMapPOS(_parent);
                if (parseInt(_newPOS[1]) > 1) {
                    $('.down-scroll-arrow').addClass('down-scroll-arrow-disabled');
                } else {
                    $('.down-scroll-arrow').removeClass('down-scroll-arrow-disabled');
                }
            }
        }

        function getMapPOS(el) {
            var _posXY = el.css('backgroundPosition').split(' ');
            _posXY[0] = parseInt(_posXY[0]);
            _posXY[1] = parseInt(_posXY[1]);

            return _posXY;
        }
    </script>
@stop
