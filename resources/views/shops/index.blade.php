@extends('layouts.pages')
@section('seo_meta')
@section('title')

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            @include('partials.headers.search')
            <div class="row-fluid">
                <div class="span9">
                    <h1 class="animated fadeInDown delay1"><span>Shops: </span>Shopping<span> Delight</span> </h1>
                    <p class="animated fadeInDown delay2">Find a listed store inside any of our malls</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
@stop

@section('content')


    <div class="row-fluid">
        <div class="hborder">
            <h4 class="paddingBottom">Shops</h4>
            <div class="row-fluid">
                @if(count($topMallByProvinces))
                    <div id="view_selection" class="btn-group span2" data-toggle="buttons-radio">
                        <button type="button" class="btn btn-inverse @if(!isset($province_id) || (int) !$province_id) active @endif btn-view" data-rel="popular">Most Popular</button>
                        <button type="button" class="btn btn-inverse @if(isset($province_id) && (int) $province_id > 0) active @endif btn-view" data-rel="all">All Shops</button>
                    </div>
                @endif
                <div class="span9 shop-filter" style="@if($topMallByProvinces)display: none;@endif">
                    <div class="@if(count($topMallByProvinces)) span3 @else span4 @endif offset1">
                        <div class="overlay-wrapper"></div>
                        <select name="provinces" id="provinces" class="fluid-width">
                            <option value="">Please Select a Province...</option>
                            @foreach($provinces as $key=>$value)
                                <option value="{{ $key }}"<?php echo ($key == $province_id) ? 'selected' : '';?>>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="span3">
                        <?php
                        $mallSelected = Session::get('selected_mall','');
                        if($mall_id) $mallSelected = $mall_id;
                        ?>
                        <div class="overlay-wrapper"></div>
                        {{ Form::select('mall_id',$malls,$mallSelected,['placeholder' => 'Please select...'], ['id'=>'mall_id','disabled']) }}
                        <?php Session::forget('selected_mall'); ?>
                    </div>
                    <div class="span3">
                        <div class="overlay-wrapper"></div>
                        <div class="input-append">
                            <input type="text"   name="Search" value="" id="search_string" placeholder="Keyword Search" />
                            <button type="submit" id="search" class="btn btn-info" id="shopsearch"><i class="icon-search"></i></button>
                        </div>
                    </div>
                </div>
                @include('partials.alphasorter', array('province_id' => $province_id, 'mall_id' => $mall_id, 'show' => false))
            </div>
        </div>

        <div class="clear"></div>

        <div class="row-fluid">
            <div class="span12">
                @if(isset($shop_category) && count($shop_category))
                    <div class="span12 alert alert-info">
                        Browsing shops in the {{ $shop_category->name }} category.
                    </div>
                @endif
                <table class="table table-border table-striped responsive shop-table"@if(count($topMallByProvinces)) style="display: none;"@endif>
                    <thead>
                    <tr id="shop_header">
                        <th>&nbsp;</th>
                        <th>
                            <span class="hidden-phone">Name</span>
                            <span class="hidden-desktop">Shop</span>
                        </th>
                        <th class="hidden-phone">Category</th>
                        <th class="hidden-phone">Shop Number</th>
                        <th class="hidden-phone">&nbsp;</th>
                        <th class="hidden-phone">Telephone</th>
                    </tr>
                    </thead>
                    <tbody id="shop_table">
                    <tr class="text-center">
                        <td colspan="6">Please select a Province first!</td>
                    </tr>
                    </tbody>
                </table>
                @if(count($topMallByProvinces))
                    <div id="top_shops">
                        <?php $count = 0; ?>
                        <h4>Most Popular Shops by Malls</h4>
                        <div class="accordion" id="top_shop_list">
                            @foreach($topMallByProvinces as $idx=>$province)
                                <?php $count++; ?>
                                @if(count($province['malls']))
                                    <div class="accordion-group">
                                        <div class="accordion-heading heading-info">
                                            <h3>
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#top_shop_list" href="#collapse{{ $idx }}">{{ $province['name'] }}</a>
                                            </h3>
                                        </div>
                                        <div id="collapse{{ $idx }}" class="accordion-body @if($count == 1)in @endif collapse">
                                            <div class="accordion-inner">
                                                @foreach($province['malls'] as $mall)
                                                    @if(count($mall['shops']))
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h6><a class="header-link" href="/malls/view/{{ $mall->id }}/{{ slugify($mall->name) }}">{{ $mall->name }}</a></h6>
                                                            </div>
                                                            <div class="panel-body">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                    <tr>
                                                                        <th>&nbsp;</th>
                                                                        <th>
                                                                            <span class="hidden-phone">Name</span>
                                                                            <span class="hidden-desktop">Shop</span>
                                                                        </th>
                                                                        <th class="hidden-phone">Category</th>
                                                                        <th class="hidden-phone">Shop Number</th>
                                                                        <th class="hidden-phone">&nbsp;</th>
                                                                        <th class="hidden-phone">Telephone</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    @foreach($mall['shops'] as $shop)
                                                                        <tr>
                                                                            <td>
                                                                                @if(!empty($shop->logo))
                                                                                    <?php $logo = 'uploadimages/mall_' . $shop->mall->mallID . '/' . $shop->logo ?>
                                                                                @else
                                                                                    <?php $logo = 'img/noshopimg.gif'; ?>
                                                                                @endif
                                                                                <img class="img-responsive" width="100" src="{{ Config::get('app.url') }}/{{ $logo }}" alt="{{ $shop->name }}" />
                                                                            </td>
                                                                            <td>
                                                                                <a href='{{ route('shops.view_shop',array($shop->shopID,slugify($shop->mall->name), slugify($shop->name) )) }}'>{{ $shop->name }}</a><br>
                                                                                <span class="hidden-desktop">{{ $shop->category }}</span>
                                                                                <span class="hidden-desktop">@if($shop->shopNumber) &nbsp;/&nbsp;{{ $shop->shopNumber }} @endif</span>
                                                                            </td>
                                                                            {{--<td class="hidden-phone">{{ $shop->mall->name }}</td>--}}
                                                                            <td class="hidden-phone">{{ $shop->category }}</td>
                                                                            <td class="hidden-phone">@if($shop->shopNumber) Shop {{ $shop->shopNumber }} @endif</td>
                                                                            <td class="hidden-phone">
                                                                                @if($shop->mapX && $shop->mapY)
                                                                                    <a href="{{ route('shop.mapImage',array($shop->shopID)) }}" data-x-pos="{{ $shop->mapX }}" data-y-pos="{{ $shop->mapY }}" data-fancybox-title="{{ $shop->name }} @ {{ $shop->mall->name }}" class="shop-map">
                                                                                        <i class="fa fa-map-marker fa-lg fa-fw"></i>
                                                                                    </a>
                                                                                @endif
                                                                            </td>
                                                                            <td>
                                                                                {{--<div class="text-center hidden-desktop">--}}
                                                                                @if($shop->mapX && $shop->mapY)
                                                                                    <a href="{{ route('shop.mapImage',array($shop->shopID)) }}" data-x-pos="{{ $shop->mapX }}" data-y-pos="{{ $shop->mapY }}"  class="shop-map hidden-desktop">
                                                                                        <i class="fa fa-map-marker fa-lg fa-fw"></i>
                                                                                    </a>
                                                                                    <div class="clearfix"></div>
                                                                                @endif
                                                                                {{--</div>--}}
                                                                                @if($shop->telephone)
                                                                                    <a href="tel:{{ $shop->telephone }}" class="hidden-phone">{{ $shop->telephone }}</a>
                                                                                    <a class="btn btn-inverse btn-small hidden-desktop" href="tel:{{ $shop->telephone }}">Call</a>
                                                                                @endif
                                                                            </td>
                                                                        </tr>
                                                                    @endforeach
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div><!--span12 -->
                @endif

            </div><!--span12 -->
        </div>
    </div>
@stop
@section('exScript')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.btn-view').on('click', function (){
                var _rel = $(this).attr('data-rel');

                $('#top_shops').hide();
                $('.shop-table').hide();
                $('.shop-filter').hide();


                if(_rel == 'popular'){
                    $('.shop-filter').hide('slow');
                    $('.alpha-sorter').hide('slow');
                    $('#top_shops').show('slow');
                } else if(_rel == 'all'){
                    $('.shop-table').show('slow');
                    $('.shop-filter').show('slow');
                }

            });
            //handle alpha sorter
            $('.alpha-sorter a').on('click', function (e){
                e.preventDefault();

                var _trg = $(this).attr('href');
                var _pid = '<?php echo $province_id;?>';
                var _mid = '<?php echo $mall_id;?>';

                if(_pid == 0 && $('#provinces').val()){
                    _pid = $('#provinces').val();
                }

                if(_mid == 0 && $('#mall_id').val()){
                    _mid = $('#mall_id').val();
                }

                if(_pid && _mid){
                    window.location = '/shops/'+_pid+'/'+_mid+'/'+_trg;
                }

                //console.log($(this).attr('href'));
            });

            $('#mall_id').attr('disabled','disabled');

            if(!$("#provinces").val() || !$("#mall_id").val()){
                $('.alpha-sorter').hide();
            }

            if( $("#provinces").val() != '' ){

                $('#mall_id').attr('disabled',false);

                //Get shops for the pre-selected mall
                getSelectedMallStores();
            }else{
                $('#mall_id').attr('disabled','disabled');
                $('#mall_id').val('Select a province ...');
            }


            $("#mall_id").change(function(){

                $("#shop_table").empty().html('<tr><td>Loading Shops...</td></tr>');

                if( $(this).val() == ''){
                    $("#shop_table").empty().html('<tr><td>Please select a Mall...</td></tr>');
                }else {
                    $('.alpha-sorter').show();
                    //Get shops for the selected mall
                    getSelectedMallStores();
                }

            });

            $('#provinces').change(function()
            {
                $("#shop_table").empty().html('<tr><td colspan="4">Loading Malls...</td></tr>');

                var $province = $('#provinces').val();
                if($province == '')
                {
                    $("#shop_table").empty().html('<tr><td>Please select a province first...</td></tr>');
                    $('#mall_id').empty().append('<option value="">Malls</option>');
                    $('#mall_id').attr('disabled','disabled');
                } else {
                    $.ajax({
                        type: 'GET',
                        dataType: 'json',
                        url: '/malls/shopfilter/' + $province,
                        success: function (data) {

                            $('#mall_id').removeAttr('disabled');
                            $('#mall_id').empty().append('<option value="">Please select a Mall...</option>');
//

                            $.each(data, function(i, val)
                            {
                                $('#mall_id').append('<option value="'+val.mallID+'">'+ val.name+'</option>');
                            })


                            $('#shop_table').empty().html('<tr><td colspan="4">Malls loaded. Please select a Mall.</td></tr>');
//
                        }
                    });
                }
            });

            $("#search").click(function(){

                if( $("#search_string").val() === '' ){

                }else{

                    var string = $("#search_string").val();
                    var url = '/shops/search/'+string;
                    $("#shop_table").empty();
                    $.ajax({
                        url: url,
                        dataType: 'json',
                        data: '',
                        success: function(data){
                            $("#shop_header").prepend("<th>Mall</th>");
                            $.each(data, function(j,e){

                                var str = e.name;
                                var strMall = e.mall;

                                $("#shop_table").append('<tr><td>'+ e.mall+'</td><td><a href="/shops/view/'+ e.shopMGID +'/'+slugify(strMall)+'/'+slugify(str)+'">'+ e.name+'</a></td><td>'+e.shopNumber+'</td><td>'+e.telephone+'</td><td>'+e.category+'</td></tr>');
                            });
                            $(".pagination").hide();

                            if( data.length == 0 ){
                                $('#shop_table').html('<tr><td colspan="5">There is no data that matches your search...</td></tr>');
                            }

                        }
                    });
                }

            });

            $("a.shop-map").fancybox({
                type:'image',
                afterLoad: function(current, previous){
                    var _mapHolder = $('.fancybox-inner');
                    // shop location marker
                    var _callerEl = $(this.element);
                    var _pointer = $('<div></div>').html('<span id="mapPointer"><img src="{{ asset('img/dot_03.gif') }}"/></span>');

                    _mapHolder.html(_mapHolder.html()+_pointer.html());
                    $('#mapPointer').attr('style', 'position: absolute; left: '+_callerEl.attr('data-x-pos')+'px; top: '+_callerEl.attr('data-y-pos')+'px');
                }
            });

            <?php if(isset($province_id) && (int) $province_id > 0){?>
            $('#top_shops').hide('slow');
            $('.alpha-sorter').show('slow');
            $('.shop-table').show('slow');
            $('.shop-filter').show('slow');
            <?php } ?>
        });

        function getSelectedMallStores(){

            var mallID = $("#mall_id").val();
            var _url = '/shops/mallshops/'+mallID+queryStrings();
            @if($category_id > 0)
                _url += '/'+{{ $category_id }}
            @endif

            $.ajax({
                url: _url,
                data:{},
                dataType: 'json',
                success: function(data){
                    $("#shop_table").empty();

                    var _html = '';
                    $.each(data, function(j,val){
                        var str = val.name;
                        var mallstr = val.mall;

                        var logo_img = 'img/noshopimg.gif';
                        if(val.logo != '' && val.logo){
                            logo_img = 'uploadimages/mall_'+val.mallID+'/'+val.logo;
                        }
                        if(val.category==null)
                        {
                            val.category="";
                        }
                        _html += '<tr>';
                        _html += '<td><img class="img-responsive" width="100" src="{{ Config::get('app.url') }}/'+logo_img+'" alt="'+val.name+'" /></td>';
                        _html += '<td><a href="/shops/view/'+ val.shopMGID +'/'+slugify(mallstr)+'/'+slugify(str)+'">'+ val.name+'</a><br><span class="hidden-desktop">'+val.category+'</span><span class="hidden-desktop">&nbsp;/&nbsp;'+val.shopNumber+'</span></td>';
                        _html += '<td class="hidden-phone"><a href="/shops/{{ $province_id }}/{{ $mall_id }}/'+val.categoryID+'">'+val.category+'</a></td>';
                        _html += '<td class="hidden-phone">'+val.shopNumber+'</td>';
                        _html += '<td class="hidden-phone">';
                        if(val.mapX && val.mapY){
                            _html += '<a href="/shops/map/'+val.shopMGID+'"  class="shop-map hidden-phone"><i class="fa fa-map-marker fa-lg fa-fw"></i></a><div class="clearfix"></div>';
                        }
                        _html += '</td>';
                        _html += '<td><a href="tel://'+val.telephone+'" class="hidden-phone">'+val.telephone+'</a>';
                        if(val.mapX && val.mapY){
                            _html += '<a href="/shops/map/'+val.shopMGID+'"  class="shop-map hidden-desktop"><i class="fa fa-map-marker fa-lg fa-fw"></i></a><div class="clearfix"></div>';
                            _html += '<a class="btn btn-inverse btn-small hidden-desktop" href="tel:'+val.telephone+'">Call</a>';
                        }
                        _html += '</td>';
                        _html += '</tr>';

                    });

                    if(!_html || _html == ''){
                        $('#shop_table').html('<tr><td colspan="7" class="text-center">There are no shops for the selected mall.</td></tr>');
                    } else {
                        $("#shop_table").append(_html);
                    }

                }

            });

        }
    </script>
@stop

