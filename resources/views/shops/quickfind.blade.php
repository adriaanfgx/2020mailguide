@extends('layouts.pages')

@section('title')
    @parent
    Shop Search Results
@stop


@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                @include('partials.headers.search')
                <div class="span6">
                    <h1 class="animated fadeInDown delay1">Shop Search <span>Results</span></h1>

                    <p class="animated fadeInDown delay2">Search for Malls, Shops or Movies</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->
@stop

@section('content')
    <div class="row-fluid hidden-phone" id="filterSection_menu">
        <div class="span12"><!--sort wrap -->
        </div>
        <!--span12 -->
    </div><!--row -->

    <div data-perrow="4" class="row-fluid">


        <div class="row-fluid">
            <div class="span12">
                <?php
                $total_results = $shops->total();

                ?>
                @if( $total_results > 0 )
                    <h4 class="hborder">Shop Matches</h4>
                    <div class="alert alert-info">Showing results.</div>
                    <table class="table table-border">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th>
                                <span class="hidden-phone">Name</span>
                                <span class="hidden-desktop">Shop</span>
                            </th>
                            <th>Mall</th>
                            <th class="hidden-phone">Category</th>
                            <th class="hidden-phone">&nbsp;</th>
                            <th class="hidden-phone">&nbsp;</th>
                            <th><span class="hidden-phone">Contact Number</span></th>
                        </tr>
                        </thead>
                        <tbody id="malls">
                        @foreach( $shops as $shop )
                            @if($shop->mall)
                                <tr>
                                    <td>
                                        @if(!empty($shop->logo))
                                            <?php $logo = 'uploadimages/mall_' . $shop->mall->mallID . '/' . $shop->logo ?>
                                        @else
                                            <?php $logo = 'img/nomalllogo.gif'; ?>
                                        @endif
                                        <img class="img-responsive" width="100" src="{{ Config::get('app.url') }}/{{ $logo }}" alt="{{ $shop->name }}" />
                                    </td>
                                    <td>
                                        <a href='{{ route('shops.view_shop',array($shop->shopID,slugify($shop->mall->name),slugify($shop->name) )) }}'>{{ $shop->name }}</a><br>
                                        <span class="hidden-desktop">{{ $shop->category }}</span>
                                        <span class="hidden-desktop">@if($shop->shopNumber) &nbsp;/&nbsp;{{ $shop->shopNumber }} @endif</span>
                                    </td>
                                    <td class="hidden-phone">{{ $shop->mall->name }}</td>
                                    <td class="hidden-phone">{{ $shop->category }}</td>
                                    <td class="hidden-phone">@if($shop->shopNumber) Shop {{ $shop->shopNumber }} @endif</td>
                                    <td class="hidden-phone">
                                        @if($shop->mapX && $shop->mapY)
                                            <a href="{{ route('shop.mapImage',array($shop->shopID)) }}" data-x-pos="{{ $shop->mapX }}" data-y-pos="{{ $shop->mapY }}" data-fancybox-title="{{ $shop->name }} @ {{ $shop->mall->name }}" class="shop-map">
                                                <i class="fa fa-map-marker fa-lg fa-fw"></i>
                                            </a>
                                        @endif
                                    </td>
                                    <td>
                                        {{--<div class="text-center hidden-desktop">--}}
                                        @if($shop->mapX && $shop->mapY)
                                            <a href="{{ route('shop.mapImage',array($shop->shopID)) }}" data-x-pos="{{ $shop->mapX }}" data-y-pos="{{ $shop->mapY }}"  class="shop-map hidden-desktop">
                                                <i class="fa fa-map-marker fa-lg fa-fw"></i>
                                            </a>
                                            <div class="clearfix"></div>
                                        @endif
                                        {{--</div>--}}
                                        @if($shop->telephone)
                                            <a href="tel:{{ $shop->telephone }}" class="hidden-phone">{{ $shop->telephone }}</a>
                                            <a class="btn btn-inverse btn-small hidden-desktop" href="tel:{{ $shop->telephone }}">Call</a>
                                        @endif
                                    </td>
                                    @endif
                                </tr>
                                @endforeach
                        </tbody>
                    </table>
                    {{ $shops->links() }}
                @else
                    <div class="alert alert-info">Sorry, there are no matches for your search...</div>
                @endif
            </div>
            <!--span12 -->
        </div>
    </div>
@stop
@section('exScript')
    <script src="{{ asset('https://maps.googleapis.com/maps/api/js?libraries=places&sensor=false') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("a.shop-map").fancybox({
                type:'image',
                afterLoad: function(current, previous){
                    var _mapHolder = $('.fancybox-inner');
                    // shop location marker
                    var _callerEl = $(this.element);
                    var _pointer = $('<div></div>').html('<span id="mapPointer"><img src="{{ asset('img/dot_03.gif') }}"/></span>');

                    _mapHolder.html(_mapHolder.html()+_pointer.html());
                    $('#mapPointer').attr('style', 'position: absolute; left: '+_callerEl.attr('data-x-pos')+'px; top: '+_callerEl.attr('data-y-pos')+'px');
                }
            });
        });

    </script>
@stop

