@extends('layouts.pages')
@section('title')
    @parent
    Movies at {{ $mall['name'] }}
@stop

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            @include('partials.headers.search')
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Movies: </span>What's <span>showing?</span></h1>

                    <p class="animated fadeInDown delay2">Enjoy the best the silver screen has to offer</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->

@stop

@section('content')
    <?php

    //$movieData = Session::get('mallMovieData');
    //$movieMalls = Session::get('cinemas');
    //$dropdownMovies = Session::get('moviesDropdown');
    //$serializedMovies = unserialize(serialize($dropdownMovies));
    //$moviesDropdown = json_decode($serializedMovies, true);
    //$moviereviews = Session::get('moviereviews');

    //print_r($movieData);exit();
    ?>

    <div class="row-fluid">
        <div class="span12 blog-content">


            @if(count($show_dates))
                <div class="span4 pull-right rightPadding">
                    <form name="change_show_date" action="" method="post">
                        <h4 class="pull-left rightPadding margin-top-five">Showing on: </h4>
                        <select name="show_date" id="slt_show_date" class="span8">
                            @foreach($show_dates as $dt_idx=>$show_date)
                                <option value="{{ $dt_idx }}" @if($dt_idx == $selected_date) selected="selected" @endif>
                                    {{ $show_date }}
                                </option>
                            @endforeach
                        </select>
                    </form>
                </div>
            @endif
            <h4 class="hborder">Movies showing at <a
                    href="/malls/view/{{ $mallID }}/{{ slugify($mall['name']) }}"
                    class="dark-link">{{ $mall['name'] }}</a></h4>

            <div class="span11">

                <?php $count = 1;
                if(count($mallMovies)){


                ?>
                <div class="holder">
                    <ul class="thumbnails">

                        @foreach( $mallMovies as $data )
                            <?php
                            //$showing = $data->showing;
                            //
                            //$showing = $showing->filter(function($showing) use($mallID)
                            //{
                            //	return $showing->mallID == $mallID;;
                            //});
                            //var_dump($showing);
                            ?>

                            @if($count>2)

                    </ul>

                    <ul class="thumbnails">
                        <?php $count = 1; ?>
                        @endif

                        <li class="span2 filterable web">
                            <?php
                            $_image = (!empty($data->bigImage)) ? $data->bigImage : "nomovie.png";
                            ?>
                            <div class="overlay-wrapper">
                                <a href="{{ route('movies.moreinfo',array($data->movieID,str_replace(' ','-',$data->name))) }}">
                                    <img src="http://www.finemovies.co.za/uploadimages/{{ $_image  }}" alt="Image"
                                         class="overlay-image" data-overlaytext="Movie Info"/>
                                </a>

                                <div class="overlay"></div>
                            </div>
                        </li>
                        <li class="span4 filterable web">
                            <h4 class="hborder">{{ $data->name }}</h4>

                            <strong>Age Restriction: </strong>{{ $data->ageRestriction }}<br/>
                            <strong>Length: </strong>{{ $data->length }}<br/>
                            <small>{{ Str::limit($data->description, 120) }}<br/>
                                <strong>Show Times: </strong> {{ $data->showtimes }}

                                <?php //} ?>
                                <br>
                                <a href="{{ route('movies.moreinfo',array($data->movieID,slugify($data->name))) }}"
                                   class="pull-right">Read more...</a></small>
                        </li>
                        <?php $count++; ?>

                        @endforeach

                    </ul>

                </div>
                <?php } else { ?>
                There are no movies to display for the selected cinema.
            <?php } ?>        <!-- holder -->
            </div>
            <!-- span9 -->


        <!-- <div class="span2 filterable web">
        <h4 class="hborder">Movie Finder</h4>
        {{ Form::select('byCinema',$cinemas,$mallID,array('id'=>'byCinema')) }}
            <select name="byMovie" id="byMovie">
                <option value="">Select a movie</option>
@foreach($movies as $movie)
            <option value="{{ $movie['movieID'] }}">{{ $movie['name'] }}</option>
            @endforeach
            </select>
{{--@include('partials/moviereviews')--}}
            </div>
-->
        </div>
        <div class="clearfix">&nbsp;</div>
        <div class="text-right">
            <button class="btn btn-inverse go_back">&laquo; Go back</button>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {

            $(':radio').change(function () {
                $('.choice').text(this.value + ' stars');
                $("#score").val(this.value);
            });

            $("#byCinema").change(function () {

                if ($('#byCinema option:selected').val() == '') {

                } else {
                    var mallToView = $('#byCinema option:selected').val();
                    var url = '/movies/mallmovies/' + mallToView;
                    window.location.assign(url);
                }

            });

            $('#slt_show_date').on('change', function () {
                $(this).closest('form').submit();
            })

            $("#byMovie").change(function () {
                if ($(this).val() == '') {

                } else {
                    var movieID = $(this).val();
                    var movieName = $('#byMovie option:selected').text();
                    var name = movieName.replace(/ /g, '-');
                    window.location.href = 'movies/moreinfo/' + movieID + '/' + encodeURI(name);
                }
            });

        });
    </script>
@stop

