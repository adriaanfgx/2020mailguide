@extends('layouts.pages')
@section('seo_meta')
@stop
@section('title')
@stop

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            @include('partials.headers.search')
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Movies: </span>Now showing </h1>
                    <p class="animated fadeInDown delay2">All the movie times, from all the malls in SA in one place</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
@stop

@section('content')

    <?php $count = 1; ?>

    <div class="row-fluid">
        <div class="span3">
            <h4 class="hborder">Movie Finder</h4>
            <select name="byCinema" class="byCinema">
                <option value="">Select a cinema</option>
                @foreach($malls as $key=>$value)
                    @if(in_array($key, $whatsShowingGL->toArray()))
                        <option value="{{ $key }}">{{ $value }}</option>
                    @endif
                @endforeach
            </select>

            <select name="byMovie" class="byMovie">
                <option value="">Select a movie</option>
                @foreach($movies as $movie)
                    <option value="{{ $movie->movieID }}">{{ $movie->name }}</option>
                @endforeach
            </select>


            @include('partials/moviereviews')

        </div>
        <div class="span9">
            <ul class="thumbnails">

                @foreach( $movies as $movie )
                    @if($count>=7)
            </ul>
            <ul class="thumbnails">
                <?php $count = 1; ?>
                @endif
                <?php
                $image = (!empty($movie->bigImage)) ? $movie->bigImage : "nomovie.png";
                ?>
                <li class="span2 filterable web print">
                    <div class="overlay-wrapper">
                        <a href="{{ route('movies.moreinfo',array($movie->movieID, slugify($movie->name))) }}">
                            <img src="http://www.finemovies.co.za/uploadimages/{{ $image }}" alt="{{ $movie->name }}"
                                 class="overlay-image" data-overlaytext="Movie Info"/>
                        </a>
                        <div class="overlay"></div>
                    </div>
                    <div class="blog-content">
                        <div class="caption">
                            <h6>{{ $movie->name }}</h6>
                            <?php
                            $blurb = Str::limit($movie->description, 100);
                            if (strlen(trim($movie->name)) >= 35) {
                                $blurb = Str::limit($movie->description, 60);
                            }
                            ?>
                            <div class="blurb">{{ $blurb }}</div>
                            <p class="readmore">
                                <a href="{{ route('movies.moreinfo',array($movie->movieID, slugify($movie->name))) }}">
                                    <em>Read More &rarr;</em></a>
                            </p>

                        </div>
                        <div class="mvie_review">
                            <a class="reviewBtn"
                               href="{{ route('movies.moreinfo',array($movie->movieID, slugify($movie->name))) }}">
                                <em>Add a Review</em></a>
                        </div>
                    </div>
                </li>
                <?php $count++; ?>
                @endforeach
            </ul>
            <div class="pagination"><?php //echo $movies->links(); ?></div>
        </div>
        <!-- row-fluid -->
    </div><!--/row-fluid-->
@stop
@section('exScript')
    <script type="text/javascript">
        $(document).ready(function () {

            $('.carousel').carousel({
                interval: 2000
            });
        });
    </script>
@stop


