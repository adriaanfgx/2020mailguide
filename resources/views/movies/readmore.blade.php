@extends('layouts.pages')

@section('seo_meta')
@section('title')

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            @include('partials.headers.search')
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Movies: </span>What's <span>showing?</span></h1>

                    <p class="animated fadeInDown delay2">Enjoy the best the silver screen has to offer</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span12">
                    {{ generateBreadcrumbs() }}
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->

@stop

@section('content')
    <div class="row-fluid padding-inner">
        <div class="span12">
            <h4 class="hborder">{{ $movie->name }}</h4>
        </div>
        <!--span12 -->
    </div><!--row -->

    <div class="row-fluid">
        <div class="span12">


            <div class="span2 filterable illustration photography">
                <?php
                $_image = (!empty($movie->bigImage)) ? $movie->bigImage : "nomovie.png";
                ?>
                <div>
                    <img src="http://www.finemovies.co.za/uploadimages/{{ $_image  }}" alt="No Image" class="overlay-image"/>
                    <div class="overlay"></div>
                </div>
                <div class="blog-content">
                    <div class="caption">
                        <h6>{{ $movie->ageRestriction }}</h6></div>
                </div>
                <br />
                <div class="text-right">
                    <button class="btn btn-inverse go_back">&laquo; Go back</button>
                </div>
            </div>
            <div class="span5">
                <div class="overlay-wrapper">

                    <div class="overlay"></div>
                </div>
                @if($youtubeID)
                    <div class="">
                        <div class="blog-content border_top">
                            <div class="mall-caption">
                                <!-- <h4 class="hborder no-margin-bottom">Movie Trailer</h4> -->
                                <iframe width="100%" height="350" src="//www.youtube.com/embed/{{ $youtubeID }}" frameborder="0"></iframe>

                            </div>
                        </div>
                        <p>&nbsp;</p>
                    </div>
                @endif
                <div class="">

                    <div class="blog-content border_top">
                        <div class="mall-caption">
                            <h4 class="hborder">Movie Information</h4>
                            <dl>
                                <dt>About the Movie</dt>
                                <dd>{{ $movie->description }}</dd>
                                <dt>Director</dt>
                                <dd>{{ $movie->director }}</dd>
                                <dt>Cast</dt>
                                <dd>{{ $movie->cast }}</dd>
                                <dt>Length</dt>
                                <dd>{{ $movie->length }}</dd>
                            </dl>
                        </div>
                    </div>
                    <p>&nbsp;</p>
                    @if(count($showtimes))
                        @include('partials/addmoviereview')
                    @else
                        @include('partials/moviereviews', array('title' => 'Movie Reviews', 'bordered' => true))
                    @endif

                </div>
            </div>

            @if(count($showtimes))
                <div class="span5">
                    <div class="blog-content border_top">
                        <div class="mall-caption">
                            <h4 class="hborder">Currently Showing at these Malls</h4>
                            <div class="alert alert-note"><em class="fa fa-lightbulb-o"></em> Move mouse over a Mall Name for show times details.</div>
                            <div class="" id="showing_list">
                                <?php
                                $count = 0;
                                $total_mall = count($showtimes);
                                ?>
                                @foreach($showtimes as $mall)
                                    <?php
                                    if(isset($mall['shows']) && count($mall['shows'])){
                                        $mall_title = '<strong>'.$mall['name'].'</strong>';
                                        $mall_title .= '<br><strong>'.date('Y-m-d', strtotime($mall['shows']['start_date'])).'</strong>';
                                        if(date('Y-m-d', strtotime($mall['shows']['end_date'])) != date('Y-m-d', strtotime($mall['shows']['start_date']))){
                                            $mall_title .= ' to <strong>'.date('Y-m-d', strtotime($mall['shows']['end_date'])).'</strong>';
                                        }
                                        if(trim($mall['shows']['time']) != ''){
                                            $mall_title .= '<br>'.$mall['shows']['time'];
                                        }
                                        $mall_title .= '<br>';
                                    }
                                    ?>
                                    <a class="mall-showtimes" data-toggle="tooltip" href="/movies/mallmovies/{{ $mall['id'] }}" data-html="true" title="{{ $mall_title }}">{{ $mall['name'] }}</a>
                                    @if($count < ($total_mall-1)), @endif
                                    <?php $count++; ?>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <p>&nbsp;</p>
                    <div class="">

                        <div class="overlay-wrapper">

                            <div class="overlay"></div>
                        </div>
                        <div class="blog-content border_top">
                            <div class="mall-caption">
                                {{--<h4 class="hborder">Showing At</h4>

                                <select name="byCinema" id="byCinema">
                                    <option value="">Select a cinema</option>
                                    @foreach($cinemas as $cinema)
                                    <option value="{{ $cinema['mallID'] }}">{{ $cinema['name'] }}</option>
                                    @endforeach
                                </select>--}}

                                <h4 class="hborder">Other Movies</h4>

                                <select name="byMovie" id="byMovie">
                                    <option value="">Select a movie</option>
                                    @foreach($movies as $dropdown)
                                        <option value="{{ $dropdown->movieID }}">{{ $dropdown->name }}</option>
                                    @endforeach
                                </select>

                            {{--<h4 class="hborder">Movie Reviews</h4>

                            <div id="myCarousel" class="carousel slide opacity-fade">
                                <!-- Carousel items -->
                                <div class="carousel-inner">

                                    <div class="item active">

                                    </div>
                                </div>
                                <br/><br/>
                                <!-- Carousel nav -->
                                <a href="#myCarousel" data-slide="prev"><i class="fa fa-chevron-circle-left fa-2x"></i></a>&nbsp;&nbsp;
                                <a href="#myCarousel" data-slide="next"><i class="fa fa-chevron-circle-right fa-2x"></i></a>

                            </div>--}}
                            @include('partials/moviereviews', array('title' => 'Movie Reviews'))
                            <!--/my carousel-->

                            </div>
                        </div>
                    </div>

                </div>
            @else
                <div class="span5">
                    @include('partials/addmoviereview')
                </div>
            @endif
        </div>

    </div><!--row fluid -->


    <script type="text/javascript">
        $(document).ready(function () {

            $('.carousel').carousel({
                interval: 2000
            });

            $('.mall-showtimes').tooltip();

            $("#byCinema").change(function(){
                var mallToView = $(this).val();

                var url = '/movies/mallmovies/'+mallToView;

                window.location.assign(url);
            });

            $("#byMovie").change(function(){

                var movieID = $(this).val();
                var movieName = $(this).find(":selected").text();
                window.location.href = '/movies/moreinfo/'+movieID+'/'+encodeURI(movieName);
            });
        });

    </script>
@stop

