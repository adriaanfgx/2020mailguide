@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Logged In </h1>
                <p class="animated fadeInDown delay2">Manager your Movies</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a> <span class="divider">/</span></li>
                    <li class="active">Memberzone</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop


@section('content')

<div class="span12 middle-headings dotted-border white-bg">
    <h2><a href="#">Movie House Member</a></h2>
    <div class="span6 pull-left">Hi {{ $user->first_name }}<br /> Listed on this page is a host of link shortcuts that will help you make the most of your listing on Mallguide. Please make use of our range of free services and tools at your disposal to help market and promote your mall to all of South Africa.</div>
</div><!--/span12-->

<div class="row-fluid">
    <div class="span7">
        <h4 class="dotted-border">Notice Board</h4>
        <span><strong>Pending</strong> - Awaiting approval</span><br />
        <div class="yellow">
            <ul>
                <li><strong>{{ $subscribed }}</strong> &nbsp;&nbsp; Mallguide member(s) have subscribed to your movie schedule newsletter</li>
            </ul>
        </div>

        <h4>Your Quick Stats</h4>

        <div class="yellow">
            <ul>
                <li><strong>{{ $countSchedules }}</strong> &nbsp;&nbsp; Movie Schedule displayed for today , &nbsp;<a href="{{ route('moviehouse.addmovieschedule') }}">Add schedule</a>&nbsp; or &nbsp;<a href="{{ route('moviehouse.listmovieschedules') }}">List schedules</a>.</li>
            </ul>
        </div><!--yellow -->
    </div><!-- span6 -->

    <div class="span5">
        <h4>Your Control Panel</h4>

        <div class="control-panel">

            <h4><span class="control-ico-about">About You</span></h4>
            <ul>
                <li><a href="{{ route('moviehouse.getUpdatePassword',$user->id) }}">Update your password</a></li>
                <li><a href="{{ route('moviehouse.getUpdateProfile',$user->id) }}">Update your personal information</a></li>
            </ul>

            <h4><span class="control-ico-links">Quick Links</span></h4>
            <ul>
                <li>
                    <a href="{{ route('moviehouse.addmovieschedule') }}">Add a Movie Schedule</a>&nbsp; | &nbsp;
                    <a href="{{ route('moviehouse.listmovieschedules') }}">List Movie Schedules</a>
                </li>
<!--                <li>-->
<!--                    <a href="{{ route('moviehouse.getCreateCompetition') }}">Add a Competition </a>&nbsp; | &nbsp;-->
<!--                    <a href="{{ route('moviehouse.getListCompetitions') }}"> List Competitions</a>-->
<!--                </li>-->
            </ul>
        </div><!-- control panel -->
    </div><!--span6 -->
</div><!--row -->
@stop
