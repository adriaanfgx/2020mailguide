@extends('layouts.pages')
@section('title')
@parent
Member Zone
@stop

@section('content')

<div class="row-fluid">
	<div class="blog-content">
    <h3>Add a new movie schedule</h3>

    To add a new schedule please specify the first day and last day of the period when the schedule is to be displayed. Then choose the movies showing on those days by clicking the checkboxes next to the relevant movie names.
    <br />
    <br />
    {{ Form::open(array('route' => 'moviehouse.postAddMovieSchedule', 'method' => 'post','name' => 'postAddMovieSchedule','id' => 'postAddMovieSchedule', 'class' => 'form-inline')) }}
    {{ Form::hidden('mallID',$mallID) }}
    <div class="row" style="padding-left: 40px;">
        <div class="span4">
            <div class="control-group {{ ($errors->has('startDate') ? 'error' : '') }}">
                <div class="controls">
                    <div class="input-append datetimepicker4">
                        <label for="startDate"><strong>Start</strong></label>
                        <div class="control-group">
                            {{ Form::text('startDate',$preselectStart,array('class'=>'span12','data-format'=>'yyyy-MM-dd','readonly')) }}
                            <span class="add-on">
                              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                              </i>
                            </span>
                            {{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group {{ ($errors->has('endDate') ? 'error' : '') }}">
                <div class="controls">
                    <label for="endDate"><strong>End</strong></label>
                    <div class="control-group">
                        <div class="input-append datetimepicker4">
                            <!--span3 -->
                            {{ Form::text('endDate',$preselectEnd,array('class'=>'span12','data-format'=>'yyyy-MM-dd','readonly')) }}
                            <span class="add-on">
                              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                              </i>
                            </span>
                            {{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    If a movie is not displayed please send a message to movies@mallguide.co.za with the missing movie title and we will add it to the database.
    <br />
    <br />
    <div class="row" style="padding-left: 40px;">
        <div class="span4">
            {{ Form::select('filter',$movieFilter,null,array('id'=>'filter','class'=>'span4'))}}
        </div>
        <div class="span4"></div>
    </div>
    <br />
    <br />
    <?php
        $count=1;
        $genre = "Genre not specified";
    ?>

        <ul class="thumbnails">
            @foreach( $movies as $movie )
            @if($count>=4)
        </ul>

	        <ul class="thumbnails">
	            <?php $count = 1; ?>
	            @endif
	            <li class="span4 filterable web print">
	                {{ Form::checkbox('movieID[]',$movie->movieID) }} &nbsp;&nbsp;
	                <label nowrap><strong>{{ $movie->name }}</strong>&nbsp;<small>( {{ !empty($movie->genre) ? $movie->genre : "Genre not specified" }} )</small></label>
	                <br />
	                {{ Form::text($movie->movieID.'_times[]',null) }}
	            </li>
	            <?php $count++; ?>
                @endforeach

	        </ul>

    <br />
    <br />
    <div class="span3">

        <button class="submit reg-btn" id="add_review">Submit Details</button><br /><br />
        <a href="{{ route('moviehouse.index') }}">Back To Member Zone</a>
    </div>
    <!--span4-->
    {{ Form::close() }}
	<div class="clear"></div>
	</div><!--/span12-->
</div><!--/span12-->

@stop

@section('exScript')

<script type="text/javascript">

    $(function () {

        $('.datetimepicker4').datetimepicker({
            pickTime: false
        }).on('changeDate', function(e){
                $(this).datetimepicker('hide');
            });


    });
</script>
@stop
