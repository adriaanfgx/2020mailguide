@extends('layouts.backpages')
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Events Listing</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
				{{ Helpers::generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')
@stop
@section('content')

<div class="row-fluid">
    <div class="blog-content">
        <h4 class="hborder">Listed Movie Schedules</h4>
    <!-- span6 -->
        @if(sizeof($movieschedules) > 0)
        <h5>Current Events</h5>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($movieschedules as $schedule)
            <tr>
                <td>{{ date_format(date_create($schedule->startDate),'l, jS F Y') }}</td>
                <td>{{ date_format(date_create($schedule->endDate),'l, jS F Y') }}</td>
                <td>
                    <a href="{{ route('moviehouse.getEditMovieSchedule',$schedule->whatsShowingID) }}" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp;|&nbsp;<a class="btn btn-danger btn-mini action_delete" href="#" id="{{ $schedule->whatsShowingID }}"><i class="fa fa-trash-o"></i> Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
        @else
        <span>There are no listed schedules .........</span>
        @endif
        <!--span12 -->
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $(".action_delete").click(function (e) {

            e.preventDefault();

            var schedule = $(this).attr('id');
            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function () {
                $.ajax({
                    type: "POST",
                    url: '/moviehouse/deletemovieschedule/'+schedule,
                    data: '',
                    success: function () {
                        location.reload();
                    }
                });
            });
        });
    });
</script>
@stop

