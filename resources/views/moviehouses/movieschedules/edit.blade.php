@extends('layouts.backpages')
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Edit Event </h1>

                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div>
            <!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/moviehouse">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Edit Event</li>
                </ul>
            </div>
            <!--/span6-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')
@stop


@section('content')

<div class="row-fluid">
	<div class="blog-content">
    <h3>Edit Movie schedule</h3>

    {{ Form::model($whatsShowing, array('route' => array('moviehouse.postEditMovieSchedule', $whatsShowing->whatsShowingID),'method'=>'post','name' => 'postEditMovieSchedule', 'id' => 'postEditMovieSchedule', 'class' => 'form-inline')) }}

    <div class="row-fluid" >
        <div class="span4">
            <div class="control-group {{ ($errors->has('startDate') ? 'error' : '') }}">
                <div class="controls">
                    <div class="input-append datetimepicker4">
                        <label for="startDate"><strong>Start</strong></label>
                        <div class="control-group">
                            {{ Form::text('startDate',$whatsShowing->startDate,array('class'=>'span12','data-format'=>'yyyy-MM-dd','readonly')) }}
                            <span class="add-on">
                              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                              </i>
                            </span>
                            {{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group {{ ($errors->has('endDate') ? 'error' : '') }}">
                <div class="controls">
                    <label for="endDate"><strong>End</strong></label>
                    <div class="control-group">
                        <div class="input-append datetimepicker4">
                            <!--span3 -->
                            {{ Form::text('endDate',$whatsShowing->endDate,array('class'=>'span12','data-format'=>'yyyy-MM-dd','readonly')) }}
                            <span class="add-on">
                              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                              </i>
                            </span>
                            {{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    If a movie is not displayed please send a message to movies@mallguide.co.za with the missing movie title and we will add it to the database.
    <br />
    <br />
    <div class="row-fluid">
        <div class="span4">
            {{ Form::select('filter',$movieFilter,null,array('id'=>'filter','class'=>'span4'))}}
        </div>
        <div class="span4"></div>
    </div>
    <br />
    <br />
    <?php $count=1; ?>
        <ul class="thumbnails">

        @foreach( $movies as $movie )
            @if($count>=4)
        </ul>
        <ul class="thumbnails">
            <?php $count = 1; ?>
            @endif
            <?php
                $checked = false;
                $moviesSelected = array();

                foreach( $whatsShowingMovies as $showing )
                {
                    array_push($moviesSelected,$showing->movieID);
                    $movieTime['time'.$showing->movieID] = $showing->times;
                }
                if( in_array($movie->movieID,$moviesSelected) ){
                    $checked = true;
                }
            ?>
            <li class="span4 filterable web print" id="{{ $movie->movieID }}">
                {{ Form::checkbox('movieID[]',$movie->movieID,$checked) }} &nbsp;&nbsp;
                <label nowrap><strong>{{ $movie->name }}</strong>&nbsp;<small>( {{ !empty($movie->genre) ? $movie->genre : "Genre not specified" }} )</small></label>
                <br />
                @if( isset($movieTime['time'.$movie->movieID]) )
                <?php $formVal = $movieTime['time'.$movie->movieID]; ?>

                @else
                <?php $formVal = null; ?>
                @endif
                {{ Form::text('times_'.$movie->movieID,$formVal) }}
            </li>
            <?php $count++; ?>
            @endforeach
        </ul>


    <div class="span3">
        <br/>
        <button class="submit reg-btn" id="add_review">Submit Details</button>
    </div>
    <!--span4-->
    {{ Form::close() }}
        <div class="clear"></div>


	</div><!--/span12-->
</div><!--/span12-->

@stop

@section('exScript')

<script type="text/javascript">

    $(function () {

        $('.datetimepicker4').datetimepicker({
            pickTime: false
        }).on('changeDate', function(e){
                $(this).datetimepicker('hide');
        });

    });
</script>
@stop
