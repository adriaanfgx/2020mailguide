@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Create Competition</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div>
            <!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/moviehouse">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Create Competition</li>
                </ul>
            </div>
            <!--/span6-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')
@stop

@section('content')

<div class="span12 middle-headings white-bg">
    <h3>Build your own competition</h3>

    {{ Form::open(array('route' => 'moviehouse.postCreateCompetition', 'method' => 'post','files'=>'true', 'name' => 'compCreate','id' =>'compCreate', 'class' => 'form-inline')) }}
    {{ Form::hidden('mallID',$mallID) }}
    {{ Form::hidden('thanksMsg','Thank you for submitting your details') }}
    <div class="span5">
        <h4 class="dotted-border">Form Recipient</h4>

        <div class="control-group {{ ($errors->has('recipient') ? 'error' : '') }}">
            <label for="recipient" class="control-label"><strong>Mandatory: E-mail address of person receiving the competition entries.</strong></label>
            <div class="controls">
                {{ Form::text('recipient',null,array('class'=>'txtbar','id'=>'recipient')) }}
                <span class="help-block">{{ ($errors->has('recipient') ? $errors->first('recipient') : '') }}</span>
            </div>
        </div>

        <h4 class="dotted-border">Competition Title</h4>

        <div class="control-group {{ ($errors->has('subject') ? 'error' : '') }}">
            <label for="subject" class="control-label"><strong>Mandatory: Also appears on the subject line of the email.</strong></label>
            <div class="controls">
                {{ Form::text('subject',null,array('class'=>'txtbar','id'=>'subject')) }}
                <span class="help-block">{{ ($errors->has('subject') ? $errors->first('subject') : '') }}</span>
            </div>
        </div>

        <div class="control-group">
            <label for="description"><strong>Competition description &amp; Rules</strong></label>
            <div class="controls">
                <textarea rows="3" class="txtbox" name="description"></textarea>
            </div>
        </div>

    </div>
    <!--span4-->

    <div class="span6">

        <div class="control-group {{ ($errors->has('startDate') ? 'error' : '') }}">
            <label for="startDate" class="control-label"><strong>Start</strong></label>
            <div class="controls">
                <div class="input-append datetimepicker4">
                    {{ Form::text('startDate',null,array('class'=>'span12','id'=>'startDate','data-format'=>'yyyy-MM-dd','readonly')) }}
                <span class="add-on">
                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                  </i>
                </span>
                    {{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}
                </div>
            </div>
        </div>

        <div class="clear"></div>
        <br/>
        <div class="control-group {{ ($errors->has('endDate') ? 'error' : '') }}">
            <label for="endDate" class="control-label"><strong>End</strong></label>
            <div class="controls">
                <div class="input-append datetimepicker4">
                    {{ Form::text('endDate',null,array('class'=>'span12','id'=>'endDate','data-format'=>'yyyy-MM-dd','readonly')) }}
                <span class="add-on">
                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                  </i>
                </span>
                    {{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}
                </div>
            </div>
        </div>

        <br />
        <br />
        <div class="clear"></div>
        <label for="activated"><strong>Display competition</strong></label>
        Yes &nbsp;<input name="activated" type="radio" value="Y" checked="checked"/>
        No &nbsp;<input name="activated" type="radio" value="N"/>

        <div class="clear"></div>
        <br/>
        <h4 class="dotted-border">Uploads</h4>
        <div class="control-group {{ ($errors->has('image1') ? 'error' : '') }}">
            <label for="image1" class="control-label"><strong>Upload an image</strong></label>
            <div class="controls">
                <span class="btn btn-file">{{ Form::file('image1') }}</span>
            </div>
            <span class="help-block">{{ ($errors->has('image1') ? $errors->first('image1') : '') }}</span>
        </div>
        <br />
        <br />
        <div class="control-group">
            <label><strong>Terms and Conditions &nbsp;<small>Only PDF files</small></strong></label>
            <div class="controls">
                <span class="btn btn-file">{{ Form::file('TsandCsFile') }}</span>
            </div>
        </div>
        <button class="submit reg-btn" id="add_review">Submit Details</button>
        <input type="hidden" name="submitBtnText" value="Submit" />
    </div>
    <!--span4-->
    {{ Form::close() }}

</div><!--/span12-->

@stop

@section('exScript')
<script>

    $(function () {

        $('.datetimepicker4').datetimepicker({
            pickTime: false
        }).on('changeDate', function(e){
                $(this).datetimepicker('hide');
            });


    });
</script>
@stop
