@extends('layouts.pages')
@section('title')
    @parent
    Competitions | {{ $mall->name }}
@stop

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            @include('partials.headers.search')
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Competitions: </span>What's <span>Up for Grabs</span>
                    </h1>
                    <p class="animated fadeInDown delay2">Lots of prizes to win</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
@stop

@section('content')
    <div class="container-fluid">
        <div class="row-fluid">
            <h4 class="span12 hborder">Competition @ {{$mall->name}}</h4>
        </div>
        <div class="row-fluid">
            @if( $competition->image)
                <div class="overlay-wrapper span2">
                    <div class="zoom-gallery">
                        <a href="{{ Config::get('app.url') }}/uploadimages/{{ $competition->image }}"
                           title="{{ $competition->subject }}">
                            <img src="{{ Config::get('app.url') }}/uploadimages/{{ $competition->thumbnail }}"
                                 alt="{{ $competition->subject }}"/>
                        </a>
                    </div>
                </div>
            @endif
            <div class="blog-content border_top span9 leftMargin">
                <h4 class="hborder">{{{ $competition->subject }}}</h4>
                <p>{{ $competition->description }}</p>
                @if($competition->TsandCsFile)
                    <p><a target="_blank"
                          href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $competition->TsandCsFile }}">Terms
                            & Conditions apply.</a></p>
                @endif
                <dl class="leftMargin">
                    <dt>Start Date</dt>
                    <dd>{{ date_format(date_create($competition->startDate),'j F Y') }}</dd>
                    <dt> End Date</dt>
                    <dd>{{ date_format(date_create($competition->endDate),'j F Y') }}</dd>
                </dl>
                <?php
                if ($competition->formFields->count() > 0 && $competition->noForm == 'N') {
                    echo Form::open(array('route' => array('competition.submit', $competition->formID), 'name' => 'competitionForm', 'files' => true, 'id' => 'competitionForm'));
                    echo Form::token();
                    echo Form::hidden('siteURL', Request::url());
                    foreach ($competition->formFields as $formField) {
                        $fieldOutput = "";
                        $required = "";
                        $classes = "form-control";
                        if ($formField->mandatory == 'Y') {
                            $classes .= ' required';
                            $required = '*';
                        }

                        $the_id = 'fieldID' . $formField->formFieldID;
                        $error = $errors->has($the_id) ? "<span class='text-error'>" . $errors->first($the_id) . '</span><br>' : "";

                        if (isset($input[$the_id])) {
                            $default = $input[$the_id];
                        } else {
                            $default = $formField->defaultValue;
                        }

                        switch ($formField->typeOfField) {
                            case "text":

                                if ($formField->emailField == 'Y') {
                                    $classes .= ' email';
                                }

                                if ($formField->name == 'Date of Birth') {
                                    $classes .= ' datepicker';
                                }

                                $fieldOutput = Form::text($the_id, $default, array('class' => trim($classes)));

                                echo "<label>" . $formField->name . $required . " </label>$error";
                                echo "<div class='form-group'>" . $fieldOutput . "</div>";

                                break;

                            case "textarea":
                                if ($formField->mandatory) {
                                    $classes = 'required';
                                }

                                $fieldOutput = Form::textarea('fieldID' . $formField->formFieldID, $default, array('class' => $classes));

                                echo "<label>" . $formField->name . "</label>$error";
                                echo "<div class='form-group'>" . $fieldOutput . "</div>";

                                break;
                            case "radio":

                                if ($formField->mandatory) {
                                    $classes = ' required';
                                }

                                //get elements of field
                                foreach ($formField->formFieldElements as $element) {
                                    $checked = false;

                                    if ($element->checkedSelected == 'Y') {
                                        $checked = true;
                                    }
                                    $fieldOutput .= "  <label class='inline'>" . Form::radio('fieldID' . $formField->formFieldID, $element->name, $checked, array('class' => $classes)) . $element->name . "</label>&nbsp;&nbsp;&nbsp;";
                                }
                                $groupClass = 'radio';
                                echo "<div class='form-group'><label>" . $formField->name . "$required</label>$error<br>" . $fieldOutput . "</div>";

                                break;
                            case "checkbox":

                                if ($formField->mandatory) {
                                    $classes = 'required';
                                }

                                //get elements of field
                                foreach ($formField->formFieldElements as $element) {
                                    $checked = false;

                                    if ($element->checkedSelected == 'Y') {
                                        $checked = true;
                                    }
                                    $fieldOutput .= Form::checkbox('fieldID' . $formField->formFieldID, $element->name, $checked, array('class' => $classes));
                                }

                                echo "<div class='checkbox'><label>" . $fieldOutput . $formField->name . "$required</label>" . $error . "</div>";

                                break;
                            case "file":

                                if ($formField->mandatory) {
                                    $classes = 'required';
                                }
                                //die(var_dump($formField->formFieldSpecs->fileTypes));
                                $subscript = "";
                                if ($formField->formFieldSpecs->fileTypes != "") {
                                    $subscript = "<div class='subscript'>file types: {$formField->formFieldSpecs->fileTypes}</div>";
                                }

                                //get elements of field
                                $checked = false;
                                $fieldOutput .= "$subscript";
                                $fieldOutput .= Form::file('fieldID' . $formField->formFieldID, array('class' => $classes));

                                echo "<div class='form-group'><label>" . $formField->name . $required . "</label>" . $fieldOutput . $error . "</div><br />";

                                break;
                            case "select" :
                                if ($formField->mandatory) {
                                    $classes .= 'required';
                                }

                                //get elements of field
                                $options = array();
                                $array['data-id'] = $formField->formFieldID;
                                $array['class'] = $classes;
                                if ($formField->formFieldSpecs->multiple == "Y") {
                                    $array['multiple'] = 'true';
                                }
                                $checked = '';
                                foreach ($formField->formFieldElements as $element) {

                                    $options[$element->name] = $element->name;

                                    if ($element->checkedSelected == 'Y') {
                                        $checked = $element->name;
                                    }

                                }
                                $fieldOutput = Form::select('fieldID' . $formField->formFieldID, $options, $checked, $array);

                                echo "<label>" . $formField->name . "</label>";
                                echo "<div class='form-group'>" . $fieldOutput . " " . $error . "</div>";

                                break;
                        }
                    }
                    echo Form::submit('Submit Entry', array('class' => 'btn btn-primary'));
                    echo Form::close();

                }

                ?>

            </div>
            <div class="clearfix"></div>
            <p>&nbsp;</p>
            <div class="text-left">
                <button class="btn btn-inverse go_back">&laquo; Go back</button>
            </div>
            <br/>
            <br/>
        </div>
        @stop
        @section('exScript')
            <script type="text/javascript">
                $(document).ready(function () {

                    $('.zoom-gallery').magnificPopup({
                        delegate: 'a',
                        type: 'image',
                        closeOnContentClick: false,
                        closeBtnInside: true,
                        mainClass: 'mfp-with-zoom mfp-img-mobile',
                        gallery: {
                            enabled: false
                        },
                        zoom: {
                            enabled: true,
                            duration: 300, // don't foget to change the duration also in CSS
                            opener: function (element) {
                                return element.find('img');
                            }
                        }
                    });

                });
            </script>
@stop
