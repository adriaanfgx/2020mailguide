@if(count($competitions))
    @foreach($competitions as $competition)
        @if(trim(strip_tags($competition->subject)) != '' && trim(strip_tags($competition->subject)) != '&nbsp;')
            <tr>
                <td>
                    <?php $mall_count = count($competition->mall); ?>
                    @foreach($competition->mall as $idx=>$mall)
                        <a href="/malls/view/{{ $mall->mallID }}/{{ slugify($mall->name) }}">{{ $mall->name }}</a>
                        @if(($mall->mallCity && $mall->mallProvince))
                            <br><a href="/malls/?city={{ $mall->city_id }}-{{ slugify($mall->mallCity->name) }}" class="dark-link">{{ $mall->mallCity->name }}</a>, <a href="/malls/provinceID/{{ $mall->province_id }}" class="dark-link">{{ $mall->mallProvince->name }}</a>
                        @endif
                        @if($idx < ($mall_count - 1))<hr size="1" noshade>@endif
                    @endforeach
                </td>
                <td><a href="/competitions/show/{{ $competition->formID }}">{{ $competition->subject }}</a></td>
                <td>{{ date_format(date_create($competition->startDate),'j F Y') }}</td>
                <td>{{ date_format(date_create($competition->endDate),'j F Y') }}</td>

            </tr>
        @endif
    @endforeach
@else
    <tr>
        <td colspan="5">There are no current competitions for the selected criteria!</td>
    </tr>
@endif
