@extends('layouts.pages')
@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            @include('partials.headers.search')
            <div class="row-fluid">
                <div class="span8">
                    <h1 class="animated fadeInDown delay1"><span>Competitions: </span>Enter to <span>Win</span></h1>
                    <p class="animated fadeInDown delay2">Retail prizes worth entering for!</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
@stop

@section('content')
    <div class="row-fluid">

        <div class="hborder">
            <h4 class="paddingBottom">Competitions</h4>

            <div class="row-fluid">
                <div class="span3 offset6">
                    <div class="overlay-wrapper"></div>
                    <select name="provinces" id="provinces">
                        <option value="">Please Select a Province...</option>
                        @foreach($provinces as $key=>$value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="span3">
                    <div class="overlay-wrapper"></div>
                    <?php $mallSelected = session()->get('selected_mall', ''); ?>
                    {{ Form::select('mall_id',$malls,$mallSelected,['placeholder' => 'Please select ...'], array('id'=>'mall_id','disabled')) }}
                    <?php session()->forget('selected_mall'); ?>
                </div>
            </div>
        </div>

        <table class="table table-bordered table-striped responsive comps-table">
            <thead>
            <tr>
                <th>Mall</th>
                <th>Title</th>
                <th>Start Date</th>
                <th>End Date</th>
            </tr>
            </thead>
            <tbody id="comp_data">
            <tr>
                <td colspan="4">Please select a Province first...</td>
            </tr>
            </tbody>
        </table>
    </div>
@stop
@section('exScript')
    <script type="text/javascript">
        $(document).ready(function () {
            if ($("#provinces").val() != '') {

                $('#mall_id').attr('disabled', false);
                //Get shops for the pre-selected mall
//            populateTable();
                refreshMallList();
            } else {
                $('#mall_id').attr('disabled', 'disabled');
                $('#mall_id').val('Select a province ...');
//    		$('#comp_data').html('<tr><td colspan="5">Please select a Province first...</td></tr>');

                populateTable('all');
            }

            $("#mall_id").change(function () {
                if ($(this).val() == '') {
                    if ($("#provinces").val() == '') {
                        populateTable('all');
                    } else {
                        refreshMallList();
                    }
                } else {
                    populateTable();
                }
            });

            $("#provinces").change(function () {
                refreshMallList();
                if ($(this).val() == '') {
                    populateTable('all');
                    return false;
                }
//            populateTable('all');
            });
        });

        function refreshMallList() {
            var $province = $('#provinces').val();
            var mall_ids = '';

            if ($province == '') {
                $('#mall_id').empty().append('<option value="">Please Select a Province...</option>');
                $('#mall_id').attr('disabled', 'disabled');
                $('#comp_data').html('<tr><td colspan="5">Please select a Province first...</td></tr>');

            } else {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: '/malls/compfilter/' + $province,
                    success: function (data) {

                        $('#mall_id').empty().append('<option value="">Please select a Mall...</option>');
                        //if(data.length){
                        $.each(data, function (i, val) {
                            $('#mall_id').append('<option value="' + val.mallID + '">' + val.name + '</option>');
                            mall_ids += val.mallID + ',';
                        });
                        //}

                        if (mall_ids != '') {
                            populateTable(mall_ids);
                        }

                        $('#mall_id').removeAttr('disabled');
                        $('#comp_data').html('<tr><td colspan="5">Please select a mall to see a list of Competitions!</td></tr>');
                    }
                });
            }

        }

        function populateTable(mall_id) {
            //alert('who');
            var $mall = $('#mall_id').val();
            if (mall_id) {
                $mall = mall_id;
            }

            $('#comp_data').load('/competitions/frontTable/' + $mall);
        }
    </script>
@stop
