@extends('layouts.default')

@section('headingText')
    <div class="heading-text">
        <h1 class="animated flipInY delay1">Listing
            <span>25,000 <a href="/shops">SHOPS</a></span>
        </h1>
        <div class="span10 offset1 animated fadeInLeftBig delay2">
            <h1 class="animated flipInY delay1 white-text">Listing
                <span>1,400 <a href="/malls">MALLS</a></span>
            </h1>
        </div>
        <!--/span2-->
        <div class="clear"></div>
    </div><!--/heading text-->
@stop


@section('content')
    <div class="row-fluid">
        <div class="span12 middle-headings">
            <div id="main-slider" class="liquid-slider">
                <div>
                    <h2 class="title">FEATURED MALL</h2>
                    @include('partials.front_page.featured_mall', ['featuredMall' => $featuredMall])
                </div>

                <div>
                    <h2 class="title">FEATURED SHOP</h2>
                    @include('partials.front_page.featured_shop', ['featuredShop' => $featuredShop])
                </div>
                <div>
                    <h2 class="title">FEATURED COMPETITION</h2>
                    @include('partials.front_page.featured_competition', ['featuredComp' => $featuredComp])
                </div>
                <div>
                    <h2 class="title">FEATURED EVENT</h2>
                    @include('partials.front_page.featured_event', ['featuredEvent' => $featuredEvent])
                </div>
                <div>
                    <h2 class="title">FEATURED PROMOTION</h2>
                    @include('partials.front_page.featured_promotion', ['featuredPromotion' => $featuredPromotion])
                </div>
            </div>
            <!-- main-slider -->

        </div>
        <!-- span12 -->
        <div class="clear"></div>
    </div>

    <div class="header-grey">
        <h2>What's
            <span>SHOWING</span>
            AT THE MOVIES
        </h2>
    </div>
    <p>Everything on the silver screen, all in one place.<a href="/movies">View them all here</a>
    </p>



    <?php $count=1; ?>

    <div class="row-fluid">

        <div class="span3">
            <h4 class="hborder">Movie Finder</h4>

            <select name="byCinema" class="byCinema">
                <option value="">Select a cinema</option>
                @foreach($movieMalls as $key=>$value)
                    <option value="{{ $key }}">{{ $value }}</option>
                @endforeach
            </select>

            <div class="text-center" style="margin-bottom:12px;">- or -</div>

            <select name="byMovie" class="byMovie">
                <option value="">Select a movie</option>
                @foreach($movies as $movie)
                    <option value="{{ $movie->movieID }}">{{ $movie->name }}</option>
                @endforeach
            </select>

            @include('partials/moviereviews')

        </div>

        <div class="span9">
            <ul class="thumbnails">
                <?php $sixMovies = $movies->take(6); ?>
                @foreach( $sixMovies as $movie )

                    <?php
                    $image=(!empty($movie->bigImage)) ? $movie->bigImage : "nomovie.png";
                    ?>
                    <li class="span2 filterable web print">
                        <div class="overlay-wrapper">
                            <a href="{{ route('movies.moreinfo',array($movie->movieID, slugify($movie->name))) }}">
                                <img src="http://www.finemovies.co.za/uploadimages/{{ $image }}" alt="{{ $movie->name }}" class="overlay-image" data-overlaytext="Movie Info"/>
                            </a>
                            <div class="overlay"></div>
                        </div>
                        <div class="blog-content">
                            <div class="caption">
                                <h6>{{ $movie->name }}</h6>
                                <?php
                                $str_limit = 90;
                                if(strlen($movie->name) > 45){
                                    $str_limit = 50;
                                }
                                ?>
                                <div class="blurb">{{ Str::limit($movie->description, $str_limit) }}</div>
                                <p class="readmore">
                                    <a href="{{ route('movies.moreinfo',array($movie->movieID, slugify($movie->name))) }}">
                                        <em>Read More &rarr;</em></a>
                                </p>

                            </div>
                            <div class="mvie_review">
                                <a class="reviewBtn" href="{{ route('movies.moreinfo',array($movie->movieID, slugify($movie->name))) }}">
                                    <em>Add a Review</em></a>
                            </div>
                        </div>
                    </li>

                @endforeach
            </ul>

        </div>
        <!-- span9 -->
    </div>
    <!--/row-fluid-->

    <div class="header-blue">
        <h2>Mallguide
            <span>Community Posts</span>
        </h2>
    </div>

    <div class="row-fluid">
        <div class="span4 comm-heads">
            <h5>Recent Job Posts
                <span class="pull-right">View All</span>
            </h5>

        </div>
        <!-- span6 -->

        <div class="span4 comm-heads">
            <h5>Recent Article Posts
                <span class="pull-right">View All</span>
            </h5>

        </div>
        <!-- span6 -->
        <div class="span4 comm-heads">
            <h5>Top 5 Malls
                <span class="pull-right">View All</span>
            </h5>

        </div>
    </div>
    <!--/row-fluid-->

    {{--@include('partials.googleads')--}}
    <p>&nbsp;</p>

    <div class="middle-headings header-grey">
        <h1>Shopping
            <span> Blogs</span>
        </h1>
    </div>
    <p>Get a peak at what's hip &amp; happening in the retail space</p>

    <div class="row-fluid">

        <ul class="thumbnails">
            <li class="span3">
                <div class="overlay-wrapper">
                    <a href="https://www.mallguide.co.za/technology/" target="_blank">
                        <img src="{{ asset('img/technology_mallguide.png') }}" alt="Mallguide Technology Blog" class="overlay-image" data-overlaytext="Technology Blog"/>
                    </a>
                    <div class="overlay"></div>
                </div>

                <h4 class="hborder">Technology</h4>

                Mallguide tech is the epitome platform for all things technology. From gadgets,new technology,apps to technology business start ups.
                <a href="https://www.mallguide.co.za/technology/" target="_blank">www.mallguide.co.za/technology</a>

            </li>
            <!-- span3 -->

            <li class="span3">
                <div class="overlay-wrapper">
                    <a href="https://www.mallguide.co.za/travel/" target="_blank">
                        <img src="{{ asset('img/travel_mallguide.png') }}" alt="Mallguide Travel Blog" class="overlay-image" data-overlaytext="Travel Blog"/>
                    </a>
                    <div class="overlay"></div>
                </div>
                <h4 class="hborder">Travel</h4>

                Mallguide travel has been established and tailored to highlight travel and tourism issues as well as expose top travel destinations around the world.
                <a href="https://www.mallguide.co.za/travel/" target="_blank">www.mallguide.co.za/travel</a>
            </li>
            <!-- span3 -->

            <li class="span3">
                <div class="overlay-wrapper">
                    <a href="https://www.mallguide.co.za/blog/" target="_blank">
                        <img src="{{ asset('img/blog_mallguide.png') }}" alt="Mallguide Music and Lifestyle Blog" class="overlay-image" data-overlaytext="Music and Lifestyle Blog"/>
                    </a>
                    <div class="overlay"></div>
                </div>
                <h4 class="hborder">Music and Lifestyle</h4>

                Mallguide blog is the home of retail lifestyle,technological developments as well as social and business events around South Africa.
                <a href="https://www.mallguide.co.za/blog/" target="_blank">www.mallguide.co.za/blog</a>
            </li>
            <!-- span3 -->

            <li class="span3">
                <div class="overlay-wrapper">
                    <a href="https://www.mallguide.co.za/finance/" target="_blank">
                        <img src="{{ asset('img/blog_finance.jpg') }}" alt="Mallguide Finance Blog" class="overlay-image" data-overlaytext="Finance Blog"/>
                    </a>
                    <div class="overlay"></div>
                </div>
                <h4 class="hborder">Finance</h4>

                Interested in the topic of Finance? Let our local Finance Guru's show you around the in's &amp; out's of finding finance.
                <a href="https://www.mallguide.co.za/finance/" target="_blank">www.mallguide.co.za/finance</a>
            </li>
            <!-- span3 -->
        </ul>
    </div>
    <!-- row-fluid -->
    <!-- Magnific popup was not working in the exScript section so we moved the javascript inside the page-->
    <script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.click-to-view').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: true,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function(element) {
                        return element.find('img');
                    }
                }
            });

            $('#myCarousel1,#myCarousel3').carousel({

                interval: 5000
            });

            $('#zoom-gallery-mall #imageCarousel').carousel({
                interval: 5000
            });

            $('#zoom-gallery-mall').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: true,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function(element) {
                        return element.find('img');
                    }
                }
            });

            $('#zoom-gallery-shops').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: true,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function(element) {
                        return element.find('img');
                    }
                }
            });

            $('#zoom-gallery-comps').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: true,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function(element) {
                        return element.find('img');
                    }
                }
            });

            $('#zoom-gallery-events').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: true,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function(element) {
                        return element.find('img');
                    }
                }
            });

            $('#zoom-gallery-promos').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: true,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function(element) {
                        return element.find('img');
                    }
                }
            });





        });
    </script>
@stop

@section('exScript')

@stop
