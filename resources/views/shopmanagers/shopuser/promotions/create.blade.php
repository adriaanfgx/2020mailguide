@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Add promotion </h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                {{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="blog-content container-fluid">
    <div class="row-fluid">
        <div class="biggerLeftMargin">
            <h4 class="dotted-border">Add a promotion</h4>

            Get the exposure your promotion deserves, list it for free on Mallguide...
            <h4 class="dotted-border">How Does It Work ?</h4>
            <ul>
                <li>You add a new promotion online</li>
                <li>Your Mall's Marketing team are notified of this submission</li>
                <li>The submission is verified and approved by your Mall's Marketing team</li>
                <li>Your promotion is automatically added to Mallguide</li>
                <li>Your promotion gets lots of exposure, and is a great success!</li>
            </ul>

            {{ Form::open(array('route' => 'promotions.postCreate', 'method' => 'post','files'=>true,'name' => 'postCreateStorePromotion','id' => 'postCreateStorePromotion', 'class' => 'form-inline')) }}
            {{ Form::hidden('mallID',null,array('id'=>'mall')) }}
            {{ Form::hidden('referer',URL::previous()) }}
            <div class="row" style="padding-left: 30px;">
                <div class="span4">
                    <div class="control-group">
                        <label for="shop"><strong>Shop</strong></label>
                        <div class="controls">
                            {{ Form::text('',$shop->name,array('id'=>'shopID','readonly'=>'readonly')) }}
                        </div>
                    </div>
                </div>
            </div>
            @include('partials/memberzone/promotionfields')
            <button class="submit reg-btn">Submit Details</button>
            {{ Form::close() }}

        </div>
    </div>
</div>


@stop
@section('exScript')
<script>
    $(document).ready(function(){
        $("#shopID").change(function(){

            var shop = $(this).val();
            $.ajax({
                type: 'GET',
                url: '/store/mall/'+shop,
                success: function(data){
                    $("#mall").val(data);
//                    console.log(data);
                }
            });

        });
        $('.datetimepicker4').datetimepicker({
            pickTime: false
        }).on('changeDate', function(e){
                $(this).datetimepicker('hide');
            });
    });
</script>
@stop
