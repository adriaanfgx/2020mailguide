@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Promotion Listing</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/store">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Promotions</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div><!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">

    <div class="span12 pull-left">
        <h4 class="hborder">Promotion Listings</h4>

    </div>
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>

    </div>  <!-- span6 -->


    <div class="row-fluid">
        <div class="span12">
            <table class="table table-border">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Promotion</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th colspan="3">Actions</th>
                </tr>
                </thead>
                <tbody id="promotion_table">
                @if(sizeof($promotions) > 0 )
                @foreach($promotions as $promo)
                <tr>
                    <td>{{ $promo->shop->name }}</td>
                    <td>{{ Str::limit($promo->promotion, 50) }}</td>
                    <td>{{ $promo->startDate }}</td>
                    <td>{{ $promo->endDate }}</td>
                    <td colspan="3">
                        <a href="{{ route('store.editPromotion',$promo->promotionsMGID) }}" class="btn btn-info btn-mini"><i class="fa fa-pencil"></i>Edit</a>&nbsp;|&nbsp;
                        <a href="#" id="{{ $promo->promotionsMGID }}" class="btn btn-info btn-mini btn_copy"><i class="fa fa-eye">Copy</i></a>&nbsp;|&nbsp;
                        <a class="btn btn-danger btn-mini action_delete" href="#" id="{{ $promo->promotionsMGID }}"><i class="fa fa-trash-o"></i>Delete</a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="7">
                        There are no listed promotions .....
                    </td>
                </tr>
                @endif
                </tbody>
            </table>

        </div><!--span12 -->
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $(".action_delete").click(function(e){

            e.preventDefault();
            var promotion = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: '/promotions/delete/'+promotion,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });


        });

        $(".btn_copy").click(function(e){

            e.preventDefault();
            var promotion = $(this).attr("id");

            bootbox.confirm("Are you sure you want to copy promotion ? ");
            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: '/promotions/copy/'+promotion,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });


        });
    });
</script>
@stop

