@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Edit Gift Idea </h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <?php $activeUser = Session::get('mgUser'); ?>
            <div id="breadcrumbs" class="span6">
                {{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="blog-content container-fluid">
    <div class="row-fluid">
        <div class="biggerLeftMargin">
            <h4 class="dotted-border">Edit  Gift Idea</h4>
            {{ Form::model($giftIdea, array('route' => array('store.postEditStoreGiftIdea', $giftIdea->giftIdeaID),'method' => 'post','files'=>true ,'name' => 'postEditStoreGiftIdea', 'id' => 'postEditStoreGiftIdea', 'class' => 'form-inline')) }}
            @include('partials/memberzone/giftideaupdatefields')
            <button class="submit reg-btn">Submit Details</button>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop
@section('exScript')
<script>
    $(document).ready(function(){

    });
</script>
@stop
