@extends('layouts.backpages')

@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Gift Ideas Listing</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                {{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="container-fluid">
    <div class="container-fluid biggerPadding blog-content">
        <div class="clearfix"></div>
        <div class="row-fluid">
            <div class="span12">
                <h4 class="hborder">Gift Ideas Listings</h4>

                <table class="table table-border">
                    <thead>
                    <tr class="text-left">
                        <th>Shop</th>
                        <th>Title</th>
                        <th>Price</th>
                        <th>Date Added</th>
                        <th>Display</th>
                        <th width="70">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(sizeof($giftideas) > 0 )
                    @foreach($giftideas as $idea)
                    <tr>
                        <td><a href="{{ route('store.editStoreGiftIdea',$idea->giftIdeaID) }}" title="Edit">{{ $idea->name }}</a></td>
                        <td>{{ $idea->title }}</td>
                        <td>{{ $idea->price }}</td>
                        <td>{{ $idea->dateAdded }}</td>
                        <?php
                        $displayIcon = '';
                        $ideaDisplay = '';
                        if($idea->display == 'Y'){
                            $ideaDisplay = 'btn-success';
                            $displayIcon = 'fa-thumbs-up';
                        }else{
                            $ideaDisplay = 'btn-danger';
                            $displayIcon = 'fa-thumbs-down';
                        }
                        ?>
                        <td>
                            <a href="#" class="btn btn-mini {{ $ideaDisplay }} idea-display" id="{{ $idea->giftIdeaID }}" data-id="{{ $idea->display }}"><i class="fa {{ $displayIcon }}"></i> {{ displayText($idea->display) }}</a>
                        </td>
                        <td width="70">
                            <a href="{{ route('store.editStoreGiftIdea',$idea->giftIdeaID) }}" class="btn btn-info btn-mini" title="Edit"><i class="fa fa-pencil"></i></a>
                            <a class="btn btn-danger btn-mini action_delete" href="#" id="{{ $idea->giftIdeaID }}" title="Delete"><i class="fa fa-trash-o"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="7">
                            There are no listed gift ideas .....
                        </td>
                    </tr>
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $(".action_delete").click(function(e){

            e.preventDefault();
            var giftidea = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: '/store/deletegiftidea/'+giftidea,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });
        });

        $(".idea-display").click(function(){

            var ideaToUpdate = $(this).attr('id');
            var currentStatus = $(this).attr('data-id');
            var newStatus = '';
            if(currentStatus == 'Y'){
                newStatus = 'N';
            }else{
                newStatus = 'Y';
            }

            bootbox.confirm("Are you sure you want to change shop display status ? ");

            $(".modal a.btn-primary").click(function () {
                $.ajax({
                    type: "POST",
                    url: '/store/giftidea/updatedisplay/'+ideaToUpdate+'/'+newStatus,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function () {
                        location.reload();
                    }
                });
            });

        });
    });
</script>
@stop

