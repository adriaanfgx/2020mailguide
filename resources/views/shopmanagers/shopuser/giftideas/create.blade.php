@extends('layouts.backpages')
@section('seo_meta')
    <meta name="description"
          content="">
    <meta name="keywords" content="">
@stop
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Add Gift Idea</h1>
                    <p class="animated fadeInDown delay2">Mall shop</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')

@stop

@section('content')

    <div class="blog-content container-fluid">
        <div class="row-fluid">
            <div class="biggerLeftMargin">
                {{ Form::open(array('route' => 'store.postAddGiftIdea', 'method' => 'post','files'=> true,'name' => 'shops.postAddGiftIdea','id' => 'postAddGiftIdea', 'class' => 'form-inline')) }}
                {{ Form::hidden('mallID',Session::get('activeMallID')) }}
                {{ Form::hidden('shopMGID',$shop, ['id'=>'mall']) }}
                @include('partials/memberzone/giftideaheader')
                <h4 class="dotted-border">Gift Idea Details</h4>
                <br/>
                @include('partials/memberzone/giftideafields')
                <button class="submit reg-btn" id="add_review">Submit Details</button>
                {{ Form::close() }}
            </div>
        </div>
        <!--row-fluid -->

        @stop

        @section('exScript')
            <script>
                $(document).ready(function () {
                    $("#malls").val($("#multi-malls").val());
                    $("#deselect_all").click(function () {

                        $(".multi-mall").select2('val', '');
                    });
                    $("#multi-malls").change(function () {
                        var malls = new Array();
                        malls.push($(this).val());
                        $("#malls").val(malls);
                    });
                    $(".multi-mall").select2({});
                });
            </script>
@stop
