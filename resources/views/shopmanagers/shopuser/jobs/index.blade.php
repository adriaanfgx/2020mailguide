@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Jobs Listing</h1>
                    <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp;
                        Communication</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')
@stop

@section('content')
    <div class="row-fluid">
        <div class="blog-content">
            <h4 class="hborder">Jobs Listings</h4>
            <table class="table table-border">
                <thead>
                <tr>
                    <th>Shop Name</th>
                    <th>Job Title</th>
                    <th>Closing Date</th>
                    <th>Show Contact Info</th>
                    <th>Display</th>
                    <th>Applicants</th>
                    <th colspan="2">Actions</th>
                </tr>
                </thead>
                <tbody id="promotion_table">
                @if(sizeof($jobs) > 0 )

                    @foreach($jobs as $job)
                        <tr>
                            <td>{{ $job->shop->name }}</td>
                            <td>{{ $job->title }}</td>
                            <td>{{ $job->endDate }}</td>
                            <td>{{ displayText($job->showInfo) }}</td>
                            <td>{{ displayText($job->display) }}</td>
                            <td>@if($job->applicants != null) {{ $job->applicants->count() }} @else 0 @endif</td>
                            @if( $job->applicants != null )
                                <a href="{{ route('chainshop.getjobapplicants',$job->jobID) }}"
                                   class="btn btn-info btn-mini" style="float:right;margin-top:1px;"><i
                                        class="fa fa-eye">View</i></a>
                            @endif
                            <td colspan="2">
                                <a href="{{ route('store.editStoreJob',$job->jobID) }}" class="btn btn-info btn-mini"><i
                                        class="fa fa-pencil"></i> Edit</a>&nbsp; | &nbsp;
                                <a class="btn btn-danger btn-mini action_delete"
                                   href="{{ route('jobs.delete',$job->jobID) }}" id="{{ $job->jobID }}"><i
                                        class="fa fa-trash-o"></i> Delete</a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7">
                            There are no listed jobs .....
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
@stop
@section('exScript')
    <script src="{{ asset('js/bootbox.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $(".action_delete").click(function (e) {

                e.preventDefault();
                var job = $(this).attr("id");

                bootbox.confirm("Are you sure you want to delete this entry ? ");

                $(".modal a.btn-primary").click(function () {
                    $.ajax({
                        type: "POST",
                        url: '/jobs/delete/' + job,
                        data: {
                            '_token': "{{ csrf_token() }}"
                        },
                        success: function () {
                            location.reload();
                        }
                    });
                });
            });
        });
    </script>
@stop

