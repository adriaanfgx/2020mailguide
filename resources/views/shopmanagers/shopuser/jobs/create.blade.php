@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Edit Job </h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                {{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="blog-content container-fluid">
    <div class="row-fluid">
        <div class="biggerLeftMargin">
            <?php
                $shop = Session::get('activeShopID');
            ?>
            {{ Form::open(array('route' => 'store.postCreateStoreJob', 'method' => 'post','name' => 'postCreateStoreJob','id' => 'postCreateStoreJob', 'class' => 'form-inline')) }}
            {{ Form::hidden('shopMGID',Session::get('activeShopID')) }}
            <h4 class="dotted-border">Shops - Add A Job</h4>
            Looking for the best staff to fill your available positions?<br />
            Let Mallguide assist you by matching our database of job seekers to your location and required skills. Have the pick of the crop with this free online service...
            <h4 class="dotted-border">How Does It Work?</h4>
            <ul>
                <li>You add a new job online </li>
                <li>Job seekers register with Mallguide, and upload their CV's</li>
                <li>The job seekers browse the list of available positions and apply online </li>
                <li>You are notified of applications</li>
                <li>You screen and approve applicants based on their submissions and CV's </li>
                <li>Approved applicants are sent your direct contact details for an interview </li>
                <li>Your store prospers under fine management and capable staff! </li>
            </ul>

            <h6>Job details</h6>

            <div class="row" style="padding-left: 30px;">
                <div class="span4">
                    <div class="control-group {{ ($errors->has('title') ? 'error' : '') }}">
                        <label for="title" class="control-label"><strong>Position*</strong></label>
                        <div class="controls">
                            {{ Form::text('title',null) }}

                        </div>
                        <span class="help-block">{{ ($errors->has('title') ? $errors->first('title') : '') }}</span>
                    </div>

                </div>
                <div class="span4">
                    <div class="control-group {{ ($errors->has('salary') ? 'error' : '') }}">
                        <label for="salary" class="control-label"><strong>Salary</strong></label>
                        <div class="controls">
                            {{ Form::text('salary',null) }}
                            <span class="help-block">{{ ($errors->has('salary') ? $errors->first('salary') : '') }}</span>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row" style="padding-left: 30px;">
                <div class="span4">
                    <div class="input-append datetimepicker4">
                        <div class="control-group {{ ($errors->has('startDate') ? 'error' : '') }}">
                            <label class="control-label" for="startDate"><strong>Starting Date</strong></label>
                            <div class="controls">
                                {{ Form::text('startDate',null,array('data-format'=>'yyyy-MM-dd','class'=>'span12','readonly')) }}
                                <span class="add-on">
                                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                  </i>
                                </span>
                                {{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}
                            </div>
                        </div>
                    </div>

                </div>
                <div class="span4">
                    <div class="control-group">
                        <label for="refNum"><strong>Ref. No</strong></label>
                        <div class="controls">
                            {{ Form::text('refNum',null) }}
                        </div>
                    </div>

                </div>
            </div>
            <div class="row" style="padding-left: 30px;">
                <div class="span4">
                    <div class="input-append datetimepicker4">
                        <div class="control-group {{ ($errors->has('endDate') ? 'error' : '') }}">
                            <label class="control-label" for="endDate"><strong>Closing Date*</strong></label>

                            <div class="controls">
                                {{ Form::text('endDate',null,array('data-format'=>'yyyy-MM-dd','class'=>'span12','readonly')) }}
                                <span class="add-on">
                                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                  </i>
                                </span>
                                {{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="span4">&nbsp;</div>
            </div>
            <div class="clearfix"></div>
            <div class="row" style="padding-left: 30px;">
                <div class="span4">
                    <div class="control-group {{ ($errors->has('description') ? 'error' : '') }}">
                        <label for="description"><strong>Description*</strong></label>
                        <div class="controls">
                            {{ Form::textarea('description',null,array('rows'=>3)) }}
                        </div>
                        <div class="help-block">{{ ($errors->has('description') ? $errors->first('description') : '') }}</div>
                    </div>
                </div>
            </div>
            <h4 class="dotted-border">Contact Details</h4>
            <div class="row" style="padding-left: 30px;">
                <div class="span4">
                    <div class="control-group">
                        <label for="contactPerson"><strong>Contact Person</strong></label>
                        <div class="controls">
                            {{ Form::text('contactPerson',null) }}
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="control-group">
                        <label for="cell"><strong>Cell</strong></label>
                        <div class="controls">
                            {{ Form::text('cell',null) }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="padding-left: 30px;">
                <div class="span4">
                    <div class="control-group">
                        <label for="telephone"><strong>Telephone</strong></label>
                        <div class="controls">
                            {{ Form::text('telephone',null) }}
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="control-group">
                        <label for="fax"><strong>Fax</strong></label>
                        <div class="controls">
                            {{ Form::text('fax',null) }}
                        </div>
                    </div>
                </div>
            </div>

            <div class="row" style="padding-left: 30px;">
                <div class="span4">
                    <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}">
                        <label for="email" class="help-block">Email</label>
                        <div class="controls">
                            {{ Form::text('email',null) }}
                            <span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
                        </div>
                    </div>
                </div>
                <div class="span4">
                    &nbsp;
                </div>
            </div>
            <div class="row" style="padding-left: 30px;">
                <div class="control-group">
                    <label for="showInfo"><strong>Must shop name and contact details for job be displayed?</strong></label>
                    <div class="controls">
                        Yes&nbsp;{{ Form::radio('showInfo','Y',true) }}&nbsp;&nbsp;No&nbsp;{{ Form::radio('showInfo','N',null) }}
                    </div>
                </div>
            </div>
            <button class="submit reg-btn">Submit Details</button>
            {{ Form::close() }}
        </div>
    </div>
</div>

@stop
@section('exScript')
<script>
    $(document).ready(function(){
        $('.datetimepicker4').datetimepicker({
            pickTime: false
        }).on('changeDate', function(e){
                $(this).datetimepicker('hide');
            });
    });
</script>
@stop
