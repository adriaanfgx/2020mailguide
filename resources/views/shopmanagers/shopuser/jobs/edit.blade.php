@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Edit Job </h1>
                <p class="animated fadeInDown delay2">Edit your Shop Job</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                {{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="blog-content container-fluid">
    <div class="row-fluid">
        <div class="biggerLeftMargin">
            <h4 class="dotted-border">Edit Job Information</h4>

            {{ Form::model($job, array('route' => array('store.postEditStoreJob', $job->jobID),'method' => 'post','name' => 'postEditStoreJob', 'id' => 'postEditStoreJob', 'class' => 'form-inline')) }}
            @include('partials/memberzone/jobfields')
            <label for="display"><strong>Must job be displayed?</strong></label>
            <div class="row" style="padding-left: 30px;">
                <div class="span2">
                    <div class="control-group">
                        <div class="controls">
                            <label for="display" class="pull-left">Yes</label>&nbsp; &nbsp;
                            {{ Form::radio('display','Y') }}
                        </div>
                    </div>
                </div>

                <div class="span2">
                    <div class="control-group">
                        <div class="controls">
                            <label for="display" class="pull-left">No</label>&nbsp; &nbsp;
                            {{ Form::radio('display','N') }}
                        </div>
                    </div>
                </div>
            </div>
            <button class="submit reg-btn">Submit Details</button>
            {{ Form::close() }}
        </div>
    </div>
</div>
@stop
@section('exScript')
<script>
    $(document).ready(function(){
        $('.datetimepicker4').datetimepicker({
            pickTime: false
        }).on('changeDate', function(e){
                $(this).datetimepicker('hide');
            });
    });
</script>
@stop
