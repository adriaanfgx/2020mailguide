@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('content')
    <div class="blog-content container-fluid">
        <div class="row-fluid">
            <div class="biggerLeftMargin">
                <h4 class="dotted-border">Add a user</h4>
                <?php
                $shop = Session::get('activeShopID');
                $chainChain = Session::get('activeChain');
                ?>
                {{ Form::open(array('route' => 'portal.postAddmalluser', 'method' => 'post','name' => 'addMallUser','id' =>'addMallUser', 'class' => 'form-inline shopCreate')) }}
                @if( isset($shop) )
                    {{ Form::hidden('admin_level','Shop Manager') }}
                    {{ Form::hidden('shopMGID',$shop) }}
                @elseif( isset($chainChain) )
                    {{ Form::hidden('admin_level','Chain Manager') }}
                    {{ Form::hidden('chain_name',$chainChain) }}
                @endif
                @include('partials/memberzone/adduserfields')
                <div class="row-fluid">
                    <div class="span4 offset2">
                        <div class="control-group">
                            <div class="controls">
                                <button class="submit reg-btn" id="update_user">Submit Details</button>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section('exScript')

    <script type="text/javascript">
        $(document).ready(function () {

            $(".numeric").mask('27b99999999');

            $('.datetimepicker4').datetimepicker({
                pickTime: false
            });
            $('#password').keyup(function () {
                var _parent = $(this).parent().parent();

                var level = 0;
                var mesg = '';
                var input = $(this).val();

                if (/(.*[A-Z])/.test(input) == false) {
                    level = 1;
                    mesg += ' No uppercase letters.<br/>';

                }

                if (/(.*[\!|\@|\#|\$|\_])/.test(input) == false) {
                    level = 2;
                    mesg += ' No special character (!@#$_).<br/>';

                }
                if (/(.*[\d])/.test(input) == false) {
                    level = 3;
                    mesg += ' No numbers.<br/>';

                }
                if (/(.*[a-z])/.test(input) == false) {
                    level = 4;
                    mesg += ' No lowercase letters.<br/>';
                }

                if (input.length < 6) {
                    level = 5;
                    mesg += ' Not long enough.<br/>';
                }

                $('#password_help').html(mesg);

                if (level != 0) {
                    _parent.addClass('error');

                    $('#sub').attr('disabled', 'disabled');
                } else {
                    $('#sub').removeAttr('disabled');
                }
            });
        });
    </script>
@stop

