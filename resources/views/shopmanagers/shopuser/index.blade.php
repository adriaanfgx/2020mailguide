@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Logged In </h1>

                <p class="animated fadeInDown delay2">Shop Member</p>
            </div>
            <!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a> <span class="divider">/</span></li>
                    <li class="active">Memberzone</li>
                </ul>
            </div>
            <!--/span6-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="span12 middle-headings dotted-border white-bg">
    @if( sizeof($shop) ==  1)
    <h2><a href="#">{{ $shop->name }} -  {{ $mall->name }}</a></h2>
    @endif
    <div class="span6 pull-left">Hi {{ $user->first_name }}<br/> Listed on this page is a host of link shortcuts that
        will help you make the most of your listing on Mallguide. Please make use of our range of <strong>free services</strong>
        and tools at your disposal to help market and promote your mall to all of South Africa.
    </div>
</div><!--/span12-->

<div class="row-fluid">
    <div class="span7">
        <h4 class="dotted-border">Your Quick Stats</h4>
        <div class="yellow">
            <ul>
                <li>{{ $promotions }} &nbsp; Promotions are currently displayed. <a href="{{ route('store.createStorePromption') }}">Add a new promotion</a>  or <a href="{{ route('store.promotions') }}">manage existing ones</a></li>
                <li>{{ $jobs }} &nbsp; Jobs are currently available. <a href="{{ route('store.createStoreJob') }}">List existing jobs</a> or <a href="{{ route('store.createStoreJob') }}">add a new one</a>.</li>
            </ul>
        </div>
        <!--yellow -->
        <h4 class="dotted-border">CMS Ignition (Shopper Communication Module)</h4>
        In order to make use of the CMS Ignition Communication features you will need to have access to the following modules:<br />
        <h6>Members</h6>
        You'll be able to view, add, edit or delete members' information.
        Members' Information can be downloaded as an Excel document.
        You can create various Member Groups and a member may belong to various groups.
        Custom Member fields can be defined in order to cater for each individual group.
        Existing Clientele can be uploaded and with the Communication Module enabled you will be able to communicate directly to "All Members, or a Group".
        <br />
        <h6>Communication Module</h6>
        The Communication Module works in tandem with the Member Module.
        Once you've loaded all your Members and purchase Communication Credits, you'll be able to SMS or send Personalised Emails to your database.
        Email templates will be designed and loaded by Finegrafix and from thereon you'll be able to compose your own Monthly,
        NewsFlash or New Product Newsletter and send it to your database with a couple of easy mouse clicks.
        <br />
        <h6>E-mail Templates</h6>
        These are ordered when you activate your Communication Module.
        There is no limit to how many Email templates we can design for you.
        These templates will form the basis of all Email Communication you send via your website.
        <br />
        <h6>Find out more about CMS Ignition...</h6>
        f you would like to get access to CMS Ignition or find out more about what we have to offer please <a href="mailto:info.co.za?subject=Mallguide user request regarding CMS Ignition Communication module">contact us.</a>
    </div>
    <!-- span6 -->

    <div class="span5">
        <h4>Your Control Panel</h4>

        <div class="control-panel">
            <h4><span class="control-ico-about">About You</span></h4>
            <span>
                We need these details for contact purposes, otherwise they are kept confidential. Your profile listing:
            </span>
            <br />
            <br />
            <div class="row" style="padding-left: 35px;">
                <div class="row" style="padding-left: 35px;">
                    <div class="span4"><label><strong>Name</strong></label></div>
                    <div class="span4">{{ $user->first_name }}</div>
                </div>
            </div>
            <div class="row" style="padding-left: 35px;">
                <div class="row" style="padding-left: 35px;">
                    <div class="span4"><label><strong>Surname</strong></label></div>
                    <div class="span4">{{ $user->last_name }}</div>
                </div>
            </div>
            <div class="row" style="padding-left: 35px;">
                <div class="row" style="padding-left: 35px;">
                    <div class="span4"><label><strong>Email</strong></label></div>
                    <div class="span4">{{ $user->email }}</div>
                </div>
            </div>
            <div class="row" style="padding-left: 35px;">
                <div class="row" style="padding-left: 35px;">
                    <div class="span4"><label><strong>Company</strong></label></div>
                    <div class="span4">{{ $user->company }}</div>
                </div>
            </div>
            <ul>
                <li><a href="{{ route('store.editprofile',$user->id) }}">Update your personal information</a></li>
                <li><a href="{{ route('store.updatepassword',$user->id) }}">Update your password</a></li>
            </ul>

            <!-- Mall And User update section -->

            <h4><span class="control-ico-links">Quick Links</span></h4>
            <ul>
                @if( sizeof($shop) ==  1 )
                <li>
                    <a href="{{ route('store.update',$shop->shopMGID) }}">Update Shop Details</a>&nbsp;
                </li>
                @else
                <li>
                    <a href="{{ route('store.shoplist') }}">View your stores</a>&nbsp;
                </li>
                @endif
                <li>
                    <a href="{{ route('store.adduser') }}">Add a User </a>&nbsp; | &nbsp;
                    <a href="{{ route('store.userlist') }}"> List Users</a>
                </li>
                <li>
                    <a href="{{ route('store.createcompetition') }}">Add a Competition</a>&nbsp; | &nbsp;
                    <a href="{{ route('store.competitions') }}">List Competitions</a>
                </li>
                <li>
                    <a href="{{ route('store.createStoreJob') }}">Add a Job</a>&nbsp; | &nbsp;
                    <a href="{{ route('store.jobs') }}">List Jobs</a>
                </li>
                <li>
                    <a href="{{ route('store.createStorePromption') }}">Add a Promotion</a>&nbsp; | &nbsp;
                    <a href="{{ route('store.promotions') }}">List Promotions</a>
                </li>
                <li>
                    <a href="{{ route('store.addgiftidea') }}">Add a Gift Idea</a>&nbsp; | &nbsp;
                    <a href="{{ route('store.giftideas') }}">List Gift Ideas</a>
                </li>
            </ul>

            <div>
                <h4><span class="control-ico-posts">Information posted by Mallguide</span></h4>
                <ul>
                    <li><a href="{{ route('store.marketinginfo') }}">Want to send an electronic Newsletter to your
                            Shopper/Client Database?</a></li>
                    <li><a href="{{ route('store.compterms') }}">Standard Terms and Conditions for Competitions
                            and Adding Entrants into Databases for further Communications</a></li>
                </ul>
            </div>
        </div>
        <!-- control panel -->
    </div>
    <!--span6 -->
</div><!--row -->


@stop
