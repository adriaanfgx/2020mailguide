@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>List stores </h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <?php
            $activeUser = Sentry::getUser();
            ?>
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/store">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">List stores</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid">
    <!--    <div class="span4">-->
    <h4 class="dotted-border">Your Stores</h4>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Name</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        @foreach( $userShops as $shop )
        <tr>
            <td>{{ $shop->name }}</td>
            <td>
                <a href="{{ route('store.update',$shop->shopMGID) }}" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@stop

@section('exScript')
<script>

    $(document).ready(function(){

        $(".action_delete").click(function(){

            var $user = $(this).attr('id');

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $.ajax({
                type: "POST",
                url: '/managerdeleteuser/'+$user,
                data: {
                    '_token': "{{ csrf_token() }}"
                },
                success: function(){
                    location.reload();
                }
            });
//            managerdeleteuser
        });

        $(".send_activation").click(function(){

            var $user = $(this).attr('id');

            $.ajax({
                type: "POST",
                url: '/resendactivation/'+ $user,
                data: {
                    '_token': "{{ csrf_token() }}"
                },
                success: function(){
                    location.reload();
                }
            });
//            alert($user);

        });
    });

</script>
@stop
