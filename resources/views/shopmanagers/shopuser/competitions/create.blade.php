@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Add competition </h1>
                <p class="animated fadeInDown delay2">Manage your Mall, Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/store">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Add competition</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')

<h4 class="dotted-border">Build your own Competition Form</h4>
<div class="row-fluid">

    <div>
        {{ Form::open(array('route' => 'store.postCreateStoreCompetition', 'method' => 'post','files'=>true,'name' => 'postCreateStoreCompetition','id' => 'postCreateStoreCompetition', 'class' => 'form-inline')) }}
        <div class="row" style="padding-left: 30px;">
            <div class="span4">
                <div class="control-group">
                    <label for="mallID"><strong>Select a store</strong></label>
                    <div class="controls">
                        {{ Form::select('shopMGID',$shops,null) }}
                    </div>
                </div>
            </div>
        </div>
        @include('partials/memberzone/competitionfields')
        <button class="submit reg-btn">Submit Details</button>
    </div>
    {{ Form::close() }}
</div><!--row-fluid -->
@stop
@section('exScript')
<script>
    $(document).ready(function(){
        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });

        $(".multi-mall").select2({});
    });
</script>
@stop
