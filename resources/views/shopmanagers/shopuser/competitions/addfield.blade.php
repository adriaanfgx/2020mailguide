@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Add form field</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/store">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Add form field</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')

<h4 class="dotted-border">Add form field</h4>
<div class="row-fluid">

    <div>
        {{ Form::open(array('route' => 'chainshop.postAddField', 'method' => 'post','name' => 'postAddField','id' => 'postAddField', 'class' => 'form-inline')) }}
        {{ Form::hidden('formID',$formID) }}
        <div class="row" style="padding-left: 30px;">
            <div class="span4">
                <div class="control-group {{ ($errors->has('name') ? 'error' : '') }}">
                    <label for="name" class="control-label"><strong>Field Name</strong></label><br />
                    <span>(Mandatory - max. 250 characters)</span>
                    <div class="controls">
                        {{ Form::text('name',null,array('class'=>'txtbar')) }}
                        <span class="help-block">{{ ($errors->has('name') ? $errors->first('name') : '') }}</span>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="mandatory"><strong>Is the Field Mandatory?</strong></label><br />
                    <span>(If Yes, form validation will be added)</span>
                    <div class="controls">
                        Yes{{ Form::radio('mandatory','Y') }}&nbsp;&nbsp;No{{ Form::radio('mandatory','N') }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-left: 30px;">
            <div class="span4">
                <div class="control-group">
                    <label for="typeOfField"><strong>Type of Field</strong></label>
                    <div class="controls">
                        {{ Form::select('typeOfField',$typeOfField,null,array('id'=>'typeOfField')) }}
                    </div>
                </div>
            </div>
            <div class="span4">&nbsp;</div>
        </div>
        <div class="row" style="padding-left: 30px;" id="textarea_props">
            <div class="span2">
                <div class="control-group">
                    <label for="rows"><strong>Number of Rows</strong></label>
                    <div class="controls">
                        {{ Form::select('rows',$rows,null,array('id'=>'rows')) }}
                    </div>
                </div>
            </div>
            <div class="span2">
                <div class="control-group">
                    <label for="cols"><strong>Number of Columns</strong></label>
                    <div class="controls">
                        {{ Form::select('cols',$cols,null,array('id'=>'cols')) }}
                    </div>
                </div>
            </div>
            <div class="span2">
                <div class="control-group">
                    <label for="wrap"><strong>Word Wrapping</strong></label>
                    <div class="controls">
                        {{ Form::select('wrap',$wrap,null,array('id'=>'wrap')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-left: 30px;" id="radio_btn_props">
            <div class="span2">
                <div class="control-group">
                    <label for="numRadioOptions"><strong>Number of Radio Button Options</strong></label>
                    <div class="controls">
                        {{ Form::select('numRadioOptions',$numRadioOptions,null,array('id'=>'numRadioOptions')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-left: 30px;" id="check_box_props">
            <div class="span2">
                <div class="control-group">
                    <label for="numCheckboxOptions"><strong>Number of Checkbox Options</strong></label>
                    <div class="controls">
                        {{ Form::select('numCheckboxOptions',$numCheckboxOptions,null,array('id'=>'numCheckboxOptions')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-left: 30px;" id="select_props">
            <div class="span2">
                <div class="control-group">
                    <label for="numSelectOptions"><strong>Number of Drop Down List Options</strong></label>
                    <div class="controls">
                        {{ Form::select('numSelectOptions',$numSelectOptions,null,array('id'=>'numSelectOptions')) }}
                    </div>
                </div>
            </div>
            <div class="span2">
                <div class="control-group">
                    <label for="multiple"><strong>Allow Multiple Selections</strong></label>
                    <div class="controls">
                        Yes&nbsp;{{ Form::radio('multiple','Y') }}&nbsp;&nbsp;No&nbsp;{{ Form::radio('multiple','N') }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-left: 30px;" id="text_default">
            <div class="span4">
                <div class="control-group">
                    <label for="defaultValue"><strong>Default Value</strong></label>
                    <div class="controls">
                        {{ Form::text('defaultValue',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="size"><strong>Text Field Size:</strong></label>
                    <div class="controls">
                        {{ Form::select('size',$size,null,array('id'=>'size')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-left: 30px;" id="text_max">
            <div class="span4">
                <div class="control-group">
                    <label for="maxlength"><strong>Max Length</strong></label><br />
                    <span>(Maximum length in characters)</span>
                    <div class="controls">
                        {{ Form::text('maxlength',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">&nbsp;</div>
        </div>
        <br />
        <button class="submit reg-btn">Submit Details</button>
    </div>
    {{ Form::close() }}
</div><!--row-fluid -->
@stop
@section('exScript')
<script>
    $(document).ready(function(){

        $("#size").val(200);
        $("#textarea_props").hide();
        $("#radio_btn_props").hide();
        $("#check_box_props").hide();
        $("#select_props").hide();

        $("#typeOfField").change(function(){

            if( $(this).val() == 'text' ){
                $("#text_default").show();
                $("#text_max").show();

                $("#radio_btn_props").hide();
                $("#numRadioOptions").val('');
                $("#textarea_props").hide();
                $("#rows").val('');
                $("#cols").val('');
                $("#wrap").val('');
            }
            else if( $(this).val() == 'textarea' ){

                $("#textarea_props").show();
                $("#rows").val(4);
                $("#cols").val(30);
                $("#wrap").val('soft');
                $("#text_default").hide();
                $("#text_max").hide();
                $("#radio_btn_props").hide();
                $("#numRadioOptions").val('');

            }else if( $(this).val() == 'radio' ){
                $("#radio_btn_props").show();
                $("#numRadioOptions").val(2);

                $("#textarea_props").hide();
                $("#rows").val('');
                $("#cols").val('');
                $("#wrap").val('');
                $("#text_default").hide();
                $("#text_max").hide();
                $("#check_box_props").hide();
                $("#numCheckboxOptions").val('');

            }else if( $(this).val() == 'checkbox' ){
                $("#check_box_props").show();
                $("#numCheckboxOptions").val(2);

                $("#radio_btn_props").hide();
                $("#numRadioOptions").val('');
                $("#textarea_props").hide();
                $("#rows").val('');
                $("#cols").val('');
                $("#wrap").val('');
                $("#text_default").hide();
                $("#text_max").hide();
                $("#select_props").hide();
                $("#numSelectOptions").val('');

            }else if( $(this).val() == 'select' ){

                $("#select_props").show();
                $("#numSelectOptions").val(2);
//                $('[name=multiple]').val('N');

                $("#radio_btn_props").hide();
                $("#numRadioOptions").val('');
                $("#textarea_props").hide();
                $("#rows").val('');
                $("#cols").val('');
                $("#wrap").val('');
                $("#text_default").hide();
                $("#text_max").hide();
            }
        });
    });
</script>
@stop
