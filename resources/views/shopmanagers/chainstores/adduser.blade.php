@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('content')
<div class="row-fluid" >
	<div class="blog-content" >
    <h4 class="dotted-border">Registration Form</h4>

        {{ Form::open(array('route' => 'portal.postAddmalluser', 'method' => 'post','name' => 'addMallUser','id' => 'addMallUser', 'class' => 'form-inline shopCreate')) }}
        <div>
            <div id="user_info">
                <div class="row" style="padding-left: 30px;">
                    <div class="span4">
                        <div class="control-group">
                            <label for="mall" class="control-label"><strong>Chain Store</strong></label>
                            <div class="controls">
                                {{ Form::text('chain_name',$chain_name,array('class'=>'txtbar','readonly')) }}
                                {{ Form::hidden('admin_level','Chain Store') }}
                            </div>
                        </div>
                    </div>
                    <div class="span4"></div>
                </div>

                @include('partials/memberzone/adduserfields')

            </div>
        </div>
        <button class="submit reg-btn" id="update_user">Submit Details</button>
        {{ Form::close() }}
    </div><!--span12 -->
</div>
@stop
@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });
		$('#province').change(function(){
        	$('#city').empty();
                $.ajax({
                    type: "GET",
                    url: '/admin/provinces/cities/'+$(this).val(),
                    data: '',
                    success: function (data) {
                        //console.log(data);
                        $('#city').html('<option value="">Please Select a City..</option>');
                        $.each(data,function(key,val){

                            $('#city').append('<option value="'+key+'">'+val+'</option>');
                        });

                    }
            });
        });
        $('#password').keyup(function() {
            var _parent = $(this).parent().parent();
			var level = 0;
			var mesg = '';
			var input = $(this).val();

			if(/(.*[A-Z])/.test(input) == false)
			{
				level = 1;
				mesg += ' No uppercase letters.<br/>';
			}

			if(/(.*[\!|\@|\#|\$|\_])/.test(input) == false) {
				level = 2;
				mesg += ' No special character (!@#$_).<br/>';
			}
			if(/(.*[\d])/.test(input) == false)
			{
				level = 3;
				mesg += ' No numbers.<br/>';
			}
			if(/(.*[a-z])/.test(input) == false)
			{
				level = 4;
				mesg += ' No lowercase letters.<br/>';
			}

			if(input.length < 6)
			{
				level = 5;
				mesg += ' Not long enough.<br/>';

			}

			$('#password_help').html(mesg);

            if(level != 0){
                _parent.addClass('error');

				$('#sub').attr('disabled','disabled');
            } else {
				$('#sub').removeAttr('disabled');
			}
        });
    });
</script>
@stop

