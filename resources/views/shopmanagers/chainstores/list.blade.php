@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('content')

<div class="row-fluid">
    <div class="blog-content">

		<div class="hborder">
		    <h4 >Chain Stores</h4>

		    <div class="span3 pull-right pull-top">
		        <?php $the_province= isset($province) ? $province : ""  ?>
		        {{ Form::select('province',$provinceList,$the_province,array('id'=>'province')) }}
		    </div>  <!-- span6 -->
	    </div>  <!-- span6 -->

        <table class="table table-border">
            <thead>
            <tr>
                <th>Mall</th>
                <th>Name</th>
                <th>Category</th>
                <th>Sub Category</th>
                <th>Display</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody id="shop_table">
            @foreach($userShops as $shop)
            <tr>
                <td>{{ $shop->name }}</td>
                <td>{{ $shop->shop }}</td>
                <td>{{ $shop->shopCategory? $shop->shopCategory->name:"" }}</td>
                <td>{{ $shop->shopSubCategory? $shop->shopSubCategory->name:"" }}</td>
                <td>{{ displayText($shop->display) }}</td>
                <td>
                    <a href="{{ route('shops.update',$shop->shopMGID) }}" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp;&nbsp;|
                    &nbsp;&nbsp;<a class="btn btn-danger btn-mini action_delete" href="{{ route('chainshop.delete',$shop->shopMGID) }}" id="{{ $shop->shopMGID }}"><i class="fa fa-trash-o"></i> Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>

    </div>
</div>
@stop
@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

        $(".action_delete").click(function(e){
            e.preventDefault();
            var shop = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: 'shops/delete/'+shop,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });
        });


        $("#province").change(function(){

           var province = $(this).val();

            if( province == '' ){

                window.location.href = '/chainstores';
            }else{
                window.location.href = '/chainstores/provincefilter/'+province;
            }

        });
    });
</script>
@stop

