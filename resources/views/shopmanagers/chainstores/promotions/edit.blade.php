@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Edit promotion </h1>
                <p class="animated fadeInDown delay2">Edit your Chain Shop Promotion</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/chainshop">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Edit Promotion</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')


<div class="row-fluid">

    <div>
        {{ Form::model($promotion, array('route' => array('promotions.postUpdate', $promotion->promotionsMGID),'method' => 'post','files'=>true ,'name' => 'postEditPromotion', 'id' => 'postEditPromotion', 'class' => 'form-inline')) }}
        {{ Form::hidden('referer',URL::previous()) }}
        @include('partials/memberzone/promotionfields')
        <button class="submit reg-btn">Submit Details</button>
    </div>
        {{ Form::close() }}
</div><!--row-fluid -->
@stop
@section('exScript')
<script>
    $(document).ready(function(){
        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });
    });
</script>
@stop
