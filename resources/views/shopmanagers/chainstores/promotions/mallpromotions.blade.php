@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Promotion Listing</h1>
                <p class="animated fadeInDown delay2">Manage your Chain Shop Promotions</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
				{{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid">

    <div class="blog-content">

		<div class="hborder">
	        <h4 >Promotion Listings</h4>
		    <div class="span3 pull-right pull-top">
		        <div class="overlay-wrapper"></div>
		            {{ Form::select('malls',$malls,$mallID,array('id'=>'mall')) }}
		    </div>
	    </div>
        <h5>Current Promotions</h5>
        <table class="table table-border">
            <thead>
            <tr>
                <th>Shop Name</th>
                <th>Promotion</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th colspan="3">Actions</th>
            </tr>
            </thead>
            <tbody id="promotion_table">
            @if(sizeof($currentPromotions) > 0 )
            @foreach($currentPromotions as $current)
            <tr>
                <td>{{ $current->name }} - {{ $current->mall }}</td>
                <td>{{ Str::limit($current->promotion, 50) }}</td>
                <td>{{ $current->startDate }}</td>
                <td>{{ $current->endDate }}</td>
                <td colspan="3">
                    <a href="{{ route('chainshop.editpromotion',$current->promotionsMGID) }}" class="btn btn-info btn-mini"><i class="fa fa-pencil"></i>Edit</a>&nbsp;|&nbsp;
                    <a href="#" id="{{ $current->promotionsMGID }}" class="btn btn-info btn-mini btn_copy"><i class="fa fa-eye">Copy</i></a>&nbsp;|&nbsp;
                    <a class="btn btn-danger btn-mini action_delete" href="#" id="{{ $current->promotionsMGID }}"><i class="fa fa-trash-o"></i>Delete</a>
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="7">
                    There are no listed current promotions .....
                </td>
            </tr>
            @endif
            </tbody>
        </table>

        <h5>Past Promotions</h5>

        <table class="table table-border">
            <thead>
            <tr>
                <th>Shop Name</th>
                <th>Promotion</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th colspan="3">Actions</th>
            </tr>
            </thead>
            <tbody id="promotion_table">
            @if(sizeof($pastPromotions) > 0 )
            @foreach($pastPromotions as $past)
            <tr>
                <td>{{ $past->name }} - {{ $past->mall }}</td>
                <td>{{ Str::limit($past->promotion, 50) }}</td>
                <td>{{ $past->startDate }}</td>
                <td>{{ $past->endDate }}</td>
                <td colspan="3">
                    <a href="{{ route('chainshop.editpromotion',$past->promotionsMGID) }}" class="btn btn-info btn-mini"><i class="fa fa-pencil"></i>Edit</a>&nbsp;|&nbsp;
                    <a href="#" id="{{ $past->promotionsMGID }}" class="btn btn-info btn-mini btn_copy"><i class="fa fa-eye">Copy</i></a>&nbsp;|&nbsp;
                    <a class="btn btn-danger btn-mini action_delete" href="#" id="{{ $past->promotionsMGID }}"><i class="fa fa-trash-o"></i>Delete</a>
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td colspan="7">
                    There are no listed past promotions .....
                </td>
            </tr>
            @endif
            </tbody>
        </table>
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $(".action_delete").click(function(e){

            e.preventDefault();
            var promotion = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: '/promotions/delete/'+promotion,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });


        });

        $(".btn_copy").click(function(){

            var promotion = $(this).attr("id");

            $.ajax({
                type: "POST",
                url: '/promotions/copy/'+promotion,
                data: {
                    '_token': "{{ csrf_token() }}"
                },
                success: function(){
                    location.reload();
                }
            });

        });

        $("#mall").change(function(){

            if( $(this).val() == '' ){
                window.location.href = '/chainstores/promotions';
            }else{
                var promotion = $(this).val();
                window.location.href = '/chain/mallpromotions/'+promotion;
            }

        });
    });
</script>
@stop

