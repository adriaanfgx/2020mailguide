@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop


@section('content')
<div class="row-fluid">
	<div class="blog-content">

		<h4 class="dotted-border">Promotion Details</h4>

	        {{ Form::open(array('route' => 'promotions.postCreate', 'method' => 'post','files'=>true,'name' => 'postCreatePromotion','id' => 'postCreatePromotion', 'class' => 'form-inline')) }}
	        {{ Form::hidden('chain_name',$chain_name) }}
	        {{ Form::hidden('referer',URL::previous()) }}
	        <div class="control-group {{ ($errors->has('mallID') ? 'error' : '') }}">
	            <label class="control-label" for="mallID"><strong>Select a mall</strong></label>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
	            <a href="#" id="deselect_all"><strong>Deselect All</strong></a>
	            <div class="controls">
	                {{ Form::select('mallID[]',$malls,$mallIDs,array('class'=>'span10 multi-mall','id'=>'mallID','multiple')) }}
	                <br />
	                <br />
	                <div class="help-block">{{ ($errors->has('mallID') ? $errors->first('mallID') : '') }}</div>
	            </div>
	        </div>
	        <br />

	        @include('partials/memberzone/promotionfields')
	        <button class="submit reg-btn">Submit Details</button>
	    {{ Form::close() }}
	</div><!--row-fluid -->
</div><!--row-fluid -->
@stop
@section('exScript')
<script>
    $(document).ready(function(){
        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });

        $("#deselect_all").click(function(){

            $(".multi-mall").select2('val', '');
        });


        $(".multi-mall").select2({});
    });
</script>
@stop
