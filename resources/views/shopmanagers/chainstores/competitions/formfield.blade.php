@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Edit competition fields</h1>
                <p class="animated fadeInDown delay2">Edit your Shop Competition field</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/chainshop">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Edit competition</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')

<h4 class="dotted-border">Edit Competition Form</h4>
<div class="row-fluid">

    <div>
        {{ Form::model($field, array('route' => array('chainshop.postEditFormField',$field->formFieldID),'method'=>'post','files'=>true ,'name' => 'postEditFormField', 'id' => 'postEditFormField', 'class' => 'form-inline')) }}
        {{ Form::hidden('formID',$formID) }}
            <div class="row" style="padding-left: 30px;">
                <div class="span4">
                    <div class="control-group {{ ($errors->has('name') ? 'error' : '') }}">
                        <label for="name" class="control-label"><strong>Field Name</strong></label><br />
                        <span>(Mandatory - max. 250 characters)</span>
                        <div class="controls">
                            {{ Form::text('name',null,array('class'=>'txtbar')) }}
                            <span class="help-block">{{ ($errors->has('name') ? $errors->first('name') : '') }}</span>
                        </div>
                    </div>

                    <div class="control-group">
                        <label for="typeOfField"><strong>Type Of Field :</strong>&nbsp;&nbsp;  {{ $field->typeOfField }}</label>
                    </div>

                    <div class="control-group">
                        <label for="defaultValue"><strong>Default Value</strong></label>
                        <div class="controls">
                            {{ Form::text('defaultValue',null,array('class'=>'txtbar')) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="size"><strong>Text Field Size:</strong></label>
                        <div class="controls">
                            {{ Form::select('size',$size,null) }}
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="maxlength"><strong>Max Length</strong></label><br />
                        <span>(Maximum length in characters)</span>
                        <div class="controls">
                            {{ Form::text('maxlength',null,array('class'=>'txtbar')) }}
                        </div>
                    </div>
                </div>
                <div class="span4">
                    <div class="control-group">
                        <label for="mandatory"><strong>Is the Field Mandatory?</strong></label><br />
                        <span>(If Yes, form validation will be added)</span>
                        <div class="controls">
                            Yes{{ Form::radio('mandatory','Y') }}&nbsp;&nbsp;No{{ Form::radio('mandatory','N') }}
                        </div>
                    </div>
                    <br />
                </div>
            </div>
        <br />
        <button class="submit reg-btn">Submit Details</button>
    </div>
    {{ Form::close() }}
</div><!--row-fluid -->
@stop
@section('exScript')
<script>
    $(document).ready(function(){
        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });

        $(".action_remove").click(function(e){

            e.preventDefault();
            var field = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: '/chainstores/competitions/deletefield/'+field,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });
        });

    });
</script>
@stop
