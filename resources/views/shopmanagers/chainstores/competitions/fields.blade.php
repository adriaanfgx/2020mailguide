@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Edit competition fields</h1>
                <p class="animated fadeInDown delay2">Edit your Shop Competition</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/chainshop">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Edit competition</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')

<h4 class="dotted-border">Edit Competition Form</h4>
<div class="row-fluid">

    <div>
        {{ Form::model($form, array('route' => array('chainshop.postEditField', $form->formID),'method'=>'post','files'=>true ,'name' => 'formFieldEdit', 'id' => 'formFieldEdit', 'class' => 'form-inline')) }}
        <div class="row" style="padding-left: 30px">
            <div class="span4">
                <h4 class="dotted-border">Form Recipient</h4>
                <div class="control-group {{ ($errors->has('recipient') ? 'error' : '') }}">
                    <label for="recipient"><strong>Mandatory: E-mail address of person receiving the competition entries.</strong></label>
                    <div class="controls">
                        {{ Form::text('recipient',null,array('class'=>'txtbar','id'=>'recipient')) }}
                        <span class="help-block">{{ ($errors->has('recipient') ? $errors->first('recipient') : '') }}</span>
                    </div>
                </div>
            </div>
            <div class="span4">
                <h4 class="dotted-border">Competition Title</h4>
                <div class="control-group {{ ($errors->has('subject') ? 'error' : '') }}">
                    <label for="subject"><strong>Mandatory: Also appears on the subject line of the email.</strong></label>
                    <div class="controls">
                        {{ Form::text('subject',null,array('class'=>'txtbar','id'=>'subject')) }}
                        <span class="help-block">{{ ($errors->has('subject') ? $errors->first('subject') : '') }}</span>
                        <span class="help-inline"></span>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row" style="padding-left: 30px">
            <div class="span4">
                <div class="control-group {{ ($errors->has('description') ? 'error' : '') }}">
                    <label for="description"><strong>Competition description &amp; Rules</strong></label>
                    <div class="controls">
                        {{ Form::textarea('description',null,array('class'=>'txtbox','rows'=>'3')) }}
                        <span class="help-block">{{ ($errors->has('description') ? $errors->first('description') : '') }}</span>
                    </div>
                </div>
            </div>
            <div class="span4">&nbsp;</div>
        </div>
        <br />
        <br />
        <div class="row" style="padding-left: 30px;">
            <div class="span4">
                <div class="control-group {{ ($errors->has('startDate') ? 'error' : '') }}">
                    <label for="startDate"><strong>Start Date</strong></label>
                    <div class="controls">
                        <div class="input-append datetimepicker4">
                            {{ Form::text('startDate',null,array('class'=>'span12','id'=>'startDate','data-format'=>'yyyy-MM-dd','readonly')) }}
                            <span class="add-on">
                              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                              </i>
                            </span>
                            {{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('endDate') ? 'error' : '') }}">
                    <label for="endDate"><strong>End Date</strong></label>
                    <div class="controls">
                        <div class="input-append datetimepicker4">
                            {{ Form::text('endDate',null,array('class'=>'span12','id'=>'endDate','data-format'=>'yyyy-MM-dd','readonly')) }}
                            <span class="add-on">
                              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                              </i>
                            </span>
                            {{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br />
        <div class="row" style="padding-left: 30px;">
            <div class="span4">
                <div class="control-group">
                    <label for="activated"><strong>Display competition</strong></label>
                    <div class="controls">
                        Yes &nbsp;{{ Form::radio('activated','Y',array('class'=>'txtbar')) }}
                        No &nbsp;{{ Form::radio('activated','N',array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="noForm"><strong>Hide online form?</strong></label>
                    <div class="controls">
                        Yes &nbsp;{{ Form::radio('noForm','Y',array('class'=>'txtbar')) }}
                        No &nbsp;{{ Form::radio('noForm','N',array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <br />
        <br />
        <div class="row" style="padding-left: 30px;">
            <div class="span4">
                <h4 class="dotted-border">Competition Image</h4>
                <div class="control-group {{ ($errors->has('image1') ? 'error' : '') }}">
                    <label><strong>Image:</strong> (Optional. Only jpg, jpeg, gif or png files. No bigger than 1 Mb.)</label>
                    <div class="controls">
                        @if(!empty($form->thumbnail))
                        <!--        <a class="btn btn-danger btn-mini action_remove" href="#" id="image1" data-id="imageBig1"><i class="fa fa-trash-o"></i> Remove Image</a><br />-->
                        <img src="{{ Config::get('app.url') }}/uploadimages/{{ $form->thumbnail }}" alt="Image" />
                        @else
                        <img src="" alt="No Image" class="overlay-image"/>
                        @endif
                        <br />
                        <span class="btn btn-file">{{ Form::file('image') }}</span><br />
                        {{ ($errors->has('image') ? $errors->first('image') : '') }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <h4 class="dotted-border">Competition Fields and Questions</h4>
                <h6>Add a competition Field</h6>
                Clicking the "Add Competition Field" button below will allow you to add a new field to the competition.
                The options available to you include the ability to add text fields, text areas, radio buttons, checkboxes and drop down lists.
                <a href="{{ route('chainshop.addcompetitionfield',$form->formID) }}" class="btn btn-mini btn-info" id="add_field" data-id="{{ $form->formID }}"><i class="fa fa-plus"></i>&nbsp; Add Competition Field</a>
                <table class="table table-bordered">
                    <h6>Edit Existing Competition Fields</h6>
                    <thead>
                    <th>Field Description</th>
                    <th>Actions</th>
                    </thead>
                    <tbody>
                    @foreach( $formFields as $field )
                    <tr>
                        <td>
                            {{ $field->name }}
                        </td>
                        <td>
                            <a href="/chainstores/competitions/editformfield/{{ $form->formID }}/{{ $field->formFieldID }}" class="btn btn-info btn-mini" id="{{ $field->formFieldID }}"><i class="fa fa-pencil"></i>Edit</a>&nbsp;|&nbsp;<a href="#" class="btn btn-danger btn-mini action_remove" id="{{ $field->formFieldID }}"><i class="fa fa-trash-o"></i>Remove</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>


        <div class="span4">

        </div>
        <button class="submit reg-btn">Submit Details</button>
    </div>
    {{ Form::close() }}
</div><!--row-fluid -->
@stop
@section('exScript')
<script>
    $(document).ready(function(){
        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });

        $(".action_remove").click(function(e){

            e.preventDefault();
            var field = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: '/chainstores/competitions/deletefield/'+field,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });
        });

    });
</script>
@stop
