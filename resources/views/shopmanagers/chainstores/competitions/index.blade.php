@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Competition Listing</h1>
                <p class="animated fadeInDown delay2">Manage your Chain Shop Competitions</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/chainshop">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Competitions</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div><!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">

    <div class="span12 pull-left">
        <h4 class="hborder">Competitions Listing</h4>

    </div>
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>

    </div>  <!-- span6 -->


    <div class="row-fluid">
        <div class="span12">
            <h5>Current Competitions</h5>
            <table class="table table-border">
                <thead>
                <tr>
                    <th>Subject</th>
                    <th>Start Date</th>
                    <th>Close Date</th>
                    <th>Displayed</th>
                    <th>Responses</th>
                    <th colspan="3">Actions</th>
                </tr>
                </thead>
                <tbody id="competition_table">
                @if( count($currentCompetitions) > 0 )
                @foreach($currentCompetitions as $current)
                <tr>
                    <td>{{ $current->subject }}</td>
                    <td>{{ $current->startDate }}</td>
                    <td>{{ $current->endDate }}</td>
                    <td>{{ displayText($current->activated) }}</td>
                    <td>
                        <span>{{ $current->count_resp }}</span><br/>
                        <span>({{ $current->winners }} {{ pluralize($current->winners,'Winner') }})</span>
                    </td>
                    <td>
                        <a href="{{ route('chainshop.compfields',$current->formID) }}" class="btn btn-info btn-mini"><i class="fa fa-pencil"></i> Edit</a>&nbsp;|&nbsp;
                        @if($current->count_resp > 0 )
                        <a href="{{ route('competitions.responses',$current->formID) }}"
                           class="btn btn-info btn-mini"><i class="fa fa-eye"> Responses</i></a>&nbsp;|&nbsp;
                        @endif
                        <a class="btn btn-danger btn-mini action_delete" href="#" id="{{ $current->formID }}"><i class="fa fa-trash-o"></i>
                            Delete</a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="6">There are no listed current competitions...</td>
                </tr>
                @endif
                </tbody>
            </table>

            <h5>Past Competitions</h5>

            <table class="table table-border">
                <thead>
                <tr>
                    <th>Subject</th>
                    <th>Start Date</th>
                    <th>Close Date</th>
                    <th>Displayed</th>
                    <th>Responses</th>
                    <th colspan="3">Actions</th>
                </tr>
                </thead>
                <tbody id="competition_table">
                @if( count($pastCompetitions) > 0 )
                @foreach($currentCompetitions as $current)
                <tr>
                    <td>{{ $current->subject }}</td>
                    <td>{{ $current->startDate }}</td>
                    <td>{{ $current->endDate }}</td>
                    <td>{{ displayText($current->activated) }}</td>
                    <td>
                        <span>{{ $current->count_resp }}</span><br/>
                        <span>({{ $current->winners }} {{ pluralize($current->winners,'Winner') }})</span>
                    </td>
                    <td>
                        <a href="{{ route('chainshop.compfields',$current->formID) }}" class="btn btn-info btn-mini"><i class="fa fa-pencil"></i> Edit</a>&nbsp;|&nbsp;
                        @if($current->count_resp > 0 )
                        <a href="{{ route('competitions.responses',$current->formID) }}"
                           class="btn btn-info btn-mini"><i class="fa fa-eye"> Responses</i></a>&nbsp;|&nbsp;
                        @endif
                        <a class="btn btn-danger btn-mini action_delete" href="#" id="{{ $current->formID }}"><i class="fa fa-trash-o"></i>
                            Delete</a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="6">There are no listed past competitions...</td>
                </tr>
                @endif
                </tbody>
            </table>

        </div><!--span12 -->
    </div>
</div>
@stop
@section('exScript')
<script type="text/javascript">
    $(document).ready(function(){

        $(".action_delete").click(function(e){

            e.preventDefault();
            var competition = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: '/chainstores/deletecomp/'+competition,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });
        });

    });
</script>
@stop

