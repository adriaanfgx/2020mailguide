@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Edit Shop </h1>
                <p class="animated fadeInDown delay2">Edit your Chain Shop</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/chainshop">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Edit Shop</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')

<div class="span12 middle-headings dotted-border white-bg">
    <h3>Shops - Edit</h3>
</div><!--/span12-->

<div class="row-fluid">

<div>
{{ Form::model($shop, array('route' => array('shops.postEdit', $shop->shopMGID),'method' => 'post','files'=>true ,'name' => 'shopEdit', 'id' => 'shopEdit', 'class' => 'form-inline')) }}
    {{ Form::hidden('referer',URL::previous()) }}
    <h4 class="dotted-border">Shop Information</h4>
    <div class="control-group {{ ($errors->has('name') ? 'error' : '') }}">
        <label for="name">Shop Name</label>
        <div>
            {{ Form::text('name',null,array('class'=>'txtbar span6','readonly')) }}
            {{ ($errors->has('name') ? $errors->first('name') : '') }}
        </div>
    </div>

    @include('partials/memberzone/shopfields')
    <span>
        If reservation notifications are sent who must receive them? (If applicable)
    </span><br />
    <?php
        $vals = explode(',',$shop->reserveWith);
//        dd($vals);
        $owner = '1';
        $manager1 = '2';
        $manager2 = '3';
        $manager3 = '4';
        $headoffice = '5';
        $finmanager = '6';
        $checked = '';
        $checked1 = '';
        $checked2 = '';
        $checked3 = '';
        $checked4 = '';
        $checked5 = '';
        if( in_array($owner,$vals)){
            $checked = ' checked ="checked"';
        }
        if( in_array($manager1,$vals)){
            $checked1 = ' checked ="checked"';
        }
        if( in_array($manager2,$vals)){
            $checked2 = ' checked ="checked"';
        }
        if( in_array($manager3,$vals)){
            $checked3 = ' checked ="checked"';
        }
        if( in_array($headoffice,$vals)){
            $checked4 = ' checked ="checked"';
        }
        if( in_array($finmanager,$vals)){
            $checked5 = ' checked ="checked"';
        }
//    var_dump($checked);
//    var_dump($checked1);
//    var_dump($checked2);
//    var_dump($checked3);
//    var_dump($checked4);
//    var_dump($checked5);
    ?>
    Owner&nbsp;{{ Form::checkbox('reserveWith[]','1',$checked) }}&nbsp;
    Manager 1&nbsp;{{ Form::checkbox('reserveWith[]','2',$checked1) }}&nbsp;
    Manager 2&nbsp;{{ Form::checkbox('reserveWith[]','3',$checked2) }}&nbsp;
    Manager 3&nbsp;{{ Form::checkbox('reserveWith[]','4',$checked3) }}&nbsp;
    Head Office&nbsp;{{ Form::checkbox('reserveWith[]','5',$checked4) }}&nbsp;
    Financial Manager&nbsp;{{ Form::checkbox('reserveWith[]','6',$checked5) }}&nbsp;<br /><br />
    <button class="submit reg-btn" id="add_review">Submit Details</button>
{{ Form::close() }}
</div><!--row-fluid -->

@stop

@section('exScript')
<script>

    $(document).ready(function(){

        refreshSubcategoryList();

        if( $("#category").val() == ''){
            $('#subcategory').empty();
        }

        $("#category").change(function(){
            refreshSubcategoryList();
        });

        $(".action_remove").click(function(e){
            var image = $(this).attr('id');
            var imageBig = $(this).attr('data-id');
            var route = window.location.href;
            var resultArr = route.split('/');
            var shop = resultArr[5];

            e.preventDefault();

            bootbox.confirm("Are you sure you want to remove this entry ? ");

            $(".modal a.btn-primary").click(function(){

                $.ajax({
                    type: "POST",
                    url: '/shops/removeimage/'+shop+'/'+image+'/'+imageBig,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
//                        location.reload();
                    }
                });
            });
        });
    });

    function refreshSubcategoryList()
    {
        var category = $('#category').val();

        $.ajax({
            type: 'GET',
            url: '/shops/subcategory/'+category ,
            data: '',
            success: function (data) {

                $('#subcategory').empty().append('<option value="">Please Select...</option>');
                $.each(data, function(key, value){
                    $('#subcategory').append('<option value="'+key+'">'+value+'</option>');
                })
            }
        });
    }


</script>
@stop
