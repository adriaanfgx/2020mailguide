@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Logged In </h1>

                <p class="animated fadeInDown delay2">Chain Shop Member</p>
            </div>
            <?php
                $usr = Sentry::getUser();
                $user = User::find($usr->id);
            ?>
            <!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a> <span class="divider">/</span></li>
                    <li class="active">Memberzone</li>
                </ul>
            </div>
            <!--/span6-->
        </div>
        <!--/row-->
    </div>
    <!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="span12 middle-headings dotted-border white-bg">
    <h2><a href="#">{{ Str::upper($user->chain_name) }}</a></h2>
    <div class="span6 pull-left">Hi {{ $user->first_name }}<br/> Listed on this page is a host of link shortcuts that
        will help you make the most of your listing on Mallguide. Please make use of our range of free services and tools at your disposal to help market and promote your mall to all of South Africa.
    </div>
</div><!--/span12-->

<div class="row-fluid">
    <div class="span7">

        <h4 class="dotted-border">Your Quick Stats</h4>
        <div class="yellow">
            <ul>
                <li>{{ $promotions }} &nbsp; promotions are currently displayed. <a href="{{ route('chainshop.createpromotion') }}">Add a new promotion</a> or <a href="{{ route('chainshop.promotions') }}">manage existing ones</a>.</li>
                <li>{{ $jobs }} &nbsp; jobs are currently available. <a href="{{ route('chainshop.jobs') }}">List existing jobs</a> or <a href="{{ route('chainshop.createjob') }}">add a new one</a>.</li>
                <li>{{ $giftIdeas }} &nbsp; gift ideas are currently listed. <a href="{{ route('chainshop.giftideas') }}">List existing gift ideas</a> or <a href="{{ route('chainshop.createGiftIdea') }}">add a new gift idea</a>.</li>
                <li>You have access to &nbsp;<strong>{{ $countShops }}</strong>&nbsp; shops. <a href="{{ route('chainshop.chainstores') }}">View All</a></li>
            </ul>
        </div>
    </div>
    <!-- span6 -->

    <div class="span5">
        <h4>Your Control Panel</h4>

        <div class="control-panel">
            <h4><span class="control-ico-about">About You</span></h4>
            <span>
                We need these details for contact purposes, otherwise they are kept confidential. Your profile listing:
            </span>
            <div class="row" style="padding-left: 35px;">
                <div class="row" style="padding-left: 35px;">
                    <div class="span4"><label><strong>Chain</strong></label></div>
                    <div class="span4">{{ $chain }}</div>
                </div>
            </div>
            <div class="row" style="padding-left: 35px;">
                <div class="row" style="padding-left: 35px;">
                    <div class="span4"><label><strong>Name</strong></label></div>
                    <div class="span4">{{ $user->first_name }}</div>
                </div>
            </div>
            <div class="row" style="padding-left: 35px;">
                <div class="row" style="padding-left: 35px;">
                    <div class="span4"><label><strong>Email</strong></label></div>
                    <div class="span4">{{ $user->email }}</div>
                </div>
            </div>
            <div class="row" style="padding-left: 35px;">
                <div class="row" style="padding-left: 35px;">
                    <div class="span4"><label><strong>Company</strong></label></div>
                    <div class="span4">{{ $user->company }}</div>
                </div>
            </div>
            <ul>
                <li><a href="{{ route('chainshop.editprofile',$user->id) }}">Update your personal information</a></li>
                <li><a href="{{ route('chainshop.updatepassword',$user->id) }}">Update your password</a></li>
            </ul>

            <!-- Mall And User update section -->

            <h4><span class="control-ico-comm">Communication with Mallguide</span></h4>
            <span>
                <strong>Under Construction....Coming Soon</strong>
            </span>

            <h4><span class="control-ico-links">Quick Links</span></h4>
            <ul>
                <li>
                    <a href="{{ route('chainshop.create') }}">Add a Shop</a>&nbsp; | &nbsp;
                    <a href="{{ route('chainshop.chainstores') }}">List Shops</a>
                </li>
                <li>
                    <a href="{{ route('chainshop.adduser') }}">Add a User </a>&nbsp; | &nbsp;
                    <a href="{{ route('chainshop.listusers') }}"> List Users</a>
                </li>
                <li>
                    <a href="{{ route('chainshop.createcompetition') }}">Add a Competition</a>&nbsp; | &nbsp;
                    <a href="{{ route('chainshop.competitions') }}">List Competitions</a>
                </li>
                <li>
                    <a href="{{ route('chainshop.createpromotion') }}">Add a Promotion</a>&nbsp; | &nbsp;
                    <a href="{{ route('chainshop.promotions') }}">List Promotions</a>
                </li>
                <li>
                    <a href="{{ route('chainshop.createjob') }}">Add a Job</a>&nbsp; | &nbsp;
                    <a href="{{ route('chainshop.jobs') }}">List Jobs</a>
                </li>
                <li>
                    <a href="{{ route('chainshop.createGiftIdea') }}">Add a Gift Idea</a>&nbsp; | &nbsp;
                    <a href="{{ route('chainshop.giftideas') }}">List Gift Ideas</a>
                </li>
            </ul>

            <div>
                <h4><span class="control-ico-posts">Information posted by Mallguide</span></h4>
                <ul>
                    <li><a href="{{ route('chainshop.marketinginfo') }}">Want to send an electronic Newsletter to your
                            Shopper/Client Database?</a></li>
                    <li><a href="{{ route('chainshop.compterms') }}">Standard Terms and Conditions for Competitions
                            and Adding Entrants into Databases for further Communications</a></li>
                </ul>
            </div>
        </div>
        <!-- control panel -->
    </div>
    <!--span6 -->
</div><!--row -->


@stop
