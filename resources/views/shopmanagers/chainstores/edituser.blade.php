@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Mall Member</h1>
                <p class="animated fadeInDown delay2">Manage your chain store users</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/chainshop">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Member Details</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div><!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>
    </div>  <!-- span6 -->
    <div class="pull-left">
        <h4>Personal Details</h4>
    </div>
    <div class="clear"></div>
    <hr class="hborder" />

    <div class="row-fluid">
        <div class="span12">
            {{ Form::model($user, array('route' => array('user.postUpdatedetails', $user->id),'method'=>'post','name' => 'userUpdate', 'id' => 'userUpdate', 'class' => 'form-inline')) }}
            <div>
                <div id="user_info">
                    <h4 class="dotted-border">Personal Details</h4>
                    <div class="row" style="padding-left: 30px;">
                        <div class="span4">
                            <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}">
                                <label for="email"><strong>Email*</strong></label>
                                <div class="controls">
                                    {{ Form::text('email',null,array('class'=>'txtbar','readonly')) }}
                                    {{ ($errors->has('email') ? $errors->first('email') : '') }}
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="control-group {{ ($errors->has('first_name') ? 'error' : '') }}">
                                <label for="first_name"><strong>First Name</strong></label>
                                <div class="controls">
                                    {{ Form::text('first_name',null,array('class'=>'txtbar')) }}
                                    {{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="last_name"><strong>Surname</strong></label>
                                <div class="controls">
                                    {{ Form::text('last_name',null,array('class'=>'txtbar')) }}
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="company"><strong>Company</strong></label>
                                <div class="controls">
                                    {{ Form::text('company',null,array('class'=>'txtbar')) }}
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="vat_number"><strong>Vat Number</strong></label>
                                <div class="controls">
                                    {{ Form::text('vat_number',null,array('class'=>'txtbar')) }}
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="postal_address"><strong>Postal Address</strong></label>
                                <div class="controls">
                                    {{ Form::textarea('postal_address',null,array('class'=>'txtbox','rows'=>'3')) }}
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="postal_code"><strong>Postal Code</strong></label>
                                <div class="controls">
                                    {{ Form::text('postal_code',null,array('class'=>'txtbar')) }}
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="province"><strong>Province List</strong></label>
                                <div class="controls">
                                    {{ Form::select('province',$provinces,$user->province,array('id'=>'province')) }}
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="city"><strong>City List</strong></label>
                                <div class="controls">
                                    {{ Form::select('city',$cities,$user->city,array('id'=>'city')) }}
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="surbub"><strong>Suburb</strong></label>
                                <div class="controls">
                                    {{ Form::text('suburb',null,array('class'=>'txtbar')) }}
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="tel_work"><strong>Work Tel</strong></label>
                                <div class="controls">
                                    {{ Form::text('tel_work',null,array('class'=>'txtbar')) }}
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="tel_home"><strong>Home Tel</strong></label>
                                <div class="controls">
                                    {{ Form::text('tel_home',null,array('class'=>'txtbar')) }}
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="fax"><strong>Fax</strong></label>
                                <div class="controls">
                                    {{ Form::text('fax',null,array('class'=>'txtbar')) }}
                                </div>
                            </div>
                            <div class="control-group">
                                <label for="cell"><strong>Cell</strong></label>
                                <div class="controls">
                                    {{ Form::text('cell',null,array('class'=>'txtbar')) }}
                                </div>
                            </div>
                            <div class="control-group {{ ($errors->has('birth_date') ? 'error' : '') }}">
                                <label for="birth_date" class="control-label"><strong>Date of Birth</strong></label>
                                <div class="controls">
                                    <div class="input-append datetimepicker4">
                                        {{ Form::text('birth_date',null,array('data-format'=>'yyyy-MM-dd','readonly')) }}
                                        <span class="add-on">
                                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                          </i>
                                        </span>
                                        {{ ($errors->has('birth_date') ? $errors->first('birth_date') : '') }}
                                    </div>
                                </div>
                                <span class="help-block">{{ ($errors->has('birth_date') ? $errors->first('birth_date') : '') }}</span>
                            </div>
                            <div class="control-group">
                                <label for="cell"><strong>Gender</strong></label>
                                <div class="controls">
                                    {{ Form::radio('gender','Female') }}Female &nbsp;&nbsp;{{ Form::radio('gender','Male') }}Male
                                </div>
                            </div>
                        </div>
                        <div class="span4">&nbsp;</div>
                    </div>
                </div>
            </div>
            <button class="submit reg-btn" id="update_user">Submit Details</button>
            {{ Form::close() }}
        </div><!--span12 -->
    </div>
</div>
@stop
@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });
    });
</script>
@stop

