@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Edit profile </h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/chainshop">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Edit profile</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div><!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>
    </div>  <!-- span6 -->
    <div class="pull-left">
        <h4>Personal Details</h4>
    </div>
    <div class="clear"></div>
    <hr class="hborder" />

    <div class="row-fluid">
        <div class="span12">
            <span>
                <strong>Please note: </strong><br />
                Removing your e-mail address or supplying a non-working address is against our policies.<br />
                Required fields are marked with *
            </span>
            <br />
            <br />
            {{ Form::model($user, array('route' => array('chainshop.postEditProfile', $user->id),'method'=>'post','name' => 'userUpdate', 'id' => 'userUpdate', 'class' => 'form-inline')) }}
            @include('partials/memberzone/userprofilefields')
            <button class="submit reg-btn" id="update_user">Submit Details</button>
            {{ Form::close() }}
        </div><!--span12 -->
    </div>
</div>
@stop
@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });

        $("#province").change(function(){

            $("#province_text").val($("#province :selected").text());
        });

        $("#city").change(function(){
            $("#city_text").val($("#city :selected").text());
        });
    });
</script>
@stop

