@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Manage your Chain Shops</h1>
                <p class="animated fadeInDown delay2">Chain Shop Member</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/chainshop">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Shops</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div><!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">

    <div class="span9 pull-left">
        <h4 class="hborder">Chain Stores</h4>

    </div>
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>
        <div class="blog-content border_top">
            <dl>
                <dt>Filter By Province</dt><br />
                <dd>
                    {{ Form::select('province',$provinceList,$province,array('id'=>'province')) }}
                </dd>
                <br>

            </dl>
        </div>

    </div>  <!-- span6 -->

    <div class="clear"></div>
    <hr class="hborder" />

    <div class="row-fluid">
        <div class="span12">
            <table class="table table-border">
                <thead>
                <tr>
                    <th>Mall</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Sub Category</th>
                    <th>Display</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody id="shop_table">
                @foreach($userShops as $shop)
                <tr>
                    <td>{{ $shop->name }}</td>
                    <td>{{ $shop->shop }}</td>
                    <td>{{ $shop->category }}</td>
                    <td>{{ $shop->subcategory }}</td>
                    <td>{{ displayText($shop->display) }}</td>
                    <td>
                        <a href="{{ route('chainshop.edit',$shop->shopMGID) }}" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp;&nbsp;|
                        &nbsp;&nbsp;<a class="btn btn-danger btn-mini action_delete" href="{{ route('chainshop.delete',$shop->shopMGID) }}" id="{{ $shop->shopMGID }}"><i class="fa fa-trash-o"></i> Delete</a>
                    </td>
                </tr>
                @endforeach
                </tbody>
            </table>

        </div><!--span12 -->
    </div>
</div>
@stop
@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

        $(".action_delete").click(function(e){
            e.preventDefault();
            var shop = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: 'deletestore/'+shop,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });
        });


        $("#province").change(function(){

            var province = $(this).val();

            if( province == '' ){

                window.location.href = '/chainstores';
            }else{
                window.location.href = '/chainstores/provincefilter/'+province;
            }

        });
    });
</script>
@stop

