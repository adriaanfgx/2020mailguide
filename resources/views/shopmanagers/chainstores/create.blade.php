@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Add Shop </h1>
                <p class="animated fadeInDown delay2">Add your Chain Shop</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
				{{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')

<div class="row-fluid">

	<div class="blog-content">
		<h3>Shops - Add</h3>
        {{ Form::open(array('route' => 'shops.postCreateShop', 'method' => 'post','files'=> true,'name' => 'shops.postCreate','id' => 'postCreate', 'class' => 'form-inline')) }}
        {{ Form::hidden('referer',URL::previous()) }}
        <h4 class="dotted-border">After Your Submission</h4>
        Each submission sent from tentants, are logged, and sent to your Mall's Marketing Team or Centre Management. This information is verified with them, and then approved. As soon as this information is approved by them, your store is updated for free. It's as easy as that...<br />

        <h4 class="dotted-border">Mall Information</h4>
        <div class="control-group {{ ($errors->has('mallID') ? 'error' : '') }}">
            <label for="mallID">Mall</label>
            <div class="controls">
                {{ Form::select('mallID',$malls,null,array('class'=>'span6')) }}
                <div class="help-block">{{ ($errors->has('mallID') ? $errors->first('mallID') : '') }}</div>
            </div>
        </div>

        <h4 class="dotted-border">Shop Information</h4>
        <div class="control-group {{ ($errors->has('name') ? 'error' : '') }}">
            <label for="name">Shop Name</label>
            <div>
                {{ Form::text('name',$shopName,array('class'=>'txtbar span6','readonly')) }}
                {{ ($errors->has('name') ? $errors->first('name') : '') }}
            </div>
        </div>
        @include('partials/memberzone/shopfields')
    <span>
        If reservation notifications are sent who must receive them? (If applicable)
    </span><br />

        Owner&nbsp;{{ Form::checkbox('reserveWith[]','1') }}&nbsp;
        Manager 1&nbsp;{{ Form::checkbox('reserveWith[]','2') }}&nbsp;
        Manager 2&nbsp;{{ Form::checkbox('reserveWith[]','3') }}&nbsp;
        Manager 3&nbsp;{{ Form::checkbox('reserveWith[]','4') }}&nbsp;
        Head Office&nbsp;{{ Form::checkbox('reserveWith[]','5') }}&nbsp;
        Financial Manager&nbsp;{{ Form::checkbox('reserveWith[]','6') }}&nbsp;<br /><br />
        <button class="submit reg-btn" id="add_review">Submit Details</button>
        {{ Form::close() }}
    </div><!--row-fluid -->
</div><!--row-fluid -->

    @stop

    @section('exScript')
    <script>

        $(document).ready(function(){

//            refreshSubcategoryList();

            if( $("#category").val() == ''){
                $('#subcategory').empty();
            }

            $("#category").change(function(){
                refreshSubcategoryList();
            });
        });

        function refreshSubcategoryList()
        {

            var category = $('#category').val();
//            alert(category);
            $.ajax({
                type: 'GET',
                url: '/shops/subcategory/'+category ,
                data: '',
                success: function (data) {

                    $('#subcategory').empty().append('<option value="">Please Select...</option>');

                    $.each(data, function(key, value){
                        $('#subcategory').append('<option value="'+key+'">'+value+'</option>');
                    })
                }
            });
        }


    </script>
    @stop
