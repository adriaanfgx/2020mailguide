@extends('layouts.backpages')
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>View Applicant</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
				{{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')


<div class="row-fluid">
    <div class="blog-content">
        <h4 class="hborder">Applicant Details</h4>
        <div class="span8">
            <table class="table table-bordered">
                <tr>
                    <td>
                        <label>First name</label>
                    </td>
                    <td>{{ $applicant->name }}</td>
                </tr>
                <tr>
                    <td>
                        <label>Surname</label>
                    </td>
                    <td>
                        {{ $applicant->surname }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Home Tel</label>
                    </td>
                    <td>
                        {{ $applicant->telephone }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Cell</label>
                    </td>
                    <td>
                        {{ $applicant->cell }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Email</label>
                    </td>
                    <td>
                        {{ $applicant->email }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>CV</label>
                    </td>
                    <td>
                        @if(!empty( $applicant->cv ) )
                        <a target="_blank" href="{{ route('jobs.cv',$applicant->cv) }}">Download</a>
                        @endif
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){


    });
</script>
@stop

