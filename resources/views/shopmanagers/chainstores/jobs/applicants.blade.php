@extends('layouts.backpages')
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Job Applicants Listing</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
				{{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid">
    <div class="blog-content">
        <h4 class="hborder">Job Applicants Listing</h4>
        <table class="table table-border">
            <thead>
            <tr>
                <th>Applicant</th>
                <th>Cell</th>
                <th>Telephone</th>
                <th>Date Applied</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($applicants as $applicant)
            <tr>
                <td>{{ $applicant->name }} &nbsp; {{ $applicant->surname }}</td>
                <td>{{ $applicant->cell }}</td>
                <td>{{ $applicant->telephone }}</td>
                <td>{{ $applicant->dateAdded }}</td>
                <td>
                    <a href="{{ route('chainshop.getjobapplicant',$applicant->jobApplicantID) }}" class="btn btn-info btn-mini"><i class="fa fa-eye">View</i></a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $(".action_delete").click(function(e){

            e.preventDefault();
            var job = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: '/jobs/delete/'+job,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });


        });
    });
</script>
@stop

