@extends('layouts.backpages')
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Add Job </h1>
                <p class="animated fadeInDown delay2">Add your Chain Shop Job</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
				{{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')

<div class="row-fluid">
	<div class="blog-content">
    <h3>Shops - Add a Job</h3>

    Looking for the best staff to fill your available positions? <br />
    Let Mallguide assist you by matching our database of job seekers to your location and required skills. Have the pick of the crop with this free online service...<br />
    <h4 class="dotted-border">How does it Work</h4>
    <ul>
        <li>You add a new job online </li>
        <li>Job seekers register with Mallguide, and upload their CV's</li>
        <li>The job seekers browse the list of available positions and apply online </li>
        <li>You screen and approve applicants based on their submissions and CV's </li>
        <li>Approved applicants are sent your direct contact details for an interview </li>
        <li>Your store prospers under fine management and capable staff! </li>
    </ul>
    <h4 class="dotted-border"></h4>
    <div>
        {{ Form::open(array('route' => 'chainshop.postCreateJob', 'method' => 'post','name' => 'postCreateJob','id' => 'postCreateJob', 'class' => 'form-inline')) }}
        <div class="row" style="padding-left: 30px">
            <div class="span10">
                <div class="control-group {{ ($errors->has('mallID') ? 'error' : '') }}">
                    <label for="mallID">Select A mall*</label>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                    <a href="#" id="deselect_all"><strong>Deselect All</strong></a>
                    <div class="controls">
                        {{ Form::select('mallID[]',$malls,$mallIDs,array('class'=>'span10 multi-mall','id'=>'mallID','multiple')) }}
                        {{ ($errors->has('mallID') ? $errors->first('mallID') : '') }}
                    </div>
                </div>
            </div>
            <div class="span4">&nbsp;</div>
        </div>

        <div class="row" style="padding-left: 30px;">
            <div class="span4">
                <label for="title" class="control-label">Position*</label>
                <div class="control-group {{ ($errors->has('title') ? 'error' : '') }}">
                    {{ Form::text('title',null) }}
                    <span class="help-block">{{ ($errors->has('title') ? $errors->first('title') : '') }}</span>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('salary') ? 'error' : '') }}">
                    <label for="salary" class="control-label">Salary</label>
                    <div class="controls">
                        {{ Form::text('salary',null) }}
                        <span class="help-block">{{ ($errors->has('salary') ? $errors->first('salary') : '') }}</span>
                    </div>
                </div>

            </div>
        </div>
        <div class="row" style="padding-left: 30px;">
            <div class="span4">
                <div class="input-append datetimepicker4">
                    <div class="control-group {{ ($errors->has('startDate') ? 'error' : '') }}">
                        <label for="startDate" class="control-label">Starting Date</label>
                        <div class="controls">
                            {{ Form::text('startDate',null,array('class' => 'span12','data-format'=>'yyyy-MM-dd','readonly')) }}
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                            </i>
                        </span>
                            <span class="help-block">{{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="input-append datetimepicker4">
                    <div class="control-group {{ ($errors->has('endDate') ? 'error' : '') }}">
                        <label for="endDate">Closing Date*</label>
                        <div class="controls">
                            {{ Form::text('endDate',null,array('class' => 'span12','data-format'=>'yyyy-MM-dd','readonly')) }}
                        <span class="add-on">
                            <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                            </i>
                        </span>
                        <span class="help-block">{{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-left: 30px;">
            <div class="span4">
                <div class="control-group">
                    <label for="refNum">Ref. No</label>
                    <div class="controls">
                        {{ Form::text('refNum',null) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group {{ ($errors->has('description') ? 'error' : '') }}" >
                    <label for="description">Description*</label>
                    <div class="controls">
                        {{ Form::textarea('description',null,array('rows'=>3)) }}
                        <span class="help-block">{{ ($errors->has('description') ? $errors->first('description') : '') }}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="padding-left: 30px;">
            <div class="span4">
                <div class="control-group">
                    <label>Must job be displayed?</label>
                    <div class="controls">
                        Yes {{ Form::radio('display','Y') }} &nbsp;
                        No {{ Form::radio('display','N') }}
                    </div>
                </div>
            </div>
        </div>
       <h4 class="dotted-border">Contact Details</h4>
        <div class="row" style="padding-left: 30px;">
            <div class="span4">
                <div class="control-group">
                    <label for="contactPerson">Contact Person</label>
                    <div class="controls">
                        {{ Form::text('contactPerson',null) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="cell">Cell</label>
                    <div class="controls">
                        {{ Form::text('cell',null) }}
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="padding-left: 30px;">
            <div class="span4">
                <div class="control-group">
                    <label for="telephone">Telephone</label>
                    <div class="controls">
                        {{ Form::text('telephone',null) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="fax">Fax</label>
                    <div class="controls">
                        {{ Form::text('fax',null) }}
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="padding-left: 30px;">
            <div class="span4">
                <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}">
                    <label for="email" class="control-label">Email</label>
                    <div class="controls">
                        {{ Form::text('email',null) }}
                        <span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
                    </div>
                </div>
            </div>
            <div class="span4">
                &nbsp;
            </div>
        </div>
        <div class="row" style="padding-left: 30px;">
            <div class="control-group">
                <label for="showInfo">Must shop name and contact details for job be displayed?</label>
                <div class="controls">
                    Yes&nbsp;{{ Form::radio('showInfo','Y',null) }}&nbsp;&nbsp;No&nbsp;{{ Form::radio('showInfo','N',null) }}
                </div>
            </div>
        </div>
        <br />
        <button class="submit reg-btn" id="add_review">Submit Details</button>
        {{ Form::close() }}
    </div><!--row-fluid -->
</div><!--row-fluid -->

    @stop

    @section('exScript')
    <script>

        $(document).ready(function(){

            $('.datetimepicker4').datetimepicker({
                pickTime: false
            });

            $("#mallID").select2({});

            $("#deselect_all").click(function(){

                $(".multi-mall").select2('val', '');
            });

            if( $("#category").val() == ''){
                $('#subcategory').empty();
            }

            $("#category").change(function(){
                refreshSubcategoryList();
            });
        });

        function refreshSubcategoryList()
        {

            var category = $('#category').val();
//            alert(category);
            $.ajax({
				type: 'GET',
                url: '/shops/subcategory/'+category ,
                data: '',
                success: function (data) {

                    $('#subcategory').empty().append('<option value="">Please Select...</option>');

                    $.each(data, function(key, value){
                        $('#subcategory').append('<option value="'+key+'">'+value+'</option>');
                    })
                }
            });
        }


    </script>
    @stop
