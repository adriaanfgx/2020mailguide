@extends('layouts.backpages')
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Job Listing</h1>
                <p class="animated fadeInDown delay2">Manage your Chain Shop Jobs</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
				{{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid">
        <div class="blog-content">

		<div class="hborder">
	        <h4>Job Listings</h4>

		    <div class="span3 pull-right pull-top">
	            {{ Form::select('malls',$malls,null,array('id'=>'mall')) }}
		    </div>  <!-- span3 -->
	    </div>  <!-- span3 -->


            <h5>Listed Jobs</h5>
            <table class="table table-border">
                <thead>
                <tr>
                    <th>Shop Name</th>
                    <th>Job Title</th>
                    <th>Closing Date</th>
                    <th>Show Contact Info</th>
                    <th>Display</th>
                    <th>Applicants</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody id="shop_table">
                @if(sizeof($currentJobs) > 0 )
                @foreach($currentJobs as $current)
                <tr>
                    <td>{{ $current->name }} - {{ $current->mall }}</td>
                    <td>{{ $current->title }}</td>
                    <td>{{ $current->endDate }}</td>
                    <td>{{ displayText($current->showInfo) }}</td>
                    <td>{{ displayText($current->display) }}</td>
                    <td>
                        {{ $current->applicants }} &nbsp;
                        @if( ($current->applicants) > 0 )
                        <a href="{{ route('chainshop.getjobapplicants',$current->jobID) }}" class="btn btn-info btn-mini" style="float:right;margin-top:1px;"><i class="fa fa-eye">View</i></a>
                        @endif
                    </td>
                    <td>
                        <a href="{{ route('jobs.edit',$current->jobID) }}" class="btn btn-info btn-mini" ><i class="fa fa-pencil"></i> Edit</a>&nbsp; | &nbsp;
                        <a class="btn btn-danger btn-mini action_delete" href="{{ route('jobs.delete',$current->jobID) }}" id="{{ $current->jobID }}"><i class="fa fa-trash-o"></i> Delete</a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="7">
                        There are no current listed jobs .....
                    </td>
                </tr>
                @endif
                </tbody>
            </table>
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $(".action_delete").click(function(e){

            e.preventDefault();
            var job = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: '/jobs/delete/'+job,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });
        });

        $("#mall").change(function(){

            if( $(this).val() == '' ){
                window.location.href = '/chainstores/jobs';
            }else{
                var job = $(this).val();
                window.location.href = '/chainstores/jobs/filtermall/'+job;
            }
        });
    });
</script>
@stop

