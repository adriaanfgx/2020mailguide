@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Gift Ideas Listing</h1>
                <p class="animated fadeInDown delay2">Manage your Chain Shop Gift Ideas</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li><a href="/chainshop">Memberzone</a><span class="divider">/</span></li>
                    <li class="active">Gift Ideas</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@include('portal.partials.submenu')

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div><!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">

    <div class="span12 pull-left">
        <h4 class="hborder">Gift Ideas Listings</h4>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <table class="table table-border">
                <thead>
                <tr>
                    <th>Shop</th>
                    <th>Title</th>
                    <th>Price</th>
                    <th>Date Added</th>
                    <th>Display</th>
                    <th colspan="2">Actions</th>
                </tr>
                </thead>
                <tbody>
                @if(sizeof($giftideas) > 0 )
                @foreach($giftideas as $idea)
                <tr>
                    <td>{{ $idea->name }} - {{ $idea->mall }}</td>
                    <td>{{ $idea->title }}</td>
                    <td>{{ $idea->price }}</td>
                    <td>{{ $idea->dateAdded }}</td>
                    <?php
                    $displayIcon = '';
                    $ideaDisplay = '';
                    if($idea->display == 'Y'){
                        $ideaDisplay = 'btn-success';
                        $displayIcon = 'fa-thumbs-up';
                    }else{
                        $ideaDisplay = 'btn-danger';
                        $displayIcon = 'fa-thumbs-down';
                    }
                    ?>
                    <td>
                        <a href="#" class="btn btn-mini {{ $ideaDisplay }} idea-display" id="{{ $idea->giftIdeaID }}" data-id="{{ $idea->display }}"><i class="fa {{ $displayIcon }}">{{ displayText($idea->display) }}</i></a>
                    </td>
                    <td colspan="2">
                        <a href="{{ route('store.editStoreGiftIdea',$idea->giftIdeaID) }}" class="btn btn-info btn-mini"><i class="fa fa-pencil"></i>Edit</a>&nbsp;|&nbsp;
                        <a class="btn btn-danger btn-mini action_delete" href="#" id="{{ $idea->giftIdeaID }}"><i class="fa fa-trash-o"></i>Delete</a>
                    </td>
                </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="7">
                        There are no listed gift ideas .....
                    </td>
                </tr>
                @endif
                </tbody>
            </table>

        </div><!--span12 -->
    </div>
</div>
@stop
@section('exScript')
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(){

        $(".action_delete").click(function(e){

            e.preventDefault();
            var giftidea = $(this).attr("id");

            bootbox.confirm("Are you sure you want to delete this entry ? ");

            $(".modal a.btn-primary").click(function(){
                $.ajax({
                    type: "POST",
                    url: '/store/deletegiftidea/'+giftidea,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function(){
                        location.reload();
                    }
                });
            });
        });

        $(".idea-display").click(function(){

            var ideaToUpdate = $(this).attr('id');
            var currentStatus = $(this).attr('data-id');
            var newStatus = '';
            if(currentStatus == 'Y'){
                newStatus = 'N';
            }else{
                newStatus = 'Y';
            }

            bootbox.confirm("Are you sure you want to change shop display status ? ");

            $(".modal a.btn-primary").click(function () {
                $.ajax({
                    type: "POST",
                    url: '/store/giftidea/updatedisplay/'+ideaToUpdate+'/'+newStatus,
                    data: {
                        '_token': "{{ csrf_token() }}"
                    },
                    success: function () {
                        location.reload();
                    }
                });
            });

        });
    });
</script>
@stop

