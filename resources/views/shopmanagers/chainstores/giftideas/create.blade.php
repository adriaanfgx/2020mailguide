@extends('layouts.backpages')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop


@section('content')

<div class="row-fluid">

    <div>
        {{ Form::open(array('route' => 'store.postAddGiftIdea', 'method' => 'post','files'=> true,'name' => 'shops.postAddGiftIdea','id' => 'postAddGiftIdea', 'class' => 'form-inline')) }}
        @include('partials/memberzone/giftideaheader')
        <h4 class="dotted-border">Add Gift Ideas</h4>
        <h6>Gift Idea Details</h6>
        <div class="row" style="padding-left: 30px;">
            <div class="control-group {{ ($errors->has('mallID') ? 'error' : '') }}">
                <label class="control-label" for="mallID"><strong>Select a mall</strong></label>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
                <a href="#" id="deselect_all"><strong>Deselect All</strong></a>
                <div class="controls">
                    {{ Form::select('mallID[]',$malls,$mallIDs,array('class'=>'span10 multi-mall','id'=>'mallID[]','multiple')) }}
                    <br />
                    <br />
                    <div class="help-block">{{ ($errors->has('mallID') ? $errors->first('mallID') : '') }}</div>
                </div>
            </div>
        </div>
        <br />
        @include('partials/memberzone/giftideafields')
        <button class="submit reg-btn" id="add_review">Submit Details</button>
        {{ Form::close() }}
    </div><!--row-fluid -->

    @stop

    @section('exScript')
    <script>

        $(document).ready(function(){
            var incrementer = 1;
//            $("#add_gift_idea").click(function(){
//                $('.gift-i').last().clone().attr('id',parseInt(incrementer)+1).appendTo('#gift_idea_div');
//                $('#gift_idea_div').find('div:last').find('input:text').val('');
//                $('#'+parseInt(incrementer)+1).find('input:file').attr('name','image-'+parseInt(incrementer)+parseInt(1));
////                $('#'+parseInt(incrementer)+1).attr('name','image-'+parseInt(incrementer)+parseInt(1));
////                alert($('#gift_idea_div').find('tr:last').find('input:text').val());
//            });

            $(".multi-mall").select2({});

            $("#deselect_all").click(function(){

                $(".multi-mall").select2('val', '');
            });

        });
    </script>
    @stop
