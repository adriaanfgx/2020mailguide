<!DOCTYPE html>
<html>
<head>

    <title>{{ $seo_title }}</title>
    @section('seo_meta')

        <meta name="description" content="{{ $seo_description }}">
        <meta name="keywords" content="{{ $seo_keywords }}">
    @show
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <!-- Mobile Specific Metas
================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="imagetoolbar" content="false" />
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="/favicon.ico" />
    <link rel="apple-touch-icon" href="apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72x72-precompressed.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114x114-precompressed.png" />

    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/capture.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/over_write.css') }}" rel="stylesheet">
    <link href="{{ asset('css/colorbox.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.css?v=2.1.5') }}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{ asset('css/jquery.timepicker.css?v=2.1.5') }}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{ asset('css/jquery-ui-1.10.4.custom.css') }}" type="text/css" media="screen" />
    <link rel="stylesheet" href="{{ asset('css/datepicker.css') }}" type="text/css" media="screen" />
    <link href="{{ asset('css/select2.css') }}" rel="stylesheet">

    <script type="text/javascript" src="{{ asset('js/jquery-1.10.2.min.js') }}"></script>



    <link href='//fonts.googleapis.com/css?family=Raleway:400,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

</head>

<body class="box-bg">
<div class="container">

    <div id="header" class="fixed-header-container">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span12">
                    <div class="navbar">
                        <div class="navbar-inners">
                            <div class="container-fluid">
                                <div class="nav-container-outer">
                                    <div class="nav-container-inner">
                                        <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <div class="logo pull-left">
                                            <a class="brand" href="{{ route('home.index') }}"><img src="{{ asset('img/mall_logo.png') }}" alt="Mall Guide"></a>
                                        </div><!--/logo-->
                                        @include('partials/admin_status')
                                        @include('layouts.menu')
                                    </div>
                                </div>
                            </div><!--/container-->
                        </div><!--/navbar inner-->
                    </div><!--/navbar-->


                </div><!--/span12-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/header-->



    @include('layouts.portal.partials.submenu')

    <div class="white mallbg1">
        <div class="container-fluid">
            <!-- Notifications -->
        @include('notifications')
        <!-- ./ notifications -->

            <!-- Content -->
        @yield('content')
        <!-- ./ content -->
        </div><!-- container-fluid -->
    </div><!-- white -->

</div>

@include('partials/footer')


<!--Plugins-->

<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/twitter-bootstrap-hover-dropdown.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/filter.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/capture.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/fixed-header.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/testimonials.js') }}"></script>


<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script src="{{ asset('js/select2.js') }}"></script>
<script src="{{ asset('js/jquery.validate.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('js/jquery.colorbox-min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.timepicker.js?v=2.1.5') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-ui-1.10.4.custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/jquery.numeric.js') }}"></script>
<script src="{{ asset('js/jquery.format-1.3.min.js') }}"></script>
<script src="{{ asset('js/jquery.maskedinput.js') }}"></script>
<!--Custom javascript-->
<script src="{{ asset('js/contactForm.js') }}"></script>
<script src="{{ asset('js/jquery.blockUI.js') }}"></script>
<script>
    $().ready(function () {
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-2064965-2', 'mallguide.co.za');
        ga('send', 'pageview');

        $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    })


</script>


<script>

    $(document).ready(function()
    {
        $("select[name='portalSwitch']").change(function ()
        {
            var switchType=$(this).data('type')
            var theID=$(this).val();
            window.location = '/switch/'+switchType+'/'+theID ;
        });
    });
</script>



<!-- extra assets -->
@yield('exAssets')


<!-- extra javascript -->

@yield('exScript')

</body>
</html>