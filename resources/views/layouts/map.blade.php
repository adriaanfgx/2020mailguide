<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="/favicon.ico">

    <title>Mall Map</title>



    <link rel="stylesheet" href="{{ asset('css/style.css') }}">


</head>

<body style="margin:0;padding:0">

@yield('content')

<!-- Javascripts
		================================================== -->
<script src="{{ asset('js/jquery.min.js') }}"></script>

<script src="{{ asset('js/jquery-ui.js') }}"></script>
<!-- extra assets -->

<!-- extra javascript -->
<script>

    $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
    function centerBootboxVertically() {
        var dialogHolder = $('.bootbox');
        dialogHolder.css({ marginTop: ($(window).height() - dialogHolder.height()) / 2 });
    }
</script>



@yield('exScript')

</body>
</html>
