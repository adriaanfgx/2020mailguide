<?php
$userSession = Session::get('mgUser');
//dd($userSession);
$user =  \Cartalyst\Sentinel\Laravel\Facades\Sentinel::getUser();
if(Session::has('activeShopID'))
{
    //die("HIO");
    $activeShopID= Session::get('activeShopID');
    $userShopsDropList=$userSession['shops'];
    $activeShop=$userShopsDropList[$activeShopID];
}
if(Session::has('activeMallID'))
{
    $activeMallID= Session::get('activeMallID');
    $userMallDropList=$userSession['malls'];
    $activeMall=$userMallDropList[$activeMallID];
}
if(Session::has('activeChain'))
{
    $activeChain= Session::get('activeChain');
    $userChainDropList=$userSession['chains'];
}
?>

@if(isset($activeMallID)  || isset($activeShopID) || isset($activeChain)  )
    <br/>
    <div class="row-fluid">
        <div class="span12">
            <h5 class="colorBlue pull-left">FILTER</h5>
            <h5 class="blueH">
                @if(isset($activeMallID))
                    <span class="leftMargin rightMargin">&nbsp; Viewing content for Mall:</span>
                    {{Form::select('portalSwitch',$userMallDropList,$activeMallID,array('class'=>'span3 navDrop','data-type'=>'mall', 'style'=>'margin-top:5px;'))}}
                @endif
                @if(isset($activeShopID))
                    <span class="leftMargin rightMargin">&nbsp; Viewing content for Shop:</span>
                    {{Form::select('portalSwitch',$userShopsDropList,$activeShopID,array('class'=>'span3 navDrop','data-type'=>'shop', 'style'=>'margin-top:5px;'))}}
                @endif
                @if(isset($activeChain))
                    <span class="leftMargin rightMargin">&nbsp; Viewing content for Chain:</span>
                    {{Form::select('portalSwitch',$userChainDropList,$activeChain,array('class'=>'span3 navDrop','data-type'=>'chain', 'style'=>'margin-top:5px;'))}}
                @endif
            </h5>
        </div>
    </div>
@endif
