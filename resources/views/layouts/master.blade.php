<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Mallguide</title>

    <meta name="title" content="Mallguide" />
    <meta name="copyright" content="Mallguide" />
    <meta name="classification" content="Internet" />
    <meta name="content-language" content="en" />
    <meta name="DateModified" content="2019-10-11 12:52:47" />

    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/images/favicons/apple-touch-icon.png')}}" />
    <link rel="icon" type="image/png" href="{{asset('assets/images/favicons/favicon-32x32.png')}}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{asset('assets/images/favicons/favicon-16x16.png')}}" sizes="16x16" />
    <link rel="manifest" href="{{asset('assets/images/favicons/manifest.json')}}" />
    <link rel="mask-icon" href="{{asset('assets/images/favicons/safari-pinned-tab.svg')}}" color="#000000" />
    <link rel="shortcut icon" href="{{asset('assets/images/favicons/favicon.ico')}}" />
    <meta name="msapplication-config" content="/assets/images/favicons/browserconfig.xml" />
    <meta name="theme-color" content="#ffffff" />

    <!-- Bootstrap core CSS -->
    <link href="{{asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/css/layout.css')}}" rel="stylesheet">
    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="{{asset('assets/css/owlcarousel/assets/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/owlcarousel/assets/owl.theme.default.min.css')}}">
</head>
@include('partials.headers.menu')
<body class="homepage">
<main role="main">
    <div class="container-fluid">
    @include('partials.headers.header')
    <!-- Example row of columns -->
        <div class="p-50"></div>
        <div class="row">
            <div class="col-md-12 col-lg-3">
                @include('partials.headers.search2')
            </div>
           @yield('snippets')
            <div class="col-md-12 col-lg-4"></div>
        </div>
        @yield('content')
        <hr>
    </div> <!-- /container -->
</main>
@include('partials.headers.footer')
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="/assets/js/bootstrap.bundle.min.js" integrity="sha384-6khuMg9gaYr5AxOqhkVIODVIvm9ynTT5J4V1cfthmT+emCG6yVmEZsRHdxlotUnm" crossorigin="anonymous"></script>
<script type="text/javascript" src="/assets/js/scripts.js"></script>
<!-- javascript -->
<script src="/assets/css/owlcarousel/owl.carousel.js"></script>
<script>
    $(document).ready(function() {
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            items: 2,
            loop: true,
            margin: 20,
            autoplay: true,
            autoplayTimeout: 2000,
            autoplayHoverPause: true,
            responsiveClass:true,
            responsive:{
                0:{
                    items:1,
                    nav:false
                },
                600:{
                    items:2,
                    nav:false
                },
                1000:{
                    items:2,
                    nav:false
                }
            }
        });
    })
</script>
</body>
</html>
