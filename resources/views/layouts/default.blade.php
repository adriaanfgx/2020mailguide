<!DOCTYPE html>
<html>
<head>


    <title>{{ $seo_title }}</title>
    <meta name="description" content="{{ $seo_description }}">
    {{--<meta name="keywords" content="{{ $seo_keywords }}">--}}

    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

    <meta name="abstract" content="Mallguide" />
    <meta name="copyright" content="Mallguide" />
    <meta name="classification" content="Internet" />
    <meta name="content-language" content="en" />
    <meta http-equiv="Author" name="Author" content="FGX Studios" />

    <META NAME="Title" CONTENT="Mallguide">
    <META NAME="Creator.Address" CONTENT="FGX">
    <META NAME="Subject" CONTENT="Mall Listings, Store Listings, Movie Listings">
    <META NAME="Description" CONTENT="Mallguide offers the local and international visitors a free resource portal, to overview everything current in retail shopping by drawing together all the information offered by all the individual Malls in the country into one easy to use package.">
    <META NAME="Date.Created" CONTENT="2014-02-19">
    <META NAME="Date.Modified" CONTENT="2014-02-19">
    <META NAME="Type" CONTENT="text">
    <META NAME="Format" CONTENT="text/html">
    <META NAME="Language" CONTENT="en">

    <META NAME="geo.placename" CONTENT="Johannesburg, South Africa">
    <META NAME="geo.region" CONTENT="ZA-ON">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="apple-touch-icon" href="/apple-touch-icon.png" />
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72-precompressed.png" />
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114-precompressed.png" />

    <!-- Bootstrap -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/capture.css') }}" rel="stylesheet">
    <link href="{{ asset('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">

    <link href="{{ asset('css/over_write.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.css') }}" rel="stylesheet">
    <link href="{{ asset('css/liquid-slider.css') }}" rel="stylesheet" >
    <link href="{{ asset('css/magnific-popup.css') }}" rel="stylesheet" />

    <script type="text/javascript" src="{{ asset('js/jquery-1.10.2.min.js') }}"></script>

    <link href='https://fonts.googleapis.com/css?family=Raleway:400,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,900,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>

</head>

<body class="box-bg">

<div class="container">
    <div id="header" class="fixed-header-container">

        <div class="navbar">
            <div class="navbar-inners">
                <div class="container">
                    <div class="nav-container-outer">
                        <div class="nav-container-inner">

                            <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <div class="logo pull-left">
                                <a class="brand" href="/"><img src="{{ asset('img/mall_logo.png') }}" alt="Mall Guide"></a>
                            </div><!--/logo-->

                        @include('partials/admin_status')


                        <!--/search-->
                            @include('layouts.menu')

                        </div>
                    </div>
                </div><!--/container-->
            </div><!--/navbar inner-->
        </div><!--/navbar-->
    </div><!--/header-->

</div><!--/container-->

<div class="container intro_img">
    <div class="row">
        <div class="span12">
            @include('partials.headers.search')
            @yield('headingText')
        </div><!--/span12-->
    </div><!--/row-->
</div><!--/header-->

<div class="container">
    <div class="three">

        <div class="row">
            <div class="span4 p-l-5">
                <div><h4>Listing 1400+ Malls</h4>
                    <p>All the Mall info in one place. <a href="/malls">Get Mall</a> or <a href="/shops">Store</a>  information in one destination...</p>
                </div>
            </div><!--/span4-->
            <div class="span4">
                <div>
                    <h4>What's happening?</h4>
                    <p>We keep up to date with <a href="/events">events</a>, <a href="/competitions">competitions</a>, <a href="/movies">movies</a> &amp; <a href="/exhibitions">exhibitions</a>. Mallguide, for people in the know...</p>
                </div>
            </div><!--/span4-->
            <div class="span3 span3-extended-ten">

                <div>
                    <h4>We like you! Like us Back?</h4>
                    <p>Get all things retail to you, in one location. Why not show us some love and <a href="https://www.facebook.com/mallguide" target="_blank">like us on Facebook.</a></p>
                </div>
            </div><!--/span4-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->

<div class="white">
    <div class="container">


        <!-- Notifications -->
    @include('notifications')
    <!-- ./ notifications -->

        <!-- Content -->
    @yield('content')
    <!-- ./ content -->


    </div><!-- container-fluid -->
</div><!-- white -->

{{--@include('partials/footer')--}}

<!--Plugins-->

<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/twitter-bootstrap-hover-dropdown.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/filter.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/capture.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/fixed-header.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/modernizr.custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/testimonials.js') }}"></script>


<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script src="{{ asset('js/select2.js') }}"></script>
<script src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('js/jquery.colorbox-min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.fancybox.pack.js?v=2.1.5') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery.timepicker.js?v=2.1.5') }}"></script>
<script type="text/javascript" src="{{ asset('js/jquery-ui-1.10.4.custom.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('js/jquery.numeric.js') }}"></script>
<script src="{{ asset('js/jquery.format-1.3.min.js') }}"></script>
<script src="{{ asset('js/jquery.maskedinput.js') }}"></script>
<!--Custom javascript-->
<script src="{{ asset('js/contactForm.js') }}"></script>
<script src="{{ asset('js/jquery.blockUI.js') }}"></script>


<!-- Slider -->
<script src="{{ asset('js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('js/jquery.touchSwipe.min.js') }}"></script>
<script src="{{ asset('js/jquery.liquid-slider.min.js') }}"></script>
<script src="{{ asset('js/sitesearch.js') }}"></script>
<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>


<script>
    (function($) {

        $(document).ready(function()
        {
            $('#main-slider').liquidSlider({includeTitle: false, dynamicTabsAlign: "center",mobileNavigation:"true",mobileNavDefaultText: "FEATURED"});

            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-2064965-2', 'mallguide.co.za');
            ga('send', 'pageview');

            $(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);

        }); // Close .read()
    })(jQuery);



    /*
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-2064965-2', 'mallguide.co.za');
 ga('send', 'pageview');
*/
</script>

<!-- extra assets -->
@yield('exAssets')


<!-- extra javascript -->

@yield('exScript')


</body>
</html>
