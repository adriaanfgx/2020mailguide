<html lang="en">
<head>
    <title>{{ config('app.name', 'Mallguide️') }}</title>
    <meta charset="utf-8">
    <meta name="robots" content="nofollow,noindex">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link
        type="text/css"
        rel="stylesheet"
        href="//unpkg.com/bootstrap-vue@latest/dist/bootstrap-vue.min.css"
    />
    <link href="https://unpkg.com/vue-bootstrap-typeahead/dist/VueBootstrapTypeahead.css" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/vue-instant.css') }}" rel="stylesheet">
    <link href="{{ asset('css/select2.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/colorbox.css') }}" rel="stylesheet"/>
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
<nav class="navbar navbar-expand-lg fixed-top navbar-dark bg-dark">
    <a class="navbar-brand mr-auto mr-lg-3" href="/admin/malls">MG</a>
    <div class="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">Malls</a>
                <div class="dropdown-menu" aria-labelledby="dropdown01">
                    <a class="dropdown-item" href="/admin/malls">List Malls</a>
                    <a class="dropdown-item" href="/admin/events">Events</a>
                    <a class="dropdown-item" href="/admin/exhibitions">Exhibitions</a>
                    <a class="dropdown-item" href="/admin/jobs">Jobs</a>
                    <a class="dropdown-item" href="/admin/maps">Maps</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">Shops</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/admin/shops">List Shops</a>
                    <a class="dropdown-item" href="/admin/categories">Categories</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/admin/promotions">Promotions</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">Competitions</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="/admin/competitions">List Competitions</a>
                    <a class="dropdown-item" href="/admin/competitions/create">Create a Competition</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" href="/admin/users">Users</a>
            </li>
        </ul>
        <div class="btn-group">
            <img class="dropdown-toggle"
                 data-toggle="dropdown"
                 aria-haspopup="true"
                 aria-expanded="false"
                 style="margin: 12px"
                 src="/img/flags/{{ session()->get('country_iso', 'za') }}.png">
            <div id="change-country" class="dropdown-menu dropdown-menu-right" style="font-size: .8rem;">
                <h6 class="dropdown-header">Set your location</h6>
                <a class="dropdown-item"
                   :href="'/change-country/' + option.id"
                   v-bind:class="{ active: option.id == {{ session()->get('country_id', 190) }}}"
                   v-for="option in countries">
                    @{{ option.name }}
                </a>
            </div>
        </div>
        <div class="nav navbar-nav btn-group">
            <div style="" class="nav-item nav-link dropdown-toggle"
                 data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                <svg
                    class="SVGInline-svg SVGInline--cleaned-svg SVG-svg Icon-svg Icon--person-svg SVG--color-svg SVG--color--gray300-svg"
                    style="width: 18px;height: 18px;background-color: white;padding: 2px;border-radius: 50%;"
                    height="16" viewBox="0 0 16 16" width="16"
                    xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M8 11a5.698 5.698 0 0 0 3.9-1.537l1.76.66A3.608 3.608 0 0 1 16 13.5V15a1 1 0 0 1-1 1H1a1 1 0 0 1-1-1v-1.5c0-1.504.933-2.85 2.34-3.378l1.76-.66A5.698 5.698 0 0 0 8 11zM7.804 0h.392a3.5 3.5 0 0 1 3.488 3.79l-.164 1.971a3.532 3.532 0 0 1-7.04 0l-.164-1.97A3.5 3.5 0 0 1 7.804 0z"
                        fill-rule="evenodd">
                    </path>
                </svg>
            </div>
            <div class="dropdown-menu dropdown-menu-right">
                <a class="dropdown-item" href="#">Profile</a>
                <a class="dropdown-item" href="#">Switch to Mallguide</a>
                <span class="divider"></span>
                <a class="dropdown-item" href="{{ route('user.logout') }}">
                    {{ __('Logout') }}
                </a>
            </div>
        </div>
    </div>
</nav>
<div class="nav-scroller bg-white shadow-sm" style="visibility: hidden">
    <nav class="nav nav-underline">
        <span class="nav-link active">Quick Links</span>
        <a class="nav-link" href="#">Link</a>
        <a class="nav-link" href="#">Link</a>
        <a class="nav-link" href="#">Link</a>
    </nav>
</div>

@yield('content')
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min-v4.3.1.js') }}"></script>
<script src="{{ asset('js/vue.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
<script src="{{ asset('js/bootstrap-vue.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/vue-bootstrap-typeahead.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/change-country.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.maskedinput.js') }}"></script>
<script src="{{ asset('js/select2.js') }}"></script>
<script src="{{ asset('js/jquery.formatUrl-1.0.5.js') }}"></script>
@yield('javascript')

</body>
</html>
