<?php
$userSession = session()->get('mgUser');
$user = Sentinel::getUser();
if (session()->has('activeShopID')) {
    $activeShopID = session()->get('activeShopID');
    $userShops = session()->get('shops');
//    $activeShop = Shop::find(session()->get('activeShopID'));
}
if (session()->has('activeMallID')) {
    $activeMallID = session()->get('activeMallID');
}
if (session()->has('activeChain')) {
    $activeChain = session()->get('activeChain');
}

$activeTab = Request::segment(1);

?>

@if( Sentinel::check() && session()->get('back')==='yes')
    <div class="nav-collapse">
        @if(isset($activeMallID) && Sentinel::hasAccess('manager'))

            <ul class="nav pull-right">

                <li><a class="muted noLink" href="javascript:void(0);">Mall Menu</a></li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shops<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('shops.register') }}">Add a Shop</a>
                        </li>
                        <li>
                            <a href="{{ route('shops.list') }}">List Shops</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Exhibitions<b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('exhibitions.create') }}">Add Exhibition</a>
                        </li>
                        <li>
                            <a href="{{ route('exhibitions.list') }}">List Exhibitions</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Events<b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('mallevents.create') }}">Add Event</a>
                        </li>
                        <li>
                            <a href="{{ route('mallevents.list') }}">List Events</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Promotions<b class="caret"></b></a>
                    <ul class="dropdown-menu ">
                        <li>
                            <a href="{{ route('mallpromotions.create') }}">Add Mall Promotion</a>
                        </li>
                        <li>
                            <a href="{{ route('mallpromotions.list') }}">View Mall Promotions</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Jobs<b class="caret"></b></a>
                    <ul class="dropdown-menu ">
                        <li>
                            <a href="{{ route('jobs.create') }}">Add Mall Job</a>
                        </li>
                        <li>
                            <a href="{{ route('jobs.index') }}">View Mall Jobs</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gift Ideas<b class="caret"></b></a>
                    <ul class="dropdown-menu ">
                        <li>
                            <a href="{{ route('store.addgiftidea') }}">Add Gift Idea</a>
                        </li>
                        <li>
                            <a href="{{ route('store.giftideas') }}">List Gift Ideas</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('booking.featured') }}">Featured Bookings</a>
                </li>

                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Users<b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('portal.adduser') }}">Add a User</a>
                        </li>
                        <li>
                            <a href="{{ route('portal.listusers') }}"> List Users</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Competitions<b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('competitions.create') }}">Add Competitions</a>
                        </li>
                        <li>
                            <a href="{{ route('competitions.list') }}">View Competitions</a>
                        </li>
                    </ul>
                </li>
{{--                <li>--}}
{{--                    <a href="{{ route('invoices') }}">xInvoices</a>--}}
{{--                </li>--}}
            </ul>
        @endif

        @if(isset($activeChain))
            <ul class="nav pull-right">
                <li><a class="muted noLink" href="javascript:void(0);">Chain Menu</a></li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Shops<b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('chainshop.create') }}">Add a Shop</a>
                        </li>
                        <li>
                            <a href="{{ route('chainshop.chainstores') }}">List Shops</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Promotions<b class="caret"></b></a>
                    <ul class="dropdown-menu ">
                        <li>
                            <a href="{{ route('chainshop.createpromotion') }}">Add Shop Chain Promotion</a>
                        </li>
                        <li>
                            <a href="{{ route('chainshop.promotions') }}">View Shop Chain Promotions</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Jobs<b class="caret"></b></a>
                    <ul class="dropdown-menu ">
                        <li>
                            <a href="{{ route('chainshop.createjob') }}">Add Shop Chain Job</a>
                        </li>
                        <li>
                            <a href="{{ route('chainshop.jobs') }}">View Shop Chain Jobs</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gift Ideas<b class="caret"></b></a>
                    <ul class="dropdown-menu ">
                        <li>
                            <a href="{{ route('store.addgiftidea') }}">Add Gift Idea</a>
                        </li>
                        <li>
                            <a href="{{ route('store.giftideas') }}">List Gift Ideas</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('booking.featured') }}">Featured Bookings</a>
                </li>

                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Users<b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('chainshop.adduser') }}">Add a User</a>
                        </li>
                        <li>
                            <a href="{{ route('chainshop.listusers') }}"> List Users</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Competitions<b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('competitions.create') }}">Add Competitions</a>
                        </li>
                        <li>
                            <a href="{{ route('competitions.list') }}">View Competitions</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('invoices') }}">Invoices</a>
                </li>
            </ul>
        @endif

        @if(Sentinel::hasAccess('independent'))

            <ul class="nav pull-right">
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Movie Schedules<b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="{{ route('moviehouse.addmovieschedule') }}">Add Movie Schedules</a>
                        </li>
                        <li>
                            <a href="{{ route('moviehouse.listmovieschedules') }}">List Movie Schedules</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Competitions<b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('competitions.create') }}">Add Competitions</a>
                        </li>
                        <li>
                            <a href="{{ route('competitions.list') }}">View Competitions</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('invoices') }}">Invoices</a>
                </li>
            </ul>
        @endif

        @if(isset($activeShopID))

            <ul class="nav pull-right">
                <li><a class="muted noLink" href="javascript:void(0);">Shop Menu</a></li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Promotions<b class="caret"></b></a>
                    <ul class="dropdown-menu ">
                        <li>
                            <a href="{{ route('mallpromotions.create') }}">Add Shop Promotion</a>
                        </li>
                        <li>
                            <a href="{{ route('mallpromotions.list') }}">View Shop Promotions</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Jobs<b class="caret"></b></a>
                    <ul class="dropdown-menu ">
                        <li>
                            <a href="{{ route('store.createStoreJob') }}">Add Job</a>
                        </li>
                        <li>
                            <a href="{{ route('store.jobs') }}">View Jobs</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Gift Ideas<b class="caret"></b></a>
                    <ul class="dropdown-menu ">
                        <li>
                            <a href="{{ route('store.addgiftidea') }}">Add Gift Idea</a>
                        </li>
                        <li>
                            <a href="{{ route('store.giftideas') }}">List Gift Ideas</a>
                        </li>
                    </ul>
                </li>

                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Users<b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('store.adduser') }}">Add a User</a>
                        </li>
                        <li>
                            <a href="{{ route('store.userlist') }}"> List Users</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown ">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Competitions<b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a href="{{ route('competitions.create') }}">Add Competitions</a>
                        </li>
                        <li>
                            <a href="{{ route('competitions.list') }}">View Competitions</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="{{ route('invoices') }}">Invoices</a>
                </li>
            </ul>
        @endif
    </div>
@else
    <div class="nav-collapse">
        <ul class="nav pull-right">
            <li class="@if($activeTab=='') active @endif">
                <a href="{{ route('home.index') }}"><i class="fa fa-home fa-lg"></i></a>
            </li>

            <li class=" @if($activeTab=='malls') active @endif">
                <a href="{{ route('malls.index') }}">Malls</a>
            </li>
            <li class=" @if($activeTab=='shops') active @endif">
                <a href="{{ route('shops.index') }}">Shops</a>
            </li>
            <li class=" @if($activeTab=='movies') active @endif">
                <a href="{{ route('movies.index') }}">Movies</a>
            </li>
            <li class=" @if($activeTab=='events') active @endif">
                <a href="{{ route('events.index') }}">Events</a>
            </li>
            <li class=" @if($activeTab=='exhibitions') active @endif">
                <a href="{{ route('exhibitions.index') }}">Exhibitions</a>
            </li>
            <li class=" @if($activeTab=='competitions') active @endif">
                <a href="{{ route('competitions.index') }}">Competitions</a>
            </li>
            <li class=" @if($activeTab=='promotions') active @endif">
                <a href="{{ route('promotions.list') }}">Promotions</a>
            </li>
            <li class=" @if($activeTab=='jobs') active @endif">
                <a href="{{ route('jobs.list') }}">Jobs</a>
            </li>
            <li>
                <a href="{{ url('http://www.mallguide.co.za/blog') }}" title="Blog"><i
                        class="fa fa-comments-o fa-lg"></i></a>
            </li>

        </ul>
    </div>

@endif
