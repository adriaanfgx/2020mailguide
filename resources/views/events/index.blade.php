@extends('layouts.pages')
@section('heading')
    <?php //print_r($allMalls);exit(); ?>
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            @include('partials.headers.search')
            <div class="row-fluid">
                @include('partials.headers.search')
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Events: </span>Upcoming <span>Events</span></h1>

                    <p class="animated fadeInDown delay2">What's happening in an area around you?</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}

                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->

@stop

@section('content')


    <div class="row-fluid">
        <div class="hborder">
            <h4 class="paddingBottom">Events</h4>
            <div class="row-fluid">
                <div class="span3 offset2">
                    <div class="overlay-wrapper"></div>
                    <select name="provinces" id="provinces">
                        <option value="">Please select ...</option>
                        @foreach( $provinces as $key=>$value )
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                <?php $mallSelected = session()->get('selected_mall',''); ?>
                <div class="span3">
                    <div class="overlay-wrapper"></div>
                    {{ Form::select('mall_id',$malls,$mallSelected,array('id'=>'mall_id','disabled')) }}
                    <?php session()->forget('selected_mall'); ?>
                </div>
                <div class="span3">
                    <div class="overlay-wrapper"></div>
                    <div class="input-append">
                        <input type="text" name="Search" value="" id="search_string" placeholder="Keyword Search"/>
                        <button type="submit" id="search" class="btn btn-info" id="shopsearch"><i class="icon-search"></i></button>
                    </div>
                </div>
            </div>
        </div>


        <div class="clear"></div>

        <div class="row-fluid">
            <div class="span12">
                <table class="table table-bordered table-striped responsive events-table">
                    <thead>
                    <tr>
                        <th>Mall</th>
                        <th>Event Name</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                    </tr>
                    </thead>
                    <tbody id="event_data">
                    @if(isset($events) && count($events))
                        @foreach($events as $event)
                            <tr>
                                <td><a href="/malls/view/{{ $event->mallID }}/{{ slugify($malls[$event->mallID]) }}">{{ $malls[$event->mallID] }}</a></td>
                                <td><a href="/events/event/{{ $event->eventsMGID }}/{{ slugify($event->name) }}">{{ $event->name }}</a></td>
                                <td>{{ $event->startDate }}</td>
                                <td>{{ $event->endDate }}</td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="4" class="text-center">Please select a province to see a list of events...</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div>
            <!--span12 -->
        </div>


    </div>
@stop


<!-- extra javascript -->

@section('exScript')
    <script type="text/javascript">
        var allMalls = [];
        @foreach($malls as $key => $value )
        <?php echo 'allMalls['.$key.'] = "'. $value .'";'; ?>
        @endforeach
        jQuery(document).ready(function () {


            if( $("#provinces").val() != '' ){

                $('#mall_id').attr('disabled',false);
                //Get shops for the pre-selected mall
                getEventsForSelectedMall();
            }else{
                $('#mall_id').attr('disabled','disabled');
                $('#mall_id').val('Select a province ...');
            }

            $("#mall_id").change(function () {

                if ($(this).val() == '') {
                    if( $("#provinces").val() != '' ){
                        refreshMallList();
                    } else {
                        $('#event_data').html('<tr><td colspan="5" class="text-center">Please select a mall to see a list of Events!</td></tr>');
                    }
                } else {
                    getEventsForSelectedMall();
                }


            });

            $("#provinces").change(function () {
                refreshMallList();
            });

            $("#search").click(function () {

                if ($("#search_string").val() === '') {
                } else {

                    var string = $("#search_string").val();
                    var url = 'events/search/' + string;
                    $("#event_data").empty();
                    $.ajax({
                        url: url,
                        data: {},
                        dataType: 'json',
                        success: function (data) {

                            $("#event_data").empty();
                            var newData = data.data;

                            if (newData.length == 0) {
                                $('#event_data').html('<tr><td colspan="5" class="text-center">There is no data for that search term...</td></tr>');
                            } else {
                                listEvents(newData);
                                /*$.each(newData, function (j, e) {

                                    var str = e.name;
                                    var m_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

                                    var start = new Date(e.startDate);
                                    var end = new Date(e.endDate);
                                    var start_date = start.getDate();
                                    var get_start_month = start.getMonth();
                                    var start_month = m_names[get_start_month];
                                    var start_year = start.getFullYear();
                                    var fin_date = end.getDate();
                                    var get_month = end.getMonth();
                                    var fin_month = m_names[get_month];
                                    var fin_year = end.getFullYear();
                                    $("#event_data").append($('<tr>').html("<td><a href='/malls/view/"+e.mallID+"/"+slugify(allMalls[e.mallID])+"'>" + allMalls[e.mallID] + "</a></td><td><a href='/events/event/" + e.eventsMGID + "/" + slugify(str) + "'>" + e.name + "</a></td><td>" + start_date + ' ' + start_month + ' ' + start_year + "</td><td>" + fin_date + ' ' + fin_month + ' ' + fin_year + "</td>"));
                                });*/
                            }

                            $(".pagination").hide();


                        }
                    });
                }

            });
        });

        function refreshMallList() {
            var $province = $('#provinces').val();
            if ($province == '') {
                $('#mall_id').empty().append('<option value="">Please Select a Province...</option>');
                $('#mall_id').attr('disabled', 'disabled');
            } else {
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    url: '/malls/eventfilter/' + $province,
                    success: function (data) {
                        $('#mall_id').empty().append('<option value="">Please select a Mall...</option>');
                        /*var _malls;
                        if(data.malls){
                            _malls = data.malls;
                        } else if(data.data){
                            _malls = data.data;
                        }*/
                        if(data.malls.length > 0){
                            $.each(data.malls, function (i, val) {
                                $('#mall_id').append('<option value="' + val.mallID + '">' + val.name + '</option>');
                            });
                        }

                        $('#mall_id').removeAttr('disabled');
                        $('#event_data').html('<tr><td colspan="5" class="text-center">Please select a mall to see a list of Events!</td></tr>');

                        if(data.events.length){
                            //console.log($.parseJSON(data.events));
                            var _evtJ = $.parseJSON(data.events);
                            if(_evtJ.data){
                                getEventsForSelectedMall(_evtJ.data);
                            }
                        } else {
                            $('#event_data').html('<tr><td colspan="5" class="text-center">There are no current events in the selected province!</td></tr>');
                        }
                    }
                });
            }

        }

        function getEventsForSelectedMall(_events){
            if(!_events){
                var mallID = $("#mall_id").val();
                $.ajax({
                    url: '/events/mallEvents/' + mallID,
                    data: {},
                    dataType: 'json',
                    success: function (data) {

                        $("#event_data").empty();
                        var newData = data.data;

                        if (newData.length == 0) {
                            $('#event_data').html('<tr><td colspan="5" class="text-center">There is no data for selected mall...</td></tr>');
                        } else {
                            listEvents(newData);
                        }


                    }

                });
            } else {
                listEvents(_events);
            }
        }

        function listEvents(_evt){
            if(_evt){
                $('#event_data').html('');
                $.each(_evt, function (j, e) {
                    //console.log(e);
                    var str = e.name;
                    var mallStr = allMalls[e.mallID];
                    var m_names = new Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");

                    var start = new Date(e.startDate);
                    var end = new Date(e.endDate);
                    var start_date = start.getDate();
                    var get_start_month = start.getMonth();
                    var start_month = m_names[get_start_month];
                    var start_year = start.getFullYear();
                    var fin_date = end.getDate();
                    var get_month = end.getMonth();
                    var fin_month = m_names[get_month];
                    var fin_year = end.getFullYear();


                    $("#event_data").append($('<tr>').html("<td><a href='/malls/view/"+e.mallID+"/"+slugify(allMalls[e.mallID])+"'>" + allMalls[e.mallID] + "</a></td><td><a href='/events/event/" + e.eventsMGID + "/" + slugify(str) + "'>" + e.name + "</a></td><td>" + start_date + ' ' + start_month + ' ' + start_year + "</td><td>" + fin_date + ' ' + fin_month + ' ' + fin_year + "</td>"));

                });
            }
        }
    </script>
@stop
