@extends('layouts.pages')


@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            @include('partials.headers.search')
            <div class="row-fluid">
                <div class="span8">
                    <h1 class="animated fadeInDown delay1"><span>Events: </span>View <span>Event</span> Information</h1>

                    <p class="animated fadeInDown delay2">Want to enjoy an event?</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->
@stop

@section('content')

    <div class="row-fluid hidden-phone" id="filterSection_menu">
        <div class="span12"><!--sort wrap -->
        </div>
        <!--span12 -->
    </div><!--row -->

    <div id="filterSection" data-perrow="4" class="row-fluid">


        <div class="span12 pull-left">
            <h4 class="hborder">Event Details</h4>
        </div>


        <div class="clear"></div>

        <br>

        <div class="row-fluid">
            <ul class="thumbnails">
                <li class="span2">
                    <div>
                        <?php
                        $event_img = (!empty($event->thumbnail1)) ? '/uploadimages/mall_' . $event->mallID . '/' . $event->thumbnail1 : 'img/temp_logo.gif';
                        ?>
                        <div class="zoom-gallery">
                            <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $event->mallID }}/{{ $event->image1 }}"
                               title="Event">
                                <img src="{{ Config::get('app.url') }}/{{ $event_img }}" alt="{{ $event->name }}"/>
                            </a>
                            @if( !empty($event->altImage1))
                                <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $event->mallID }}/{{ $event->altImage1 }}"
                                   title="Alternate Image 1" class="gallery-image">
                                    <img
                                        src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $event->mallID }}/{{ $event->altImage1 }}"
                                        alt="{{ $event->name }}"/>
                                </a>
                            @endif
                            @if( !empty($event->altImage2))
                                <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $event->mallID }}/{{ $event->altImage2 }}"
                                   title="Alternate Image 2" class="gallery-image">
                                    <img
                                        src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $event->mallID }}/{{ $event->altImage2 }}"
                                        alt="{{ $event->name }}"/>
                                </a>
                            @endif
                            @if( !empty($event->altImage3))
                                <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $event->mallID }}/{{ $event->altImage3 }}"
                                   title="Alternate Image 3" class="gallery-image">
                                    <img
                                        src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $event->mallID }}/{{ $event->altImage3 }}"
                                        alt="{{ $event->name }}"/>
                                </a>
                            @endif
                        </div>
                        <div class="overlay"></div>
                    </div>

                </li>
                <li class="span10">
                    <div class="overlay-wrapper">

                        <div class="overlay"></div>
                    </div>

                    <div class="blog-content border_top">
                        <div class="mall-caption">
                            <h4 class="hborder">{{ $event->name }}</h4>
                            <dl>
                                <dd>{{ stripTagsContent($event->event, '<br>') }}</dd>
                                <dt>Start Date</dt>
                                <dd>{{ date('j F Y', strtotime($event->startDate)) }}</dd>
                                <dt> End Date</dt>
                                <dd>{{ date('j F Y', strtotime($event->endDate)) }}</dd>
                                <dt>Mall</dt>
                                <dd>
                                    <a href="/malls/view/{{ $mall->mallID }}/{{ slugify($mall->name) }}">{{ $mall->name }}</a>
                                </dd>
                                @if($event->fileName)
                                    <dd>
                                        <a target="_blank"
                                           href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $mall->mallID }}/{{ $event->fileName }}">Terms
                                            &amp; Conditions apply</a>.
                                    </dd>
                                @endif
                            </dl>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="text-right">
                <button class="btn btn-inverse go_back">&laquo; Go back</button>
            </div>
        </div>
    </div>
@stop

@section('exScript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.zoom-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: true,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function (element) {
                        return element.find('img');
                    }
                }
            });
        });
    </script>
@stop
