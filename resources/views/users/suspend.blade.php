@extends('layouts.admin')

{{-- Web site Title --}}
@section('title')
@parent
Suspend User
@stop

{{-- Content --}}
@section('content')
<div class="row">
    <div class="span4 offset4">
        {{ Form::open(array('action' => array('suspendUserForm', $id), 'method' => 'post')) }}
 
            <h2>Suspend User</h2>

            <div class="control-group {{ ($errors->has('suspendTime')) ? 'has-error' : '' }}">
                {{ Form::text('suspendTime', null, array('class' => 'form-control', 'placeholder' => 'Suspend Time', 'autofocus')) }}
                <span class="help-block">{{ ($errors->has('suspendTime') ? $errors->first('suspendTime') : '') }}</span>
            </div>    	   

            {{ Form::hidden('id', $id) }}

            {{ Form::submit('Suspend User', array('class' => 'btn btn-primary')) }}
            
        {{ Form::close() }}
    </div>
</div>

@stop