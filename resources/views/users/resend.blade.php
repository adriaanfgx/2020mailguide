@extends('layouts.admin')

{{-- Web site Title --}}
@section('title')
@parent
Resend Activation
@stop

{{-- Content --}}
@section('content')
<div class="row">
    <div class="span4 offset4">
        {{ Form::open(array('action' => 'UserController@resend', 'method' => 'post')) }}
        	
            <h2>Resend Activation Email</h2>
    		
            <div class="control-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
                {{ Form::text('email', null, array('class' => 'form-control', 'placeholder' => 'E-mail', 'autofocus')) }}
                {{ ($errors->has('email') ? $errors->first('email') : '') }}
            </div>

            {{ Form::submit('Resend', array('class' => 'btn btn-primary')) }}

        {{ Form::close() }}
    </div>
</div>

@stop
