@extends('layouts.pages')

{{-- Content --}}
@section('content')
<div class="span4">
    <div class="box">
        <div class="box-header">
            <h4 class="dotted-border"><i class="fa fa-user"></i><span class="break"></span> Reset Password</h4>
        </div>
        <div class="box-content">
            <form class="form-horizontal" action="{{ URL::to('users/reset-password') }}" method="post">
                {{ Form::token() }}
                <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}" for="email">
                    <label for="email" style="padding-left: 0px;">E-mail</label>

                    <div class="col-lg-4">
                        <input name="email" id="email" value="{{ Request::old('email') }}" type="text"
                               class="form-control" placeholder="E-mail">
                        {{ ($errors->has('email') ? $errors->first('email') : '') }}
                    </div>
                </div>

                <div class="control-group">
                    <button class="btn btn-primary" type="submit">Reset Password</button>
                </div>
            </form>
        </div>
    </div>
    <!-- .box -->

</div>

@stop