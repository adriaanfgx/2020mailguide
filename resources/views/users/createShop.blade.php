@extends('layouts.pages')

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Shops - </span> Register</h1>
                    <p class="animated fadeInDown delay2">Register a new store</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}

                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
@stop

@section('content')

    <div class="row-fluid">
        <div class="blog-content span12">

            <div class="row-fluid">
                <h3 class="dotted-border"> Shops - Registration </h3>
            </div>
            <div class="row-fluid">
                <h4 class="dotted-border">After your submission</h4>
            </div>
            <div class="row-fluid">
                <div class="span7">
                    Each submission sent from tentants, are logged, and sent to your Mall's Marketing Team or Centre Management. This information is verified with them, and then approved. As soon as this information is approved by them, your store is updated for free. It's as easy as that...<br />
                </div>
                <div class="span5 well well-small">
                    Before entering all your shop details Please make use of our search functionality to make sure your shop is not listed!
                </div>
            </div>

            <h4 class="dotted-border">Mall Information</h4>
            <br />
            {{ Form::open(array('route' => 'shops.postCreateShop', 'method' => 'post','files'=>true,'name' => 'shops.postCreateShop','id' => 'postCreateShop', 'class' => 'form-inline postCreateShop')) }}
            <div class="row" style="padding-left: 30px;">
                <div class="span4">
                    <div class="control-group">
                        <label for="mallID"><strong>Mall Information</strong></label>
                        <div class="controls">
                            {{ Form::select('mallID',$malls,null) }}
                        </div>
                    </div>
                </div>
                <div class="span4"></div>
            </div>
            <h4 class="dotted-border">Shop Information</h4>
            <div class="row" style="padding-left: 30px">
                <div class="span4">
                    <div class="control-group {{ ($errors->has('name') ? 'error' : '') }}">
                        <label for="name" class="control-label"><strong>Shop Name</strong></label>
                        <div class="controls">
                            {{ Form::text('name',null,array('class'=>'txtbar')) }}
                            <span class="help-block">{{ ($errors->has('name') ? $errors->first('name') : '') }}</span>
                        </div>
                    </div>
                </div>
                <div class="span4"></div>
            </div>
            @include('partials/memberzone/shopfields')
            <span>
        <strong>If reservation notifications are sent who must receive them? (If applicable)</strong>
    </span><br />

            Owner&nbsp;{{ Form::checkbox('reserveWith[]','1') }}&nbsp;
            Manager 1&nbsp;{{ Form::checkbox('reserveWith[]','2') }}&nbsp;
            Manager 2&nbsp;{{ Form::checkbox('reserveWith[]','3') }}&nbsp;
            Manager 3&nbsp;{{ Form::checkbox('reserveWith[]','4') }}&nbsp;
            Head Office&nbsp;{{ Form::checkbox('reserveWith[]','5') }}&nbsp;
            Financial Manager&nbsp;{{ Form::checkbox('reserveWith[]','6') }}&nbsp;<br /><br />
            <input class="btn btn-info" type="submit" value="Submit" id="">
            {{ Form::close() }}
        </div><!-- span 12-->
    </div>

@stop
@section('exScript')
    <script type="text/javascript">
        $(document).ready(function(){

            $('.datetimepicker4').datetimepicker({
                pickTime: false
            });

            $("#category").change(function(){
                refreshSubcategoryList();
            });
        });

        function refreshSubcategoryList()
        {

            var category = $('#category').val();

            $.ajax({
                type: 'GET',
                url: '/shops/subcategory/'+category ,
                data: '',
                success: function (data) {

                    $('#subcategory').empty().append('<option value="">Please Select...</option>');
                    $.each(data, function(key, value){
                        $('#subcategory').append('<option value="'+key+'">'+value+'</option>');
                    })
                }
            });
        }

    </script>
@stop