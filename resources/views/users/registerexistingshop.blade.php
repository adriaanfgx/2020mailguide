@extends('layouts.pages')
@section('title')
    @parent
    Shops get access
@stop

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Shops - </span>Get access </h1>
                    <p class="animated fadeInDown delay2">Register as a member of an existing mall on mallguide</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
@stop

@section('content')

    <div class="row-fluid">
        <div class="row-fluid">
            <h3 class="dotted-border"> Sign-up as Mallguide Shop Tenant </h3>
        </div>
        <div class="span6">

            <h4 class="dotted-border">Is your shop already listed on Mallguide? </h4>
            I found my shop listed on Mallguide and now I would like to update the information, what's the next step
            ?<br/>
            Fill in & submit the registration form and as soon as your account has been approved you will receive an
            activation email...<br/>

        </div><!-- span 6-->
        <div class="span5">

            <h4 class="dotted-border">After your submission</h4>
            Each submission sent from tenants, are logged, and sent to your Mall's Marketing Team or Centre
            Management.
            This information is verified with them, and then approved.
            As soon as this information is approved, your store is updated for free.
            It's as easy as that...

        </div><!-- span 6-->

        <div class="padding"></div>
        <div class="row-fluid">
            <div class="span12">
                <h4 class="dotted-border">Mall Information</h4>
                {{ Form::open(array('route' => 'user.signup', 'method' => 'post','name' => 'storeUserSignUp','id' => 'mallSignUp', 'class' => 'form-inline mallSignUp')) }}
                {{ Form::hidden('admin_level','Shop Manager') }}
            </div>
        </div>
        <div class="row-fluid smallPaddingLeft">
            <div class="span6">
                <div class="control-group" id="shop_mall_select">
                    <label for="mallID"><strong>Find Shops by mall</strong></label>
                    <div class="controls">
                        {{ Form::select('shop_mallID[]',$malls,null,array('class'=>'multi-shop_mall','id'=>'shop_mall','multiple')) }}
                    </div>
                </div>
            </div>
            <div class="span5">
                <br/>
                If your mall is not listed, please <a href="mailto:info@mallguide.co.za">Contact the Mallguide
                    team</a> and we'll get back to you
            </div>


        </div>
        <div id="shop_multiselect" class="row-fluid smallPaddingLeft ">
            <div class="span6">
                <div class="control-group">
                    <label for="shopID">Please select your shop*</label>
                    <div class="controls">
                        {{ Form::select('shopMGID[]',$shops,null,array('class'=>'multi-shop','id'=>'multishopMGID','multiple')) }}
                    </div>
                </div>
            </div>
            <div class="span5">
                <br/>
                If your shop is not listed, please <a href="{{ route('shops.createfrontend') }}">Register your
                    Shop</a>
            </div>
        </div>
        <div class="row-fluid">
            <div class="leftMargin">
                @include('partials/loginInfo')
            </div>
        </div>

        {{ Form::close() }}


    </div><!-- span 6-->
    </div><!-- span 6-->
    <div class="clearfix"></div>
    </div>
    </div>
    </div>
    </div>

@stop
@section('exScript')
    <script type="text/javascript">
        $(document).ready(function () {

            $(".numeric").mask('27b99999999');
            $("#shop_multiselect").hide();
            $(".multi-shop").select2({
                allowClear: true
            });
            $(".multi-shop_mall").select2({});
            $('.datetimepicker4').datetimepicker({
                pickTime: false
            });
            multiShop();

            $('#password').keyup(function () {
                var _parent = $(this).parent().parent();

                var level = 0;
                var mesg = '';
                var input = $(this).val();

                if (/(.*[A-Z])/.test(input) == false) {
                    level = 1;
                    mesg += ' No uppercase letters.<br/>';
                }

                if (/(.*[\!|\@|\#|\$|\_])/.test(input) == false) {
                    level = 2;
                    mesg += ' No special character (!@#$_).<br/>';

                }
                if (/(.*[\d])/.test(input) == false) {
                    level = 3;
                    mesg += ' No numbers.<br/>';
                }
                if (/(.*[a-z])/.test(input) == false) {
                    level = 4;
                    mesg += ' No lowercase letters.<br/>';
                }

                if (input.length < 6) {
                    level = 5;
                    mesg += ' Not long enough.<br/>';
                }

                $('#password_help').html(mesg);

                if (level != 0) {
                    _parent.addClass('error');

                    $('#sub').attr('disabled', 'disabled');
                } else {
                    $('#sub').removeAttr('disabled');
                }
            });

        });

        function multiShop(mall) {
            $("#shop_mall").change(function () {
                var mall = $(this).val();
                $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    data: {mallIDs: mall},
                    url: '/admin/shops/getbymall',
                    success: function (data) {

                        $("#shop_multiselect").show();
                        $('#multishopMGID').empty();
                        //clear shops
                        $(".multi-shop").select2('data', null)
                        $.each(data, function (key, value) {
                            $('#multishopMGID').append('<option value="' + key + '">' + value + '</option>');
                        });
                    }
                });
            });
        }
    </script>
@stop