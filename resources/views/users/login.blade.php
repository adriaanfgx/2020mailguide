@extends('layouts.backpages')

@section('seo_meta')
    <meta name="description"
          content="">
    <meta name="keywords" content="">
@stop
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Login </h1>
                    <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp;
                        Communication</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a> <span class="divider">/</span></li>
                        <li class="active">Login</li>

                    </ul>
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
    @include('portal.partials.submenu')

@stop

@section('content')


    <div class="row-fluid">
        <div class="span6">

            <h2 class="dotted-border">Get Access to Mallguide</h2>
            Mallguide offers the local and international visitors a free resource portal, to briefly overview everything
            current
            in retail shopping. With this free tool, you will be able to manage your tenants, promotions, events,
            exhibitions and also
            drive higher volumes of traffic and shoppers to your site and premise...

            <br/><br/>

            <div class="yellow">

                <ul>
                    <li>What free tools does Mallguide offer you as an Existing Mall?</li>
                    <li>Full listing of malls (currently listing 1000+ malls)</li>
                    <li>Full listing of all shops</li>
                    <li>Free listings of current events, promotions and exhibitions, we help you get noticed!</li>
                    <li>Movies, full listings for all cinema's including Ster-Kinekor, Nu Metro and independents</li>
                    <li>Free job listings, we connect quality staff with your tenants</li>
                    <li>Tenant communication, add forums &amp; send sms's and newsletters</li>
                    <li>Approve your own tenant content updates</li>
                </ul>

            </div><!--yellow -->

        </div><!-- span 6-->

        <div class="span5">

            <h2 class="dotted-border">Log In</h2>
            @include('notifications')

            <form role="form" accept-charset="UTF-8" action="/login" method="POST">
                {{ Form::token() }}
                <div class="control-group {{ ($errors->has('email')) ? 'error' : '' }}" for="email">
                    <label class="control-label" for="email">E-mail</label>
                    <div class="controls">
                        <input name="email" id="email" value="{{ Request::old('email') }}" type="text"
                               class="input-large" placeholder="E-mail">
                        <div class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</div>
                    </div>

                </div>

                <div class="control-group {{ $errors->has('password') ? 'error' : '' }}" for="password">
                    <label class="control-label" for="password">Password</label>
                    <div class="controls">
                        <input name="password" value="" class="input-large" type="password">
                        <div
                            class="help-block">{{ ($errors->has('password') ?  $errors->first('password') : '') }}</div>
                    </div>

                </div>

                <div class="control-group">
                    <div class="controls">
                        <button class="btn " type="submit">Log In</button>
                        <br/>
                        <a href="{{ URL::to('users/reset-password') }}" class="btn btn-link">Forgot Password?</a>
                    </div>
                </div>
            </form>
            {{-- Form::close() --}}

        </div><!-- span 6-->

    </div>

@stop

@section('exScript')
    <script>
        $(document).ready(function () {
            $('#email').focus();
        })
    </script>
@stop
