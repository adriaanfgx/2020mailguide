@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop

@section('content')
    <div class="blog-content container-fluid">
        @include('partials/updateUser')
    </div>
@stop
@section('exScript')

    <script type="text/javascript">
        $(document).ready(function () {

            $('.datetimepicker4').datetimepicker({
                pickTime: false
            }).on('changeDate', function (e) {
                $(this).datetimepicker('hide');
            });

            $('#province').change(function () {
                $('#city').empty();
                $.ajax({
                    type: "GET",
                    url: '/admin/provinces/cities/' + $(this).val(),
                    data: '',
                    success: function (data) {
                        //console.log(data);
                        $('#city').html('<option value="">Please Select a City..</option>');
                        $.each(data, function (key, val) {

                            $('#city').append('<option value="' + key + '">' + val + '</option>');
                        });

                    }
                });
            });

            $("#city").change(function () {
                $("#city_text").val($("#city :selected").text());
            });
        });
    </script>
@stop

