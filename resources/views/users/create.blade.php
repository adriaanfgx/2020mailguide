@extends('layouts.admin')
@section('title')
@parent
Add admin users
@stop

@section('content')
<fieldset>
<legend> Add a user </legend>
{{ Form::open(array('route' => 'user.signup', 'method' => 'post','name' => 'postCreateUser','id' => 'postCreateUser', 'class' => 'form-inline')) }}
<div class="accordion" id="accordion2">
    <div class="accordion-group">
        <div class="accordion-heading">
            <h6>
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">Login Details</a>
            </h6>
        </div>
        <div id="collapseOne" class="accordion-body collapse">
            <div class="accordion-inner">
                <div class="row-fluid">
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}">
                            <label class="control-label" for="email"><strong>Email (Compulsory)</strong></label>
                            <div class="controls">
                                {{ Form::text('email',null,array('class'=>'txtbar')) }}
                            </div>
                            <span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ $errors->has('admin_level') ? 'error' : '' }}">
                            <label class="control-label"><strong>Type Of User </strong></label><br />
                            <div class="controls">
                                <select name="admin_level" id="user_type">
                                    <option value="">Select the type of user ...</option>
                                    @foreach ($adminLevel as $level)
                                    <option value="{{ $level }}">{{ $level }}</option>
                                    @endforeach
                                </select>

                                <div class="help-block">{{ ($errors->has('permission') ? $errors->first('permission') : '') }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group" id="mall_select">
                            <label for="mallID"><strong>Select a mall for the user</strong></label>
                            <div class="controls">
                                {{ Form::select('mallID',$malls,null,array('id'=>'mall')) }}
                            </div>
                        </div>
                        <div class="control-group" id="shop_mall_select">
                            <label for="mallID"><strong>Find Shops by mall</strong></label>
                            <div class="controls">
                                {{ Form::select('shop_mallID[]',$malls,null,array('class'=>'multi-shop_mall','id'=>'shop_mall','multiple')) }}
                            </div>
                        </div>
                        <div class="control-group" id="shop_multiselect">
                            <label class="control-label" for="multishopMGID"><strong>Please one or more shops from the list below: </strong></label>
                            <div class="controls">
                                {{ Form::select('shopMGID[]',$shops,null,array('class'=>'multi-shop','id'=>'multishopMGID','multiple')) }}
                            </div>
                        </div>

                        <div class="control-group" id="chain_name">
                            <label for="chain_name"><strong>Enter a shop name</strong></label>
                            <div class="controls">
                                {{ Form::select('chain_name[]',$chains,array(),array('id'=>'chain','class'=>'multi-chain','multiple')) }}
                            </div>
                        </div>
                        <div id="multi_mall">
                            <div class="control-group">
                                <label class="control-label" for="multimallID"><strong>Please one or more malls from the list below: </strong></label>
                                <div class="controls">
                                    {{ Form::select('mallIDs[]',$malls,null,array('class'=>'multi-mall','id'=>'multimallID','multiple')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <strong>Password needs to have :</strong>
                        <br />
                        <ul>
                            <li>At least one upper case english letter</li>
                            <li>At least one lower case english letter</li>
                            <li>At least one digit</li>
                            <li>At least one special character (!@#$_)</li>
                            <li>Minimum 6 characters in length</li>
                        </ul>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ $errors->has('password') ? 'error' : '' }}">
                            <label class="control-label" for="password"><strong>Password</strong></label>
                            <div class="controls">
                                {{ Form::password('password',array('class'=>'input-medium','id'=>'password')) }}
                            </div>
                            <div class="help-block" id="password_help"> {{ ($errors->has('password') ?  $errors->first('password') : '') }}</div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group {{ $errors->has('password_confirmation') ? 'error' : '' }}">
                            <label class="control-label" for="password_confirmation"><strong>Confirm Password</strong></label>
                            <div class="controls">
                                {{ Form::password('password_confirmation',array('class'=>'input-medium')) }}
                            </div>
                            <div class="help-block">{{ ($errors->has('password_confirmation') ? $errors->first('password_confirmation') : '') }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="accordion-group">
        <div class="accordion-heading">
            <h6>
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">Personal Details</a>
            </h6>
        </div>
        <div id="collapseTwo" class="accordion-body collapse">
            <div class="accordion-inner">
                <div class="row-fluid">
                    <div class="span4">
                        <div class="control-group {{ ($errors->has('first_name')) ? 'error' : '' }}">
                            <label for="first_name" class="control-label"><strong>First Name * </strong></label>
                            <div class="controls">
                                {{ Form::text('first_name',null) }}
                            </div>
                            <div class="help-block">{{ ($errors->has('first_name') ?  $errors->first('first_name') : '') }}</div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="last_name" class="control-label"><strong>Last Name</strong></label>
                            <div class="controls">
                                {{ Form::text('last_name',null) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="cell" class="control-label"><strong>Cell Phone</strong></label>
                            <div class="controls">
                                {{ Form::text('cell',null,array('class'=>'numeric')) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <div class="control-group">
                            <label for="tel_home" class="control-label"><strong>Home phone</strong></label>
                            <div class="controls">
                                {{ Form::text('tel_home',null,array('class'=>'numeric')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="tel_work" class="control-label"><strong>Work Phone</strong></label>
                            <div class="controls">
                                {{ Form::text('tel_work',null,array('class'=>'numeric')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="fax" class="control-label"><strong>Fax</strong></label>
                            <div class="controls">
                                {{ Form::text('fax',null) }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <div class="control-group">
                            <label for="postal_address" class="control-label"><strong>Postal Address</strong></label>
                            <div class="controls">
                                {{ Form::textarea('postal_address',null,array('rows'=>3)) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="postal_code" class="control-label"><strong>Postal Code</strong></label>
                            <div class="controls">
                                {{ Form::text('postal_code',null,array('class'=>'span2')) }}
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="gender"><strong>Gender</strong></label>
                            <div class="controls">
                                {{ Form::radio('gender','Female') }}Female &nbsp;&nbsp;{{ Form::radio('gender','Male') }}Male
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4">
                        <div class="controls-group">
                            <label for="birth_date" class="control-label"><strong>Date of Birth</strong></label>
                            <div class="controls">
                                <div class="input-append datetimepicker4">
                                    {{ Form::text('birth_date',null,array('data-format'=>'yyyy-MM-dd','readonly')) }}
                                <span class="add-on">
                                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                  </i>
                                </span>
                                </div>
                                <span class="help-block">{{ ($errors->has('birth_date') ? $errors->first('birth_date') : '') }}</span>
                            </div>

                        </div>
                    </div>
                    <div class="span4">
                        <div class="control-group">
                            <label for="province"><strong>Province </strong></label>
                            <div class="controls">

                                <select name="province_id" id="province" class="txtbar">
									<option value="">Please Select...</option>
									@foreach($provinces as $id=>$province)
                                                <option value="{{ $id }}">{{ $province }}</option>

									@endforeach
								</select>
                            </div>
                        </div>
                    </div>
                    <div class="span4">
                            <div class="control-group">
                                <label for="city"><strong>City</strong></label>
                                <div class="controls">
                                   <select name="city_id" id="city">
										<option value="">Please Select...</option>

											@foreach($cities as $key=>$value)
										<option value="{{ $key }}" >{{ $value }}</option>
										@endforeach
									</select>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="accordion-group">
        <div class="accordion-heading">
            <h6>
                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">Company Details</a>
            </h6>
        </div>
        <div id="collapseThree" class="accordion-body collapse">
            <div class="accordion-inner">
                <div class="row-fluid">
                    <div class="span4">
                        <div class="control-group">
                            <label for="company"><strong>Company List : </strong></label>
                            <div class="controls">
                                {{ Form::select('company',$companyList,null) }}
                            </div>
                        </div>
                    </div>
                    
                    <div class="span4">
                        <div class="control-group">
                            <label for="vat_number"><strong>Vat Number</strong></label>
                            <div class="controls">
                                {{ Form::text('vat_number',null,array('class'=>'txtbar')) }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="control-group">
        <div class="controls">
            <input class="btn-primary btn" type="submit" id="sub" value="Create" />
            <input class="btn " type="reset" value="Reset Form" />
        </div>
    </div>
</div>
{{ Form::close() }}
</fieldset>
@stop

@section('exScript')
<script>

    $(document).ready(function()
    {
		var usertype= $('#user_type').val();

		showHideFields(usertype);
		multiShop();
        $(".collapse").collapse();
        $(".numeric").mask('0b99999999');

        $(".multi-mall").select2({});
        $(".multi-shop_mall").select2({});
		$(".multi-chain").select2({});
		$(".multi-shop").select2({
			allowClear: true
		});

		$('#sub').attr('disabled','disabled');

        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });

        $("#user_type").change(function(){
            usertype = $(this).val();
            showHideFields(usertype);
        });

        $('#password').keyup(function() {
            var _parent = $(this).parent().parent();
			var level = 0;
			var mesg = '';
			var input = $(this).val();
		
			if(/(.*[A-Z])/.test(input) == false) 
			{
				level = 1;
				mesg += ' No uppercase letters.<br/>';
			}
			if(/(.*[\!|\@|\#|\$|\_])/.test(input) == false) {
				level = 2;
				mesg += ' No special character (!@#$_).<br/>';
			
			}
			if(/(.*[\d])/.test(input) == false)
			{
				level = 3;
				mesg += ' No numbers.<br/>';
			}
			if(/(.*[a-z])/.test(input) == false)
			{
				level = 4;
				mesg += ' No lowercase letters.<br/>';
			}
			if(input.length < 6)
			{
				level = 5;
				mesg += ' Not long enough.<br/>';
			}

			$('#password_help').html(mesg);
			
            if(level != 0){
                _parent.addClass('error');
				$('#sub').attr('disabled','disabled');
            } else {
				$('#sub').removeAttr('disabled');
			}
        });
        $('#province').change(function(){
        	$('#city').empty();
                $.ajax({
                    type: "GET",
                    url: '/admin/provinces/cities/'+$(this).val(),
                    data: '',
                    success: function (data) {
                        //console.log(data);
                        $('#city').html('<option value="">Please Select a City..</option>');
                        $.each(data,function(key,val){

                            $('#city').append('<option value="'+key+'">'+val+'</option>');
                        });

                    }
            });
        });
    });

    function showHideFields(usertype)
    {
		if( usertype == 'Mall Manager' )
		{
            $("#mall_select").show();
            $("#multi_mall").hide();
            $("#chain_name").hide();
            $("#shop_mall_select").hide();
            $("#shop_multiselect").hide();
            $("#chainList").hide();
        }

        if( usertype == 'Marketing Manager' ){
            $("#multi_mall").show();
            $("#mall_select").hide();
            $("#shop_mall_select").hide();
            $("#chain_name").hide();
            $("#chainList").hide();
            $("#shop_multiselect").hide();
        }

        if( usertype == 'Shop Manager' ){

            $("#chain_name").hide();
            $("#shop_multiselect").hide();
            $("#chainList").hide();
            $("#shop_mall_select").show();
            $("#mall_select").hide();
            $("#multi_mall").hide();
        }

        if( usertype == 'Chain Manager' ){

            $("#chain_name").show();
            $("#mall_select").hide();
            $("#multi_mall").hide();
            $("#shop_mall_select").hide();
            $("#shop_multiselect").hide();
	        $("#chainList").hide();
        }
        if( usertype == 'FGX Staff' || usertype=='Master' || usertype==""){

            $("#mall_select").hide();
	        $("#multi_mall").hide();
	        $("#chain_name").hide();
            $("#shop_mall_select").hide();
	        $("#shop_multiselect").hide();
	        $("#chainList").hide();
        }
    }

    function multiShop(mall)
    {
		$("#shop_mall").change(function(){
            var mall = $(this).val();
            $.ajax({
                type: 'POST',
                dataType:'json',
                data: {mallIDs : mall},
                url: '/admin/shops/getbymall',
                success: function (data) {

                    $("#shop_multiselect").show();
					$('#multishopMGID').empty();
					//clear shops
					$(".multi-shop").select2('data',null)
                    $.each(data, function(key, value){
                        $('#multishopMGID').append('<option value="'+key+'">'+value+'</option>');
                    });
                }
            });
        });
    }

</script>
@stop
