@extends('layouts.pages')

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Shops - </span>Get chain </h1>
                    <p class="animated fadeInDown delay2">Register as a chain store user</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
@stop

@section('content')
    <div class="row-fluid">
        <div class="blog-content span12">
            <h3 class="dotted-border"> Shops - Apply as a chain member </h3>
            <div class="span5">
                <h4 class="dotted-border">Have more than 2 shops ? </h4>
                We've made it easier to update all your Shops' info by issuing a single Username and Password to control
                your Shop Chain.<br/>

                You need to submit your Chain Shop details and create an unique Username and Password.<br/>
            </div>
            <div class="span6">
                <h4 class="dotted-border">After your submission</h4>
                Unfortunately Mallguide can't display any updated Shop information until your Head office has confirmed
                your identity via email <a href="mailto:chainshop@mallguide.co.za">chainshop@mallguide.co.za</a> or fax
                (011 883 2304).<br/>
                After this you will be able to upload Promotions, Gift ideas and update any Shop in the Chain. It's as
                easy as that...<br/>
            </div>

            <div class="span5">
                <h4 class="dotted-border">Want a slice of the action ?</h4>
                Just tell us which Chain you would like to update and complete the registration form.
                Once your Head Office has confirmed your details, your profile will be activated and you will receive a
                confirmation email.
            </div>
            {{ Form::open(array('route' => 'user.signup', 'method' => 'post','name' => 'userSignUp','id' => 'chainSignUp', 'class' => 'form-inline userSignUp')) }}
                  class="form-inline userSignUp">
                <div class="span6">
                    <h4 class="dotted-border">Shop Chain Details</h4>
                    <div class="control-group {{ ($errors->has('chain_name') ? 'error' : '') }}">
                        <span><strong>Please enter all the names for your shop chain</strong></span>
                        <div class="controls">
                            {{ Form::select('chain_name[]',$chains,array(),array('id'=>'chainName','class'=>'multi-chain','multiple')) }}
                        </div>
                        <span class="help-block">{{ ($errors->has('shop_name') ? $errors->first('shop_name') : '') }}</span>
                    </div>
                </div>
                <div class="row-fluid">
                    {{ Form::hidden('admin_level',"Chain Manager",array('id'=>'admin_level')) }}
                    @include('partials/loginInfo')
                </div>
            {{ Form::close()}}
            <div class="row-fluid">
                <div id="info" class="span12">
                </div>
            </div>
        </div>
    </div>
@stop
@section('exScript')
    <script type="text/javascript">
        $(document).ready(function () {

            $(".multi-chain").select2({});
            $('.datetimepicker4').datetimepicker({
                pickTime: false
            });
            $('#password').keyup(function () {
                var _parent = $(this).parent().parent();


                var level = 0;
                var mesg = '';
                var input = $(this).val();

                if (/(.*[A-Z])/.test(input) == false) {
                    level = 1;
                    mesg += ' No uppercase letters.<br/>';

                }

                if (/(.*[\!|\@|\#|\$|\_])/.test(input) == false) {
                    level = 2;
                    mesg += ' No special character (!@#$_).<br/>';

                }
                if (/(.*[\d])/.test(input) == false) {
                    level = 3;
                    mesg += ' No numbers.<br/>';

                }
                if (/(.*[a-z])/.test(input) == false) {
                    level = 4;
                    mesg += ' No lowercase letters.<br/>';

                }

                if (input.length < 6) {
                    level = 5;
                    mesg += ' Not long enough.<br/>';

                }


                $('#password_help').html(mesg);

                if (level != 0) {
                    _parent.addClass('error');

                    $('#sub').attr('disabled', 'disabled');
                } else {
                    $('#sub').removeAttr('disabled');
                }
            });
        });

    </script>
@stop