@extends('layouts.pages')

@section('title')
@parent
Register
@stop

@section('heading')
<div class="inner-heading">
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span6">
				<h1 class="animated fadeInDown delay1">Register yourself on <span>Mallguide</span></h1>
				<p class="animated fadeInDown delay2">Add some text here</p>
			</div><!--/span6-->
			<div id="breadcrumbs" class="span6">
				<ul class="breadcrumb">
					<li><a href="/">Home</a> <span class="divider">/</span></li>
					<li class="active"><a href="/register">Register</a></li>
				</ul>
			</div><!--/span6-->
		</div><!--/row-->
	</div><!--/container-->
</div><!--/three-->
@stop

@section('content')
<div class="span12">
	<p>&nbsp;</p>
</div>

	<div class="span12">
	
			<form class="form-horizontal" action="{{ URL::to('users/create') }}" method="post" role="form">
				{{ Form::token() }}
		
				<div class="control-group {{ ($errors->has('email')) ? 'error' : '' }}" for="email">
					<label class="control-label span3" for="email">E-mail</label>
					<div class="controls">
						<input name="email" id="email" value="{{ Request::old('email') }}" type="text" class="input-medium" placeholder="E-mail"/>
						{{ ($errors->has('email') ? $errors->first('email') : '') }}
					</div>
				</div>
		
				<div class="control-group {{ $errors->has('password') ? 'error' : '' }}" >
					<label class="control-label span3" for="password">Password </label>
					<div class="controls">
						<input name="password" value="" type="password" class="input-medium"/>
						{{ ($errors->has('password') ?  $errors->first('password') : '') }}
					</div>
				</div>
		
				<div class="control-group {{ $errors->has('password_confirmation') ? 'error' : '' }}" >
					<label class="control-label span3" for="password_confirmation">Confirm Password </label>
					<div class="controls">
						<input name="password_confirmation" value="" type="password" class="input-medium"  />
						{{ ($errors->has('password_confirmation') ? $errors->first('password_confirmation') : '') }}
					</div>
				</div>
		
				<div class="control-group">
					<div class="controls">
						<input class="btn-primary btn" type="submit" value="Register">
						<input class="btn " type="reset" value="Reset">
					</div>
				</div>
			</form>
	
	</div>

@stop
