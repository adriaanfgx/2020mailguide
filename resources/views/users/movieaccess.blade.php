@extends('layouts.pages')
@section('title')
    @parent
    Movies - Add your mall
@stop

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Movies - </span>Add your mall </h1>
                    <p class="animated fadeInDown delay2">Register as a movie house manager</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
@stop

@section('content')

    <div class="row-fluid">
        <div class="span12 blog-content">
            <div class="row-fluid">

                <h3 class="dotted-border"> Movies - Add the mall </h3>

                Mallguide lists all Ster-Kinekor and Nu Metro movies for free. If you are an independent movie house, then simply complete your details below to be added.
                <br />
            </div>
            <div class="row-fluid">
                <h4 class="dotted-border"> How does it work ?</h4>
            </div>
            <div class="row-fluid">
                <ul class="span5">
                    <li>1. Complete your details below to register </li>
                    <li>2. Once approved, you can login to Mallguide to access your Control Panel </li>

                </ul>
                <ul class="span6">
                    <li>3. To upload your schedule, select a movie from a predefined list & add viewing times.</li>
                    <li>4. Schedules will update automatically every Thursday at midnight.</li>

                </ul>
            </div>
            <h4 class="dotted-border">What will it cost ? </h4>
            It will cost you nothing to list all your current movies and show times.
            You will also receive 200 free emails sent weekly to your subscribers. <br />Once your subscriber list has grown to over 200 emails,
            you can simply purchase additional email credits should you wish to extend your schedule to new subscribers.

            <table class="table">
                <thead>
                <tr>
                    <th>Description</th>
                    <th>Updated</th>
                    <th>Fee</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Independent Movie house schedule listing </td>
                    <td>Weekly </td>
                    <td>FREE </td>
                </tr>
                <tr>
                    <td>First 200 movie schedule emails p/w </td>
                    <td>Weekly</td>
                    <td>FREE</td>
                </tr>
                <tr>
                    <td>201 - 2000 movie schedule emails p/w </td>
                    <td>Weekly</td>
                    <td>15c per mail</td>
                </tr>
                <tr>
                    <td>2000+ movie schedule emails per month 	1 Week 	18c per mail All prices excludes VAT </td>
                    <td>1 Week </td>
                    <td>18c per mail</td>
                </tr>
                </tbody>
            </table>
            {{ Form::open(array('route' => 'user.signup', 'method' => 'post','name' => 'userSignUp','id' => 'userSignUp', 'class' => 'form-inline userSignUp')) }}
            {{ Form::hidden('admin_level','Movie House') }}
            <div class="row-fluid">
                <div class="span12">
                    <h4 class="dotted-border">Mall Information</h4>
                    <br />
                    <div class="control-group {{ ($errors->has('mallID') ? 'error' : '') }}">
                        <label for="mallID"><strong>Please Select your mall</strong></label>
                        <div class="controls">
                            {{ Form::select('mallID',$mallList,NULL,array('class'=>'span4', 'id'=>'mallID')) }}
                        </div>
                        <span class="help-block">{{ ($errors->has('mallID') ? $errors->first('mallID') : '') }}</span>
                    </div>
                </div>
            </div>

            <div class="movieHouse">
                <div class="row-fluid">
                    <h4 class="dotted-border">Movie House information</h4>
                </div>
                <div class="row-fluid">
                    <div class="span5">
                        <div class="control-group {{ ($errors->has('whichGroup') ? 'error' : '') }}">
                            <label for="whichGroup" class="control-label"><strong>Cinema Name</strong></label>
                            <div class="controls">
                                {{ Form::text('whichGroup',null)}}
                            </div>
                            <span class="help-block">{{ ($errors->has('whichGroup') ? $errors->first('whichGroup') : '') }}</span>
                        </div>
                    </div>


                    <div class="span2">
                        <div class="control-group">
                            <label for="numCinemas"><strong>Number of cinemas</strong></label>
                            <div class="controls">
                                {{ Form::text('numCinemas',null)}}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row-fluid">
                @include('partials/loginInfo')
            </div>
            {{ Form::close() }}
        </div>
    </div>
    </div>

@stop
@section('exScript')
    <script type="text/javascript">
        $(document).ready(function(){
            $('.datetimepicker4').datetimepicker({
                pickTime: false
            });
            var mallID=$("#mallID").val();
            if(mallID!="")
            {
                $.get("/movies/hasMallHouse/"+mallID).
                done(function(data)
                {
                    if(data=='Yes')
                    {
                        $('.movieHouse').hide()
                    }
                    else
                    {
                        $('.movieHouse').show()
                    }
                });
            }
            else
            {
                $('#movieHouse').hide()
            }

            $("#mallID").change(function()
            {
                var mallID=$(this).val();
                if(mallID!="")
                {
                    $.get("/movies/hasMallHouse/"+mallID).
                    done(function(data)
                    {
                        if(data=='Yes')
                        {
                            $('.movieHouse').hide()
                        }
                        else
                        {
                            $('.movieHouse').show()
                        }
                    });
                }
                else
                {
                    $('#movieHouse').hide()
                }
            });

            $('#password').keyup(function() {
                var _parent = $(this).parent().parent();
                var level = 0;
                var mesg = '';
                var input = $(this).val();

                if(/(.*[A-Z])/.test(input) == false)
                {
                    level = 1;
                    mesg += ' No uppercase letters.<br/>';
                }

                if(/(.*[\!|\@|\#|\$|\_])/.test(input) == false) {
                    level = 2;
                    mesg += ' No special character (!@#$_).<br/>';
                }
                if(/(.*[\d])/.test(input) == false)
                {
                    level = 3;
                    mesg += ' No numbers.<br/>';
                }
                if(/(.*[a-z])/.test(input) == false)
                {
                    level = 4;
                    mesg += ' No lowercase letters.<br/>';
                }

                if(input.length < 6)
                {
                    level = 5;
                    mesg += ' Not long enough.<br/>';
                }

                $('#password_help').html(mesg);

                if(level != 0){
                    _parent.addClass('error');

                    $('#sub').attr('disabled','disabled');
                } else {
                    $('#sub').removeAttr('disabled');
                }
            });
        });
    </script>
@stop