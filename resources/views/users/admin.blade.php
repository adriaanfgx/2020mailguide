@extends('layouts.admin')

@section('title')
@parent
List Users
@stop

@section('content')
<div class="row-fluid">
    <h3>Users</h3>
    <span>To refine the user listing below, use any of the filters:</span>
    <div class="row-fluid">
        <div class="12">
            <div class="well">
                <div class="span6">
                    <div class="control-group pull-right">
                        <label for="admin_level">Type Of User</label>
                        <div class="controls">
                            {{ Form::select('admin_level',$adminLevel,null,array('id'=>'admin_level')) }}
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <div class="control-group pull-right">
                        <label>List accounts that are :</label>
                        <div class="controls">
                            {{ Form::select('activated',$userStatus,null,array('id'=>'activated')) }}
                        </div>
                    </div>
                </div>
                <div class="span3">
                    <div class="control-group pull-right">
                        <label>Email search</label>
                        <div class="controls">
                            {{ Form::text('email',null,array('id'=>'email_search'))}}
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <span>To add a new user click the "Add a mall" button to the right:</span>
        <a href="{{ route('user.create') }}" class="btn btn-info btn-small pull-right"><i class="fa fa-plus"></i> Add a user</a>
    </div>
    <br />
    <div class="clearfix"></div>
    <div class="row-fluid">
        <table class="table table-striped table-bordered adminTableText">
            <thead>
            <tr>
                <th>User</th>
                <th>Type of user</th>
                <th>Activated</th>
                <th>FGX Approved</th>
                <th>When added</th>
                <th width="120">Options</th>
            </tr>
            </thead>
            <tbody id="user_table">
            @include('partials/admin/users/filterUserByRole')

            </tbody>
        </table>
        <div id="paginator">
            <div class="span12 text-center">{{ $users->links() }}</div>
        </div>
    </div>
</div>

@stop

@section('exScript')
<script type="text/javascript">
    $(document).ready(function(){

//        $('#user_table').on('load', function(){
//            var _elems = $('#user_table').children('tr').children('td').children('.send_activation');
//            _elems.click(function(){
//                var $user = $(this).attr('id');
//
//                $.ajax({
//                    type: "POST",
//                    url: '/resendactivation/'+$user,
//                    data: '',
//                    success: function(){
//                        location.reload();
//                    }
//                });
//            });
//        });

        $("#admin_level").change(function(){

            var userType = $(this).val();

            if( userType == '' ){

                location.reload();

            }else{

                $('#user_table').empty();
                $('#paginator').hide();

                $('#user_table').load( '/admin/users/'+encodeURI(userType));

            }

        });

        $("#activated").change(function(){

            var status = $(this).val();

            if( status == '' ){

                location.reload();

            }else{

                $('#user_table').empty();
                $('#paginator').hide();

                $('#user_table').load( '/admin/users/status/'+status);

            }

        });
		$(".send_activation").click(function(){

            var $user = $(this).attr('id');

            $.ajax({
                type: "POST",
                url: '/resendactivation/'+$user,
                data: {
                    '_token': "{{ csrf_token() }}"
                },
                success: function(){
                    location.reload();
                }
            });

        });
        $(".approve_user").click(function(){

            var $user = $(this).attr('id');

            $.ajax({
                type: "GET",
                url: '/user/approve/'+$user,
                data: '',
                success: function()
                {
                    location.reload();
                }
            });

        });
        $("#email_search").keypress(function(e){

            if (e.keyCode == 13){

                var email = $(this).val();

                if( email == '' ){
                    alert("Please enter a valid email address ....");
                }else{

                    $('#user_table').empty();
                    $('#paginator').hide();
                    $('#user_table').load( '/admin/users/email/'+email);


                }

            }
        });
    });
</script>
@stop
