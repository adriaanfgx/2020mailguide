@extends('layouts.pages')
@parent
Malls get access
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Malls - </span>Get access </h1>
                <p class="animated fadeInDown delay2">Register as a member of an existing mall on mallguide</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                {{ generateBreadcrumbs() }}
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->
@stop

@section('content')


<div class="row-fluid">
<div class="blog-content span12">
<div class="span4">


    <h4 class="dotted-border">Listing For Free On MallGuide</h4>
    Mallguide offers the local and international visitors a free resource portal,
    to see everything current in retail shopping. With this free tool,
    you will be able to manage your tenants, promotions, events,
    etc and also drive higher volumes of traffic and shoppers to your site and premise...

    <br /><br />
    <h4 class="dotted-border">What free tools does Mallguide offer you as an Existing Mall ? </h4>

    <ul>
        <li>Full listing of malls (currently listing 990+ malls)</li>
        <li>Full listing of all shops </li>
        <li>Free listings of current events, promotions and exhibitions, we help you get noticed!  </li>
        <li>Movies, full listings for all cinemas including Ster-Kinekor, Nu Metro and independents </li>
        <li>Movie newsletters, visitors can subscribe to receive free movie times and listings </li>
        <li>Free job listings, we connect quality staff with your tenants </li>
        <li>Shop vacancies, we help you fill your vacant stores with new tenants </li>
        <li>Tenant communication, add forums & send sms's and newsletters </li>
        <li>Approve your own tenant content updates </li>
        <li>Grow your shopper database </li>
    </ul>
</div>
<div class="span8">
    <h4 class="dotted-border">Want a slice of the action ?</h4>
    Simply tell us who you are and which Mall you control, once we've confirmed your status, a Username and Password will be issued.
    <h4 class="dotted-border">Mall Information</h4>

{{ Form::open(array('route' => 'user.signup', 'method' => 'post','name' => 'mallSignUp','id' => 'mallSignUp', 'class' => 'form-inline mallSignUp')) }}
    {{ Form::hidden('admin_level','Mall Manager') }}
    <span><strong>Please select your mall</strong></span>
    <br />
    {{ Form::select('mallID',$mallList,$mall,array('id'=>'mall')) }}
    <br />
    If your mall is not listed, please <a href="{{ route('malls.register') }}">Register your Mall</a>
    <br />
    @if( isset($centreManager) && isset($marketingManager) )
    <span><strong>Please note that there is already an Active "Mall marketing consultant" Member and an Active "Mall centre manager" Member registered for this Mall.</strong></span>
    <div class="clearfix"></div>
    <div class="row-fluid">
        <div class="span4">
            <div class="control-group">
                <label for="mallSold"><strong>Has your mall been sold ?</strong></label>
                <div class="controls">
                    {{ Form::radio('mallSold','Y','checked') }}&nbsp;Yes&nbsp;
                    {{ Form::radio('mallSold','N') }}&nbsp;No&nbsp;
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="newCentreManagement"><strong>Has a new Centre Management been appointed ?</strong></label>
                <div class="controls">
                    {{ Form::radio('newCentreManagement','Y','checked') }}&nbsp;Yes&nbsp;
                    {{ Form::radio('newCentreManagement','N') }}&nbsp;No&nbsp;
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span4">
            <div class="control-group">
                <label for="newMarketingTeam"><strong>Has a new Marketing Team been appointed ?</strong></label>
                <div class="controls">
                    {{ Form::radio('newMarketingTeam','Y','checked') }}&nbsp;Yes&nbsp;
                    {{ Form::radio('newMarketingTeam','N') }}&nbsp;No&nbsp;
                </div>
            </div>
        </div>
        <div class="span4">
            <div class="control-group">
                <label for="newMarketingOrCentre"><strong>Are you the new Marketing or Centre Manager ?</strong></label>
                <div class="controls">
                    {{ Form::radio('newMarketingOrCentre','Y','checked') }}&nbsp;Yes&nbsp;
                    {{ Form::radio('newMarketingOrCentre','N') }}&nbsp;No&nbsp;
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="clearfix"></div>
    <div class="row-fluid">
        <h4 class="dotted-border">Personal Details</h4>
        <div class="span10">
            <span><strong>Required fields are marked with *</strong></span><br />
            <h4 class="dotted-border">Login Info</h4>
            <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}">
                <label for="email" class="control-label"><strong>Email*</strong></label>
                <div class="controls">
                    {{ Form::text('email',null,array('class'=>'txtbar span5')) }}
                    <span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
                </div>
            </div>
        </div>
        <div class="span4"></div>
    </div>
    <div class="row-fluid">
        <div class="span10">
            <span>Password needs to have :</span>
            <ul>
                <li>At least one upper case english letter, lower case english letter, one digit, special character</li>
                <li>Minimum 6 characters in length</li>
            </ul>
        </div>
    </div>
    <div class="row-fluid">
        <div class="clearfix"></div>
        <div class="span5">
            <div class="control-group {{ ($errors->has('password') ? 'error' : '') }}">
                <label for="password" class="control-label"><strong>Password*</strong></label>
                <div class="controls">
                    {{ Form::password('password',null,array('class'=>'txtbar')) }}
                </div>
                <span class="help-block">{{ ($errors->has('password') ? $errors->first('password') : '') }}</span>
            </div>
        </div>
        <div class="span5">
            <div class="control-group {{ ($errors->has('password') ? 'error' : '') }}">
                <label for="password_confirmation" class="control-label"><strong>Confirm Password*</strong></label>
                <div class="controls">
                    {{ Form::password('password_confirmation',null,array('class'=>'txtbar')) }}
                </div>
                <span class="help-block">{{ ($errors->has('password_confirmation') ? $errors->first('password_confirmation') : '') }}</span>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="clearfix"></div>
        <div class="span5">
            <div class="control-group {{ ($errors->has('first_name') ? 'error' : '') }}">
                <label for="first_name" class="control-label"><strong>First Name *</strong></label>
                <div class="controls">
                    {{ Form::text('first_name',null,array('class'=>'txtbar')) }}
                    <span class="help-block">{{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}</span>
                </div>
            </div>
        </div>
        <div class="span5">
            <div class="control-group">
                <label for="last_name"><strong>Last Name</strong></label>
                <div class="controls">
                    {{ Form::text('last_name',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="clearfix"></div>
        <div class="span5">
            <div class="control-group">
                <label for="company"><strong>Company</strong></label>
                <div class="controls">
                    {{ Form::text('company',null) }}
                </div>
            </div>
        </div>
        <div class="span5">
            <div class="control-group">
                <label for="vat_number"><strong>Vat Number</strong></label>
                <div class="controls">
                    {{ Form::text('vat_number',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="clearfix"></div>
        <div class="span5">
            <div class="control-group">
                <label for="postal_address"><strong>Postal Address</strong></label>
                <div class="controls">
                    {{ Form::textarea('postal_address',null,array('class'=>'txtbox','rows'=>'3')) }}
                </div>
            </div>
        </div>
        <div class="span5">
            <div class="control-group">
                <label for="postal_code"><strong>Postal Code</strong></label>
                <div class="controls">
                    {{ Form::text('postal_code',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="clearfix"></div>
        <div class="span5">
            <div class="control-group">
                <label for="tel_work"><strong>Work Tel</strong></label>
                <div class="controls">
                    {{ Form::text('tel_work',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
        <div class="span5">
            <div class="control-group">
                <label for="fax"><strong>Fax</strong></label>
                <div class="controls">
                    {{ Form::text('fax',null,array('class'=>'txtbar')) }}
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="clearfix"></div>
        <div class="span5">
            <div class="control-group">
                <label for="cell"><strong>Cell</strong></label>
                <div class="controls">
                    {{ Form::text('cell',null,array('class'=>'txtbar numeric')) }}
                </div>
            </div>
        </div>
        <div class="span5">
            <div class="control-group {{ ($errors->has('birth_date') ? 'error' : '') }}">
                <label for="birth_date" class="control-label"><strong>Date of Birth</strong></label>
                <div class="controls">
                    <div class="input-append datetimepicker4">
                        {{ Form::text('birth_date',null,array('data-format'=>'yyyy-MM-dd','readonly')) }}
                        <span class="add-on">
                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                          </i>
                        </span>
                    </div>
                    <div class="help-block">{{ ($errors->has('birth_date') ? $errors->first('birth_date') : '') }}</div>
                </div>
                <span class="help-block">{{ ($errors->has('birth_date') ? $errors->first('birth_date') : '') }}</span>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="clearfix"></div>
        <div class="span5">
            <div class="control-group">
                <label for="cell"><strong>Gender</strong></label>
                <div class="controls">
                    {{ Form::radio('gender','Female') }}Female &nbsp;&nbsp;{{ Form::radio('gender','Male') }}Male
                </div>
            </div>
        </div>
        <div class="span5">
            <div class="control-group">
                <button class="submit" id="update_user">Submit Details</button>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div><!-- span 8-->
</div><!-- span 8-->

</div>

@stop
@section('exScript')
<script type="text/javascript">
    $(document).ready(function(){

        $(".numeric").mask('27b99999999');
        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });

        $("#mall").change(function(){

            var mallID = $(this).val();

            window.location.href = '/mall/getaccess/'+mallID;

        });
    });
</script>
@stop
