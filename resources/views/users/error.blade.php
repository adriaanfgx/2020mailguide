@extends('layouts.backpages')

@section('seo_meta')
    <meta name="description"
          content="">
    <meta name="keywords" content="">
@stop
@section('title')
    @parent
    Member Zone
@stop

@section('heading')
    <div class="inner-heading">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Error </h1>
                    <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    <ul class="breadcrumb">
                        <li><a href="/">Home</a> <span class="divider">/</span></li>
                        <li class="active">Error</li>

                    </ul>
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->


@stop

@section('content')
    <div class="row">
        <div class="span12">



            @include('notifications')

        </div>

    </div><!-- span 6-->



@stop
