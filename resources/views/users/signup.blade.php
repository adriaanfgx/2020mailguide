@extends('layouts.pages')
@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Members - </span>Join </h1>
                    <p class="animated fadeInDown delay2">Register as a member on Mallguide</p>
                </div>
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row-fluid">
        <div class="blog-content">
            <h3 class="dotted-border"> Members - Join Now </h3>
            <div class="span6">
                Mallguide offers a wide range of functional tools to different user types, each set customised to meet
                your
                requirements as a Mall, a Shop, or a Chain Shop.

                <br/><br/>
                Please see below for a complete functionality breakdown for each user type:<br/><br/>
                <ul>
                    <li>Malls - register your Mall if you are Centre Manager or the Marketing Manager</li>
                    <li>Shops - register your Shop if you are a tenant</li>
                    <li>Chain Shops - register your Chain if you are a tenant in more than 1 Mall</li>
                </ul>
            </div>
            <div class="span5">
                <span><strong>What would you like to register as ?</strong></span><br/><br/>
                <select id="user_type" name="user_type">
                    <option value="" selected="selected">Select Type Of User Registration</option>
                    @foreach($userTypes as $key => $value)
                        <option value="{{ $key }}">
                            {{ $value }}
                        </option>
                    @endforeach
                </select>
            </div>
            <div class="clear"></div>
        </div>
    </div>
@endsection
@section('exScript')
    <script type="text/javascript">
        $(document).ready(function () {
            $("#user_type").change(function () {
                var selectedOption = $(this).val();

                if (selectedOption == 'add_center_manager') {
                    window.location.href = 'mall/register/center';
                } else if (selectedOption == 'add_marketing_manager') {
                    window.location.href = 'mall/register/marketing';
                } else if (selectedOption == 'new_mall') {
                    window.location.href = 'malls/register';
                } else if (selectedOption == 'new_shop') {
                    window.location.href = 'shops/createfrontend';
                } else if (selectedOption == 'existing_shop') {
                    window.location.href = 'shop/getaccess';
                } else if (selectedOption == 'chain_store') {
                    window.location.href = 'user/getchain';
                } else if (selectedOption == 'movie_house') {
                    window.location.href = 'movies/getaccess';
                }
            });
        });
    </script>
@endsection
