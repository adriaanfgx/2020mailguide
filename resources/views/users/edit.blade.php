@extends('layouts.admin')
@section('title')
@parent
Update User
@stop

@section('content')
<div class="row-fluid">
    <h4>Update User Details</h4>
    {{ Form::model($user, array('route' => array('user.postEdit', $user->id),'method' => 'post','name' => 'userEdit', 'id' => 'userEdit', 'class' => 'form-inline')) }}
    <div class="accordion" id="accordion2">
        <div class="accordion-group">
            <div class="accordion-heading">
                <h6>
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">Profile Info</a>
                </h6>
            </div>
            <div id="collapseOne" class="accordion-body collapse">
                <div class="accordion-inner">
                    <div class="row-fluid">
                        <div class="span4">
                            <div class="control-group {{ $errors->first('email', ' error') }}">
                                <label class="control-label" for="email"><strong>Email Address</strong></label>
                                <div class="controls">
                                    {{ Form::text('email',null) }}
                                </div>
                                <span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label class="control-label" for="admin_level"><strong>Admin Level</strong></label>
                                <div class="controls">
                                    {{ Form::select('admin_level',$adminLevel,null,array('class'=>'txtbar')) }}
                                </div>
                            </div>
                        </div>
                        <div class="span4">
							<label class="checkbox">
								<input type="checkbox" name="activated" value="1" @if($user->activated) checked="checked" @endif> Activate User
							</label>
						</div>
                    </div>


                    <div class="row-fluid">
                        <div class="span4">
                            @if( $user->admin_level == 'Mall Manager')
                            <div class="control-group">
                                <label class="control-label" for="mallID"><strong>Please select from the list below to change the user's mall: </strong></label>
                                <div class="controls">
                                    {{ Form::select('mallID',$malls,isset($selectMall) ? $selectMall : "",array('id'=>'mallID')) }}
                                </div>
                            </div>
                            @elseif( $user->admin_level == 'Marketing Manager' )
                            <div class="control-group">
                                <label class="control-label" for="user_malls"><strong>Please select one or more malls from the list below: </strong></label>
                                <div class="controls">
                                    {{ Form::select('mallID[]',$malls,isset($selectMall) ? $selectMall : array(),array('class'=>'multi-mall','id'=>'mallID','multiple')) }}
                                </div>
                            </div>
                            @elseif( $user->admin_level == 'Shop Manager' )
	                        <div class="control-group" id="shop_mall_select">
	                            <label for="shop_mallID"><strong>Select a mall for the user</strong></label>
	                            <div class="controls">
	                                {{ Form::select('shop_mallID[]',$malls,$selectedMalls,array('class'=>'multi-shop_mall','id'=>'shop_mall','multiple')) }}
	                            </div>
	                        </div>
                            <div class="control-group">
                                <label class="control-label" for="shopMGID"><strong>Update user shops: </strong></label>
                                <div class="controls">
                                    {{ Form::select('shopMGID[]', $shops ,$selectedShops,array('class'=>'multi-shop','id'=>'shopMGID','multiple')) }}
                                </div>
                            </div>
                            @endif
                            @if( $user->admin_level == 'Chain Manager' )
                              <div class="control-group" id="chain_name">
	                            <label for="chain_name"><strong>Enter a shop name</strong></label>
	                            <div class="controls">
	                                {{ Form::select('chain_name[]',$chains,$userchain,array('id'=>'chain','class'=>'multi-chain','multiple'=>true)) }}
	                            </div>
	                        </div>
							@endif
                        </div>

                        <div class="span4">
                            @if( $user->admin_level == 'Mall Manager' || $user->admin_level == 'Marketing Manager' )
                            <div class="control-group">
                                <label><strong>Malls Selected</strong></label>
                                <div class="controls">
                                    <ul>
                                        @if(isset($selectedMalls) && count($selectedMalls))
                                        @foreach( $selectedMalls as $selected )
                                        <li>{{ $selected }}</li>
                                        @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            @elseif( $user->admin_level == 'Chain Manager' )
	                            @if(isset($userchain))
	                            <br />
	                            <div class="control-group">
	                                <label><strong>Chain Store</strong></label>
	                                <div class="controls">
	                                @foreach($userchain as $chain)
					                    {{ $chain }}<br />
					                @endforeach
	                                </div>
	                            </div>
	                            @endif
                            @elseif( $user->admin_level == 'Shop Manager' )
                            <br />
                            <div class="control-group">
                                <label><strong>User Shop(s)</strong></label>
                                <div class="controls">
                                    <ul>
                                        @if(isset($userShops) && count($userShops))
                                        @foreach( $userShops as $shopID =>$shopName )
                                        <li>{{ $shopName }}</li>
                                        @endforeach
                                        @endif
                                    </ul>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>










                    
                </div>
            </div>
        </div>
        <div class="accordion-group">
            <div class="accordion-heading">
                <h6>
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">Personal Details</a>
                </h6>
            </div>
            <div id="collapseTwo" class="accordion-body collapse">
                <div class="accordion-inner">
                    <div class="row-fluid">
                        <div class="span4">
                            <div class="control-group {{ $errors->first('first_name', ' error') }}">
                                <label for="first_name"><strong>First Name</strong></label>
                                <div class="controls">
                                    {{ Form::text('first_name',null) }}
                                </div>
								<span class="help-block">{{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}</span>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group {{ $errors->first('last_name', ' error') }}">
                                <label for="last_name"><strong>Last Name</strong></label>
                                <div class="controls">
                                    {{ Form::text('last_name',null) }}
                                </div>
								<span class="help-block">{{ ($errors->has('last_name') ? $errors->first('last_name') : '') }}</span>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="cell" class="control-label"><strong>Cell Phone</strong></label>
                                <div class="controls">
                                    {{ Form::text('cell',null,array('class'=>'numeric')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">
                            <div class="control-group">
                                <label for="tel_home" class="control-label"><strong>Home phone</strong></label>
                                <div class="controls">
                                    {{ Form::text('tel_home',null) }}
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="tel_work" class="control-label"><strong>Work Phone</strong></label>
                                <div class="controls">
                                    {{ Form::text('tel_work',null) }}
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="fax" class="control-label"><strong>Fax</strong></label>
                                <div class="controls">
                                    {{ Form::text('fax',null) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">
                            <div class="control-group">
                                <label for="postal_address" class="control-label"><strong>Postal Address</strong></label>
                                <div class="controls">
                                    {{ Form::textarea('postal_address',null,array('rows'=>3)) }}
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="postal_code" class="control-label"><strong>Postal Code</strong></label>
                                <div class="controls">
                                    {{ Form::text('postal_code',null,array('class'=>'span2')) }}
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="controls-group">
                                <label for="birth_date" class="control-label"><strong>Date of Birth</strong></label>
                                <div class="controls">
                                    <div class="input-append datetimepicker4">
                                        {{ Form::text('birth_date',null,array('data-format'=>'yyyy-MM-dd','readonly')) }}
                                        <span class="add-on">
                                          <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                          </i>
                                        </span>
                                    </div>
                                    <span class="help-block">{{ ($errors->has('birth_date') ? $errors->first('birth_date') : '') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span4">
                            <div class="control-group">
                                <label for="province"><strong>Province </strong></label>
                                <div class="controls">

                                    <select name="province_id" id="province" class="txtbar">
										<option value="">Please Select...</option>
										@foreach($provinces as $id=>$province)
                                        			<option value="{{ $id }}"@if($user->province_id == $id) selected="selected" @endif>{{ $province }}</option>
										
										@endforeach
									</select>
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="city"><strong>City</strong></label>
                                <div class="controls">
                                   <select name="city_id" id="city">
										<option value="">Please Select...</option>

											@foreach($cities as $key=>$value)
										<option value="{{ $key }}" {{$user->city_id == $key ? 'selected="selected"':""}} >{{ $value }}</option>
										@endforeach
									</select>
                                </div>
                            </div>
                        </div>
                        <div class="span4"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="accordion-group">
            <div class="accordion-heading">
                <h6>
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">Company Details</a>
                </h6>
            </div>
            <div id="collapseThree" class="accordion-body collapse">
                <div class="accordion-inner">
                    <div class="row-fluid">
                        <div class="span4">
                            <div class="control-group">
                                <label for="company"><strong>Company List : </strong></label>
                                <div class="controls">
                                    {{ Form::select('company',$companyList,null) }}
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="company"><strong>Company</strong></label>
                                <div class="controls">
                                    {{ Form::text('company',null,array('class'=>'txtbar')) }}
                                </div>
                            </div>
                        </div>
                        <div class="span4">
                            <div class="control-group">
                                <label for="vat_number"><strong>Vat Number</strong></label>
                                <div class="controls">
                                    {{ Form::text('vat_number',null,array('class'=>'txtbar')) }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <div class="control-group">
                <div class="controls">
                    <button class="btn btn-success" type="submit">Submit</button>
                </div>
            </div>
        </div>
    </div>
    {{ Form::close() }}
</div>

@stop

@section('exScript')
<script>

    $(document).ready(function(){

        $(".collapse").collapse();
        $(".numeric").mask('0b99999999');

        $('.datetimepicker4').datetimepicker({
            pickTime: false
        });
		$(".multi-chain").select2({});
        $(".multi-mall").select2({});
        $(".multi-shop_mall").select2({});
        $(".multi-shop").select2({
			allowClear: true
		});

        $('#province').change(function(){
        	
        	$('#city').empty();
                    $.ajax({
                        type: "GET",
                        url: '/admin/provinces/cities/'+$(this).val(),
                        data: '',
                        success: function (data) {
            				//console.log(data);
                            $('#city').html('<option value="">Please Select a City..</option>');
                            $.each(data,function(key,val){
            
                                $('#city').append('<option value="'+key+'">'+val+'</option>');
                            });
            
                        }
                    });
        });

        $("#shop_mall").change(function()
        {
            var mall = $(this).val();
            var clear = true;
			multiShop(mall, clear);
        });
    });
    function multiShop(mall,clear)
    {
        $.ajax({
            type: 'POST',
            dataType:'json',
            data: {mallIDs : mall},
            url: '/admin/shops/getbymall',
            success: function (data) {

                $("#shop_multiselect").show();
				//clear shops
				if(clear)
				{
					$(".multi-shop").select2('data',null)
				}
                $.each(data, function(key, value){
                    $('#multishopMGID').append('<option value="'+key+'">'+value+'</option>');
                });
            }
        });
    }


</script>
@stop
