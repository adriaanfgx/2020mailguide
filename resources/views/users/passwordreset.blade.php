@extends('layouts.default')
@section('seo_meta')
<meta name="description"
      content="">
<meta name="keywords" content="">
@stop
@section('title')
@parent
Member Zone
@stop

@section('heading')
<div class="inner-heading">
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span6">
                <h1 class="animated fadeInDown delay1"><span>Memberzone: </span>Mall Member</h1>
                <p class="animated fadeInDown delay2">Manager your Mall &amp; Tenant Information &amp; Communication</p>
            </div><!--/span6-->
            <div id="breadcrumbs" class="span6">
                <ul class="breadcrumb">
                    <li><a href="/">Home</a><span class="divider">/</span></li>
                    <li class="active">Reset Password</li>
                </ul>
            </div><!--/span6-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/three-->

@stop

@section('content')
<div class="row-fluid hidden-phone" id="filterSection_menu">
    <div class="span12"><!--sort wrap -->
    </div><!--span12 -->
</div><!--row -->

<div id="filterSection" data-perrow="4" class="row-fluid">
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>
    </div>  <!-- span6 -->

    <div class="clear"></div>
    <div class="row-fluid">
        <div class="span12">
            <form name="passwordReset" id="passwordReset" method="post" action="/passwordreset/{{ $code }}/{{ $user->id }}">
            {{ Form::token() }}
            <h4 class="dotted-border">Reset Password</h4>
            <div class="span6">
                <span>Please make sure <strong>all</strong> the fields are filled in:</span>
                <div id="user_info">
                    <div class="control-group {{ ($errors->has('new_pass') ? 'error' : '') }}">
                        <label for="new_pass">Please Note : Your password must have at least,</label>
                        <ul>
                            <li>1 upper case letter,</li>
                            <li>1 lower case letter,</li>
                            <li>1 special character,</li>
                            <li>1 digit,</li>
                            <li>and must be at least <strong>6</strong> characters long.</li>
                        </ul>
                        <div class="controls">
                            <label for="new_pass" class="control-label">Enter Password</label>
                            {{ Form::password('new_pass',null,array('class'=>'txtbar')) }}
                            <span class="help-block">{{ ($errors->has('new_pass') ? $errors->first('new_pass') : '') }}</span>
                        </div>
                    </div>
                    <div class="control-group {{ ($errors->has('confirm_pass') ? 'error' : '') }}">
                        <label for="new_pass_confirmation">Confirm New Password</label>
                        <div class="controls">
                            {{ Form::password('new_pass_confirmation',null,array('class'=>'txtbar')) }}
                            {{ ($errors->has('new_pass_confirmation') ? $errors->first('confirm_pass') : '') }}
                        </div>
                    </div>
                </div>
                <button class="submit reg-btn" id="update_user">Submit Details</button>
            </div>
            {{ Form::close() }}
        </div><!--span12 -->
    </div>
</div>
@stop
@section('exScript')

<script type="text/javascript">
    $(document).ready(function(){

    });
</script>
@stop

