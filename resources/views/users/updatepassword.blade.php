@extends('layouts.backpages')
@section('title')
    @parent
    Member Zone
@stop


@section('content')
    <div class="blog-content container-fluid">
        <div class="row-fluid">
            <div class="biggerLeftMargin">
                <h4>Update your password</h4>

                {{ Form::model($user, array('route' => array('user.postUpdatepassword', $user->id),'method'=>'post','name' => 'passwordUpdate', 'id' => 'passwordUpdate', 'class' => 'form-inline')) }}
                <div class="span4">
                    <span>Please make sure <strong>all</strong> the fields are filled in:</span>
                    <div id="user_info">
                        <div class="control-group {{ ($errors->has('old_pass') ? 'error' : '') }}">
                            <label for="old_pass" class="control-label"><strong>Old Password</strong></label>
                            <div class="controls">
                                {{ Form::password('old_pass',null,array('class'=>'txtbar')) }}
                            </div>
                            <span
                                class="help-block">{{ ($errors->has('old_pass') ? $errors->first('old_pass') : '') }}</span>
                        </div>
                        <span>Password needs to have :</span>
                        <ul>
                            <li>At least one upper case english letter</li>
                            <li>At least one lower case english letter</li>
                            <li>At least one digit</li>
                            <li>At least one special character (! @ # $ _)</li>
                            <li>Minimum 6 characters in length</li>
                        </ul>
                        <div class="control-group {{ ($errors->has('new_pass') ? 'error' : '') }}">
                            <label for="new_pass" class="control-label"><strong>New Password</strong><small>&nbsp;(Minimum
                                    6 characters)</small></label>
                            <div class="controls">
                                {{ Form::password('new_pass',array('class'=>'txtbar','id'=>'password')) }}
                            </div>
                            <div class="help-block"
                                 id="password_help"> {{ ($errors->has('new_pass') ?  $errors->first('new_pass') : '') }}</div>
                        </div>
                        <div class="control-group {{ ($errors->has('confirm_pass') ? 'error' : '') }}">
                            <label for="new_pass_confirmation" class="control-label"><strong>Confirm New
                                    Password</strong></label>
                            <div class="controls">
                                {{ Form::password('new_pass_confirmation',array('class'=>'txtbar')) }}

                            </div>
                            <span
                                class="help-block">{{ ($errors->has('new_pass_confirmation') ? $errors->first('confirm_pass') : '') }}</span>
                        </div>
                    </div>
                    <button class="submit reg-btn" id="update_user">Submit Details</button>
                    <div class="clearfix"></div>
                    <br/>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
@stop
@section('exScript')

    <script type="text/javascript">
        $(document).ready(function () {
            $('#password').keyup(function () {
                var _parent = $(this).parent().parent();
                var level = 0;
                var mesg = '';
                var input = $(this).val();

                if (/(.*[A-Z])/.test(input) == false) {
                    level = 1;
                    mesg += ' No uppercase letters.<br/>';
                }
                if (/(.*[\!|\@|\#|\$|\_])/.test(input) == false) {
                    level = 2;
                    mesg += ' No special character (!@#$_).<br/>';

                }
                if (/(.*[\d])/.test(input) == false) {
                    level = 3;
                    mesg += ' No numbers.<br/>';
                }
                if (/(.*[a-z])/.test(input) == false) {
                    level = 4;
                    mesg += ' No lowercase letters.<br/>';
                }
                if (input.length < 6) {
                    level = 5;
                    mesg += ' Not long enough.<br/>';
                }

                $('#password_help').html(mesg);

                if (level != 0) {
                    _parent.addClass('error');
                    $('#sub').attr('disabled', 'disabled');
                } else {
                    _parent.removeClass('error');
                    $('#sub').removeAttr('disabled');
                }
            });
        });
    </script>
@stop

