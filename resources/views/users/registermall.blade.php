@extends('layouts.pages')

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span6">
                    <h1 class="animated fadeInDown delay1"><span>Malls - </span>Get access </h1>
                    <p class="animated fadeInDown delay2">Register as a member of an existing mall on Mallguide</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->
@endsection

@section('content')
    <div class="row-fluid">
        <div class="blog-content">
            <div class="row-fluid">
                <h3 class="dotted-border"> Malls - New Malls Get Listed </h3>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <h4 class="dotted-border">Listing For Free On MallGuide</h4>
                    Mallguide offers local and international visitors a free resource portal,
                    to briefly overview everything current in retail shopping.
                </div><!-- span 6-->
                <div class="span6">
                    <h4 class="dotted-border">Want a slice of the action ?</h4>
                    Simply tell us who you are and which Mall you control, once we've confirmed your status, a Username
                    and Password will be issued.
                    <br/><br/>
                </div>
            </div>
            <div class="row-fluid">
                <h4 class="dotted-border">What free tools does Mallguide offer you as an Existing Mall ? </h4>

                <ul class="span6">
                    <li>Full listing of malls (currently listing 990+ malls)</li>
                    <li>Full listing of all shops</li>
                    <li>Free listings of current events, promotions and exhibitions, we help you get noticed!</li>
                    <li>Free job listings, we connect staff with your tenants</li>
                </ul>
                <ul class="span5">
                    <li>Shop vacancies, we help you fill your vacant stores with new tenants</li>
                    <li>Tenant communication, add forums & send sms's and newsletters</li>
                    <li>Approve your own tenant content updates</li>
                    <li>Grow your shopper database</li>
                </ul>

            </div><!-- span 6-->
            <div class="row-fluid">
                <h4 class="dotted-border">Mall Information</h4>
            </div>
            {{ Form::open(array('route' => 'user.signup', 'method' => 'post','name' => 'mallSignUp','id' => 'mallSignUp', 'class' => 'form-inline mallSignUp')) }}
            {{ Form::hidden('admin_level',$for) }}
            <div class="row-fluid">
                <div class="span5">
                    @if($for=="Marketing Manager")
                        <div class="control-group  {{ ($errors->has('mallID') ? 'error' : '') }}">
                            <label for="email" class="control-label"><strong>Your mall*</strong></label>
                            <div class="controls">
                                {{ Form::select('mallIDs[]',$mallList,NULL,array('id'=>'mall', 'class'=>'multi-mall','multiple')) }}
                                <span class="help-block">{{ ($errors->has('mallIDs') ? $errors->first('mallIDs') : '') }}</span>
                            </div>
                        </div>
                    @else
                        <div class="control-group  {{ ($errors->has('mallID') ? 'error' : '') }}">
                            <label for="email" class="control-label"><strong>Please select your mall*</strong></label>
                            <div class="controls">
                                {{ Form::select('mallID',$mallList,NULL,array('id'=>'mall')) }}
                                <span class="help-block">{{ ($errors->has('mallID') ? $errors->first('mallID') : '') }}</span>
                            </div>
                        </div>
                    @endif
                </div>

                <div class="span5">
                    <br/>
                    If your mall is not listed, please <a href="{{ route('malls.register') }}">Register your Mall</a>
                </div>

            </div>

            @include('partials.loginInfo')
            <div class="clearfix"></div>
        </div>
    </div>
@endsection

@section('exScript')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".multi-mall").select2({});
            $('.datetimepicker4').datetimepicker({
                pickTime: false
            });
            $('#password').keyup(function () {
                var _parent = $(this).parent().parent();


                var level = 0;
                var mesg = '';
                var input = $(this).val();

                if (/(.*[A-Z])/.test(input) == false) {
                    level = 1;
                    mesg += ' No uppercase letters.<br/>';

                }

                if (/(.*[\!|\@|\#|\$|\_])/.test(input) == false) {
                    level = 2;
                    mesg += ' No special character (!@#$_).<br/>';

                }
                if (/(.*[\d])/.test(input) == false) {
                    level = 3;
                    mesg += ' No numbers.<br/>';

                }
                if (/(.*[a-z])/.test(input) == false) {
                    level = 4;
                    mesg += ' No lowercase letters.<br/>';

                }

                if (input.length < 6) {
                    level = 5;
                    mesg += ' Not long enough.<br/>';

                }


                $('#password_help').html(mesg);

                if (level != 0) {
                    _parent.addClass('error');

                    $('#sub').attr('disabled', 'disabled');
                } else {
                    $('#sub').removeAttr('disabled');
                }
            });
        });
    </script>
@endsection