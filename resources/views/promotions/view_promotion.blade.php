@extends('layouts.pages')
@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            @include('partials.headers.search')
            <div class="row-fluid">
                <div class="span8">
                    <h1 class="animated fadeInDown delay1"><span>Promotions: </span>New and current</h1>

                    <p class="animated fadeInDown delay2">Love a good deal? So do we, get them here!</p>
                </div>
                <!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div>
                <!--/span6-->
            </div>
            <!--/row-->
        </div>
        <!--/container-->
    </div><!--/three-->
@stop

@section('content')

    <div class="container-fluid"><!--row --><!--row -->
        <div class="row-fluid">
            <div class="span12">
                <h4 class="hborder">Promotion @ <a class="header-link"
                                                   href="/shops/view/{{$promotion->shop->shopMGID}}/{{ slugify($promotion->shop->mall->name) }}/{{ slugify($promotion->shop->name) }}">{{ $promotion->shop->name }}</a>
                    - <a href="/malls/view/{{$promotion->mall->mallID}}/{{ slugify($promotion->mall->name) }}"
                         class="header-link">{{ $promotion->mall->name }}</a></h4>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span2">

                <div class="carousel-inner">
                    <?php
                    $_image = (!empty($promotion->thumbnail1)) ? 'uploadimages/mall_' . $mall->mallID . '/' . $promotion->thumbnail1 : 'img/nopromotion.png';
                    ?>
                    <div class="zoom-gallery">
                        <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $promotion->mallID }}/{{ $promotion->image1 }}"
                           title="Promotion">
                            <img src="{{ Config::get('app.url') }}/{{ $_image }}"
                                 alt="{{ $promotion->shop->name }} promotion"/>
                        </a>
                    </div>

                </div>


            </div>

            <div class="span10">
                <div class="overlay-wrapper">

                    <div class="overlay"></div>
                </div>
                <div class="blog-content border_top">
                    <div>
                        <dl>
                            {{ date('j F Y', strtotime($promotion->startDate)) }} - {{ date('j F Y',
                                strtotime($promotion->endDate)) }}

                        </dl>

                        <dl>

                            {{ $promotion->promotion }}

                        </dl>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span12">
            <br/>
            <div class="text-right">
                <button class="btn btn-inverse go_back">&laquo; Go back</button>
            </div>

        </div>
    </div>
    <!--/container-->

@stop

@section('exScript')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.zoom-gallery').magnificPopup({
                delegate: 'a',
                type: 'image',
                closeOnContentClick: false,
                closeBtnInside: true,
                mainClass: 'mfp-with-zoom mfp-img-mobile',
                gallery: {
                    enabled: true
                },
                zoom: {
                    enabled: true,
                    duration: 300, // don't foget to change the duration also in CSS
                    opener: function (element) {
                        return element.find('img');
                    }
                }

            });
        });
    </script>
@stop
