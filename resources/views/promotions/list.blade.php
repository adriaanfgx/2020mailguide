@extends('layouts.pages')

@section('heading')
    <div class="inner-heading" style="background: url('/img/{{ $banner }}.jpg') no-repeat top right;">
        <div class="container-fluid">
            @include('partials.headers.search')
            <div class="row-fluid">
                <div class="span8">
                    <h1 class="animated fadeInDown delay1"><span>Promotions: </span>New and current</h1>
                    <p class="animated fadeInDown delay2">Love a good deal? So do we, get them here!</p>
                </div><!--/span6-->
                <div id="breadcrumbs" class="span6">
                    {{ generateBreadcrumbs() }}
                </div><!--/span6-->
            </div><!--/row-->
        </div><!--/container-->
    </div><!--/three-->

@stop

@section('content')
    <div class="row-fluid">
        <div class="hborder">
            <h4>Promotions</h4>
            <div class="row-fluid">
                <div class="span3 offset2">
                    <div class="overlay-wrapper"></div>
                    <select name="provinces" id="provinces">
                        <option value="">Please Select a Province...</option>
                        @foreach($provinces as $key=>$value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="span3">
                    <?php $mallSelected = session()->get('selected_mall', ''); ?>
                    <div class="overlay-wrapper"></div>
                    {{ Form::select('mall_id',$malls,$mallSelected,array('id'=>'mall_id','disabled')) }}
                    <?php session()->forget('selected_mall'); ?>
                </div>
                <div class="span3">
                    <div class="overlay-wrapper"></div>
                    <div class="input-append">
                        <input type="text" name="Search" value="" id="search_string" placeholder="Keyword Search"/>
                        <button type="submit" id="search" class="btn btn-info" id="shopsearch"><i
                                    class="icon-search"></i></button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row-fluid">
        <div class="span12">
            <table class="table table-bordered table-striped responsive promotions-table">
                <thead>
                <tr>
                    <th>Mall</th>
                    {{--<th>Shop</th>--}}
                    <th>Promotion</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                </tr>
                </thead>
                <tbody id="promotion_data">
                @if(isset($promotions) && count($promotions))
                    @foreach($promotions as $promotion)
                        {{--@if(isset($promotion->mall))--}}
                        <tr>
                            <td>
                                <a href="/malls/view/{{ $promotion->mall->mallID }}/{{ slugify($promotion->mall->name) }}">{{ $promotion->mall->name }}</a>
                                @if(($promotion->mall->mallCity && $promotion->mall->mallProvince))
                                    <br>
                                    <a href="/malls/?city={{ $promotion->mall->city_id }}-{{ slugify($promotion->mall->mallCity->name) }}"
                                       class="dark-link">{{ $promotion->mall->mallCity->name }}</a>, <a
                                            href="/malls/provinceID/{{ $promotion->mall->province_id }}"
                                            class="dark-link">{{ $promotion->mall->mallProvince->name }}</a>
                                @endif
                            </td>
                            {{--<td><a href='/shops/view/{{$promotion->shop->shopMGID}}/{{ slugify($promotion->shop->mall->name) }}/{{ slugify($promotion->shop->name) }}'>{{ $promotion->shop->name }}</a></td>--}}
                            <td>
                                <a href='{{ route('promotions.view',$promotion->promotionsMGID) }}'>{{ Str::limit(strip_tags($promotion->promotion), 30) }}</a>
                            </td>
                            <td>{{ date('j F Y', strtotime($promotion->startDate)) }}</td>
                            <td>{{ date('j F Y', strtotime($promotion->endDate)) }}</td>
                        </tr>
                        {{--@endif--}}
                    @endforeach
                @else

                    <tr>
                        <td colspan="5">Please select a Province first...</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div><!--span12 -->
    </div>


    </div>

@stop

@section('exScript')
    <script>

        $(document).ready(function () {

            $('#mall_id').attr('disabled', 'disabled');

            if ($("#provinces").val() != '') {

                $('#mall_id').attr('disabled', false);
                var mallID = $('#mall_id').val();
                //Get shops for the pre-selected mall
                if (mallID) {
                    $('#promotion_data').load('/promotions/populateTable/' + mallID);
                } else {
                    $('#promotion_data').load('/promotions/populateTable/0/' + mallID);
                }
            } else {
                $('#mall_id').attr('disabled', 'disabled');
                $('#mall_id').val('Select a province ...');
            }

            $("#mall_id").change(function () {

                $("#promotion_data").empty().html('<tr><td colspan="4">Loading Promotions...</td></tr>');

                var mallID = $(this).val();

                if ($(this).val() == '') {
                    if ($('#provinces').val() != '') {
                        $('#promotion_data').load('/promotions/populateTable/0/' + $('#provinces').val());
                    } else {
                        $("#promotion_data").empty().html('<tr><td colspan="4">Please select a Mall...</td></tr>');
                    }
                } else {
                    $('#promotion_data').load('/promotions/populateTable/' + mallID);
                }


            });


            $('#provinces').change(function () {
                $("#promotion_data").empty().html('<tr><td colspan="4">Loading Malls...</td></tr>');

                var $province = $('#provinces').val();
                if ($province == '') {
                    $('#promotion_data').load('/promotions/populateTable/');
//                $("#promotion_data").empty().html('<tr><td colspan="4">Please select a province first...</td></tr>');
                    $('#mall_id').empty().append('<option value="">Malls</option>');
                    $('#mall_id').attr('disabled', 'disabled');
                } else {
                    $('#promotion_data').load('/promotions/populateTable/0/' + $province);
                    $.ajax({
                        type: 'GET',
                        dataType: 'json',
                        url: '/malls/filter/' + $province,
                        success: function (data) {

                            $('#mall_id').removeAttr('disabled');
                            $('#mall_id').empty().append('<option value="">Please select a Mall...</option>');

                            $.each(data, function (i, val) {
                                $('#mall_id').append('<option value="' + val.mallID + '">' + val.name + ' (' + val.totalPromotions + ')</option>');
                            });


                            $('#promotion_data').empty().html('<tr><td colspan="5">Malls loaded. Please select a Mall.</td></tr>');
//
                        }
                    });
                }
            });
        });

        $("#search").click(function () {

            if ($("#search_string").val() != '') {
                $('#promotion_data').load('/promotions/search/' + $("#search_string").val());
            }
        });
    </script>
@stop
