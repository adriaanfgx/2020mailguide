<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Mallguide</title>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,500,800' rel='stylesheet' type='text/css'>
</head>

<body marginheight="0">
<table width="650" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="650" align="left" valign="top"><table width="650" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td width="650" height="40" align="right" valign="middle" style="font-family:'Raleway', sans-serif; font-size:12px; color:#828689; font-weight:400;">Having trouble viewing this email? <a href="<ONLINE_LINK>" style="font-family:'Raleway', sans-serif; font-size:12px; color:#828689; font-weight:400; text-decoration:none;">Click here</a> to view it in your browser.</td>
  </tr>
  <tr>
    <td width="650" align="left" valign="top"><a href="http://www.mallguide.co.za/"><img src="http://www.mallguide.co.za/mallguide/img/newsletter/mallguide001.jpg" width="650" height="249" style="display:block; font-family:'Raleway', sans-serif; font-size:12px; color:#828689; background-color:#333333;" border="0" alt="Mallguide" /></a></td>
  </tr>
  <tr>
    <td width="650" align="left" valign="top" style="background-color:#31353a;"><table width="650" border="0" align="center" cellpadding="0" cellspacing="0">
      <tr>
        <td align="left" valign="middle" style="font-family:'Raleway', sans-serif; font-size:14px; color:#828689; font-weight:400; padding-left:15px;">Mallguide strives to bring all things retail to you, in one location.<br />
Why not show us some love and socialize with us...</td>
        <td align="right" valign="top" width="55"><a href="https://twitter.com/mallguide"><img src="http://www.mallguide.co.za/mallguide/img/newsletter/mallguide004.jpg" width="55" height="65" style="display:block; font-family:'Raleway', sans-serif; font-size:12px; color:#fff;" border="0" alt="Twitter" /></a></td>
        <td align="right" valign="top" width="55"><a href="https://www.facebook.com/mallguide"><img src="http://www.mallguide.co.za/mallguide/img/newsletter/mallguide003.jpg" width="55" height="65" style="display:block; font-family:'Raleway', sans-serif; font-size:12px; color:#fff;" border="0" alt="Facebook" /></a></td>
        <td align="right" valign="top" width="63"><a href="http://pinterest.com/mallguide/"><img src="http://www.mallguide.co.za/mallguide/img/newsletter/mallguide002.jpg" width="63" height="65" style="display:block; font-family:'Raleway', sans-serif; font-size:12px; color:#fff;" border="0" alt="Pintrest" /></a></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td width="650" align="left" valign="top" style="background-color:#1192d3;"><img src="http://www.mallguide.co.za/mallguide/img/newsletter/mallguide005.jpg" width="1" height="4" style="display:block;" /></td>
  </tr>
  <tr>
    <td width="650" height="45" align="left" valign="middle" style="font-family:'Raleway', sans-serif; font-size:24px; color:#828689; font-weight:400">RESET YOUR <font style="font-weight:800; color:#31353a;">PASSWORD.</font></td>
  </tr>
  <tr>
    <td width="650" align="left" valign="top" style="background-color:#efefef;"><img src="http://www.mallguide.co.za/mallguide/img/newsletter/mallguide007.jpg" width="1" height="1" /></td>
  </tr>
  <tr>
    <td width="650" align="left" valign="top" style="font-family:'Raleway', sans-serif; font-size:16px; color:#828689; font-weight:400; padding-top:15px; padding-bottom:15px; text-align:justify;">



 Dear Client<br />
<br />


As you may be aware we have been shaking things up a bit at Mallguide, and we are ready to release the new look management area.
From here you can manage your stores, events, exhibitions and Mall's information and more - all under one roof, and now from your iPad's as well!<br /><br />

Next in our pipe-line is the new communication system, that will again make available email and SMS communication. We are working
at making this function even easier to use and added some awesome additional reporting. This will be available soon, but remember should
you need to send out any communication in the interim - we are happy to help out. Please send through your requirements to <a href="mailto:andrea@fgx.co.za" style="font-family:'Raleway', sans-serif; color:#31353a; font-weight:400; text-decoration:none;">andrea@fgx.co.za</a> and we will make sure it's taken care of in a jiffy.<br /><br />

To access the new Management section, you will need to reset your password <a href="http://www.mallguide.co.za/users/reset-password" target="_blank" style="font-family:'Raleway', sans-serif; color:#31353a; font-weight:400; text-decoration:none;">here</a>. Should you not be able to reset your password using your existing email address, please contact Nicky (<a href="mailto:nicky@fgx.co.za" style="font-family:'Raleway', sans-serif; color:#31353a; font-weight:400; text-decoration:none;">nicky@fgx.co.za</a>) for assistance.<br />
<br />

We hope you all enjoy the new look!<br /><br />

The Mallguide team@fgx



    </td>
  </tr>
  <tr>
    <td width="650" align="left" valign="top" style="background-color:#eeeeee;"><img src="http://www.mallguide.co.za/mallguide/img/newsletter/mallguide006.jpg" width="1" height="43" style="display:block;" /></td>
  </tr>
  <tr>
    <td width="650" align="left" valign="middle" style="font-family:'Raleway', sans-serif; font-size:24px; color:#fff; font-weight:400; padding-left:15px; background-color:#31353a; padding-top:15px; padding-bottom:5px;">GET IN TOUCH <font style="font-weight:800; color:#fff;">WITH US.</font></td>
  </tr>
  <tr>
    <td width="650" align="left" valign="top" style="font-family:'Raleway', sans-serif; font-size:16px; color:#fff; font-weight:400; padding-left:15px; background-color:#31353a; padding-bottom:15px; line-height:24px;">Tell: 0861 349 932<br />
Email us: <a href="mailto:info@mallguide.co.za" style="font-family:'Raleway', sans-serif; font-size:16px; color:#fff; font-weight:400; text-decoration:none;">info@mallguide.co.za</a></td>
  </tr>
  <tr>
    <td width="650" height="45" align="left" valign="middle" style="font-family:'Raleway', sans-serif; font-size:12px; color:#828689; font-weight:400;">Copyright 2014 - Mallguide - All Rights Reserved&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a href="<UNSUBSCRIBE>" style="font-family:'Raleway', sans-serif; font-size:12px; color:#828689; font-weight:400; text-decoration:none;">Unsubscribe</a></td>
  </tr>
</table></td>
  </tr>
</table>
</body>
</html>


