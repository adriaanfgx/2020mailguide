<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Mallguide</title>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,800' rel='stylesheet' type='text/css'>
</head>

<body marginheight="0">
<table width="650" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
        <td width="650" align="left" valign="top">
            <table width="650" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="650" height="20"></td>
                </tr>
                <tr>
                    <td width="650" align="left" valign="top"><a href="http://www.mallguide.co.za/"><img
                                    src="http://www.mallguide.co.za/img/newsletter/mallguide001.jpg" width="650"
                                    height="249"
                                    style="display:block; font-family:'Raleway', sans-serif; font-size:12px; color:#828689; background-color:#333333;"
                                    border="0" alt="Mallguide"/></a></td>
                </tr>
                <tr>
                    <td width="650" align="left" valign="top" style="background-color:#31353a;">
                        <table width="650" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td align="left" valign="middle"
                                    style="font-family:'Raleway', sans-serif; font-size:14px; color:#828689; font-weight:400; padding-left:15px;">
                                    Mallguide strives to bring all things retail to you, in one location.<br/>
                                    Why not show us some love and socialize with us...
                                </td>
                                <td align="right" valign="top" width="55"><a href="https://twitter.com/mallguide"><img
                                                src="http://www.mallguide.co.za/img/newsletter/mallguide004.jpg"
                                                width="55" height="65"
                                                style="display:block; font-family:'Raleway', sans-serif; font-size:12px; color:#fff;"
                                                border="0" alt="Twitter"/></a></td>
                                <td align="right" valign="top" width="55"><a
                                            href="https://www.facebook.com/mallguide"><img
                                                src="http://www.mallguide.co.za/img/newsletter/mallguide003.jpg"
                                                width="55" height="65"
                                                style="display:block; font-family:'Raleway', sans-serif; font-size:12px; color:#fff;"
                                                border="0" alt="Facebook"/></a></td>
                                <td align="right" valign="top" width="63"><a href="http://pinterest.com/mallguide/"><img
                                                src="http://www.mallguide.co.za/img/newsletter/mallguide002.jpg"
                                                width="63" height="65"
                                                style="display:block; font-family:'Raleway', sans-serif; font-size:12px; color:#fff;"
                                                border="0" alt="Pintrest"/></a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td width="650" align="left" valign="top" style="background-color:#1192d3;"><img
                                src="http://www.mallguide.co.za/img/newsletter/mallguide005.jpg" width="1" height="4"
                                style="display:block;"/></td>
                </tr>
                <tr>
                    <td width="650" height="45" align="left" valign="middle"
                        style="font-family:'Raleway', sans-serif; font-size:24px; color:#828689; font-weight:400">NEW
                        SHOP <font style="font-weight:800; color:#31353a;">REGISTRATION.</font></td>
                </tr>
                <tr>
                    <td width="650" align="left" valign="top" style="background-color:#efefef;"><img
                                src="http://www.mallguide.co.za/img/newsletter/mallguide007.jpg" width="1" height="1"/>
                    </td>
                </tr>
                <tr>
                    <td width="650" align="left" valign="top"
                        style="font-family:'Raleway', sans-serif; font-size:16px; color:#828689; font-weight:400; padding-top:15px; padding-bottom:15px; text-align:justify;">


                        Hi {{$user_name}},
                        <br/>
                        <br/>
                        <div class="article-content">
                            {{$the_message}}

                            <p>Here are some of the details that have been added for this shop.</p>
                            <table class="table">
                                <tr>
                                    <td>
                                        Name:
                                    <td>
                                        {{$name}}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Mall:
                                    <td>
                                        {{$mall_name}}
                                    </td>
                                </tr>
                                @if(isset($keywords) && $keywords!=NULL)
                                    <tr>
                                        <td>
                                            Keywords:
                                        </td>
                                        <td>
                                            {{$keywords}}
                                        </td>
                                    </tr>
                                @endif
                                @if(isset($description) && $description!=NULL)
                                    <tr>
                                        <td>
                                            Description:
                                        </td>
                                        <td>
                                            {{$description}}
                                        </td>
                                    </tr>
                                @endif
                                @if(isset($category) && $category!=NULL)
                                    <tr>
                                        <td>
                                            Category:
                                        </td>
                                        <td>
                                            {{$category}}
                                        </td>
                                    </tr>
                                @endif
                                @if(isset($shopNumber) && $shopNumber!=NULL)
                                    <tr>
                                        <td>
                                            Shop Number:
                                        </td>
                                        <td>
                                            {{$shopNumber}}
                                        </td>
                                    </tr>
                                @endif
                                @if(isset($landmark) && $landmark!=NULL)
                                    <tr>
                                        <td>
                                            Land Mark:
                                        </td>
                                        <td>
                                            {{$landmark}}
                                        </td>
                                    </tr>
                                @endif
                                @if(isset($telephone) && $telephone!=NULL)
                                    <tr>
                                        <td>
                                            Telephone:
                                        </td>
                                        <td>
                                            {{$telephone}}
                                        </td>
                                    </tr>
                                @endif
                                @if(isset($fax) && $fax!=NULL)
                                    <tr>
                                        <td>
                                            Fax:
                                        </td>
                                        <td>
                                            {{$fax}}
                                        </td>
                                    </tr>
                                @endif
                                @if(isset($contactPerson) && $contactPerson!=NULL)
                                    <tr>
                                        <td>
                                            Contact Person:
                                        </td>
                                        <td>
                                            {{$contactPerson}}
                                        </td>
                                    </tr>
                                @endif
                                @if(isset($email) && $email!=NULL)
                                    <tr>
                                        <td>
                                            Email:
                                        </td>
                                        <td>
                                            {{$email}}
                                        </td>
                                    </tr>
                                @endif
                                @if(isset($tradingHours) && $tradingHours!=NULL)
                                    <tr>
                                        <td>
                                            Trading Hours:
                                        </td>
                                        <td>
                                            {{$tradingHours}}
                                        </td>
                                    </tr>
                                @endif
                                @if(isset($managerName) && $managerName!=NULL)
                                    <tr>
                                        <td>
                                            Manager Name:
                                        </td>
                                        <td>
                                            {{$managerName}}
                                        </td>
                                    </tr>
                                @endif
                                @if(isset($province) && $province!=NULL)
                                    <tr>
                                        <td>
                                            Province:
                                        </td>
                                        <td>
                                            {{$province}}
                                        </td>
                                    </tr>
                                @endif
                                @if(isset($city) && $city!=NULL)
                                    <tr>
                                        <td>
                                            City:
                                        </td>
                                        <td>
                                            {{$city}}
                                        </td>
                                    </tr>
                                @endif
                                @if(isset($physicalAddress) && $physicalAddress!=NULL)
                                    <tr>
                                        <td>
                                            Physical Address:
                                        </td>
                                        <td>
                                            $physicalAddress
                                        </td>
                                    </tr>
                                @endif

                            </table>

                        </div>
                        <br/><br/>

                        Kind regards<br/>

                        The Mallguide team@fgx


                    </td>
                </tr>
                <tr>
                    <td width="650" align="left" valign="top" style="background-color:#eeeeee;"><img
                                src="http://www.mallguide.co.za/img/newsletter/mallguide006.jpg" width="1" height="43"
                                style="display:block;"/></td>
                </tr>
                <tr>
                    <td width="650" align="left" valign="middle"
                        style="font-family:'Raleway', sans-serif; font-size:24px; color:#fff; font-weight:400; padding-left:15px; background-color:#31353a; padding-top:15px; padding-bottom:5px;">
                        GET IN TOUCH <font style="font-weight:800; color:#fff;">WITH US.</font></td>
                </tr>
                <tr>
                    <td width="650" align="left" valign="top"
                        style="font-family:'Raleway', sans-serif; font-size:16px; color:#fff; font-weight:400; padding-left:15px; background-color:#31353a; padding-bottom:15px; line-height:24px;">
                        Tel: 011 051 2800<br/>
                        Email us: <a href="mailto:info@mallguide.co.za"
                                     style="font-family:'Raleway', sans-serif; font-size:16px; color:#fff; font-weight:400; text-decoration:underline;">info@mallguide.co.za</a>
                    </td>
                </tr>
                <tr>
                    <td width="650" height="45" align="left" valign="middle"
                        style="font-family:'Raleway', sans-serif; font-size:12px; color:#828689; font-weight:400;">
                        Copyright 2014 - Mallguide - All Rights Reserved&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;<a
                                href="<UNSUBSCRIBE>"
                                style="font-family:'Raleway', sans-serif; font-size:12px; color:#828689; font-weight:400; text-decoration:underline;">Unsubscribe</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>