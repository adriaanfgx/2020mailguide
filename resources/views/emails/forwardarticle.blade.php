<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
    <title>Mallguide</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'><style type="text/css">

    </style>


</head>

<body style="padding: 0;margin-left: 0px;margin-top: 0px;margin-right: 0px;margin-bottom: 0px;background-color: #fff;width: 100%;">
<!--start BGtable-->
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="BGtable" background="fff" style="border-collapse: collapse;margin: 0;padding: 0;background-color: #fff;">
    <tr>
        <td valign="top" class="BGtable-inner" style="padding: 15px 0;">

            <!--start header style3-->
            <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="row" style="border-collapse: collapse;">
                <tr>
                    <td><table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                            <tr>
                                <td class="align-td" style="border: none;padding: 4px 3px 4px 4px;"><table width="292" border="0" cellpadding="0" cellspacing="0" class="col2-oblong ori main-color" style="border-collapse: collapse;background-color: #fff;width: 292px;height: 142px;">
                                        <tr>
                                            <td height="142" align="center" class="logo"><img src="http://www.cmsignition.co.za/uploadimages/mallguide/mall_logo.gif" alt="Mallguide - Find Shopping Your Way" style="border: 0;display: block;color: #ddd;"></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table>
                        <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse; background-color:fff;">
                            <tr>
                                <td class="box-td" style="padding: 4px;"><table width="142" border="0" cellpadding="0" cellspacing="0" class="col4 second-color" style="border-collapse: collapse;background-color: #fff;width: 142px;height: 142px;">

                                        <tr>
                                            <td height="36" bgcolor="#19799F" class="icon-title" style="padding: 0 5px 0 10px;height: 36px;">
                                                <div class="icon-title-box" style="width: 127px;word-wrap: normal;font-family:'Open Sans','Segoe UI', Tahoma, Arial;color: #fff;font-weight: 400;font-size: 14px;white-space: nowrap;"><a href="<ONLINE_LINK>" target="_blank" style="text-decoration: none;outline: none;color: #fff;">Everything Retail<br />
                                                        Nov 2013<br />View Online</a></div></td>
                                        </tr>
                                    </table></td>
                                <td class="box-td hide360" style="padding: 4px;"><table width="142" border="0" cellpadding="0" cellspacing="0" class="col4" style="border-collapse: collapse;width: 142px;height: 142px;">
                                        <tr>
                                            <td width="67" height="67" align="center" valign="middle" bgcolor="#F1B634" class="third-color social-td" style="background-color: #F1B634;"><a href="https://www.facebook.com/mallguide" target="_blank" style="text-decoration: underline;outline: none;color: #fff;"><img src="http://www.cmsignition.co.za/uploadimages/mallguide/logo_fb1.gif" alt="facebook" class="icon-48" style="border: 0;display: block;color: #ddd;max-height: 48px;"></a></td>
                                            <td width="8"></td>
                                            <td width="67" height="67" align="center" valign="middle" bgcolor="#11B9E3" class="third-color social-td" style="background-color: #18C0EA;"><a href="https://twitter.com/mallguide" target="_blank" style="text-decoration: underline;outline: none;color: #fff;"><img src="http://www.cmsignition.co.za/uploadimages/mallguide/logo_twitter1.gif" alt="twitter" class="icon-48" style="border: 0;display: block;color: #ddd;max-height: 48px;"></a></td>
                                        </tr>
                                        <tr>
                                            <td height="8"></td>
                                            <td width="8" height="8"></td>
                                            <td height="8"></td>
                                        </tr>
                                        <tr>
                                            <td width="67" height="67" align="center" valign="middle" bgcolor="#18C0EA" class="third-color social-td" style="background-color: #18C0EA;"><a href="http://pinterest.com/mallguide/" target="_blank" style="text-decoration: underline;outline: none;color: #fff;"><img src="http://www.cmsignition.co.za/uploadimages/mallguide/logo_pin1.gif" alt="pinterest" class="icon-48" style="border: 0;display: block;color: #ddd;max-height: 48px;"></a></td>
                                            <td width="8"></td>
                                            <td width="67" height="67" align="center" valign="middle" bgcolor="#F1B634" class="third-color social-td" style="background-color: #F1B634;"><a href="{{ Config::get('app.url') }}/" target="_blank" style="text-decoration: underline;outline: none;color: #fff;"><img src="http://www.cmsignition.co.za/uploadimages/mallguide/logo_web1.gif" alt="www.mallguide.co.za" class="icon-48" style="border: 0;display: block;color: #ddd;max-height: 48px;"></a></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
            </table>
            <!--end header style3-->

            <!--start header img with title--><!--end header img with title-->

            <!--start header letter-->
            <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="row" style="border-collapse: collapse;">
                <tr>
                    <td class="box-td" style="padding: 4px;"><table width="592" border="0" cellpadding="0" cellspacing="0" class="full text second-color" style="border-collapse: collapse;background-color: #fff;width: 592px;height: 292px;">
                            <tr>
                                <td height="282" valign="top" class="content letter text" style="padding: 7px 15px 3px;">
                                    <div class="article-title h3" style="font-family: 'open_sanslight', 'Open Sans', 'Segoe UI' 'Lucida Grande', sans-serif;color: #515151;font-weight: 100;font-size: 25px;line-height: 50px;letter-spacing: -2px;vertical-align: middle;width: 100%;overflow: hidden;text-overflow: clip;word-wrap: break-word;text-align: left;max-height: 100px;">Hi {{ $friendsName }}</div>

                                    <div class="article-content">
                                        A friend of yours, $yourName, thinks you will find the following page to be of interest:<br />
                                        <a href="{{ $articleLink }}">{{ $articleLink }}</a>
                                        <br />
                                        <br />
                                        Kind regards<br />
                                        The Mallguide Team<br />
                                        <br />
                                        <a href="{{ Config::get('app.url') }}">{{ Config::get('app.url') }}</a>
                                    </div></td>
                            </tr>
                        </table></td>
                </tr>
            </table>
            <!--end header letter-->
            <!--start col-2 text with img--><!--start col-2 img with text--><!--start col4 icon--><!--end col4 icon--><!--start footer style2-->
            <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="row" style="border-collapse: collapse;">
                <tr>
                    <td><table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse: collapse;border: none;mso-table-lspace: 0pt;mso-table-rspace: 0pt;">
                            <tr>
                                <td valign="top" class="align-td" style="border: none;padding: 4px 3px 4px 4px;"><table width="292" border="0" cellpadding="0" cellspacing="0" class="second-color col2-oblong address" style="border-collapse: collapse;background-color: #e97041;width: 292px;height: 142px;">
                                        <tr>
                                            <td height="132" valign="top" class="content tel" style="padding: 7px 15px 3px;"><table border="0" cellpadding="0" cellspacing="0" class="btn tel" style="border-collapse: collapse;height: 24px;">
                                                    <tr>
                                                        <td width="25" height="24" class="icon" style="height: 24px;padding: 0 10px 0 0;width: 20px;"><a href="#" style="text-decoration: underline;outline: none;color: #fff;"><img src="http://www.cmsignition.co.za/uploadimages/mallguide/web.png" alt="»" width="20" height="18" class="icon-btn" style="border: 0;display: block;color: #ddd;max-height: 18px;height: 18px;width: auto;"></a></td>
                                                        <td height="24" class="icon-title" style="font-family:'Open Sans','Segoe UI', Tahoma, Arial;color: #fff;font-weight: 400;height: 24px;padding: 0 4px 0 0;line-height: 13px;font-size: 13px;"><a href="{{ Config::get('app.url') }}" target="_blank" style="text-decoration: none;outline: none;color: #fff;">Web Site</a></td>
                                                    </tr>
                                                </table>
                                                <table border="0" cellpadding="0" cellspacing="0" class="btn tel" style="border-collapse: collapse;height: 24px;">
                                                    <tr>
                                                        <td width="25" height="24" class="icon" style="height: 24px;padding: 0 10px 0 0;width: 20px;"><a href="#" style="text-decoration: underline;outline: none;color: #fff;"><img src="http://www.cmsignition.co.za/uploadimages/mallguide/unsubscribe.png" alt="»" width="18" height="18" class="icon-btn" style="border: 0;display: block;color: #ddd;max-height: 18px;height: 18px;width: auto;"></a></td>
                                                        <td height="24" class="icon-title" style="font-family:'Open Sans','Segoe UI', Tahoma, Arial;color: #fff;font-weight: 400;height: 24px;padding: 0 4px 0 0;line-height: 13px;font-size: 13px;"><a href="<UNSUBSCRIBE>" style="text-decoration: none;outline: none;color: #fff;">Unsubscribe Here</a></td>
                                                    </tr>
                                                </table>
                                                <table border="0" cellpadding="0" cellspacing="0" class="btn tel hide480" style="border-collapse: collapse;height: 24px;">
                                                    <tr>
                                                        <td width="25" height="24" class="icon" style="height: 24px;padding: 0 10px 0 0;width: 20px;"><img src="http://www.cmsignition.co.za/uploadimages/mallguide/copyright.png" alt="©" width="18" height="18" class="icon-btn" style="border: 0;display: block;color: #ddd;max-height: 18px;height: 18px;width: auto;"></td>
                                                        <td height="24" class="icon-title" style="font-family:'Open Sans','Segoe UI', Tahoma, Arial;color: #fff;font-weight: 400;height: 24px;padding: 0 4px 0 0;line-height: 13px;font-size: 13px;">Mallguide 2013</td>
                                                    </tr>
                                                </table></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table>
                        <table height="100" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;">
                            <tr>
                                <td valign="top" class="box-td hide360" style="padding: 4px;"><table width="142" border="0" cellpadding="0" cellspacing="0" class="main-color col4" style="border-collapse: collapse;background-color: #19799f;width: 142px;height: 142px;">
                                        <tr>
                                            <td height="142" align="center" valign="middle" class="big-la-logo"><img src="http://www.cmsignition.co.za/uploadimages/mallguide/mallguide.png" width="110" height="132" alt="small-logo" style="border: 0;display: block;color: #ddd;"></td>
                                        </tr>
                                    </table></td>
                                <td valign="top" class="box-td" style="padding: 4px;"><table width="142" border="0" cellpadding="0" cellspacing="0" class="col4" style="border-collapse: collapse;width: 142px;height: 142px;">
                                        <tr>
                                            <td width="67" height="67" align="center" valign="middle" class="third-color social-td" style="background-color: #e97041;"><a href="https://www.facebook.com/mallguide" target="_blank" style="text-decoration: underline;outline: none;color: #fff;"><img src="http://www.cmsignition.co.za/uploadimages/mallguide/facebook.png" alt="Facebook" width="48" height="48" class="icon-48" style="border: 0;display: block;color: #ddd;max-height: 48px;"></a></td>
                                            <td width="8"></td>
                                            <td width="67" height="67" align="center" valign="middle" class="third-color social-td" style="background-color: #e97041;"><a href="https://twitter.com/mallguide" target="_blank" style="text-decoration: underline;outline: none;color: #fff;"><img src="http://www.cmsignition.co.za/uploadimages/mallguide/twitter.png" alt="Twitter" width="48" height="48" class="icon-48" style="border: 0;display: block;color: #ddd;max-height: 48px;"></a></td>
                                        </tr>
                                        <tr>
                                            <td height="8"></td>
                                            <td width="8" height="8"></td>
                                            <td height="8"></td>
                                        </tr>
                                        <tr>
                                            <td width="67" height="67" align="center" valign="middle" class="third-color social-td" style="background-color: #e97041;"><a href="http://pinterest.com/mallguide/" target="_blank" style="text-decoration: underline;outline: none;color: #fff;"><img src="http://www.cmsignition.co.za/uploadimages/mallguide/pintewrest.png" alt="Pinterest" width="48" height="48" class="icon-48" style="border: 0;display: block;color: #ddd;max-height: 48px;"></a></td>
                                            <td width="8"></td>
                                            <td width="67" height="67" align="center" valign="middle" class="third-color social-td" style="background-color: #e97041;"><a href="{{ Config::get('app.url') }}" style="text-decoration: underline;outline: none;color: #fff;"><img src="http://www.cmsignition.co.za/uploadimages/mallguide/web2.png" alt="Visit Mallguide" width="48" height="48" class="icon-48" style="border: 0;display: block;color: #ddd;max-height: 48px;"></a></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
            </table>
            <!--end footer style2-->



        </td>
    </tr>
</table>
<!--end BGtable-->
</body>
</html>
