<div align='center' style='background-color: #E5E5E5; '>
    <br />
    <table border='0' cellpadding='0' cellspacing='0' style='padding: 1px; width: 604px; background-color: #DDDDDD; border: 1px solid #D5D5D5;'>
        <tr>
            <td style='background-color: #FFFFFF; padding: 12px; text-align: left; font-size: 11px; font-family: Arial; ' align='left'>
                <h1 style='font-size: 17px; color: #A01212; margin:3px 0px 8px 0px;'>{{ $competition->subject }}: Competition entry</h1>
                Competition entry received on: {{ date_format(date_create($competition->startDate),'jS F Y') }} <br />
                <table border='0' cellpadding='3' cellspacing='0' width='98%' style='font-size: 11px; margin-top: 8px; font-family: Arial;  '>
                    @foreach($entries as $entry)
                        <tr><td>{{ $entry['fieldName'] }}</td><td>{{ $entry['value'] }}</td></tr>
                    @endforeach
                    <tr><td>competitionURL </td><td><a href="$competitionURL">{{$competitionURL}}</a></td></tr>
                </table>

            </td>
        </tr>
    </table>

</div>