@if(count($promotions))
    @foreach($promotions as $promotion)
        <tr>
            <td>
                <a href="/malls/view/{{ $promotion->mall->mallID }}/{{ slugify($promotion->mall->name) }}">{{ $promotion->mall->name }}</a>
                @if(($promotion->mall->mallCity && $promotion->mall->mallProvince))
                    <br>
                    <a href="/malls/?city={{ $promotion->mall->city_id }}-{{ slugify($promotion->mall->mallCity->name) }}"
                       class="dark-link">{{ $promotion->mall->mallCity->name }}</a>, <a
                            href="/malls/provinceID/{{ $promotion->mall->province_id }}"
                            class="dark-link">{{ $promotion->mall->mallProvince->name }}</a>
                @endif
            </td>
            <td>
                <a href='/shops/view/{{$promotion->shop->shopMGID}}/{{ slugify($promotion->shop->mall->name) }}/{{ slugify($promotion->shop->name) }}'>{{ $promotion->shop->name }}</a>
            </td>
            <td>
                <a href='{{ route('promotions.view',$promotion->promotionsMGID) }}'>{{ Str::limit(strip_tags($promotion->promotion), 50) }}</a>
            </td>
            <td>{{ date('j F Y', strtotime($promotion->startDate)) }}</td>
            <td>{{ date('j F Y', strtotime($promotion->endDate)) }}</td>
        </tr>
    @endforeach
@else
    <tr>
        <td colspan="5">There are no Promotions listed for that Mall!</td>
    </tr>
@endif
