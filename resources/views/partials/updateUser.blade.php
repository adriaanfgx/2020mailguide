<div class="row-fluid">
    <div class="leftMargin">
        <h4 class="dotted-border">Update your personal details        <label class="label label-info">Required fields are marked with *</label></h4>

        {{ Form::model($user, array('route' => array('user.postUpdatedetails', $user->id),'method'=>'post','name' => 'userUpdate', 'id' => 'userUpdate', 'class' => 'form-inline')) }}
        <div class="row-fluid">
            <div class="span4">
                <div class="control-group {{ ($errors->has('first_name') ? 'error' : '') }}">
                    <label for="first_name" class="control-label"><strong>First Name*</strong></label>
                    <div class="controls">
                        {{ Form::text('first_name',null,array('class'=>'txtbar')) }}
                    </div>
                    <span class="help-block">{{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}</span>
                </div>
            </div>

            <div class="span4">
                <div class="control-group">
                    <label for="last_name"><strong>Surname</strong></label>
                    <div class="controls">
                        {{ Form::text('last_name',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span4">
                <div class="control-group">
                    <label for="company">Company</label>
                    <div class="controls">
                        {{ Form::text('company',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="vat_number">Vat Number</label>
                    <div class="controls">
                        {{ Form::text('vat_number',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span4">
                <div class="control-group">
                    <label for="postal_address">Postal Address</label>
                    <div class="controls">
                        {{ Form::textarea('postal_address',null,array('class'=>'txtbox','rows'=>'3')) }}
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="province"><strong>Province </strong></label>
                    <div class="controls">

                        <select name="province_id" id="province" class="txtbar">
							<option value="">Please Select...</option>
							@foreach($provinces as $id=>$province)
                                        <option value="{{ $id }}"@if($user->province_id == $id) selected="selected" @endif>{{ $province }}</option>
							@endforeach
						</select>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="control-group">
                    <label for="city"><strong>City</strong></label>
                    <div class="controls">
                       <select name="city_id" id="city">
							<option value="">Please Select...</option>
								@foreach($cities as $key=>$value)
							<option value="{{ $key }}" {{$user->city_id == $key ? 'selected="selected"':""}} >{{ $value }}</option>
							@endforeach
						</select>
                    </div>
                </div>
            </div>
             <div class="span4">
                <div class="control-group">
                    <label for="postal_code">Postal Code</label>
                    <div class="controls">
                        {{ Form::text('postal_code',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
			<div class="span4">
                <div class="control-group">
                    <label for="surbub">Suburb</label>
                    <div class="controls">
                        {{ Form::text('suburb',null,array('class'=>'txtbar')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <h4 class="dotted-border">Contact Details</h4>
        </div>
        <div class="row-fluid">
            <div class="span3">
                <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}">
                    <label for="email">Email*</label>
                    <div class="controls">
                        {{ Form::text('email',null,array('class'=>'span11','readonly')) }}
                        {{ ($errors->has('email') ? $errors->first('email') : '') }}
                    </div>
                </div>
            </div>
            <div class="span3">
                <div class="control-group">
                    <label for="tel_work">Work Tel</label>
                    <div class="controls">
                        {{ Form::text('tel_work',null,array('class'=>'span11')) }}
                    </div>
                </div>
            </div>
            <div class="span3">
                <div class="control-group">
                    <label for="tel_home">Home Tel</label>
                    <div class="controls">
                        {{ Form::text('tel_home',null,array('class'=>'span11')) }}
                    </div>
                </div>
            </div>
            <div class="span3">
                <div class="control-group">
                    <label for="cell">Cell</label>
                    <div class="controls">
                        {{ Form::text('cell',null,array('class'=>'span11')) }}
                    </div>
                </div>
            </div>
        </div>
        <div class="row-fluid">

            <div class="span3">
                <div class="control-group">
                    <label for="fax">Fax</label>
                    <div class="controls">
                        {{ Form::text('fax',null,array('class'=>'span11')) }}
                    </div>
                </div>
            </div>
			<div class="span3">
                <div class="control-group {{ ($errors->has('birth_date') ? 'error' : '') }}">
                    <label for="birth_date" class="control-label">Date of birth</label>
                    <div class="controls">
                        <div class="input-append datetimepicker4">
                            {{ Form::text('birth_date',null,array('readonly')) }}
                            <span class="add-on">
                              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                              </i>
                            </span>
                        </div>
                        <span class="help-block">{{ ($errors->has('birth_date') ? $errors->first('birth_date') : '') }}</span>
                    </div>
                </div>
            </div>
            <div class="span3">
                <div class="control-group">
                    <label for="gender">Gender</label>
                    <div class="controls">
                        {{ Form::radio('gender','Female') }}Female &nbsp;&nbsp;{{ Form::radio('gender','Male') }}Male
                    </div>
                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span4">
                <div class="control-group">
                    <div class="controls">
                        <button class="submit btn btn-info" id="update_user">Submit Details</button>
                    </div>
                </div>
            </div>

        </div>
    </div>

    {{ Form::close() }}
    </div>
</div>