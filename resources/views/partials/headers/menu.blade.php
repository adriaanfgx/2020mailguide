<!-- NAVIGATION AREA -->
<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
    <a href="/malls.php">MALLS</a>
    <a href="/shops.php">SHOPS</a>
    <a href="/restaurants.php">RESTAURANTS</a>
    <a href="/movies.php">MOVIES</a>
    <a href="/events.php">EVENTS</a>
    <a href="/exhibitions.php">EXHIBITIONS</a>
    <a href="/competitions.php">COMPETITIONS</a>
    <a href="/promotions.php">PROMOTIONS</a>
    <a href="/jobs.php">JOBS</a>
    <br /><br />
    <a href="/login.php">LOG IN</a>
    <a href="/sign-up.php">SIGN UP</a>
</div>
<!-- END OF NAVIGATION AREA -->
