<!-- MOVIE SEARCH AREA -->
<div class="searchbg">
    <div class="col-8 col-md-8 p-lr-0"><span class="font30">MOVIE <b>FINDER</b></span></div>
    <div class="col-4 col-md-4 p-t-10 text-right">
        <a href="https://www.facebook.com/mallguide/" target="_blank"><img src="/assets/images/facebook.png" width="25px" alt="Mallguide Facebook" /></a>
        <a href="https://twitter.com/mallguide" target="_blank"><img src="/assets/images/twitter.png" width="25px" alt="Mallguide Twitter" /></a>
    </div>
    <div class="clearfix"></div>
    <br /><br />
    <div class="btn-group">
        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Search a Cinema
        </button>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="#">Bayside</a>
            <a class="dropdown-item" href="#">Bedford Centre</a>
            <a class="dropdown-item" href="#">Blue Route</a>
            <a class="dropdown-item" href="#">Broklyn Mall</a>
        </div>
    </div>
    <br />
    <div class="btn-group">
        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Search Movie
        </button>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="#">1917</a>
            <a class="dropdown-item" href="#">21 Bridges</a>
            <a class="dropdown-item" href="#">Always and Forever</a>
            <a class="dropdown-item" href="#">Arctic Justice</a>
            <a class="dropdown-item" href="#">Bad Boys 3</a>
        </div>
    </div>
    <br />
    <button type="button" class="btn btn-primary">Search</button>
</div>
<!-- END OF MOVIE SEARCH AREA -->
