<!-- SEARCH MODAL -->
<div class="modal fade bd-example-modal-lg" id="searchmodal" tabindex="-1" role="dialog" aria-labelledby="searchmodal" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="col-sm-12">
                    <h2>Search Mallguide</h2>
                    <form class="form-inline mt-2 mt-md-0">
                        <input class="form-control mr-sm-2" type="text" placeholder="Search Store or Mall" aria-label="Search Store or Mall">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<!-- HEADER AREA -->
<div class="row">
    <div class="col-6 col-md-6 col-lg-6">
        <a href="/"><img src="/assets/images/mallguide-logo.png" class="img-fluid" alt="Mallguide" /></a>
    </div>
    <div class="col-6 col-md-6 col-lg-6">
        <a href="javascript:void(0);" class="menu hvr-push" onclick="openNav()"></a>
        <a href="javascript:void(0);" data-toggle="modal" data-target="#searchmodal" class="search"></a>
    </div>
</div>
<!-- END OF HEADER AREA -->
