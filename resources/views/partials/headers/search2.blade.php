<!-- SEARCH AREA -->
<div class="searchbg">
    <div class="col-8 col-md-8 p-lr-0"><span class="font30">SEARCH <b>HERE</b></span></div>
    <div class="col-4 col-md-4 p-t-10 p-lr-0 text-right">
        <a href="https://www.facebook.com/mallguide/" target="_blank"><img src="/assets/images/facebook.png" width="25px" alt="Mallguide Facebook" /></a>
        <a href="https://twitter.com/mallguide" target="_blank"><img src="/assets/images/twitter.png" width="25px" alt="Mallguide Twitter" /></a>
    </div>
    <div class="clearfix"></div>
    <br /><br />
    <div class="btn-group">
        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Search Shop
        </button>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="#">ABSA</a>
            <a class="dropdown-item" href="#">Accessories</a>
            <a class="dropdown-item" href="#">Bata</a>
            <a class="dropdown-item" href="#">Dischem</a>
        </div>
    </div>
    <br />
    <div class="btn-group">
        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Search Mall
        </button>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="#">Fourways Mall</a>
            <a class="dropdown-item" href="#">Sandton City</a>
            <a class="dropdown-item" href="#">Cresta Shopping Centre</a>
            <a class="dropdown-item" href="#">Randburg Square</a>
        </div>
    </div>
    <br />
    <div class="btn-group">
        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Search Location
        </button>
        <div class="dropdown-menu">
            <a class="dropdown-item" href="#">Johannesburg</a>
            <a class="dropdown-item" href="#">Pretoria</a>
            <a class="dropdown-item" href="#">Durban</a>
            <a class="dropdown-item" href="#">Mpumalanga</a>
        </div>
    </div>
    <br />
    <button type="button" class="btn btn-primary">Search</button>
</div>
<!-- END OF SEARCH AREA -->
