<div class="searchtophead">
    <form action="/malls/quickfind" method="POST" name='mall_search' id="mallsearchform">
        @csrf
        <div class="input-append">
            <input type="text" class="" value="{{ Session::get('mallSearchKeyword') }}" name="mall_name" id="mall_name" placeholder="Mall search" />
            <button type="submit" class="btn btn-info" id="mallsearch"><i class="icon-search"></i></button>
        </div>
        <!--                                            </form>-->
    </form>
    <form action="/shops/quickfind" method="POST" name='shop_search' id="shopsearchform">
        @csrf
        <div class="input-append">
            <input type="text" class=""  name="shop_name" value="{{ Session::get('shopSearchKeyword') }}" id="shop_name" placeholder="Shop Search" />
            <button type="submit" class="btn btn-info" id="shopsearch"><i class="icon-search"></i></button>
        </div>

    </form>
</div>
