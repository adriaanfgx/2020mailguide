<footer class="container-fluid font15">
    <p>Mallguide. Copyright &copy; 2020. All rights reserved. Designed & Developed by <a href="https://fgx.co.za" target="_blank" style="color:white">FGX Studios</a>. <a href="/terms-of-use.php" style="color:white">Terms of Use</a> | <a href="/privacy-policy.php" style="color:white">Privacy Policy</a></p><br />
</footer>
