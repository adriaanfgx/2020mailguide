<div class="">
            <div class="overlay-wrapper">
                <div class="overlay"></div>
            </div>
            <div class="blog-content border_top">
                <div class="mall-caption">
                    <h4 class="hborder">Add Review</h4>
                    {{ Form::open(array('route' => 'movies.postReview', 'method' => 'post', 'name' => 'movieReview',
                    'id' => 'movieReview', 'class' => 'form-inline dbdContact')) }}
                    <div class="control-group {{ ($errors->has('reviewer_name') ? 'error' : '') }}">
                        <label for="reviewer_name" class="control-label">Name</label>
                        <div class="controls">
                            {{ Form::text('reviewer_name',null,array('class'=>'txtbar')) }}
                        </div>
                        <span class="help-block">{{ ($errors->has('reviewer_name') ? $errors->first('reviewer_name') : '') }}</span>
                    </div>
                    <div class="control-group">
                        <label for="surname">Surname</label>
                        <div class="controls">
                            <input class="placeholder  txtbar" type="text" name="surname" title="Surname" />
                            <span class="help-inline"></span>
                        </div>
                    </div>
                    <div class="control-group {{ ($errors->has('reviewer_email') ? 'error' : '') }}">
                        <label for="email" class="control-label">Email</label>
                        <div class="controls">
                            {{ Form::text('reviewer_email',null,array('class'=>'txtbar')) }}
                        </div>
                        <span class="help-block">{{ ($errors->has('reviewer_email') ? $errors->first('reviewer_email') : '') }}</span>
                    </div>
                    <div class="control-group">
                        <label>Would you like to subscribe to our news letter?</label>

                        <div class="controls">
                            <input type="radio" name="subscribe" value="Y">Yes
                            <input type="radio" name="subscribe" value="N">No
                        </div>
                    </div>
                    <div class="control-group {{ ($errors->has('review') ? 'error' : '') }}">
                        <label for="review" class="control-label">Review</label>
                        <div class="controls">
                            {{ Form::textarea('review',null,array('class'=>'txtbox','rows'=>'3')) }}
                        </div>
                        <span class="help-block">{{ ($errors->has('review') ? $errors->first('review') : '') }}</span>
                    </div>

                    <div class="control-group {{ ($errors->has('rating') ? 'error' : '') }}">
						<label  class="control-label">Please select a rating</label>
                        <div class="controls" id="custom-form">
                            <span>
                              <input type="radio" name="rating" value="1"><i>1</i>
                              <input type="radio" name="rating" value="2"><i>2</i>
                              <input type="radio" name="rating" value="3"><i>3</i>
                              <input type="radio" name="rating" value="4"><i>4</i>
                              <input type="radio" name="rating" value="5"><i>5</i>

                            </span>
                        </div>
                        <span class="help-block">{{ ($errors->has('rating') ? $errors->first('rating') : '') }}</span>
                        <input type="hidden" name="score" id="score">
                        <input type="hidden" name="movieID" value="{{ $movie->movieID }}">
                        <?php $review = isset($revID) ? $revID : 0; ?>
                        <input type="hidden" name="revID" id="revID" value="{{ $review }}">
                    </div>
                    <div class="control-group">
                        {{ Html::image(Captcha::img(), 'Captcha image') }}
                    </div>
                    <div class="control-group {{ ($errors->has('reviewer_captcha')) ? 'error' : '' }}">
                        <label for="reviewer_captcha" class="control-label">Captcha</label>
                        <div class="controls">
                            {{ Form::text('reviewer_captcha',null) }}
                        </div>
                        <div class="help-block">{{ ($errors->has('reviewer_captcha') ? $errors->first('reviewer_captcha') : '') }}</div>
                    </div>
                    <button class="submit reg-btn" id="add_review">Add Review</button>
                    <span></span>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
<script type="text/javascript">
    $(document).ready(function () {

        $("#thnk_msg").hide();
        $(':radio').change(function () {
            $('.choice').text(this.value + ' stars');
            $("#score").val(this.value);
        });
    });
</script>
