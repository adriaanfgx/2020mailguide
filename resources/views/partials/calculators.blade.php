<div class="span5">
    <h3>Available Calculators</h3>
    Below is a list of our free financial calculators.
    <ul>
        @if(Request::url() != Route('calculator.bond'))
        <li>
            <span class="fa fa-calculator">&nbsp;</span><a href="{{ Route('calculator.bond') }}">Bond Calculator</a>
        </li>
        @endif
        @if(Request::url() != Route('calculator.personal'))
        <li>
           <span class="fa fa-calculator">&nbsp;</span><a href="#">Personal Loan Calculator</a>
        </li>
        @endif
        @if(Request::url() != Route('calculator.vehicle'))
        <li>
          <span class="fa fa-calculator">&nbsp;</span><a href="#">Vehicle Finance Calculator</a>
        </li>
        @endif
    </ul>
</div><!-- span 5-->
