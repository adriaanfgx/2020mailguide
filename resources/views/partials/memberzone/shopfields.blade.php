<div class="row-fluid">
    <div class="span4">
        <div class="control-group">
            <label for="tradingHours">Trading Hours</label>
            <div class="controls">
                {{Form::textarea('tradingHours', null,array('class'=>'txtbox','rows'=>'3')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label for="description">Description</label>
            <div class="controls">
                {{Form::textarea('description',null,array('class'=>'txtbox','rows'=>'3')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label for="keywords">Keywords</label>
            <div class="controls">
                {{Form::textarea('keywords', null,array('class'=>'txtbox','rows'=>'3')) }}
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
        <div class="control-group">
            <label for="category">Category</label>
            <div class="controls">
                {{ Form::select('category',$categories,null,array('id'=>'category')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label for="subcategory">Sub Category</label>
            <div class="controls">
                {{ Form::select('subcategory',$subcategories,null,array('id'=>'subcategory')) }}
            </div>
        </div>
    </div>

</div>
<div class="row-fluid">
    <h4 class="dotted-border">Contact Details</h4>
</div>
<div class="row-fluid">
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="shopNumber"><strong>Shop Number</strong></label>
                {{Form::text('shopNumber', null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="landmark"><strong>Landmark</strong></label>
                {{Form::text('landmark', null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
        <div class="control-group">
            <label for="contactPerson"><strong>Contact Person</strong></label>
            <div class="controls">
                {{Form::text('contactPerson', old('contactPerson'),array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label for="telephone"><strong>Telephone</strong></label>
            <div class="controls">
                {{Form::text('telephone', old('telephone'),array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label for="cell"><strong>Cell</strong></label>
            <div class="controls">
                {{Form::text('cell',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>

</div>

<div class="row-fluid">
    <div class="span4">
        <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}">
            <label for="email"><strong>Email</strong>
                <small class="muted"> Used to send verification email</small>
            </label>
            <div class="controls">
                {{Form::text('email', null,array('class'=>'txtbar')) }}
            </div>
            <span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
        </div>
    </div>
    <div class="span4">
        <div class="control-group {{ ($errors->has('url') ? 'error' : '') }}">
            <div class="controls">
                <label for="url" class="control-label"><strong>Web</strong> &nbsp;
                    <small>( Must start with http:// or https:// )</small>
                </label>
                {{Form::text('url',null,array('class'=>'txtbar')) }}
            </div>
            <span class="help-block">{{ ($errors->has('url') ? $errors->first('url') : '') }}</span>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="fax"><strong>Fax</strong></label>
                {{Form::text('fax', old('fax'),array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <h4 class="dotted-border">Products</h4>
    </div>
</div>
<div class="row-fluid">
    <div class="span4">
        <div class="control-group">
            <label for="product1"><strong>Product 1</strong></label>
            <div class="controls">
                {{Form::text('product1',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label for="product2"><strong>Product 2</strong></label>
            <div class="controls">
                {{Form::text('product2', old('product2'),array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label for="product3"><strong>Product 3</strong></label>
            <div class="controls">
                {{Form::text('product3', null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>

</div>

<div class="row-fluid">
    <div class="span4">
        <div class="control-group">
            <label for="product4"><strong>Product 4</strong></label>
            <div class="controls">
                {{Form::text('product4', null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product5"><strong>Product 5</strong></label>
                {{Form::text('product5', null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product6"><strong>Product 6</strong></label>
                {{Form::text('product6', null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>

</div>
<div class="row-fluid">
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product7"><strong>Product 7</strong></label>
                {{Form::text('product7', null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product8"><strong>Product 8</strong></label>
                {{Form::text('product8', null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product9"><strong>Product 9</strong></label>
                {{Form::text('product9', old('product9'),array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">

    <div class="span4">
        <div class="control-group">
            <div class="controls">
                <label for="product10"><strong>Product 10</strong></label>
                {{Form::text('product10', old('product10'),array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <h4 class="dotted-border">Store Images</h4>
</div>
<div class="row-fluid">
    <div class="span4">
        <div class="control-group {{ ($errors->has('image1') ? 'error' : '') }}">
            <label for="image1" class="control-label"><strong>Store front Image 1</strong></label><br/>
            <div class="controls">
                @if(!empty($shop->image1))
                    <a class="btn btn-danger btn-mini action_remove" href="#" id="1"><i class="fa fa-trash-o"></i>
                        Remove Image</a><br/>
                    <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image1 }}"
                         alt="Image"/>
                @else
                    <img src="" alt="No Image" class="overlay-image"/>
                @endif
                <br/>
                {{ Form::file('image1') }}<br/>
                <span class="help-block">{{ ($errors->has('image1') ? $errors->first('image1') : '') }}</span>
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group {{ ($errors->has('image2') ? 'error' : '') }}">
            <label for="image2"><strong>Image 2</strong>(Only gif, jpg or png files)</label><br/>
            <div class="controls">
                @if(!empty($shop->image2))
                    <a class="btn btn-danger btn-mini action_remove" href="#" id="1"><i class="fa fa-trash-o"></i>
                        Remove Image</a><br/>
                    <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image2 }}"
                         alt="Image"/>
                @else
                    <img src="" alt="No Image" class="overlay-image"/>
                @endif
                <br/>
                {{ Form::file('image2') }}<br/>

                <span class="help-block">{{ ($errors->has('image2') ? $errors->first('image2') : '') }}</span>
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group {{ ($errors->has('image3') ? 'error' : '') }}">
            <label for="image3"><strong>Image 3</strong>(Only gif, jpg or png files)</label><br/>
            <div class="controls">
                @if(!empty($shop->image3))
                    <a class="btn btn-danger btn-mini action_remove" href="#" id="1"><i class="fa fa-trash-o"></i>
                        Remove Image</a><br/>
                    <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image3 }}"
                         alt="Image"/>
                @else
                    <img src="" alt="No Image" class="overlay-image"/>
                @endif
                <br/>
                {{ Form::file('image3') }}<br/>
                <span class="help-block">{{ ($errors->has('image3') ? $errors->first('image3') : '') }}</span>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">

    <div class="span4">
        <div class="control-group {{ ($errors->has('image4') ? 'error' : '') }}">
            <label for="image4"><strong>Image 4</strong>(Only gif, jpg or png files)</label><br/>
            <div class="controls">
                @if(!empty($shop->image4))
                    <a class="btn btn-danger btn-mini action_remove" href="#" id="1"><i class="fa fa-trash-o"></i>
                        Remove Image</a><br/>
                    <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image4 }}"
                         alt="Image"/>
                @else
                    <img src="" alt="No Image" class="overlay-image"/>
                @endif
                <br/>
                {{ Form::file('image4') }}<br/>
                <span class="help-block">{{ ($errors->has('image4') ? $errors->first('image4') : '') }}</span>
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group {{ ($errors->has('image5') ? 'error' : '') }}">
            <label for="image5"><strong>Image 5</strong>(Only gif, jpg or png files)</label><br/>
            <div class="controls">
                @if(!empty($shop->image5))
                    <a class="btn btn-danger btn-mini action_remove" href="#" id="1"><i class="fa fa-trash-o"></i>
                        Remove Image</a><br/>
                    <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $shop->mallID }}/{{ $shop->image5 }}"
                         alt="Image"/>
                @else
                    <img src="" alt="No Image" class="overlay-image"/>
                @endif
                <br/>
                {{ Form::file('image5') }}<br/>
                <span class="help-block">{{ ($errors->has('image5') ? $errors->first('image5') : '') }}</span>
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="clearfix"></div>

    <div class="span4"></div>
</div>
<div class="row-fluid">
    <h4 class="dotted-border">Credit Cards</h4>
    Mastercard&nbsp;{{Form::checkbox('mastercard','Y',array('class'=>'pull-left')) }}&nbsp;&nbsp;
    Visa&nbsp;{{Form::checkbox('visa', 'Y',array('class'=>'pull-left')) }}&nbsp;&nbsp;
    AMEX&nbsp;{{Form::checkbox('amex','Y',array('class'=>'pull-left')) }}&nbsp;&nbsp;
    Diners club&nbsp{{Form::checkbox('diners', 'Y',array('class'=>'pull-left')) }}
</div>
<br/>
<div class="row-fluid">
    <div class="control-group span4">
        <label for="newShop"><strong>Is the shop new ?</strong></label>
        <div class="controls">
            {{Form::radio('newShop','Y') }}&nbsp;Yes &nbsp;&nbsp;{{Form::radio('newShop','N', 'checked') }}No
        </div>
    </div>

    <div class="control-group span4">
        <label for="display"><strong>Must your store be displayed?</strong></label>
        <div class="controls">
            {{Form::radio('display','Y', 'checked') }}&nbsp;Yes &nbsp;&nbsp;{{Form::radio('display','N', 'checked') }}No
        </div>
    </div>
</div>
<div class="row-fluid">
    <h4 class="dotted-border">Owner and Manager Contact Details</h4>
</div>
<div class="row-fluid">
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="ownerName"><strong>First Name</strong>
                <small class="muted"> Owner</small>
            </label>
            <div class="controls">
                {{ Form::text('ownerName',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="ownerSurname"><strong>Surname</strong>
                <small class="muted"> Owner</small>
            </label>
            <div class="controls">
                {{ Form::text('ownerSurname',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="ownerCell"><strong>Cell</strong>
                <small class="muted"> Owner</small>
            </label>
            <div class="controls">
                {{ Form::text('ownerCell',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group {{ ($errors->has('ownerEmail') ? 'error' : '') }}">
            <label class="control-label" for="ownerEmail"><strong>Email</strong>
                <small class="muted"> Owner</small>
            </label>
            <div class="controls">
                {{ Form::text('ownerEmail',null,array('class'=>'txtbar')) }}
            </div>
            <span class="help-block">{{ ($errors->has('ownerEmail') ? $errors->first('ownerEmail') : '') }}</span>
        </div>
    </div>
</div>
<div class="row-fluid">

</div>
<div class="row-fluid">
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="managerName"><strong>First Name</strong>
                <small class="muted"> Marketing Manager</small>
            </label>
            <div class="controls">
                {{ Form::text('managerName',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="managerSurname"><strong>Surname</strong>
                <small class="muted"> Marketing Manager</small>
            </label>
            <div class="controls">
                {{ Form::text('managerSurname',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="managerCell"><strong>Cell</strong>
                <small class="muted"> Marketing Manager</small>
            </label>
            <div class="controls">
                {{ Form::text('managerCell',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group {{ ($errors->has('managerEmail') ? 'error' : '') }}">
            <label class="control-label" for="managerEmail"><strong>Email </strong>
                <small class="muted"> Marketing Manager</small>
            </label>
            <div class="controls">
                {{ Form::text('managerEmail',null,array('class'=>'txtbar')) }}
            </div>
            <div class="help-block">{{ ($errors->has('managerEmail') ? $errors->first('managerEmail') : '') }}</div>
        </div>
    </div>
</div>
<div class="row-fluid">

</div>
<div class="row-fluid">
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="managerName2"><strong>first name</strong>
                <small class="muted">Manager 2</small>
            </label>
            <div class="controls">
                {{ Form::text('managerName2',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="managerSurname2"><strong>Surname</strong>
                <small class="muted"> Manager 2</small>
            </label>
            <div class="controls">
                {{ Form::text('managerSurname2',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="managerCell2"><strong>Cell</strong>
                <small class="muted">Manager 2</small>
            </label>
            <div class="controls">
                {{ Form::text('managerCell2',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group {{ ($errors->has('managerEmail2') ? 'error' : '') }}">
            <label class="control-label" for="managerEmail2"><strong>Email</strong>
                <small class="muted">Manager 2</small>
            </label>
            <div class="controls">
                {{ Form::text('managerEmail2',null,array('class'=>'txtbar')) }}
            </div>
            <span class="help-block">{{ ($errors->has('managerEmail2') ? $errors->first('managerEmail2') : '') }}</span>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="clearfix"></div>

</div>
<div class="row-fluid">
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="managerName3"><strong>First Name</strong>
                <small class="muted">Manager 3</small>
            </label>
            <div class="controls">
                {{ Form::text('managerName3',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="managerSurname3"><strong>Surname</strong>
                <small class="muted">Manager 3</small>
            </label>
            <div class="controls">
                {{ Form::text('managerSurname3',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="managerCell3"><strong>Cell</strong>
                <small class="muted">Manager 3</small>
            </label>
            <div class="controls">
                {{ Form::text('managerCell3',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group {{ ($errors->has('managerEmail3') ? 'error' : '') }}">
            <label class="control-label" for="managerEmail3"><strong>Email</strong>
                <small class="muted">Manager 3</small>
            </label>
            <div class="controls">
                {{ Form::text('managerEmail3',null,array('class'=>'txtbar')) }}
            </div>
            <span class="help-block">{{ ($errors->has('managerEmail3') ? $errors->first('managerEmail3') : '') }}</span>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="headOfficeName"><strong>First Name
                    <small class="muted">Head Office</small>
                </strong></label>
            <div class="controls">
                {{ Form::text('headOfficeName',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="headOfficeSurname"><strong>Surname</strong>
                <small class="muted"> Head Office</small>
            </label>
            <div class="controls">
                {{ Form::text('headOfficeSurname',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="headOfficeCell"><strong>Cell</strong>
                <small class="muted"> Head Office</small>
            </label>
            <div class="controls">
                {{ Form::text('headOfficeCell',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group {{ ($errors->has('headOfficeEmail') ? 'error' : '') }}">
            <label class="control-label" for="headOfficeEmail"><strong>Email</strong>
                <small class="muted"> Head Office</small>
            </label>
            <div class="controls">
                {{ Form::text('headOfficeEmail',null,array('class'=>'txtbar')) }}
            </div>
            <div class="help-block">{{ ($errors->has('headOfficeEmail') ? $errors->first('headOfficeEmail') : '') }}</div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="areaManagerName"><strong>First Name</strong>
                <small class="muted"> Area Manager</small>
            </label>
            <div class="controls">
                {{ Form::text('areaManagerName',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="areaManagerSurname"><strong>Surname</strong>
                <small class="muted"> Area Manager</small>
            </label>
            <div class="controls">
                {{ Form::text('areaManagerSurname',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="areaManagerCell"><strong>Cell</strong>
                <small class="muted"> Area Manager</small>
            </label>
            <div class="controls">
                {{ Form::text('areaManagerCell',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group {{ ($errors->has('areaManagerEmail') ? 'error' : '') }}">
            <label class="control-label" for="areaManagerEmail"><strong>Email</strong>
                <small class="muted"> Area Manager</small>
            </label>
            <div class="controls">
                {{ Form::text('areaManagerEmail',null,array('class'=>'txtbar')) }}
            </div>
            <span class="help-block">{{ ($errors->has('areaManagerEmail') ? $errors->first('areaManagerEmail') : '') }}</span>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="opsManagerName"><strong>First Name</strong>
                <small class="muted"> Ops Manager</small>
            </label>
            <div class="controls">
                {{ Form::text('opsManagerName',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="opsManagerSurname"><strong>Surname</strong>
                <small class="muted"> Ops Manager</small>
            </label>
            <div class="controls">
                {{ Form::text('opsManagerSurname',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="opsManagerCell"><strong>Cell</strong>
                <small class="muted"> Ops Manager</small>
            </label>
            <div class="controls">
                {{ Form::text('opsManagerCell',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group {{ ($errors->has('opsManagerEmail') ? 'error' : '') }}">
            <label class="control-label" for="opsManagerEmail"><strong>Email</strong>
                <small class="muted"> Ops Manager</small>
            </label>
            <div class="controls">
                {{ Form::text('opsManagerEmail',null,array('class'=>'txtbar')) }}
            </div>
            <span class="help-block">{{ ($errors->has('opsManagerEmail') ? $errors->first('opsManagerEmail') : '') }}</span>
        </div>
    </div>

</div>
<div class="row-fluid">
</div>
<div class="row-fluid">
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="emergencyName"><strong>First Name</strong>
                <small class="muted"> Emergency Contact</small>
            </label>
            <div class="controls">
                {{ Form::text('emergencyName',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="emergencySurname"><strong>Surname</strong>
                <small class="muted"> Emergency Contact</small>
            </label>
            <div class="controls">
                {{ Form::text('emergencySurname',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="emergencyCell"><strong>Cell</strong>
                <small class="muted"> Emergency Contact</small>
            </label>
            <div class="controls">
                {{ Form::text('emergencyCell',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group {{ ($errors->has('emergencyEmail') ? 'error' : '') }}">
            <label class="control-label" for="emergencyEmail"><strong>Email</strong>
                <small class="muted"> Emergency Contact</small>
            </label>
            <div class="controls">
                {{ Form::text('emergencyEmail',null,array('class'=>'txtbar')) }}
            </div>
            <span class="help-block">{{ ($errors->has('emergencyEmail') ? $errors->first('emergencyEmail') : '') }}</span>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="emergencyName2"><strong>First Name</strong>
                <small class="muted"> Emergency Contact 2</small>
            </label>
            <div class="controls">
                {{ Form::text('emergencyName2',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="emergencySurname2"><strong>Surname</strong>
                <small class="muted"> Emergency Contact 2</small>
            </label>
            <div class="controls">
                {{ Form::text('emergencySurname2',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label class="control-label" for="emergencyCell2"><strong>Cell</strong>
                <small class="muted"> Emergency Contact 2</small>
            </label>
            <div class="controls">
                {{ Form::text('emergencyCell2',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group {{ ($errors->has('emergencyEmail2') ? 'error' : '') }}">
            <label class="control-label" for="emergencyEmail2"><strong>Email</strong>
                <small class="muted"> Emergency Contact 2</small>
            </label>
            <div class="controls">
                {{ Form::text('emergencyEmail2',null,array('class'=>'txtbar')) }}
            </div>
            <span class="help-block">{{ ($errors->has('emergencyEmail2') ? $errors->first('emergencyEmail2') : '') }}</span>
        </div>
    </div>
</div>









