<div class="row-fluid">
    <div class="span12">
        <strong>Please note : You may only enter an email address not yet linked to a registered user. </strong><br/>
        A verification email containing the user's login details will be sent to the email address entered below, please
        make sure it's an existing, valid address...
    </div>

</div>
<span>

</span>
<div class="row-fluid">
    <div class="span4">
        <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}">
            <label for="email" class="control-label">Email*</label>
            <div class="controls">
                {{ Form::text('email',null,array('class'=>'txtbar')) }}
                <span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
            </div>
        </div>
    </div>
</div>
<br/>
<div class="row-fluid">
    <div class="span4">
        <div class="control-group {{ ($errors->has('password') ? 'error' : '') }}">
            <label for="password" class="control-label">Password*</label>
            <div class="controls">
                {{ Form::password('password',array('class'=>'txtbar', 'id'=>'password')) }}
            </div>
            <div class="help-block"
                 id="password_help"> {{ ($errors->has('password') ?  $errors->first('password') : '') }}</div>
        </div>
        <div class="control-group {{ ($errors->has('password_confirmation') ? 'error' : '') }}">
            <label for="password_confirmation" class="control-label">Confirm Password*</label>
            <div class="controls">
                {{ Form::password('password_confirmation',null,array('class'=>'txtbar')) }}
            </div>
            <span
                class="help-block">{{ ($errors->has('password_confirmation') ? $errors->first('password_confirmation') : '') }}</span>
        </div>
    </div>
    <div class="span4">
        <span>Password needs to have :</span>
        <ul>
            <li>At least one upper case english letter</li>
            <li>At least one lower case english letter</li>
            <li>At least one digit</li>
            <li>At least one of these special characters <span class="label label-info">! @ # $ _</span></li>
            <li>Minimum 6 in length</li>
        </ul>
    </div>
</div>
<br/>
<div class="clearfix"></div>
<div class="row-fluid">
    <div class="span4 control-group {{ ($errors->has('first_name') ? 'error' : '') }}">
        <label for="first_name" class="control-label">First Name *</label>
        <div class="controls">
            {{ Form::text('first_name',null,array('class'=>'txtbar')) }}
            <span class="help-block">{{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}</span>
        </div>
    </div>
    <div class="control-group span4">
        <label for="last_name">Surname</label>
        <div class="controls">
            {{ Form::text('last_name',null,array('class'=>'txtbar')) }}
        </div>
    </div>
</div>
<div class="row-fluid">
    @if(isset($companies))
        <div class="control-group span3">
            <label for="company">Company List</label>
            <div class="controls">
                {{ Form::select('company',$companies) }}
            </div>
        </div>
    @endif
    <div class="control-group  span3">
        <label for="vat_number">Vat Number</label>
        <div class="controls">
            {{ Form::text('vat_number',null,array('class'=>'txtbar span6')) }}
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label for="postal_address">Postal Address</label>
            <div class="controls">
                {{ Form::textarea('postal_address',null,array('class'=>'txtbox','rows'=>'3')) }}
            </div>
        </div>
    </div>
    <div class="control-group  span3">
        <label for="postal_code">Postal Code</label>
        <div class="controls">
            {{ Form::text('postal_code',null,array('class'=>'txtbar')) }}
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="control-group span3">
        <label for="tel_work">Work Tel</label>
        <div class="controls">
            {{ Form::text('tel_work',null,array('class'=>'txtbar')) }}
        </div>
    </div>

    <div class="control-group span3">
        <label for="cell">Cell</label>
        <div class="controls">
            {{ Form::text('cell',null,array('class'=>'txtbar numeric')) }}
        </div>
    </div>
    <div class="control-group span3">
        <label for="fax">Fax</label>
        <div class="controls">
            {{ Form::text('fax',null,array('class'=>'txtbar')) }}
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span3">
        <div class="control-group">
            <label for="province"><strong>Province </strong></label>
            <div class="controls">

                <select name="province_id" id="province" class="txtbar">
                    <option value="">Please Select...</option>
                    @foreach($provinces as $id=>$province)
                        <option value="{{ $id }}">{{ $province }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label for="city"><strong>City</strong></label>
            <div class="controls">
                <select name="city_id" id="city">
                    <option value="">Please Select...</option>
                    @if(isset($cities))
                        @foreach($cities as $key=>$value)
                            <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
    <div class="control-group span3 {{ ($errors->has('birth_date') ? 'error' : '') }}">
        <label for="birth_date" class="control-label"><strong>Date of Birth</strong></label>
        <div class="controls">
            <div class="input-append datetimepicker4">
                {{ Form::text('birth_date',null,array('data-format'=>'yyyy-MM-dd','readonly','class'=>'span9')) }}
                <span class="add-on">
                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                  </i>
                </span>
            </div>
            <span class="help-block">{{ ($errors->has('birth_date') ? $errors->first('birth_date') : '') }}</span>
        </div>
    </div>
    <div class="control-group span3">
        <label for="cell">Gender</label>
        <div class="controls">
            {{ Form::radio('gender','Female') }}Female &nbsp;&nbsp;{{ Form::radio('gender','Male') }}Male
        </div>
    </div>
</div>
