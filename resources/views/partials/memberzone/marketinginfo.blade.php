<div id="filterSection" data-perrow="4" class="row-fluid">
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>
    </div>  <!-- span6 -->
    <div class="pull-left">
        <h4>Want to send an electronic Newsletter to your Shopper/Client Database?</h4>
    </div>
    <div class="clear"></div>
    <hr class="hborder" />

    <div class="row-fluid">
        <div class="span12">
            <p>
                Increase customer loyalty by providing relevant information to your customer database in a cost effective manner.
                E-mail is probably the most cost effective method of advertising in getting your message across directly to your target audience.
                Thus also creating brand awareness and brand loyalty.
                In short, MESSAGE4U will provide a monthly or ad hoc service whereby a personalised Newsletter will be sent out to your customer database.
            </p>
            <p>
                View our standard quotation for more details.
            </p>
            <strong>
                Download:&nbsp;<a href="{{ route('portal.smsterms') }}">STANDARD_MALLS_Newsletter__SMS.pdf (206 Kb)</a>
            </strong>
        </div><!--span12 -->
    </div>
</div>