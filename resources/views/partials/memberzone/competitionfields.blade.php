<input type="hidden" name="thanksMsg" value="Thank you for submitting your details" />
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <h4 class="dotted-border">Form Recipient</h4>
        <div class="control-group {{ ($errors->has('recipient') ? 'error' : '') }}">
            <label for="recipient"><strong>Mandatory: E-mail address of person receiving the competition entries.</strong></label>
            <div class="controls">
                {{ Form::text('recipient',null,array('class'=>'txtbar','id'=>'recipient')) }}
                <span class="help-block">{{ ($errors->has('recipient') ? $errors->first('recipient') : '') }}</span>
            </div>
        </div>
    </div>
    <div class="span4">
        <h4 class="dotted-border">Competition Title</h4>
        <div class="control-group {{ ($errors->has('subject') ? 'error' : '') }}">
            <label for="subject"><strong>Mandatory: Also appears on the subject line of the email.</strong></label>
            <div class="controls">
                {{ Form::text('subject',null,array('class'=>'txtbar','id'=>'subject')) }}
                {{ ($errors->has('subject') ? $errors->first('subject') : '') }}
                <span class="help-inline"></span>
            </div>
        </div>
    </div>
</div>
<div class="clear"></div>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group {{ ($errors->has('description') ? 'error' : '') }}">
            <label for="description"><strong>Competition description &amp; Rules</strong></label>
            <div class="controls">
                {{ Form::textarea('description',null,array('class'=>'txtbox','rows'=>3)) }}
                <span class="help-block">{{ ($errors->has('description') ? $errors->first('description') : '') }}</span>
            </div>
        </div>
    </div>
    <div class="span4">&nbsp;</div>
</div>
<br />
<br />
<div class="clear"></div>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group {{ ($errors->has('startDate') ? 'error' : '') }}">
            <label for="startDate"><strong>Start Date</strong></label>
            <div class="controls">
                <div class="input-append datetimepicker4">
                    {{ Form::text('startDate',null,array('class'=>'span12','id'=>'startDate','data-format'=>'yyyy-MM-dd','readonly')) }}
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                      </i>
                    </span>
                    {{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}
                </div>
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group {{ ($errors->has('endDate') ? 'error' : '') }}">
            <label for="endDate"><strong>End Date</strong></label>
            <div class="controls">
                <div class="input-append datetimepicker4">
                    {{ Form::text('endDate',null,array('class'=>'span12','id'=>'endDate','data-format'=>'yyyy-MM-dd','readonly')) }}
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                      </i>
                    </span>
                    {{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}
                </div>
            </div>
        </div>
    </div>
</div>
<br />
<br />
<div class="clear"></div>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group">
            <label for="activated"><strong>Display the competition</strong></label>
            <div class="controls">
                Yes &nbsp;<input name="activated" type="radio" value="Y" checked="checked"/>
                No &nbsp;<input name="activated" type="radio" value="N"/>
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group {{ ($errors->has('image1') ? 'error' : '') }}">
            <label><strong>Image:</strong> (Optional. Only jpg, jpeg, gif or png files. No bigger than 1 Mb.)</label>
            <div class="controls">
                @if(!empty($form->thumbnail))
                <!--        <a class="btn btn-danger btn-mini action_remove" href="#" id="image1" data-id="imageBig1"><i class="fa fa-trash-o"></i> Remove Image</a><br />-->
                <img src="{{ Config::get('app.url') }}/uploadimages/{{ $form->thumbnail }}" alt="Image" />
                @else
                <img src="" alt="No Image" class="overlay-image"/>
                @endif
                <br />
                <span class="btn btn-file">{{ Form::file('image') }}</span><br />
                {{ ($errors->has('image') ? $errors->first('image') : '') }}
            </div>
        </div>
    </div>
</div>
