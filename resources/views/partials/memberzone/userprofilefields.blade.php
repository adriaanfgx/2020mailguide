<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group {{ ($errors->has('first_name') ? 'error' : '') }}">
            <label for="first_name"><strong>First Name</strong></label>
            <div class="controls">
                {{ Form::text('first_name',null,array('class'=>'txtbar')) }}
                <span class="help-block">{{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}</span>
            </div>
        </div>
        <div class="control-group">
            <label for="last_name"><strong>Surname</strong></label>
            <div class="controls">
                {{ Form::text('last_name',null,array('class'=>'txtbar')) }}
            </div>
        </div>
        <div class="control-group">
            <label for="company"><strong>Company</strong></label>
            <div class="controls">
                {{ Form::text('company',null,array('class'=>'txtbar')) }}
            </div>
        </div>
        <div class="control-group">
            <label for="vat_number"><strong>Vat Number</strong></label>
            <div class="controls">
                {{ Form::text('vat_number',null,array('class'=>'txtbar')) }}
            </div>
        </div>
        <div class="control-group">
            <label for="postal_address"><strong>Postal Address</strong></label>
            <div class="controls">
                {{ Form::textarea('postal_address',null,array('class'=>'txtbox','rows'=>'3')) }}
            </div>
        </div>
        <div class="control-group">
            <label for="postal_code"><strong>Postal Code</strong></label>
            <div class="controls">
                {{ Form::text('postal_code',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
    <div class="span4">&nbsp;</div>
</div>

<span><strong>To specify the province where you live select an existing one from the list below :</strong></span>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group">
            <label for="province"><strong>Province List</strong></label>
            <div class="controls">
                {{ Form::select('province',$provinces,null,array('id'=>'province')) }}
            </div>
        </div>
    </div>
<!--    <div class="span4">-->
<!--        <div class="control-group">-->
<!--            <label for="province"><strong>Province where you live</strong></label>-->
<!--            <div class="controls">-->
<!--                {{ Form::text('province',null,array('class'=>'txtbar','id'=>'province_text')) }}-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
</div>
<br />
<span><strong>To specify the city where you live select an existing one from the list below :</strong></span>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group">
            <label for="city"><strong>City List</strong></label>
            <div class="controls">
                {{ Form::select('city',$cities,null,array('id'=>'city')) }}
            </div>
        </div>
    </div>
<!--    <div class="span4">-->
<!--        <div class="control-group">-->
<!--            <label for="city"><strong>City where you live</strong></label>-->
<!--            <div class="controls">-->
<!--                {{ Form::text('city',null,array('class'=>'txtbar','id'=>'city_text')) }}-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
</div>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group">
            <label for="surbub"><strong>Suburb where you live</strong></label>
            <div class="controls">
                {{ Form::text('suburb',null,array('class'=>'txtbar')) }}
            </div>
        </div>
        <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}">
            <label for="email"><strong>Email*</strong></label>
            <div class="controls">
                {{ Form::text('email',null,array('class'=>'txtbar','readonly')) }}
                <span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
            </div>
        </div>
        <div class="control-group">
            <label for="tel_work"><strong>Work Tel</strong></label>
            <div class="controls">
                {{ Form::text('tel_work',null,array('class'=>'txtbar')) }}
            </div>
        </div>
        <div class="control-group">
            <label for="tel_home"><strong>Home Tel</strong></label>
            <div class="controls">
                {{ Form::text('tel_home',null,array('class'=>'txtbar')) }}
            </div>
        </div>
        <div class="control-group">
            <label for="fax"><strong>Fax</strong></label>
            <div class="controls">
                {{ Form::text('fax',null,array('class'=>'txtbar')) }}
            </div>
        </div>
        <div class="control-group">
            <label for="cell"><strong>Cell</strong></label>
            <div class="controls">
                {{ Form::text('cell',null,array('class'=>'txtbar')) }}
            </div>
        </div>
        <div class="control-group">
            <label for="birth_date"><strong>Date of Birth</strong></label>
            <div class="controls">
                <div class="input-append datetimepicker4">
                    {{ Form::text('birth_date',null,array('class'=>'span12','id'=>'birth_date','data-format'=>'yyyy-MM-dd','readonly')) }}
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                      </i>
                    </span>
                </div>
            </div>
        </div>
        <div class="control-group">
            <label for="gender"><strong>Gender</strong></label>
            <div class="controls">
                {{ Form::radio('gender','Female') }}Female &nbsp;&nbsp;{{ Form::radio('gender','Male') }}Male
            </div>
        </div>
    </div>
    <div class="span4">&nbsp;</div>
</div>