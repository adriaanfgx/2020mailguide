<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <label for="title" class="control-label"><strong>Position*</strong></label>
        <div class="control-group {{ ($errors->has('title') ? 'error' : '') }}">
            {{ Form::text('title',null) }}
            <span class="help-block">{{ ($errors->has('title') ? $errors->first('title') : '') }}</span>
        </div>
    </div>
    <div class="span4">
        <div class="control-group {{ ($errors->has('salary') ? 'error' : '') }}">
            <label for="salary" class="control-label"><strong>Salary</strong></label>
            <div class="controls">
                {{ Form::text('salary',null) }}
                <span class="help-block">{{ ($errors->has('salary') ? $errors->first('salary') : '') }}</span>
            </div>
        </div>

    </div>
</div>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="input-append datetimepicker4">
            <div class="control-group {{ ($errors->has('startDate') ? 'error' : '') }}">
                <label class="control-label" for="startDate"><strong>Starting Date</strong></label>
                <div class="controls">
                    {{ Form::text('startDate',null,array('data-format'=>'yyyy-MM-dd','class'=>'span12','readonly')) }}
                                <span class="add-on">
                                  <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                                  </i>
                                </span>
                    {{ ($errors->has('startDate') ? $errors->first('startDate') : '') }}
                </div>
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label for="refNum"><strong>Ref. No</strong></label>
            <div class="controls">
                {{ Form::text('refNum',null) }}
            </div>
        </div>

    </div>
</div>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="input-append datetimepicker4">
            <div class="control-group {{ ($errors->has('endDate') ? 'error' : '') }}">
                <label class="control-label" for="endDate"><strong>Closing Date*</strong></label>

                <div class="controls">
                    {{ Form::text('endDate',null,array('data-format'=>'yyyy-MM-dd','class'=>'span12','readonly')) }}
            <span class="add-on">
              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
              </i>
            </span>
                    {{ ($errors->has('endDate') ? $errors->first('endDate') : '') }}
                </div>
            </div>
        </div>
    </div>
    <div class="span4">&nbsp;</div>
</div>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group {{ ($errors->has('description') ? 'error' : '') }}">
            <label for="description"><strong>Description*</strong></label>
            <div class="controls">
                {{ Form::textarea('description',null,array('rows'=>3)) }}
                {{ ($errors->has('description') ? $errors->first('description') : '') }}
            </div>
        </div>
    </div>
</div>
<h4 class="dotted-border">Contact Details</h4>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group">
            <label for="contactPerson"><strong>Contact Person</strong></label>
            <div class="controls">
                {{ Form::text('contactPerson',null) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label for="cell"><strong>Cell</strong></label>
            <div class="controls">
                {{ Form::text('cell',null) }}
            </div>
        </div>
    </div>
</div>

<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group">
            <label for="telephone"><strong>Telephone</strong></label>
            <div class="controls">
                {{ Form::text('telephone',null) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label for="fax"><strong>Fax</strong></label>
            <div class="controls">
                {{ Form::text('fax',null) }}
            </div>
        </div>
    </div>
</div>

<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}">
            <label for="email" class="help-block"><strong>Email</strong></label>
            <div class="controls">
                {{ Form::text('email',null) }}
                <span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
            </div>
        </div>
    </div>
    <div class="span4">
        &nbsp;
    </div>
</div>
<div class="row" style="padding-left: 30px;">
    <div class="control-group">
        <label for="showInfo"><strong>Must shop name and contact details for job be displayed?</strong></label>
        <div class="controls">
            Yes&nbsp;{{ Form::radio('showInfo','Y',null) }}&nbsp;&nbsp;No&nbsp;{{ Form::radio('showInfo','N',null) }}
        </div>
    </div>
</div>