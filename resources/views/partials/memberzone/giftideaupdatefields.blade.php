<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group {{ ($errors->has('title') ? 'error' : '') }}">
            <label for="title"><strong>Product Name</strong></label>
            <div class="controls">
                {{ Form::text('title',null,array('class'=>'txtbar')) }}
            </div>
            <span class="help-block">{{ ($errors->has('title') ? $errors->first('title') : '') }}</span>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label for="manufacturer"><strong>Manufacturer</strong></label>
            <div class="controls">
                {{ Form::text('manufacturer',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
</div>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group {{ ($errors->has('category') ? 'error' : '') }}">
            <label for="category"><strong>Category List</strong></label>
            <div class="controls">
                {{ Form::select('category',$categories,null, ['placeholder' => 'Select category...']) }}
            </div>
            <span class="help-block">{{ ($errors->has('category') ? $errors->first('category') : '') }}</span>
        </div>
    </div>
    <div class="span4">&nbsp;</div>
</div>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group">
            <label for="price"><strong>Price</strong></label>
            <div class="controls">
                {{ Form::select('price',$price,null) }}
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group">
            <label for="forWho"><strong>For</strong></label>
            <div class="controls">
                {{ Form::select('forWho',$for,null) }}
            </div>
        </div>
    </div>
</div>
<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <div class="control-group {{ ($errors->has('description') ? 'error' : '') }}">
            <label for="description"><strong>Description</strong></label>
            <div class="controls">
                {{Form::textarea('description',null,array('class'=>'txtbox','rows'=>'3')) }}
                <span class="help-block">{{ ($errors->has('description') ? $errors->first('description') : '') }}</span>
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="control-group {{ ($errors->has('image1') ? 'error' : '') }}">
            <label for="image2"><strong>Image (Optional)</strong></label><br />
            <div class="controls">
                @if(!empty($giftIdea->thumbnail1))
                <img src="{{ Config::get('app.url') }}/uploadimages/{{ $giftIdea->thumbnail1 }}" alt="Image" />
                @else
                <img src="" alt="No Image" class="overlay-image"/>
                @endif
                <br />
                <span class="btn btn-file">{{ Form::file('image1') }}</span><br />
                {{ ($errors->has('image1') ? $errors->first('image1') : '') }}
            </div>
        </div>

    </div>
</div>
<div class="row" style="padding-left: 30px">
    <div class="span4">
        <div class="control-group">
            <label for="display"><strong>Must gift idea be displayed ?</strong></label>
            <div class="controls">
                Yes &nbsp;{{ Form::radio('display','Y') }} &nbsp; No &nbsp;{{ Form::radio('display','N') }}
            </div>
        </div>
    </div>
    <div class="span4">&nbsp;</div>
</div>
