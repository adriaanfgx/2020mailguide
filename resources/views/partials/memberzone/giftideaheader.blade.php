<h4 class="dotted-border">Add a Gift Idea</h4>
Each shop is allowed to upload 5 of their hottest Gift Idea products.
Visitors can browse all the Gift Ideas online, and then visit your shop to purchase these items, thus driving actual sales to your store.
<div class="row-fluid">
    <div class="span6">
        <h4 class="dotted-border">How Does It Work ?</h4>
        <ul>
            <li>Shops need to log in </li>
            <li>Upload your Gift Idea online </li>
            <li>Your Mall's Marketing team or Centre Management are notified of this submission </li>
            <li>The submission is verified and approved by your Mall's Marketing team or Centre Management </li>
            <li>Your promotion is automatically added to Mallguide </li>
            <li>Your drive actual sales and shoppers to your store! </li>
        </ul>
    </div>
    <div class="span6">
        <h4 class="dotted-border">Good To Know.....</h4>
        Visitors to Mallguide can search Gift Ideas by:
        <ul>
            <li>Mall, the full listing </li>
            <li>For (A man, woman, teenager or kid) </li>
            <li>Price range</li>
            <li>Category (Birthday, Anniversary, Wedding, Graduation, New baby, Get Well) </li>
            <li>Submissions are automatically removed every 4 months, to keep this area current </li>
        </ul>
    </div>
</div>


