<div id="filterSection" data-perrow="4" class="row-fluid">
    <div class="span3 pull-right">
        <div class="overlay-wrapper"></div>
    </div>  <!-- span6 -->
    <div class="pull-left">
        <h4>Standard Terms and Conditions for Competitions and Adding Entrants into Databases for further Communications</h4>
    </div>
    <div class="clear"></div>
    <hr class="hborder" />

    <div class="row-fluid">
        <div class="span12">
            <p>
                Mallguide provides you with the functionality of running competitions online for your mall or shop FOR FREE.
                You can also customise your entry form so as to get the exact information you want from your entrants.
                And you can view all your entrants and their info when you login.
            </p>
            <p>
                If you're wanting to add competition entrants to your communications database,
                it's important to include something that TELLS them that this is what you're going to do with their info.
            </p>
            <p>
                We’ve written up some standard terms and conditions that you can copy and paste into your competition description and edit according to your needs.
                By these terms and conditions, entrants into your competition automatically agree to receive further communications from you,
                from which they can choose to be unsubscribed at any time. If you have any questions regarding this, be sure to send us an email with your queries.
            </p>
            <strong>
                Download:&nbsp;<a href="{{ route('portal.compterms') }}">Competition_Standard_Terms_and_Conditions.pdf (23 Kb)</a>
            </strong>
        </div><!--span12 -->
    </div>
</div>