<!--<h4 class="dotted-border">Promotion Details</h4>-->
<div class="clear"></div>
<br/>
        <div class="control-group span6 no-margin">
            <label for="title" class="control-label"><strong>Title*</strong></label>
            <div class="controls">
                {{ Form::text('title',null) }}
            </div>
        </div>
<div class="clear"></div>

<div class="input-append datetimepicker4">
    <div class="control-group {{ ($errors->has('startDate') ? 'error' : '') }}">
        <label class="control-label" for="startDate"><strong>Start Date</strong></label>
        <div class="controls">
            {{ Form::text('startDate',null,array('data-format'=>'yyyy-MM-dd','class'=>'span12','readonly')) }}
            <span class="add-on">
              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
              </i>
            </span>
        </div>

    </div>
</div>
<div class="clear"></div>
<br/>
<div class="input-append datetimepicker4">
    <div class="control-group {{ ($errors->has('endDate') ? 'error' : '') }}">
        <label class="control-label" for="endDate"><strong>End Date</strong></label>

        <div class="controls">
            {{ Form::text('endDate',null,array('data-format'=>'yyyy-MM-dd','class'=>'span12','readonly')) }}
            <span class="add-on">
              <i data-time-icon="icon-time" data-date-icon="icon-calendar">
              </i>
            </span>
        </div>
    </div>
</div>
<br />
<br />
<div class="control-group {{ ($errors->has('promotion') ? 'error' : '') }}">
    <label class="control-label" for="promotion"><strong>Description</strong></label>
    <div class="controls">
        {{ Form::textarea('promotion',null,array('class'=>'txtbox span4','rows'=>'3')) }}
        <span class="help-block">{{ ($errors->has('promotion') ? $errors->first('promotion') : '') }}</span>
    </div>
</div>
<div class="control-group {{ ($errors->has('image1') ? 'error' : '') }}">
    <div class="controls">
        @if(!empty($promotion->thumbnail1))
<!--        <a class="btn btn-danger btn-mini action_remove" href="#" id="image1" data-id="imageBig1"><i class="fa fa-trash-o"></i> Remove Image</a><br />-->
        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $promotion->mallID }}/{{ $promotion->thumbnail1 }}" alt="Image" />
        @else
        <img src="" alt="No Image" class="overlay-image"/>
        @endif
        <br />
        <span class="btn btn-file">{{ Form::file('image1') }}</span><br />
        <span class="help-block">{{ ($errors->has('image1') ? $errors->first('image1') : '') }}</span>
    </div>
</div>
<div class="clear"></div>
<br/>


