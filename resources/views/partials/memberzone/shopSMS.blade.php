

<?php 
$total = 0; 
function changeNum($num)
{
	$number = str_replace(' ','',$num);
	if(substr($number,0,1)== '0')
	{
		$number = '27' . substr($number,1);
	}
	return $number;
}
?>
	@foreach($recipientList as $contact)
	<?php
	$count = 0;
	$numbers = array();
	
	$owners = array();
	$managers = array();
	$head_office = array();
	$financials = array();
	
	if(!empty($contact->ownerCell))
	{
		$count++;
		$owners[] = '&nbsp;&nbsp;' . $contact->ownerCell . ' ('.$contact->ownerName.' '.$contact->ownerSurname.')';
		$numbers[] = changeNum($contact->ownerCell);
	}
	if(!empty($contact->emergencyCell))
	{
		$count++;
		$owners[] = '&nbsp;&nbsp;' . $contact->emergencyCell . ' ('.$contact->emergencyName.' '.$contact->emergencySurname.')';
		$numbers[] = changeNum($contact->emergencyCell);
	}
	if(!empty($contact->emergencyCell2))
	{
		$count++;
		$owners[] = '&nbsp;&nbsp;' . $contact->emergencyCell2 . ' ('.$contact->emergencyName2.' '.$contact->emergencySurname2.')';
		$numbers[] = changeNum($contact->emergencyCell2);
	}
	if(!empty($contact->managerCell))
	{
		$count++;
		$managers[] = '&nbsp;&nbsp;' . $contact->managerCell . ' ('.$contact->managerName.' '.$contact->managerSurname.')';
		$numbers[] = changeNum($contact->managerCell);
	}
	if(!empty($contact->managerCell2))
	{
		$count++;
		$managers[] = '&nbsp;&nbsp;' . $contact->managerCell2 . ' ('.$contact->managerName2.' '.$contact->managerSurname2.')';
		$numbers[] = changeNum($contact->managerCell2);
	}
	if(!empty($contact->managerCell3))
	{
		$count++;
		$managers[] = '&nbsp;&nbsp;' . $contact->managerCell3 . ' ('.$contact->managerName3.' '.$contact->managerSurname3.')';
		$numbers[] = changeNum($contact->managerCell3);
	}
	if(!empty($contact->headOfficeCell))
	{
		$count++;
		$head_office[] = '&nbsp;&nbsp;' . $contact->headOfficeCell . ' ('.$contact->headOfficeName.' '.$contact->headOfficeSurname.')';
		$numbers[] = changeNum($contact->headOfficeCell);
	}
	if(!empty($contact->areaManagerCell))
	{
		$count++;
		$head_office[] = '&nbsp;&nbsp;' . $contact->areaManagerCell.  ' ('.$contact->areaManagerName.' '.$contact->areaManagerSurname.')';
		$numbers[] = changeNum($contact->areaManagerCell);
	}
	if(!empty($contact->opsManagerCell))
	{
		$count++;
		$head_office[] = '&nbsp;&nbsp;' . $contact->opsManagerCell. ' ('.$contact->opsManagerName.' '.$contact->opsManagerSurname.')';
		$numbers[] = changeNum($contact->opsManagerCell);
	}
	if(!empty($contact->financialCell))
	{
		$count++;
		$financials[] = '&nbsp;&nbsp;' . $contact->financialCell.  ' ('.$contact->financialName.' '.$contact->financialSurname.')';
		$numbers[] = changeNum($contact->financialCell);
	}

	$n = implode(',',$numbers);

	$total += $count;
	?>
	<tr>
		<td width="30">
			@if($count)
				<input type="checkbox" name="shop[]" class="chooseShop" data-count="{{ $count }}" value="{{ $contact->shopMGID }}"> 
			@endif
		</td>
		<td width="200">
			<?php
				if($count)
				{
					echo '<a href="/shops/update/'.$contact->shopMGID.'" title="Edit this Shop\'s details">'.$contact->name.'</a>';
				} else {
					echo '<span class="muted">'.$contact->name.'</span>';
				}
			?>
		</td>
	
		<td>
			<input type="hidden" name="numbers_{{ $contact->shopMGID }}" id="numbers_{{ $contact->shopMGID }}" value="{{ $n }}" />
			@if(!empty($owners))
			<div class="btn-group">
				<a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">{{ count($owners) }} Owner/s&nbsp;&nbsp;<span class="caret"></span></a>
				<ul class="dropdown-menu">
					@foreach($owners as $key=>$value)
					<li>{{ $value }}</li>
					@endforeach
				</ul>
			</div>
			@endif

			@if(!empty($managers))
			<div class="btn-group">
				<a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">{{ count($managers) }} Manager/s&nbsp;&nbsp;<span class="caret"></span></a>
				<ul class="dropdown-menu">
					@foreach($managers as $key=>$value)
					<li>{{ $value }}</li>
					@endforeach
				</ul>
			</div>
			@endif

			@if(!empty($head_offices))
			<div class="btn-group">
				<a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">{{ count($head_offices) }} Head Office&nbsp;&nbsp;<span class="caret"></span></a>
				<ul class="dropdown-menu">
					@foreach($head_offices as $key=>$value)
					<li>{{ $value }}</li>
					@endforeach
				</ul>
			</div>
			@endif

			@if(!empty($financials))
			<div class="btn-group">
				<a class="btn btn-small dropdown-toggle" data-toggle="dropdown" href="#">{{ count($financials) }} Financial Officer/s&nbsp;&nbsp;<span class="caret"></span></a>
				<ul class="dropdown-menu">
					@foreach($financials as $key=>$value)
					<li>{{ $value }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			
			
			<div class="pull-right"><span class="badge badge-info" id="count_{{ $contact->shopMGID }}">{{ $count }}</span></div>
		</td>
	</tr>
	
	@endforeach
	



 
  