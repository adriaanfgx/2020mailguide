<div class="row" style="padding-left: 30px;">
    <div class="span4">
        <span>Please make sure <strong>all</strong> the fields are filled in:</span>
        <div id="user_info">
            <div class="control-group {{ ($errors->has('old_pass') ? 'error' : '') }}">
                <label for="old_pass" class="control-label"><strong>Old Password</strong></label>
                <div class="controls">
                    {{ Form::password('old_pass',null,array('class'=>'txtbar')) }}
                    <span class="help-block">{{ ($errors->has('old_pass') ? $errors->first('old_pass') : '') }}</span>
                </div>
            </div>
            <br />
            <strong>Password needs to have :</strong>
            <ul>
                <li>At least one upper case english letter</li>
                <li>At least one lower case english letter</li>
                <li>At least one digit</li>
                <li>At least one special character</li>
                <li>Minimum 6 characters in length</li>
            </ul>
            <div class="control-group {{ ($errors->has('new_pass') ? 'error' : '') }}">
                <label for="new_pass" class="control-label"><strong>New Password (Minimum 6 characters)</strong></label>
                <div class="controls">
                    {{ Form::password('new_pass',null,array('class'=>'txtbar')) }}
                    <span class="help-block">{{ ($errors->has('new_pass') ? $errors->first('new_pass') : '') }}</span>
                </div>
            </div>
            <div class="control-group {{ ($errors->has('new_pass_confirmation') ? 'error' : '') }}">
                <label for="new_pass_confirmation" class="control-label"><strong>Confirm New Password</strong></label>
                <div class="controls">
                    {{ Form::password('new_pass_confirmation',null,array('class'=>'txtbar')) }}
                    <span class="help-block">{{ ($errors->has('new_pass_confirmation') ? $errors->first('new_pass_confirmation') : '') }}</span>
                </div>
            </div>
        </div>
        <button class="submit reg-btn" id="update_user">Submit Details</button>
    </div>
</div>
