<?php
    $activeMall = Session::get('activeMallID');
    $activeChain = Session::get('activeChain');
?>
@if( isset($activeMall) )
<div class="row-fluid">
    <div class="span6">
        <div class="control-group">
            <label for="shopMGID">Select a shop</label>
            <div class="controls">
                {{ Form::select('shopMGID',$mallShops) }}
            </div>
        </div>
    </div>
    <div class="span6"></div>
</div>
@endif
@if( isset($activeChain) )
<div class="row-fluid">
    <div class="span12">
        <div class="control-group">
            <label class="control-label" for="mallID"><strong>Select a mall</strong></label>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
            <a href="#" id="deselect_all"><strong>Deselect All</strong></a>
            <div class="controls">
                {{ Form::select('mallID',$chainStoreMalls,$mallIDs,array('class'=>'span10 multi-mall','multiple','id'=>'multi-malls')) }}
                {{ Form::hidden('malls',null,array('id'=>'malls')) }}
            </div>
        </div>
    </div>
<!--    <div class="span6"></div>-->
</div>
<div class="clearfix"></div>
<br />
@endif
<div class="row-fluid">
    <div class="span6">
        <div class="control-group {{ ($errors->has('title') ? 'error' : '') }}">
            <label for="title"><strong>Product Name</strong></label>
            <div class="controls">
                {{ Form::text('title',null,array('class'=>'txtbar')) }}
            </div>
            <span class="help-block">{{ ($errors->has('title') ? $errors->first('title') : '') }}</span>
        </div>
    </div>
    <div class="span6">
        <div class="control-group">
            <label for="manufacturer"><strong>Manufacturer</strong></label>
            <div class="controls">
                {{ Form::text('manufacturer',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <div class="control-group {{ ($errors->has('category') ? 'error' : '') }}">
            <label for="category"><strong>Category List</strong></label>
            <div class="controls">
                {{ Form::select('category',$categories, null, ['placeholder' => 'Select category...']) }}
            </div>
            <span class="help-block">{{ ($errors->has('category') ? $errors->first('category') : '') }}</span>
        </div>
    </div>
    <div class="span6">
        <div class="control-group">
            <label for="price"><strong>Price</strong></label>
            <div class="controls">
                {{ Form::select('price',$price,null) }}
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <div class="control-group">
            <label for="for"><strong>For</strong></label>
            <div class="controls">
                {{ Form::select('for',$for,null) }}
            </div>
        </div>
    </div>
    <div class="span6">
        <div class="control-group {{ ($errors->has('description') ? 'error' : '') }}">
            <label for="description"><strong>Description</strong></label>
            <div class="controls">
                {{Form::textarea('description',null,array('class'=>'txtbox','rows'=>'3')) }}
            </div>
            <span class="help-block">{{ ($errors->has('description') ? $errors->first('description') : '') }}</span>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span6">
        <div class="control-group {{ ($errors->has('image1') ? 'error' : '') }}">
            <label for="image1"><strong>Image (Optional)</strong></label><br />
            <div class="controls">
                <span class="btn btn-file">{{ Form::file('image1',array('class'=>'images')) }}</span><br />

            </div>
            <span class="help-block">{{ ($errors->has('image1') ? $errors->first('image1') : '') }}</span>
        </div>
    </div>
    <div class="span6"></div>
</div>
