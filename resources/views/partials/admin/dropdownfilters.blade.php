
<div class="span3">
    <label><strong>Province</strong></label>
    <div class="control-group">
        <select name="province_id" id="province">
            <option value="">Please Select...</option>
            @foreach($provinces as $key=>$value)
                <option  value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select>
    </div>
</div>
