@if(isset($bordered) && $bordered == true)
<div class="blog-content border_top">
                <div class="mall-caption">
@endif
<h4 class="hborder">{{ isset($title) ? $title : 'Latest Movie Reviews' }}</h4>

<div id="reviewsCarousel" class="carousel slide opacity-fade">
	<!-- Carousel items -->
	<div class="carousel-inner">
     <?php  $movie_count = 0; ?>
@if(isset($reviews) && !empty($reviews))
		<?php
		$reviews = $reviews->toArray();
		?>
		@foreach($reviews as $idx=>$movie)
			@if($movie['reviews'][0]['display'] == 'Y')
				<div class="item @if($movie_count == 0)active @endif">
					<strong>{{ $movie['name'] }}</strong><br />
					Review by: <strong>{{ $movie['reviews'][0]['reviewer']['name'] }} {{ $movie['reviews'][0]['reviewer']['surname'] }}</strong><br />
					Rating: {{ $movie['reviews'][0]['score'] }} Stars<br /><br />
					{{ Str::limit($movie['reviews'][0]['review'], 100) }}
				</div>
			<?php $movie_count++; ?>
			@endif
		@endforeach
@endif
		{{--<div class="item active">

		</div>--}}
	</div>
	@if($movie_count > 1)
	<br/>
	<br/>
	<!-- Carousel nav -->
	<a href="#reviewsCarousel" data-slide="prev"><i class="fa fa-chevron-circle-left fa-2x"></i></a>
	&nbsp;&nbsp;
	<a href="#reviewsCarousel" data-slide="next"><i class="fa fa-chevron-circle-right fa-2x"></i></a>
	@else
	    <div class="center">
	        There are no movie reviews to display.
	    </div>
    @endif
</div>
@if(isset($bordered) && $bordered == true)
</div>
</div>
@endif

@section('exScript')
<script type="text/javascript">
	$(document).ready(function () {
		$('#reviewsCarousel').carousel({
			interval: 5000
		});
	});
</script>
@stop