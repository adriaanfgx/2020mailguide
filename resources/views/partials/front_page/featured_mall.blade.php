<div class="span12">
    @if( isset($featuredMall) )
        <div class="span2">

            <div id="zoom-gallery-mall">
                <!--
                Width/height ratio of thumbnail and the main image must match to avoid glitches.
                If ratios are different, you may add CSS3 opacity transition to the main image to make the change less noticable.
                 -->
                <?php
                $_malllogo = (!empty($featuredMall->logo)) ? 'uploadimages/mall_'.$featuredMall->mallID .'/'. $featuredMall->logo : 'img/temp_logo.gif';
                ?>
                <a href="{{ Config::get('app.url') }}/{{ $_malllogo }}" title="Mall Logo">
                    <img src="{{ Config::get('app.url') }}/{{ $_malllogo }}" />
                </a>
                @if( !empty($featuredMall->thumbnail1))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail1 }}" title="{{ $featuredMall->name  }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail1 }}" alt="{{ $featuredMall->name  }}" />
                    </a>
                @endif
                @if( !empty($featuredMall->thumbnail2))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail2 }}" title="{{ $featuredMall->name  }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail2 }}" alt="{{ $featuredMall->name  }}" />
                    </a>
                @endif
                @if( !empty($featuredMall->thumbnail3))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail3 }}" title="{{ $featuredMall->name  }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail3 }}" alt="{{ $featuredMall->name  }}" />
                    </a>
                @endif
                @if( !empty($featuredMall->thumbnail4))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail4 }}" title="{{ $featuredMall->name  }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail4 }}" alt="{{ $featuredMall->name  }}" />
                    </a>
                @endif
                @if( !empty($featuredMall->thumbnail5))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail5 }}" title="{{ $featuredMall->name  }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail5 }}" alt="{{ $featuredMall->name  }}" />
                    </a>
                @endif
                @if( !empty($featuredMall->thumbnail6))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail6 }}" title="{{ $featuredMall->name  }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail6 }}" alt="{{ $featuredMall->name  }}" />
                    </a>
                @endif
                @if( !empty($featuredMall->thumbnail7))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail7 }}" title="{{ $featuredMall->name  }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail7 }}" alt="{{ $featuredMall->name }}" />
                    </a>
                @endif
                @if( !empty($featuredMall->thumbnail8))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail8 }}" title="{{ $featuredMall->name }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail8 }}" alt="{{ $featuredMall->name }}" />
                    </a>
                @endif
                @if( !empty($featuredMall->thumbnail9))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail9 }}" title="{{ $featuredMall->name }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail9 }}" alt="{{ $featuredMall->name }}" />
                    </a>
                @endif
                @if( !empty($featuredMall->thumbnail10))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail10 }}" title="{{ $featuredMall->name }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredMall->mallID }}/{{ $featuredMall->thumbnail10 }}" alt="{{ $featuredMall->name }}" />
                    </a>
                @endif
            </div>
        </div>
    @endif
    <div class="span8">
        @if( isset($featuredMall) && sizeof($featuredMall) > 0 )
            <h3>{{ $featuredMall->name }}</h3>
            <strong>{{ $featuredMall->city }}</strong><br />
            {{ strip_tags($featuredMall->mallDescription, '<br>') }}
            <br />
        @else
            <p>&nbsp;</p>
            <span>There is no Featured Mall for today</span>
            <br />
        @endif
    </div>
    <!-- span8 -->
    <div class="span2">
        @if( isset($featuredMall) )
            <h5>
                <a href="malls/view/{{ $featuredMall->mallID }}/{{ slugify($featuredMall->name) }}">VIEW MALL</a>
            </h5>
            <h5>
                <a href="malls/view/{{ $featuredMall->mallID }}/{{ slugify($featuredMall->name) }}#mallShops"> VIEW SHOPS</a>
            </h5>

        @endif
    </div>
</div>
