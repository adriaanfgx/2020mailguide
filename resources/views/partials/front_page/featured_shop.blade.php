<div class="span12">
    @if( isset($featuredShop) && sizeof($featuredShop) > 0 )
        <div class="span2">
            <div id="zoom-gallery-shops">
                <!--
                Width/height ratio of thumbnail and the main image must match to avoid glitches.
                If ratios are different, you may add CSS3 opacity transition to the main image to make the change less noticable.
                 -->
                <?php
                $_image = (!empty($featuredShop->image1)) ? 'uploadimages/mall_'.$featuredShop->mallID .'/'. $featuredShop->image1 : 'img/noshop.png';
                $_imageBig1 = (!empty($featuredShop->imageBig1)) ? 'uploadimages/mall_'.$featuredShop->mallID .'/'. $featuredShop->imageBig1 : 'img/noshop.png';
                ?>
                <a href="{{ Config::get('app.url') }}/{{ $_imageBig1 }}" title="{{ $featuredShop->name }}">
                    <img src="{{ Config::get('app.url') }}/{{ $_image }}" alt="{{ $featuredShop->name }}" />
                </a>

                @if( !empty($featuredShop->image2))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->imageBig2 }}" title="{{ $featuredShop->name }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->image2 }}" alt="{{ $featuredShop->name }}" />
                    </a>
                @endif
                @if( !empty($featuredShop->image3))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->imageBig3 }}" title="{{ $featuredShop->name }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->image3 }}" alt="{{ $featuredShop->name }}" />
                    </a>
                @endif
                @if( !empty($featuredShop->image4))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->imageBig4 }}" title="{{ $featuredShop->name }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->image4 }}" alt="{{ $featuredShop->name }}" />
                    </a>
                @endif
                @if( !empty($featuredShop->image5))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->imageBig5 }}" title="{{ $featuredShop->name }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->image5 }}" alt="{{ $featuredShop->name }}" />
                    </a>
                @endif
                @if( !empty($featuredShop->image6))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->imageBig6 }}" title="{{ $featuredShop->name }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->image6 }}" alt="{{ $featuredShop->name }}" />
                    </a>
                @endif
                @if( !empty($featuredShop->image7))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->imageBig7 }}" title="{{ $featuredShop->name }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->image7 }}" alt="{{ $featuredShop->name }}" />
                    </a>
                @endif
                @if( !empty($featuredShop->image8))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->imageBig8 }}" title="{{ $featuredShop->name }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->image8 }}" alt="{{ $featuredShop->name }}" />
                    </a>
                @endif
                @if( !empty($featuredShop->image9))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->imageBig9 }}" title="{{ $featuredShop->name }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->image9 }}" alt="{{ $featuredShop->name }}" />
                    </a>
                @endif
                @if( !empty($featuredShop->image10))
                    <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->imageBig10 }}" title="{{ $featuredShop->name }}" class="gallery-image">
                        <img src="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredShop->mallID }}/{{ $featuredShop->image10 }}" alt="{{ $featuredShop->name }}" />
                    </a>
                @endif
            </div>

        </div>
    @endif
    <div class="span8">
        @if( isset($featuredShop) && sizeof($featuredShop) > 0 )
            <h3>{{ $featuredShop->name }} | <a href="malls/view/{{ $featuredShop->mall->mallID }}/{{ slugify($featuredShop->mall->name) }}" class="header-link">{{ $featuredShop->mall->name }}</a></h3>
            <strong>{{ $featuredShop->category }}</strong><br />
            {{ strip_tags($featuredShop->description, '<br>') }}
            <br />
        @else
            <br /><br />
            <span>There is no Featured Shop for today</span>
            <br />
        @endif
    </div>
    <!-- span8 -->
    @if( isset($featuredShop) )
        <div class="span2">
            <h5>
                <a href="shops/view/{{ $featuredShop->shopID }}/{{ slugify($featuredShop->mall->name) }}/{{ slugify($featuredShop->name) }}">VIEW SHOP</a>
            </h5>
            @if($featuredShop->telephone !='')
                <h5>
                    <span class="mvie_review">{{ $featuredShop->telephone }}</span>
                </h5>
            @endif
        </div>
    @endif
</div>
