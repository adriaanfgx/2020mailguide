<div class="span12">
    @if( isset($featuredComp) && sizeof($featuredComp) > 0 )
        <div class="span2">
            <?php
            if(isset($featuredComp->image)){ ?>
            <div class="feature-img" id="zoom-gallery-comps">
                <a href="{{ Config::get('app.url') }}/uploadimages/{{ $featuredComp->image }}" title="{{ $featuredComp->subject }}">
                    <img src="{{ Config::get('app.url') }}/uploadimages/{{ $featuredComp->thumbnail }}" alt="{{ $featuredComp->subject }}"/>
                </a>
            </div>
            <?php }	?>
        </div>
    @endif
    <div class="span8">
        @if( isset($featuredComp) && sizeof($featuredComp) > 0 )
            <h3>{{ $featuredComp->subject }} @if(isset($featuredComp->mall[0]))<br /><a href="malls/view/{{ $featuredComp->mall[0]->mallID }}/{{ slugify($featuredComp->mall[0]->name) }}" class="header-link">{{ $featuredComp->mall[0]->name }}</a> @endif</h3>
            <p>( {{ date_format(date_create($featuredComp->startDate),'j F Y') }} - {{ date_format(date_create($featuredComp->endDate),'j F Y') }} )</p>
            {{ str_limit(strip_tags($featuredComp->description, '<br>'), $limit = 300) }}<br /><a href="competitions/show/{{ $featuredComp->formID }}">Read more ...</a>
            <br />
        @else
            <br /><br />
            <span>There is no Featured Competition for today</span>
            <br /><br /><br />
        @endif
    </div>
    <!-- span8 -->
    <div class="span2">
        @if( isset($featuredComp) )
            <h5>
                <a href="competitions/show/{{ $featuredComp->formID }}">VIEW COMPETITION</a>
            </h5>
            <h5>
                <a href="competitions">VIEW ALL</a>
            </h5>
        @endif
    </div>
</div>
