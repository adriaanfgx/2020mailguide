<div class="span12">
    <div class="span2">
        @if( isset($featuredEvent) && sizeof($featuredEvent) > 0 )
            <?php
            $event_img=(!empty($featuredEvent->thumbnail1)) ? '/uploadimages/mall_'.$featuredEvent->mallID.'/'.$featuredEvent->thumbnail1 : 'img/temp_logo.gif';
            ?>
            <div class="feature-img" id="zoom-gallery-events">
                <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredEvent->mallID }}/{{ $featuredEvent->image1 }}" title="{{ $featuredEvent->name }}">
                    <img src="{{ Config::get('app.url') }}/{{ $event_img }}" alt="{{ $featuredEvent->name }}"/>
                </a>
            </div>
        @endif
    </div>
    <div class="span8">
        @if( isset($featuredEvent) && sizeof($featuredEvent) > 0 )

            <h3>{{ $featuredEvent->name }} &nbsp;| &nbsp; <a href="malls/view/{{ $featuredEvent->mall->mallID }}/{{ slugify($featuredEvent->mall->name) }}" class="header-link">{{ $featuredEvent->mall->name }}</a></h3>
            <p>( {{ date_format(date_create($featuredEvent->startDate),'j F Y') }} - {{ date_format(date_create($featuredEvent->endDate),'j F Y') }} )</p>
            {{ str_limit(strip_tags($featuredEvent->event, '<br>') , $limit = 300) }} <br><a href="events/event/{{ $featuredEvent->eventsMGID }}/{{ slugify($featuredEvent->name) }}">Read more ...</a>
        @else
            <p>&nbsp;</p>
            <span>There are no featured events for today</span>
            <p>&nbsp;</p>
        @endif
    </div>
    <!-- span8 -->
    <div class="span2">
        @if( isset($featuredEvent) )
            <h5>
                <a href="events/event/{{ $featuredEvent->eventsMGID }}/{{ slugify($featuredEvent->name) }}">VIEW EVENT</a>
            </h5>
            <h5>
                <a href="events">VIEW ALL</a>
            </h5>
        @endif
    </div>
</div>
