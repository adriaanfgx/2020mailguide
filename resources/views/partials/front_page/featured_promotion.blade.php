<div class="span12">
    <div class="span2">
        @if( isset($featuredPromotion) && sizeof($featuredPromotion) > 0 )
            <?php

            $promo_img=(!empty($featuredPromotion->thumbnail1)) ? '/uploadimages/mall_'.$featuredPromotion->mallID.'/'.$featuredPromotion->thumbnail1 : 'img/temp_logo.gif';
            ?>
            <div class="feature-img" id="zoom-gallery-promos">
                <a href="{{ Config::get('app.url') }}/uploadimages/mall_{{ $featuredPromotion->mallID }}/{{ $featuredPromotion->image1 }}" title="{{ $featuredPromotion->shop->name }} Promotion">
                    <img src="{{ Config::get('app.url') }}/{{ $promo_img }}" alt="{{ $featuredPromotion->shop->name }}"/>
                </a>
            </div>
        @endif
    </div>
    <div class="span8">
        @if( isset($featuredPromotion) && sizeof($featuredPromotion) > 0 )
            <p>&nbsp;</p>
            <h3>{{ $featuredPromotion->shop->name }} &nbsp;| &nbsp; <a href="malls/view/{{ $featuredPromotion->shop->mall->mallID }}/{{ slugify($featuredPromotion->shop->mall->name) }}" class="header-link">{{ $featuredPromotion->shop->mall->name }}</a></h3>
            <p>( {{ date_format(date_create($featuredPromotion->startDate),'j F Y') }} - {{ date_format(date_create($featuredPromotion->endDate),'j F Y') }} )</p>
            {{ str_limit(strip_tags($featuredPromotion->promotion, '<br>'), $limit = 300) }} <br><a href="promotions/view/{{ $featuredPromotion->promotionsMGID }}">Read more ...</a>
            <br>
        @else
            <p>&nbsp;</p>
            <span>There is no Featured Promotion for today</span>
            <p>&nbsp;</p>
        @endif
    </div>
    <!-- span8 -->
    @if( isset($featuredPromotion) )
        <div class="span2">
            <h5>
                <a href="promotions/view/{{ $featuredPromotion->promotionsMGID }}">VIEW PROMOTION</a>
            </h5>
            <h5>
                <a href="promotions/list">VIEW ALL</a>
            </h5>
        </div>
    @endif
</div>
