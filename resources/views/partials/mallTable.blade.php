@if(sizeof($malls)>0)
    @foreach($malls as $mall)
        <?php

        //$name = urlencode($mall->name);
        ?>
        <tr>
            <td>
                @if(!empty($mall->logo))
                    <?php $logo = 'uploadimages/mall_' . $mall->mallID . '/' . $mall->logo ?>
                @else
                    <?php $logo = 'img/nomalllogo.gif'; ?>
                @endif
                <img class="img-responsive" width="100" src="{{ Config::get('app.url') }}/{{ $logo }}" alt="{{ $mall->name }}" />
            </td>
            <td>
                <a href="/malls/view/{{ $mall->mallID }}/{{ slugify($mall->name) }}">{{ $mall->name }}</a><br/>
                @if($mall->mallCity)
                    <span class="hidden-desktop">{{ $mall->mallCity->name }}</span>
                @endif
            </td>
            <td class="hidden-phone">
                @if($mall->mallCity)
                    <a href="/malls?city=<?php echo $mall->mallCity->id; ?>-<?php echo slugify($mall->mallCity->name); ?>" class="dark-link">
                        {{ $mall->mallCity->name }}
                    </a>
                @endif
            </td>

            <td>
                @if(!empty($mall->centreManagerTelephone))
                    <?php
                    $tel = str_replace('+27','0', $mall->centreManagerTelephone );
                    $tel = str_replace('0 ','0', $tel );
                    $tel2 = str_replace(' ','', $tel );
                    $tel2 = str_replace('(','', $tel2 );
                    $tel2 = str_replace(')','', $tel2 );
                    ?>

                    <i class="fa fa-phone-square fa-lg hidden-phone"></i>&nbsp;
                    <a class="tel hidden-phone" href="tel:{{ $tel2 }}">{{ $tel }}</a>
                    <a class="btn btn-inverse btn-small hidden-desktop" href="tel:{{ $tel2 }}">Call</a>
                @endif
            </td>
            <td>
                <a href="http://maps.google.com/?ie=UTF&hq=&ll={{ $mall->mapX }},{{ $mall->mapY }}&z=13" target="_blank">
                    <i class="fa fa-map-marker fa-lg fa-fw"></i>
                </a>
            </td>
            <?php

            $count = $mall->shops->count();
            ?>
            <td style="overflow:hidden;" class="hidden-phone">{{ ($count == 0) ? '0' : $count }}</td>
            <td>
                @if($count != 0)
                    <a href="/malls/view/{{ $mall->mallID }}/{{ slugify($mall->name) }}#mallShops" class="btn btn-mini btn-info">View Shops</a>
                @endif
            </td>
        </tr>

    @endforeach
    <tr id="paginator">
        <td class="legend " colspan="7">
            <div class="pagination nomargin"><?php echo $malls->appends(array('alpha' => request()->get('alpha'),'city' => request()->get('city')))->links(); ?></div>
        </td>
    </tr>
@else
    <tr>
        <td colspan="7">
            Sorry, there are currently no Malls listed in that Province...
        </td>
    </tr>
@endif
