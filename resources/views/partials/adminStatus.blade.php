<div class="adminStatus text-right">
	
	    <div class="btn-group">
			<a class="btn  btn-small dropdown-toggle" data-toggle="dropdown" href="#">
				<img class="flag" src="/img/flags/{{ strtolower(Session::get('default_country_code')) }}.png" />
				<span class="caret"></span>
			</a>
			<ul class="dropdown-menu">
				<li class="disabled"><a href="#">Set your location</a></li>
				<li class="divider"></li>
				@foreach($countries as $country)
				<li @if($country->id == Session::get('default_country_id')) class="disabled" @endif><a href="/change-country/{{ $country->id }}">{{ $country->name }}</a></li>
				@endforeach
			</ul>
        </div>
		
	
	@if(Sentry::check() )
	
		<a class="btn btn-inverse btn-small" href="{{ route('user.logout') }}"><span class="fa fa-sign-out"></span> Log-out</a>
		<?php
		$u = Sentry::getUser();

		?>
		@if($u->hasAccess('admin'))
			<a class="btn btn-action btn-small" href="{{ route('admin.index') }}"><span class="fa fa-home"></span> Admin Section</a>
		@elseif( Session::has('back') && Session::get('back')==='yes')
			<a class="btn btn-action btn-small" href="{{ route('portal.index') }}"><span class="fa fa-home"></span> Control Panel</a>
			<a class="btn btn-warning btn-small" href="{{ route('home.index') }}"><span class="fa fa-toggle-on"></span> Switch to Mallguide</a>
		@else
			<a class="btn btn-warning btn-small" href="{{ route('portal.index') }}"><span class="fa fa-toggle-on"></span> Switch to Member Zone</a>
		@endif
	
	@else
		<a class="btn btn-info btn-small" href="{{ route('user.login') }}"><span class="fa fa-sign-in"></span> Log-in</a>
		<a class="btn btn-default btn-small" href="{{ route('register') }}"><span class="fa fa-sign-out"></span> Sign Up</a>
	@endif

	
</div>