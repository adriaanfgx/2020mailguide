<div class="footer-meta">
	<div class="container">
		<div class="row">
			<div class="span4 biggerLeftPadding">
				<h6>About Us</h6>
				<p><strong>Mallguide is a directory listing for Malls;</strong></p>
				<p>What you see here today is really the amalgamation of years of loving work and ideas, developed with all possible users in mind. Mallguide will service malls, shops, leasing agents and even independent movie houses who until now have not been able to provide their customers with a free resource to all relevant information. </p>
				<address>
					<strong>For Mall or Store contact details please search in Mallguide’s Directory.</strong> <br><br>
					Developed by <a href="https://fgx.co.za/" target="_blank" title="Web &amp; Development Services"></a>FGX Studios. <!-- <abbr title="Phone">Tel:</abbr> (011) 051-2800 -->
				</address>
			</div><!--/span 3-->
			
			<div class="span4">
				
			</div><!--/span 3-->
			<div class="span3">
				<h6>Useful Links</h6>
				<ul class="footer-list">


					<li><a href="http://www.islandlightholidays.co.za/" target="_blank" title="hotels in mauritius" >Hotels in Mauritius</a></li>
					<li><a href="http://mallguide.co.za/finance" target="_blank" title="Mallguide Finance">Mallguide Finance</a></li>
					<li><a href="http://www.africacleaning.co.za/" target="_blank" title="business cleaning services">Business cleaning services</a></li>
					<li><a href="http://fgx.co.za/services_seo.htm" target="_blank" title="SEO services">SEO Services</a></li>

				</ul>
			</div><!--/span 3-->
		</div><!--/row-->
	</div><!--/container-->
</div><!--/footer meta-->

<div class="footer">
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="span6">
				<p class="copyright">Copyright {{ date('Y') }} - Mallguide - All Rights Reserved</p>
			</div><!--/span 6-->
			<div class="span6 social-icons">
				<div class="fontawesome-icon circle-social"><a href="https://www.facebook.com/mallguide" target="_blank"><i class="fa fa-facebook"></i></a></div>
				<div class="fontawesome-icon circle-social"><a href="https://twitter.com/mallguide" target="_blank"><i class="fa fa-twitter"></i></a></div>

			</div><!--/span 6-->
		</div><!--/row-->
	</div><!--/container-->
</div><!--/double dark-->


