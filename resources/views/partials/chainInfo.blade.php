<h3 class="dotted-border"> Shops - Apply as a chain member </h3>
<div class="span5">
    <h4 class="dotted-border">Have more than 2 shops ? </h4>
    We've made it easier to update all your Shops' info by issuing a single Username and Password to control your Shop Chain.<br />

    You need to submit your Chain Shop details and create an unique Username and Password.<br />
</div>
<div class="span6">
    <h4 class="dotted-border">After your submission</h4>
    Unfortunately Mallguide can't display any updated Shop information until your Head office has confirmed your identity via email <a href="mailto:chainshop@mallguide.co.za">chainshop@mallguide.co.za</a> or fax (011 883 2304).<br />
    After this you will be able to upload Promotions, Gift ideas and update any Shop in the Chain. It's as easy as that...<br />
</div>

<div class="span5">
    <h4 class="dotted-border">Want a slice of the action ?</h4>
    Just tell us which Chain you would like to update and complete the registration form.
    Once your Head Office has confirmed your details, your profile will be activated and you will receive a confirmation email.
</div>
<div class="span6">
    <h4 class="dotted-border">Shop Chain Details</h4>
    <span><strong>Please enter the name of your shop chain</strong></span>
    <br />
    {{ Form::text('shop_name',null,array('id'=>'chainName')) }}
    <br />
    <br />
    <div class="span12">
        <a class="btn btn-info rightMargin" href="#" type="button" id="search">
        Search
        </a>
        If your shop chain is not listed, please <a href="{{ route('shops.createfrontend') }}">Register 2 or more shops</a>
    </div>
</div>