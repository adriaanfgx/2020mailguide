<div class="row-fluid">
    <div class="span5">
        <div class="control-group {{ ($errors->has('email') ? 'error' : '') }}">
            <label for="email" class="control-label"><strong>Email*</strong></label>
            <div class="controls">
                {{ Form::text('email',null,array('class'=>'txtbar')) }}
                <span class="help-block">{{ ($errors->has('email') ? $errors->first('email') : '') }}</span>
            </div>
        </div>
    </div>
    <div class="span5">
        <br />
        This will be used when you login.</a>
    </div>
</div>
<div class="row-fluid">
    <div class="span12">
        <div class="biggerLeftMargin"><strong>Password needs to have :</strong></div>
        <ul>
            <li>At least one <b>upper case english letter</b>,<b> lower case english letter</b>, <b>digit</b>, <b>special character </b><span class="label label-info">! @ # $ % ^ - _</span></li>
            <li>Minimum <b>6 characters</b> in length</li>
        </ul>
    </div>
</div>
<div class="row-fluid">
    <div class="span5">
        <div class="control-group {{ $errors->has('password') ? 'error' : '' }}">
            <label class="control-label" for="password"><strong>Password</strong></label>
            <div class="controls">
                {{ Form::password('password',array('class'=>'input-medium','id'=>'password')) }}
            </div>
            <div class="help-block" id="password_help"> {{ ($errors->has('password') ?  $errors->first('password') : '') }}</div>
        </div>
    </div>
    <div class="span5">
        <div class="control-group {{ ($errors->has('password') ? 'error' : '') }}">
            <label for="password_confirmation" class="control-label"><strong>Confirm Password*</strong></label>
            <div class="controls">
                {{ Form::password('password_confirmation',null,array('class'=>'txtbar')) }}
            </div>
            <span class="help-block">{{ ($errors->has('password_confirmation') ? $errors->first('password_confirmation') : '') }}</span>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span5">
        <div class="control-group {{ ($errors->has('first_name') ? 'error' : '') }}">
            <label for="first_name" class="control-label"><strong>First Name *</strong></label>
            <div class="controls">
                {{ Form::text('first_name',null,array('class'=>'txtbar')) }}
                <span class="help-block">{{ ($errors->has('first_name') ? $errors->first('first_name') : '') }}</span>
            </div>
        </div>
    </div>
    <div class="span5">
        <div class="control-group">
            <label for="last_name"><strong>Last Name</strong></label>
            <div class="controls">
                {{ Form::text('last_name',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
</div>
<div class="row-fluid">
    <div class="span5">
        <div class="control-group">
            <label for="company"><strong>Company</strong></label>
            <div class="controls">
                {{ Form::text('company',null) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group">
            <label for="vat_number"><strong>Vat Number</strong></label>
            <div class="controls">
                {{ Form::text('vat_number',null,array('class'=>'txtbar')) }}
            </div>
        </div>
    </div>
</div>

<div class="row-fluid">
    <div class="span5">
        <div class="control-group">
            <label for="cell"><strong>Cell</strong></label>
            <div class="controls">
                {{ Form::text('cell',null,array('class'=>'txtbar numeric')) }}
            </div>
        </div>
    </div>
    <div class="span3">
        <div class="control-group {{ ($errors->has('birth_date') ? 'error' : '') }}">
            <label for="birth_date" class="control-label"><strong>Date of Birth</strong></label>
            <div class="controls">
                <div class="input-append datetimepicker4">
                    {{ Form::text('birth_date',null,array('data-format'=>'yyyy-MM-dd','readonly')) }}
                    <span class="add-on">
                      <i data-time-icon="icon-time" data-date-icon="icon-calendar">
                      </i>
                    </span>
                </div>
                <div class="help-block">{{ ($errors->has('birth_date') ? $errors->first('birth_date') : '') }}</div>
            </div>
            <span class="help-block">{{ ($errors->has('birth_date') ? $errors->first('birth_date') : '') }}</span>
        </div>
    </div>
    <div class="span3">
        <div class="control-group {{ ($errors->has('gender') ? 'error' : '') }}">
        <label for="cell"><strong>Gender</strong></label>
            <div class="controls">
                {{ Form::radio('gender','Female') }}Female &nbsp;&nbsp;{{ Form::radio('gender','Male') }}Male
                <span class="help-block">{{ ($errors->has('gender') ? $errors->first('gender') : '') }}</span>
            </div>
        </div>
    </div>
</div>

