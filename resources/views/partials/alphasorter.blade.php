<div class="span9 offset2 text-center white-bg alpha-sorter" @if(isset($show) && $show == false) style="display: none;"@endif>
    <?php echo (!request()->has('alpha')) ? '<span class="current">' : '<a href="?alpha=">';?>ALL<?php echo (!request()->has('alpha')) ? '</span>' : '</a>';?>&nbsp;
    &nbsp;|&nbsp;
    <?php
    $arr_alpha = range('a', 'z');
    foreach($arr_alpha as $idx=>$alpha){
    $alpha = strtoupper($alpha);
    ?>
    <?php echo (request()->has('alpha') && request()->get('alpha') == $alpha) ? '<span class="current">' : '<a href="?alpha='.$alpha.'">';?><?php echo $alpha; ?><?php echo (request()->has('alpha') && request()->get('alpha') == $alpha) ? '</span>' : '</a>';?>&nbsp;
    <?php
    if($idx < 25) echo '&nbsp;|&nbsp;';
    }
    ?>
</div>
