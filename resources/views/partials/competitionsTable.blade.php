
@if(count($competitions))
	@foreach($competitions as $competition)
	<tr>
	
		<td>{{ $competition['subject'] }}</td>
		<td>{{ date('F j Y', strtotime($competition['startDate'])) }}</td>
		<td>{{ date('F j Y', strtotime($competition['endDate'])) }}</td>
		<td>
			<a href="/competitions/edit/{{ $competition['formID'] }}" class="btn btn-mini btn-info"><i class="fa fa-pencil"></i></a>
			<a href="/competitions/delete/{{ $competition['formID'] }}" class="btn btn-mini btn-danger"><i class="fa fa-trash-o"></i> </a>
		</td>
	</tr>
	@endforeach
@else
	<tr>
		<td colspan="5">There are no Competitions listed for that Mall!</td>
	</tr>
@endif  