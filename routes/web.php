<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/', 'FrontendController@index')->name('home.index');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/countries', 'FrontendController@countryDropdown');
Route::get('/change-country/{id}', 'FrontendController@changeCountry');
Route::get('/provinces/{countryId?}', 'FrontendController@getCountryProvinces');
Route::get('/cities/{provinceId?}', 'FrontendController@getProvinceCities');
Route::get('/mall-dropdown-list/{provinceId?}', 'FrontendController@getProvinceMallDropdown');

Route::get('/events/province/{id}', 'FrontendController@getProvinceEvents');
Route::get('/events/province/{id}', 'FrontendController@getProvinceEvents');
Route::get('/events/mall/{id}', 'Frontend\\EventsController@getMallEvents');


Route::get('/exhibitions/province/{id}', 'FrontendController@getExhibitionsEvents');
Route::get('/jobs/province/{id}', 'FrontendController@getProvinceJobs');
Route::get('/jobs/mall/{id}', 'FrontendController@getMallJobs');
Route::get('/jobs/shop/{shopId}', 'FrontendController@getShopJobs');
Route::get('/jobs/{jobId}/applicants', 'FrontendController@getJobApplicants');

Route::get('/malls/exhibitions/{mallId}', 'FrontendController@getMallExhibitions');
Route::get('/malls/province/{id}', 'FrontendController@getProvinceMalls');
Route::get('/malls/autocomplete', 'FrontendController@getAutocompleteMalls');

Route::get('/maps/province/{id}', 'FrontendController@getProvinceMaps');
Route::get('/maps/mall/{id}', 'FrontendController@getMallMaps');

Route::get('/mall/competitions/{mallId}', 'FrontendController@getMallCompetitions');
Route::get('/competition/responses/{competitionId}', 'CompetitionsController@getResponses');
Route::get('/competition/response/answers/{responseId}', 'CompetitionsController@getResponseAnswers');
Route::get('/competitions/', 'Frontend\\CompetitionsController@getIndex')->name('competitions.index');
Route::get('/competitions/frontTable/{id}', 'Frontend\\CompetitionsController@getFrontTable')->name('competitions.table');
Route::get('/competitions/show/{id}', 'Frontend\\CompetitionsController@getShow')->name('competitions.view');

Route::get('/malls-with-promotions/{provinceId}', 'FrontendController@getPromotionMalls');

//Frontend routes
Route::get('/malls', 'Frontend\\MallsController@index')->name('malls.index');
Route::post('/malls/quickfind/{param?}', array('as' => 'malls.quickfind', 'uses' => 'Frontend\\MallsController@getMallByKeyWord'));
Route::get('/malls/quickfind/{query?}/{page?}', array('as' => 'malls.quickfind-paginate', 'uses' => 'Frontend\\MallsController@getMallByKeyWord'));
Route::get('/malls/province-malls/{province?}', array('as' => 'malls.province-malls', 'uses' => 'Frontend\\MallsController@getProvinceMalls'));
Route::get('/malls/view/{id}/{name}', array('as' => 'malls.view_mall', 'uses' => 'Frontend\\MallsController@getView'));
Route::get('/malls/provinceID/{provinceID}', array('as' => 'malls.provinceid', 'uses' => 'Frontend\\MallsController@getByProvinceID'));
Route::get('/malls/filter/{province}','Frontend\\MallsController@getFilter')->name('malls.provinceFilter');
Route::get('/malls/compfilter/{province}','Frontend\\MallsController@getCompFilter')->name('malls.compFilter');
Route::get('/malls/eventfilter/{province}', 'Frontend\\MallsController@getEventFilter')->name('malls.eventFilter');
Route::post('malls/update/{id}', array('as' => 'malls.postUpdate', 'uses' => 'Frontend\\MallsController@postUpdate'));

//Route::get('/shops', 'Frontend\\ShopController@getShops')->name('shops.index');
Route::post('/shops/quickfind/{param?}', array('as' => 'shops.quickfind', 'uses' => 'Frontend\\ShopController@getShopByKeyWord'));
Route::get('/shops/quickfind/{query?}/{page?}', array('as' => 'shops.quickfind-paginate', 'uses' => 'Frontend\\ShopController@getShopByKeyWord'));
Route::get('/shops/view/{id}/{mallname?}/{name?}', 'Frontend\\ShopController@getViewShop')->name('shops.view_shop');
Route::get('/shops/map/{shopID}', array('as' => 'shop.mapImage', 'uses' => 'Frontend\\ShopController@getShopMap'));

Route::get('/shops/', array('as' => 'shops.index', 'uses' => 'Frontend\\ShopController@getIndex'));
Route::get('/shops/filter/{mallID}', array('as' => 'shops.provinceFilter', 'uses' => 'Frontend\\ShopController@getFilter'));
Route::get('/shops/mallshops/{mallID}/{categoryID?}', array('as' => 'shops.mallshops', 'uses' => 'Frontend\\ShopController@getMallShops'));
Route::get('/shops/search/{keyword}', array('as' => 'shops.keywordsearch', 'uses' => 'Frontend\\ShopController@getKeyWordSearch'));
Route::get('/shops/searchkey/{keyword}', array('as' => 'shops.searchkey', 'uses' => 'Frontend\\ShopController@getSiteSearch'));
Route::get('/shops/filterbymall/{mallID}', array('as' => 'shops.filterbymall', 'uses' => 'Frontend\\ShopController@getFilterShopsByMall'));
Route::get('/shops/map/{shopID}', array('as' => 'shop.mapImage', 'uses' => 'Frontend\\ShopController@getShopMap'));
Route::get('/shops/{provinceID}/{mallID}', array('as' => 'shops.provinceMallshops', 'uses' => 'Frontend\\ShopController@getIndex'))->where(['provinceID' => '[0-9]+']);
Route::get('/shops/{provinceID}/{mallID}/{categoryID}', array('as' => 'shops.provinceMallshops', 'uses' => 'Frontend\\ShopController@getIndex'))->where(['provinceID' => '[0-9]+']);;
Route::post('shops/edit/{id}', array('as' => 'shops.postEdit', 'uses' => 'Frontend\\ShopController@postEdit'));

Route::get('/shops/list/{mallID?}', 'Frontend\\ShopController@getShopList')->name('shops.list');
Route::get('/shops/export/{mallID}', array('as' => 'shops.export', 'uses' => 'Frontend\\ShopController@getExportMallShops'));

Route::get('/movies', 'Frontend\\MovieController@getIndex')->name('movies.index');
Route::get('/movies/moreinfo/{id}/{name?}/{extra?}', array('as' => 'movies.moreinfo', 'uses' => 'Frontend\\MovieController@getMoreInfo'));
Route::get('/movies/cinema', array('as' => 'movies.cinema', 'uses' => 'Frontend\\MovieController@getCinemaInfo'));
Route::get('/movies/province/{id}', array('as' => 'movies.province', 'uses' => 'Frontend\\MovieController@getByProvince'));
Route::post('/movies/rate', array('as' => 'movies.postReview', 'uses' => 'Frontend\\MovieController@postReview'));
Route::get('/movies/mallmovies/{mallID}', array('as' => 'movies.mallmovies', 'uses' => 'Frontend\\MovieController@getMallMovies'));
Route::post('/movies/mallmovies/{mallID}', array('as' => 'movies.mallmovies', 'uses' => 'Frontend\\MovieController@getMallMovies'));

Route::get('/jobs/list', 'Frontend\\JobController@getList')->name('jobs.list');
Route::get('/jobs/view/{id}', 'Frontend\\JobController@getShow')->name('job.view');
Route::get('/jobs/search/{keyword}', 'Frontend\\JobController@getKeyWordSearch')->name('jobs.search');
Route::get('/jobs/index/{mallID?}', array('as' => 'jobs.index', 'uses' => 'Frontend\\JobController@getIndex'));
Route::get('/jobs/portalCreate/{mallID?}', array('as' => 'jobs.create', 'uses' => 'Frontend\\JobController@getCreate'));

//Portal Jobs Routes
Route::get('/jobs/mall/{mallID}', array('as' => 'jobs.mall-index', 'uses' => 'Frontend\\JobController@getMallIndex'));
Route::post('/jobs/delete/{id}', array('as' => 'jobs.delete', 'uses' => 'Frontend\\JobController@postDelete'));
Route::post('/jobs/create', array('as' => 'jobs.postCreate', 'uses' => 'Frontend\\JobController@postCreate'));
Route::get('/jobs/applicants/{id}', array('as' => 'jobs.applicants', 'uses' => 'Frontend\\JobController@getApplicants'));
Route::get('/jobs/applicant/{id}', array('as' => 'applicant.view', 'uses' => 'Frontend\\JobController@getApplicant'));
Route::get('/jobs/edit/{id}', array('as' => 'jobs.edit', 'uses' => 'Frontend\\JobController@getEdit'));
Route::post('/jobs/edit/{id}', array('as' => 'jobs.postEdit', 'uses' => 'Frontend\\JobController@postEdit'));
Route::get('/jobs/cv/{id}', array('as' => 'jobs.cv', 'uses' => 'Frontend\\JobController@getCV'));

//Portal Event Routes
Route::get('/events', 'Frontend\\EventsController@getIndex')->name('events.index');
Route::get('/events/mallEvents/{mallID}', array('as' => 'events.mallEvents', 'uses' => 'Frontend\\EventsController@getMallEvents'));
Route::get('/events/mall/{mallID}', array('as' => 'events.mallEventsPage', 'uses' => 'Frontend\\EventsController@getMallEventsPage'));
Route::get('/events/provinceEvents/{province}', array('as' => 'events.provinceEvents', 'uses' => 'EventsController@getProvinceEvents'));
Route::get('/events/event/{id}/{name}', array('as' => 'events.view_event', 'uses' => 'Frontend\\EventsController@getViewEvent'));
Route::get('/events/search/{keyword}', array('as' => 'events.search', 'uses' => 'Frontend\\EventsController@getKeyWordSearch'));
Route::get('/events/list/{mallID?}','Frontend\\EventsController@getEventList')->name('mallevents.list');
Route::get('/events/portalCreate/{mallID?}', 'Frontend\\EventsController@getCreate')->name('mallevents.create');
Route::post('/events/delete/{id}', array('as' => 'events.delete', 'uses' => 'Frontend\\EventsController@postDelete'));
Route::post('/events/create', array('as' => 'mallevents.postCreate', 'uses' => 'Frontend\\EventsController@postCreate'));
Route::get('/events/edit/{id?}', array('as' => 'mallevents.edit', 'uses' => 'Frontend\\EventsController@getEdit'));
Route::post('/events/edit/{id?}', array('as' => 'mallevents.postEdit', 'uses' => 'Frontend\\EventsController@postEdit'));

Route::get('/exhibitions', 'Frontend\\ExhibitionController@getIndex')->name('exhibitions.index');
Route::get('/exhibitions/mallExhibitions/{mallID}', 'Frontend\\ExhibitionController@getMallExhibitions')->name('exhibitions.mallexhibitions');
Route::get('/exhibitions/provincefilter/{id?}', 'Frontend\\ExhibitionController@getProvinceFilter')->name('exhibitions.ajaxFilter');
Route::get('/exhibitions/list/{mallID?}', array('as' => 'exhibitions.list', 'uses' => 'Frontend\\ExhibitionController@getList'));
Route::post('/exhibitions/delete/{id}', array('as' => 'exhibitions.delete', 'uses' => 'Frontend\\ExhibitionController@postDelete'));
Route::get('/exhibitions/create/{mallID?}', array('as' => 'exhibitions.create', 'uses' => 'Frontend\\ExhibitionController@getCreate'));
Route::post('/exhibitions/postCreate', array('as' => 'exhibitions.postCreate', 'uses' => 'Frontend\\ExhibitionController@postCreate'));
Route::get('/exhibitions/edit/{id?}', array('as' => 'exhibitions.edit', 'uses' => 'Frontend\\ExhibitionController@getEdit'));
Route::post('/exhibitions/edit/{id?}', array('as' => 'exhibitions.postEdit', 'uses' => 'Frontend\\ExhibitionController@postEdit'));

Route::get('/promotions/list', 'Frontend\\PromotionController@getList')->name('promotions.list');
Route::get('/promotions/view/{id}', 'Frontend\\PromotionController@getShow')->name('promotions.view');
Route::get('/promotions/mall/{mallID}', 'Frontend\\PromotionController@getMallIndex')->name('promotions.mallIndex');
Route::get('/promotions/populateTable/{id}/{provinceID?}', 'Frontend\\PromotionController@getPopulateTable')->name('promotions.populate');
Route::get('/promotions/search/{keyword}', 'Frontend\\PromotionController@getKeyWordSearch')->name('promotions.search');
Route::get('/promotions/portalList/{mallID?}', 'Frontend\\PromotionController@getListMallPromotions')->name('mallpromotions.list');
Route::get('/promotions/portalAdd/{mallID?}', 'Frontend\\PromotionController@getCreateMallPromotion')->name('mallpromotions.create');
Route::post('/promotions/create', array('as' => 'promotions.postCreate', 'uses' => 'Frontend\\PromotionController@postCreate'));
Route::get('/promotions/update/{id}', array('as' => 'promotions.update', 'uses' => 'Frontend\\PromotionController@getUpdate'));

//Portal Promotions Routes
Route::post('/promotions/delete/{id}', array('as' => 'promotions.delete', 'uses' => 'Frontend\\PromotionController@postDelete'));
Route::post('/promotions/copy/{id}', array('as' => 'promotions.copy', 'uses' => 'Frontend\\PromotionController@postCopyPromotion'));
//Remote Add
Route::post('/promotions/remote/add', array('as' => 'promotions.postRemoteAdd', 'uses' => 'Frontend\\PromotionController@postAddRemote'));
Route::post('/promotions/remote/update/{id?}', array('as' => 'promotions.postRemoteUpdate', 'uses' => 'Frontend\\PromotionController@postRemoteUpdate'));
Route::post('promotions/update/{id}', array('as' => 'promotions.postUpdate', 'uses' => 'Frontend\\PromotionController@postUpdate'));

Route::get('/join', 'Frontend\\UserController@getSignUp')->name('register');
Route::get('/mall/register/{for}', 'Frontend\\UserController@getMallRegister')->name('mall.register.for');

Route::get('/malls/register', 'Frontend\\UserController@getFrontMallRegister')->name('malls.register');
Route::post('/malls/register', 'Frontend\\UserController@postRegisterMall')->name('malls.postRegister');

Route::get('/shop/getaccess',  'Frontend\\UserController@getExistingShopRegister')->name('shop.getaccess');
Route::post('/user/signup','Frontend\\UserController@postSignUp')->name('user.signup');
Route::get('/shops/createfrontend', 'Frontend\\ShopController@getCreateFrontend')->name('shops.createfrontend');
Route::post('/shops/postcreate', 'Frontend\\ShopController@postCreate')->name('shops.postCreateShop');
Route::any('/shops/subcategory/{category?}','Frontend\\ShopController@getSubcategoryByCategory')->name('shops.subcategory');
Route::get('/shops/registernew/{mallID}', 'Frontend\\ShopController@getRegisterNew')->name('shops.registernew');
Route::get('/shops/register/{mallID?}', 'Frontend\\ShopController@getRegister')->name('shops.register');

Route::get('/user/getchain', 'Frontend\\UserController@getChainAccess')->name('user.getchain');
Route::get('/movies/getaccess', 'Frontend\\UserController@getMovieAccess')->name('movies.getaccess');
Route::get('/movies/hasMallHouse/{mallID}', 'Frontend\\UserController@getHasMallHouse')->name('movies.hasMallHouse');

Route::get('/activate/{userID}/{activationCode}', 'Frontend\\UserController@getActivate')->name('users.activate');

//sds
Route::get('/login', 'Frontend\\UserController@getLogin')->name('user.login');
Route::post('/login', 'Frontend\\UserController@postLogin')->name('user.postLogin');
Route::get('/logout', 'Frontend\\UserController@postLogout')->name('user.logout');

Route::get('/users/updatepassword/{id}', array('as' => 'user.updatepassword', 'uses' => 'Frontend\\UserController@getUpdatePassword'));
Route::post('/users/updatepassword/{id}', array('as' => 'user.postUpdatepassword', 'uses' => 'Frontend\\UserController@postUpdatePassword'));
Route::get('/users/updatedetails', array('as' => 'user.updatedetails', 'uses' => 'Frontend\\UserController@getUpdateUserDetails'));
Route::get('/users/updatedetails', array('as' => 'user.updatedetails', 'uses' => 'Frontend\\UserController@getUpdateUserDetails'));
Route::post('/users/updatedetails/{id}', array('as' => 'user.postUpdatedetails', 'uses' => 'Frontend\\UserController@postUpdateUserDetails'));
Route::get('/users/reset-password', array('as' => 'user.resetPassword', 'uses' => 'Frontend\\UserController@getResetPassword'));
Route::post('/users/reset-password', array('as' => 'user.postResetPassword', 'uses' => 'Frontend\\UserController@postResetPassword'));

Route::get('/portal', 'PortalController@getIndex')->name('portal.index');
Route::get('/users/resend_activation/{id}', 'PortalController@getResendActivation')->name('users.resend_activation');

Route::get('/memberbenefits', array('as' => 'memberbenefits', 'uses' => 'HomeController@getIndex'));
Route::get('/marketinginfo', array('as' => 'portal.marketinginfo', 'uses' => 'PortalController@getMarketingInfo'));
Route::get('/smsterms', array('as' => 'portal.smsterms', 'uses' => 'PortalController@getSMSTerms'));
Route::get('/competitioninfo', array('as' => 'portal.competitioninfo', 'uses' => 'PortalController@getCompetitionInfo'));
Route::get('/compterms', array('as' => 'portal.compterms', 'uses' => 'PortalController@getCompTerms'));
Route::get('/malldetails/{id?}', array('as' => 'portal.malldetails', 'uses' => 'Frontend\\MallsController@getMallDetails'));
Route::get('/mallhours/{id}', array('as' => 'portal.mallhours', 'uses' => 'MallController@getUpdateMallHours'));
Route::post('/updatemallhours/{id}', array('as' => 'portal.updatemallhours', 'uses' => 'MallController@postUpdateMallHours'));
Route::post('/insertmallhours', array('as' => 'portal.insertmallhours', 'uses' => 'MallController@postAddMallHours'));

Route::get('/manager/buysmscredits', array('as' => 'manager.buysmscredits', 'uses' => 'PortalController@getBuySMSCredits'));
Route::post('/manager/buysmscredits', array('as' => 'manager.postBuysmscredits', 'uses' => 'PortalController@postBuySMSCredits'));
//Route::get('manager/sendsms', array('as' => 'manager.getsendsms', 'uses' => 'PortalController@getSendSMS'));
Route::post('/manager/getrecipients', array('as' => 'manager.getfilterrecipient', 'uses' => 'PortalController@postFilterSMSRecipient'));
Route::get('/manager/sendsms', array('as' => 'manager.getfiltermall', 'uses' => 'PortalController@getFilterSMSMall'));
Route::post('/manager/sendtestsms', array('as' => 'manager.sendtestsms', 'uses' => 'PortalController@postSendTestSMS'));
Route::post('/manager/sendsms', array('as' => 'manager.postSendSMS', 'uses' => 'PortalController@postSendSMS'));
Route::get('/manager/smsreports', array('as' => 'manager.smsreports', 'uses' => 'PortalController@getSMSReports'));
Route::get('/manager/smsreport/{id}', array('as' => 'manager.detailedsmsreports', 'uses' => 'PortalController@getDetailedSMSReport'));
Route::post('/manager/validaterecipient', array('as' => 'manager.validaterecipient', 'uses' => 'PortalController@postValidateRecipientList'));
Route::post('/manager/updateusercell/{shopID}/{usertype}/{newcell}', array('as' => 'manager.validaterecipient', 'uses' => 'PortalController@postUpdateUserCell'));

/**
 * Gift Ideas
 */
Route::get('/store/giftideas', array('as' => 'store.giftideas', 'uses' => 'ShopManagerController@getStoreGiftIdeas'));
Route::get('/store/addgiftidea', array('as' => 'store.addgiftidea', 'uses' => 'ShopManagerController@getAddGiftIdea'));
Route::get('/store/giftidea/edit/{id}', array('as' => 'store.editStoreGiftIdea', 'uses' => 'ShopManagerController@getEditStoreGiftIdea'));
Route::post('/store/giftidea/edit/{id}', array('as' => 'store.postEditStoreGiftIdea', 'uses' => 'ShopManagerController@postEditStoreGiftIdea'));
Route::post('/store/addgiftidea', array('as' => 'store.postAddGiftIdea', 'uses' => 'ShopManagerController@postAddGiftIdea'));
Route::post('/store/deletegiftidea/{id}', array('as' => 'store.postDeleteStoreGiftIdea', 'uses' => 'ShopManagerController@postDeleteStoreGiftIdea'));
Route::post('/store/giftidea/updatedisplay/{id}/{status}', array('as' => 'store.postUpdateGiftIdeaDisplayStatus', 'uses' => 'ShopManagerController@postUpdateGiftIdeaDisplayStatus'));

/**
 * Jobs
 */
Route::get('/store/jobs', array('as' => 'store.jobs', 'uses' => 'ShopManagerController@getStoreJobs'));
Route::get('/store/jobs/create', array('as' => 'store.createStoreJob', 'uses' => 'ShopManagerController@getCreateStoreJob'));
Route::post('/store/jobs/create', array('as' => 'store.postCreateStoreJob', 'uses' => 'ShopManagerController@postCreateStoreJob'));
Route::get('/store/jobs/edit/{id}', array('as' => 'store.editStoreJob', 'uses' => 'ShopManagerController@getEditStoreJob'));
Route::post('/store/jobs/edit/{id}', array('as' => 'store.postEditStoreJob', 'uses' => 'ShopManagerController@postEditStoreJob'));

/**
 * Booking
 */
Route::get('/booking/featured', array('as' => 'booking.featured', 'uses' => 'PortalController@getFeatured'));
Route::get('/booking/info/{id?}', array('as' => 'booking.info', 'uses' => 'PortalController@getBookingInfo'));
Route::get('/booking/displayNew', array('as' => 'booking.displayNew', 'uses' => 'PortalController@getDisplayNewBooking'));
Route::post('/booking/featured', array('as' => 'booking.postBookFeatured', 'uses' => 'PortalController@postBookFeatured'));
Route::get('/invoices', array('as' => 'invoices', 'uses' => 'PortalController@getInvoiceList'));

//Mall manager add users
Route::get('/portal/adduser', array('as' => 'portal.adduser', 'uses' => 'PortalController@getAddUser'));
Route::get('/portal/listusers', array('as' => 'portal.listusers', 'uses' => 'PortalController@getListUsers'));
Route::get('/portal/list-tenants', array('as' => 'portal.list-tenants', 'uses' => 'PortalController@getListTenants'));
Route::get('/portal/unapprovedusers', array('as' => 'portal.unapprovedusers', 'uses' => 'PortalController@getListUnapprovedUsers'));
Route::get('/portal/addmalluser/{mallID?}', array('as' => 'malls.quickfind.paginate', 'uses' => 'PortalController@getAddMallUser'));
Route::post('/portal/addmalluser', array('as' => 'portal.postAddmalluser', 'uses' => 'PortalController@postAddMallUser'));
Route::get('/portal/addstoreuser/{mallID}', array('as' => 'portal.addstoreuser', 'uses' => 'PortalController@getAddStoreUser'));

Route::get('/shops/manageradd/{mallID}', array('as' => 'shops.manageradd', 'uses' => 'Frontend\\ShopController@getRegister'));
Route::get('/shops/update/{id}', array('as' => 'shops.update', 'uses' => 'Frontend\\ShopController@getUpdate'));
Route::post('/shops/update/{id}', array('as' => 'shops.postUpdate', 'uses' => 'PortalController@postUpdate'));
Route::post('/shops/files', 'Frontend\\ShopController@postFiles');

//Shops Routes (Portal)
Route::get('/shops/create/{mallID?}', array('as' => 'shops.create', 'uses' => 'Frontend\\ShopController@getIndex'));
Route::get('/shops/category/{mallID}/{category?}', array('as' => 'shops.category', 'uses' => 'Frontend\\ShopController@getCategoryShops'));
Route::post('/shops/delete/{id}', array('as' => 'shops.delete', 'uses' => 'Frontend\\ShopController@postDelete'));
Route::post('/shops/removeimage/{id}/{image}/{imageBig?}', array('as' => 'shops.removeimage', 'uses' => 'Frontend\\ShopController@postRemoveImage'));
Route::post('/shops/register', array('as' => 'shops.postRegister', 'uses' => 'PortalController@postRegisterShop'));

Route::get('/users/edit/{id}', array('as' => 'user.edit', 'uses' => 'Frontend\\UserController@getEdit'));
Route::post('/users/edit/{id}', array('as' => 'user.postEdit', 'uses' => 'Frontend\\UserController@postEdit'));
//Get Password Reset Page
Route::get('/passwordreset/{resetcode}/{id}', array('as' => 'user.getPasswordReset', 'uses' => 'Frontend\\UserController@getPasswordReset'));
Route::post('/passwordreset/{resetcode}/{id}', array('as' => 'user.postPasswordReset', 'uses' => 'Frontend\\UserController@postPasswordReset'));

//Portal Competitions Routes
Route::post('/completeCompetition/{formID}', array('as' => 'competition.submit', 'uses' => 'Frontend\\CompetitionController@postCompetition'));
Route::post('/remoteCompetition/{formID}', array('as' => 'competition.remote', 'uses' => 'Frontend\\CompetitionController@remoteCompetition'));

Route::get('competitions/create', array('as' => 'competitions.create', 'uses' => 'Frontend\\CompetitionsController@getCreate'));
Route::post('competitions/create', array('as' => 'competitions.postCreate', 'uses' => 'Frontend\\CompetitionsController@postCreateCompetition'));

Route::get('/competitions/list', array('as' => 'competitions.list', 'uses' => 'Frontend\\CompetitionsController@getList'));
Route::get('/competitions/responses/{id}', array('as' => 'competitions.responses', 'uses' => 'Frontend\\CompetitionsController@getResponses'));
Route::get('/competitions/feedback/view/{id}', array('as' => 'feedback.view', 'uses' => 'Frontend\\CompetitionsController@getViewFeedback'));
Route::get('/competitions/create/{mallID?}', array('as' => 'competitions.mallCreate', 'uses' => 'Frontend\\CompetitionsController@getCreate'));
Route::post('/competitions/create', array('as' => 'competitions.postInsert', 'uses' => 'Frontend\\CompetitionsController@postCreateCompetition'));
Route::get('/competitions/fields/{id}', array('as' => 'competitions.fields', 'uses' => 'Frontend\\CompetitionsController@getFields'));
Route::get('/competitions/update/{id}', array('as' => 'competitions.update', 'uses' => 'Frontend\\CompetitionsController@getUpdate'));
Route::get('/competitions/addfield', array('as' => 'competitions.addfield', 'uses' => 'Frontend\\CompetitionsController@getAddField'));
Route::post('/competitions/addfield/{id}', array('as' => 'competitions.postAddField', 'uses' => 'Frontend\\CompetitionsController@postAddField'));
Route::post('/competitions/delete/{id}', array('as' => 'competitions.postDelete', 'uses' => 'Frontend\\CompetitionsController@postDelete'));
Route::post('/competitions/fields/{id}', array('as' => 'competitions.postFields', 'uses' => 'AdminController@postUpdateCompetition'));
Route::get('/competitions/export/{id}', array('as' => 'feedback.export', 'uses' => 'Frontend\\CompetitionsController@getExportFeedback'));
Route::get('/competitions/randomize/{id}', array('as' => 'feedback.randomize', 'uses' => 'Frontend\\CompetitionsController@getRandomFeedback'));
Route::get('/competitions/disqualify/{fid}', array('as' => 'winner.disqualify', 'uses' => 'Frontend\\CompetitionsController@getDisqualifyFeedback'));
Route::get('/competitions/updatefield/{id}/{formID}', array('as' => 'comp.updatefield', 'uses' => 'Frontend\\CompetitionsController@getUpdateCompField'));
//Remote Add
Route::any('/competitions/remote/add', array('as' => 'competitions.postRemoteAdd', 'uses' => 'Frontend\\CompetitionsController@postInsertRemote'));
Route::post('/competitions/remote/update/{id?}', array('as' => 'competitions.postRemoteUpdate', 'uses' => 'Frontend\\CompetitionsController@postRemoteUpdate'));

/**
 * Bookings in the backend
 */
Route::get('/admin/bookings', array('as' => 'admin.bookings', 'uses' => 'AdminController@getFeatured'));
Route::get('/admin/bookingdata', array('as' => 'admin.bookingdata', 'uses' => 'AdminController@getBookingData'));
Route::get('/admin/bookingDropdown', array('as' => 'admin.bookingDropdown', 'uses' => 'AdminController@getBookingDropdown'));
Route::get('/admin/bookingMalls', array('as' => 'admin.bookingMalls', 'uses' => 'AdminController@getBookingMalls'));
Route::get('/admin/bookingShopsList', array('as' => 'admin.bookingShops', 'uses' => 'AdminController@getBookingShopList'));
Route::get('/admin/bookingShops', array('as' => 'admin.getBookingShops', 'uses' => 'AdminController@getBookingShops'));
Route::get('/admin/checkDates', array('as' => 'admin.checkDates', 'uses' => 'AdminController@checkDates'));
Route::get('/admin/checkStart', array('as' => 'admin.checkStart', 'uses' => 'AdminController@checkStart'));
Route::post('/admin/bookings/create', array('as' => 'admin.createbooking', 'uses' => 'AdminController@postCreateBooking'));
Route::post('/admin/bookings/remove/{id}', array('as' => 'admin.removeBooking', 'uses' => 'AdminController@removeFeature'));
Route::get('/admin/approveBooking/{id}/{approved}', array('as' => 'admin.approveBooking', 'uses' => 'AdminController@approveBooking'));
Route::get('/admin/editBooking/{id}', array('as' => 'admin.editBooking', 'uses' => 'AdminController@editBooking'));

/**
 * Tenant
 */
Route::get('/store/adduser', array('as' => 'store.adduser', 'uses' => 'ShopManagerController@getAddStoreUser'));
Route::get('/store/userlist', array('as' => 'store.userlist', 'uses' => 'ShopManagerController@getListUsers'));
Route::get('/store/edituser/{id}', array('as' => 'store.edituser', 'uses' => 'ShopManagerController@getEditUser'));

Route::get('/store', array('as' => 'store.index', 'uses' => 'ShopManagerController@getStoreIndex'));
Route::get('/store/portalUpdate/{id?}', array('as' => 'store.update', 'uses' => 'ShopManagerController@getUpdateMallStore'));
Route::get('/store/list', array('as' => 'store.shoplist', 'uses' => 'ShopManagerController@getListUserStores'));
Route::post('/store/update', array('as' => 'store.postUpdateShop', 'uses' => 'ShopManagerController@postUpdateShop'));
Route::get('/store/marketinginfo', array('as' => 'store.marketinginfo', 'uses' => 'ShopManagerController@getStoreMarketingInfo'));
Route::get('/store/compterms', array('as' => 'store.compterms', 'uses' => 'ShopManagerController@getStoreCompetitionTerms'));
Route::get('/store/editprofile/{id}', array('as' => 'store.editprofile', 'uses' => 'ShopManagerController@getEditStoreUserProfile'));
Route::post('/store/editprofile/{id}', array('as' => 'store.postEditUserProfile', 'uses' => 'ShopManagerController@postEditUserProfile'));
Route::get('/store/updatepassword/{id}', array('as' => 'store.updatepassword', 'uses' => 'ShopManagerController@getUpdateStoreUserPassword'));
Route::post('/store/updatepassword/{id}', array('as' => 'store.postUpdatePassword', 'uses' => 'ShopManagerController@postUpdateStoreUserPassword'));

Route::get('/exhibitions/view/{id}', array('as' => 'exhibitions.view_exhibition', 'uses' => 'Frontend\\ExhibitionController@getViewExhibition'));
Route::get('/exhibitions/mall/{mallID}', array('as' => 'exhibitions.mall', 'uses' => 'Frontend\\ExhibitionController@getByMallID'));
Route::get('/exhibitions/provinceExhibitions/{mallID}', array('as' => 'exhibitions.provinceexhibitions', 'uses' => 'Frontend\\ExhibitionController@getProvinceExhibitions'));

/**
 * Shops
 */
Route::get('/chainstores', array('as' => 'chainshop.chainstores', 'uses' => 'ShopManagerController@getChainShops'));
Route::post('/deletestore/{id}', array('as' => 'chainshop.delete', 'uses' => 'ShopManagerController@postDeleteStore'));
Route::get('/chainstores/edit/{id}', array('as' => 'chainshop.edit', 'uses' => 'ShopManagerController@getEditChainStore'));
Route::post('/chainstores/store/postedit/{id}', array('as' => 'chainshop.postEditShop', 'uses' => 'ShopManagerController@postEditStore'));
Route::get('/chainstores/shop/portalCreate', array('as' => 'chainshop.create', 'uses' => 'ShopManagerController@getCreateShop'));
Route::get('/chainstores/provincefilter/{id?}', array('as' => 'chainshop.provincefilter', 'uses' => 'ShopManagerController@getFilterChainByProvince'));

/**
 * Promotions
 */
Route::get('/chainstores/promotions', array('as' => 'chainshop.promotions', 'uses' => 'ShopManagerController@getChainPromotionList'));
Route::get('/chainstores/promotion/edit/{id}', array('as' => 'chainshop.editpromotion', 'uses' => 'ShopManagerController@getEditPromotion'));
Route::get('/chainstores/promotions/create', array('as' => 'chainshop.createpromotion', 'uses' => 'ShopManagerController@getCreateChainPromotion'));
Route::get('/chainstores/promotions/filtermall/{mallID}', array('as' => 'chainshop.filtermall', 'uses' => 'ShopManagerController@getFilterPromotionByMall'));
Route::get('/chainstores/promotions/mallpromotions/{mallID}', array('as' => 'chainshop.mallpromotions', 'uses' => 'ShopManagerController@getMallPromotions'));
Route::post('/chainstores/promotion/create', array('as' => 'chainshop.postCreatePromotion', 'uses' => 'ShopManagerController@postCreatePromotion'));
Route::get('/chain/mallpromotions/{id}', array('as' => 'chain.listPromoByMall', 'uses' => 'ShopManagerController@getListChainPromosByMall'));

/**
 * Jobs
 */
Route::get('/chainstores/jobs', array('as' => 'chainshop.jobs', 'uses' => 'ShopManagerController@getJobsList'));
Route::get('/chainstores/jobs/create', array('as' => 'chainshop.createjob', 'uses' => 'ShopManagerController@getCreateJob'));
Route::get('/chainstores/jobs/applicants/{jobID}', array('as' => 'chainshop.getjobapplicants', 'uses' => 'ShopManagerController@getJobApplicants'));
Route::get('/chainstores/jobs/applicant/{id}', array('as' => 'chainshop.getjobapplicant', 'uses' => 'ShopManagerController@getJobApplicant'));
Route::get('/chainstores/jobs/cv/{cv}', array('as' => 'chainshop.getapplicantcv', 'uses' => 'ShopManagerController@getApplicantCv'));
Route::post('/chainstores/jobs/create', array('as' => 'chainshop.postCreateJob', 'uses' => 'ShopManagerController@postCreateJob'));
Route::get('/chainstores/jobs/filtermall/{id}', array('as' => 'chainshop.filterjobbymall', 'uses' => 'ShopManagerController@getFilterJobByMall'));
Route::get('/chainstores/jobs/malljobs/{id}', array('as' => 'chainshop.malljobs', 'uses' => 'ShopManagerController@getMallJobs'));

/* * ********************************************************** */
/* Chain Shop User
  /************************************************************ */
Route::get('/chainshop/marketinginfo', array('as' => 'chainshop.marketinginfo', 'uses' => 'ShopManagerController@getMarketingInfo'));
/**
 * Users
 */
Route::get('/chainstores/editprofile/{id}', array('as' => 'chainshop.editprofile', 'uses' => 'ShopManagerController@getEditUserProfile'));
Route::post('/chainstores/editprofile/{id}', array('as' => 'chainshop.postEditProfile', 'uses' => 'ShopManagerController@postEditProfile'));
Route::get('/chainstores/updatepassword/{id}', array('as' => 'chainshop.updatepassword', 'uses' => 'ShopManagerController@getUpdatePassword'));
Route::get('/chainstores/adduser', array('as' => 'chainshop.adduser', 'uses' => 'ShopManagerController@getAddChainUser'));
Route::get('/chainstores/listusers', array('as' => 'chainshop.listusers', 'uses' => 'ShopManagerController@getChainUserList'));
Route::get('/chainstores/edituser/{id}', array('as' => 'chainshop.edituser', 'uses' => 'ShopManagerController@getEditChainUser'));
Route::post('/chainstores/updatepassword/{id}', array('as' => 'chainshop.postUpdatePassword', 'uses' => 'ShopManagerController@postUpdatePassword'));

/* * ************************************************************** */
/* Independent Movie House
  /**************************************************************** */
Route::get('/moviehouse', array('as' => 'moviehouse.index', 'uses' => 'Frontend\\MovieHouseController@getIndex'));
Route::get('/moviehouse/addmovieschedule', array('as' => 'moviehouse.addmovieschedule', 'uses' => 'Frontend\\MovieHouseController@getAddMovieSchedule'));
Route::post('/moviehouse/postaddmovieschedule', array('as' => 'moviehouse.postAddMovieSchedule', 'uses' => 'Frontend\\MovieHouseController@postAddMovieSchedule'));
Route::get('/moviehouse/listmovieschedules', array('as' => 'moviehouse.listmovieschedules', 'uses' => 'Frontend\\MovieHouseController@getListMovieSchedules'));
Route::post('/moviehouse/deletemovieschedule/{id}', array('as' => 'moviehouse.deletemovieschedule', 'uses' => 'Frontend\\MovieHouseController@postDeleteMovieSchedule'));
Route::get('/moviehouse/movieschedule/edit/{id}', array('as' => 'moviehouse.getEditMovieSchedule', 'uses' => 'Frontend\\MovieHouseController@getEditMovieSchedule'));
Route::post('/moviehouse/movieschedule/edit/{id}', array('as' => 'moviehouse.postEditMovieSchedule', 'uses' => 'Frontend\\MovieHouseController@postEditMovieSchedule'));
Route::get('/moviehouse/competitions/list', array('as' => 'moviehouse.getListCompetitions', 'uses' => 'Frontend\\MovieHouseController@getListCompetitions'));
Route::get('/moviehouse/competitions/create', array('as' => 'moviehouse.getCreateCompetition', 'uses' => 'Frontend\\MovieHouseController@getCreateCompetition'));
Route::post('/moviehouse/competitions/create', array('as' => 'moviehouse.postCreateCompetition', 'uses' => 'Frontend\\MovieHouseController@postCreateCompetition'));
Route::get('/moviehouse/updatepassword/{id}', array('as' => 'moviehouse.getUpdatePassword', 'uses' => 'Frontend\\MovieHouseController@getUpdatePassword'));
Route::post('/moviehouse/updatepassword/{id}', array('as' => 'moviehouse.postUpdatePassword', 'uses' => 'Frontend\\MovieHouseController@postUpdatePassword'));
Route::get('/moviehouse/updateprofile/{id}', array('as' => 'moviehouse.getUpdateProfile', 'uses' => 'Frontend\\MovieHouseController@getUpdateProfile'));
Route::post('/moviehouse/updateprofile/{id}', array('as' => 'moviehouse.postUpdateProfile', 'uses' => 'Frontend\\MovieHouseController@postUpdateProfile'));

//Auth::routes();

Route::group(array('prefix' => 'admin'), function () {
    Route::get('/malls', 'MallsController@index')->name('admin.index');
    Route::get('/malls/add', 'MallsController@getAddMall')->name('mall.add.get');
    Route::post('/malls/add', 'MallsController@postAddMall')->name('mall.add.post');
    Route::get('/malls/edit/{id}', 'MallsController@edit')->name('malls.edit.get');
    Route::post('/malls/edit/{id}/mall-details', 'MallsController@postMallDetailsForm');
    Route::post('/malls/edit/{id}/information', 'MallsController@postMallInformationForm');
    Route::post('/malls/edit/{id}/images', 'MallsController@postMallImagesForm');
    Route::post('/malls/edit/{id}/special-features', 'MallsController@postMallSpecialFeaturesForm');
    Route::post('/malls/edit/{id}/centre-management', 'MallsController@postMallCentreManagementForm');
    Route::post('/malls/edit/{id}/marketing-consultant', 'MallsController@postMallMarketingConsultantForm');
    Route::post('/malls/edit/{id}/leasing-agent', 'MallsController@postMallLeasingAgentForm');
    Route::post('/malls/edit/{id}/shopping-council', 'MallsController@postMallShoppingCouncilForm');
    Route::post('/malls/edit/{id}/trading-hours', 'MallsController@postMallTradingHoursForm');
    Route::post('/malls/edit/{id}/settings', 'MallsController@postMallSettingsForm');
    Route::delete('/malls/removeimage/{mallId}/{imageId}', 'MallsController@deleteMallRemoveImage');

    Route::get('/exhibitions', 'ExhibitionsController@exhibitions')->name('exhibitions');
    Route::get('/exhibitions/{mallId}', 'ExhibitionsController@getMallExhibitions')->name('admin.getMallExhibitions');
    Route::get('/exhibitions/create/{mallId}/', 'ExhibitionsController@getAddExhibition')->name('exhibitions.create.get');
    Route::post('/exhibitions/create/{mallId}', 'ExhibitionsController@postAddExhibition')->name('exhibitions.create.post');
    Route::get('/exhibitions/edit/{exhibitionId}', 'ExhibitionsController@getEditExhibition')->name('exhibitions.edit.get');
    Route::post('/exhibitions/edit/{exhibitionId}', 'ExhibitionsController@postEditExhibition')->name('exhibitions.edit.post');
    Route::get('/exhibitions/delete/{exhibitionId}', 'ExhibitionsController@postDelete')->name('exhibitions.delete.post');

    Route::get('/jobs/', 'JobsController@getJobs')->name('admin.jobs');
    Route::get('/jobs/mall/{mallId}', 'JobsController@getMallJobs')->name('jobs.mall.list');
    Route::get('/jobs/shop/{shopId}', 'JobsController@getShopJobs')->name('jobs.shop.list');
    Route::get('/jobs/{jobId}/applicants', 'JobsController@getJobApplicants')->name('jobs.applicants.list');
    Route::get('/jobs/applicant/{applicantId}', 'JobsController@getJobApplicant')->name('jobs.applicant.get');
    Route::get('/jobs/shop/{shopId}/add', 'JobsController@getAddShopJob')->name('shop.job.add.get');
    Route::post('/jobs/shop/{shopId}/add', 'JobsController@postAddShopJob')->name('shop.job.add.post');
    Route::get('/jobs/{jobId}/edit', 'JobsController@getJobEdit')->name('jobs.edit.get');
    Route::post('/jobs/{jobId}/edit', 'JobsController@postJobEdit')->name('jobs.edit.post');
    Route::get('/jobs/delete/{jobId}', 'JobsController@postJobDelete')->name('jobs.delete.post');

    Route::get('/mall-list/{provinceId}', 'AdminController@getMalls');

    Route::get('/events', 'MallsController@events')->name('events');
    Route::get('/events/create/{mallID}', 'EventController@getCreate')->name('events.create.get');
    Route::post('/events/post-create/{mallID}', 'EventController@postCreate');
    Route::get('/events/{mallID}', 'EventController@getEventsByMall');
    Route::get('/events/edit/{id}', 'EventController@getEdit');
    Route::get('/events/get-backend/{mallID}', 'EventController@getMallBackendEvents');
    Route::post('/events/post-update/{id}', 'EventController@postUpdate');
    Route::get('/events/delete/{id}', 'EventController@postDelete');
    Route::post('/events/removesession/{mallID}', array('as' => 'events.removesession', 'uses' => 'AdminController@postRemoveSessionKey'));

    Route::get('/maps', 'MapsController@index')->name('maps');
    Route::get('/maps/{mallID}', 'MapsController@getMapsByMall');
    Route::get('/maps/create/{mallID}', 'MapsController@getCreate')->name('maps.create.get');
    Route::post('/maps/post-create/{mallID}', 'MapsController@postCreate');
    Route::post('/maps/post-update/{id}', 'MapsController@postUpdate');
    Route::get('/maps/edit/{id}', 'MapsController@getEdit');
    Route::get('/maps/delete/{id}', 'MapsController@postDelete');

    Route::get('/users', 'AdminController@getUsers')->name('users');
    Route::get('/user-list', 'AdminController@getUserList')->name('users.list');
    Route::get('/users/create', 'AdminController@getCreateUser')->name('users.create.get');
    Route::post('/users/create', 'AdminController@postCreateUser')->name('users.create.post');
    Route::get('/users/suspend/{id}', 'AdminController@getSuspendUser')->name('users.suspend.get');
    Route::post('/users/suspend/{id}', 'AdminController@postSuspendUser')->name('users.suspend.post');
    Route::get('/users/unsuspend/{id}', 'AdminController@getUnsuspendUser')->name('users.unsuspend');
    Route::get('/users/approve/{id}', 'AdminController@getApproveUser')->name('users.approve');
    Route::delete('/users/delete/{id}', 'AdminController@deleteUser')->name('users.delete');
    Route::get('/users/edit/{userId}', 'AdminController@getEditUser')->name('users.edit.get');
    Route::post('/users/edit/{userId}', 'AdminController@postEditUser')->name('users.edit.post');
    Route::get('/provinces/cities/{province}', 'AdminController@getProvinceCities');
    Route::get('/shops/getbymall', 'Frontend\\ShopController@postShopByMalls');

    Route::get('/competitions/{mallId?}', 'CompetitionsController@getCompetitions')->name('admin.competitions')->where('mallId', '[0-9]+');
    Route::get('/competitions/create/{mallId?}', 'CompetitionsController@getCreateCompetition')->name('competitions.create.get');
    Route::get('/competitions/edit/{competitionId}', 'CompetitionsController@getEditCompetition')->name('competitions.edit.get');
    Route::post('/updateCompetition/{formID}',  'CompetitionsController@postUpdateCompetition')->name('competitions.update.post');
    Route::get('/editField/{fieldID}', 'CompetitionsController@getEditField')->name('admin.editField');
    Route::get('/changeOrder/{fieldID}/{move}', 'CompetitionsController@getChangeOrder')->name('admin.malldropdown');
    Route::post('/updateField/{fieldID}', 'AdminController@postUpdateField')->name('admin.updateField');
    Route::post('/competitions/create', 'CompetitionsController@postCreateCompetition')->name('competitions.create.post');
    Route::get('/competitions/page-form-fields', 'CompetitionsController@getDefaultFormFields')->name('competitions.form-fields');
    Route::get('/competition/responses/{competitionId}', 'CompetitionsController@getCompetitionResponses')->name('competitions.responses.list');
    Route::get('/competition/responses/{responseId}/random-winner', 'CompetitionsController@getCompetitionChooseRandomWinner')->name('competitions.responses.random-winner');
    Route::delete('/competition/responses/delete/{responseId}/', 'CompetitionsController@deleteCompetitionResponse')->name('competitions.responses.delete');
    Route::get('/competition/responses/{responseId}/download-csv/{page?}/{perPage?}', 'CompetitionsController@getCompetitionResponsesDownloadCSV')->name('competitions.responses.download-csv');
    Route::get('/competition/response/answers/{responseId}', 'CompetitionsController@getCompetitionResponseAnswers')->name('competitions.responses.answers.list');
    Route::get('/competition/response/answers/{responseId}/choose-winner', 'CompetitionsController@getCompetitionChooseWinner')->name('competitions.responses.answers.choose-winner');
    Route::get('/competition/response/answers/{competitionWinnerId}/remove-winner', 'CompetitionsController@getCompetitionRemoveWinner')->name('competitions.responses.answers.remove-winner');
    Route::delete('/competitions/delete/{competitionId}', 'CompetitionsController@deleteCompetition')->name('competitions.delete');
    Route::get('/removeWinner/{feedbackID}', 'CompetitionsController@getRemoveWinner')->name('admin.chooseWinner');

    Route::get('/categories', 'CategoryController@index')->name('categories');
    Route::get('/shop-categories', 'ShopController@getCategories')->name('shop-categories');
    Route::get('/categories/edit/{id}', 'CategoryController@getEdit')->name('categories.edit');
    Route::post('/categories/post-update/{id}', 'CategoryController@postUpdate')->name('categories.post-update');
    Route::get('/categories/create', 'CategoryController@getCreate')->name('categories.create');
    Route::post('/categories/post-create', 'CategoryController@postCreate')->name('categories.post-create');
    Route::get('/categories/delete/{id}', 'CategoryController@postDelete');

    Route::get('/sub-categories/{id}', 'CategoryController@subCategories')->name('sub-categories');
    Route::get('/sub-categories/create/{id}', 'CategoryController@addSubCategory')->name('sub-categories.create');
    Route::get('/category/children/{id}', 'CategoryController@getSubCategories')->name('categories-children');
    Route::post('/sub-category/post-create', 'CategoryController@postCreateCategory')->name('sub-category-post-create');
    Route::get('/sub-category/edit/{id}', 'CategoryController@editSubCategory')->name('sub-category-get-edit');
    Route::post('/sub-category/update/{id}', 'CategoryController@updateSubCategory')->name('sub-category-update');
    Route::get('/sub-category/delete/{id}', 'CategoryController@deleteSubCategory');

    Route::get('/promotions', 'PromotionController@index')->name('promotions');
    Route::get('/mall-promotions/{mallID}', 'PromotionController@getMallShops')->name('mall-promotions');
    Route::get('/shop-promotions/{shopID}', 'PromotionController@getShopPromotions')->name('shop-promotions');
    Route::get('/shop-promotions-data-point/{shopID}', 'PromotionController@shopPromotionsDataPoint')->name('shop-promotions');
    Route::get('/promotions/create/{shopId?}', 'PromotionController@getCreate')->name('promotions.create.get');
    Route::get('/promotions/edit/{id?}', 'PromotionController@getEdit')->name('promotions.edit.get');
    Route::post('/promotions/post-create', 'PromotionController@postCreate')->name('promotions.post-create');
    Route::post('/promotions/post-edit/{id}', 'PromotionController@postUpdate')->name('promotions.post-edit');
    Route::get('/create-mall-promotions/{mallId?}', 'PromotionController@getCreateMallPromotion')->name('create-mall-promotions.get');
    Route::get('/promotions/delete/{id}', 'PromotionController@postDelete');

    Route::get('/shops', 'ShopController@index')->name('shops');
    Route::get('/shops/province-malls/{mallID}', 'ShopController@getProvinceMalls')->name('shop-province-malls');
    Route::get('/mall-shops/{mallID}', 'ShopController@getShopsByMall')->name('mall-shops');
    Route::get('/shops/create/{mallID}', 'ShopController@getCreate')->name('shops.create.get');
    Route::get('/shops/mall/{mallID}', 'ShopController@mallShopsData')->name('mall-shops.data');
    Route::get('/shops/edit/{id}', 'ShopController@getEdit')->name('shops.edit.get');
    Route::post('/shops/post-create', 'ShopController@postCreate')->name('shops.post-create');
    Route::post('/shops/post-edit/{id}', 'ShopController@postUpdate')->name('shops.post-update');
    Route::post('/shops/post-product-details', 'ShopController@postProductDetails')->name('shops.post-product-details');
    Route::post('/shops/post-contact-details', 'ShopController@postContactDetails')->name('shops.post-contact-details');
    Route::get('/maps/editimage/{id}', array('as' => 'admin.editImage', 'uses' => 'ShopController@getEditMapImage'));
    Route::post('/shops/post-map-details', 'ShopController@postMapDetails')->name('shops.post-map-details');
    Route::post('/shops/post-image-details', 'ShopController@postImageDetails')->name('shops.post-image-details');
    Route::post('/shops/post-shop-brands', 'ShopController@postShopBrands')->name('shops.post-shop-brands');
    Route::post('/shops/post-card-details', 'ShopController@postShopCards')->name('shops.post-shop-cards');
    Route::post('/shops/post-owner-details', 'ShopController@postOwnerDetails')->name('shops.post-owner-details');
    Route::post('/shops/post-reservation-details', 'ShopController@postReservationDetails')->name('shops.post-reservation-details');
    Route::get('/shops/delete/{id}', 'ShopController@postDelete');
    Route::get('/shops/export-to-excel/{mallId}', 'ShopController@exportToExcel');
    Route::get('/shops/get-generic', 'ShopController@getGenericShops');
    Route::get('/shops/get-generic-shop-names', 'ShopController@getGenericShopNames');
    Route::get('/shops/edit-generic/{id}', 'ShopController@editGenericShop');
    Route::post('/shops/update-generic/{id}', 'ShopController@updateGenericShop');
    Route::post('/shops/update-generic-products', 'ShopController@updateGenericShopProducts');
    Route::post('/shops/update-generic-images', 'ShopController@updateGenericShopImages');
    Route::post('/shops/post-generic-card-details', 'ShopController@updateGenericShopCards');
    Route::get('/shops/write-to-generic/{shopId}', 'ShopController@writeToGeneric');
    Route::get('/shops/copy-from-generic/{shopId}/{genericId}', 'ShopController@copyFromGeneric');
    Route::get('/shops/remove-image/{id}/{image}/{imageBig?}', array('as' => 'shops.removeimage', 'uses' => 'ShopController@postRemoveImage'));
    Route::post('/shops/file-upload', array('as' => 'shops.file-upload', 'uses' => 'ShopController@uploadFile'));
    Route::get('/shops/delete-file/{id}', array('as' => 'shops.delete-file', 'uses' => 'ShopController@deleteFileUpload'));

    Route::get('/createpageformfields',  'AdminController@getCreateFormFields')->name('admin.createpageformfields');
    Route::get('/addField/{formID?}',  'AdminController@getAddField')->name('admin.addfield');
    Route::get('/getFields/{form_id?}',  'AdminController@getFields')->name('admin.getFields');
});
Route::get('/test',function (){
    return view('frontend');
});
