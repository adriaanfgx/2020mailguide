<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $table = 'movie';
    protected $connection = "mysql-finemov";
    public $timestamps = false;
    protected $primaryKey = 'movieID';

    public function scopeDisplay($query)
    {
        return $query->where('display', '=', 'Y');
    }

    public function showing()
    {
        return $this->belongsToMany('App\WhatsShowing', 'whatsShowingMovies','movieID', 'whatsShowingID')->withPivot('times', 'featured','finalWeek');
    }

    public function malls()
    {
        return $this->belongsToMany('App\MovieMall');
    }

    public function thisMall($movie, $mallID)
    {
        return $movie->mallID == $mallID;
    }

    public function reviews()
    {
        return $this->hasMany('App\Review', 'whatMovieID');
    }
}
