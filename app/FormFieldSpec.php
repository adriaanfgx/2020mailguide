<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormFieldSpec extends Model
{
    protected $table = 'formFieldSpecs';

    public $timestamps = false;

    protected $primaryKey = 'formFieldSpecID';

    protected $fillable = array('formFieldID', 'defaultValue', 'maxlength', 'size', 'rows', 'cols', 'wrap', 'multiple', 'fileTypes');
}
