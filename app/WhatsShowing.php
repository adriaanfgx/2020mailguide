<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WhatsShowing extends Model
{
    protected $table = 'whatsShowing';
    protected $connection = "mysql-finemov";
    public $timestamps = false;
    protected $primaryKey = 'whatsShowingID';
    protected $guarded = array('whatsShowingID');

    public function moviesshowing(){

        return $this->hasMany('App\MovieShowing','whatsShowingID');
    }
    public function movie()
    {
        return $this->belongsToMany('App\Movie', 'whatsShowingMovies', 'whatsShowingID','movieID')->withPivot('times', 'featured','finalWeek');
    }

    public function mall()
    {
        return $this->belongsTo('App\MovieMall');
    }
}
