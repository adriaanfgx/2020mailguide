<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{

    public $timestamps = false;

    public function malls()
    {
        return $this->hasMany('shoppingMall', 'province_id');
    }

    public function cities()
    {
        return $this->hasMany('App\City');
    }

}
