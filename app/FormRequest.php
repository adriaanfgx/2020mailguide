<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormRequest extends Model
{
    protected $table = 'formRequest';

    public $timestamps = false;

    protected $guarded = array('formRequestID');

    protected $primaryKey = 'formRequestID';

    public function scopeOrdered($query)
    {
        return $query->orderBy('dateAdded', 'desc');
    }

    public function scopeActive($query)
    {
        return $query->where('display', '=', 'Y');
    }

    public function mall()
    {
        return $this->belongsTo('App\ShoppingMall', 'mallID');
    }

    public function shop()
    {
        return $this->belongsTo('Shop', 'shopMGID');
    }
}
