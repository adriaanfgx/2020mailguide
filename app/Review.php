<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Review extends Model
{
    protected $table = 'review';
    protected $connection = "mysql-finemov";
    public $timestamps = false;
    protected $primaryKey = 'reviewID';

    public function reviewer()
    {
        return $this->belongsTo('App\Reviewer', 'reviewerID');
    }

    public function movie()
    {
        return $this->belongsTo('App\Movie');
    }

    public static function validate($input){

        $rules = array (
            'reviewer_captcha' => 'required|captcha',
            'reviewer_name' => 'required',
            'reviewer_email' => 'required|email',
            'review'=>'required',
            'rating'=>'required'
        );

        $messages = array(
            'captcha' => 'The captcha you entered is invalid..'
        );

        return Validator::make( $input, $rules,$messages );

    }
}
