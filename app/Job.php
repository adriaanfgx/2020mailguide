<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Job extends Model
{
    protected $table = 'jobs';

    protected $primaryKey = 'jobID';

    public $timestamps = false;

    protected $fillable = array('title','shopMGID','refNum','startDate','salary','description','contactPerson','telephone','fax','cell','email','dateAdded','mallID','display','endDate','showInfo');

    public function mall()
    {
        return $this->belongsTo('App\ShoppingMall', 'mallID');
    }

    public function shop()
    {
        return $this->belongsTo('App\Shop', 'shopMGID');
    }

    public function applications()
    {
        return $this->hasMany('JobApplication', 'JobID');
    }

    public function scopeDisplay($query)
    {
        return $query->where( 'display', '=', 'Y');
    }

    public function scopeCurrent($query)
    {
        return $query->where( 'endDate', '>=', date('Y-m-d'));
    }

    public static function validate( $input ){

        $rules = array (
            'mallID' =>'sometimes',
            'title' => 'required',
            'endDate' => 'required',
            'description' => 'required',
            'email' => 'Between:3,64|Email'
        );

        return Validator::make( $input, $rules );
    }

}
