<?php
/**
 * Created by JetBrains PhpStorm.
 * User: likho
 * Date: 2014/08/19
 * Time: 3:07 PM
 * To change this template use File | Settings | File Templates.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;


class Category extends Model
{
    protected $table = 'categories';

    protected $guarded = array('id');
}
