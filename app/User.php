<?php

namespace App;

use Cartalyst\Sentinel\Users\EloquentUser;
use Cartalyst\Sentinel\Users\UserInterface;


class User extends EloquentUser implements UserInterface {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];

    /**
     *
     */
    protected $guarded = ['id'];

    protected $fillable = [];

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    /**
     *Mall Association
     */
    public function malls(){
        return $this->belongsToMany('App\ShoppingMall','user_malls','user_id','mall_id');
    }

    public function userMalls(){
        return $this->hasMany('App\UserMall');
    }

    public function userShops()
    {
        return $this->hasMany('App\UserShop');
    }

    /**
     * Chain Association
     */
    public function chains(){
        return $this->belongsToMany('App\Shop','user_chain','user_id','chain_name');
    }
    /**
     * Chain Association
     */
    public function chainName(){
        return $this->hasMany('App\UserChain');
    }

    /**
     * Shop Association
     */
    public function shops(){
        return $this->belongsToMany('App\Shop','user_shops','user_id','shop_id');
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }


    public function adminUser()
    {
        return $this->belongsTo('App\AdminUser');
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }
}
