<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class BookingAccount extends Model
{

    protected $table = 'bookingAccount';
    public $timestamps = false;
    protected $primaryKey = 'bookingAccountID';
    protected $guarded = array('bookingAccountID');

    public function bookedBy()
    {

        return $this->hasOne('BookedBy', 'bookedByID', 'bookedByID');
    }

}
