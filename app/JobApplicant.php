<?php

namespace App;

use \Illuminate\Database\Eloquent\Model;

class JobApplicant extends Model
{

    protected $table = 'jobApplicants';

    public $timestamps = false;
    protected $primaryKey = 'jobApplicantID';
    protected $guarded = array('jobApplicantID');

    public static function validate($input)
    {
        $rules = array(
            'name' => 'required|alpha',
            'surname' => 'required|alpha',
            'email' => 'Between:8,64|email',
            'cell' => 'digits_between:6,13',
            'cv' => 'mimes:doc,docx,txt,pdf|max:1000'
        );

        return Validator::make($input, $rules);
    }

    public function application()
    {
        return $this->hasOne('JobApplication', 'jobApplicantID');
    }
}
