<?php
namespace App;
use  \Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{

    protected $table = 'jobApplications';

    public $timestamps = false;
    protected $primaryKey = 'jobApplicantID';
    protected $fillable = array('jobApplicantID', 'jobID', 'dateAdded', 'viewed', 'dateViewed', 'chosen', 'dateChosen');

    public function job()
    {
        return $this->belongsTo('App\Job', 'jobID');
    }
}
