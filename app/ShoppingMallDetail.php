<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class ShoppingMallDetail extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shoppingMall_details';

    public $timestamps = false;


    protected $fillable = array('mallID', 'management', 'marketing', 'parking', 'security', 'facilities', 'leasing', 'giftcards', 'tourism');

    public function mall()
    {
        return $this->belongsTo('App\ShoppingMall', 'mallID');
    }

}
