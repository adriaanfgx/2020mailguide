<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MGGeneric extends Model
{
    protected $table = 'shopGeneric';
    public $timestamps = false;
    protected $primaryKey = 'shopGenericID';
    protected $guarded = ['shopGenericID'];
}
