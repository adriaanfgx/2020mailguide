<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = 'client';

    protected $connection = "mysql_fineclient";

    public $timestamps = false;

    protected $primaryKey = 'clientID';
}
