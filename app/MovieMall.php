<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieMall extends Model
{
    protected $table = 'shoppingMall';
    protected $connection = "mysql-finemov";
    public $timestamps = false;
    protected $primaryKey = 'mallID';
    protected $guarded = array('');


    public function movies()
    {
        return $this->belongsToMany('App\Movie');
    }

    public function whatsShowing()
    {
        return $this->hasMany('App\WhatsShowing', 'mallID');
    }

    public function scopeDisplay($query)
    {
        return $query->where( 'display', '=', 'Y');
    }
}
