<?php
namespace App;

use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Model;

class MGForm extends Model
{

    protected $table = 'forms';

    protected $guarded = array('formID');

    public $timestamps = false;

    protected $primaryKey = 'formID';

    protected $fillable = array('formID', 'pageID', 'recipient', 'subject', 'description', 'thanksMsg', 'submitBtnText', 'dateAdded', 'activated', 'formType', 'startDate', 'endDate', 'thumbnail', 'image', 'noForm', 'displayFor', 'facebook', 'imageFB', 'TsandCsFile');

    public function mall()
    {
        return $this->belongsToMany('App\ShoppingMall', 'formRequest', 'formID', 'mallID')->withPivot('userID', 'shopMGID', 'dateAdded', 'display');
    }

    public function formRequest()
    {
        return $this->hasOne('App\FormRequest', 'formID', 'formID');
    }

    public function shop()
    {
        return $this->belongsToMany('Shop', 'formRequest', 'formID', 'shopMGID')->withPivot('userID', 'mallID', 'dateAdded', 'display');
    }

    public function scopeCurrent($query)
    {
        return $query->where('endDate', '>=', date('Y-m-d'));
    }

    public function scopeActive($query)
    {
        return $query->where('activated', '=', 'Y');
    }

    public function scopeCompetition($query)
    {
        return $query->where('formType', '=', 'Competition');
    }

    public static function validate($input)
    {

        $rules = array(
            'mallID' => 'sometimes',
            'recipient' => 'required|email',
            'subject' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'image1' => 'mimes:jpeg,jpg,png,gif',
            'TsandCsFile' => 'mimes:pdf,doc'
        );

        return Validator::make($input, $rules);
    }

    public static function validateAdmin($input)
    {

        $rules = array(
            'mallID' => 'required',
            'recipient' => 'required|email',
            'subject' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'TsandCsFile' => 'mimes:pdf,doc'
        );

        return Validator::make($input, $rules);
    }

    public static function validateComp($input, $fields)
    {
        $finRule = array();
        //dd($input);
        $messages = array(
            'required' => 'Field is required!',
            'email' => 'Please enter a valid e-mail address!',
            'mimes' => 'Please upload a valid file type!'
        );

        foreach ($fields as $field) {
            $rule = array();
            if ($field->mandatory == 'Y') {
                $rule[] = 'required';
            }
            if ($field->emailField == 'Y') {
                $rule[] = 'email';
            }
            if ($field->formFieldSpecs->fileTypes != '' & $field->formFieldSpecs->fileTypes != NULL) {
                $rule[] = 'mimes:' . $field->formFieldSpecs->fileTypes;
            }

            $name = "fieldID" . $field->formFieldID;
            $finRule[$name] = implode($rule, '|');
        }
        //dd($finRule);
        return Validator::make($input, $finRule, $messages);
    }

    public function formFields()
    {
        return $this->hasMany('App\FormField', 'formID')->orderBy("contentOrder");
    }

    public function formFeedback()
    {
        return $this->hasMany('App\FormFeedback', 'formID')->orderBy("dateAdded", "desc");
    }

    public static function fieldTypes($hasFields)
    {
        $fieldTypes = array(
            'text' => 'text',
            'textarea' => 'textarea',
            'radio' => 'radio button',
            'checkbox' => 'checkbox',
            'select' => 'drop down',
            'file' => 'file upload'
        );

        if ($hasFields === 1) {
            $fieldTypes['dob'] = 'date of birth';
        }
        return $fieldTypes;
    }

    public static function fileTypes()
    {
        $fieldTypes = array(
            'jpeg' => '.jpg',
            'png' => '.png',
            'bmp' => '.bmp',
            'gif' => '.gif',
            'pdf' => '.pdf',
            'doc' => '.doc',
            'docx' => '.docx',
            'txt' => '.txt',
            'csv' => '.csv',
            'ppt' => '.pptx'
        );
        return $fieldTypes;
    }

    public static function defaultFields()
    {
        $fields = array();
        $fields[] = MGForm::nameField();
        $fields[] = MGForm::surnameField();
        $fields[] = MGForm::emailField();
        $fields[] = MGForm::cellField();
        $fields[] = MGForm::tellField();

        return $fields;
    }

    public static function defaultField()
    {

        $field = array(
            'function' => 'emailField',
            'name' => '',
            'mandatory' => 'N',
            'emailField' => 'X',
            'contentOrder' => '1',
            'typeOfField' => 'text',
            'size' => 200,
//                'defaultValue' => '',
        );
        return $field;
    }

    public static function emailField()
    {

        $field = array(
            'function' => 'emailField',
            'name' => 'E-mail address',
            'mandatory' => 'Y',
            'emailField' => 'Y',
            'contentOrder' => '3',
            'typeOfField' => 'text',
            'size' => 200,
            'defaultValue' => '',
        );
        return $field;
    }

    public static function nameField()
    {
        $field = array(
            'function' => 'nameField',
            'name' => 'Name',
            'mandatory' => 'Y',
            'emailField' => 'X',
            'contentOrder' => '1',
            'typeOfField' => 'text',
            'size' => 200,
            'defaultValue' => '',
        );
        return $field;
    }

    public static function surnameField()
    {
        $field = array(
            'function' => 'surnameField',
            'name' => 'Surname',
            'mandatory' => 'Y',
            'emailField' => 'X',
            'contentOrder' => '2',
            'typeOfField' => 'text',
            'size' => 200,
            'defaultValue' => '',
        );
        return $field;
    }

    public static function cellField()
    {
        $field = array(
            'function' => 'cellField',
            'name' => 'Cell',
            'mandatory' => 'N',
            'emailField' => 'X',
            'contentOrder' => '4',
            'typeOfField' => 'text',
            'size' => 200,
            'defaultValue' => '',
        );
        return $field;
    }

    public static function tellField()
    {
        $field = array(
            'function' => 'tellField',
            'name' => 'Telephone',
            'mandatory' => 'N',
            'emailField' => 'X',
            'contentOrder' => '5',
            'typeOfField' => 'text',
            'size' => 200,
            'defaultValue' => '',
        );
        return $field;
    }

    public function booking()
    {
        return $this->hasMany('BookingSchedule', 'formID');
    }
}
