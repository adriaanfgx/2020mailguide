<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{
    protected $table = 'forms';
    protected $primaryKey = 'formID';

    public function formRequest(){
        return $this->hasOne('App\FormRequest','formID','formID');
    }

    public function scopeActive($query) {
        $now = Carbon::now()->format('Y-m-d');

        return $query->where('activated', 'Y')
            ->where('startDate', '<=', $now)
            ->where('endDate', '>=', $now);
    }

    public function formFeedback() {
        return $this->hasMany('App\FormFeedback','formID')->orderBy('dateAdded','desc');
    }
}