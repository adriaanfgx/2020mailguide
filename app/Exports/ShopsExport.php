<?php

namespace App\Exports;


use Maatwebsite\Excel\Concerns\FromArray;

class ShopsExport implements FromArray
{
    protected $shops;

    public function __construct(array $shops)
    {
        $this->shops = $shops;
    }

    public function array(): array
    {
        return $this->shops;
    }
}
