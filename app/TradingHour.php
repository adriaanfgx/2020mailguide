<?php
/**
 * Created by JetBrains PhpStorm.
 * User: likho
 * Date: 2014/09/30
 * Time: 4:36 PM
 * To change this template use File | Settings | File Templates.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradingHour extends Model
{

    protected $table = 'tradingHours';

    public $timestamps = false;

    protected $primaryKey = 'hoursID';

    protected $guarded = array('hoursID');

    public function mall()
    {

        return $this->belongsTo('App\ShoppingMall');
    }
}
