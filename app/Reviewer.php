<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reviewer extends Model
{
    protected $table = 'reviewer';
    protected $connection = "mysql-finemov";
    public $timestamps = false;
    protected $primaryKey = 'reviewerID';

    public function reviews()
    {
        return $this->hasMany('App\Review');
    }
}
