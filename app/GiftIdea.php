<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class GiftIdea extends Model
{
    protected $table = 'giftIdeas';
    protected $primaryKey = 'giftIdeaID';
    protected $guarded = array('giftIdeaID');

    public static function validate($input)
    {
        $rules = array(

            'title' => 'required',
            'category' => 'required',
            'description' => 'required',
            'image1' => 'mimes:jpeg,png,gif|image'
        );

        return Validator::make($input, $rules);
    }
}
