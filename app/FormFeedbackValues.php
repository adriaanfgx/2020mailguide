<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormFeedbackValues extends Model
{
    protected $table = 'formFeedbackValues';

    public $timestamps = false;

    protected $primaryKey = 'feedbackValID';

    protected $guarded = array('feedbackValID');
}
