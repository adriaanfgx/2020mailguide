<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookedBy extends Model
{

    protected $table = 'bookedBy';
    public $timestamps = false;
    protected $primaryKey = 'bookedByID';
    protected $guarded = array('bookedByID');
    protected $fillable = array('bookingScheduleID', 'startDate', 'endDate', 'dateBooked', 'approved', 'dateApproved', 'bookingWhat', 'bannerAdID', 'adZoneID', 'mallID', 'shopMGID', 'promotionsMGID', 'formID', 'eventsMGID', 'exhibitionID', 'vacancyMGID', 'jobID', 'featured', 'userID', 'justOpenedID', 'surveyListingID', 'creditsID', 'creditsTempID', 'giftIdeaID', 'adScheduleList', 'newsletterID', 'newsletterPosition', 'bookingWhatBanner', 'premierID', 'poNumber', 'bookingAccountID');

    public function bookedBy()
    {
        return $this->belongsTo('App\BookingSchedule', 'bookingScheduleID');
    }

    public function account()
    {
        return $this->belongsTo('App\BookingAccount');
    }

    public function creditTemp()
    {

        return $this->belongsTo('CreditTemp', 'creditsTempID');
    }

}
