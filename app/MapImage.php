<?php


namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;


class MapImage extends Model
{
    protected $table = 'mapImage';
    public $timestamps = false;
    protected $primaryKey = 'mapImageMGID';
    protected $guarded = array('mapImageMGID');

    public static function validate( $input ){

        $rules = array(
            'location' => 'required',
            'imageSrc' => 'sometimes|required|mimes:jpeg,png,gif|image'
        );

        return Validator::make( $input, $rules );
    }

    public function mall(){

        return $this->belongsTo('App\ShoppingMall', 'mallID');
    }

}
