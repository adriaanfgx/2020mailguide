<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class MGEvent extends Model
{
    protected $table = 'events';
    protected $primaryKey = 'eventsMGID';
    protected $guarded = ['eventsMGID'];
    public $timestamps = false;

    public static function validate( $input ){

        $rules = array(
            'name' => 'required',
            'startDate' => 'required|date',
            'endDate' => 'required|date',
            'thumbnail1' => 'mimes:jpeg,png,gif|image',
            'altImage1' => 'mimes:jpeg,png,gif|image',
            'image1' => 'mimes:jpeg,png,gif|image',
            'altImage2' => 'mimes:jpeg,png,gif|image',
            'altImage3' => 'mimes:jpeg,png,gif|image',
            'fileName' => 'sometimes|mimes:pdf,doc',
        );

        return Validator::make( $input, $rules );
    }

    public function scopeCurrent($query)
    {
        return $query->where('endDate', '>=', date('Y-m-d'));
    }

    public function scopeDisplay($query)
    {
        return $query->where('display', '=', 'Y');
    }
}
