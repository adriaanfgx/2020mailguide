<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class MGCrud extends Model
{

    /****************************************/
    /*Shop Functions
    /****************************************/
    public static function DeleteShop($id)
    {
        Shop::find($id)->delete();
    }

    public static function UpdateUserProfile($user)
    {

        $user->first_name = Input::get('first_name');
        $user->last_name = Input::get('last_name');
        $user->company = Input::get('company');
        $user->vat_number = Input::get('vat_number');
        $user->postal_address = Input::get('postal_address');
        $user->postal_code = Input::get('postal_code');
        $user->province = Input::get('province');
        $user->city = Input::get('city');
        $user->suburb = Input::get('suburb');
        $user->email = Input::get('email');
        $user->tel_work = Input::get('tel_work');
        $user->tel_home = Input::get('tel_home');
        $user->fax = Input::get('fax');
        $user->cell = Input::get('cell');
        $user->birth_date = Input::get('birth_date');
        $user->gender = Input::get('gender');

        $user->save();
    }

    /**
     * Update User password
     * @param $user
     * @return bool
     */
    public static function UpdateUserPassword($user)
    {

        $passwordMatch = true;
        if (Hash::check(Input::get('old_pass'), $user->password)) {

            $user->password = Input::get('new_pass');
            $user->save();
            $passwordMatch;
        } else {
            return false;
        }
    }

    /**
     * Update Gift Idea
     */
    public static function UpdateGiftIdea($id)
    {

        $giftIdea = GiftIdea::find($id);
        $data['title'] = Input::get('title');
        $data['manufacturer'] = Input::get('manufacturer');
        $data['category'] = Input::get('category');
        $data['price'] = Input::get('price');
        $data['forWho'] = Input::get('forWho');
        $data['description'] = Input::get('description');
        $data['display'] = Input::get('display');

        $thumbMaxWidth = 150;
        $largeMaxWidth = 450;

        $file1 = Input::file('image1');

        if (isset($file1)) {

            $destinationPath = Config::get('image_upload_path');
            $img1 = uploadImage($file1, $destinationPath, $thumbMaxWidth, $largeMaxWidth, $large = true);
            $data['thumbnail1'] = $img1['thumbnail'];
            $data['image1'] = $img1['largeImage'];
        }

        GiftIdea::find($id)->update($data);

    }

    /**
     * Send SMS Messages
     * @param $params
     * @param $url
     * @return mixed
     */
    public static function SendCurlRequest($params, $url)
    {

        $curlHandler = curl_init();

        curl_setopt($curlHandler, CURLOPT_URL, 'http://api.communicationignition.co.za/api/1.0.0/' . $url);
        curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curlHandler, CURLOPT_USERPWD, 'mallguide:f1negraf1x');
        curl_setopt($curlHandler, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandler, CURLOPT_POST, true);
        curl_setopt($curlHandler, CURLOPT_POSTFIELDS, json_encode($params));

        $response = json_decode(curl_exec($curlHandler));
        $info = curl_getinfo($curlHandler);
        curl_close($curlHandler);


        return $response;

    }

    /**
     * Verify Phone numbers provided by the user
     */
    public static function verifyPhoneNumber($param, $url)
    {

        $curlHandler = curl_init();

//        $params = array();
        $params['batch'] = $param; //required

        curl_setopt($curlHandler, CURLOPT_URL, 'http://api.communicationignition.co.za/api/1.0.0/' . $url);
        curl_setopt($curlHandler, CURLOPT_FRESH_CONNECT, TRUE); //prevent caching
        curl_setopt($curlHandler, CURLOPT_HTTPAUTH, CURLAUTH_BASIC); //basic auth sent in header
        curl_setopt($curlHandler, CURLOPT_USERPWD, 'mallguide:f1negraf1x'); //auth credentials
        curl_setopt($curlHandler, CURLOPT_HTTPHEADER, array('Content-Type: application/json')); //header type MUST be JSON
        curl_setopt($curlHandler, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlHandler, CURLOPT_POST, true);
        curl_setopt($curlHandler, CURLOPT_POSTFIELDS, json_encode($params)); //POST fields MUST be JSON formatted

        $response = json_decode(curl_exec($curlHandler));

//        print_r($response);exit();
//        var_dump(curl_exec($curlHandler));
        if (isset($response->invalidNumbers)) {
            return $response->invalidNumbers;
        }
//        print_r($response->invalidNumbers);exit();
        $info = curl_getinfo($curlHandler);
        curl_close($curlHandler);

        if ($info['http_code'] == 200) {
            var_dump($response->status);
            var_dump($response->time);
            var_dump($response->message);

            if ($response->status == 'success')
                var_dump($response->invalidNumbers);
        }

    }

    /**
     * Generate password - helper function
     * From http://www.phpscribble.com/i4xzZu/Generate-random-passwords-of-given-length-and-strength
     *
     */
    public static function generatePassword($length = 6)
    {
        $vowels = 'aeiou';
        $consonants = '23456789';

        $password = '';
        $alt = time() % 2;
        for ($i = 0; $i < $length; $i++) {
            if ($alt == 1) {
                $password .= $consonants[(rand() % strlen($consonants))];
                $alt = 0;
            } else {
                $password .= $vowels[(rand() % strlen($vowels))];
                $alt = 1;
            }
        }
        return $password;
    }

    /**
     * Get SMS Reports
     */
//    public static function SMSReportCurlRequest( ){
//
//    }
}
