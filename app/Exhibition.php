<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Exhibition extends Model
{
    protected $primaryKey = 'exhibitionID';

    protected $guarded = array('exhibitionID');

    public $timestamps = false;

    public static function validate($input)
    {

        $rules = array(
            'name' => 'required',
            'startDate' => 'required|date',
            'endDate' => 'required|date',
            'thumbnail1' => 'mimes:gif,jpeg,png',
            'image1' => 'mimes:gif,jpeg,png',
            'altImage1' => 'mimes:gif,jpeg,png',
            'altImage2' => 'mimes:gif,jpeg,png',
            'altImage3' => 'mimes:gif,jpeg,png'
        );

        return Validator::make($input, $rules);
    }

    public function mall()
    {
        return $this->belongsTo('App\ShoppingMall');
    }

    public function scopeCurrent($query)
    {
        return $query->where('endDate', '>=', Carbon::now()->toDateString())->where('display', '=', 'Y');
    }
}
