<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\ShoppingMallDetail;
use Illuminate\Support\Facades\Validator;

class ShoppingMall extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'shoppingMall';

    public $timestamps = false;

    protected $primaryKey = 'mallID';

    protected $guarded = array('mallID', 'startHoursMon', 'endHoursMon', 'startHoursTues', 'endHoursTues', 'startHoursWed', 'endHoursWed', 'startHoursThurs', 'endHoursThurs', 'startHoursFri', 'endHoursFri', 'startHoursSat', 'endHoursSat', 'startHoursSun', 'endHoursSun', 'startHoursPublicHolidays', 'endHoursPublicHolidays');

    public static function validate($input)
    {

        $rules = array(
            'name' => 'required',
            'marketingManagerEmail' => 'email',
            'marketingAssistantEmail' => 'email',
            'leasingEmail' => 'email',
            'centreManagerEmail' => 'email',
            'logo' => 'mimes:jpeg,png,gif|image',
            'image1' => 'mimes:jpeg,png,gif|image',
            'image2' => 'mimes:jpeg,png,gif|image',
            'image3' => 'mimes:jpeg,png,gif|image',
            'image4' => 'mimes:jpeg,png,gif|image',
            'image5' => 'mimes:jpeg,png,gif|image',
            'image6' => 'mimes:jpeg,png,gif|image',
            'image7' => 'mimes:jpeg,png,gif|image',
            'image8' => 'mimes:jpeg,png,gif|image',
            'image9' => 'mimes:jpeg,png,gif|image',
            'image10' => 'mimes:jpeg,png,gif|image',
            'website' => [
                function ($attribute, $value, $fail) {
                    if (!preg_match('/((http|https)\:\/\/)?[a-zA-Z0-9\.\/\?\:@\-_=#]+\.([a-zA-Z0-9\.\/\?\:@\-_=#])*/', $value)) {
                        $fail($attribute . ' is invalid.');
                    }
                }
            ]
        );

        return Validator::make($input, $rules);
    }

    public function details()
    {
        return $this->hasOne('App\ShoppingMallDetail', 'mallID');
    }

    public function tradinghour()
    {
        return $this->hasOne('App\TradingHour', 'mallID');
    }

    public function shops()
    {
        return $this->hasMany('App\Shop', 'mallID', 'mallID')->orderBy('name');
    }

    /**
     * @param $mallID
     * @return int
     */
    public static function hasFieldAccess($mallID = null)
    {
        $mallsAccess = [828 => 828, 838 => 838, 1150 => 1150, 1133 => 1133, 218 => 218, 1134 => 1134, 1939 => 1939];
        if (isset($mallsAccess[$mallID])) {
            return 1;
        } else {
            return 0;
        }
    }

    public function scopeDisplay($query)
    {
        return $query->where('mallguideDisplay', '=', 'Y');
    }

    public function mallCity()
    {
        return $this->hasOne('App\City', 'id', 'city_id');
    }

    public function curEvents()
    {
        return $this->hasMany('App\MGEvent', 'mallID')
            ->where('display', '=', 'Y')
            ->where('endDate', '>=', Carbon::now()
                ->toDateTimeString());
    }

    public function curJobs()
    {
        return $this->hasMany('App\Job', 'mallID')->where('endDate', '>=', Carbon::now()->toDateTimeString())->where('display', '=', 'Y');
    }

    public function mallProvince()
    {

        return $this->belongsTo('App\Province', 'province_id');
    }

    public function promotions()
    {
        return $this->hasMany('App\Promotion', 'mallID');
    }

    public function curPromotions()
    {
        return $this->hasMany('App\Promotion', 'mallID')->where('endDate', '>=', Carbon::now()->toDateTimeString());
    }

    public function curCompetitions()
    {
        return $this->belongsToMany('App\MGForm', 'formRequest', 'mallID', 'formID')->withPivot('userID', 'shopMGID', 'dateAdded', 'display')->where('endDate', '>=', Carbon::now()->toDateTimeString());
    }

    public function formRequests()
    {
        return $this->hasMany('App\FormRequest', 'mallID');
    }

    public function curExhibitions()
    {
        return $this->hasMany('App\Exhibition', 'mallID')->where('endDate', '>=', Carbon::now()->toDateTimeString());
    }

    public function exhibitions()
    {
        return $this->hasMany('App\Exhibition', 'mallID');
    }

    public function events()
    {
        return $this->hasMany('App\MGEvent', 'mallID')->orderBy('eventsMGID', 'desc');
    }

    /**
     *Mall Association
     */
    public function user()
    {
        return $this->belongsToMany('App\User', 'user_malls', 'mall_id', 'user_id');
    }
}
