<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminUser extends Model
{
    protected $table = 'adminUsers';

    protected $primaryKey = 'userID';

    public $timestamps = false;

    public function user()
    {
        return $this->hasOne('App\User', 'userID');
    }
}
