<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Promotion extends Model
{
    protected $table = 'promotions';
    public $timestamps = false;
    protected $primaryKey = 'promotionsMGID';
    protected $guarded = ['promotionsMGID'];

    public function scopeCurrent($query)
    {
        return $query->where('endDate', '>=', date('Y-m-d'));
    }

    public function mall()
    {
        return $this->belongsTo('App\ShoppingMall', 'mallID');
    }

    public function shop()
    {
        return $this->belongsTo('App\Shop', 'shopMGID');
    }

    public function scopePast($query)
    {
        return $query->where('endDate', '<', date('Y-m-d'));
    }

    public static function validate($input)
    {

        $rules = array(
            'mallID' => 'sometimes|required',
            'startDate' => 'required',
            'endDate' => 'required',
            'title' => 'required',
            'promotion' => 'required',
            'image1' => 'mimes:jpeg,png,gif|image',
            'image2' => 'mimes:jpeg,png,gif|image'
        );

        return Validator::make($input, $rules);
    }

    public function booking()
    {
        return $this->hasMany('App\BookingSchedule', 'promotionsMGID');
    }
}
