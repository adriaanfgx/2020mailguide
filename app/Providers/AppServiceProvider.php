<?php

namespace App\Providers;

use App\Movie;
use App\Province;
use App\ShoppingMall;
use App\WhatsShowing;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        $response = callApi('countries');
        $decoded = json_decode($response);
        $expiresAt = Carbon::now()->addDay(1);
        $country_id = Cookie::get('country_id', 190);

        $countries = [];
        foreach ($decoded as $key => $value) {
            $countries[] = [
                'id' => $key,
                'name' => $value
            ];

        }

        $banner = rand(0, 6);

        $movies = Cache::remember('currentMovies', $expiresAt, function () {
            return Movie::with('reviews.reviewer')->whereHas('showing', function ($q) {
                $q->where('endDate', '>=', Carbon::now()->toDateTimeString());
            })
                ->where('description', '!=', '')->orderBy('name', 'ASC')
                ->distinct()
                ->select('name', 'description', 'movieID', 'bigImage')
                ->get();
        });

        $provinces = Cache::remember('provinces_' . $country_id, $expiresAt, function () use ($country_id) {
            $result = array();
            $prov = Province::where('country_id', '=', $country_id)->orderBy('name')->pluck('name', 'id');

            if (count($prov) >= 1) {
                $result = $prov;
            }
            return $result;
        });

        if (\request()->is('admin/*')) {
            $malls = ShoppingMall::orderBy('name')->pluck('name', 'mallID');

        } else {
            $malls = Cache::remember('allMalls_' . $country_id, $expiresAt, function () use ($country_id) {
                return ShoppingMall::where('country_id', '=', $country_id)
                    ->where('mallguideDisplay', '=', 'Y')
                    ->where('activate', '=', 'Y')
                    ->orderBy('name')
                    ->pluck('name', 'mallID');
            });
        }

        $whatsShowingGL = Cache::remember('whatsShowingGL', $expiresAt, function () {
            return WhatsShowing::select('mallID')->where('endDate', '>=', Carbon::now()->toDateTimeString())->groupBy('mallID')->pluck('mallID');
        });

        View::share('countries', $countries);
        View::share('banner', $banner);
        View::share('movies', $movies);
        View::share('provinces', $provinces);
        View::share('malls', $malls);
        View::share('whatsShowingGL', $whatsShowingGL);
    }
}
