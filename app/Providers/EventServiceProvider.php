<?php

namespace App\Providers;

use App\MallVisit;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen('mall.view', function ($mallID) {
            $mallListed = MallVisit::where('mall_id','=',$mallID)->get();

            if( sizeof($mallListed) > 0 ){
                MallVisit::where('mall_id','=',$mallID)->increment('visits');
            }else{
                MallVisit::insert(array('mall_id'=>$mallID,'updated_at'=>date("Y-m-d H:i:s")));
            }

        });
    }
}
