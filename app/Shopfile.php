<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shopfile extends Model
{
    protected $table = 'shopfiles';

    public function shop()
    {
        return $this->belongsTo('App\Shop', 'shop_id');
    }
}
