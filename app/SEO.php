<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SEO extends Model
{
    protected $table = 'seo';
    protected $primaryKey = 'id';
    protected $guarded = array('id');

    public static function validate( $input ){

        $rules = array (
            'route' =>'required',
            'title' => 'required'
        );

        return Validator::make( $input, $rules );
    }
}
