<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credit extends Model
{
    protected $table = 'credits';
//    protected $connection = "mysql-fineclient";
    public $timestamps = false;
    protected $primaryKey = 'creditsID';
    protected $fillable = array('creditsType', 'numCredits', 'mallID', 'lastUsed', 'lastAdded');

}
