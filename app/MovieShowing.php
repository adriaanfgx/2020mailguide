<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieShowing extends Model
{
    protected $table = 'whatsShowingMovies';
    protected $connection = "mysql-finemov";
    public $timestamps = false;
    protected $primaryKey = 'whatMovieID';

    /*public function moviesshowing(){

        return $this->hasMany('MovieShowing','whatsShowingID');
    }*/

}
