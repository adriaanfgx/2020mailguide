<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class BookingSchedule extends Model
{
    protected $table = 'bookingSchedule';
    public $timestamps = false;
    protected $primaryKey = 'bookingScheduleID';
    protected $guarded = array('bookingScheduleID');

    public function mall()
    {
        return $this->belongsTo('App\ShoppingMall', 'mallID');
    }

    public function shop()
    {
        return $this->belongsTo('App\Shop', 'shopMGID');
    }

    public function bookedBy()
    {
        return $this->hasOne('App\BookedBy', 'bookingScheduleID');
    }

    public function account()
    {
        return $this->belongsTo('App\BookingAccount', 'bookingAccountID');
    }

    public static function validate($input)
    {

        $rules = array(
            'bookingWhat' => 'required',
            'mallID' => 'sometimes|required',
            'startDate' => 'required',
            'endDate' => 'required'
        );

        return Validator::make($input, $rules);
    }

    public function competition()
    {
        return $this->belongsTo('App\MGForm', 'formID');
    }

    public function event()
    {
        return $this->belongsTo('App\MGEvent', 'eventsMGID');
    }

    public function promotion()
    {
        return $this->belongsTo('App\Promotion', 'promotionsMGID');
    }
}
