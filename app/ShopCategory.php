<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopCategory extends Model
{
    protected $table = 'shop_categories';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];

    public function shops(){
        return $this->hasMany('App\Shop','category_id');
    }
}
