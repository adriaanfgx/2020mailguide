<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompetitionWinners extends Model
{
    protected $table = 'competitionWinners';

    public $timestamps = false;

    protected $primaryKey = 'compWinnerID';

    protected $guarded = array('compWinnerID');
}
