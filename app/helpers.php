<?php


use App\ShoppingMall;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\FileSystem\Filesystem;
use Illuminate\Http\Request;

if (!function_exists('callApi')) {
    function callApi($path)
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', \config('app.api_url') . $path);

        return $response->getBody();
    }
}


if (!function_exists('uploadImage')) {

    /**
     * @param $inputFile
     * @param $destinationPath
     * @param $thumbnailWidth
     * @param $thumbnail
     * @param $largeImage
     * @return mixed of the added image names
     * @access public
     * Move the file sent trough in the first param to the path sent trough in the second param
     * then also uploads a thumbnail accourding to the thumbnail width sent trough in param 4 if param 5 is set.
     * Also renames and reuploads the file in param one if last param is set
     */
    function uploadImage($inputFile, $destinationPath, $thumbnailWidth = null, $thumbnail = null, $largeImage = null)
    {

        $returnData = array();

        $uploadDate = date("YmdHis");
        $file = $inputFile->getClientOriginalName();
        $extension = $inputFile->getClientOriginalExtension();
        $inputFile->move($destinationPath, $file);
        $originalExtensions = ['pdf', 'doc'];

        //Get the file's original name
        $info = pathinfo($file);
        $file_name = str_replace(" ", "_", $info['filename']);


        if (isset($thumbnail)) {
            Image::make($destinationPath . '/' . $file)->resize($thumbnailWidth, null, function ($constraint) {
                $constraint->aspectRatio();
            })->save($destinationPath . '/' . $file_name . '_' . $uploadDate . '_thumbnail.' . $extension);
            $returnData['thumbnail'] = $file_name . '_' . $uploadDate . '_thumbnail.' . $extension;
        }

        if (isset($largeImage)) {
            Image::make($destinationPath . '/' . $file)->save($destinationPath . '/' . $file_name . '_' . $uploadDate . '_large.' . $extension);
            $returnData['largeImage'] = $file_name . '_' . $uploadDate . '_large.' . $extension;
        } else {
            $returnData['largeImage'] = $info['basename'];
        }

        if (!isset($thumbnailWidth) && !in_array($inputFile->getClientOriginalExtension(), $originalExtensions)) {
            Image::make($destinationPath . '/' . $file)->save($destinationPath . '/' . $file_name . '_' . $uploadDate . '.' . $extension);
            $returnData['unresized'] = $file_name . '_' . $uploadDate . '.' . $extension;
        } else {
            if (!in_array($inputFile->getClientOriginalExtension(), $originalExtensions)) {
                $new_thumb = Image::make($destinationPath . '/' . $file)
                    ->resize($thumbnailWidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->save($destinationPath . '/' . $file_name . '_' . $uploadDate . '_thumbnail.' . $extension);
                $returnData['thumbnail'] = $new_thumb->basename;
            }
        }

        return $returnData;
    }
}

if (!function_exists('slugify')) {
    function slugify($str)
    {

        return Str::slug($str);
    }
}

if (!function_exists('initSEO')) {
    function initSEO($parms = array())
    {
        $uri = \request()->path();
        $seo_title = $parms['title'];
        $seo_description = '';
        $seo_keywords = '';

        $seo = \App\SEO::where('route', (string)$uri)->first();

        if (!empty($seo)) {

            $seo_title = $seo->title;
            $seo_description = $seo->description;
            $seo_keywords = $seo->keywords;

        } else {
            if (isset($parms['description'])) {
                $seo_description = $parms['description'];
            }

            if (isset($parms['title'])) {
                $seo_title = $parms['title'];
            }

            if (isset($parms['keywords'])) {
                $seo_title = $parms['keywords'];
            }
        }

        View::share(
            array(
                'seo_title' => $seo_title,
                'seo_description' => $seo_description,
                'seo_keywords' => $seo_keywords
            )
        );
    }
}

if (!function_exists('generateBreadcrumbs')) {
    function generateBreadcrumbs()
    {
        $controller = \request()->segment(1);
        $action = \request()->segment(2);
        $variable = \request()->segment(3);

        $html = '<ul class="breadcrumb"><li><a href="/">HOME</a></li>';
        $html .= '<li><span class="divider">/</span><a href="/' . $controller . '">' . $controller . '</a></li>';
        if (!empty($action)) {
            $html .= '<li class="active"><span class="divider">/</span>' . $action . '</li>';
        }
        if (!empty($variable)) {

            if (is_numeric(intval($variable))) {
                $html .= '<li class="active"><span class="divider">/</span>VIEW EVENT</li>';
            } else {
                $html .= '<li class="active"><span class="divider">/</span>' . $variable . '</li>';
            }

        }
        $html .= '</ul>';

        return '';
    }
}

if (!function_exists('removeOldImage')) {
    function removeOldImage($fileLocation, $filename)
    {
        if (isset($filename)) {
            if (File::isFile($fileLocation . '/' . $filename)) {
                File::delete($fileLocation . '/' . $filename);
            }
        }
    }
}

if (!function_exists('getMoveList')) {
    function getMoveList()
    {

        $moves = array
        (
            "" => "Move Field",
            "top" => "move to top",
            "up" => "move up one",
            "down" => "move down one ",
            "bottom" => "move to bottom",
        );

        return $moves;
    }
}

if (!function_exists('getQueryStringParam')) {
    function getQueryStringParam($param, $min_value = null, $max_value = null)
    {
        if (\request()->has($param)) {
            $value = \request()->get($param);

            if (!is_null($min_value) && !is_null($max_value)) {
                if ($value < $min_value) $value = $min_value;
                if ($value > $max_value) $value = $max_value;
            }

            return $value;
        }

        return FALSE;
    }
}

if (!function_exists('pageTitle')) {
    function pageTitle($parts, $separator = ' | ')
    {
        if (is_array($parts) && count($parts)) {
            $title = '';
            $total_parts = count($parts);
            foreach ($parts as $idx => $part) {
                $title .= $part;
                if ($idx < ($total_parts - 1)) {
                    $title .= $separator;
                }

            }

            return $title;
        }

        return FALSE;
    }
}

if (!function_exists('metaDescription')) {
    function metaDescription($str, $length = 160)
    {

        return substr(strip_tags($str), 0, $length) . '...';
    }
}
if (!function_exists('removeURLProtocol')) {
    function removeURLProtocol($url)
    {
        if (strpos($url, 'http') !== FALSE) {
            $url = str_replace('https', '', $url);
            $url = str_replace('http', '', $url);
            $url = str_replace('://', '', $url);
            $url = str_replace('//', '', $url);
            $url = str_replace(' ', '', $url);
        }
        return $url;
    }
}

if (!function_exists('storeSelectedDropdownMallInSession')) {
    function storeSelectedDropdownMallInSession($mallID)
    {
        session()->put('selected_mall', $mallID);
    }
}


/**
 * @access public
 * Get a list of malls for selected country
 * @param integer $provinceID
 * @return string
 */
if (!function_exists('activeCountryMalls')) {
    function activeCountryMalls($provinceID = 0)
    {
        $country_id = Cookie::get('country_id', 190);

        if ($country_id) {
            if (Cache::has('allMalls_' . $country_id)) {
                if ($provinceID > 0) {
                    return ShoppingMall::where('province_id', $provinceID)->pluck('name', 'mallID');
                }
                return Cache::get('allMalls_' . $country_id);
            }
        }

        return [];
    }
}

/**
 * Strip tags and its content
 *
 * @param string $text
 * @param string $tags
 * @param boolean $invert
 * @return string
 */
if (!function_exists('stripTagsContent')) {
    function stripTagsContent($text, $tags = '', $invert = FALSE)
    {

        preg_match_all('/<(.+?)[\s]*\/?[\s]*>/si', trim($tags), $tags);
        $tags = array_unique($tags[1]);

        if (is_array($tags) AND count($tags) > 0) {
            if ($invert == FALSE) {
                return preg_replace('@<(?!(?:' . implode('|', $tags) . ')\b)(\w+)\b.*?>.*?</\1>@si', '', $text);
            } else {
                return preg_replace('@<(' . implode('|', $tags) . ')\b.*?>.*?</\1>@si', '', $text);
            }
        } elseif ($invert == FALSE) {
            return preg_replace('@<(\w+)\b.*?>.*?</\1>@si', '', $text);
        }
        return $text;
    }
}

/**
 * @access public
 * Get a list of malls for selected country
 * @return string
 */
if (!function_exists('activeProvinces')) {
    function activeProvinces()
    {
        $country_id = Cookie::get('country_id', 190);
        if ($country_id) {
            if (Cache::has('provinces_' . $country_id)) {
                return Cache::get('provinces_' . $country_id);
            }
        }

        return array();
    }
}

if (!function_exists('displayText')) {
    function displayText($string)
    {
        if ($string == 'Y') {
            return 'Yes';
        } elseif ($string == 'N') {
            return 'No';
        } elseif ($string == 'C') {
            return 'Cancelled';
        }
    }
}

if (!function_exists('displayAsText')) {
    function displayAsText($int)
    {
        if ($int == 1) {
            return 'Yes';
        } else {
            return 'No';
        }
    }
}

/**
 * @access public
 * Get a list of movies showing at a mall from Movies API
 * @return string
 */
if (!function_exists('getMallAPIMovies')) {
    function getMallAPIMovies($mallID, $filter_date = null)
    {
        $mall_movies = array();
        $show_dates = array();
        $client = new Client();
        if (is_null($filter_date)) $filter_date = date('Y-m-d');

        if ($response = $client->get('http://movies.mallguide.global/api/v1/whats-showing?mallID=' . $mallID . '&endDate=' . $filter_date . '&operator[endDate]=gte&with=movies',
            ['headers' => [
                'token' => Config::get('movie.token'),
                'clientId' => Config::get('movie.client_id')
            ]]
        )) {
            try {
                $resp = $response->getBody(true);
                if ($movie_info = json_decode($resp)) {
                    if ($movie_info->error == false) {
//                        $whatshowing = $movie_info->data;
                        if (count($movie_info->data)) {
                            foreach ($movie_info->data as $datum) {
                                $movies = $datum->movies;
                                if (count($movies)) {
                                    foreach ($movies as $movie) {
                                        $mall_movies[$movie->movieID] = $movie;

                                        //$mall_movies[$movie->movieID]->showtimes[$movie->pivot->whatsShowingID] = new stdClass();
                                        $show_dates[$movie->movieID][] = $datum->startDate;
                                        $show_dates[$movie->movieID][] = $datum->endDate;

                                        $mall_movies[$movie->movieID]->showtimes = $movie->pivot->times;
                                        unset($mall_movies[$movie->movieID]->pivot);
                                    }
                                }
                            }
                        }

                        if (count($mall_movies)) {

//                            asort($mall_movies);
                            foreach ($mall_movies as $idx => $movie) {
                                if (count($show_dates[$idx])) {
                                    $start_date = min($show_dates[$idx]);
                                    $end_date = max($show_dates[$idx]);


                                    unset($show_dates[$idx]);

                                    $show_dates[$idx]['start_date'] = $start_date;
                                    $show_dates[$idx]['end_date'] = $end_date;
                                }

                            }
                        }
                    }
                }
            } catch (Guzzle\Http\Exception\BadResponseException $e) {
            }
        }

        return array('movies' => $mall_movies, 'dates' => $show_dates);
    }
}

/**
 * @param string $image
 * @param string $dir
 * @param integer $x
 * @param integer $y
 * @param integer $width
 * @param integer $height
 * @access public
 * Create a thumbnail of an image using the specified parameters
 * @return array of the added image names
 */
if (!function_exists('makeThumbnail')) {
    function makeThumbnail($image, $dir, $x, $y, $width, $height, $new_name = '')
    {
        if (file_exists($dir . '/' . $image) && is_file($dir . '/' . $image)) {
            $filepath = pathinfo($dir . '/' . $image);
            if (isset($filepath['extension'])) {
                if (!$new_name) $new_name = $filepath['filename'] . '_thumbnail.' . $filepath['extension'];
                @unlink($dir . '/' . $new_name);
                if (Image::make($dir . '/' . $image)->crop($width, $height, $x, $y)->save($dir . '/' . $new_name)) {
                    return $new_name;
                }
            }
        }

        return FALSE;
    }
}

if (!function_exists('pluralize')) {
    function pluralize($counter, $str)
    {

        if ($counter == 1) {
            return $str;
        } else {
            return str_plural($str);
        }
    }
}

if (!function_exists('taxPrice')) {
    function taxPrice($price)
    {
        $tax = ($price) * 0.14;
        $nettPrice = $tax + $price;
        return $nettPrice;
    }
}
