<?php
/**
 * Created by JetBrains PhpStorm.
 * User: likho
 * Date: 2014/10/02
 * Time: 1:59 PM
 * To change this template use File | Settings | File Templates.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{

    protected $table = 'cities';
    protected $primaryKey = 'id';
    public $timestamps = false;

    public function province()
    {

        return $this->belongsTo('App\Province');
    }

    public function malls()
    {
        return $this->hasMany('App\ShoppingMall');
    }

}
