<?php

namespace App\Http\Controllers;

use App\Exhibition;
use App\ShoppingMall;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ExhibitionsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['sentinel.auth', 'is_admin']);
    }


    public function exhibitions()
    {
        return view('admin.exhibitions.list');
    }

    /**
     * @param $mallID
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getMallExhibitions($mallId)
    {
        $data['mallId'] = $mallId;
        $data['mallName'] = ShoppingMall::find($mallId)->name;
        return view('admin.exhibitions.exhibitions', $data);
    }

    public function getAddExhibition($mallId)
    {

        $data['mall'] = ShoppingMall::find($mallId);
        $data['categories'] = Exhibition::distinct('category')->where('category', '<>', '')->orderBy('category')->get(['category', 'category']);
        $data['locations'] = Exhibition::where('mallID', '=', $mallId)
            ->distinct('location')
            ->orderBy('location')
            ->get(['location', 'location']);

        return view('admin.exhibitions.add', $data);
    }

    public function postAddExhibition($mallId, Request $request)
    {
        $form = $request->except('_token');

        $files = $request->allFiles();

        $rules = [
            'name' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'thumbnail1' => 'mimes:gif,jpeg,png',
            'image1' => 'mimes:gif,jpeg,png',
            'altImage1' => 'mimes:gif,jpeg,png',
            'altImage2' => 'mimes:gif,jpeg,png',
            'altImage3' => 'mimes:gif,jpeg,png'
        ];

        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {
            $storagePath = base_path('uploadimages/mall_' . $mallId);
            $thumbMaxWidth = 260;

            foreach ($files as $key => $file) {

                if ($key == 'thumbnail1') {
                    $image = uploadImage($file, $storagePath, $thumbMaxWidth, $thumb = true, $large = true);
                    $form['thumbnail1'] = $image['thumbnail'];
                    $form['image1'] = $image['largeImage'];
                } else {
                    $imageNumber = preg_replace('/[^0-9]/', '', $key);

                    $imageColumnName = 'altImage' . $imageNumber;

                    $image = uploadImage($file, $storagePath, $thumbMaxWidth, $thumb = true, $large = true);

                    $form[$imageColumnName] = $image['largeImage'];
                }
            }

            $form['mallID'] = $mallId;
            $form['exhibitor'] = '';

            Exhibition::insert($form);

            Session::put('success', 'The exhibition was added');
            return Redirect::route('admin.getMallExhibitions', $mallId);
        } else {
            Session::put('error', 'There are some errors, please review the form...');
            return back()->withErrors($validation)->withInput();
        }
    }

    public function getEditExhibition($exhibitionId)
    {

        $exhibition = Exhibition::find($exhibitionId);
        $data['exhibition'] = $exhibition;
        $data['categories'] = Exhibition::distinct('category')->where('category', '<>', '')->orderBy('category')->get(['category', 'category']);
        $data['locations'] = Exhibition::where('mallID', '=', $exhibition->mallID)
            ->distinct('location')
            ->orderBy('location')
            ->get(['location', 'location']);


        return view('admin.exhibitions.edit', $data);
    }

    public function postEditExhibition($exhibitionId, Request $request)
    {
        $form = $request->except('_token');

        $files = $request->allFiles();

        $rules = [
            'name' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'thumbnail1' => 'mimes:gif,jpeg,png',
            'image1' => 'mimes:gif,jpeg,png',
            'altImage1' => 'mimes:gif,jpeg,png',
            'altImage2' => 'mimes:gif,jpeg,png',
            'altImage3' => 'mimes:gif,jpeg,png'
        ];

        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {
            $exhibition = Exhibition::find($exhibitionId);

            $storagePath = base_path('uploadimages/mall_' . $exhibition->mallID);

            $thumbMaxWidth = 260;

            foreach ($files as $key => $file) {

                if ($key == 'thumbnail1') {
                    $image = uploadImage($file, $storagePath, $thumbMaxWidth, $thumb = true, $large = true);
                    $form['thumbnail1'] = $image['thumbnail'];
                    $form['image1'] = $image['largeImage'];
                } else {
                    $imageNumber = preg_replace('/[^0-9]/', '', $key);

                    $imageColumnName = 'altImage' . $imageNumber;

                    $image = uploadImage($file, $storagePath, $thumbMaxWidth, $thumb = true, $large = true);

                    $form[$imageColumnName] = $image['largeImage'];
                }
            }

            $form['mallID'] = $exhibition->mallID;
            $form['exhibitor'] = '';

            $exhibition->update($form);
            Session::flash('success', 'Exhibition updated successfully');

            return redirect('admin/exhibitions/' . $exhibition->mallID);

        } else {
            Session::put('error', 'There are some errors, please review the form...');
            return back()->withErrors($validation)->withInput();
        }
    }

    public function postDelete($id)
    {
        if (Exhibition::find($id)->delete()) {
            return 'success';
        }
    }
}
