<?php

namespace App\Http\Controllers;

use App\Job;
use App\JobApplication;
use App\JobApplicant;
use App\Shop;
use App\ShoppingMall;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class JobsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['sentinel.auth', 'is_admin']);
    }

    public function getJobs()
    {
        return view('admin.jobs.jobs');
    }

    public function getMallJobs($mallId)
    {

        $mall = ShoppingMall::find($mallId);

        $data['mall'] = $mall;

        $data['shops'] = $mall->shops;

        return view('admin.jobs.mall', $data);
    }

    public function getShopJobs($shopId)
    {
        $shop = Shop::with(
            ['jobs' => function ($q) {
                $q->orderBy("startDate", 'desc')->orderBy("endDate", 'desc');
            }
            ]
        )->with('mall')->find($shopId);

        $data['shop'] = $shop;
        $data['mall'] = $shop->mall;
        $data['jobs'] = $shop->jobs;

        return view('admin.jobs.shop', $data);
    }

    public function getJobApplicants($jobId)
    {

        $job = Job::find($jobId);

        $data['job'] = $job;

        $data['shop'] = $data['job']->shop;

        $data['mall'] = $data['job']->mall;

        $applicantID = JobApplication::where('jobID', '=', $jobId)->get(['jobApplicantID', 'jobApplicantID']);

        $data['applicants'] = JobApplicant::whereIn('jobApplicantID', $applicantID)->get();

        return view('admin.jobs.applicants', $data);
    }

    public function getJobApplicant($jobApplicantId)
    {
        $jobApplication = JobApplication::find($jobApplicantId);

        $data['job'] = $jobApplication->job;

        $data['shop'] = $jobApplication->job->shop;

        $data['mall'] = $jobApplication->job->mall;

        $data['applicant'] = JobApplicant::find($jobApplicantId);

        return view('admin.jobs.applicant', $data);
    }

    public function getAddShopJob($shopId)
    {
        $data['shop'] = Shop::find($shopId);

        $data['shopID'] = $shopId;

        $data['mall'] = ShoppingMall::find($data['shop']->mallID);

        return view('admin.jobs.add', $data);
    }

    public function postAddShopJob(Request $request, $shopId)
    {

        $form = $request->except('_token');

        $rules = array(
            'mallID' => 'sometimes',
            'title' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'description' => 'required',
            'email' => 'Between:3,64|Email'
        );

        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {
            $shop = Shop::find($shopId);

            $form['dateAdded'] = date("Y-m-d H:i:s");

            $form['shopMGID'] = $shopId;

            $form['mallID'] = $shop->mallID;

            Job::create($form);

            Session::flash('success', 'The job was added');

            return Redirect::to(route('jobs.shop.list', $shopId));
        }

        Session::flash('error', 'Please recheck the form');

        return back()->withErrors($validation)->withInput();
    }

    public function getJobEdit($jobId)
    {
        $job = Job::find($jobId);

        $data['heading'] = "Edit Job";

        $data['job'] = $job;

        $data['mall'] = $job->mall;

        $data['shop'] = $job->shop;

        $data['shopID'] = $job->shopID;

        return view('admin.jobs.edit', $data);
    }


    public function postJobEdit(Request $request, $jobId)
    {

        $form = $request->except('_token');

        $rules = array(
            'mallID' => 'sometimes',
            'title' => 'required',
            'startDate' => 'required',
            'endDate' => 'required',
            'description' => 'required',
            'email' => 'Between:3,64|Email'
        );

        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {
            $job = Job::find($jobId);

            $job->update($form);

            Session::flash('success', 'The job was updated.');

            return Redirect::to(route('jobs.shop.list', $job->shopMGID));
        }

        Session::flash('error', 'Please recheck the form');
        return back()->withErrors($validation)->withInput();
    }

    public function postJobDelete($jobId) {
        if (Job::find($jobId)->delete()) {
            return 'success';
        }
    }
}
