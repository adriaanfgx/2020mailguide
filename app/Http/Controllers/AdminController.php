<?php

namespace App\Http\Controllers;

use App\AdminUser;
use App\City;
use App\Company;
use App\FormField;
use App\FormFieldElements;
use App\MGForm;
use App\MovieMall;
use App\Province;
use App\Shop;
use App\ShoppingMall;
use App\User;
use App\UserChain;
use App\UserMall;
use App\UserShop;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware(['sentinel.auth', 'is_admin']);
    }

    /**
     * @param $provinceId
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getMalls($provinceId)
    {
        $mallList = callApi('malls/province/' . $provinceId);
        return $mallList;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUsers()
    {
        return view('admin.users.index');
    }

    /**
     * Retrieve users list from the API
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getUserList()
    {
//        $users = callApi('adminusers');

        $data['users'] = User::orderBy('created_at', 'desc')->get(['id', 'email', 'activated', 'created_at', 'admin_level', 'fgx_approved', 'status']);

        return response($data)->header('Content-Type', 'application/json');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreateUser()
    {
        $data['adminLevel'] = [
            'Master' => 'Master',
            'FGX Staff' => 'FGX Staff',
            'Mall Manager' => 'Mall Manager',
            'Marketing Manager' => 'Marketing Manager',
            'Shop Manager' => 'Shop Manager',
            'Chain Manager' => 'Chain Manager'
        ];

        $data['shops'] = Shop::orderBy('name')->pluck('name', 'shopMGID');

        $data['malls'] = ShoppingMall::orderBy('name')->where('name', '<>', '')->pluck('name', 'mallID');
        //GET LIST OF CHAINS (SHOP NAMES FOUND MORE THAN ONCE)
        $data['chains'] = Shop::orderBy('name')->groupBy('name')->havingRaw("COUNT('name') > 1")->where('display', '=', 'Y')->pluck('name', 'name');

        $data['cities'] = ShoppingMall::orderBy('city')->distinct('city')->where('city', '<>', '')->pluck('city', 'city');

        $data['companyList'] = Company::orderBy('company', 'asc')->distinct('company')->get(['company', 'company']);

        $data['provinces'] = Province::where('country_id', session()->get('country_id', 190))->pluck('name', 'id');

        return view('admin.users.create')->with($data);
    }

    /**
     * @param null $provinceID
     * @return mixed
     */
    public function getProvinceCities($provinceID = NULL)
    {
        $cities = City::where('province_id', $provinceID)->orderBy('name')->pluck('name', 'id');

        return $cities;
    }

    public function postCreateUser(Request $request)
    {
        $form = $request->except('_token');

        $currentDate = Carbon::now()->subYears(16)->toDateTimeString();

        if (Sentinel::getUser()) {
            $currentUser = Sentinel::getUser();
        }

        $rules = [
            'admin_level' => 'required',
            'email' => 'required|unique:users|min:4|max:64|email',
            'first_name' => 'required',
            'mallID' => 'sometimes',
            'password' => 'required|confirmed|regex:((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#!$%_]).{6,20})',
            'password_confirmation' => 'required',
            'birth_date' => 'nullable|date|before:' . $currentDate
        ];

        $messages = [
            'admin_level.required' => 'Please select the User type!',
            'password.regex' => 'Your password does not match the minimum requirements, please choose a stronger password!'
        ];

        if ($form['admin_level'] == "Mall Manager") {
            $rules['mallID'] = "required";
        }
        if ($form['admin_level'] == "Marketing Manager") {
            $rules['mallIDs'] = "required";
        }
        if ($form['admin_level'] == "Chain Manager") {
            $rules['chain_name'] = "required";
        }

        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {

            try {
                $newUser = Sentinel::register([
                    'email' => $form['email'],
                    'password' => $form['password'],
                ]);

                $activation = Activation::create($newUser);

                unset($form['password_confirmation']);
                $password = $form['password'];
                unset($form['password']);
                unset($form['mallSold']);
                $form['activation_code'] = $activation->getCode();
                $form['default_country_id'] = session()->get('default_country_id', 190);
                unset($form['newCentreManagement']);
                unset($form['newMarketingTeam']);
                unset($form['newMarketingOrCentre']);
                unset($form['shop_mallID']);

                if (isset($form['whichGroup'])) {
                    $movieHouse['whichGroup'] = $form['whichGroup'];
                    unset($form['whichGroup']);
                }
                if (isset($form['numCinemas'])) {
                    $movieHouse['numCinemas'] = $form['numCinemas'];
                    unset($form['numCinemas']);
                }
                if (isset($form['mallIDs'])) {
                    $mallIDs = $form['mallIDs'];
                    unset($form['mallIDs']);
                }
                if (isset($form['shopMGID'])) {
                    $shopMGIDs = $form['shopMGID'];
                    unset($form['shopMGID']);
                }
                if (isset($form['chain_name'])) {
                    $user_chain = $form['chain_name'];
                    unset($form['chain_name']);
                }

                $form['parent_user_id'] = 0;

                if (isset($currentUser) && $currentUser->admin_level === 'FGX Staff') {
                    $form['fgx_approved'] = 1;
                } else {
                    $form['fgx_approved'] = 0;
                }

                $createdUser = User::find($newUser->id);

                $updatedUser = $createdUser->update($form);

                if ($updatedUser) {
                    if (isset($form['mallID'])) {
                        $hasMovie = self::getHasMallHouse($form['mallID']);
                        if ($form['admin_level'] == 'Movie House' && $hasMovie == "No") {
                            $form['movieHouse'] = $movieHouse;
                            $form['movieHouse']['mallID'] = $form['mallID'];
                            $mall = ShoppingMall::find($form['mallID']);
                            $form['movieHouse']['name'] = $mall->name;
                            $form['movieHouse']['display'] = 'N';
                            $form['movieHouse']['province'] = $mall->province;
                            $form['movieHouse']['city'] = $mall->city;
                        }
                    }

                    //When adding a mall user, assign malls to the user
                    if ($form['admin_level'] == 'Marketing Manager') {

                        $groupID = 1;

                        $adminRole = Sentinel::findRoleById($groupID);
                        $adminRole->users()->attach($newUser);

                        if (isset($mallIDs)) {
                            foreach ($mallIDs as $mallID) {
                                DB::table('user_malls')->insert(array('user_id' => $newUser->id, 'mall_id' => $mallID));
                            }
                        } else {
                            DB::table('user_malls')->insert(array('user_id' => $newUser->id, 'mall_id' => $form['mallID']));
                        }
                    } elseif ($form['admin_level'] == 'Mall Manager') {
                        $groupID = 1;
                        $adminRole = Sentinel::findRoleById($groupID);
                        $adminRole->users()->attach($newUser);

                        DB::table('user_malls')->insert(array('user_id' => $newUser->id, 'mall_id' => $form['mallID']));

                    } elseif ($form['admin_level'] == 'Shop Manager') {

                        $groupID = 4;
                        $adminRole = Sentinel::findRoleById($groupID);
                        $adminRole->users()->attach($newUser);

                        foreach ($shopMGIDs as $shopMGID) {
                            DB::table('user_shops')->insert(array('user_id' => $newUser->id, 'shop_id' => $shopMGID));
                        }
                    } elseif ($form['admin_level'] == 'Chain Manager') {
                        $groupID = 4;
                        $adminRole = Sentinel::findRoleById($groupID);
                        $adminRole->users()->attach($newUser);

                        foreach ($user_chain as $chain) {
                            UserChain::insert(array('user_id' => $newUser->id, 'chain_name' => $chain));
                        }
                    } elseif ($form['admin_level'] == 'FGX Staff') {
                        $groupID = 2;
                        $adminRole = Sentinel::findRoleById($groupID);
                        $adminRole->users()->attach($newUser);

                        $createdUser->admin_level = $adminRole->name;
                        $createdUser->save();

                    } elseif ($form['admin_level'] == 'Master') {
                        $groupID = 5;
                        $adminRole = Sentinel::findRoleById($groupID);
                        $adminRole->users()->attach($newUser);

                        $createdUser->admin_level = $adminRole->name;
                        $createdUser->save();

                    } elseif ($form['admin_level'] == 'Movie House') {

                        $groupID = 6;
                        $adminRole = Sentinel::findRoleById($groupID);
                        $adminRole->users()->attach($newUser);

                        if ($hasMovie == "No") {
                            MovieMall::create($form['movieHouse']);
                        }
                        DB::table('user_malls')->insert(array('user_id' => $newUser->id, 'mall_id' => $form['mallID']));
                    }

                    if (isset($currentUser) && $currentUser->hasAccess('admin')) {
                        $form['name'] = $form['first_name'];
                        $form['userId'] = $createdUser->id;
                        $form['password'] = $password;
                        $form['activationCode'] = $createdUser->activation_code;
                        self::sendActivationEmail($form);
                    } else {
                        self::sendAddUserMail($form);
                    }

                    if (isset($currentUser) && $currentUser->hasAccess('admin')) {
                        session()->flash('success', "The user's info has been added.");

                        return redirect('admin/users');
                    } else {
                        //success!
                        session()->flash('success', 'Your info has been added. After your account has been approved an email will be sent with an activation link.');
                        return redirect('login');
                    }
                } else {
                    if (isset($currentUser) && $currentUser->hasAccess('admin')) {
                        session()->flash('success', "The user's info has been added, without access rights.");

                        return redirect('admin/users');
                    } else {
                        return redirect('login');
                    }
                }

            } catch (Exception $e) {
                session()->flash('error', 'A user with this email address already exists.');
                return back()->withErrors($validation)->withInput();
            }
        } else {
            session()->flash('error', 'There are some errors, please review the form...');

            return back()->withErrors($validation)->withInput();
        }
    }

    private function sendAddUserMail($input)
    {
        if (isset($input['mallID'])) {
            $users = User::whereHas('malls', function ($q) use ($input) {
                $q->where('mall_id', '=', $input['mallID']);
            })->get();

            foreach ($users as $user) {
                if (($user->admin_level == 'Mall Manager' || $user->admin_level == "Mall Manager") && $user->email != NULL) {
                    $input['to_cc'][$user->email] = $user->email;
                }
            }

            $mall = ShoppingMall::find($input['mallID']);
            $input['mall_name'] = $mall->name;
            $input['user_name'] = $mall->name;

        } elseif (isset($input['shop_name'])) {
            $input['user_name'] = $input['shop_name'];
        } else {
            $input['user_name'] = "";
        }

        //message for mall managers
        $input['the_message'] = $input['first_name'] . " has been added as a user on Mallguide. This user will only be able to access Mallguide after you have changed their user status to 'Approved' under the 'Users' tab on your control panel.";

        if (isset($input['movieHouse'])) {
            $second_message = "A new " . $input['movieHouse']['whichGroup'] . " movie house has been added.<br /><br />";

            $second_message .= '<table class="table">';
            if (isset($input['movieHouse']['whichGroup']) && $input['movieHouse']['whichGroup'] != NULL) {
                $second_message .= "<tr>
                    <td>
                        Cinema Name: &nbsp;
                    <td>
                        {$input['movieHouse']['whichGroup']}
                    </td>
                </tr>";
            }
            if (isset($input['movieHouse']['numCinemas']) && $input['movieHouse']['numCinemas'] != NULL) {
                $second_message .= "<tr>
                    <td>
                        Number of Cinemas:&nbsp;
                    <td>
                        {$input['movieHouse']['numCinemas']}
                    </td>
                </tr>";
            }
            $second_message .= "</table>";
            $input['second_message'] = $second_message;
        }

        if (isset($input['shopID'])) {
            $shop = ShoppingMall::find($input['shopID']);
            $input['shop_name'] = $shop->name;
        }

        Mail::send('emails.addUser', $input, function ($m) use ($input) {
            $m->from('webmaster@mallguide.co.za', Config::get('app.name'));
            $m->to('info@mallguide.co.za')->subject('User Registration | ' . Config::get('app.name'));
            if (isset($input['to_cc'])) {
                $m->cc($input['to_cc']);
            }
        });

        if (isset($input['email'])) {
            $input['the_message'] = "Your info, has successfully been added on Mallguide. After your account has been approved you will receive an email with an activation link. Please use these contact details for further queries: <a href='mailto:info@mallguide.co.za' style='font-weight:400; text-decoration:underline;'>info@mallguide.co.za</a>";
            $input['user_name'] = $input['first_name'];
            $email = $input['email'];
            Mail::send('emails.addUser', $input, function ($m) use ($email) {
                $m->from('webmaster@mallguide.co.za', Config::get('app.name'));
                $m->to($email)->subject('User Registration | ' . Config::get('app.name'));
            });
        }

    }

    public function getEditUser($userId)
    {
        $data['chains'] = Shop::orderBy('name')->groupBy('name')->havingRaw("COUNT('name') > 1")->where('display', '=', 'Y')->pluck('name');

        $mallIds = array();

        $data['user'] = User::find($userId);

        $countryId = session()->get('country_id', 190);

        $data['cities'] = City::whereHas('province', function ($q) use ($countryId) {
            $q->where('country_id', '=', $countryId);
        })->get(['name', 'id']);

        $data['companyList'] = AdminUser::orderBy('company')->distinct('company')->get(['company', 'company']);

        $data['provinces'] = Province::where('country_id', session()->get('country_id', 190))->pluck('name', 'id');

        $data['malls'] = ShoppingMall::orderBy('name')->distinct('name')->get(['name', 'mallID']);

        if ($data['user']->admin_level == 'Mall Manager' || $data['user']->admin_level == 'Marketing Manager') {
            $data['selectedMalls'] = $data['user']->malls->pluck('name', 'mallID')->toArray();

            $data['selectMall'] = "";

            if ($data['user']->admin_level == 'Mall Manager') {
                $arrays = array_divide($data['selectedMalls']);

                if (isset($arrays[0][0])) {
                    $data['selectMall'] = $arrays[0][0];
                }
            } else {
                $mallArray = array_divide($data['selectedMalls']);

                if (isset($mallArray[0])) {
                    $data['selectMall'] = $mallArray[0];
                }
            }

        } elseif ($data['user']->admin_level == 'Chain Manager') {
            $userChains = UserChain::where('user_id', '=', $userId)->pluck('chain_name');
            $data['userchain'] = $userChains;
        } elseif ($data['user']->admin_level == 'Shop Manager') {
            $data["selectedMalls"] = $data['user']->shops->pluck('mallID')->toArray();

            $data["userShops"] = $data['user']->shops->pluck('name', 'shopMGID');

            $data["shops"] = [];
            if (count($data["selectedMalls"])) {
                $data["shops"] = Shop::whereIn('mallID', $data["selectedMalls"])->get(['name', 'shopMGID']);
            }

            $arrays = array_divide($data["userShops"]->toArray());

            $data['selectedShops'] = $arrays[0];
        }

        if (Sentinel::hasAccess("admin")) {
            $data['adminLevel'] = array('' => 'Select Admin Level', 'Master' => 'Master', 'FGX Staff' => 'FGX Staff', 'Mall Manager' => 'Mall Manager', 'Marketing Manager' => 'Marketing Manager', 'Shop Manager' => 'Shop Manager', 'Chain Manager' => 'Chain Manager');

            return view('admin.users.edit', $data);
        } else {
            if (Sentinel::hasAccess('manager')) {
                $malls = session()->get('mgUser')['malls'];
                $arrays = array_divide($malls);
                $data['shopList'] = Shop::whereIn('mallID', $arrays[0])->orderBy('name')->distinct('name')->lists('name', 'shopMGID');
                $data['portalMalls'] = $malls;

            }
            return View::make('portal.users.edit')->with($data);
        }

    }

    public function postEditUser($userId)
    {
        $input = array(
            'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
            'email' => Input::get('email')
        );

        // Set Validation Rules
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|min:4|max:64|email'
        );

        //Run input validation
        $v = Validator::make($input, $rules);

        if ($v->fails()) {
            // Validation has failed
            session()->flash('error', 'Please review the form!');
            return redirect('admin/users/edit/' . $userId)->withErrors($v)->withInput();
        } else {
            try {
                $user = User::find($userId);

                $activated = Input::get('activated') == 'on' ? true : false;

                // Update the user details
                $user->first_name = Input::get('first_name');
                $user->last_name = Input::get('last_name');
                $user->cell = Input::get('cell');
                $user->tel_home = Input::get('tel_home');
                $user->tel_work = Input::get('tel_work');
                $user->fax = Input::get('fax');
                $user->postal_address = Input::get('postal_address');
                $user->postal_code = Input::get('postal_code');
                $user->birth_date = Input::get('birth_date');
                $user->default_country_id = Input::get('default_country_id', 190);
                $user->province_id = Input::get('province_id');
                $user->city_id = Input::get('city_id');
                $user->suburb = Input::get('suburb');
                $user->company = Input::get('company');
                $user->vat_number = Input::get('vat_number');
                $user->activated = $activated;

                if ($user->admin_level == 'Mall Manager') {

                    $currentMalls = array();
                    $userMall = UserMall::where('user_id', '=', $user->id)->get();

                    foreach ($userMall as $mall) {
                        array_push($currentMalls, $mall->mall_id);
                    }

                    if (!in_array(Input::get('mallID'), $currentMalls)) {
                        //Do the Update
                        UserMall::where('user_id', '=', $user->id)->update(array('mall_id' => Input::get('mallID')));
                    }

                } elseif ($user->admin_level == 'Marketing Manager') {

                    $currentMalls = array();
                    $userMalls = UserMall::where('user_id', '=', $user->id)->get();

                    foreach ($userMalls as $mall) {
                        array_push($currentMalls, $mall->mall_id);
                    }

                    $newMallsAdded = array_diff(Input::get('mallID'), $currentMalls);

                    if (sizeof($newMallsAdded) > 0) {
                        //Insert the new malls
                        foreach ($newMallsAdded as $newMalls) {

                            UserMall::insert(array('user_id' => $userId, 'mall_id' => $newMalls));
                        }
                    }

                    if (sizeof(Input::get('mallID')) < sizeof($currentMalls)) {

                        $mallsToRemove = array_diff($currentMalls, Input::get('mallID'));
                        foreach ($mallsToRemove as $remove) {
                            UserMall::where('user_id', '=', $userId)->where('mall_id', '=', $remove)->delete();
                        }
                    }

                } elseif ($user->admin_level == 'Chain Manager') {
                    //delete current chains
                    foreach ($user->chainName as $userChain) {
                        $userChain->delete();
                    }
                    $userChains = Input::get('chain_name');

                    foreach ($userChains as $newChain) {
                        UserChain::insert(array('user_id' => $userId, 'chain_name' => $newChain));
                    }
                } elseif ($user->admin_level == 'Shop Manager') {
                    foreach ($user->userShops as $userShop) {
                        $userShop->delete();
                    }
                    $userShops = Input::get('shopMGID');

                    foreach ($userShops as $newShop) {
                        UserShop::insert(array('user_id' => $userId, 'shop_id' => $newShop));
                    }
                }
                // Update the user
                if ($user->save()) {

                    if ($activated) {
                        Activation::complete($user, $user->activation_code);
                    }

                    // User information was updated
                    session()->flash('success', 'Profile updated.');
                    if (Sentinel::hasAccess("admin")) {
                        return redirect('admin/users');
                    } else {
                        return redirect('portal/listusers');
                    }
                } else {
                    // User information was not updated
                    session()->flash('error', 'User profile could not be updated.');
                    return back()->withInput();
                }

            } catch (Cartalyst\Sentry\Users\UserExistsException $e) {
                session()->flash('error', 'User already exists.');
                return back()->withInput();
            } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
                session()->flash('error', 'User was not found.');
                return back();
            }
        }
    }

    public function getSuspendUser($userId)
    {
        $data['user'] = Sentinel::findById($userId);
        $data['id'] = $userId;

        return view('admin.users.suspend', $data);
    }

    public function postSuspendUser($userId)
    {
        // Gather Sanitized Input
        $input = array(
            'suspendTime' => Input::get('suspendTime')
        );

        // Set Validation Rules
        $rules = array(
            'suspendTime' => 'required|numeric'
        );

        //Run input validation
        $v = Validator::make($input, $rules);

        if ($v->fails()) {
            session()->flash('error', 'Please review the form!');

            return redirect('admin/users/suspend/' . $userId)->withErrors($v)->withInput();
        } else {

            $user = Sentinel::findById($userId);

            /**
             * When suspending a user, remove their activation record from the Actiovations table so that they do not
             * attemt to reactivate themselves by clicking on the previous activation link sent to them.
             * Activating them again will send them a new activation link.
             */
            Activation::remove($user);

            $user = User::find($userId);
            $user->status = 'suspended';
            $user->activated = false;
            $user->save();

            session()->flash('success', "User has been suspended.");
            return redirect('admin/users');
        }
    }

    public function getUnsuspendUser($userId)
    {
        $user = Sentinel::findById($userId);

        $activation = Activation::create($user);

        Activation::complete($user, $activation->code);
        // Unsuspend the user
        $user = User::find($userId);
        $user->status = 'unsuspended';
        $user->activated = true;
        $user->activation_code = $activation->code;
        $user->save();

        $userArray = $user->toArray();
        $data['name'] = $userArray['first_name'];
        $data['activationCode'] = $userArray['activation_code'];
        $data['email'] = $userArray['email'];
        $data['userId'] = $userArray['id'];

        self::sendActivationEmail($data);

        session()->flash('info', 'User unsuspended successfully!!');
        return redirect('admin/users');
    }

    public function getApproveUser($userId)
    {
        $user = User::find($userId);
        $user->fgx_approved = true;
        $savedUser = $user->save();

        if ($savedUser) {
            $userArray = $user->toArray();
            $data['name'] = $userArray['first_name'];
            $data['activationCode'] = $userArray['activation_code'];
            $data['email'] = $userArray['email'];
            $data['userId'] = $userArray['id'];

            self::sendActivationEmail($data);

            session()->flash('success', 'The email was sent.');
        } else {
            session()->flash('error', 'User could not be approved');
        }

        return back();
    }

    private function sendActivationEmail($newUser)
    {
        //send email with activation link.
        Mail::send('emails.auth.activate', $newUser, function ($m) use ($newUser) {
            $m->from('webmaster@mallguide.co.za', Config::get('app.name'));
            $m->to($newUser['email'])->subject('Account activation | Mall Guide');
        });

    }

    public function deleteUser($userId)
    {
        $user = Sentinel::findById($userId);

        // Delete the user
        if ($user->delete()) {
            session()->flash('success', 'User has been deleted.');
        } else {
            // There was a problem deleting the user
            session()->flash('error', 'There was a problem deleting that user.');
        }
    }

    public function getHasMallHouse($mallID)
    {
        $movieHouse = MovieMall::find($mallID);

        if ($movieHouse) {
            return "Yes";
        } else {
            return "No";
        }
    }

    public function getCreateFormFields()
    {

        $formFieldArray['formFields'] = MGForm::defaultFields();

        return Response::json($formFieldArray);
    }

    /**
     * get add competition field view
     * @param null $formID
     * @return mixed
     */
    public function getAddField($formID = NULL)
    {
        if (Sentinel::check()) {
            $user = Sentinel::getUser();

            if ($formID != NULL) {

                $data['formID'] = $formID;
                $form = MGForm::find($formID);
                $mallID = $form->formRequest->mallID;
                //amounts of checkbox/radio button as well as amount of textbox rows for dropdown
                $data['rows'] = array_combine(range(1, 20), range(1, 20));
                $data['width'] = array_combine(range(15, 60), range(15, 60));
                $data['wrap'] = array('none' => 'None', 'soft' => 'Soft', 'hard' => 'Hard');
                $data['prev_type'] = 'none';
                $data['size'] = array_combine(range(30, 300), range(30, 300));
                $data['hasFields'] = ShoppingMall::hasFieldAccess($mallID);
                $data['fieldTypes'] = MGForm::fieldTypes($data['hasFields']);
                $data['fileTypes'] = MGForm::fileTypes();

            } else {
                $data['rows'] = array_combine(range(1, 20), range(1, 20));
                $data['width'] = array_combine(range(15, 60), range(15, 60));
                $data['wrap'] = array('none' => 'None', 'soft' => 'Soft', 'hard' => 'Hard');
                $data['prev_type'] = 'none';
                $data['size'] = array_combine(range(30, 300), range(30, 300));
                $data['hasFields'] = ShoppingMall::hasFieldAccess();
                $data['fieldTypes'] = MGForm::fieldTypes($data['hasFields']);
                $data['fileTypes'] = MGForm::fileTypes();
            }

            $data['heading'] = "Add A Field";

            if ($user->hasAccess('admin')) {
                return view('admin.competitions.add_field', $data);
            } else {
                return view('portal/competitions/updatecompfield', $data)->nest('add_field', 'admins.competitions.add_field', $data);
            }
        } else {
            session()->flash('error', "You're not logged in!");
            return redirect('/login');
        }
    }


    /**
     * @param $form_id
     * @return mixed
     */
    public function getFields($form_id = null)
    {
        //Get the current user's id.
        if (Sentinel::check()) {
            $user = Sentinel::getUser();

            //Do they have admin access?
            if ($user->hasAccess('admin')) {
                if (isset($form_id)) {

                    $form = MGForm::find($form_id);
                    $data['moves'] = getMoveList();
                    $fields = $form->formFields->sortBy('contentOrder');
                    $data['formFields'] = $fields;

                    return view('admin.competitions.fields', $data);

                } else {

                    $data['moves'] = getMoveList();

                    $data['formFields'] = array(
                        0 => array(
                            'formFieldID' => 0,
                            'formID' => 0,
                            'name' => 'E-mail address',
                            'typeOfField' => 'text',
                            'mandatory' => 'Y',
                            'contentOrder' => '1',
                            'emailField' => 'Y'
                        ),
                        1 => array(
                            'formFieldID' => 0,
                            'formID' => 0,
                            'name' => 'Name',
                            'typeOfField' => 'text',
                            'mandatory' => 'Y',
                            'contentOrder' => '2',
                            'emailField' => 'N'
                        ),
                        2 => array(
                            'formFieldID' => 0,
                            'formID' => 0,
                            'name' => 'Surname',
                            'typeOfField' => 'text',
                            'mandatory' => 'N',
                            'contentOrder' => '3',
                            'emailField' => 'N'
                        ),
                        3 => array(
                            'formFieldID' => 0,
                            'formID' => 0,
                            'name' => 'Cell',
                            'typeOfField' => 'text',
                            'mandatory' => 'N',
                            'contentOrder' => '4',
                            'emailField' => 'N'
                        )

                    );
                    return view('admin.competitions.fields', $data);
                }
            } else {
                session()->flash('error', 'You do not have the required permissions to access that page!');
                return redirect('login');
            }
        } else {
            session()->flash('error', 'You\'re not an Administrator!');
            return redirect('/');
        }
    }

    /**
     * @param $fieldID
     */
    public function postUpdateField($fieldID)
    {
        $field = FormField::find($fieldID);
        $formID = $field->formID;

        $formFieldData = Input::get();

        if (isset($formFieldData['checkName'])) {
            $the_names = $formFieldData['checkName'];
            unset($formFieldData['checkName']);
        }
        if (isset($formFieldData['fileTypes'])) {
            $fileTypes = $formFieldData['fileTypes'];
            unset($formFieldData['fileTypes']);
        }
        $formFieldData['formID'] = $formID;
        $formFieldData['contentOrder'] = $field->contentOrder;
        //Insert Form Field

        $formField = $field->update($formFieldData);
        //Insert Form Field Specs
        $formFieldSpecData = Input::get();
        if (isset($fileTypes)) {
            $formFieldSpecData['fileTypes'] = implode(',', $fileTypes);
        }
        $formFieldSpecData['formFieldID'] = $field->formFieldID;
        $field->formFieldSpecs->update($formFieldSpecData);

        //element data if field is a radio button or a checkbox group
        if (isset($the_names)) {
            //$the_names=Input::get('name');
            //dd($the_names);
            $formElements = array();
            foreach ($field->FormfieldElements as $elem) {
                $elem->delete();
            }
            foreach ($the_names as $index => $name) {
                $formElements['name'] = $name;
                if ($name != '') {
                    $count = $index + 1;
                    $formElements['contentOrder'] = $count;
                    $formElements['formFieldID'] = $field->formFieldID;
                    $select = "checkedSelected" . $count;
                    //var_dump($select);
                    if (isset($formFieldData[$select])) {
                        $formElements['checkedSelected'] = 'Y';
                    } else {
                        $formElements['checkedSelected'] = 'N';
                    }
                    FormFieldElements::create($formElements);
                }
            }
        }


    }

    public function getMallEvents($mallID) {
        $events = MGEvent::select('eventsMGID', 'mallID', 'name', 'startDate', 'endDate')
            ->where('mallID', '=', $mallID)
            ->orderBy('endDate')
            ->get();
    }
}
