<?php

namespace App\Http\Controllers;

use App\AdminUser;
use App\BookingAccount;
use App\Credit;
use App\Exhibition;
use App\GiftIdea;
use App\Job;
use App\MGEvent;
use App\NewsletterMovie;
use App\Promotion;
use App\Shop;
use App\ShoppingMall;
use App\Tenant;
use App\User;
use App\WhatsShowing;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class PortalController extends Controller
{
    private $data = null;
    private $sms;

    public function __construct(SMSRepository $sms)
    {
        $this->sms = $sms;
        initSEO(array('title' => 'Mallguide'));
    }

    public function getResendActivation($userId)
    {
        if (Sentinel::hasAccess("manager") || Sentinel::hasAccess("user")) {

            $user = Sentinel::findUserById($userId);

            $user->fgx_approved = true;
            $savedUser = $user->save();

            if ($savedUser) {
                $userArray = $user->toArray();
                $data['name'] = $userArray['first_name'];
                $data['activationCode'] = $userArray['activation_code'];
                $data['email'] = $userArray['email'];
                $data['userId'] = $userArray['id'];

                self::sendActivationEmail($data);

                session()->flash('success', 'The email was sent.');
            } else {
                session()->flash('error', 'User could not be approved');
            }
        } else {
            session()->flash('error', "You 're not allowed to be here!");
            return redirect('/');
        }

        return back();
    }

    private function sendActivationEmail($newUser)
    {
        //send email with activation link.
        Mail::send('emails.auth.activate', $newUser, function ($m) use ($newUser) {
            $m->from('webmaster@mallguide.co.za', Config::get('app.name'));
            $m->to($newUser['email'])->subject('Account activation | Mall Guide');
        });
    }

    public function getIndex()
    {
        if (Sentinel::check()) {
            if (session()->has('back')) {
                session()->put('back', 'yes');
            }
            // Find the user using the user id
            $user = Sentinel::getUser();
            $usr = session()->get('mgUser');
            $data['user'] = $user;
            $data['headingName'] = '';
            $data['message'] = "";
            //user is allowed access to multiple Malls
            if (isset($usr['malls']) && sizeof($usr['malls']) > 0) {
                $userMalls = $usr['malls'];
                if (sizeof($userMalls) >= 1) {
                    $data['activeMallID'] = $newMall = session()->get('activeMallID');
                    $data['message'] = "";
                    if (Sentinel::hasAccess('manager')) {
                        $data['message'] = "mall";
                        $mall = ShoppingMall::find($newMall);
                        $data['countCompetitions'] = count($mall->curCompetitions);
                        $data['countShops'] = Shop::where('mallID', '=', $newMall)->where('display', '=', 'Y')->count();
                        $data['countEvents'] = MGEvent::where('mallID', '=', $newMall)->where('display', '=', 'Y')->where('endDate', '>=', date('Y-m-d'))->count();
                        $data['countExhibitions'] = Exhibition::where('mallID', '=', $newMall)->where('display', '=', 'Y')->where('endDate', '>=', date('Y-m-d'))->count();
                        $data['countPromotions'] = Promotion::where('mallID', '=', $newMall)->where('endDate', '>=', date('Y-m-d'))->count();
                        $data['countJobs'] = Job::where('mallID', '=', $newMall)->where('display', '=', 'Y')->count();
                        $data['unapprovedShops'] = Shop::where('mallID', $newMall)->where('display', '=', 'N')->count();
                        $mallShop = Shop::where('mallID', '=', $newMall)->pluck('shopMGID');

                        $data['mallUsers'] = ShoppingMall::where('mallID', '=', $newMall)->whereHas('user', function ($q) {
                            $q->where('fgx_approved', '!=', 1);
                        })->count();


                        //$queries = DB::getQueryLog();
                        //$last_query = end($queries);
                        $data['tenants'] = Tenant::where('mallID', $newMall)->where('activated', 0)->count();
                        $data['shopUsers'] = null;
                        if (count($mallShop)) {
                            $data['shopUsers'] = Shop::whereIn('shopMGID', $mallShop)->whereHas('user', function ($q) {
                                $q->where('fgx_approved', '!=', 1);
                            })->count();
                        }
//                        $data['smsCredits'] = Credit::where('mallID', '=', $newMall)->where('creditsType', '=', 'SMS')->sum('numCredits');
                    } else {
                        $data['message'] = "movie house";
                        $data['subscribed'] = NewsletterMovie::where('mallID', '=', $data['activeMallID'])->count('userID');
                        $data['countSchedules'] = WhatsShowing::where('mallID', '=', $data['activeMallID'])->where('startDate', '<=', date('Y-m-d H:i:s'))->where('endDate', '>=', date('Y-m-d H:i:s'))->count('whatsShowingID');
                    }

                    $data['headingName'] = $userMalls[$newMall];

                }
            } elseif (isset($usr['chains']) && sizeof($usr['chains']) > 0) {
                $data['message'] = "shops";
                $userChain = session()->get('activeChain');
                if ($userChain) {
                    $data['activeChain'] = $data['headingName'] = $userChain;
                    $data['user'] = $user;
                    $userShops = Shop::where('shop.name', '=', $userChain)->pluck('shopMGID');
                    $data['countPromotions'] = Promotion::whereIn('shopMGID', $userShops)->where('endDate', '>=', date("Y-m-d H:i:s"))->count();
                    $data['countJobs'] = Job::whereIn('shopMGID', $userShops)->where('endDate', '>=', date('Y-m-d H:i:s'))->count();
                    $data['countGiftIdeas'] = GiftIdea::whereIn('shopMGID', $userShops)->where('display', '=', 'Y')->count();
                    $data['countShops'] = sizeof($userShops);
                }
            } elseif (isset($usr['shops']) && sizeof($usr['shops']) > 0) {
                $data['activeShopID'] = session()->get('activeShopID');
                $data['message'] = "shop";
                $data['shop'] = Shop::with('competitions')->find(session()->get('activeShopID'));
                $data['headingName'] = $data['shop']->name;
                $data['activeShop'] = $data['shop'];

                $data['user'] = $user;
                $data['countPromotions'] = $data['shop']->curPromotions->count();
                $data['countJobs'] = $data['shop']->curJobs->count();
                $data['countCompetitions'] = count($data['shop']->curCompetitions);
            } else {
                session()->flush();
                Sentinel::logout();
                session()->flash('error', 'You\'re not logged in!');
                return redirect('/');
            }

            return view('portal/index', $data);
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * List Users for Logged In User
     */
    public function getListUsers()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager') || $user->hasAccess('user') && session()->has('activeMallID')) {
                $userMallID = session()->get('activeMallID');

                $data['mallUsers'] = User::whereHas('malls', function ($q) use ($userMallID) {
                    $q->where('mall_id', '=', $userMallID);

                })->orderBy('users.created_at')->get()->toArray();

                $data['shopUsers'] = User::whereHas('shops', function ($q) use ($userMallID) {
                    $q->where('shop.mallID', '=', $userMallID);

                })->orderBy('users.created_at')->get()->toArray();

                $usersTmp = array_merge($data['mallUsers'], $data['shopUsers']);
                $data['users'] = json_decode(json_encode($usersTmp), false);

                return view('/portal/users/list', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getSMSTerms()
    {

        $file = base_path() . 'download/Standard_Malls_Newsletter_and_SMS.pdf';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return response()->download($file, 'Standard_Malls_Newsletter_and_SMS.pdf', $headers);
    }

    /*****************************************************/
    /*Mall marketing manager pages
    /*****************************************************/
    /**
     * If user has multiple mall access Get the user's malls
     */
    public function getUserMalls()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;

            if ($user->hasAccess('manager')) {

                $usr = User::find($user->id);
                $userMalls = array();
                $malls = $usr->malls;

                foreach ($malls as $mall) {
                    array_push($userMalls, $mall->mallID);
                }

                $data['mallList'] = ShoppingMall::whereIn('mallID', $userMalls)->orderBy('name')->get();

                return view('portal/marketing/index', $data);

            } else {

                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    public function checkUser($bookingID)
    {
        $user = Sentinel::getUser();

        $booking = BookingSchedule::find($bookingID);
        if ($booking->userID = $user->user_id) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $type
     * @param $var
     * @return mixed
     */
    public function getSwitchActive($type, $var)
    {
        //change session active mall Social Campaigns
        if ($type == 'chain') {
            session()->put('activeChain', $var);
        }
        if ($type == 'shop') {
            session()->put('activeShopID', $var);
        }
        if ($type == 'mall') {
            session()->put('activeMallID', $var);
        }

        return Redirect::back();
        //to do refresh current page
    }

    /**
     * List shops for loggedin user
     * @return mixed
     */
    public function getShopList()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;
            $usr = User::find($user->id);

            if ($user->hasAccess('manager')) {

                $usrMalls = array();
                $malls = $usr->malls;

                foreach ($malls as $mall) {
                    array_push($usrMalls, $mall->mallID);
                }

                $data['shopList'] = Shop::whereIn('mallID', $usrMalls)->get();
                $data['malls'] = array('' => 'Select Mall') + ShoppingMall::whereIn('mallID', $usrMalls)->orderBy('name')->pluck('name', 'mallID');

                return view('portal/marketing/shops', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Filter shops by mallID
     */
    public function getMallShops($mallID)
    {
        if (Sentinel::check()) {
            return Shop::where('mallID', '=', $mallID)->orderBy('name')->get();
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * List all malls and add mall for selected shop
     */
    public function getAddMallShops()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;
            $usr = User::find($user->id);

            if ($user->hasAccess('manager')) {

                $usrMalls = array();
                $malls = $usr->malls;

                foreach ($malls as $mall) {
                    array_push($usrMalls, $mall->mallID);
                }

                $data['mallList'] = ShoppingMall::whereIn('mallID', $usrMalls)->get();

                return view('portal/marketing/mallshops', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get events for Mall MArketing manager
     */
    public function getMarketingEventList()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;
            $usr = User::find($user->id);

            if ($user->hasAccess('manager')) {

                $usrMalls = array();
                $malls = $usr->malls;

                foreach ($malls as $mall) {
                    array_push($usrMalls, $mall->mallID);
                }

                $data['events'] = MGEvent::whereIn('mallID', $usrMalls)->orderBy('startDate', 'desc')->get();
                $data['mallList'] = array('' => 'Filter...') + ShoppingMall::whereIn('mallID', $usrMalls)->orderBy('name')->pluck('name', 'mallID');

                return view('portal/marketing/events/index', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get Event Malls
     */
    public function getAddMallEvents()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;
            $usr = User::find($user->id);

            if ($user->hasAccess('manager')) {

                $usrMalls = array();
                $malls = $usr->malls;

                foreach ($malls as $mall) {
                    array_push($usrMalls, $mall->mallID);
                }

                $data['mallList'] = ShoppingMall::whereIn('mallID', $usrMalls)->get();

                return view('portal/marketing/events/malls', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Filter Events by mall
     * @param $mallID
     */
    public function getFilterEventByMall($mallID)
    {

        if ($mallID == 'all') {
            return MGEvent::select('eventsMGID', 'events.mallID', 'events.name', 'startDate', 'endDate', 'events.display')->join('shoppingMall', 'shoppingMall.mallID', '=', 'events.mallID')->orderBy('endDate')->get();
        } else {
            return MGEvent::select('eventsMGID', 'events.mallID', 'events.name', 'startDate', 'endDate', 'events.display')->join('shoppingMall', 'shoppingMall.mallID', '=', 'events.mallID')->where('shoppingMall.mallID', '=', $mallID)->orderBy('endDate', 'desc')->get();
        }

    }

    /**
     * Filter Exhibitions by mall
     * @param $mallID
     */
    public function getFilterExhByMall($mallID)
    {
        return Exhibition::where('mallID', $mallID)->get();
    }

    /**
     * Get Exhibitions for Logged in user
     */
    public function getUserExhibitions()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;
            $usr = User::find($user->id);

            if ($user->hasAccess('manager')) {

                $usrMalls = array();
                $malls = $usr->malls;

                foreach ($malls as $mall) {
                    array_push($usrMalls, $mall->mallID);
                }

                $data['mallList'] = array('' => 'Select...') + ShoppingMall::whereIn('mallID', $usrMalls)->orderBy('name')->pluck('name', 'mallID');
                $data['exhibitions'] = Exhibition::whereIn('mallID', $usrMalls)->get();

                return view('portal/marketing/exhibitions/index', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    public function getAddExhibition()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;
            $usr = User::find($user->id);

            if ($user->hasAccess('manager')) {

                $usrMalls = array();
                $malls = $usr->malls;

                foreach ($malls as $mall) {
                    array_push($usrMalls, $mall->mallID);
                }

                $data['mallList'] = ShoppingMall::whereIn('mallID', $usrMalls)->orderBy('name')->get();

                return view('portal/marketing/exhibitions/malls', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * List Competitions
     */
    public function getMarketingCompetitions()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;

            if ($user->hasAccess('manager')) {

                $usr = User::find($user->id);
                $usrMalls = array();
                $malls = $usr->malls;

                foreach ($malls as $mall) {
                    array_push($usrMalls, $mall->mallID);
                }

                $data['mallList'] = ShoppingMall::whereIn('mallID', $usrMalls)->orderBy('name')->pluck('name', 'mallID');

                $data['comps'] = DB::table('forms')->select(DB::raw('forms.formID,pageID,recipient,subject,description,thanksMsg,submitBtnText,forms.dateAdded,activated,startDate,endDate,SUM(CASE WHEN formFeedback.formID = forms.formID THEN 1 ELSE 0 END) as count_resp,(SELECT COUNT(competitionWinners.compWinnerID) FROM competitionWinners WHERE competitionWinners.formID = forms.formID) as winners'))
                    ->leftjoin('formFeedback', 'formFeedback.formID', '=', 'forms.formID')
                    ->leftjoin('competitionWinners', 'competitionWinners.formID', '=', 'formFeedback.formID')
                    ->leftjoin('formRequest', 'formRequest.formID', '=', 'forms.formID')
                    ->where('formType', '=', 'Competition')
                    ->whereIn('formRequest.mallID', $usrMalls)
                    ->groupBy('forms.formID')
                    ->get();

                return view('portal/marketing/competitions/index', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    public function getAddCompForMall()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;
            $usr = User::find($user->id);

            if ($user->hasAccess('manager')) {

                $usrMalls = array();
                $malls = $usr->malls;

                foreach ($malls as $mall) {
                    array_push($usrMalls, $mall->mallID);
                }

                $data['mallList'] = ShoppingMall::whereIn('mallID', $usrMalls)->orderBy('name')->get();

                return view('portal/marketing/competitions/malls', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getInvoiceList()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            $data['invoices'] = BookingAccount::where('userID', '=', $user->id)->get();

            return view('portal/invoices/list', $data);

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * List all malls and add promotion for each
     */
    public function getAddPromoForMall()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;
            $usr = User::find($user->id);

            if ($user->hasAccess('manager')) {

                $usrMalls = array();
                $malls = $usr->malls;

                foreach ($malls as $mall) {
                    array_push($usrMalls, $mall->mallID);
                }

                $data['mallList'] = ShoppingMall::whereIn('mallID', $usrMalls)->orderBy('name')->get();

                return view('portal/marketing/promotions/malls', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Get Job Listing
     */
    public function getJobs()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;
            $usr = User::find($user->id);

            if ($user->hasAccess('manager')) {

                $usrMalls = array();
                $malls = $usr->malls;

                foreach ($malls->toArray() as $mall) {
                    array_push($usrMalls, $mall['mallID']);
                }

                $data['mallList'] = array('' => 'Select.....') + ShoppingMall::whereIn('mallID', $usrMalls)->orderBy('name')->pluck('name', 'mallID');
                $data['jobs'] = Job::select(DB::raw('jobs.*,shop.name,SUM(CASE WHEN jobApplications.jobID = jobs.jobID THEN 1 ELSE 0 END) as applicants'))
                    ->leftjoin('shop', 'shop.shopMGID', '=', 'jobs.shopMGID')
                    ->leftjoin('jobApplications', 'jobApplications.jobID', '=', 'jobs.jobID')
                    ->leftjoin('jobApplicants', 'jobApplicants.jobApplicantID', '=', 'jobApplications.jobApplicantID')
                    ->whereIn('jobs.mallID', $usrMalls)
                    ->groupBy('jobs.jobID')
                    ->orderBy('jobs.endDate', 'desc')
                    ->get();

                return view('portal/marketing/jobs/index', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Filter jobs by mallID
     * @param $mallID
     * @return mixed
     */
    public function filterJobByMall($mallID)
    {

        return Job::select(DB::raw('jobs.*,shop.name,SUM(CASE WHEN jobApplications.jobID = jobs.jobID THEN 1 ELSE 0 END) as applicants'))
            ->leftjoin('shop', 'shop.shopMGID', '=', 'jobs.shopMGID')
            ->leftjoin('jobApplications', 'jobApplications.jobID', '=', 'jobs.jobID')
            ->leftjoin('jobApplicants', 'jobApplicants.jobApplicantID', '=', 'jobApplications.jobApplicantID')
            ->where('jobs.mallID', '=', $mallID)
            ->groupBy('jobs.jobID')
            ->orderBy('jobs.endDate', 'desc')
            ->get();
    }

    public function getAddJob()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;
            $usr = User::find($user->id);

            if ($user->hasAccess('manager')) {

                $usrMalls = array();
                $malls = $usr->malls;

                foreach ($malls->toArray() as $mall) {
                    array_push($usrMalls, $mall['mallID']);
                }

                $data['mallList'] = ShoppingMall::whereIn('mallID', $usrMalls)->orderBy('name')->get();

                return view('portal/marketing/jobs/malls', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Filter promotions by mall id
     * @param $mallID
     */
    public function getMallPromotions($mallID)
    {

        return Promotion::select('shop.name as shop', 'promotion', 'startDate', 'endDate', 'promotions.promotionsMGID')->where('promotions.mallID', $mallID)->join('shop', 'shop.shopMGID', '=', 'promotions.shopMGID')->orderBy('endDate', 'desc')->get();
    }

    public function getMarketingInfo()
    {


        return view('portal/marketinginfo');
    }

    public function getCompetitionInfo()
    {

        return view('portal/compinfo');
    }

    public function getCompTerms()
    {

        $file = base_path() . '/download/Competition_Standard_Terms_and_Conditions.pdf';
        $headers = array(
            'Content-Type: application/pdf',
        );
        return Response::download($file, 'Competition_Standard_Terms_and_Conditions.pdf', $headers);
    }

    /**
     * Get Pending shops for mall manager
     * @param $mallID
     */
    public function getPendingShops($mallID)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;

            if ($user->hasAccess('manager')) {

                $data['pendingShops'] = PendingApplication::select('shop.shopMGID', 'shop.name', 'shoppingMall.name as mall')->join('shop', 'shop.shopMGID', '=', 'pending_applications.shop_id')->join('shoppingMall', 'shoppingMall.mallID', '=', 'shop.mallID')->where('mall_id', $mallID)->get();
//                dd($data['pendingShops']);

                return view('portal/shops/pending', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Get unapproved shop details for mall manager
     * @param $shopMGID
     */
    public function getShopToApprove($shopMGID)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;

            if ($user->hasAccess('manager')) {

                $data['shop'] = Shop::find($shopMGID);

                return view('portal/shops/approve', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Center manager approve store
     * @param $shopMGID
     */
    public function postApproveShop($shopMGID)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;

            if ($user->hasAccess('manager')) {

                Shop::find($shopMGID)->update(array('display' => 'Y'));
                PendingApplication::where('shop_id', '=', $shopMGID)->update(array('approved' => 1, 'approved_by' => $user->id));

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Decline shop and email user with reason why
     */
    public function postDeclineShop($id, $reason)
    {

//        echo "Shop : ".$id." reason : ".$reason;

        $shop = Shop::find($id);

        $maildata['recipient'] = $shop->managerEmail;
        $maildata['name'] = $shop->managerName;
        $maildata['reason'] = $reason;
        Mail::send('emails.shopdeclined', $maildata, function ($m) use ($maildata) {
            $m->from('webmaster@mallguide.co.za', 'Mallguide');
            $m->to($maildata['recipient'])->subject('Shop Application Declined | Mall Guide');
        });

    }

    /**
     * Manager Add user
     */
    public function getAddUser()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager') && session()->has('activeMallID')) {
                $data['mallID'] = session()->get('activeMallID');
                return view('/portal/users/add', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Manager Add Mall User
     * @param $mallID
     */
    public function getAddMallUser($mallID = NULL)
    {
        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager') || $user->hasAccess('user')) {
                if ($mallID != NULL) {
                    $data['mall'] = ShoppingMall::find($mallID);
                } else {
                    $data['mall'] = ShoppingMall::find(session()->get('activeMallID'));
                }

                //$data['provinces'] = array(''=>'Select an existing province') + DB::table('provinces')->pluck('name','id');
                $data['cities'] = ShoppingMall::distinct('city')->orderBy('city')->pluck('city_id', 'city');
                $data['companies'] = AdminUser::whereIn('adminLevel', ['Mall centre manager', 'Mall marketing consultant'])->distinct('company')->orderBy('company')->pluck('company', 'company');

                return view('/portal/users/add_mall_user', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Add Mall User
     */
    public function postAddMallUser()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $input['email'] = Input::get('email');
                $input['password'] = Input::get('password');
                $input['password_confirmation'] = Input::get('password_confirmation');
                $input['first_name'] = Input::get('first_name');
                $input['birth_date'] = Input::get('birth_date');
                $currentDate = date("Y-m-d H:i:s");

                $rules = array(
                    'email' => 'required|min:4|max:64|email',
                    'first_name' => 'required',
                    'password' => 'required|confirmed|regex:((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%_]).{6,20})',
                    'password_confirmation' => 'required',
                    'birth_date' => 'date|before:' . $currentDate
                );

                $v = Validator::make($input, $rules);

                if ($v->passes()) {

                    try {
                        $newUser = Sentinel::register(array('email' => $input['email'], 'password' => $input['password']));

                        //Get the activation code & prep data for email
                        $data['activationCode'] = $newUser->GetActivationCode();
                        $data['email'] = $input['email'];

                        $data['userId'] = $newUser->getId();
                        $data['password'] = $input['password'];
                        $data['name'] = $user->first_name;

                        $newUser->admin_level = Input::get('admin_level');
                        $newUser->first_name = Input::get('first_name');
                        $newUser->parent_user_id = $user->id;
                        $newUser->last_name = Input::get('last_name');
                        $newUser->company = Input::get('company');
                        $newUser->vat_number = Input::get('vat_number');
                        $newUser->postal_address = Input::get('postal_address');
                        $newUser->postal_code = Input::get('postal_code');
                        $newUser->tel_work = Input::get('tel_work');
                        $newUser->default_country_id = session()->get('default_country_id');
                        $newUser->fax = Input::get('fax');
                        $newUser->cell = Input::get('cell');
                        $newUser->province_id = Input::get('province_id');
                        $newUser->fgx_approved = 1;
                        $newUser->city_id = Input::get('city_id');
                        $newUser->birth_date = Input::get('birth_date');
                        $newUser->gender = Input::get('gender');

                        $newUser->save();

                        $thisNewUser = Sentinel::findUserById($newUser->id);

                        if ($newUser->admin_level == 'Mall Manager' || $newUser->admin_level == 'Marketing Manager' || $newUser->admin_level == 'Chain Manager') {

                            $adminGroup = Sentinel::findGroupById(1);
                            $thisNewUser->addGroup($adminGroup);

                        } elseif ($newUser->admin_level == 'Shop Manager') {

                            $adminGroup = Sentinel::findGroupById(4);
                            $thisNewUser->addGroup($adminGroup);
                        }
//
                        $mgUser = User::find($user->id);
                        $userMalls = $mgUser->malls;
                        //When adding a mall user, assign malls to the user
                        if (Input::get('admin_level') == 'Mall Manager' || Input::get('admin_level') == 'Marketing Manager') {

                            if (is_array(Input::get('mallID'))) {

                                $mallsAssigned = Input::get('mallID');
                                foreach ($mallsAssigned as $newUserMalls) {
                                    DB::table('user_malls')->insert(array('user_id' => $newUser->id, 'mall_id' => $newUserMalls));
                                }
                            } else {
                                foreach ($userMalls as $malls) {
                                    DB::table('user_malls')->insert(array('user_id' => $newUser->id, 'mall_id' => $malls->mallID));
                                }
                            }
                        } elseif (Input::get('admin_level') == 'Shop Manager') {

                            //Insert User into user shops
                            $newUser->first_name = Input::get('first_name');
                            $newUser->admin_level = Input::get('admin_level');
                            $newUser->parent_user_id = $user->id;
                            $newUser->save();

                            DB::table('user_shops')->insert(array('user_id' => $newUser->id, 'shop_id' => Input::get('shopMGID')));

                        } elseif (Input::get('admin_level') == 'Chain Manager') {

                            $newUser->first_name = Input::get('first_name');
                            $newUser->admin_level = Input::get('admin_level');
                            $newUser->parent_user_id = $user->id;
                            $newUser->save();

                            DB::table('user_chain')->insert(array('user_id' => $newUser->id, 'chain_name' => Input::get('chain_name')));
                        }
//
//                        //send email with activation link.
                        Mail::send('emails.auth.activate', $data, function ($m) use ($data) {
                            $m->from('webmaster@mallguide.co.za', 'Mallguide');
                            $m->to($data['email'])->subject('Account activation | Mall Guide');
                        });

                        //success!
                        session()->flash('success', 'User account has been created. An email will be sent to the user with an activation link and login details.');

                        if ($user->hasAccess('manager') || $user->hasAccess('user')) {
                            return redirect('/portal');
                        }

                    } catch (Cartalyst\Sentinel\Users\UserExistsException $e) {
                        session()->flash('error', 'A user already exists with this email ... ');
                        return back()->withErrors($v)->withInput();
                    }

                } else {
                    session()->flash('error', 'There are errors,please review the form ..');
                    return back()->withErrors($v)->withInput();
                }
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    public function getListTenants()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager') && session()->has('activeMallID')) {
                $userMallID = session()->get('activeMallID');

                $data['tenants'] = Tenant::where('mallID', $userMallID)->orderBy('last_name')->get();

                return view('/portal/tenants/list', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * List Unapproved Users
     */
    public function getListUnapprovedUsers()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager')) {
                $usr = User::find($user->id);
                $compareArray = array();

                $userMalls = $usr->malls;

                foreach ($userMalls as $usrMall) {
                    array_push($compareArray, $usrMall->mallID);
                }

                $data['users'] = User::whereHas('shops', function ($q) use ($compareArray) {
                    $q->whereIn('shop.mallID', $compareArray)->where('activated', '=', 0);

                })->orderBy('users.first_name')->get();

                return view('/portal/users/unapproved', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Logged in manager Edit User
     */
    public function getEditUser($userID)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager')) {

                $data['user'] = User::find($userID);
                $data['provinces'] = array('' => 'Select an existing province') + DB::table('provinces')->pluck('name', 'id');
                $data['cities'] = ShoppingMall::distinct('city')->orderBy('city')->pluck('city', 'city');

                return view('/portal/users/edit', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get add Store User
     */
    public function getAddStoreUser($mallID = NULL)
    {
        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager') || $user->hasAccess('user')) {
                if ($mallID != NULL) {
                    $data['mall'] = ShoppingMall::find($mallID);
                } else {
                    $data['mall'] = session()->get('activeMallID');
                }

                $data['mall'] = ShoppingMall::find($mallID);
                $data['shops'] = Shop::where('mallID', '=', $mallID)->distinct('name')->orderBy('name')->pluck('name', 'shopMGID');
                //$data['provinces'] = array(''=>'Select an existing province') + DB::table('provinces')->pluck('name','id');
                $data['cities'] = DB::table('adminUsers')->distinct('city')->orderBy('city')->pluck('city', 'city');
                $data['companies'] = AdminUser::whereIn('adminLevel', array('Mall centre manager', 'Mall marketing consultant'))->distinct('company')->orderBy('company')->pluck('company', 'company');

                return view('/portal/users/add_store_user', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Post Delete User
     */
    public function postManagerDeleteUser($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                User::find($id)->delete();

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Resend Activation Link
     */
    public function postResendActivation($userID)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $userToReactivate = Sentinel::getUserProvider()->findById($userID);
                $data['activationCode'] = $userToReactivate->getActivationCode();

                $data['email'] = $userToReactivate->email;
                $data['userId'] = $userID;
                $data['name'] = $userToReactivate->first_name;
                $userToReactivate->fgx_approved = 1;
                $userToReactivate->save();

                //send email with activation link.
                Mail::send('emails.auth.activate', $data, function ($m) use ($data) {
                    $m->from('webmaster@mallguide.co.za', 'Mallguide');
                    $m->to($data['email'])->subject('Account activation | Mall Guide');
                });
                session()->flash('success', 'The email was sent.');

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Post Delete User
     */
    public function postDeleteTenant($id)
    {

        // Find the user using the user id
        $user = Sentinel::getUser();

        if ($user->hasAccess('manager')) {

            Tenant::find($id)->delete();
            return true;

        } else {

            return false;
        }

    }

    /**
     * Resend Activation Link
     */
    public function postActivateTenant($userID)
    {


        // Find the user using the user id
        $user = Sentinel::getUser();

        if ($user->hasAccess('manager')) {

            $userToReactivate = Tenant::find($userID);
            $data['activationCode'] = $userToReactivate->activation_code;
            $data['email'] = $userToReactivate->email;
            $data['userId'] = $userID;
            $data['name'] = $userToReactivate->first_name;
            $userToReactivate->save();

            //send email with activation link.
            Mail::send('emails.auth.activate', $data, function ($m) use ($data) {
                $m->from('webmaster@mallguide.co.za', 'Tenant Zone');
                $m->to($data['email'])->subject('Account activation | Tenant Zone');
            });
            session()->flash('success', 'The activation email was sent.');

        } else {
            session()->flash('error', 'You\'re not allowed to be here!');
            return redirect('/');
        }


    }

    /**
     * Mall Marketing manager add users
     */
    public function AddMarketingUser()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager')) {

                $usr = User::find($user->id);
                $userMalls = $usr->malls;
                $mallIDs = array();

                foreach ($userMalls as $malls) {

                    array_push($mallIDs, $malls->mallID);
                }

                $data['usermalls'] = ShoppingMall::whereIn('mallID', $mallIDs)->orderBy('name')->pluck('name', 'mallID');
                $data['malls'] = ShoppingMall::whereIn('mallID', $mallIDs)->orderBy('name')->pluck('mallID');
                $data['provinces'] = array('' => 'Select an existing province') + DB::table('provinces')->pluck('name', 'id');
                $data['cities'] = ShoppingMall::distinct('city')->orderBy('city')->pluck('city', 'city');
                $data['companies'] = AdminUser::whereIn('adminLevel', array('Mall centre manager', 'Mall marketing consultant'))->distinct('company')->orderBy('company')->pluck('company', 'company');

                return view('/portal/marketing/users/add', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Buy SMS Credits
     */
    public function getBuySMSCredits()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager') && session()->has('activeMallID')) {
                $mgUser = session()->get('mgUser');
                $malls = $mgUser['malls'];
                $data['activeMall'] = Helpers::userMallName();

                //$data['user'] = User::find($user->id);
                $data['user'] = $user;
                $data['name'] = $user->first_name . ' ' . $user->last_name;
                $packageOptions = BookingPrice::select('creditsCostSMSFeather', 'creditsCostSMSLite', 'creditsCostSMSMiddle', 'creditsCostSMSHeavy', 'numCreditsSMSFeather', 'numCreditsSMSLite', 'numCreditsSMSMiddle', 'numCreditsSMSHeavy')->first();
                $data['featherCost'] = $packageOptions->creditsCostSMSFeather;
                $data['liteCost'] = $packageOptions->creditsCostSMSLite;
                $data['middleCost'] = $packageOptions->creditsCostSMSMiddle;
                $data['heavyCost'] = $packageOptions->creditsCostSMSHeavy;
                $data['featherCount'] = $packageOptions->numCreditsSMSFeather;
                $data['liteCount'] = $packageOptions->numCreditsSMSLite;
                $data['middleCount'] = $packageOptions->numCreditsSMSMiddle;
                $data['heavyCount'] = $packageOptions->numCreditsSMSHeavy;

                return view('/portal/communication/buysms', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Post SMS Credits
     */
    public function postBuySMSCredits()
    {

        if (Sentinel::check()) {

            $usr = Sentinel::getUser();


            // Validate User Input
            $input['accountName'] = Input::get('accountName');
            $input['accountEmail'] = Input::get('accountEmail');

            $rules = array(
                'accountName' => 'required',
//                'description' => 'required',
                'accountEmail' => 'required|min:4|max:64|email'
            );

            $v = Validator::make($input, $rules);

            if ($v->passes()) {
                $invPrefix = null;
                $description = Input::get('description');

                $prices = BookingPrice::first();
                switch ($description) {
                    case 'SMS Credits - Feather package (250 credits)':
                        $price = $prices->creditsCostSMSFeather;
                        break;
                    case 'SMS Credits - Lite package (500 credits)':
                        $price = $prices->creditsCostSMSLite;
                        break;
                    case 'SMS Credits - Middle package (1500 credits)':
                        $price = $prices->creditsCostSMSMiddle;
                        break;
                    case 'SMS Credits - Heavy package (1500 credits)':
                        $price = $prices->creditsCostSMSHeavy;
                        break;
                    default:
                        $price = 0;
                }

                //See if loggen in user has credits
                $userCreditNum = Credit::where('mallID', '=', Input::get('mallID'))->max('numCredits');
                $userCredit = Credit::where('mallID', '=', Input::get('mallID'))->where('numCredits', '=', $userCreditNum)->first();

                if (isset($userCredit)) {
                    //User has credits, update number of credits
                    $newCreditTempID = CreditTemp::insertGetId(array('creditsID' => $userCredit->creditsID, 'userID' => $usr->userID, 'mallID' => Input::get('mallID'), 'creditsType' => 'SMS', 'numCredits' => Input::get('numCredits'), 'dateAdded' => date("Y-m-d H:i:s"), 'allocated' => 'N'));

                    $newCreditTemp = CreditTemp::find($newCreditTempID);
                    $bookedBy = BookedBy::insertGetId(array('bookingScheduleID' => 0, 'startDate' => date("Y-m-d H:i:s"), 'endDate' => date("Y-m-d H:i:s"), 'dateBooked' => date("Y-m-d H:i:s"), 'approved' => 'N', 'bookingWhat' => 'SMS Credits', 'userID' => $usr->id, 'creditsID' => $userCredit->creditsID, 'creditsTempID' => $newCreditTemp->creditsTempID, 'poNumber' => Input::get('poNumber'), 'bookingAccountID' => 0));

                    if ($bookedBy < 10000) {
                        $invPrefix = '0';
                    } elseif ($bookedBy < 1000) {
                        $invPrefix = '00';
                    } elseif ($bookedBy < 100) {
                        $invPrefix = '000';
                    } elseif ($bookedBy < 10) {
                        $invPrefix = '0000';
                    }

                    $input = Input::all();
                    $bookingAccountData = array();
                    $bookingAccountData['bookedByID'] = $bookedBy;
                    $bookingAccountData['userID'] = $usr->id;
                    $bookingAccountData['description'] = Input::get('description');
                    $bookingAccountData['price'] = $price;
                    $bookingAccountData['discountPercent'] = 0;
                    $bookingAccountData['discount'] = 0;
                    $bookingAccountData['accountName'] = Input::get('accountName');
                    $bookingAccountData['accountEmail'] = Input::get('accountEmail');
                    $bookingAccountData['poNumber'] = Input::get('poNumber');
                    $bookingAccountData['vat'] = $usr->vat_number;
                    $bookingAccountData['company'] = $usr->company;
                    if (isset($input['postal_code']) && $input['postal_code'] != "") {
                        $bookingAccountData['postal_code'] = $input['postal_code'];
                    } else {
                        $bookingAccountData['postal_code'] = $usr->postal_code;
                    }
                    $bookingAccountData['accountAddress'] = Input::get('accountAddress');
                    $bookingAccountData['invoiceDoc'] = 'INV' . $invPrefix . $bookedBy;
                    $bookingAccountData['invoiceDate'] = date("Y-m-d H:i:s");
                    $bookingAccountData['ref'] = 'REF' . time();
                    $bookingAccountData['mallID'] = Input::get('mallID');

                    $bookingAccount = BookingAccount::create($bookingAccountData);
//                    dd($bookingAccount->bookingAccountID);
                    Helpers::sendInvoice($bookingAccount);

                    BookedBy::find($bookedBy)->update(array('bookingAccountID' => $bookingAccount->bookingAccountID));

                    session()->flash('success', 'Successfully ordered credits , your credits will only reflect when your order has been approved.');
                    return redirect('/portal');

                } else {

                    $userCreditID = Credit::insertGetId(array('userID' => $usr->userID, 'creditsType' => 'SMS', 'numCredits' => 0, 'lastAdded' => date("Y-m-d H:i:s")));
                    $userCredit = Credit::where('creditsID', '=', $userCreditID)->first();
                    //If User doesn't have credits, create new entry...
                    $newCreditTempID = CreditTemp::insertGetId(array('creditsID' => $userCredit->creditsID, 'userID' => $usr->userID, 'creditsType' => 'SMS', 'numCredits' => Input::get('numCredits'), 'dateAdded' => date("Y-m-d H:i:s"), 'allocated' => 'N'));
                    $newCreditTemp = CreditTemp::find($newCreditTempID);

                    $bookedBy = BookedBy::insertGetId(array('bookingScheduleID' => 0, 'startDate' => date("Y-m-d H:i:s"), 'endDate' => date("Y-m-d H:i:s"), 'dateBooked' => date("Y-m-d H:i:s"), 'approved' => 'N', 'bookingWhat' => 'SMS Credits', 'userID' => $usr->userID, 'creditsID' => $userCredit->creditsID, 'creditsTempID' => $newCreditTemp->creditsTempID, 'poNumber' => Input::get('poNumber'), 'bookingAccountID' => 0));

                    if ($bookedBy < 10000) {
                        $invPrefix = '0';
                    } elseif ($bookedBy < 1000) {
                        $invPrefix = '00';
                    } elseif ($bookedBy < 100) {
                        $invPrefix = '000';
                    } elseif ($bookedBy < 10) {
                        $invPrefix = '0000';
                    }

                    $bookingAccountData = array();
                    $bookingAccountData['bookedByID'] = $bookedBy;
                    $bookingAccountData['userID'] = $usr->id;
                    $bookingAccountData['description'] = Input::get('description');
                    $bookingAccountData['price'] = $price;
                    $bookingAccountData['discountPercent'] = 0;
                    $bookingAccountData['discount'] = 0;
                    $bookingAccountData['accountName'] = Input::get('accountName');
                    $bookingAccountData['accountEmail'] = Input::get('accountEmail');
                    $bookingAccountData['accountAddress'] = Input::get('accountAddress');
                    $bookingAccountData['invoiceDoc'] = 'INV' . $invPrefix . $bookedBy;
                    $bookingAccountData['invoiceDate'] = date("Y-m-d H:i:s");
                    $bookingAccountData['ref'] = 'REF' . time();

                    $bookingAccount = BookingAccount::create($bookingAccountData);
                    Helpers::sendInvoice($bookingAccount);

                    BookedBy::find($bookedBy)->update(array('bookingAccountID' => $bookingAccount->bookingAccountID));

                    session()->flash('success', 'Successfully ordered credits !!!');
                    return redirect('/portal');
                }

            } else {
                return redirect('/manager/buysmscredits')->withErrors($v)->withInput();
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }


    public function updateCreditsMallID()
    {

    }

    /**
     * Filter SMS batch by mall
     */
    public function getFilterSMSMall()
    {


        if (session()->has('activeMallID')) {
            $mallID = session()->get('activeMallID');

            $data['mall'] = ShoppingMall::find($mallID);
            $data['credits'] = Credit::where('mallID', '=', $mallID)->where('creditsType', '=', 'SMS')->sum('numCredits');
//			$data['smsRecipients'] = array('all'=>'All the shop\'s tenants','owners'=>'Owners only','manager1'=>'Managers 1 only','manager2'=>'Managers 2 only','manager3'=>'Marketing managers only','head'=>'Head office contacts only','financial'=>'Financial managers only','area'=>'Area managers only','ops'=>'Ops managers only','emergency1'=>'Emergecy contact 1 only','emergency2'=>'Emergecy contact 2 only');
//			$data['recipientList'] = Shop::where('mallID','=',$mallID)->select('shopMGID','name as name','ownerCell as owner','managerCell as manager','managerCell2 as manager2','managerCell3 as manager3','headOfficeCell as headoffice','financialCell as financial','areaManagerCell as areaManager','opsManagerCell as opsManager','emergencyCell as emergency1','emergencyCell2 as emergency2')->orderBy('name')->get();

            return view('/portal/communication/filtermall', $data);
        } else {
            session()->flash('error', 'You do not have a Mall selected.');
            return redirect('/portal');
        }

    }

    /**
     * Filter SMS Recipients by admin level
     */
    public function postFilterSMSRecipient()
    {

        $groups = Input::get('groups');

        if (count($groups)) {

//				$recipientList = Shop::where('mallID','=',session()->get('activeMallID'))->select('shopMGID','name')->orderBy('name');
            $items = Shop::where('mallID', '=', session()->get('activeMallID'))->select('shopMGID', 'name')->orderBy('name');


            foreach ($groups as $key => $value) {
                if ($value == 'owners' || $value == 'all') {
                    $items->addSelect('ownerCell', 'emergencyCell', 'emergencyCell2');
                }
                if ($value == 'managers' || $value == 'all') {
                    $items->addSelect('managerCell', 'managerCell2', 'managerCell3');
                }
                if ($value == 'headoffice' || $value == 'all') {
                    $items->addSelect('headOfficeCell', 'areaManagerCell', 'opsManagerCell');
                }
                if ($value == 'financial' || $value == 'all') {
                    $items->addSelect('financialCell');
                }
            }

            $recipientList = $items->get();
            //$items->chunk(50, function($shops)
            //{
            //
            //	foreach($shops as $shop)
            //	{
            //		$recipientList[] = $shop;
            //	}
            //	//session()->put('recipients', $recipientList);
            //});

            //dd($recipientList->toArray());

            $data['recipientList'] = $recipientList;
            //dd($data['recipientList']);
            return Response::view('partials/memberzone/shopSMS', $data);

        }

    }

    public function postSendTestSMS()
    {


        $usr = Sentinel::getUser();
        $mallID = session()->get('activeMallID');

        $credits = Credit::where('mallID', '=', $mallID)->where('creditsType', '=', 'SMS')->first();
        $userCredits = $credits->numCredits;

        $totalSpent = 1;

        $input['title'] = Input::get('title');
        $input['recipientList'] = Input::get('cell_number');


        $rules = array(
            'title' => 'required',
            'recipientList' => 'required'
        );

        $v = Validator::make($input, $rules);

        if ($v->passes()) {

            if ($userCredits < $totalSpent) {

                //User does not have sufficient credits
                $toRet = 'You do not have enough credits, please &nbsp;&nbsp;<a href="/manager/buysmscredits">order more</a> !!!';
                return $toRet;

            } else {

                // save message to db
                $message = array();
                $message['userID'] = $usr->id;
                $message['smsBody'] = Input::get('title');
                $message['dateAdded'] = Carbon::now('Africa/Johannesburg')->toDateTimeString();
                $message['recipients'] = $input['recipientList'];

                $source_id = SMSMessage::insertGetId($message);

                $response = new Client('https://iweb.itouchsa.co.za');

                $body = array(
                    'UserId' => 'FGX_Otto',
                    'Password' => 'XGGKBSJA',
                    'PhoneNumber' => Input::get('cell_number'),
                    'MessageText' => Input::get('title') . ' REPLY STOP TO OPT OUT',
                    'Reference' => $source_id
                );


                $request = $response->post('/Submit');
                $request->setBody($body);
                $request->setHeader('Content-Type', 'text/html');

                try {
                    $data = $request->send();
                    $toRet = $data->getBody(true);

                    $ret = explode('&', $toRet);

                    if ($ret[0] == 'Success') {
                        $credits->decrement('numCredits', 1);
                        return 'Message sent';
                    }
                    if ($ret[0] == 'Error') {
                        $message = $ret[1] . ' - ' . $ret[2];
                        return $message;
                    }

                } catch (\Guzzle\Http\Exception\BadResponseException $e) {
                    \Log::error(json_encode($e->getMessage()));
                    return $e->getMessage();
                } catch (\Guzzle\Http\Exception\CurlException $e) {
                    \Log::error(json_encode($e->getMessage()));
                    return $e->getMessage();
                }


            }
        }


    }

    public function postSendSMS()
    {


        if (Sentinel::check()) {

            $usr = Sentinel::getUser();
//            $usr = User::find($user->id);

            $mallID = session()->get('activeMallID');

            $credits = Credit::where('mallID', '=', $mallID)->where('creditsType', '=', 'SMS')->first();
            $userCredits = $credits->numCredits;


            $numArr = Input::get('extras');
            //dd($numArr);
            $additionalRecipients = array_where($numArr, function ($key, $value) {
                return strlen($value) == 11;
            });


            $totalSpent = intval(Input::get('count')) + count($additionalRecipients);

            $input['title'] = Input::get('title');

            $input['recipientList'] = explode(',', Input::get('number_array'));


            $rules = array(
                'title' => 'required',
                'recipientList' => 'required'
            );

            $v = Validator::make($input, $rules);

            if ($v->passes()) {

                if ($userCredits < $totalSpent) {

                    //User does not have sufficient credits
                    session()->flash('error', 'You do not have enough credits, please &nbsp;&nbsp;<a href="/manager/buysmscredits">order more</a> !!!');
                    return redirect('manager/sendsms');

                } else {

                    $recipients = array_merge($additionalRecipients, $input['recipientList']);
//					$recipientList = implode(',',$recipients);

                    // save message to db
                    $transaction = new SMSTransaction();
                    $transaction->mallID = $mallID;
                    $transaction->user_id = $usr->id;
                    $transaction->message = Input::get('title');
                    $transaction->save();

                    //$message = new SMSReport();
                    //$message->userID = $usr->id;
                    //$message->smsBody =  Input::get('title');
                    //$message->dateAdded = Carbon::now('Africa/Johannesburg')->toDateTimeString();
                    //$message->recipients = $recipientList;
                    //$message->save();

                    //$source_id = $transaction->id;

                    $response = new Client('https://iweb.itouchsa.co.za');


                    foreach ($recipients as $recipient) {
                        if (!empty($recipient)) {
                            try {

                                $body = array(
                                    'UserId' => 'FGX_Otto',
                                    'Password' => 'XGGKBSJA',
                                    'PhoneNumber' => $recipient,
                                    'MessageText' => Input::get('title') . ' REPLY STOP TO OPT OUT',
                                    'Reference' => $transaction->id,
                                    'Notification' => 'IWEB'
                                );

                                Log::info(json_encode($body));

                                $request = $response->post('/Submit');
                                $request->setBody($body);
                                $request->setHeader('Content-Type', 'text/html');
                                $data = $request->send();

                                $toRet = $data->getBody(true);
                                Log::info($toRet);
                                $ret = explode('&', $toRet);
                                $ret1 = explode('=', $ret[1]);
                                $ret2 = explode('=', $ret[2]);

                                if ($ret[0] == 'Success') {

                                    $report = new SMSReport();
                                    $report->sms_transaction_id = $transaction->id;
                                    $report->reference = $ret1[1];
                                    $report->phone_number = $ret2[1];
                                    $report->save();

                                    $credits->decrement('numCredits', 1);

                                }
                                if ($ret[0] == 'Error') {

                                    $report = new SMSReport();
                                    $report->sms_transaction_id = $transaction->id;
                                    $report->phone_number = $ret2[1];
                                    $report->error_code = $ret1[1];
                                    $report->error_description = $ret2[1];
                                    $report->save();


                                }


                            } catch (\Guzzle\Http\Exception\BadResponseException $e) {
                                \Log::error(json_encode($e->getMessage()));

                                return $e->getMessage();
                            } catch (\Guzzle\Http\Exception\CurlException $e) {
                                \Log::error(json_encode($e->getMessage()));

                                return $e->getMessage();
                            }
                        }


                    }
                    session()->flash('success', 'Messages sent! A report will be available within 24 hours...');
                    return redirect('/manager/sendsms');


                }
            } else {
                session()->flash('error', 'Please review the form!');

                return redirect('manager/sendsms/' . Input::get('type') . '/' . $mallID)->withErrors($v)->withInput();
            }
        } else {

            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    public function getProcessSMSReport()
    {

        $input = Input::all();


        $statusCodes = array(
            1 => 'Invalid command',
            2 => 'Invalid session identifier',
            3 => 'Invalid username or password',
            4 => 'Invalid scheduled time',
            5 => 'Missing required parameters',
            6 => 'Message not found',
            7 => 'Submit error (See description)',
            99 => 'Technical error'
        );
        Log::info(json_encode($input));

        if (isset($input['MessageReference'])) {
            $report = SMSReport::where('reference', $input['MessageReference'])->first();
            if ($report) {
                $report->status = $input['Status'];
                $report->status_description = $input['StatusDescription'];
                $report->submit_time = $input['SubmitTime'];
                $report->confirm_time = $input['ConfirmTime'];
                $report->save();
            }
        }


        $response = Response::make('success', 200);

        $response->header('Content-Type', 'text/html');

        return $response;

    }

    public function postValidateRecipientList()
    {

        $recipientList = array_filter(array_merge(array_unique(Input::get('cell_number')), Input::get('recipientList')));

        $testResult = MGCrud::verifyPhoneNumber($recipientList, 'sms/verify');

        $invalidNumbers = array_diff($testResult, array_values($recipientList));

        return $invalidNumbers;

    }

    /**
     * Update User Cell after Validating
     */
    public function postUpdateUserCell($shopID, $usertype, $newcell)
    {

        Shop::find($shopID)->update(array($usertype => $newcell));

        return Shop::find($shopID);
    }

    /**
     * Get SMS Reports
     */
    public function getSMSReports()
    {

        if (Sentinel::check()) {

            $usr = Sentinel::getUser();

            $data['messages'] = SMSMessage::where('userID', '=', $usr->id)->orderBy('dateAdded', 'desc')->get();

            return view('/portal/communication/member_sent_messages', $data);

        } else {

            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Get a detailed report for each sms batch
     */
    public function getDetailedSMSReport($id)
    {

        if (Sentinel::check()) {

            $usr = Sentinel::getUser();
//            $usr = User::find($user->id);

            $userMalls = $usr->malls;

            if (sizeof($userMalls) > 1) {

                $mallID = array();

//                foreach( $userMalls as $mall ){
//                    array_push($mallID,$mall->mallID);
//                }

                $params = array();
                $params['clientId'] = $usr->client_id;
                $data['userCredits'] = Credit::whereIn('mallID', $mallID)->where('creditsType', '=', 'SMS')->sum('numCredits');
                $campaign = MGCrud::SendCurlRequest($params, 'sms/campaign/' . $id);

                $data['campaign'] = $campaign;

            } else {

                $mallID = 0;

                foreach ($userMalls as $mall) {
                    $mallID = $mall->mallID;
                }

                $params = array();
                $params['clientId'] = $usr->client_id;
                $data['userCredits'] = Credit::where('mallID', '=', $mallID)->where('creditsType', '=', 'SMS')->sum('numCredits');

                $campaign = MGCrud::SendCurlRequest($params, 'sms/campaign/' . $id);
//                dd($campaign);
                $data['campaign'] = $campaign;

            }

            return view('/portal/communication/member_sent_message', $data);

        } else {

            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');

        }
    }

    /**
     * @return mixed
     */
    public function getFeatured()
    {
        if (Sentinel::check()) {
            $data['featuredOptionsDropdown'] = array();
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager')) {
                $data['featuredOptionsDropdown'] += array('Mall' => 'Mall', 'Shop' => 'Shop', 'Event' => 'Event', 'Competition' => 'Competition', 'Promotion' => 'Promotion');
                if (session()->has('activeMallID')) {
                    $data['activeMallID'] = session()->get('activeMallID');
                }
            }
            if ($user->hasAccess('independent')) {
                $data['featuredOptionsDropdown'] += array('Competition' => 'Competition');
                if (session()->has('activeMallID')) {
                    $data['activeMallID'] = session()->get('activeMallID');
                }
            }
            if ($user->hasAccess('user')) {
                $data['featuredOptionsDropdown'] += array('Shop' => 'Shop', 'Competition' => 'Competition', 'Promotion' => 'Promotion');
                if (session()->has('activeChain')) {
                    $shops = Shop::where('shop.name', '=', session()->get('activeChain'))->with('mall')->get();

                    foreach ($shops as $shop) {
                        //die(var_dump($shop->mall));
                        $data['shopList'][$shop->shopMGID] = $shop->name . " (" . $shop->mall['name'] . ")";
                    }
                    //die(var_dump($data['shopList']));
                } elseif (session()->has('activeShopID')) {
                    $data['activeShopID'] = session()->get('activeShopID');
                }
            }

            //$data['mallList'] = ShoppingMall::active()->orderBy('name')->pluck('name','mallID');
            //$data['shopList'] = Shop::display()->orderBy('name')->pluck('name','shopMGID');

            return view('portal/booking/featured', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * @return mixed
     */
    public function getBookingInfo($bookingID)
    {

        if (Sentinel::check()) {
            $booking = BookingSchedule::find($bookingID);

            $data['startDate'] = substr($booking->startDate, 0, 10);
            $data['endDate'] = substr($booking->endDate, 0, 10);
            $data['type'] = $booking->bookingWhat;
            if ($booking->bookingWhat == 'Mall') {
                $data['bookingPrice'] = BookingPrice::first()->featuredCostMall;
                $data['taxPrice'] = taxPrice($data['bookingPrice']);
                $data['type'] = 'Mall';
                $item = ShoppingMall::find($booking->mallID);
                $data['typeLabel'] = $item->name;
            } elseif ($booking->bookingWhat == 'Competition') {

                $data['bookingPrice'] = BookingPrice::first()->featuredCostCompetition;
                $data['taxPrice'] = taxPrice($data['bookingPrice']);
                $item = MGForm::find($booking->formID);
                $data['typeLabel'] = $item->subject;

            } elseif ($booking->bookingWhat == 'Event') {

                $data['bookingPrice'] = BookingPrice::first()->featuredCostEvent;
                $data['taxPrice'] = taxPrice($data['bookingPrice']);
                $item = MGEvent::find($booking->eventsMGID);
                $data['typeLabel'] = $item->name;

            } elseif ($booking->bookingWhat == 'Promotion') {

                $data['bookingPrice'] = BookingPrice::first()->featuredCostPromotion;
                $data['taxPrice'] = taxPrice($data['bookingPrice']);
                $item = Promotion::find($booking->promotionsMGID);
                $data['typeLabel'] = substr($item->promotion, 0, 30) . "...";

            } elseif ($booking->bookingWhat == 'Shop') {

                $data['bookingPrice'] = BookingPrice::first()->featuredCostShop;
                $data['taxPrice'] = taxPrice($data['bookingPrice']);
                $item = Shop::find($booking->shopMGID);
                $mall = ShoppingMall::find($item->mallID);
                $data['typeLabel'] = $item->name;
                $data['mallName'] = $mall->name;

            }

            return json_encode($data);

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * @return mixed
     */
    public function getDisplayNewBooking()
    {
        if (Sentinel::check()) {
            $input = Input::get();

            $data['startDate'] = date('Y-m-d', $input['startDate']);
            $data['endDate'] = date('Y-m-d', strtotime($data['startDate'] . ' + 6 days'));
            $mgUser = session()->get('mgUser');
            $mallName = "";
            if (session()->has('activeMallID') && !isset($input['mallID'])) {
                $input['mallID'] = session()->get('activeMallID');
                $mallName = $mgUser['malls'][$input['mallID']];
            } elseif (session()->has('activeShopID') && !isset($input['shopMGID'])) {
                $input['shopMGID'] = session()->get('activeShopID');
                $shopName = $mgUser['shops'][$input['shopMGID']];
            }

            if ($input['bookingWhat'] == 'Mall') {
                $data['bookingPrice'] = BookingPrice::first()->featuredCostMall;
                $data['taxPrice'] = taxPrice($data['bookingPrice']);
                $data['type'] = 'Mall';
                if ($mallName) {
                    $data['typeLabel'] = $mallName;
                } else {
                    $item = ShoppingMall::find($input['mallID']);
                    $data['typeLabel'] = $item->name;
                }
            } elseif ($input['bookingWhat'] == 'Shop') {
                $data['type'] = $input['bookingWhat'];
                $data['bookingPrice'] = BookingPrice::first()->featuredCostShop;
                $data['taxPrice'] = taxPrice($data['bookingPrice']);
                if (isset($shopName)) {
                    $data['typeLabel'] = $shopName;
                } else {
                    $item = Shop::find($input['shopMGID']);
                    $data['typeLabel'] = $item->name;
                }
            } elseif ($input['bookingWhat'] == 'Competition') {
                $data['bookingPrice'] = BookingPrice::first()->featuredCostCompetition;
                $data['taxPrice'] = taxPrice($data['bookingPrice']);

            } elseif ($input['bookingWhat'] == 'Event') {

                $data['bookingPrice'] = BookingPrice::first()->featuredCostEvent;
                $data['taxPrice'] = taxPrice($data['bookingPrice']);

            } elseif ($input['bookingWhat'] == 'Promotion') {

                $data['bookingPrice'] = BookingPrice::first()->featuredCostPromotion;
                $data['taxPrice'] = taxPrice($data['bookingPrice']);
            }

            return json_encode($data);

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Post Book Featured
     */
    public function postBookFeatured()
    {

//        dd(Input::all());
        if (Sentinel::check()) {
            $usr = Sentinel::getUser();
            $user = User::find($usr->id);

            $invPrefix = null;
            $description = Input::get('bookingWhat');
            $bookingWhat = '';
            switch ($description) {
                case 'mall':
                    $bookingAccountDescription = 'Featured Mall Booking - from ' . date('F - j - Y', strtotime(Input::get('startDate'))) . ' to ' . date('F - j - Y', strtotime(Input::get('endDate')));
                    $bookingWhat = 'Mall';
                    break;
                case 'competition':
                    $bookingAccountDescription = 'Featured Competition Booking - from ' . date('F - j - Y', strtotime(Input::get('startDate'))) . ' to ' . date('F - j - Y', strtotime(Input::get('endDate')));
                    $bookingWhat = 'Competition';
                    break;
                case 'event':
                    $bookingAccountDescription = 'Featured Event Booking - from ' . date('F - j - Y', strtotime(Input::get('startDate'))) . ' to ' . date('F - j - Y', strtotime(Input::get('endDate')));
                    $bookingWhat = 'Event';
                    break;
                case 'promotion':
                    $bookingAccountDescription = 'Featured Promotion Booking - from ' . date('F - j - Y', strtotime(Input::get('startDate'))) . ' to ' . date('F - j - Y', strtotime(Input::get('endDate')));
                    $bookingWhat = 'Promotion';
                    break;
                case 'shop':
                    $bookingAccountDescription = 'Featured Shop Booking - from ' . date('F - j - Y', strtotime(Input::get('startDate'))) . ' to ' . date('F - j - Y', strtotime(Input::get('endDate')));
                    $bookingWhat = 'Shop';
                    break;
                case 'job':
                    $bookingAccountDescription = 'Featured Job Booking - from ' . date('F - j - Y', strtotime(Input::get('startDate'))) . ' to ' . date('F - j - Y', strtotime(Input::get('endDate')));
                    $bookingWhat = 'job';
                    break;
                case 'exhibition':
                    $bookingAccountDescription = 'Featured Exhibition Booking - from ' . date('F - j - Y', strtotime(Input::get('startDate'))) . ' to ' . date('F - j - Y', strtotime(Input::get('endDate')));
                    $bookingWhat = 'exhibition';
                    break;
                default:
                    $bookingAccountDescription = '';
            }

            //First Insert into bookingSchedule
            $startDateArray = Input::get('startDay');
            $endDateArray = Input::get('endDay');

            $bookedByIDs = array();

            if (isset($startDateArray) && isset($endDateArray)) {

                foreach ($startDateArray as $k => $v) {

                    $startDate = $v . ' 00:00:00';
                    $endDate = $endDateArray[$k] . ' 23:59:59';

                    $bookingSchedule = BookingSchedule::insertGetId(array('startDate' => $startDate, 'endDate' => $endDate, 'dateBooked' => date('Y-m-d H:i:s'), 'bookingWhat' => $bookingWhat, 'mallID' => Input::get('mallID'), 'formID' => Input::get('formID'), 'eventsMGID' => Input::get('eventsMGID'), 'promotionsMGID' => Input::get('promotionsMGID'), 'shopMGID' => Input::get('shopMGID'), 'featured' => 'Y'));
                    $bookedBy = BookedBy::insertGetId(array('bookingScheduleID' => $bookingSchedule, 'startDate' => $startDate, 'endDate' => $endDate, 'dateBooked' => date("Y-m-d H:i:s"), 'approved' => 'N', 'bookingWhat' => $bookingWhat, 'userID' => $usr->id));

                    array_push($bookedByIDs, $bookedBy);
                }

                //Get Last Inserted Row in the booked by table

                $last = BookedBy::orderBy('bookedByID', 'desc')->first()->bookedByID;

                if ($last < 10000) {
                    $invPrefix = '0';
                } elseif ($last < 1000) {
                    $invPrefix = '00';
                } elseif ($last < 100) {
                    $invPrefix = '000';
                } elseif ($last < 10) {
                    $invPrefix = '0000';
                }

                $bookingAccountData = array();
//                $bookingAccountData['bookedByID'] = $bookedBy;
                $bookingAccountData['userID'] = $usr->id;
                $bookingAccountData['description'] = $bookingAccountDescription;
                $bookingAccountData['price'] = Input::get('price');
                $bookingAccountData['discountPercent'] = 0;
                $bookingAccountData['discount'] = 0;
                $bookingAccountData['accountName'] = $user->first_name;
                $bookingAccountData['accountEmail'] = $user->email;
                $bookingAccountData['accountAddress'] = $user->postal_address;
                $bookingAccountData['postal_code'] = $user->postal_code;
                $bookingAccountData['vat'] = $user->vat_number;
                $bookingAccountData['company'] = $user->company;
                $bookingAccountData['invoiceDate'] = date("Y-m-d H:i:s");
                $bookingAccountData['ref'] = 'REF' . time();
                $bookingAccountData['invoiceDoc'] = 'INV' . $invPrefix . $bookedBy;

                $booking = BookingAccount::insertGetId($bookingAccountData);

                foreach ($bookedByIDs as $a) {

                    BookedBy::find($a)->update(array('bookingAccountID' => $booking));
                }

                session()->flash('success', 'Successfully made featured booking !!!');
                return redirect('/portal');

            } else {

                $startDate = Input::get('startDate') . ' 00:00:00';
                $endDate = Input::get('endDate') . ' 23:59:59';

                $bookingSchedule = BookingSchedule::insertGetId(array('startDate' => $startDate, 'endDate' => $endDate, 'dateBooked' => date('Y-m-d H:i:s'), 'bookingWhat' => $bookingWhat, 'mallID' => Input::get('mallID'), 'formID' => Input::get('formID'), 'eventsMGID' => Input::get('eventsMGID'), 'promotionsMGID' => Input::get('promotionsMGID'), 'shopMGID' => Input::get('shopMGID'), 'featured' => 'Y'));
                $bookedBy = BookedBy::insertGetId(array('bookingScheduleID' => $bookingSchedule, 'startDate' => $startDate, 'endDate' => $endDate, 'dateBooked' => date("Y-m-d H:i:s"), 'approved' => 'N', 'bookingWhat' => $bookingWhat, 'userID' => $usr->id));

                if ($bookedBy < 10000) {
                    $invPrefix = '0';
                } elseif ($bookedBy < 1000) {
                    $invPrefix = '00';
                } elseif ($bookedBy < 100) {
                    $invPrefix = '000';
                } elseif ($bookedBy < 10) {
                    $invPrefix = '0000';
                }

                $bookingAccountData = array();
                $bookingAccountData['bookedByID'] = $bookedBy;
                $bookingAccountData['userID'] = $usr->id;
                $bookingAccountData['description'] = $bookingAccountDescription;
                $bookingAccountData['price'] = Input::get('price');
                $bookingAccountData['discountPercent'] = 0;
                $bookingAccountData['discount'] = 0;
                $bookingAccountData['accountName'] = $user->first_name;
                $bookingAccountData['accountEmail'] = $user->email;
                $bookingAccountData['accountAddress'] = $user->postal_address;
                $bookingAccountData['postal_code'] = $user->postal_code;
                $bookingAccountData['vat'] = $user->vat_number;
                $bookingAccountData['company'] = $user->company;
                $bookingAccountData['invoiceDate'] = date("Y-m-d H:i:s");
                $bookingAccountData['ref'] = 'REF' . time();
                $bookingAccountData['invoiceDoc'] = 'INV' . $invPrefix . $bookedBy;

                BookingAccount::insert($bookingAccountData);

                session()->flash('success', 'Successfully made featured booking !!!');
                return redirect('/portal');
            }

        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }


}
