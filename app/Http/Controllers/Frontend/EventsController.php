<?php

namespace App\Http\Controllers\Frontend;

use App\MGEvent;
use App\Shop;
use App\ShoppingMall;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

class EventsController extends Controller
{

    public function __construct()
    {
        initSEO(array('title' => 'Events | ' . Config::get('app.name')));
    }

    public function getIndex()
    {
        $malls = activeCountryMalls();

        if (count($malls)) {
            $data['events'] = MGEvent::select('eventsMGID', 'mallID', 'name', 'startDate', 'endDate')
                ->current()->display()
                ->whereIn('mallID', array_keys($malls->toArray()))
                ->orderBy('endDate')
                ->get();
        } else {
            $data['events'] = array();
        }

        return view('events.index', $data);
    }

    public function getMallEvents($mallID)
    {
        storeSelectedDropdownMallInSession($mallID);
        $events = MGEvent::select('eventsMGID', 'mallID', 'name', 'startDate', 'endDate')
            ->current()->display()
            ->where('mallID', '=', $mallID)
            ->orderBy('endDate')
            ->get();

        return view('events.index', array('events' => $events));
    }

    public function getMallEventsPage($id)
    {
        storeSelectedDropdownMallInSession($id);
        $events = MGEvent::select('eventsMGID', 'mallID', 'name', 'startDate', 'endDate')
            ->current()->display()
            ->where('mallID', '=', $id)
            ->orderBy('endDate')
            ->get();

        return view('events/index', array('events' => $events));
    }

    public function getViewEvent($id)
    {

        $dbEvntData = MGEvent::find($id);

        $data['mall'] = ShoppingMall::find($dbEvntData->mallID);
        $data['shop'] = Shop::find($dbEvntData->shopMGID);

        $data['event'] = $dbEvntData;

        initSEO(array('title' => pageTitle(array($dbEvntData->name, $data['mall']->name . ' Events', Config::get('app.name')))));

        return view('events.view_event')->with($data);
    }

    public function getKeyWordSearch($keyWord)
    {
        $stringToSearch = trim($keyWord);

        $events = MGEvent::select('eventsMGID', 'mallID', 'name', 'startDate', 'endDate')
            ->current()->display()
            ->where('name', 'like', '%' . $stringToSearch . '%')
            ->orderBy('endDate')
            ->paginate(15)
            ->toJson();

        return $events;
    }

    public function getEventList($mallID = NULL)
    {
        if (session()->has('activeMallID') && $mallID == NULL) {
            $mallID = session()->get('activeMallID');
        }
        $data['oldEvents'] = MGEvent::where('mallID', '=', $mallID)->where('endDate', '<', date('Y-m-d'))->orderBy('startDate', 'desc')->paginate(50);
        $data['currentEvents'] = MGEvent::where('mallID', '=', $mallID)->where('endDate', '>=', date('Y-m-d'))->orderBy('startDate', 'desc')->get();
        return view('portal/events/list', $data);
    }


    public function getCreate($mallID = NULL)
    {
        if (Sentinel::check()) {

            if (session()->has('activeMallID')) {
                $mallID = session()->get('activeMallID');
            }
            $data['mallID'] = $mallID;
            $data['categories'] = MGEvent::distinct('category')->orderBy('category')->pluck('category', 'category');
            $data['location'] = MGEvent::select('location')->distinct('location')->where('mallID', '=', $mallID)->pluck('location', 'location');

            return view('portal/events/create', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    public function postCreate()
    {
        if (Sentinel::check()) {
            $input = Input::all();

            $postingUrl = URL::previous();
            $mallID = Input::get('mallID');

            $v = MGEvent::validate($input);

            if ($v->passes()) {

                if (Input::hasFile('fileName')) {
                    $file2 = Input::file('fileName');
                    $destinationPath = base_path('/uploadimages') . '/mall_' . $mallID;
                    $input['fileName'] = uploadFile($file2, $destinationPath);
                }

                $createVal = $this->doCreate($input);

                if ($createVal === true) {
                    session()->flash('success', 'Event created successfully !!!');
                } else {
                    session()->flash('error', 'The event could not be created.');
                }

                $user = Sentinel::getUser();
                if ($user->hasAccess('admin')) {

                    return redirect('/admin/events/' . $mallID);
                } else {
                    return redirect('/events/list');
                }

            } else {
                session()->flash('error', 'Please review the form!');
                return redirect($postingUrl)->withErrors($v)->withInput();
            }
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getEdit($id)
    {
        $eventData = MGEvent::find($id);
        $data['event'] = $eventData;
        $data['categories'] = Shop::where('display', '=', 'Y')->orderBy('category')->pluck('category', 'category');
        $data['location'] = array('' => 'Select a location') + MGEvent::where('mallID', '=', $eventData->mallID)->orderBy('location')->pluck('location', 'location')->toArray();

        return view('portal/events/edit', $data);
    }

    public function postDelete($id)
    {
        MGEvent::find($id)->delete();
    }

    public function postEdit($id)
    {

        $input = Input::all();
        $mallID = Input::get('mallID');
        $postingUrl = URL::previous();

        $v = MGEvent::validate($input);

        if ($v->passes()) {

            $returnVal = $this->doEdit($id, $input);

            if ($returnVal == 1) {
                session()->flash('success', 'The event has been updated !!');
            } else {
                session()->flash('error', 'Event could not be updated..');
            }

            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('admin')) {

                return redirect('/admin/events/' . $mallID);
            }

            return redirect('/events/list');

        } else {
            session()->flash('error', 'Please review the form!');
            return redirect($postingUrl)->withErrors($v)->withInput();
        }
    }


    public function doCreate($input)
    {
        $data['name'] = isset($input['name']) ? $input['name'] : '';
        if (session()->has('activeMallID')) {
            $data['mallID'] = session()->get('activeMallID');
        } else {
            $data['mallID'] = isset($input['mallID']) ? $input['mallID'] : 0;
        }
        $data['shopMGID'] = isset($input['shopMGID']) ? $input['shopMGID'] : 0;
        $data['startDate'] = isset($input['startDate']) ? $input['startDate'] : '';
        $data['endDate'] = isset($input['endDate']) ? $input['endDate'] : '';
        $data['event'] = isset($input['event']) ? $input['event'] : '';
        $data['location'] = isset($input['location']) ? $input['location'] : '';
        $data['display'] = isset($input['display']) ? $input['display'] : 'Y';
        $data['category'] = isset($input['category']) ? $input['category'] : '';
        $data['fileName'] = isset($input['fileName']) ? $input['fileName'] : '';

        $thumbnail = isset($input['thumbnail1']) ? $input['thumbnail1'] : null;
        $altImage1 = isset($input['altImage1']) ? $input['altImage1'] : null;
        $altImage2 = isset($input['altImage2']) ? $input['altImage2'] : null;
        $altImage3 = isset($input['altImage3']) ? $input['altImage3'] : null;

        $thumbMaxWidth = 260;

        $destinationPath = base_path('uploadimages') . '/mall_' . $data['mallID'];

        if (is_object($thumbnail)) {

            $image1 = uploadImage($thumbnail, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['thumbnail1'] = $image1['thumbnail'];
            $data['image1'] = $image1['largeImage'];

        }

        if (is_object($altImage1)) {

            $alt1 = uploadImage($altImage1, $destinationPath);
            $data['altImage1'] = $alt1['unresized'];

        }

        if (is_object($altImage2)) {

            $alt2 = uploadImage($altImage2, $destinationPath);
            $data['altImage2'] = $alt2['unresized'];

        }

        if (is_object($altImage3)) {

            $alt3 = uploadImage($altImage3, $destinationPath);
            $data['altImage3'] = $alt3['unresized'];

        }

        $eventInserted = MGEvent::insert($data);

        return $eventInserted;
    }

}
