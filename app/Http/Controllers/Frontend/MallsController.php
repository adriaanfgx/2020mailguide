<?php

namespace App\Http\Controllers\Frontend;

use App\City;
use App\MGEvent;
use App\Province;
use App\Shop;
use App\ShoppingMall;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;

class MallsController extends Controller
{
    public function __construct()
    {
        initSEO(['title' => 'Malls | ' . Config::get('app.name')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $city_id = 0;
        $province_id = 0;
        $city_name = '';
        $province_name = '';

        $item_per_page = getQueryStringParam('per_page', 50, 250);


        initSEO(['title' => pageTitle(array('Malls', Config::get('app.name')))]);

        if ($request->has('city') && trim($request->get('city')) != '') {
            $arr_city = explode('-', $request->get('city'));
            $city_id = $arr_city[0];
            $city = City::where('id', $city_id)->first();
            if ($city != null) {
                if ($city->count()) {
                    $province = $city->province;
                    if ($province->count()) {
                        $city_name = $city->name;
                        $province_id = $province->id;
                        $province_name = $province->name;

                        initSEO(array('title' => pageTitle(array('Malls in ' . $city_name . ', ' . $province_name, Config::get('app.name')))));
                    }
                }
            }

        }


        return view('malls/index', array('city' => $city_name, 'city_id' => $city_id,
            'province' => $province_name, 'province_id' => $province_id, 'items_per_page' => $item_per_page));
    }

    /**
     * @param Request $request
     * @param null $keyword
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getMallByKeyWord(Request $request, $keyword = NULL)
    {
        if ($keyword != NULL) {
            $keyword = trim($keyword);
        } else {
            $keyword = trim($request->get('mall_name'));
        }

        if ($keyword) {
            session()->put('mallSearchKeyword', $keyword);
        }

        $records_count = 25;
        $searchResult = ShoppingMall::where('name', 'LIKE', '%' . $keyword . '%')->where('activate', '=', 'Y')->orderBy('name')->paginate($records_count);

        if (sizeof($searchResult) > 0) {

            $searchResult->setPath('/malls/quickfind/' . $keyword . '/');

        }

        return view('malls.quickfind', array('malls' => $searchResult));
    }

    /**
     * @param null $id
     * @return \Illuminate\Http\Response
     */
    public function getProvinceMalls($id = null)
    {
        // no province was specified, get selected country provinces
        if (!$id) {
            $country_id = session()->has('default_country_id') ? session()->get('default_country_id') : 190;
            $province_ids = Province::where('country_id', $country_id)->pluck('id');
        } else {
            $province_ids = array($id);
        }


        $alpha = \request()->get('alpha');

        $item_per_page = getQueryStringParam('per_page', 50, 250);

        $malls = ShoppingMall::whereIn('province_id', $province_ids)
            ->display()->where('activate', '=', 'Y')
            ->where('name', 'like', $alpha . '%')
            ->orderBy('name')
            ->select('name', 'physicalAddress', 'centreManagerTelephone', 'city', 'city_id', 'mallID', 'numShops', 'mapX', 'mapY', 'logo');
        if (\request()->has('city') && trim(\request()->get('city')) != '') {
            $arr_city = explode('-', \request()->get('city'));
            //$city = $arr_city[0];
            $city_id = $arr_city[0];
            $malls->where('city_id', $city_id);
        }
        $tmp = $malls->paginate($item_per_page);
        //dd(Helpers::getLastQuery());

        return response()->view('partials.mallTable', array('malls' => $tmp));
    }

    public function getView($id)
    {

        $alpha = \request()->get('alpha');
        $data['alpha'] = $alpha;

        $data['mall'] = ShoppingMall::with('details')->find($id);
        $city = City::find($data['mall']->city_id);
        if ($city) {
            $data['mall_city'] = $city;
        }

        $data['listedShops'] = Shop::where('mallID', '=', $id)
            ->where('name', 'like', $alpha . '%')
            ->display()->groupBy('name')
            ->orderBy('name')
            ->paginate(15);

        //Event listener fired when user views mall
        event('mall.view', array('id' => $id));

        initSEO(array('title' => pageTitle(array($data['mall']->name, Config::get('app.name'))), 'description' => metaDescription($data['mall']->mallDescription)));

        return view('malls/view', $data);
    }

    public function getByProvinceID($id)
    {
        $province_name = '';
        $province = Province::where('id', $id)->pluck('name');
        if ($province) {
            $province_name = $province . ' ';
        }

        initSEO(array('title' => pageTitle([$province_name . 'Malls', Config::get('app.name')])));


        return view('malls.index', ['id' => $id]);
    }

    public function getFilter($provinceID)
    {
        $malls = ShoppingMall::whereHas('promotions', function ($q) {
            $q->current();
        })
            ->orderBy('name')
            ->select('mallID', 'name')->display()
            ->where('province_id', '=', $provinceID)
            ->where('activate', '=', 'Y')
            ->get();

        if ($malls->count()) {
            foreach ($malls as $idx => $mall) {
                $malls[$idx]->totalPromotions = $mall->curPromotions->count();
            }
        }

        return $malls;
    }

    public function getCompFilter($province_id)
    {
        // $data = array('malls' => array(), 'competitions' => array());
        $malls = ShoppingMall::whereHas('formRequests', function ($query) {
            $query->active();
        })
            ->select('mallID', 'name')
            ->where('province_id', '=', $province_id)
            ->where('activate', '=', 'Y')
            ->where('mallguideDisplay', '=', 'Y')
            ->orderBy('name')
            ->get();

        //remove malls with no current competitions
        if ($malls) {
            foreach ($malls as $idx => $mall) {
                if (!$mall->curCompetitions->count()) {
                    unset($malls[$idx]);
                }
            }
        }

        return $malls;
    }

    public function getEventFilter($provinceID)
    {
        $arr_events = array();
        $mall_ids = array();
        $allMallEvents = array();

        $malls = ShoppingMall::whereHas('events', function ($q) {
            $q->current();
        })
            ->select('mallID', 'name')
            ->where('province_id', '=', $provinceID)
            ->where('activate', '=', 'Y')->display()
            ->orderBy('name')
            ->get();

        if ($malls->count()) {
            //get events count for each mall
            foreach ($malls as $idx => $mall) {
                $mall_ids[] = $mall->mallID;
                $events = MGEvent::select('eventsMGID', 'mallID', 'name', 'startDate', 'endDate')
                    ->current()
                    ->where('mallID', '=', $mall->mallID)
                    ->orderBy('endDate')
                    ->get();

                $malls[$idx]->name .= ' (' . $events->count() . ')';
            }

            $allMallEvents = MGEvent::select('eventsMGID', 'mallID', 'name', 'startDate', 'endDate')
                ->current()
                ->whereIn('mallID', $mall_ids)
                ->orderBy('endDate')
                ->paginate(15)
                ->toJson();
        }

        $data = array('malls' => $malls, 'events' => $allMallEvents);

        return $data;
    }

    public function getMallDetails($mallID = NULL)
    {
        if (Sentinel::check()) {
            if (session()->has('activeMallID') && $mallID == NULL) {
                $mallID = session()->get('activeMallID');
            }
            $data['mall'] = ShoppingMall::find($mallID);
            $data['cities'] = City::orderBy('name')->pluck('name', 'id');

            return view('portal.malldetails', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function postUpdate($id)
    {

        $mall = ShoppingMall::find($id);
        $input['name'] = Input::get('name');
        $input['mallDescription'] = Input::get('mallDescription');
        $input['province'] = Input::get('province');
        $input['city_id'] = Input::get('city_id');
        $input['classification'] = Input::get('classification');
        $input['physicalAddress'] = Input::get('physicalAddress');
        $input['glaRetailM2'] = Input::get('glaRetailM2');
        $input['numRetailFloors'] = Input::get('numRetailFloors');
        $input['coveredParkingBays'] = Input::get('coveredParkingBays');
        $input['totalParkingBays'] = Input::get('totalParkingBays');
        $input['postalAddress'] = Input::get('postalAddress');
        $input['website'] = Input::get('website');
        $input['numShops'] = Input::get('numShops');
        $input['openParkingBays'] = Input::get('openParkingBays');
        $input['anchorTenants'] = Input::get('anchorTenants');
        $input['specialFeature1'] = Input::get('specialFeature1');
        $input['specialFeature2'] = Input::get('specialFeature2');
        $input['specialFeature3'] = Input::get('specialFeature3');
        $input['specialFeature4'] = Input::get('specialFeature4');
        $input['specialFeature5'] = Input::get('specialFeature5');
        $input['specialFeature6'] = Input::get('specialFeature6');
        $input['specialFeature7'] = Input::get('specialFeature7');
        $input['specialFeature8'] = Input::get('specialFeature8');
        $input['specialFeature9'] = Input::get('specialFeature9');
        $input['specialFeature10'] = Input::get('specialFeature10');
        $input['centreManager'] = Input::get('centreManager');
        $input['centreManagerFax'] = Input::get('centreManagerFax');
        $input['centreManagerEmail'] = Input::get('centreManagerEmail');
        $input['centreManagerTelephone'] = Input::get('centreManagerTelephone');
        $input['centreManagerCell'] = Input::get('centreManagerCell');
        $input['marketingConsultant'] = Input::get('marketingConsultant');
        $input['marketingCell'] = Input::get('marketingCell');
        $input['marketingManager'] = Input::get('marketingManager');
        $input['marketingAssistant'] = Input::get('marketingAssistant');
        $input['marketingContractStart'] = Input::get('marketingContractStart');
        $input['marketingTelephone'] = Input::get('marketingTelephone');
        $input['marketingFax'] = Input::get('marketingFax');
        $input['marketingManagerEmail'] = Input::get('marketingManagerEmail');
        $input['marketingAssistantEmail'] = Input::get('marketingAssistantEmail');
        $input['marketingContractEnd'] = Input::get('marketingContractEnd');
        $input['leasingAgent'] = Input::get('leasingAgent');
        $input['leasingTelephone'] = Input::get('leasingTelephone');
        $input['leasingCell'] = Input::get('leasingCell');
        $input['leasingContactPerson'] = Input::get('leasingContactPerson');
        $input['leasingFax'] = Input::get('leasingFax');
        $input['leasingEmail'] = Input::get('leasingEmail');
        $input['shoppingCouncilMember'] = Input::get('shoppingCouncilMember');
        $input['shoppingCouncilNumber'] = Input::get('shoppingCouncilNumber');


        if (isset($mall->thumbWidth)) {

            $thumbMaxWidth = $mall->thumbWidth;
            $largeMaxWidth = $thumbMaxWidth * 2;
        } else {

            $thumbMaxWidth = 150;
            $largeMaxWidth = 300;
        }

        $v = ShoppingMall::validate($input);

        if ($v->passes()) {

            $destinationPath = base_path('uploadimages') . '/mall_' . $mall->mallID;

            $logo = Input::file('logo');
            $image1 = Input::file('image1');
            $image2 = Input::file('image2');
            $image3 = Input::file('image3');
            $image4 = Input::file('image4');
            $image5 = Input::file('image5');
            $image6 = Input::file('image6');
            $image7 = Input::file('image7');
            $image8 = Input::file('image8');
            $image9 = Input::file('image9');
            $image10 = Input::file('image10');

            if (isset($logo)) {
                $logo1 = uploadImage($logo, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['logo'] = $logo1['thumbnail'];
            }

            if (isset($image1)) {

                $img1 = uploadImage($image1, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['thumbnail1'] = $img1['thumbnail'];
                $input['image1'] = $img1['largeImage'];
            }

            if (isset($image2)) {
                $img2 = uploadImage($image2, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['thumbnail2'] = $img2['thumbnail'];
                $input['image2'] = $img2['largeImage'];
            }

            if (isset($image3)) {
                $img3 = uploadImage($image3, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['thumbnail3'] = $img3['thumbnail'];
                $input['image3'] = $img3['largeImage'];
            }

            if (isset($image4)) {
                $img4 = uploadImage($image4, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['thumbnail4'] = $img4['thumbnail'];
                $input['image4'] = $img4['largeImage'];
            }

            if (isset($image5)) {
                $img5 = uploadImage($image5, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['thumbnail5'] = $img5['thumbnail'];
                $input['image5'] = $img5['largeImage'];
            }

            if (isset($image6)) {
                $img6 = uploadImage($image6, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['thumbnail6'] = $img6['thumbnail'];
                $input['image6'] = $img6['largeImage'];
            }

            if (isset($image7)) {
                $img7 = uploadImage($image7, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['thumbnail7'] = $img7['thumbnail'];
                $input['image7'] = $img7['largeImage'];
            }

            if (isset($image8)) {
                $img8 = uploadImage($image8, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['thumbnail8'] = $img8['thumbnail'];
                $input['image8'] = $img8['largeImage'];
            }

            if (isset($image9)) {
                $img9 = uploadImage($image9, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['thumbnail9'] = $img9['thumbnail'];
                $input['image9'] = $img9['largeImage'];
            }

            if (isset($image10)) {
                $img10 = uploadImage($image10, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['thumbnail10'] = $img10['thumbnail'];
                $input['image10'] = $img10['largeImage'];
            }

            $mall->update($input);

            session()->flash('success', 'The Mall details were successfully updated');
            return redirect('/portal');
        } else {
            session()->flash('error', 'Please review the form!');
            return redirect('malldetails/' . $id)->withErrors($v)->withInput();
        }
    }

    /**
     * Filter malls by province id
     */
    public function getFilterMallsByProvince($id)
    {

        return ShoppingMall::where('province_id', '=', $id)->orderBy('name')->lists('name', 'mallID');
    }

    /**
     * Filter cities by province
     */
    public function getFilterCitiesByProvince($id)
    {
        return City::where('province_id', '=', $id)->orderBy('name')->select('name', 'id')->get();
    }

    /**
     * Remove mall image on edit
     */
    public function postRemoveMallImage($id, $image)
    {

        ShoppingMall::find($id)->update(array($image => ''));
    }


}
