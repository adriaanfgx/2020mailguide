<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Job;
use App\JobApplicant;
use App\JobApplication;
use App\Shop;
use App\ShoppingMall;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class JobController extends Controller
{
    protected $country;

    /**
     * Create a new controller instance.
     * @param $country
     * @return void
     */
    public function __construct()
    {
        initSEO(['title' => 'Jobs | ' . Config::get('app.name')]);
    }


    public function getList()
    {
        $malls = activeCountryMalls();

        if (count($malls)) {
            $data['jobs'] = Job::whereIn('mallID', array_keys($malls->toArray()))->where('endDate', '>=', date('y-m-d'))->display()->current()->orderBy('endDate', 'desc')->get();
        } else {
            $data['jobs'] = array();
        }

        $data['malls'] = ShoppingMall::whereHas('curJobs', function ($q) use ($malls) {

        })->orderBy('name')
            ->where('activate', '=', 'Y')
            ->whereIn('mallID', array_keys($malls->toArray()))
            ->pluck('name', 'mallID');

        return view('jobs.list', $data);
    }

    public function getShow($id)
    {
        $job = Job::find($id);
        $mall = ShoppingMall::find($job->mallID);

        initSEO(array('title' => pageTitle(array($job->title . ' needed', $mall->name, Config::get('app.name'))), 'description' => metaDescription($job->description)));

        return view('jobs/view', ['job' => $job, 'mall' => $mall]);
    }

    public function getKeyWordSearch($keyWord)
    {
        $stringToSearch = trim($keyWord);
        $malls = activeCountryMalls();

        return Job::select('jobs.description', 'jobs.jobID', 'jobs.title', 'shoppingMall.province', 'jobs.dateAdded', 'shoppingMall.name as mall')
            ->join('shoppingMall', 'shoppingMall.mallID', '=', 'jobs.mallID')->whereIn('shoppingMall.mallID', array_keys($malls->toArray()))
            ->display()
            ->current()
            ->whereRaw("(`jobs`.`description` LIKE  '%" . $stringToSearch . "%' OR  `shoppingMall`.`name` LIKE '%" . $stringToSearch . "%' or `shoppingMall`.`province` LIKE '%" . $stringToSearch . "%' or `jobs`.`title` LIKE '%" . $stringToSearch . "%')")
            ->get();
    }

    public function getIndex($mallID = NULL)
    {
        if (Sentinel::check()) {
            if (session()->has('activeMallID') && $mallID == NULL) {
                $mallID = session()->get('activeMallID');
            }

            $data['jobs'] = Job::select(DB::raw('jobs.*,shop.name,SUM(CASE WHEN jobApplications.jobID = jobs.jobID THEN 1 ELSE 0 END) as applicants'))
                ->leftjoin('shop', 'shop.shopMGID', '=', 'jobs.shopMGID')
                ->leftjoin('jobApplications', 'jobApplications.jobID', '=', 'jobs.jobID')
                ->leftjoin('jobApplicants', 'jobApplicants.jobApplicantID', '=', 'jobApplications.jobApplicantID')
                ->where('jobs.mallID', '=', $mallID)
                ->groupBy('jobs.jobID')
                ->orderBy('jobs.endDate', 'desc')
                ->get();

            $data['mallID'] = $mallID;
            return view('portal.jobs.index', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getCreate($mallID = NULL)
    {
        if (Sentinel::check()) {
            if (session()->has('activeMallID') && $mallID == NULL) {
                $mallID = session()->get('activeMallID');
            }
            if ($mallID) {
                $data['mall'] = ShoppingMall::find($mallID);
                $data['shops'] = Shop::where('mallID', '=', $mallID)->display()->orderBy('name')->pluck('name', 'shopMGID');
            } elseif (session()->get('activeShopID')) {
                $data['shopID'] = session()->get('activeShopID');
            }

            return view('portal.jobs.create', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getMallIndex($mallID)
    {
        $mall = ShoppingMall::find($mallID);
        $data['jobs'] = $mall->curJobs;

        return view('jobs.list', $data);
    }

    public function getApplicants($id)
    {
        if (Sentinel::check()) {
            $data['job'] = Job::find($id);
            $applicantID = JobApplication::where('jobID', '=', $id)->lists('jobApplicantID', 'jobApplicantID');
            $data['applicants'] = JobApplicant::whereIn('jobApplicantID', $applicantID)->get();
            return view('portal/jobs/applicants', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getApplicant($id)
    {
        if (Sentinel::check()) {
            $data['applicant'] = JobApplicant::where('jobApplicantID', '=', $id)->first();
            return view('portal/jobs/applicant', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function postCreate()
    {
        $mallID = Input::get('mallID');
        $referer = Input::get('referer');

        $data = Input::get();

        $data['dateAdded'] = date("Y-m-d H:i:s");
        $valid = Job::validate($data);
        if ($valid->passes()) {
            $job = Job::create($data);
            if ($job->jobID) {
                session()->flash('success', 'The job has been added');

                if ($referer == Config::get('app.url') . '/portal/jobs/malls') {
                    return redirect('portal/mjobs');
                } else {
                    return redirect('/jobs/index/' . $mallID);
                }
            } else {
                session()->flash('error', 'Please review the form.');
                return back()->withInput();
            }
        } else {
            session()->flash('error', 'Please review the form');
            return back()->withErrors($valid)->withInput();
        }
    }

    public function postApply($jobID)
    {

        $input = Input::all();
        $input['cv'] = Input::file('cv');
        $valid = JobApplicant::validate($input);

        if ($valid->passes()) {
            if (Input::hasFile('cv')) {
                $file2 = Input::file('cv');
                $destinationPath = Config::get('download_path');
                $input['cv'] = uploadImage($file2, $destinationPath);
            }
            $input['jobID'] = $jobID;
            $input['dateAdded'] = date("Y-m-d H:i:s");
            $application = JobApplicant::create($input);
            $applicant = array(
                'jobApplicantID' => $application->jobApplicantID,
                'dateAdded' => $input['dateAdded'],
                'jobID' => $jobID
            );

            JobApplication::create($applicant);

            session()->flash('success', 'Your application has been submitted successfully');
            return redirect("jobs/view/$jobID");

        } else {
            session()->flash('error', 'Please review the form.');
            return redirect("jobs/view/$jobID")->withInput()->withErrors($valid);

        }
    }

    public function getEdit($id)
    {
        if (Sentinel::check()) {
            $job = Job::find($id);
            $data['job'] = $job;

            $data['shops'] = Shop::where('mallID', '=', $job->mallID)->display()->orderBy('name')->pluck('name', 'shopMGID');

            return view('portal.jobs.edit', $data);
        } else {
            session()->flash('error', 'You are not logged in!');
            return redirect('/login');
        }
    }

    /**
     * @param $mallID
     * @return mixed
     */
    public function getPopulateTable($mallID = NULL)
    {
        $jobs = NULL;
        if ($mallID != NULL) {
            $jobs = Job::where('mallID', '=', $mallID)->display()->current()->get();
        } else {
            $malls = activeCountryMalls();

            $jobs = Job::whereIn('mallID', array_keys($malls))->display()->current()->get();
//            $jobs = array();
        }
        storeSelectedDropdownMallInSession($mallID);
        return response()->view('jobs/jobsTable', array('jobs' => $jobs));
    }

    public function postEdit($id)
    {

        $referer = Input::get('referer');
        $mallID = Input::get('mallID');
        $title = Input::get('title');
        $refNum = Input::get('refNum');
        $startDate = Input::get('startDate');
        $salary = Input::get('salary');
        $description = Input::get('description');
        $contactPerson = Input::get('contactPerson');
        $telephone = Input::get('telephone');
        $fax = Input::get('fax');
        $cell = Input::get('cell');
        $email = Input::get('email');
        $endDate = Input::get('endDate');
        $showInfo = Input::get('showInfo');
        $display = Input::get('display');

        $input_data = Input::get();

        $valid = Job::validate($input_data);
        if ($valid->passes()) {
            Job::where('jobID', '=', $id)->update(array('title' => $title, 'refNum' => $refNum, 'startDate' => $startDate, 'salary' => $salary, 'description' => $description, 'contactPerson' => $contactPerson, 'telephone' => $telephone, 'fax' => $fax, 'cell' => $cell, 'email' => $email, 'endDate' => $endDate, 'showInfo' => $showInfo, 'display' => $display));
            if (session()->has('activeChain')) {
                return redirect('chainstores/jobs');
            }

            session()->flash('success', 'The job was updated');
            if ($referer == Config::get('app.url') . '/portal/mjobs') {
                if (session()->has('activeChain')) {
                    return redirect('chainstores/jobs');
                } else {
                    return redirect('portal/mjobs');
                }
            } else {
                return redirect('/jobs/index/' . $mallID);
            }
        } else {
            session()->flash('error', 'Please review the form.');
            return redirect("jobs/edit/$id")->withInput()->withErrors($valid);
        }
    }

    public function getCV($cv)
    {
        $file = Config::get('download_path') . "/$cv";

        $fileExt = File::extension($cv);
        $headers = array(
            'Content-Type: application/' . $fileExt,
        );
        return response()->download($file, $cv, $headers);
    }

    public function postDelete($id)
    {
        $job = Job::find($id);
        if ($job) {
            $job->delete();
        }
    }

}
