<?php


namespace App\Http\Controllers\Frontend;


use App\FormFeedback;
use App\FormFeedbackValues;
use App\FormField;
use App\FormFieldElements;
use App\FormFieldSpec;
use App\FormRequest;
use App\MGForm;
use App\Shop;
use App\ShoppingMall;
use App\User;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class CompetitionsController
{
    public function __construct()
    {
        initSEO(array('title' => 'Competitions | ' . Config::get('app.name')));
    }

    public function getIndex()
    {
        return view('competitions.index');
    }

    public function getFrontTable($mallID)
    {
        //is this a single mall or a list of comma separated malls
        if ($mallID != 'all') {
            if (strpos($mallID, ',') === false) {
                storeSelectedDropdownMallInSession($mallID);
            } else {
                $mallID = substr($mallID, 0, -1);
            }
        }

        if ($mallID && $mallID != 'all') {
            $malls = ShoppingMall::whereIn('mallID', explode(',', $mallID))->get();
        } else {
            $mallID = array_keys(activeCountryMalls()->toArray());
            $malls = [];
            if (count($mallID)) {
                $malls = ShoppingMall::whereIn('mallID', $mallID)->get();
            }
        }

        $competitions = [];

        if (!is_array($malls) && $malls->count()) {
            foreach ($malls as $mall) {
                $cmps = $mall->curCompetitions;
                if (count($cmps)) {
                    foreach ($cmps as $cmp) {
                        if ($cmp->activated == 'Y')
                            $competitions[$cmp->formID] = $cmp;
                    }
                }
            }
        }
        $data["comps"] = $competitions;

        return view('competitions.table')->with('competitions', $data["comps"]);
    }

    public function getShow($id = null)
    {

        // Find the competition
        $competition = MGForm::find($id);
        $mall = $competition->formRequest->mall;

        initSEO(array('title' => pageTitle([$competition->subject, $mall->name, Config::get('app.name')]), 'description' => metaDescription($competition->description)));

        return view('competitions.show', ['competition' => $competition, 'mall' => $mall]);
    }

    public function getCreate($mallID = NULL)
    {
        if (Sentinel::check()) {
            if ($mallID == NULL && session()->has('activeMallID')) {
                $mallID = session()->get('activeMallID');
            } elseif (session()->has('activeChain')) {
                $data['chainMallIDs'] = Shop::where('name', '=', session()->get('activeChain'))->pluck('mallID');
                $data['chainMalls'] = ShoppingMall::whereIn('mallID', $data['chainMallIDs'])->orderBy('name')->pluck('name', 'mallID');
            }

            $data['mallID'] = $mallID;
            //does this mall have access to new fields?
            //$data['hasFields']=ShoppingMall::hasFieldAccess($mallID);

            return view('portal/competitions/create', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function postCreate()
    {

        $input = array(
            'recipient' => Input::get('recipient'),
            'subject' => Input::get('subject'),
            'startDate' => Input::get('startDate'),
            'endDate' => Input::get('endDate')
        );

        // Set Validation Rules
        $rules = array(
            'recipient' => 'required|email',
            'subject' => 'required',
            'startDate' => 'required|date',
            'endDate' => 'required|date'
        );

        $v = validator()->make($input, $rules);

        if ($v->fails()) {
            session()->flash('error', 'Please review the form!');
            $response_values = array(
                'validation_failed' => 1,
                'errors' => $v->errors()->toArray());
            return response()->json($response_values);
        } else {
            $response_values = array(
                'validation_passed' => 1,
                'errors' => array());
            return response()->json($response_values);
        }
    }

    /**
     * @return mixed
     */
    public function postCreateCompetition()
    {
//        print_r(json_decode(Input::get('formCustomFields')));exit();
        //if user is logged in
        if (Sentinel::check()) {
            $user = Sentinel::getUser();

            //Do they have admin access?
            if ($user->hasAccess('admin') || $user->hasAccess('manager') || $user->hasAccess('user') || $user->hasAccess('independent')) {
                $input = Input::all();

                $input['endDate'] = Carbon::parse($input['endDate'])->toDateString() . ' 23:59:59';

                if (Input::hasFile('image1')) {
                    $input['image1'] = Input::file('image1');
                } else if (isset($input['image1'])) {
                    unset($input['image1']);
                }
                if (Input::hasFile('TsandCsFile')) {
                    $input['TsandCsFile'] = Input::file('TsandCsFile');
                } else if (isset($input['TsandCsFile'])) {
                    unset($input['TsandCsFile']);
                }

                // Set Validation Rules
                if ($user->hasAccess('admin')) {
                    $v = MGForm::validateAdmin($input);
                } else {
                    $v = MGForm::validate($input);
                }
                if ($v->fails()) {
                    $response_values = array(
                        'validation_failed' => 1,
                        'errors' => $v->errors()->toArray());
                    //var_dump($v->errors()->toArray());
                    session()->flash('error', 'Please review the form');

                    if ($user->hasAccess('admin')) {
                        return response()->json($response_values);
                    } else {
                        session()->flash('error', 'Please review the form!');
                        return redirect('competitions/create')->withErrors($v)->withInput();
                    }
                } else {
                    $userID = $user->userID;

                    if (Input::has('mallID')) {
                        $mallID = Input::get('mallID');
                    } else {
                        if (session()->has('activeMallID')) {
                            $mallID = session()->get('activeMallID');
                        }
                        if (session()->has('activeMallID')) {
                            $mallID = session()->get('activeMallID');
                        } elseif (session()->has('activeShopID')) {
                            $shop = Shop::find(session()->get('activeShopID'));
                            $mallID = $shop->mallID;
                        }
                    }

                    $data = Input::all();
                    $data['endDate'] = Carbon::parse($input['endDate'])->toDateString() . ' 23:59:59';
                    $data['userID'] = $user->id;
                    $data['pageID'] = 0;
                    $data['dateAdded'] = time();
                    $data['formType'] = 'Competition';
                    $data['displayFor'] = Input::get('displayFor', 'M');

                    if (isset($data['facebook'])) {
                        $data['activated'] = $data['facebook'] == "Y" ? 'N' : $data['activated'];
                    }
                    $thumbMaxWidth = 150;
                    $largeMaxWidth = 450;

                    $uploadDate = date("YmdHis");


                    if (Input::hasFile('image1')) {
                        $file1 = Input::file('image1');
                        $destinationPath = base_path('uploadimages');

                        $image_array = uploadImage($file1, $destinationPath, $thumbMaxWidth);

                        $data['image'] = $image_array['largeImage'];
                        if (isset($data['facebook']) && $data['facebook'] == "Y") {
                            $data['imageFB'] = $image_array['largeImage'];
                        }
                        if (isset($image_array['thumbnail'])) {
                            $data['thumbnail'] = $image_array['thumbnail'];
                        }
                    }
                    if (Input::hasFile('TsandCsFile')) {
                        $file2 = Input::file('TsandCsFile');
                        $destinationPath = base_path('uploadimages') . '/mall_' . $mallID;

                        $data['TsandCsFile'] = uploadImage($file2, $destinationPath)['largeImage'];
                    }

                    $competition = MGForm::create($data);

                    $response_values = array(
                        'validation_passed' => 1,
                        'errors' => array(),
                        'status' => 'success',
                        'mallID' => $mallID,
                        'id' => $competition->formID
                    );

                    if ($competition) {
                        if (is_array($mallID) && session()->has('activeChain')) {
                            $shopMGIDs = Shop::where('name', '=', session()->get('activeChain'))->pluck('shopMGID', 'mallID');

                            foreach ($mallID as $cur_mallID) {
                                if (isset($shopMGIDs[$cur_mallID])) {
                                    $formReqData['shopMGID'] = $shopMGIDs[$cur_mallID];
                                    $formReqData['formID'] = $competition->formID;
                                    $formReqData['userID'] = $userID;
                                    $formReqData['mallID'] = $cur_mallID;
                                    $formReqData['dateAdded'] = time();
                                    $formReqData['display'] = 'Y';

                                    $formRequest = FormRequest::create($formReqData);
                                }
                            }
                        } else {
                            $formReqData['formID'] = $competition->formID;
                            $formReqData['userID'] = $userID;
                            $formReqData['mallID'] = $mallID;
                            if (session()->has('activeShopID')) {
                                $formReqData['shopMGID'] = session()->get('activeShopID');
                                //dd(session()->get('activeShopID'));
                            } else {
                                $formReqData['shopMGID'] = Input::get('shopMGID');
                            }
                            $formReqData['dateAdded'] = time();
                            $formReqData['display'] = 'Y';

                            FormRequest::create($formReqData);

                        }
                        //Insert Into formRequest
                    }

                    //dd(json_decode($data['formCustomFields'], true)[0]);
                    if (!$user->hasAccess('admin')) {
                        $formFieldArray = MGForm::defaultFields();
                    } else {
                        $formFieldArray = $data['formCustomFields'];
                        //dd($formFieldArray);
                    }
                    //Insert into the table formFields

                    foreach ($formFieldArray as $k => $val_array) {
                        if ($user->hasAccess('admin')) {
                            $val_array = json_decode($val_array, true);
                        }
                        //Insert into the table formFieldSpecs
                        $val_array['formID'] = $competition->formID;

                        $formField = FormField::create($val_array);

                        //Get latest Entry and insert into formFieldSpecs
                        $formFieldSpecData['formFieldID'] = $formField->formFieldID;
                        if (isset($val_array['size'])) {
                            $formFieldSpecData['size'] = $val_array['size'];
                        }
                        $formFieldSpecsData['defaultValue'] = isset($val_array['defaultValue']) ? $val_array['defaultValue'] : null;
                        $formFieldSpecsData['maxlength'] = isset($val_array['maxlength']) ? $val_array['maxlength'] : '';
                        $formFieldSpecsData['rows'] = isset($val_array['rows']) ? $val_array['rows'] : '';
                        $formFieldSpecsData['cols'] = isset($val_array['cols']) ? $val_array['cols'] : '';
                        $formFieldSpecsData['wrap'] = isset($val_array['wrap']) ? $val_array['wrap'] : '';
                        $formFieldSpecsData['multiple'] = isset($val_array['multiple']) ? $val_array['multiple'] : '';
                        $formFieldElem['multiple'] = isset($val_array['multiple']) ? $val_array['multiple'] : '';
                        if (isset($val_array['fileTypes'])) {
                            $formFieldSpecData['fileTypes'] = $val_array['fileTypes'];
                        }
                        //$formFieldSpecData['fileTypes'] = $val_array['fileTypes[]'];

                        if (isset($val_array['checkName'])) {
                            $names = explode(',', $val_array['checkName']);

                            foreach ($names as $index => $name) {
                                $formElemData['name'] = $name;
                                $formElemData['formFieldID'] = $formField->formFieldID;
                                $checkNum = $index + 1;
                                if (isset($val_array["checkedSelected" . $checkNum])) {
                                    $formElemData['checkedSelected'] = 'Y';
                                } else {
                                    $formElemData['checkedSelected'] = 'N';
                                }
                                $formElemData['contentOrder'] = $checkNum;
                                FormFieldElements::create($formElemData);
                            }
                        }
                        $formSpecs = FormFieldSpec::create($formFieldSpecData);
                    }
                    if ($user->hasAccess('admin')) {
                        return response()->json($response_values);
                    } else {
                        return redirect(route('competitions.fields', $competition->formID));
                    }
                }
            } else {
                session()->flash('error', 'You do not have the required permissions to access that page!');
                return redirect('login');
            }
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function postCompetition($formID)
    {
        //Input::flash();
        $input = Input::all();

        $competition = MGForm::find($formID);
        $fields = $competition->formFields;
        $valid = MGForm::validateComp($input, $fields, $input);

        if ($valid->passes()) {
            $this->saveComp($formID, $competition, $input);
            return redirect(route('competition.success'));
        } else {
            //dd($valid->failed());
            //return response()->json($valid->failed());
            //dd($input);s
            //var_dump($valid->errors()->toArray());
            session()->flash('error', 'Please review the form!');
            return redirect("competitions/show/$formID")->withInput()->withErrors($valid);
        }
    }

    private function saveComp($formID, $competition, $input)
    {
        $ipAddress = request()->getClientIp();
        $userAgent = request()->server('HTTP_USER_AGENT');
        $feedback = FormFeedback::create(array(
                'formID' => $formID, 'REMOTE_ADDR' => $ipAddress,
                'HTTP_USER_AGENT' => $userAgent,
                'dateAdded' => DB::raw('now()')
            )
        );
        //die($input);
        if (isset($input['siteURL'])) {
            $redirectURL = $input['siteURL'];
        } else {
            $redirectURL = "";
        }
        $entry = array();
        //if a file was sent trough
        $files = Input::file();
        foreach ($files as $name => $file) {
            if (Input::hasFile($name)) {
                $destinationPath = base_path('uploadimages') . '/competiton' . $formID;
                $input[$name] = Config::get('app.url') . '/uploadimages/competiton' . $formID . '/' . uploadImage($file, $destinationPath);
            }
        }
        foreach ($competition->formFields as $formField) {
            $field = 'fieldID' . $formField->formFieldID;

            if (isset($input[$field]) && $input[$field] != '') {
                $entryFields = array(
                    'feedbackID' => $feedback->feedbackID,
                    'fieldName' => $formField->name,
                    'value' => $input[$field],
                    'formFieldID' => $formField->formFieldID,
                );

                FormFeedbackValues::create($entryFields);
                $entry[] = $entryFields;
            }
        }


        $data['competition'] = $competition;
        $data['entries'] = $entry;
        if (isset($input["siteURL"])) {
            $data['competitionURL'] = $input["siteURL"];
        } else {
            $data['competitionURL'] = "no valid URL";
        }
        //send mail

        Mail::send('emails.competition_entry', $data, function ($message) use ($competition) {
            //$message->from('tanja3hlers@gmail.com');
            $message->from('info@mallguide.co.za');
            $message->to($competition->recipient)->subject($competition->subject);
        });

        return $redirectURL;
    }

    public function remoteCompetition($formID)
    {
        //Input::flash();
        $input = Input::all();

        $competition = MGForm::with('formFields')->find($formID);
        $fields = $competition->formFields;
        $valid = MGForm::validateComp($input, $fields);

        if ($valid->passes()) {
            $URL = $input['siteURL'];
            $this->saveComp($formID, $competition, $input);
            //redirect back where the competition came from
            return redirect($URL . "&success&message=sent");
        } else {
            $URL = $input['siteURL'];
            //dd($URL);
            $messages = $valid->messages();
            //dd($fields);
            $message = "";
            foreach ($fields as $row) {
                if ($messages->has("fieldID" . $row->formFieldID)) {
                    if ($message != "") {
                        $message .= "<br />";
                    }
                    $message .= $row->name . ": " . $messages->first("fieldID" . $row->formFieldID);

                }
            }
            $message = urlencode($message);
            //redirect back where the competition came from
            return redirect($URL . "&error=$message");
        }
    }

    public function getCompetitionSuccess()
    {
        $data['heading'] = 'Thank You For Entering';
        return view('competitions/success', $data);
    }

    public function getEdit($id = null)
    {

        //Get the current user's id.
        Sentinel::check();
        $user = Sentinel::getUser();

        //Do they have admin access?
        if ($user->hasAccess('admin')) {
            $form = MGForm::find($id);
            return view('competitions/edit', array('form' => $form, 'user' => $user));
        } else {
            session()->flash('error', 'You do not have the required permissions to access that page!');
            return redirect('login');
        }
    }

    public function getClearTerms($formID)
    {
        //Get the current user's id.
        Sentinel::check();
        $user = Sentinel::getUser();

        //Do they have admin access?
        if ($user->hasAccess('admin')) {
            $form = MGForm::find($formID);
            $form->TsandCsFile = "";
            $form->save();

            return;
        } else {
            return FALSE;
        }
    }

    public function getDelete($id = null)
    {

        //Get the current user's id.
        Sentinel::check();
        $user = Sentinel::getUser();

        //Do they have admin access?
        if ($user->hasAccess('admin')) {
            $formRequest = FormRequest::where('formID', '=', $id)->delete();
            $form = MGForm::find($id)->delete();

            //success!
            session()->flash('success', 'The Competition has been deleted!');
            return redirect('/competitions');
        } else {
            session()->flash('error', 'You do not have the required permissions to access that page!');
            return redirect('login');
        }

    }


    /**************************************************/
    /*Portal Functions
    /**************************************************/
    public function getList()
    {
        if (Sentinel::check()) {
            $the_date = date('Y-m-d');
            $data = array();
            if (session()->has('activeMallID')) {
                $mallID = session()->get('activeMallID');
            } elseif (session()->has('activeShopID')) {
                $shop = Shop::with('competitions')->find(session()->get('activeShopID'));
                $data['pastComp'] = $shop->pastCompetitions;
                $data['currentComp'] = $shop->curCompetitions;

            } elseif (session()->has('activeChain')) {

                $userChain = session()->get('activeChain');
                $shopIDs = Shop::where('name', 'LIKE', '%' . $userChain . '%')->pluck('shopMGID', 'shopMGID');
                $formIDs = FormRequest::whereIn('shopMGID', $shopIDs)->pluck('formID');
                if (sizeof($formIDs) > 0) {
                    $data['pastComp'] = MGForm::whereIn('formID', $formIDs)->where("endDate", "<", $the_date)->orderBy('endDate', 'desc')->orderBy('startDate', 'desc')->get();
                    $data['currentComp'] = MGForm::whereIn('formID', $formIDs)->where("endDate", ">=", $the_date)->orderBy('endDate', 'desc')->orderBy('startDate', 'desc')->get();
                } else {
                    $data['pastComp'] = array();
                    $data['currentComp'] = array();
                }
            }


            if (isset($mallID)) {
                $mall = ShoppingMall::find($mallID);
                $data['pastComp'] = $mall->pastCompetitions;
                $data['currentComp'] = $mall->curCompetitions;
            }
            return view('portal/competitions/list', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getResponses($formID)
    {

        if (Sentinel::check()) {
            $formField = DB::table('formFields')->where('formID', '=', $formID)->where('emailField', '=', 'Y')->select('formFieldID')->get();

            $formFieldID = 0;
            foreach ($formField as $field) {
                $formFieldID = $field->formFieldID;
            }

            $entries = DB::table('formFeedbackValues')->select('formFeedback.dateAdded as date', 'formFeedbackValues.value as email', 'formFeedbackValues.feedbackID')->join('formFeedback', 'formFeedback.feedbackID', '=', 'formFeedbackValues.feedbackID')->where('formFieldID', '=', $formFieldID)->get();
            if ($entries) {
                foreach ($entries as $idx => $entry) {
                    $entries[$idx]->winner = 0;
                    $winner = DB::table('competitionWinners')->select('*')->where('competitionWinners.feedbackID', '=', $entry->feedbackID)->first();
                    if ($winner) {
                        $entries[$idx]->winner = 1;
                    }
                }
            }

            $data['respondents'] = $entries;
            $data['formID'] = $formID;

            return view('portal/competitions/responses', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getViewFeedback($id)
    {
        if (Sentinel::check()) {
            $data['feedbackdata'] = DB::table('formFeedbackValues')->where('feedbackID', '=', $id)->get();

            return view('/portal/competitions/feedback', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function postInsertRemote()
    {
        $input = Input::all();

        //
        //if(!$input['mallID']){
        //    App::abort(404);
        //}

        $data['pageID'] = 0;
        $data['recipient'] = Input::get('recipient');
        $data['subject'] = Input::get('subject');
        $data['description'] = Input::get('description');
        $data['dateAdded'] = Carbon::now()->toDateString();
        $data['activated'] = Input::get('activated');
        $data['formType'] = 'Competition';

        $data['startDate'] = Carbon::parse(Input::get('startDate'))->toDateTimeString();
        $data['endDate'] = Carbon::parse(Input::get('endDate'))->toDateTimeString();
        $displayFor = Input::get('displayFor');
        $data['displayFor'] = $displayFor ? $displayFor : 'M';
        $return_url = Input::get('return_url');


        $rules = array(
            'recipient' => 'required|email',
            'subject' => 'required',
            'startDate' => 'required|date',
            'endDate' => 'required|date'
        );

        $v = validator()->make($data, $rules);

        if ($v->passes()) {

            $thumbMaxWidth = 150;
            $largeMaxWidth = 450;

            $uploadDate = date("YmdHis");

            $file1 = Input::file('imageName');

            if (isset($file1)) {

                $destinationPath = base_path('uploadimages/mall_' . $input['mallID']);
                $filename1 = $file1->getClientOriginalName();
                $mime_type = $file1->getMimeType();
                $extension = $file1->getClientOriginalExtension();
                $upload_success = $file1->move($destinationPath, $filename1);

                //Get Name with out the extension
                $info = pathinfo($filename1);
                $file_name = $info['filename'];

                Image::make($destinationPath . '/' . $filename1)->resize($thumbMaxWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $file_name . '_' . $uploadDate . '_thumbnail.' . $extension);
                Image::make($destinationPath . '/' . $filename1)->resize($largeMaxWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $file_name . '_' . $uploadDate . '_large.' . $extension);

                $data['thumbnail'] = $file_name . '_' . $uploadDate . '_thumbnail.' . $extension;
                $data['image'] = $file_name . '_' . $uploadDate . '_large.' . $extension;

            }

            $competition = DB::table('forms')->insertGetId($data);

            if ($competition) {

                $formReqData['formID'] = $competition;
                $formReqData['mallID'] = $input['mallID'];
                $formReqData['shopMGID'] = isset($input['shopMGID']) ? $input['shopMGID'] : 0;
                $formReqData['dateAdded'] = date('Y-m-d H:i:s');
                $formReqData['display'] = ($data['activated'] == 'Y') ? $data['activated'] : 'N';

                //Insert Into formRequest
                $formRequest = DB::table('formRequest')->insertGetId($formReqData);

                //Insert into the table formFields
                $formFieldArray = array('E-mail address', 'Name', 'Surname', 'Cell', 'Telephone');
                foreach ($formFieldArray as $k => $val) {

                    //Insert into the table formFieldSpecs
                    $formFieldData['formID'] = $competition;
                    $formFieldData['name'] = $val;
                    $formFieldData['typeOfField'] = 'text';
                    $formFieldData['contentOrder'] = $k + 1;

                    if ($k < 3) {
                        $formFieldData['mandatory'] = 'Y';
                    } else {
                        $formFieldData['mandatory'] = 'N';
                    }

                    if ($k == 0) {
                        $formFieldData['emailField'] = 'Y';
                    } else {
                        $formFieldData['emailField'] = 'X';
                    }

                    $formFieldID = DB::table('formFields')->insertGetId($formFieldData);

                    //Get latest Entry and insert into formFieldSpecs
                    $formFieldSpecData['formFieldID'] = $formFieldID;
                    $formFieldSpecData['defaultValue'] = '';
                    $formFieldSpecData['maxlength'] = '';
                    $formFieldSpecData['size'] = 200;
                    $formFieldSpecData['rows'] = '';
                    $formFieldSpecData['cols'] = '';
                    $formFieldSpecData['wrap'] = '';
                    $formFieldSpecData['multiple'] = '';

                    $formSpecs = DB::table('formFieldSpecs')->insert($formFieldSpecData);

                }

                $data['competition'] = $competition;


                if ($return_url) {
                    return redirect($return_url . '?status=1');
                } elseif (isset($referrer)) {
                    return redirect($referrer . '?status=1');
                } else {
                    return redirect(Request::server('HTTP_REFERER'));
                }
            }

        } else {
            if ($return_url) {
                return redirect($return_url . '?status=0');
            } elseif (isset($referrer)) {
                return redirect($referrer . '?status=0');
            } else {
                return redirect(Request::server('HTTP_REFERER'));
            }
            //return redirect('competitions/create/'.$mallID)->withErrors($v)->withInput();
        }


    }

    public function postRemoteUpdate($formID = null)
    {

//        dd(Input::all());
        if (!$formID && Input::has('formID')) {
            $formID = Input::get('formID');
        }

        $mallID = Input::get('mallID');

        if (!$formID) {
            App::abort(404);
        }

        $data['pageID'] = 0;
        $referrer = request()->server('HTTP_REFERER');
        $data['recipient'] = Input::get('recipient');
        $data['subject'] = Input::get('subject');
        $data['description'] = Input::get('description');

        $data['activated'] = Input::get('activated');
        $data['formType'] = 'Competition';
        $data['startDate'] = Input::get('startDate') ? Input::get('startDate') : Carbon::parse(Input::get('year') . '-' . Input::get('month') . '-' . Input::get('day'))->toDateTimeString();
        $data['endDate'] = Input::get('endDate') ? Input::get('endDate') : Carbon::parse(Input::get('yearEnd') . '-' . Input::get('monthEnd') . '-' . Input::get('dayEnd'))->toDateTimeString();
        $data['displayFor'] = Input::get('displayFor', 'M');
        $data['thanksMsg'] = Input::get('thanksMsg');
        $data['noForm'] = Input::get('noForm');
        $return_url = Input::get('return_url');

        $rules = array(
            'recipient' => 'required|email',
            'subject' => 'required',
        );

        $v = validator()->make($data, $rules);

        if ($v->passes()) {

            $thumbMaxWidth = 150;
            $largeMaxWidth = 450;

            $uploadDate = date("YmdHis");

            $file1 = Input::file('imageName');

            if (isset($file1)) {

                $destinationPath = base_path('uploadimages') . '/mall_' . $mallID;

                $filename1 = $file1->getClientOriginalName();
                $mime_type = $file1->getMimeType();
                $extension = $file1->getClientOriginalExtension();
                $upload_success = $file1->move($destinationPath, $filename1);

                //Get Name with out the extension
                $info = pathinfo($filename1);
                $file_name = $info['filename'];

                Image::make($destinationPath . '/' . $filename1)->resize($thumbMaxWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $file_name . '_' . $uploadDate . '_thumbnail.' . $extension);
                Image::make($destinationPath . '/' . $filename1)->resize($largeMaxWidth, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($destinationPath . '/' . $file_name . '_' . $uploadDate . '_large.' . $extension);

                $data['thumbnail'] = $file_name . '_' . $uploadDate . '_thumbnail.' . $extension;
                $data['image'] = $file_name . '_' . $uploadDate . '_large.' . $extension;

            }

            $data['formID'] = $formID;

            $competition = MGForm::find($formID);
            $competition->update($data);

            if ($competition) {
                $formRequest = FormRequest::where('formID', $formID)->first();

                $formReqData['display'] = Input::get('activated', 'N');


                $formRequest->update($formReqData);

                if ($return_url) {
                    return redirect($return_url . '?status=1');
                } elseif (isset($referrer)) {
                    return redirect($referrer . '?status=1');
                } else {
                    return redirect('competitions/list/' . $mallID);
                }
            }

        } else {
            if ($return_url) {
                return redirect($return_url . '?status=0');
            } elseif (isset($referrer)) {
                return redirect($referrer . '?status=0');
            } else {
                return redirect('competitions/list/' . $mallID);
            }

        }
    }

    /************************************************************/
    /*Front End Functions
     /***********************************************************/
    public function getPopulateTable($mallID)
    {
        if ($mallID) {
            $formRequests = FormRequest::where('mallID', '=', $mallID)->get();
        }
        $toRet = array();
        if (count($formRequests)) {
            foreach ($formRequests as $formRequest) {
                $toRet[] = $formRequest->MGForm->toArray();
            }
        }

        return response()->view('partials/competitionsTable', array('competitions' => $toRet));
    }

    public function getAdd()
    {

        //Get the current user's id.
        Sentinel::check();
        $user = Sentinel::getUser();

        //Do they have admin access?
        if ($user->hasAccess('admin')) {
            return view('/competitions/create');
        } else {
            session()->flash('error', 'You do not have the required permissions to access that page!');
            return redirect('login');
        }
    }

    public function getFields($formID)
    {
        if (Sentinel::check()) {
            $data['form'] = MGForm::find($formID);
            $request = $data['form']->formRequest;
            $data['moves'] = getMoveList();
            $data['hasFields'] = ShoppingMall::hasFieldAccess($data['form']->formRequest->mallID);
            if ($data['form']->thumbnail) {
                $data['imagePath'] = url('uploadimages/' . $data['form']->thumbnail);
            }
            if ($data['form']->TsandCsFile) {
                $data['filePath'] = url('uploadimages/mall_' . $request->mallID . '/' . $data['form']->TsandCsFile);
            }

            $data['formFields'] = DB::table('formFields')->where('formID', '=', $data['form']->formID)->orderBy('contentOrder')->get();

            return view('portal/competitions/fields', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getChangeOrder($fieldID, $move)
    {
        $field = FormField::find($fieldID);
        $form = MGForm::find($field->formID);
        $fields = $form->formFields->sortBy('contentOrder');
        $contentOrders = $fields->pluck('contentOrder', 'contentOrder');
        $curOrder = $field->contentOrder;
        if ($move == "up") {
            $move_to = $contentOrders[$field->contentOrder] - 1;
            $other_field = FormField::where('contentOrder', '=', $move_to)->where('formID', '=', $field->formID)->first();
            $saved = $other_field->update(array('contentOrder' => $curOrder));
        } elseif ($move == "down") {
            $move_to = $contentOrders[$field->contentOrder] + 1;
            $other_field = FormField::where('contentOrder', '=', $move_to)->where('formID', '=', $field->formID)->first();
            $saved = $other_field->update(array('contentOrder' => $curOrder));
        } elseif ($move == "top") {
            foreach ($fields as $a_field) {
                if ($a_field->formFieldID != $fieldID && $a_field->contentOrder < $field->contentOrder) {
                    $nr = $a_field->contentOrder;
                    $a_field->contentOrder = $nr + 1;
                    $a_field->save();
                }
            }
            $move_to = 1;
        } elseif ($move == "bottom") {
            $other_field = $fields->last();
            $move_to = $other_field->contentOrder;
            foreach ($fields as $a_field) {
                if ($a_field->formFieldID != $fieldID && $a_field->contentOrder > $field->contentOrder) {
                    $nr = $a_field->contentOrder;
                    $a_field->contentOrder = $nr - 1;
                    $a_field->save();
                }
            }
        }

        $saved = $field->update(array('contentOrder' => $move_to));

        if ($saved) {
            session()->flash('success', 'fields have been reordered!');
        } else {
            session()->flash('failed', 'Could not be saved!');
        }
        return $field->formID;
    }

    public function postFields($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager') || $user->hasAccess('user') || $user->hasAccess('independent')) {

                $usr = User::find($user->id);
                $data['user'] = $user;

                $userMalls = $usr->malls;

                if (sizeof($userMalls) == 1) {
                    $mallID = $userMalls[0]['mallID'];
                }

                $recipient = Input::get('recipient');
                $subject = Input::get('subject');
                $description = Input::get('description');
                $startDate = Input::get('startDate');
                $endDate = Input::get('endDate');
                $activated = Input::get('activated');
                $noForm = Input::get('noForm');

                $data['recipient'] = Input::get('recipient');
                $data['subject'] = Input::get('subject');
                $data['startDate'] = Input::get('startDate');
                $data['endDate'] = Input::get('endDate');

                $rules = array(
                    'recipient' => 'required|email',
                    'subject' => 'required',
                    'startDate' => 'required|date',
                    'endDate' => 'required|date'
                );

                $v = validator()->make($data, $rules);

                if ($v->passes()) {
                    $uploadDate = date("YmdHis");

                    $file1 = Input::file('image');

                    if (isset($file1)) {

                        $destinationPath = base_path('uploadimages') . '/';

                        $filename1 = $file1->getClientOriginalName();
                        $mime_type = $file1->getMimeType();
                        $extension = $file1->getClientOriginalExtension();
                        $upload_success = $file1->move($destinationPath, $filename1);

                        $imageOriginalSize = getimagesize($upload_success)[0];

                        $thumbMaxWidth = 150;
                        $largeMaxWidth = 450;

                        //Get Name with out the extension
                        $info = pathinfo($filename1);
                        $file_name = $info['filename'];

                        Image::make($destinationPath . '/' . $filename1)->resize($thumbMaxWidth, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($destinationPath . '/' . $file_name . '_' . $uploadDate . '_thumbnail.' . $extension);

                        if ($imageOriginalSize > $largeMaxWidth) {

                            Image::make($destinationPath . '/' . $filename1)->resize($largeMaxWidth, null, function ($constraint) {
                                $constraint->aspectRatio();
                            })->save($destinationPath . '/' . $file_name . '_' . $uploadDate . '_large.' . $extension);
                        } else {
                            Image::make($destinationPath . '/' . $filename1)->save($destinationPath . '/' . $file_name . '_' . $uploadDate . '_large.' . $extension);
                        }


                        $thumbnail = $file_name . '_' . $uploadDate . '_thumbnail.' . $extension;
                        $image = $file_name . '_' . $uploadDate . '_large.' . $extension;

                        MGForm::where('formID', '=', $id)->update(array('recipient' => $recipient, 'subject' => $subject, 'description' => $description, 'startDate' => $startDate, 'endDate' => $endDate, 'activated' => $activated, 'noForm' => $noForm, 'thumbnail' => $thumbnail, 'image' => $image));
                    } else {
                        MGForm::where('formID', '=', $id)->update(array('recipient' => $recipient, 'subject' => $subject, 'description' => $description, 'startDate' => $startDate, 'endDate' => $endDate, 'activated' => $activated, 'noForm' => $noForm));
                    }

                    session()->flash('success', 'The competition has been updated');

                    if (sizeof($userMalls) == 1) {
                        return redirect('/competitions/list/' . $mallID);
                    } else {
                        return redirect('portal/mcomps');
                    }
                } else {
                    session()->flash('error', 'Please review the form!');
                    return redirect('competitions/fields/' . $id)->withErrors($v)->withInput();
                }

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function postDelete($id)
    {

        FormRequest::where('formID', '=', $id)->delete();
        return DB::table('forms')->where('formID', '=', $id)->delete();
    }

    /**
     * @param $fieldID
     * @return mixed
     */
    public function getDeleteField($fieldID)
    {

        //Get the current user's id.
        Sentinel::check();
        if (Sentinel::check()) {
            $user = Sentinel::getUser();

            $field = FormField::find($fieldID);
            $form = MGForm::find($field->formID);
            $fields = $form->formFields->sortBy('contentOrder');
            foreach ($fields as $a_field) {
                if ($a_field->formFieldID != $fieldID && $a_field->contentOrder > $field->contentOrder) {
                    $nr = $a_field->contentOrder;
                    $a_field->contentOrder = $nr - 1;
                    $a_field->save();
                }
            }
            //$field->FormfieldElements->delete();
            if (sizeof($field->FormfieldElements)) {
                foreach ($field->FormfieldElements as $elem) {
                    $elem->delete();
                }
            }
            $fieldSpec = $field->FormfieldSpecs;

            if ($fieldSpec) {
                $fieldSpec->delete();
            }
            //$field->FormfieldSpec->delete();
            $field->delete();
            //success!
            session()->flash('success', 'The Field has been deleted!');
            return $form->formID;
            //return redirect('/competitions');
        } else {
            session()->flash('error', 'You\'re not an Administrator!');
            return redirect('/');
        }
    }

    public function getDisqualifyFeedback($feedbackID)
    {
        if (Sentinel::check()) {
            $winner = DB::table('competitionWinners')->select('*')->where('feedbackID', '=', $feedbackID)->first();
            if ($winner) {
                $formID = $winner->formID;
                DB::table('competitionWinners')->where('feedbackID', '=', $feedbackID)->delete();

                session()->flash('success', 'Winner status was changed successfully.');
                return redirect(route('competitions.responses', $formID));
            }

            session()->flash('error', 'Winner status could not be changed.');
            return redirect(route('competitions.list', Sentinel::getUser()->mallID));
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }


    public function getRandomFeedback($formID)
    {
        if (Sentinel::check()) {
            $formField = DB::table('formFields')->where('formID', '=', $formID)->where('emailField', '=', 'Y')->select('formFieldID')->get();

            $formFieldID = 0;
            foreach ($formField as $field) {
                $formFieldID = $field->formFieldID;
            }

            $winner = DB::table('formFeedbackValues')->select('formFeedback.dateAdded as date', 'formFeedbackValues.value as email', 'formFeedbackValues.feedbackID')->join('formFeedback', 'formFeedback.feedbackID', '=', 'formFeedbackValues.feedbackID')->where('formFieldID', '=', $formFieldID)->orderBy(DB::raw('RAND()'))->first();
            if ($winner) {
                DB::table('competitionWinners')->insert(
                    array('formID' => $formID, 'feedbackID' => $winner->feedbackID, 'prizePosition' => 1)
                );
                session()->flash('success', 'Random winner was picked successfully.');
            } else {
                session()->flash('error', 'Random winner could not be picked.');
            }


            return redirect(route('competitions.responses', $formID));
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getExportFeedback($formID)
    {
        if (Sentinel::check()) {
            $formField = DB::table('formFields')->where('formID', '=', $formID)->where('emailField', '=', 'Y')->select('formFieldID')->get();

            $formFieldID = 0;
            foreach ($formField as $field) {
                $formFieldID = $field->formFieldID;
            }

            $entries = FormFeedbackValues::select('formFeedback.dateAdded as date', 'formFeedbackValues.value as email', 'formFeedbackValues.feedbackID')->join('formFeedback', 'formFeedback.feedbackID', '=', 'formFeedbackValues.feedbackID')->where('formFieldID', '=', $formFieldID)->get();
            if ($entries) {
                $counter = 1;
                $csv_arr[0] = array('Date', 'Name', 'Surname', 'Cell', 'Telephone', 'Email Address');
                foreach ($entries as $entry) {
                    if ($entry->date) {
                        $entry_user_info = DB::table('formFeedbackValues')->where('feedbackID', '=', $entry->feedbackID)->get();

                        if ($entry_user_info) {
                            $entry->name = '';
                            $entry->surname = '';
                            $entry->cell = '';
                            $entry->phone = '';

                            foreach ($entry_user_info as $entry_info) {
                                if ($entry_info->fieldName == 'Name') {
                                    $entry->name = $entry_info->value;
                                } elseif ($entry_info->fieldName == 'Surname') {
                                    $entry->surname = $entry_info->value;
                                } elseif ($entry_info->fieldName == 'Cell') {
                                    $entry->cell = $entry_info->value;
                                } elseif ($entry_info->fieldName == 'Telephone') {
                                    $entry->phone = $entry_info->value;
                                }
                            }
                            $csv_arr[$entry->email] = array($entry->date, $entry->name, $entry->surname, $entry->cell, $entry->phone, $entry->email);

                        }
                    }

                    $counter++;
                }

                Excel::create('downloads/competition_responses')
                    ->sheet('Competition Responses')
                    ->setTitle('competition_responses_' . $formID)
                    ->with($csv_arr)
                    ->export('csv');
            } else {
                session()->flash('error', 'There are no data to be exported.');

                return redirect(route('competitions.responses', $formID));
            }
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getMallIndex($mallID)
    {

        $mall = ShoppingMall::find($mallID);
        $data['mall'] = $mall;
        $data["competitions"] = $mall->curCompetitions;

        return view('competitions/mallcompetitions', $data);
    }

}
