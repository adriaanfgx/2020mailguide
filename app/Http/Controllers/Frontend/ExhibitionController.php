<?php


namespace App\Http\Controllers\Frontend;


use App\Exhibition;
use App\Http\Controllers\Controller;
use App\ShoppingMall;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ExhibitionController extends Controller
{
    public function __construct()
    {
        initSEO(array('title' => 'Exhibitions | ' . Config::get('app.name')));
    }

    public function getIndex()
    {
        return view('exhibitions.index');
    }

    public function getMallExhibitions($mallID)
    {
        storeSelectedDropdownMallInSession($mallID);

        if ($mallID == 0) {
            return Exhibition::select('exhibitions.*', 'shoppingMall.name as mall',
                'shoppingMall.province_id as mallProvinceID',
                'shoppingMall.city_id as mallCityID', 'provinces.name as mallProvince', 'cities.name as mallCity')
                ->join('shoppingMall', 'shoppingMall.mallID', '=', 'exhibitions.mallID')
                ->join('provinces', 'provinces.id', '=', 'shoppingMall.province_id')
                ->join('cities', 'cities.id', '=', 'shoppingMall.city_id')
                ->where('endDate', '>=', date("Y-m-d"))->orderBy('endDate')->paginate(45)->toJson();
        } else {
            return Exhibition::select('exhibitions.*', 'shoppingMall.name as mall')->join('shoppingMall', 'shoppingMall.mallID', '=', 'exhibitions.mallID')->where('exhibitions.mallID', '=', $mallID)->where('endDate', '>=', date("Y-m-d"))->orderBy('name')->paginate(45)->toJson();
        }
    }

    public function getProvinceFilter($provinceID)
    {

        $data = ['malls' => [], 'exhibitions' => []];

        $malls = ShoppingMall::whereHas('exhibitions', function ($q) {
            $q->where('endDate', '>=', date("Y-m-d"));
        })
            ->select('mallID', 'name')
            ->where('province_id', '=', $provinceID)
            ->where('activate', '=', 'Y')
            ->orderBy('name')
            ->get();

        if ($malls->count()) {
            foreach ($malls as $idx => $mall) {
                $malls[$idx]['totalExhibitions'] = $mall->curExhibitions->count();
            }
            $data['malls'] = $malls;
            $data['exhibitions'] = Exhibition::join('shoppingMall', 'shoppingMall.mallID', '=', 'exhibitions.mallID')->select('exhibitions.*', 'shoppingMall.name as mall')->where('shoppingMall.province_id', '=', $provinceID)->where('endDate', '>=', date("Y-m-d"))->paginate(15)->toArray();
        }

        return json_encode($data);
    }

    public function getList($mallID = NULL)
    {
        if (Sentinel::check()) {
            if (session()->has('activeMallID') && $mallID == NULL) {
                $mallID = session()->get('activeMallID');
            }
            $data['exhibitions'] = Exhibition::where('mallID', '=', $mallID)->get();

            return view('portal/exhibitions/list', $data);
        } else {
            session()->flash('error', 'You are not logged in..!!');
            return redirect('login');
        }
    }

    public function postDelete($id)
    {
        if (Sentinel::check()) {
            Exhibition::find($id)->delete();
        } else {
            session()->flash('error', 'You are not logged in ..!!');
        }
    }

    public function getCreate($mallID = NULL)
    {
        if (Sentinel::check()) {
            if (session()->has('activeMallID') && $mallID == NULL) {
                $data['mallID'] = session()->get('activeMallID');
            } else {
                $data['mallID'] = $mallID;
            }
            $data['categories'] = Exhibition::distinct('category')->where('category', '!=', '')->orderBy('category')->pluck('category', 'category')->toArray();
            $data['location'] = array('' => 'Select a location') + Exhibition::where('mallID', '=', $mallID)->distinct('location')->orderBy('location')->pluck('location', 'location')->toArray();

            return view('portal.exhibitions.create', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function postCreate(Request $request)
    {
        $input = $request->all();

        $mallID = $input['mall'];
        $referer = $input['referer'];
        $data['shopMGID'] = 0;
        $data['name'] = $input['name'];
        $data['startDate'] = $input['startDate'];
        $data['endDate'] = $input['endDate'];
        $data['exhibition'] = $input['exhibition'];
        $data['location'] = $input['location'];
        $data['mallID'] = $mallID;
        $data['display'] = 'Y';
        $data['category'] = $input['category'];

        $v = Exhibition::validate($input);

        if ($v->passes()) {

            $thumbMaxWidth = 150;
            $largeMaxWidth = 450;

            $file1 = $request->file('image');

            if (isset($file1)) {
                $destinationPath = base_path('uploadimages') . '/mall_' . $mallID;
                $img1 = uploadImage($file1, $destinationPath, $thumbMaxWidth, $largeMaxWidth, $large = true);
                $data['thumbnail1'] = $img1['thumbnail'];
                $data['image1'] = $img1['largeImage'];
            }

            Exhibition::insertGetId($data);
            session()->flash('success', 'The exhibition was added');
            if ($referer == Config::get('app.url') . '/portal/exhibitions/malls') {
                return redirect('portal/mexhibitions');
            } else {
                return redirect('/exhibitions/list/' . $mallID);
            }
        } else {
            session()->flash('error', 'Please review the form!');
            return redirect(route('exhibitions.create', $mallID))->withErrors($v)->withInput();
        }
    }

    public function getEdit($id)
    {

        if (Sentinel::check()) {
            $exhibition = Exhibition::find($id);
            $mallID = $exhibition->mallID;
            $data['exhibition'] = $exhibition;
            $data['categories'] = Exhibition::where('display', '=', 'Y')->orderBy('category')->pluck('category', 'category');
            $data['location'] = Exhibition::where('mallID', '=', $mallID)->orderBy('location')->pluck('location', 'location');

            return view('portal/exhibitions/edit', $data);

        } else {

            session()->flash('error', 'You are not logged in ..!!');
            return redirect('login');
        }

    }

    public function postEdit(Request $request, $id)
    {

        $input = $request->all();

        $referer = $input['referer'];
        $exbtn = Exhibition::find($id);
        $mallID = $input['mall'];
        $name = $input['name'];
        $start = $input['startDate'];
        $end = $input['endDate'];
        $exhibition = $input['exhibition'];
        $location = $input['location'];
        $display = $input['display'];
        $category = $input['category'];

        $v = Exhibition::validate($input);

        if ($v->passes()) {
            $thumbMaxWidth = 150;
            $largeMaxWidth = 450;

            $image = $request->file('image1');

            $data = array('name' => $name, 'startDate' => $start, 'endDate' => $end, 'exhibition' => $exhibition, 'location' => $location, 'display' => $display, 'category' => $category);

            if (isset($image)) {
                $destinationPath = Config::get('image_upload_path') . '/mall_' . $mallID;
                $oldImage1 = $exbtn->thumbnail1;
                removeOldImage($destinationPath, $oldImage1);

                $img1 = uploadImage($image, $destinationPath, $thumbMaxWidth, $largeMaxWidth, $large = true);
                $data['thumbnail1'] = $img1['thumbnail'];
                $data['image1'] = $img1['largeImage'];

                $exbtn->update($data);

            } else {
                $exbtn->update($data);
            }

            session()->flash('success', 'The exhibition was updated');

            if ($referer == Config::get('app.url') . '/portal/mexhibitions') {
                return redirect('portal/mexhibitions');
            } else {
                return redirect('/exhibitions/list/' . $mallID);
            }
        } else {
            session()->flash('error', 'Please review the form!');
            return redirect(route('exhibitions.edit', $id))->withErrors($v)->withInput();
        }
    }

    public function getByMallID($mallID)
    {
        $data['exhibitions'] = Exhibition::where('exhibitions.mallID', '=', $mallID)->select('exhibitions.*', 'shoppingMall.mallID', 'shoppingMall.name as mall')->join('shoppingMall', 'shoppingMall.mallID', '=', 'exhibitions.mallID')->where('endDate', '>=', date("Y-m-d"))->orderBy('exhibitions.name')->get();
        $exhibitionMalls = Exhibition::where('endDate', '>=', date('Y-m-d'))->pluck('mallID');
        $data['malls'] = ShoppingMall::whereIn('mallID', $exhibitionMalls)->orderBy('name')->pluck('name', 'mallID');
        $data['mallID'] = $mallID;
        return view('exhibitions.mallexhibitions', $data);
    }

    public function getViewExhibition($id)
    {
        $thisExhibition = Exhibition::find($id);
        $exhibitionMalls = Exhibition::where('endDate', '>=', date('Y-m-d'))->pluck('mallID');
        $data['malls'] = ShoppingMall::whereIn('mallID', $exhibitionMalls)->orderBy('name')->pluck('name', 'mallID');
        $data['mall'] = ShoppingMall::find($thisExhibition->mallID);
        $data['exhibition'] = $thisExhibition;

        initSEO(array('title' => pageTitle(array($thisExhibition->name, $data['mall']->name . ' Exhibitions', Config::get('app.name'))), 'description' => $thisExhibition->exhibition));

        return view('exhibitions.view_exhibition', $data);
    }

    public function getProvinceExhibitions($province)
    {
        $selectedProvince = trim($province);
        if ($selectedProvince == 'all') {
            return $provinceEvents = Exhibition::join('shoppingMall', 'shoppingMall.mallID', '=', 'exhibitions.mallID')->where('endDate', '>=', date("Y-m-d"))->paginate(15)->toJson();
        } else {
            return $provinceEvents = Exhibition::join('shoppingMall', 'shoppingMall.mallID', '=', 'exhibitions.mallID')->where('shoppingMall.province', '=', $selectedProvince)->where('endDate', '>=', date("Y-m-d"))->paginate(15)->toJson();
        }
    }
}
