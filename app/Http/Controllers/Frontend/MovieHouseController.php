<?php

namespace App\Http\Controllers\Frontend;

use App\AdminUser;
use App\Http\Controllers\Controller;
use App\MGCrud;
use App\MGForm;
use App\Movie;
use App\MovieShowing;
use App\User;
use App\WhatsShowing;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class MovieHouseController extends Controller
{
    public function __construct()
    {
        initSEO(array('title' => 'Mallguide | Events'));
    }

    public function getIndex()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('independent')) {

                $data['user'] = $user;
                $mallID = Session::get('activeMallID');
                $data['subscribed'] = NewsletterMovie::where('mallID', '=', $mallID)->count('userID');
                $data['countSchedules'] = WhatsShowing::where('mallID', '=', $mallID)->where('startDate', '<=', date('Y-m-d H:i:s'))->where('endDate', '>=', date('Y-m-d H:i:s'))->count('whatsShowingID');

                return View::make('moviehouses/index', $data);

            } else {

                Session::flash('error', 'You\'re not allowed to be here!');
                return Redirect::to('/');
            }

        } else {
            Session::flash('error', 'You\'re not logged in!');
            return Redirect::to('/login');
        }

    }

    /**
     * Add movie schedule
     */
    public function getAddMovieSchedule()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('independent')) {

                $data['user'] = $user;
                $mallID = Session::get('activeMallID');
                $data['movieFilter'] = array('all' => 'All', 'last50' => 'The 50 most recent movies', 'last100' => 'The 100 most recent movies', 'last200' => 'The 200 most recent movies', 'last400' => 'The 400 most recent movies ');
                $data['movies'] = Movie::orderBy('movieID', 'desc')->select('movieID', 'name', 'genre')->get();
                $lastSchedule = WhatsShowing::where('mallID', '=', $mallID)->orderBy('endDate', 'desc')->first();
                if ($lastSchedule) {
                    $date = $lastSchedule->endDate();
                } else {
                    $date = date('Y-m-d');
                }
                $data['preselectStart'] = date('Y-m-d', strtotime($date . ' + 1 days'));
                $data['preselectEnd'] = date('Y-m-d', strtotime($date . ' + 7 days'));
                $data['mallID'] = $mallID;

                return View::make('moviehouses/movieschedules/create', $data);

            } else {

                Session::flash('error', 'You\'re not allowed to be here!');
                return Redirect::to('/');
            }

        } else {
            Session::flash('error', 'You\'re not logged in!');
            return Redirect::to('/login');
        }

    }

    /**
     * Post add movie schedule
     */
    public function postAddMovieSchedule()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;

            if ($user->hasAccess('independent')) {

                $selectedMovies = Input::get('movieID');
                $showingData['mallID'] = Input::get('mallID');
                $showingData['startDate'] = Input::get('startDate');
                $showingData['endDate'] = Input::get('endDate');

                $whatsShowing = WhatsShowing::insertGetId($showingData);

                foreach ($selectedMovies as $movie) {

                    $movieShowing['whatsShowingID'] = $whatsShowing;
                    $movieShowing['movieID'] = $movie;
                    $movieShowing['times'] = Input::get($movie . '_times')[0];
                    $movieShowing['featured'] = 'N';

                    MovieShowing::insert($movieShowing);
                }

                Session::flash('success', 'Movie schedule successfully added !!');
                return Redirect::route('moviehouse.listmovieschedules');

            } else {

                Session::flash('error', 'You\'re not allowed to be here!');
                return Redirect::to('/');
            }

        } else {
            Session::flash('error', 'You\'re not logged in!');
            return Redirect::to('/login');
        }

    }

    /**
     * Edit Movie Schedule
     */
    public function getEditMovieSchedule($whatsShowingID)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('independent')) {

                $data = array();
                $whatsShowing = WhatsShowing::find($whatsShowingID);
                $data['whatsShowing'] = $whatsShowing;
                $data['whatsShowingMovies'] = $whatsShowing->moviesshowing;
                $data['movieFilter'] = array('all' => 'All', 'last50' => 'The 50 most recent movies', 'last100' => 'The 100 most recent movies', 'last200' => 'The 200 most recent movies', 'last400' => 'The 400 most recent movies ');
                $data['movies'] = Movie::orderBy('movieID', 'desc')->select('movieID', 'name', 'genre')->get();
                //die(var_dump($data['movies']));

                return view('moviehouses/movieschedules/edit', $data);

            } else {

                Session::flash('error', 'You\'re not allowed to be here!');
                return Redirect::to('/');
            }

        } else {
            Session::flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Post Edit Movie schedule
     */
    public function postEditMovieSchedule($id)
    {

        if (Sentinel::check()) {
//            print_r(Input::all());exit();
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('independent')) {

                //Get stored Movies For selected Schedule
                $storedMovies = MovieShowing::where('whatsShowingID', '=', $id)->lists('movieID');
                $newPosted = Input::get('movieID');

                //Check if any of the stored movies are missing from the array,delete them
                foreach ($storedMovies as $stored) {

                    if (!in_array($stored, $newPosted)) {

                        MovieShowing::where('whatsShowingID', '=', $id)->where('movieID', '=', $stored)->delete();
                    }
                }

                //Check if any of the newly submitted IDs are not in the already stored array, add them to the db
                $newMovies = array();
                foreach ($newPosted as $new) {

                    if (!in_array($new, $storedMovies)) {

                        array_push($newMovies, $new);
                    }
                }

                //Insert The new entries
                foreach ($newMovies as $newInsert) {

                    $movieShowing['whatsShowingID'] = $id;
                    $movieShowing['movieID'] = $newInsert;
                    $movieShowing['times'] = Input::get('times_' . $newInsert);
                    $movieShowing['featured'] = 'N';

                    MovieShowing::insert($movieShowing);

                }

                //Check if any of the new entries are already in the db,update them
                foreach ($newPosted as $new) {

                    if (in_array($new, $storedMovies)) {

                        MovieShowing::where('movieID', '=', $new)->where('whatsShowingID', '=', $id)->update(array('times' => Input::get('times_' . $new)));

                    }
                }

                Session::flash('success', 'The movie schedule has successfully been updated !!!');
                return Redirect::route('moviehouse.listmovieschedules');

            } else {

                Session::flash('error', 'You\'re not allowed to be here!');
                return Redirect::to('/');
            }

        } else {
            Session::flash('error', 'You\'re not logged in!');
            return Redirect::to('/login');
        }

    }

    /**
     * List Movie schedules
     */
    public function getListMovieSchedules()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;

            if ($user->hasAccess('independent')) {

                $usr = User::find($user->id);

                $mallID = $usr->malls[0]->mallID;
                $data['movieschedules'] = WhatsShowing::where('mallID', '=', $mallID)->select('whatsShowingID', 'mallID', 'startDate', 'endDate')->orderBy('startDate', 'desc')->get();

                return view('moviehouses/movieschedules/list', $data);

            } else {

                Session::flash('error', 'You\'re not allowed to be here!');
                return Redirect::to('/');
            }

        } else {
            Session::flash('error', 'You\'re not logged in!');
            return Redirect::to('/login');
        }
    }

    /**
     * List Competitions
     */
    public function getListCompetitions()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('independent')) {

                $usr = User::find($user->id);
                $userMall = $usr->malls[0]->mallID;

                $userComps = MGForm::select(DB::raw('forms.formID,pageID,recipient,subject,description,thanksMsg,submitBtnText,forms.dateAdded,activated,startDate,endDate,SUM(CASE WHEN formFeedback.formID = forms.formID THEN 1 ELSE 0 END) as count_resp,(SELECT COUNT(competitionWinners.compWinnerID) FROM competitionWinners WHERE competitionWinners.formID = forms.formID) as winners'))
                    ->leftjoin('formFeedback', 'formFeedback.formID', '=', 'forms.formID')
                    ->leftjoin('competitionWinners', 'competitionWinners.formID', '=', 'formFeedback.formID')
                    ->leftjoin('formRequest', 'formRequest.formID', '=', 'forms.formID')
                    ->where('endDate', '>=', date('Y-m-d'))
                    ->where('formType', '=', 'Competition')
                    ->where('formRequest.mallID', '=', $userMall)
                    ->groupBy('forms.formID')
                    ->get();

                return view('moviehouses/competitions/list', array('comps' => $userComps));

            } else {

                Session::flash('error', 'You\'re not allowed to be here!');
                return Redirect::to('/');
            }

        } else {
            Session::flash('error', 'You\'re not logged in!');
            return Redirect::to('/login');
        }

    }

    /**
     * Add Competition
     */
    public function getCreateCompetition()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('independent')) {

                $usr = User::find($user->id);
                $data['mallID'] = $usr->malls[0]->mallID;

                return View::make('moviehouses/competitions/create', $data);

            } else {

                Session::flash('error', 'You\'re not allowed to be here!');
                return Redirect::to('/');
            }

        } else {
            Session::flash('error', 'You\'re not logged in!');
            return Redirect::to('/login');
        }

    }

    /**
     * Post Create competition
     */
    public function postCreateCompetition()
    {
//        dd(Input::all());
        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('independent')) {

                $input = Input::all();

                $v = MGForm::validate($input);

                if ($v->passes()) {

                } else {
                    Session::flash('error', 'Please review the form!');
                    return Redirect::to('/moviehouse/competitions/create')->withErrors($v)->withInput();
                }

            } else {

                Session::flash('error', 'You\'re not allowed to be here!');
                return Redirect::to('/');
            }

        } else {
            Session::flash('error', 'You\'re not logged in!');
            return Redirect::to('/login');
        }

    }

    /**
     * Delete Movie schedule
     */
    public function postDeleteMovieSchedule($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user'] = $user;

            if ($user->hasAccess('independent')) {

                WhatsShowing::find($id)->delete();

            } else {

                Session::flash('error', 'You\'re not allowed to be here!');
                return Redirect::to('/');
            }

        } else {
            Session::flash('error', 'You\'re not logged in!');
            return Redirect::to('/login');
        }

    }

    /**
     * Update user password
     */
    public function getUpdatePassword($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('independent')) {

                $data['user'] = User::find($id);

                return View::make('moviehouses/updatepassword', $data);

            } else {

                Session::flash('error', 'You\'re not allowed to be here!');
                return Redirect::to('/');
            }

        } else {
            Session::flash('error', 'You\'re not logged in!');
            return Redirect::to('/login');
        }

    }

    /**
     * Post Update Password
     */
    public function postUpdatePassword($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('independent')) {

                $input = array(
                    'old_pass' => Input::get('old_pass'),
                    'new_pass' => Input::get('new_pass'),
                    'new_pass_confirmation' => Input::get('new_pass_confirmation')
                );

                // Set Validation Rules
                $rules = array(
                    'old_pass' => 'required',
                    'new_pass' => 'required|confirmed|regex:/\A(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{6,}\z/',
                    'new_pass_confirmation' => 'required|regex:/\A(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{6,}\z/'
                );

                $v = Validator::make($input, $rules);

                if ($v->passes()) {

                    if (Hash::check(Input::get('old_pass'), $user->password)) {

                        $user->password = Input::get('new_pass');

                        if ($user->save()) {
                            Session::flash('success', 'Your password has been updated.');
                            return Redirect::to('/moviehouse');
                        } else {
                            Session::flash('error', 'Your password could not be updated.');
                            return Redirect::to('/moviehouse');
                        }

                    } else {

                        Session::flash('error', 'The password you enterd does not match your current password.');
                        return Redirect::to('moviehouse.getUpdatePassword', $id)->withErrors($v)->withInput();
                    }

                } else {

                    return Redirect::route('moviehouse.getUpdatePassword', $id)->withErrors($v)->withInput();
                }

            } else {

                Session::flash('error', 'You\'re not allowed to be here!');
                return Redirect::to('/');
            }

        } else {
            Session::flash('error', 'You\'re not logged in!');
            return Redirect::to('/login');
        }

    }

    /**
     * Update User profile
     */
    public function getUpdateProfile($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('independent')) {

                $data['user'] = User::find($id);
                $data['provinces'] = array('' => 'Select an existing province') + Province::lists('name', 'id');
                $data['cities'] = AdminUser::distinct('city')->orderBy('city')->lists('city', 'city');

                return View::make('moviehouses/editprofile', $data);

            } else {

                Session::flash('error', 'You\'re not allowed to be here!');
                return Redirect::to('/');
            }

        } else {
            Session::flash('error', 'You\'re not logged in!');
            return Redirect::to('/login');
        }

    }

    /**
     * Post Update profile
     */
    public function postUpdateProfile($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('independent')) {

                $input = Input::all();

                $v = User::validate($input);

                if ($v->passes()) {

                    MGCrud::UpdateUserProfile($user);

                    Session::flash('success', 'Your details have been updated !!');
                    return Redirect::to('/moviehouse');

                } else {
                    Session::flash('error', 'Please review the form!');
                    return Redirect::route('moviehouse.getUpdateProfile', $id)->withErrors($v)->withInput();
                }

            } else {

                Session::flash('error', 'You\'re not allowed to be here!');
                return Redirect::to('/');
            }

        } else {
            Session::flash('error', 'You\'re not logged in!');
            return Redirect::to('/login');
        }

    }
}
