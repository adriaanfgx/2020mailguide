<?php

namespace App\Http\Controllers\Frontend;

use App\AdminUser;
use App\City;
use App\MovieMall;
use App\Province;
use App\Shop;
use App\ShoppingMall as ShoppingMallAlias;
use App\ShoppingMall;
use App\User;
use App\UserChain;
use App\UserMall;
use App\UserShop;
use Carbon\Carbon;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends Controller
{
    //
    public function __construct()
    {
        initSEO(array('title' => 'Sign Up | ' . Config::get('app.name')));
    }
    /**
     * Register Users on the front end
     */
    /**
     * User register page
     */
    public function getSignUp()
    {
        $data['userTypes'] = [
            'add_center_manager' => 'Centre Manager',
            'add_marketing_manager' => 'Marketing Manager',
            'existing_shop' => 'Shop Tenant',
            'chain_store' => 'Chain Store Manager',
            'new_mall' => 'New Mall - not on ' . Config::get('app.name'),
            'new_shop' => 'New Shop - not on ' . Config::get('app.name'),
            'movie_house' => 'New Movie House'
        ];

        return view('users.signup', $data);
    }

    /**
     * Existing mall register on mallguide
     */
    public function getMallRegister($for)
    {
        if ($for == "center") {
            $data['for'] = 'Mall Manager';
        } else {
            $data['for'] = 'Marketing Manager';
        }

        $data['mallList'] = ShoppingMallAlias::where('activate', '=', 'Y')->orderBy('name')->pluck('name', 'mallID');

        return view('users.registermall', $data);
    }

    /**
     * Existing tenant register shop
     */
    public function getExistingShopRegister()
    {
        $data['shops'] = Shop::orderBy('name')->pluck('name', 'shopMGID');
        $data['mallList'] = ShoppingMall::where('activate', '=', 'Y')->whereHas('shops', function ($q) {
            $q->where('display', '=', 'Y');
        })->orderBy('name')->pluck('name', 'mallID');

        return view('users.registerexistingshop', $data);
    }

    /**
     * Chain store registration landing page on front end
     */
    public function getChainAccess()
    {
        //GET LIST OF CHAINS (SHOP NAMES FOUND MORE THAN ONCE)
        $data['chains'] = Shop::orderBy('name')->groupBy('name')->havingRaw("COUNT('name') > 1")->where('display', '=', 'Y')->pluck('name', 'name');

        return view('users.chainaccess', $data);
    }

    /**
     * Used for registering a mall on the front end
     */
    public function getFrontMallRegister()
    {
        $data['provinces'] = Province::orderBy('id')->pluck('name', 'id');
        $data['admin_level'] = array('' => 'select...', 'Mall Manager' => 'Centre Manager', 'Marketing Manager' => 'Marketing Manager');
        $data['cities'] = ShoppingMall::distinct('city')->orderBy('city')->pluck('city', 'city');
        $data['classification'] = ShoppingMall::distinct('classification')->orderBy('classification')->pluck('classification', 'classification');

        return view('malls.register', $data);
    }

    /**
     * @param null $userId
     * @param null $activationCode
     * @return mixed
     */
    public function getActivate($userId = null, $activationCode = null)
    {
        if ($user = Sentinel::findUserById($userId)) {
            if ($activated = Activation::complete($user, $activationCode)) {
                $user->activated = 1;

                $user->save();

                session()->flash('success', 'Your account has been activated...');

                return redirect('/login');
            } else {
                // User activation failed
                session()->flash('error', 'There was a problem activating this account. Please contact the system administrator.');
                return redirect('/login');
            }

        } else {
            session()->flash('error', 'User does not exist.');
            return view('users.error');
        }
    }

    /**
     * Movie house registration on the front end
     */
    public function getMovieAccess()
    {

        $malls = ShoppingMall::where('activate', '=', 'Y')->distinct('name')->orderBy('name')->pluck('name', 'mallID')->all();

        $data['mallList'] = ['' => 'Select a mall...'] + $malls;
        return view('users.movieaccess', $data);
    }

    /**
     * Post Function for adding Users on the front end
     */
    public function postSignUp(Request $request)
    {

        $input = $request->all();

        $currentDate = Carbon::now()->subYears(16)->toDateTimeString();

        if (Sentinel::check()) {
            $currentUser = Sentinel::getUser();
        }

        $rules = array(
            'admin_level' => 'required',
            'email' => 'required|min:4|max:64|email|unique:users',
            'first_name' => 'required',
            'mallID' => 'sometimes',
            'password' => 'required|confirmed|regex:((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#!$%_]).{6,20})',
            'password_confirmation' => 'required',
            'birth_date' => 'date|before:' . $currentDate
        );

        $messages = [
            'admin_level.required' => 'Please select the User type!',
            'password.regex' => 'Your password does not match the minimum requirements, please choose a stronger password!'
        ];

        if ($input['admin_level'] == "Mall Manager") {
            $rules['mallID'] = "required";
        }
        if ($input['admin_level'] == "Marketing Manager") {
            $rules['mallIDs'] = "required";
        }
        if ($input['admin_level'] == "Chain Manager") {
            $rules['chain_name'] = "required";
        }
        $v = Validator::make($input, $rules, $messages);

        if ($v->passes()) {
            $newUser = Sentinel::register([
                'email' => $input['email'],
                'password' => $input['password'],
            ]);

            $activation = Activation::create($newUser);

            unset($input['password_confirmation']);
            $password = $input['password'];
            unset($input['password']);
            unset($input['mallSold']);
            $input['activation_code'] = $activation->getCode();
            $input['activated'] = false;
            $input['default_country_id'] = session()->get('default_country_id');
            unset($input['newCentreManagement']);
            unset($input['newMarketingTeam']);
            unset($input['newMarketingOrCentre']);
            unset($input['shop_mallID']);

            if (isset($input['whichGroup'])) {
                $movieHouse['whichGroup'] = $input['whichGroup'];
            }
            unset($input['whichGroup']);

            if (isset($input['numCinemas'])) {
                $movieHouse['numCinemas'] = $input['numCinemas'];
            }
            unset($input['numCinemas']);

            if (isset($input['mallIDs'])) {
                $mallIDs = $input['mallIDs'];
            }
            unset($input['mallIDs']);

            if (isset($input['shopMGID'])) {
                $shopMGIDs = $input['shopMGID'];
            }
            unset($input['shopMGID']);

            if (isset($input['chain_name'])) {
                $user_chain = $input['chain_name'];
            }
            unset($input['chain_name']);

            $input['parent_user_id'] = 0;

            if (isset($currentUser) && $currentUser->hasAccess('admin')) {
                $input['fgx_approved'] = 1;
            } else {
                $input['fgx_approved'] = 0;
            }

            //use our model
            $createdUser = User::find($newUser->id);

            $updatedUser = $createdUser->update($input);
            if ($updatedUser) {
                if (isset($input['mallID'])) {
                    $hasMovie = self::getHasMallHouse($input['mallID']);
                    if ($input['admin_level'] == 'Movie House' && $hasMovie == "No") {
                        $input['movieHouse'] = $movieHouse;
                        $input['movieHouse']['mallID'] = $input['mallID'];
                        $mall = ShoppingMall::find($input['mallID']);
                        $input['movieHouse']['name'] = $mall->name;
                        $input['movieHouse']['display'] = 'N';
                        $input['movieHouse']['province'] = $mall->province;
                        $input['movieHouse']['city'] = $mall->city;
                    }
                }

                //When adding a mall user, assign malls to the user
                if ($input['admin_level'] == 'Marketing Manager') {

                    $groupID = 1;
                    $adminRole = Sentinel::findRoleById($groupID);
                    $adminRole->users()->attach($newUser);

                    if (isset($mallIDs)) {
                        foreach ($mallIDs as $mallID) {
                            DB::table('user_malls')->insert(array('user_id' => $newUser->id, 'mall_id' => $mallID));
                        }
                    } else {
                        DB::table('user_malls')->insert(array('user_id' => $newUser->id, 'mall_id' => $input['mallID']));
                    }
                } elseif ($input['admin_level'] == 'Mall Manager') {

                    $groupID = 1;
                    $adminRole = Sentinel::findRoleById($groupID);
                    $adminRole->users()->attach($newUser);

                    DB::table('user_malls')->insert(array('user_id' => $newUser->id, 'mall_id' => $input['mallID']));

                } elseif ($input['admin_level'] == 'Shop Manager') {

                    $groupID = 4;
                    $adminRole = Sentinel::findRoleById($groupID);
                    $adminRole->users()->attach($newUser);

                    foreach ($shopMGIDs as $shopMGID) {
                        DB::table('user_shops')->insert(array('user_id' => $newUser->id, 'shop_id' => $shopMGID));
                    }
                } elseif ($input['admin_level'] == 'Chain Manager') {
                    $groupID = 4;
                    $adminRole = Sentinel::findRoleById($groupID);
                    $adminRole->users()->attach($newUser);

                    foreach ($user_chain as $chain) {
                        UserChain::insert(array('user_id' => $newUser->id, 'chain_name' => $chain));
                    }
                } elseif ($input['admin_level'] == 'FGX Staff') {
                    $groupID = 2;
                    $adminRole = Sentinel::findRoleById($groupID);
                    $adminRole->users()->attach($newUser);

                } elseif ($input['admin_level'] == 'Master') {
                    $groupID = 5;
                    $adminRole = Sentinel::findRoleById($groupID);
                    $adminRole->users()->attach($newUser);

                } elseif ($input['admin_level'] == 'Movie House') {
                    $groupID = 6;
                    $adminRole = Sentinel::findRoleById($groupID);
                    $adminRole->users()->attach($newUser);

                    if ($hasMovie == "No") {
                        MovieMall::create($input['movieHouse']);
                    }
                    DB::table('user_malls')->insert(array('user_id' => $newUser->id, 'mall_id' => $input['mallID']));
                }

                if (isset($currentUser) && $currentUser->hasAccess('admin')) {
                    $input['name'] = $input['first_name'];
                    $input['userId'] = $createdUser->id;
                    $input['password'] = $password;
                    $input['activationCode'] = $createdUser->activation_code;
                    self::sendActivationEmail($input);
                } else {
                    self::sendAddUserMail($input);
                }
                if (isset($currentUser) && $currentUser->hasAccess('admin')) {
                    session()->flash('success', "The user's info has been added.");
                    return redirect('/users/admin');

                } else {
                    //success!
                    session()->flash('success', 'Your info has been added. After your account has been approved an email will be sent with an activation link.');
                    return redirect('login');
                }

            } else {
                if (isset($currentUser) && $currentUser->hasAccess('admin')) {
                    session()->flash('success', "The user's info has been added, without access rights.");
                    return redirect('users/admin');
                } else {
                    return redirect('login');
                }
            }
        } else {
            session()->flash('error', 'There are some errors, please review the form .. ');

            return back()->withErrors($v)->withInput();
        }
    }

    /**
     * @param $mallID
     * @return string
     */
    public function getHasMallHouse($mallID)
    {
        $movieHouse = MovieMall::find($mallID);
        if ($movieHouse) {
            return "Yes";
        } else {
            return "No";
        }
    }

    private function sendActivationEmail($newUser)
    {
        //send email with activation link.
        Mail::send('emails.auth.activate', $newUser, function ($m) use ($newUser) {
            $m->from('webmaster@mallguide.co.za', Config::get('app.name'));
            $m->to($newUser['email'])->subject('Account activation | Mall Guide');
        });
    }

    private function sendAddUserMail($input)
    {
        //get shops managers
        //var_dump($input['mallID']);

        if (isset($input['mallID'])) {
            $users = User::whereHas('malls', function ($q) use ($input) {
                $q->where('mall_id', '=', $input['mallID']);
            })->get();

            foreach ($users as $user) {
                if (($user->admin_level == 'Mall Manager' || $user->admin_level == "Mall Manager") && $user->email != NULL) {
                    $input['to_cc'][$user->email] = $user->email;
                }
            }

            $mall = ShoppingMall::find($input['mallID']);
            $input['mall_name'] = $mall->name;
            $input['user_name'] = $mall->name;

        } elseif (isset($input['shop_name'])) {
            $input['user_name'] = $input['shop_name'];
        } else {
            $input['user_name'] = "";
        }

        //message for mall managers
        $input['the_message'] = $input['first_name'] . " has been added as a user on Mallguide. This user will only be able to access Mallguide after you have changed their user status to 'Approved' under the 'Users' tab on your control panel.";

        if (isset($input['movieHouse'])) {
            $second_message = "A new " . $input['movieHouse']['whichGroup'] . " movie house has been added.<br /><br />";

            $second_message .= '<table class="table">';
            if (isset($input['movieHouse']['whichGroup']) && $input['movieHouse']['whichGroup'] != NULL) {
                $second_message .= "<tr>
                    <td>
                        Cinema Name: &nbsp;
                    <td>
                        {$input['movieHouse']['whichGroup']}
                    </td>
                </tr>";
            }
            if (isset($input['movieHouse']['numCinemas']) && $input['movieHouse']['numCinemas'] != NULL) {
                $second_message .= "<tr>
                    <td>
                        Number of Cinemas:&nbsp;
                    <td>
                        {$input['movieHouse']['numCinemas']}
                    </td>
                </tr>";
            }
            $second_message .= "</table>";
            $input['second_message'] = $second_message;
        }

        if (isset($input['shopID'])) {
            $shop = ShoppingMall::find($input['shopID']);
            $input['shop_name'] = $shop->name;
        }

        Mail::send('emails.addUser', $input, function ($m) use ($input) {
            $m->from('webmaster@mallguide.co.za', Config::get('app.name'));
            $m->to("info@mallguide.co.za")->subject('User Registration | ' . Config::get('app.name'));
            if (isset($input['to_cc'])) {
                $m->cc($input['to_cc']);
            }
        });

        if (isset($input['email'])) {
            $input['the_message'] = "Your info, has successfully been added on Mallguide. After your account has been approved you will receive an email with an activation link. Please use these contact details for further queries: <a href='mailto:info@mallguide.co.za' style='font-weight:400; text-decoration:underline;'>info@mallguide.co.za</a>";
            $input['user_name'] = $input['first_name'];
            $email = $input['email'];
            Mail::send('emails.addUser', $input, function ($m) use ($email) {
                $m->from('webmaster@mallguide.co.za', Config::get('app.name'));
                $m->to($email)->subject('User Registration | ' . Config::get('app.name'));
            });
        }

    }

    /**
     * Register New mall
     */
    public function postRegisterMall(Request $request)
    {

        $input = $request->all();

//        unset($input['email']);
//        $input['email'] = Input::get('email');
//        $input['admin_level'] = Input::get('admin_level');
//        $input['password'] = Input::get('password');
//        $input['password_confirmation'] = Input::get('password_confirmation');
//        $input['first_name'] = Input::get('first_name');
//        $input['name'] = Input::get('name');
//        $input['logo'] = Input::file('logo');
//        $input['image1'] = Input::file('image1');
//        $input['image2'] = Input::file('image2');
//        $input['image3'] = Input::file('image3');
//        $input['centreManagerEmail'] = Input::get('centreManagerEmail');
//        $input['marketingManagerEmail'] = Input::get('marketingManagerEmail');
//        $input['marketingAssistantEmail'] = Input::get('marketingAssistantEmail');
//        $input['leasingEmail'] = Input::get('leasingEmail');

        $rules = array(
            'email' => 'required|min:4|max:64|email|unique:users',
            'password' => 'required|confirmed|regex:((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%_]).{6,20})',
            'password_confirmation' => 'required',
            'first_name' => 'required',
            'admin_level' => 'required',
            'gender' => 'required',
            'name' => 'required|min:4',
            'logo' => 'mimes:jpeg,png,gif|image',
            'image1' => 'mimes:jpeg,png,gif|image',
            'image2' => 'mimes:jpeg,png,gif|image',
            'image3' => 'mimes:jpeg,png,gif|image',
            'centreManagerEmail' => 'min:4|max:64|email',
            'marketingManagerEmail' => 'min:4|max:64|email',
            'marketingAssistantEmail' => 'min:4|max:64|email',
            'leasingEmail' => 'min:4|max:64|email'
        );

        $v = Validator::make($input, $rules);

        if ($v->passes()) {
            //First create a user
            $newUser = Sentinel::register([
                'email' => $input['email'],
                'password' => $input['password'],
            ]);

            $activation = Activation::create($newUser);

            //Get the activation code & prep data for email
            $data['activationCode'] = $activation->getCode();
            $data['email'] = $input['email'];
            $data['userId'] = $newUser->id;

            $createdUser = User::find($newUser->id);

            $createdUser->admin_level = $input['admin_level'];
            $createdUser->first_name = $input['first_name'];
            $createdUser->parent_user_id = 0;
            $createdUser->last_name = $input['last_name'];
            $createdUser->cell = $input['cell'];
            $createdUser->date_of_birth = $input['birth_date'];
            $createdUser->vat_number = $input['vat_number'];
            $createdUser->company = $input['company'];
            $createdUser->gender = $input['gender'];
            $createdUser->activation_code = $activation->getCode();
            $createdUser->save();

            $thisNewUser = Sentinel::findUserById($newUser->id);

            if ($createdUser->admin_level == 'Mall Manager') {
                $groupID = 1;

                $adminRole = Sentinel::findRoleById($groupID);
                $adminRole->users()->attach($newUser);
            }
            self::sendAddUserMail($input);

            //When user has been created Create a mall for the User
            $newMall['name'] = $input['name'];
            $newMall['classification'] = $input['classification'];
            $newMall['province_id'] = $input['province'];
            $newMall['city'] = $input['city'];
            $newMall['physicalAddress'] = $input['physicalAddress'];
            $newMall['postalAddress'] = $input['postalAddress'];
            $newMall['glaRetailM2'] = $input['glaRetailM2'];
            $newMall['website'] = $input['website'];
            $newMall['numShops'] = $input['numShops'];
            $newMall['coveredParkingBays'] = $input['coveredParkingBays'];
            $newMall['openParkingBays'] = $input['openParkingBays'];
            $newMall['totalParkingBays'] = $input['totalParkingBays'];
            $newMall['anchorTenants'] = $input['anchorTenants'];
            $newMall['specialFeature1'] = $input['specialFeature1'];
            $newMall['specialFeature2'] = $input['specialFeature2'];
            $newMall['specialFeature3'] = $input['specialFeature3'];
            $newMall['specialFeature4'] = $input['specialFeature4'];
            $newMall['specialFeature5'] = $input['specialFeature5'];
            $newMall['specialFeature6'] = $input['specialFeature6'];
            $newMall['specialFeature7'] = $input['specialFeature7'];
            $newMall['specialFeature8'] = $input['specialFeature8'];
            $newMall['specialFeature9'] = $input['specialFeature9'];
            $newMall['specialFeature10'] = $input['specialFeature10'];
            $newMall['centreManager'] = $input['centreManager'];
            $newMall['centreManagerTelephone'] = $input['centreManagerTelephone'];
            $newMall['centreManagerFax'] = $input['centreManagerFax'];
            $newMall['marketingCell'] = $input['marketingCell'];
            $newMall['centreManagerEmail'] = $input['centreManagerEmail'];
            $newMall['marketingConsultant'] = $input['marketingConsultant'];
            $newMall['marketingTelephone'] = $input['marketingTelephone'];
            $newMall['marketingFax'] = $input['marketingFax'];
            $newMall['marketingManager'] = $input['marketingManager'];
            $newMall['marketingManagerEmail'] = $input['marketingManagerEmail'];
            $newMall['marketingAssistant'] = $input['marketingAssistant'];
            $newMall['marketingAssistantEmail'] = $input['marketingAssistantEmail'];
            $newMall['marketingContractStart'] = $input['marketingContractStart'];
            $newMall['marketingContractEnd'] = $input['marketingContractEnd'];
            $newMall['leasingAgent'] = $input['leasingAgent'];
            $newMall['leasingContactPerson'] = $input['leasingContactPerson'];
            $newMall['leasingTelephone'] = $input['leasingTelephone'];
            $newMall['leasingFax'] = $input['leasingFax'];
            $newMall['leasingCell'] = $input['leasingCell'];
            $newMall['leasingEmail'] = $input['leasingEmail'];
            $newMall['shoppingCouncilMember'] = isset($input['shoppingCouncilMember']) ? $input['shoppingCouncilMember'] : '';
            $newMall['shoppingCouncilNumber'] = $input['shoppingCouncilNumber'];
            $newMall['activate'] = 'N';

            $mallInsert = ShoppingMall::insertGetId($newMall);

            $newMall['mallID'] = $mallInsert;
            $newMall['email'] = $input['email'];
            self::sendAddMallMail($newMall);

            $destinationPath = base_path('uploadimages' . '/mall_' . $mallInsert);

            $logo = $request->file('logo');
            $image1 = $request->file('image1');
            $image2 = $request->file('image2');
            $image3 = $request->file('image3');

            $thumbMaxWidth = 260;
            $largeMaxWidth = 500;

            if (isset($logo)) {
                $logo1 = uploadImage($logo, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['logo'] = $logo1['thumbnail'];
            }

            if (isset($image1)) {

                $img1 = uploadImage($image1, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['thumbnail1'] = $img1['thumbnail'];
                $input['image1'] = $img1['largeImage'];
            }

            if (isset($image2)) {

                $img2 = uploadImage($image2, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['thumbnail1'] = $img2['thumbnail'];
                $input['image1'] = $img2['largeImage'];
            }

            if (isset($image3)) {

                $img3 = uploadImage($image3, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $input['thumbnail1'] = $img3['thumbnail'];
                $input['image1'] = $img3['largeImage'];
            }

            if ($input['admin_level'] == 'Mall Manager') {
                DB::table('user_malls')->insert(['user_id' => $newUser->id, 'mall_id' => $mallInsert]);
            }

            session()->flash('success', 'Your details have been submitted, the mall guide team will look at your application and get back to you.<br />In the meantime please check your email for a verifivation email..');
            return redirect('/join');
        } else {
            session()->flash('error', 'Please review the form!');
            return redirect('/malls/register')->withErrors($v)->withInput();
        }
    }

    private function sendAddMallMail($input)
    {
        $the_message = "" . $input['name'] . " has been added as a mall on " . Config::get('app.name') . ". This mall will only display after you have changed the 'Active' & '" . Config::get('app.name') . "' status to yes on edit.";
        $input['the_message'] = $the_message;
        $mall = ShoppingMall::find($input['mallID']);
        $input['mall_name'] = $mall->name;
        $input['user_name'] = $mall->name;

        Mail::send('emails.addMall', $input, function ($m) use ($input) {
            $m->from('webmaster@mallguide.co.za', Config::get('app.name'));
            $m->to("info@mallguide.co.za")->subject('New Mall Registration | ' . Config::get('app.name'));
            if (isset($input['to_cc'])) {
                $m->cc($input['to_cc']);
            }
        });

        if (isset($input['email'])) {
            $input['the_message'] = "Your mall, " . $input['name'] . " has been added successfully on mallguide. This mall will only display after the Finegrafix has approved it. Please use these contact details for further queries: info@mallguide.co.za";
            $input['user_name'] = "";
            $email = $input['email'];
            Mail::send('emails.addMall', $input, function ($m) use ($email) {
                $m->from('webmaster@mallguide.co.za', Config::get('app.name'));
                $m->to($email)->subject('New Mall Registration | ' . Config::get('app.name'));
            });
        }
    }

    public function postLogout()
    {
        Sentinel::logout();
        session()->forget('mgUser');
        session()->forget('userMalls');
        session()->flush();
        return redirect('/');
    }

    public function getLogin()
    {
        //Check if there is an active user session
        if (!session()->has('mgUser')) {
            return view('users.login');
        } else {
            $userActive = session()->get('mgUser');
            $userID = $userActive['id'];
            $user = Sentinel::findById($userID);

            if ($user->hasAccess('admin')) {
                return redirect(route('admin.index'));
            } else {
                return redirect('/portal');
            }
        }
    }

    public function postLogin()
    {
        // Gather Sanitized Input
        $input = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );
        // Set Validation Rules
        $rules = array(
            'email' => 'required|min:4|max:64|email',
            'password' => 'required|min:6'
        );

        //Run input validation
        $v = Validator::make($input, $rules);

        if ($v->fails()) {
            session()->flash('error', 'Please review the form!');
            return redirect('/login')->withErrors($v)->withInput();
        } else {
            try {
                // Set login credentials
                $credentials = array(
                    'email' => $input['email'],
                    'password' => $input['password']
                );
                //Check for suspension or banned status
                if ($user = Sentinel::authenticate($credentials, true)) {
                    if ($user->fgx_approved) {
                        $user->last_login = date("Y-m-d H:i:s");
                        $user->last_activity = date("Y-m-d H:i:s");
                        $user->save();
                        $allocation = self::listAllocations($user->id);

                        $userMallDropList = [];
                        $userShopDropList = [];
                        if (isset($allocation['malls'][0])) {
                            $userMallDropList = ShoppingMall::whereIn('mallID', $allocation['malls'])->orderBy('name')->pluck('name', 'mallID');
                            session()->put('activeMallID', $allocation['malls'][0]);
                        }
                        if (isset($allocation['shops'][0])) {
                            $userShopDropList = Shop::whereIn('shopMGID', $allocation['shops'])->orderBy('name')->pluck('name', 'shopMGID');
                            session()->put('activeShopID', $allocation['shops'][0]);
                        }

                        if (isset(array_values($allocation['chains']->toArray())[0])) {
                            session()->put('activeChain', array_values($allocation['chains']->toArray())[0]);
                        }
                        $sessionUser = array
                        (
                            'id' => $user->id,
                            'first_name' => $user->first_name,
                            'last_name' => $user->last_name,
                            'email' => $user->email,
                            'shops' => $userShopDropList,
                            'malls' => $userMallDropList,
                            'chains' => $allocation['chains'],

                        );

                        session()->put('mgUser', $sessionUser);
                        session()->put('back', 'yes');

                        if ($user->hasAccess('admin')) {
                            return redirect('/admin/malls');

                        } else {
                            return redirect('/portal');
                        }
                    } else {

                        session()->flash('error', 'Your account still needs to be approved by the fgx team.');
                        return redirect('/login')->withErrors($v)->withInput();
                    }
                } else {
                    session()->flash('error', 'Invalid username or password.');
                    return redirect('/login')->withErrors($v)->withInput();
                }

//                dd($user->hasAccess('admin'));
            } catch (NotActivatedException $e) {
                session()->flash('error', 'Login failed. Please make sure your account is activated.');
                return redirect('/login');
            } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
                session()->flash('error', 'Invalid username or password.');
                return redirect('/login')->withErrors($v)->withInput();
            } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {

                session()->flash('error', 'You have not yet activated this account.');
                return redirect('/login')->withErrors($v)->withInput();
            } catch (Cartalyst\Sentry\Throttling\ UserSuspendedException $e) {
                session()->flash('error', 'You account has been suspended, please contact a administrator.');
                return redirect('/login')->withErrors($v)->withInput();
            } catch (ThrottlingException $e) {
                session()->flash('error', $e->getMessage());
                return redirect('/login');
            }
        }
    }

    public function listAllocations($id)
    {
        $shops = UserShop::where('user_id', '=', $id)->select('shop_id')->get()->toArray();
        $toRet['shops'] = array_flatten($shops);

        $malls = UserMall::where('user_id', '=', $id)->select('mall_id')->get()->toArray();
        $toRet['malls'] = array_flatten($malls);


        $chains = UserChain::where('user_id', '=', $id)->select('chain_name')->pluck('chain_name', 'chain_name');
        $toRet['chains'] = $chains;

        return $toRet;
    }

    public function getUpdatePassword($id)
    {
        if (Sentinel::check()) {
            $user = Sentinel::findById($id);
            $data['user'] = $user;
            return view('users.updatepassword', $data);
        } else {
            session()->flash('error', 'You are not logged in ..');
            return redirect('login');
        }
    }


    public function postUpdatePassword($id)
    {
        $user = Sentinel::findUserById($id);

        $input = array(
            'old_pass' => Input::get('old_pass'),
            'new_pass' => Input::get('new_pass'),
            'new_pass_confirmation' => Input::get('new_pass_confirmation')
        );

        // Set Validation Rules
        $rules = array(
            'old_pass' => 'required',
            'new_pass' => 'required|confirmed|min:6',
            'new_pass_confirmation' => 'required'
        );

        //Run input validation
        $v = Validator::make($input, $rules);
        if ($v->passes()) {
            if (Hash::check(Input::get('old_pass'), $user->password)) {

                $user->password = $input['new_pass'];

                if ($user->save()) {

                    session()->flash('success', 'Your password has been updated.');
                    return redirect('/portal');
                } else {

                    session()->flash('error', 'Your password could not be updated.');
                    return redirect('/portal');
                }
            } else {

                session()->flash('error', 'The password you entered does not match your current password.');
                return redirect('users/updatepassword/' . $user->id)->withErrors($v)->withInput();
            }
        } else {
            session()->flash('error', 'Please review the form!');
            return redirect(route('user.postUpdatepassword', $user->id))->withErrors($v)->withInput();
        }
    }

    public function getUpdateUserDetails()
    {
        if (Sentinel::check()) {
            $data['user'] = Sentinel::getUser();
            $data['cities'] = DB::table('adminUsers')->distinct('city')->orderBy('city')->pluck('city', 'city');

            if ($data['user']->hasAccess('admin')) {
                return view('admins/users/updateDetails', $data);
            } else {
                return view('users/userdetails', $data);
            }
        } else {
            session()->flash('error', 'You are not logged in ..');
            return redirect('login');
        }
    }

    public function getEdit($id)
    {
        if (Sentinel::check()) {
            //GET LIST OF CHAINS (SHOP NAMES FOUND MORE THAN ONCE)
            $data['chains'] = Shop::orderBy('name')->groupBy('name')->havingRaw("COUNT('name') > 1")->where('display', '=', 'Y')->pluck('name', 'name');

            try {
                //Get the current user's id.
                $data['user'] = User::find($id);

                $countryID = session()->get('default_country_id');
                $data['cities'] = City::whereHas('province', function ($q) use ($countryID) {
                    $q->where('country_id', '=', $countryID);
                })->pluck('name', 'id');

                $data['companyList'] = AdminUser::orderBy('company')->distinct('company')->pluck('company', 'company');

                if ($data['user']->admin_level == 'Mall Manager' || $data['user']->admin_level == 'Marketing Manager') {
                    $data['selectedMalls'] = $data['user']->malls->pluck('name', 'mallID');
                    //var_dump($userMalls);

                    $data['selectMall'] = "";
                    if ($data['user']->admin_level == 'Mall Manager') {
                        $arrays = Arr::divide($data['selectedMalls']->toArray());
                        if (isset($arrays[0][0])) {
                            $data['selectMall'] = $arrays[0][0];
                        }
                    } else {
                        $mallArray = Arr::divide($data['selectedMalls']->toArray());
                        if (isset($mallArray[0])) {
                            $data['selectMall'] = $mallArray[0];
                        }
                    }

                    $data['malls'] = ShoppingMall::orderBy('name')->distinct('name')->pluck('name', 'mallID');

                } elseif ($data['user']->admin_level == 'Chain Manager') {
                    $userChains = UserChain::where('user_id', '=', $id)->pluck('chain_name');
                    $data['userchain'] = $userChains;
                } elseif ($data['user']->admin_level == 'Shop Manager') {
                    $data["selectedMalls"] = $data['user']->shops->pluck('mallID');

                    $data["userShops"] = $data['user']->shops->pluck('name', 'shopMGID');

                    $data["shops"] = [];
                    if (count($data["selectedMalls"])) {
                        $data["shops"] = Shop::whereIn('mallID', $data["selectedMalls"])->pluck('name', 'shopMGID');
                    }

                    $arrays = Arr::divide($data["userShops"]->toArray());
                    $data['selectedShops'] = $arrays[0];
                }

                if (Sentinel::hasAccess("admin")) {
                    $data['adminLevel'] = array('' => 'Select Admin Level', 'Master' => 'Master', 'FGX Staff' => 'FGX Staff', 'Mall Manager' => 'Mall Manager', 'Marketing Manager' => 'Marketing Manager', 'Shop Manager' => 'Shop Manager', 'Chain Manager' => 'Chain Manager');
                    return view('users.edit')->with($data);

                } else {
                    if (Sentinel::hasAccess('manager')) {
                        $malls = session()->get('mgUser')['malls'];
                        $arrays = Arr::divide($malls->toArray());
                        $data['shopList'] = Shop::whereIn('mallID', $arrays[0])->orderBy('name')->distinct('name')->pluck('name', 'shopMGID');
                        $data['portalMalls'] = $malls;

                    }
                    return view('portal.users.edit')->with($data);
                }
            } catch (Cartalyst\Sentry\UserNotFoundException $e) {
                session()->flash('error', 'There was a problem accessing your account.');
                return redirect('login');
            }
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function postEdit($id)
    {
        // Gather Sanitized Input
        $input = array(
            'first_name' => Input::get('first_name'),
            'last_name' => Input::get('last_name'),
            'email' => Input::get('email')
        );

        $currentDate = date("Y-m-d H:i:s");

        // Set Validation Rules
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'alpha_spaces',
            'email' => 'required|min:4|max:64|email'
        );

        //Run input validation
        $v = Validator::make($input, $rules);

        if ($v->fails()) {
            // Validation has failed
            session()->flash('error', 'Please review the form!');
            return redirect('users/edit/' . $id)->withErrors($v)->withInput();
        } else {
            try {
                //Get the current user's id.
                Sentinel::check();
                $currentUser = Sentinel::getUser();

                //Do they have admin access?
                //if ( $currentUser->hasAccess('admin') )
                //{
                // Find the user using the user id
                $user = User::find($id);

                // Update the user details
                $user->first_name = Input::get('first_name');
                $user->last_name = Input::get('last_name');
                $user->cell = Input::get('cell');
                $user->tel_home = Input::get('tel_home');
                $user->tel_work = Input::get('tel_work');
                $user->fax = Input::get('fax');
                $user->postal_address = Input::get('postal_address');
                $user->postal_code = Input::get('postal_code');
                $user->birth_date = Input::get('birth_date');
                $user->default_country_id = Input::get('default_country_id', 190);
                $user->province_id = Input::get('province_id');
                $user->city_id = Input::get('city_id');
                $user->suburb = Input::get('suburb');
                $user->company = Input::get('company');
                $user->vat_number = Input::get('vat_number');
                $user->activated = Input::get('activated', 0);


                if ($user->admin_level == 'Mall Manager') {

                    $currentMalls = [];
                    $userMall = UserMall::where('user_id', '=', $user->id)->get();

                    foreach ($userMall as $mall) {
                        array_push($currentMalls, $mall->mall_id);
                    }

                    if (!in_array(Input::get('mallID'), $currentMalls)) {
                        //Do the Update
                        UserMall::where('user_id', '=', $user->id)->update(array('mall_id' => Input::get('mallID')));
                    }

                } elseif ($user->admin_level == 'Marketing Manager') {

                    $currentMalls = [];
                    $userMalls = UserMall::where('user_id', '=', $user->id)->get();

                    foreach ($userMalls as $mall) {
                        array_push($currentMalls, $mall->mall_id);
                    }

                    $newMallsAdded = array_diff(Input::get('mallID'), $currentMalls);

                    if (sizeof($newMallsAdded) > 0) {
                        //Insert the new malls
                        foreach ($newMallsAdded as $newMalls) {

                            UserMall::insert(array('user_id' => $id, 'mall_id' => $newMalls));
                        }
                    }

                    if (sizeof(Input::get('mallID')) < sizeof($currentMalls)) {

                        $mallsToRemove = array_diff($currentMalls, Input::get('mallID'));
                        foreach ($mallsToRemove as $remove) {
                            UserMall::where('user_id', '=', $id)->where('mall_id', '=', $remove)->delete();
                        }
                    }

                } elseif ($user->admin_level == 'Chain Manager') {
                    //delete current chains
                    foreach ($user->chainName as $userChain) {
                        $userChain->delete();
                    }
                    $userChains = Input::get('chain_name');

                    foreach ($userChains as $newChain) {
                        UserChain::insert(array('user_id' => $id, 'chain_name' => $newChain));
                    }
                } elseif ($user->admin_level == 'Shop Manager') {
                    foreach ($user->userShops as $userShop) {
                        $userShop->delete();
                    }
                    $userShops = Input::get('shopMGID');

                    foreach ($userShops as $newShop) {
                        UserShop::insert(array('user_id' => $id, 'shop_id' => $newShop));
                    }
                }
                // Update the user
                if ($user->save()) {
                    // User information was updated
                    session()->flash('success', 'Profile updated.');
                    if (Sentinel::hasAccess("admin")) {
                        return redirect('users/admin');
                    } else {
                        return redirect('portal/listusers');
                    }
                } else {
                    // User information was not updated
                    session()->flash('error', 'User profile could not be updated.');
                    return back();
                }

            } catch (Cartalyst\Sentry\Users\UserExistsException $e) {
                session()->flash('error', 'User already exists.');
                return redirect('users/edit/' . $id);
            } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
                session()->flash('error', 'User was not found.');
                return redirect('users/edit/' . $id);
            }
        }
    }


    public function postUpdateUserDetails($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $loggedInUser = Sentinel::getUser();
            $user = Sentinel::findById($id);

            $input = array(
                'email' => Input::get('email'),
                'first_name' => Input::get('first_name')
            );

            // Set Validation Rules
            $rules = array(
                'email' => 'required|min:4|max:64|email',
                'first_name' => 'required'
            );

            //Run input validation
            $v = Validator::make($input, $rules);

            if ($v->passes()) {

                $userData['first_name'] = Input::get('first_name');
                $userData['last_name'] = Input::get('last_name');
                $userData['company'] = Input::get('company');
                $userData['vat_number'] = Input::get('vat_number');
                $userData['postal_address'] = Input::get('postal_address');
                $userData['postal_code'] = Input::get('postal_code');
                $userData['province_id'] = Input::get('province_id');
                $userData['city'] = Input::get('city');
                $userData['suburb'] = Input::get('suburb');

                $userData['tel_work'] = Input::get('tel_work');
                $userData['tel_home'] = Input::get('tel_home');
                $userData['fax'] = Input::get('fax');
                $userData['cell'] = Input::get('cell');
                $userData['birth_date'] = Input::get('birth_date');
                $userData['gender'] = Input::get('gender');

                User::find($id)->update($userData);

                session()->flash('success', 'User details have been updated');

                if ($loggedInUser->id != $user->id) {
                    if ($loggedInUser->hasAccess('manager')) {
                        return redirect('/portal/listusers');
                    } elseif ($loggedInUser->hasAccess('user')) {

                        return redirect('/store/userlist');
                    }
                } else {
                    return redirect('/portal');
                }
            } else {
                session()->flash('error', 'Please review the form!');
                return redirect('users/updatedetails/' . $user->id)->withErrors($v)->withInput();
            }
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getResetpassword()
    {
        // Show the change password
        return view('users.reset');
    }

    public function postResetpassword()
    {
        // Gather Sanitized Input
        $input = array(
            'email' => Input::get('email')
        );

        // Set Validation Rules
        $rules = array(
            'email' => 'required|min:4|max:64|email'
        );

        //Run input validation
        $v = Validator::make($input, $rules);

        if ($v->fails()) {
            // Validation has failed
            session()->flash('error', 'Please review the form!');
            return redirect('users/reset-password')->withErrors($v)->withInput();
        } else {
            $user = Sentinel::findByCredentials(
                [
                    'login' => Input::get('email')
                ]
            );

            if ($user) {
                if ($user->password == '') {
                    $newPassword = Str::random(8);
                    $usr['email'] = $input['email'];
                    $usr['name'] = $user->first_name;
                    $usr['password'] = $newPassword;

                    $user->password = password_hash($newPassword, PASSWORD_DEFAULT);;
                    $user->activated = 1;
                    $user->save();
                    $existsInGroup = DB::table('role_users')->where('user_id', '=', $user->id)->get();

                    if (sizeof($existsInGroup) == 0) {

                        if ($user->chain_name !== '') {
                            DB::table('role_users')->insert(array('user_id' => $user->id, 'role_id' => 1));
                        } elseif ($user->shopMGID !== null) {
                            DB::table('role_users')->insert(array('user_id' => $user->id, 'role_id' => 4));
                        } else {
                            DB::table('role_users')->insert(array('user_id' => $user->id, 'role_id' => 3));
                        }

                    }

                    Mail::send('emails.mallguideaccess', $usr, function ($m) use ($usr) {
                        $m->from('webmaster@mallguide.co.za', Config::get('app.name'));
                        $m->to($usr['email'])->subject('Manager Portal Access | Mall Guide');
                    });

                    session()->flash('success', 'We have sent you an email with your login information.');
                    return redirect('/login');

                } else {

                    $data['email'] = $input['email'];
                    $data['resetCode'] = '123YYYY';// $user->getResetPasswordCode();
                    $data['id'] = $user->id;
                    $data['name'] = $user->name;

                    $user->reset_password_code = $data['resetCode'];
                    $user->save();

                    // Email the reset password to the user
                    Mail::send('emails.reset', $data, function ($m) use ($data) {
                        $m->from('webmaster@mallguide.co.za', Config::get('app.name'));
                        $m->to($data['email'])->subject('Password Reset Confirmation | Mall Guide');
                    });

                    session()->flash('success', 'We have sent you an email with password reset information.');
                    return redirect('/login');
                }
            } else {
                session()->flash('error', 'The email you entered is not registered with Mallguide..');
                return redirect('/login');
            }

        }
    }

    public function getPasswordReset($code, $id)
    {
        $user = Sentinel::findUserById($id);
        if ($user->checkResetPasswordCode($code)) {

            $data['user'] = $user;
            $data['code'] = $code;
            return view('users.passwordreset', $data);
        } else {
            "Code not In DB";
        }
    }

    public function postPasswordReset($code, $id)
    {
        $user = Sentinel::findById($id);

        $input = array(
            'new_pass' => Input::get('new_pass'),
            'new_pass_confirmation' => Input::get('new_pass_confirmation')
        );

        // Set Validation Rules
        $rules = array(
//            'new_pass' => 'required|confirmed|regex:/\A(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{6,}\z/',
            'new_pass' => 'required|confirmed|regex:((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#!$%_]).{6,20})',
            'new_pass_confirmation' => 'required'
        );

        $messages = [
            'new_pass.regex' => 'Your password does not match the minimum requirements, please choose a stronger password!'
        ];

        //Run input validation
        $v = Validator::make($input, $rules);

        if ($v->passes()) {

            if ($user->attemptResetPassword($code, $input['new_pass'])) {
                $user->activated = 1;
                $user->reset_password_code = '';
                session()->flash('success', 'Your password has been reset, Login.');
                return redirect('/login');
            } else {
                session()->flash('error', 'Not with this old code.');
                return redirect('passwordreset/' . $code . '/' . $id);
            }
        } else {
            session()->flash('error', 'Please review the form!');
            return redirect('passwordreset/' . $code . '/' . $id)->withErrors($v)->withInput();
        }
    }
}
