<?php


namespace App\Http\Controllers\Frontend;


use App\Promotion;
use App\Shop;
use App\ShoppingMall;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;

class PromotionController
{

    public function __construct()
    {
        initSEO(array('title' => 'Promotions | ' . Config::get('app.name')));

        $shops = Shop::orderBy('name')->pluck('name', 'shopMGID');

        view()->share(['shops' => $shops]);

    }

    public function getList()
    {

        $malls = activeCountryMalls();
        if (count($malls)) {
            $promotions = Promotion::whereIn('mallID', array_keys($malls->toArray()))->current()->get();
        } else {
            $promotions = [];
        }

        return view('promotions.list', ['promotions' => $promotions]);
    }

    public function getView($id)
    {

        $data['promotion'] = Promotion::find($id);
        $data['mall'] = ShoppingMall::find($data['promotion']->mallID);

        return view('promotions.view_promotion', $data);
    }

    public function getMallIndex($mallID)
    {
        $promotions = array();
        $mall = ShoppingMall::find($mallID);
        if ($mall) {
            $promotions = $mall->curPromotions;
        }

        return view('promotions.list', ['promotions' => $promotions]);
    }

    public function getPopulateTable($mallID, $provinceID = 0)
    {
        if ($mallID) {
            storeSelectedDropdownMallInSession($mallID);
            $mallID = explode(',', $mallID);
        } else {
            $malls = activeCountryMalls($provinceID);
            if (count($malls)) {
                $mallID = array_keys($malls->toArray());
            } else {
                $mallID = [];
            }
        }

        //get malls in the selected province
        $promotions = Promotion::whereIn('mallID', $mallID)->current()->get();

        if (empty($promotions)) {
            $promotions = [];
        }

        return view('partials.promotionsTable', ['promotions' => $promotions]);
    }

    public function getKeyWordSearch($keyWord)
    {
        $stringToSearch = trim($keyWord);

        $promotions = Promotion::where('promotions.promotion', 'like', '%' . $stringToSearch . '%')->where('promotion', "!=", "")->current()->orderBy('promotion')->get();

        return response()->view('partials/promotionsTable', array('promotions' => $promotions));
    }


    public function getListMallPromotions($mallID = NULL)
    {
        if (Sentinel::check()) {
            if ($mallID == NULL) {
                if (session()->has('activeMallID')) {
                    $mallID = session()->get('activeMallID');
                    $shopIDs = Shop::where('mallID', '=', $mallID)->pluck('shopMGID');
                    $data['currentPromo'] = Promotion::with('shop')->current()->whereIn('shopMGID', $shopIDs)->orderBy('startDate', 'desc')->get();
                    $data['pastPromo'] = Promotion::with('shop')->past()->whereIn('shopMGID', $shopIDs)->orderBy('startDate', 'desc')->get();
                } elseif (session()->has('activeShopID')) {
                    $shopID = session()->get('activeShopID');
                    $data['shop'] = Shop::find($shopID);
                    $data['pastPromo'] = $data['shop']->pastPromotions;
                    $data['currentPromo'] = $data['shop']->curPromotions;
                } elseif (session()->has('activeChain')) {
                    $chain = session()->get('activeChain');
                    $shopIDs = Shop::where('name', '=', $chain)->pluck('shopMGID');
                    $data['currentPromo'] = Promotion::with('shop')->current()->whereIn('shopMGID', $shopIDs)->orderBy('startDate', 'desc')->get();
                    $data['pastPromo'] = Promotion::with('shop')->past()->whereIn('shopMGID', $shopIDs)->orderBy('startDate', 'desc')->get();
                }
            } else {
                $shopIDs = Shop::where('mallID', '=', $mallID)->pluck('shopMGID');
                $data['currentPromo'] = Promotion::with('shop')->current()->whereIn('shopMGID', $shopIDs)->orderBy('startDate', 'desc')->get();
                $data['pastPromo'] = Promotion::with('shop')->past()->whereIn('shopMGID', $shopIDs)->orderBy('startDate', 'desc')->get();
            }
            $data['mall'] = ShoppingMall::with('shops')->find($mallID);
            return view('portal.promotions.list', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getCreateMallPromotion($mallID = NULL)
    {
        if (Sentinel::check()) {
            if (Sentinel::hasAccess('manager')) {
                $mallID = session()->get('activeMallID');
                $mall = ShoppingMall::find($mallID);
                $shops = $mall->shops;
                $data['shops'] = $shops;
            }
            if (Sentinel::hasAccess('user')) {
                $shopID = session()->get('activeShopID');
                $shop = Shop::find($shopID);
                $mallID = $shop->mallID;
                $mall = ShoppingMall::find($mallID);
            }
            $data['mall'] = $mall;
            return view('portal/promotions/create', $data);
        } else {
            session()->flash('error', 'You are not logged in!');
            return redirect('/login');
        }
    }

    public function postCreate()
    {
        $input = Input::all();

        if (session()->has('activeShopID')) {
            $input['shopMGID'] = session()->get('activeShopID');
        }
        $v = Promotion::validate($input);

        if ($v->passes()) {
            //Create Promo
            $returnVal = $this->doCreate($input);

            if ($returnVal == true) {
                session()->flash('success', 'Promotion Created Successfully..');
                if (Sentinel::check()) {
                    $user = Sentinel::getUser();
                    if ($user->hasAccess('admin')) {
                        return redirect('admin/promotions/' . Input::get('shopMGID'));
                    } elseif (session()->has('activeChain')) {
                        return redirect('chainstores/promotions');
                    } else {
                        return redirect('/promotions/portalList');
                    }
                }
            } else {
                session()->flash('error', 'Promotion could not be saved..');
                return redirect('/promotions/portalList');
            }
        } else {

            session()->flash('error', 'Errors were found, please review the form..');
            return back()->withErrors($v)->withInput();
        }
    }

    public function doCreate($input)
    {
        if (session()->has('activeShopID')) {
            $data['shopMGID'] = $data['shopID'] = session()->get('activeShopID');
            $shop = Shop::find($data['shopID']);
            $data['mallID'] = $shop->mallID;
        } elseif (session()->has('activeMallID')) {
            $data['mallID'] = session()->get('activeMallID');
            $data['shopID'] = isset($input['shopMGID']) ? $input['shopMGID'] : '';
        } else {
            $data['mallID'] = $input['mallID'];
            $data['shopID'] = isset($input['shopMGID']) ? $input['shopMGID'] : '';
        }

        $chain_name = isset($input['chain_name']) ? $input['chain_name'] : '';
        $data['shopMGID'] = isset($input['shopMGID']) ? $input['shopMGID'] : '';
        $data['startDate'] = $input['startDate'] . ' 23:59:59';
        $data['endDate'] = $input['endDate'] . ' 23:59:59';
        $data['promotion'] = $input['promotion'];
        $data['title'] = isset($input['title']) ? $input['title'] : '';

        $file1 = Input::file('image1');
        $file2 = Input::file('image2');

        $thumbMaxWidth = 150;
        $largeMaxWidth = 450;

        if (is_array($data['mallID'])) {

            $shops = Shop::where('name', '=', $chain_name)->whereIn('mallID', $input['mallID'])->pluck('shopMGID');

            if (isset($file1)) {

                $uploadDate = date("YmdHis");
                $file = $file1->getClientOriginalName();
                $extension = $file1->getClientOriginalExtension();

                $file1->move(base_path('uploadimages') . '/', $file);
            }

            if (isset($file2)) {

                $uploadDate = date("YmdHis");
                $file = $file2->getClientOriginalName();
                $extension = $file2->getClientOriginalExtension();

                $file2->move(base_path('uploadimages') . '/', $file);
            }

            foreach ($input['mallID'] as $k => $v) {

                $shop = Shop::find($shops[$k]);
                $data['mallID'] = $shop->mallID;
                $data['shopMGID'] = $shop->shopMGID;
                $data['shopID'] = $shop->shopMGID;

                $destinationPath = base_path('uploadimages') . '/mall_' . $v;

                if (!File::exists($destinationPath)) {
                    File::makeDirectory(base_path('uploadimages') . '/mall_' . $v);
                }


                $originPath = base_path('uploadimages') . '/';

                if (isset($file1)) {
                    Image::make($originPath . '/' . $file)->resize($thumbMaxWidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath . '/' . $file . '_' . $uploadDate . '_thumbnail.' . $extension);
                    Image::make($originPath . '/' . $file)->resize($largeMaxWidth, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($destinationPath . '/' . $file . '_' . $uploadDate . '_large.' . $extension);

                    $data['thumbnail1'] = $file . '_' . $uploadDate . '_thumbnail.' . $extension;
                    $data['image1'] = $file . '_' . $uploadDate . '_large.' . $extension;
                }

                if ($newPromotion = Promotion::insertGetId($data)) {

                    Promotion::find($newPromotion)->update(array('promotionsID' => $newPromotion));
                    $promoInserted = true;

                } else {
                    $promoInserted = false;
                }
            }
        } else {
            if (isset($file1)) {
                $destinationPath = base_path('uploadimages') . '/mall_' . $data['mallID'];

                $img1 = uploadImage($file1, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

                $data['thumbnail1'] = $img1['thumbnail'];
                $data['image1'] = $img1['largeImage'];
            }

            if (isset($file2)) {
                $destinationPath = base_path('uploadimages') . '/mall_' . $data['mallID'];

                $img2 = uploadImage($file2, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

                $data['thumbnail2'] = $img2['thumbnail'];
                $data['image2'] = $img2['largeImage'];
            }

            $promoInserted = Promotion::insert($data);
        }

        return $promoInserted;
    }

    public function getUpdate($id)
    {
        if (Sentinel::check()) {
            $data['promotion'] = Promotion::find($id);
            return view('portal/promotions/edit', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function postDelete($id)
    {
        Promotion::find($id)->delete();
    }

    public function postCopyPromotion($id)
    {
        $promotion = Promotion::find($id);
        Promotion::insert(array('shopMGID' => $promotion->shopMGID, 'shopID' => $promotion->shopID, 'startDate' => $promotion->startDate, 'endDate' => $promotion->endDate, 'promotion' => $promotion->promotion, 'title' => $promotion->title, 'mallID' => $promotion->mallID, 'thumbnail1' => $promotion->thumbnail1, 'image1' => $promotion->image1));
    }

    public function postAddRemote()
    {
        $mallID = Input::get('mallID');
        $referrer = Request::server('HTTP_REFERER');
        $data['shopMGID'] = Input::get('shopMGID') ? Input::get('shopMGID') : Input::get('shopID');
        $data['shopID'] = Input::get('shopMGID') ? Input::get('shopMGID') : Input::get('shopID');
        $data['startDate'] = Input::get('startDate') ? Input::get('startDate') : Carbon::parse(Input::get('year') . '-' . Input::get('month') . '-' . Input::get('day'))->toDateTimeString();
        $data['endDate'] = Input::get('endDate') ? Input::get('endDate') : Carbon::parse(Input::get('yearEnd') . '-' . Input::get('monthEnd') . '-' . Input::get('dayEnd'))->toDateTimeString();
        $data['title'] = Input::get('title');
        $data['promotion'] = Input::get('promotion');
        $data['mallID'] = $mallID;
        $return_url = Input::get('return_url');

        $input['image1'] = Input::file('imageName');

        $rules = array(
            'imageName' => 'mimes:gif,jpeg,png'
        );

        $v = validator()->make($input, $rules);

        if ($v->passes()) {

            $thumbMaxWidth = 150;
            $largeMaxWidth = 450;

            if (isset($input['image1'])) {
                $destinationPath = base_path('uploadimages') . '/mall_' . $mallID;

                $img1 = uploadImage($input['image1'], $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $data['thumbnail1'] = $img1['thumbnail'];
                $data['image1'] = $img1['largeImage'];
            }

            $promotion = Promotion::insertGetId($data);
            Promotion::find($promotion)->update(array('promotionsID' => $promotion));
            //Session::flash('success', 'The promotion was added');

            if ($return_url) {
                return redirect($return_url . '?status=1');
            } elseif (isset($referrer)) {
                return redirect($referrer . '?status=1');
            } else {
                return redirect('promotions/list/' . $mallID);
            }

        } else {
            if ($return_url) {
                return redirect($return_url . '?status=0');
            } elseif (isset($referrer)) {
                return redirect($referrer . '?status=0');
            } else {
                return redirect('promotions/list/' . $mallID);
            }
        }
    }

    public function postRemoteUpdate($id)
    {
        if (!$id && Input::has('promotionsMGID')) {
            $id = Input::get('promotionsMGID');
        }
        $promo = Promotion::find($id);
        if (!$promo) {
            App::abort(404);
        }

        $referrer = Request::server('HTTP_REFERER');
        $mallID = Input::get('mallID');
        $startDate = Input::get('startDate');
        $endDate = Input::get('endDate');
        $promotion = Input::get('promotion');
        $title = Input::get('title');
        $return_url = Input::get('return_url');

        $thumbMaxWidth = 150;
        $largeMaxWidth = 450;

        $file1 = Input::file('imageName');

        if (isset($file1)) {
            $destinationPath = base_path('uploadimages') . '/mall_' . $mallID;
            $oldImage1 = $promo->thumbnail1;
            removeOldImage($destinationPath, $oldImage1);

            $img1 = uploadImage($file1, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['thumbnail1'] = $img1['thumbnail'];
            $data['image1'] = $img1['largeImage'];

            Promotion::where('promotionsMGID', '=', $id)->update(array('startDate' => $startDate, 'endDate' => $endDate, 'promotion' => $promotion, 'title' => $title, 'thumbnail1' => $data['thumbnail1'], 'image1' => $data['image1']));
        } else {
            Promotion::where('promotionsMGID', '=', $id)->update(array('startDate' => $startDate, 'endDate' => $endDate, 'promotion' => $promotion, 'title' => $title));
        }

        if ($return_url) {
            return redirect($return_url . '?status=1');
        } elseif (isset($referrer)) {
            return redirect($referrer . '?status=1');
        } else {
            return redirect('promotions/list/' . $mallID);
        }
    }

    public function postUpdate($id)
    {
        $input = Input::all();
        $mallID = Input::get('mallID');
        $shopMGID = Input::get('shopMGID');

        $postingUrl = URL::previous();

        $v = Promotion::validate($input);

        if ($v->passes()) {
            $returnVal = $this->doUpdate($input, $id);
            if ($returnVal == 1) {
                //Return Success
                session()->flash('success', 'Promotion updated successfully !!!');
            } else {
                //Return Fail
                session()->flash('error', 'The promotion could not be updated !!!');
            }

            $user = Sentinel::getUser();
            if ($user->hasAccess('admin')) {

                return redirect('/admin/promotions/' . $shopMGID);
            } else {
                return redirect('/promotions/portalList');
            }
        } else {
            session()->flash('error', 'Errors were found, please review the form..');
            return redirect($postingUrl)->withErrors($v)->withInput();
        }
    }


    public function doUpdate($input, $id)
    {
        $promo = Promotion::find($id);
        $mallID = $promo->mallID;
        $data['startDate'] = $input['startDate'];
        $data['endDate'] = $input['endDate'];
        $data['promotion'] = $input['promotion'];
        $data['title'] = $input['title'];

        $thumbMaxWidth = 150;
        $largeMaxWidth = 450;

        $destinationPath = base_path('uploadimages') . '/mall_' . $mallID;

        $file1 = isset($input['image1']) ? $input['image1'] : null;
        $file2 = isset($input['image2']) ? $input['image2'] : '';

        if (isset($file1)) {

            $oldThumbNail = $promo->thumbnail1;
            $oldImage = $promo->image1;

            removeOldImage($destinationPath, $oldThumbNail);
            removeOldImage($destinationPath, $oldImage);

            $img1 = uploadImage($file1, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['thumbnail1'] = $img1['thumbnail'];
            $data['image1'] = $img1['largeImage'];
        }

        if (isset($file2) && is_object($file2)) {

            $oldThumbNail2 = $promo->thumbnail2;
            $oldImage2 = $promo->image2;

            removeOldImage($destinationPath, $oldThumbNail2);
            removeOldImage($destinationPath, $oldImage2);

            $img2 = uploadImage($file2, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['thumbnail2'] = $img2['thumbnail'];
            $data['image2'] = $img2['largeImage'];

        }
        $promoUpdated = Promotion::find($id)->update($data);

        return $promoUpdated;
    }


    public function getShow($id)
    {
        $promotion = Promotion::find($id);

        $mall = ShoppingMall::find($promotion->mallID);

        initSEO(array('title' => pageTitle(array('Promotion Details', $promotion->shop->name, $mall->name, Config::get('app.name'))), 'description' => metaDescription($promotion->promotion)));

        return view('promotions/view_promotion', array('promotion' => $promotion, 'mall' => $mall));
    }

}
