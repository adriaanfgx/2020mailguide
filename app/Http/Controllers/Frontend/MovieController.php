<?php

namespace App\Http\Controllers\Frontend;

use App\Movie;
use App\Review;
use App\Reviewer;
use App\ShoppingMall;
use App\WhatsShowing;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use GuzzleHttp\Psr7\Request as GuzzeRequest;

class MovieController extends Controller
{
    public function __construct()
    {
        initSEO(array('title' => 'Movies | ' . Config::get('app.name')));
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        return view('movies.index');
    }

    public function getMoreInfo($id)
    {

        $thisMovie = Movie::find($id);

        if (!$thisMovie) {
            App::abort(404);
        }

        $data['movie'] = $thisMovie;
        $data['youtubeID'] = '';

        //Query API for showtimes
        $showtimes = array();
        $client = new Client();

        try {
            $response = $client->get('http://movies.mallguide.global/api/v1/movie/' . $id . ' ?with=whatsshowing&whatsshowing.endDate=2019-08-05&operator[whatsshowing.endDate]=gte', [
                'headers' => [
                    'token' => Config::get('movie.token'),
                    'clientId' => Config::get('movie.client_id')
                ]
            ]);

            if ($movie_info = (json_decode(((string)$response->getBody())))) {
                if (isset($movie_info->data->whatsshowing)) {
                    $show_data = $movie_info->data->whatsshowing;
                    $data['youtubeID'] = $movie_info->data->YouTubeID;
                    foreach ($show_data as $showtime) {
                        if ($showtime->mallID) {
                            $show_mall = ShoppingMall::where('mallID', $showtime->mallID)->display()->active()->pluck('name');
                            if ($show_mall) {
                                if ($showtime->endDate >= date('Y-m-d')) {
                                    $mall_hash = slugify($show_mall) . '_' . $showtime->mallID;
                                    $showtimes[$mall_hash]['id'] = $showtime->mallID;
                                    $showtimes[$mall_hash]['name'] = $show_mall;
                                    $showtime_hash = substr(md5($showtime->startDate . $showtime->endDate), 0, 9);
                                    $showtimes[$mall_hash]['showtimes'][$showtime_hash]['start_date'] = $showtime->startDate;
                                    $showtimes[$mall_hash]['showtimes'][$showtime_hash]['end_date'] = $showtime->endDate;
                                    $showtimes[$mall_hash]['showtimes'][$showtime_hash]['times'] = $showtime->pivot->times;

                                    $showtimes[$mall_hash]['showdates'][] = $showtime->startDate;
                                    $showtimes[$mall_hash]['showdates'][] = $showtime->endDate;
                                }
                            }
                        }
                    }
                }

                if (count($showtimes)) {
                    foreach ($showtimes as $idx => $shows) {
                        $dates = $shows['showdates'];
                        unset($showtimes[$idx]['showdates']);
                        $showtimes[$idx]['shows']['start_date'] = min($dates);
                        $showtimes[$idx]['shows']['end_date'] = max($dates);
                        $show_times = $shows['showtimes'];
                        unset($showtimes[$idx]['showtimes']);

                        if (count($show_times)) {
                            foreach ($show_times as $time) {
                                $showtimes[$idx]['shows']['time'] = $time['times'];
                                break;
                            }
                        }
                    }

                    // sort by mall name
                    foreach ($showtimes as $key => $row) {
                        $malls[$key] = $row['name'];
                    }
                    array_multisort($malls, SORT_ASC, $showtimes);
                }
            }
        } catch (ClientException $e) {
        }

        $data['showtimes'] = $showtimes;

        $data['mallMovieData'] = Movie::whereHas('showing', function ($q) {
            $q->where('endDate', '>=', Carbon::now()->toDateTimeString());
        })
            ->orderBy('name', 'ASC')
            ->distinct()
            ->get();

        $data['cinemas'] = WhatsShowing::where('whatsShowing.endDate', '>=', date("Y-m-d H:i:s"))
            ->join('shoppingMall', 'shoppingMall.mallID', '=', 'whatsShowing.mallID')
            ->orderBy('shoppingMall.name')
            ->groupBy('whatsShowing.mallID')
            ->select('shoppingMall.name', 'shoppingMall.mallID')
            ->get()
            ->toArray();

        initSEO(array('title' => pageTitle(array($data['movie']->name, Config::get('app.name'))), 'description' => metaDescription($data['movie']->description)));

        return view('movies/readmore', $data);
    }

    public function getCinemaInfo()
    {
        return view('movies.cinema');
    }

    public function postReview(Request $request)
    {
        $input = $request->all();
        $v = Review::validate($input);

        if ($v->passes()) {

            $data = array();

            $data['name'] = $input['reviewer_name'];
            $data['surname'] = $input['surname'];
            $data['username'] = '';
            $data['password'] = '';
            $data['email'] = $input['reviewer_email'];
            $data['subscribe'] = Input::get('subscribe');
            $data['activate'] = 'Y';
            $data['userID'] = '0';
            $data['certified'] = 'N';

            $reviewerID = Reviewer::insertGetId($data);

            $reviewData = array();
            $reviewData['whatMovieID'] = $input['movieID'];
            $reviewData['reviewerID'] = $reviewerID;
            $reviewData['review'] = $input['review'];
            $reviewData['reviewDate'] = date("Y-m-d H:i:s");
            $reviewData['display'] = 'N';
            $reviewData['mallID'] = 0;
            $reviewData['score'] = $input['score'];
            $reviewData['featured'] = 'N';

            Review::insertGetId($reviewData);
            session()->flash('success', 'Review added successfully.');
            return redirect(\request()->server('HTTP_REFERER') . '#movieReview');
        } else {
            session()->flash('error', 'There are some errors, please review the form');
            return redirect(\request()->server('HTTP_REFERER') . '#movieReview')->withErrors($v)->withInput();
        }
    }

    public function getMallMovies($mallID)
    {

        //Query API for showtimes
        $movies = getMallAPIMovies($mallID, Input::get('show_date'));
        $mall_movies = $movies['movies'];
        $show_dates = $movies['dates'];
        $data['show_dates'] = array();
        //showing date options
        if (count($show_dates)) {
            foreach ($show_dates as $date) {
//                $actual_dates[date('Y-m-d', strtotime($date['start_date']))] = date('l d, F Y', strtotime($date['start_date']));
//                $actual_dates[date('Y-m-d', strtotime($date['end_date']))] = date('l d, F Y', strtotime($date['end_date']));
                $actual_dates[] = date('Y-m-d', strtotime($date['start_date']));
                $actual_dates[] = date('Y-m-d', strtotime($date['end_date']));
            }
            //$show_start_date = min($actual_dates);
            $show_start_date = date('Y-m-d');
            $show_end_date = max($actual_dates);
            if ($show_start_date < $show_end_date) {
                $actual_dates = array();
                $to_date = Carbon::parse($show_end_date);
                $from_date = Carbon::parse($show_start_date);
                while ($from_date->lt($to_date)) {
                    $actual_dates[$from_date->format('Y-m-d')] = $from_date->format('l d, F Y');
                    $from_date->addDay();
                }
            }
            $data['show_dates'] = $actual_dates;
            $data['selected_date'] = $show_start_date;
        }

        if (Input::has('show_date')) {
            $data['selected_date'] = Input::get('show_date');
        }

        if (!count($mall_movies)) {
            $data['mallMovies'] = Movie::whereHas('showing', function ($q) use ($mallID) {
                $q->where('endDate', '>=', Carbon::now()->toDateTimeString())
                    ->where('mallID', $mallID);
            })
                ->orderBy('name', 'ASC')
                ->distinct()
                ->get();
        } else {
            $data['mallMovies'] = $mall_movies;
        }

        $data['mallID'] = $mallID;
        $data['mallMovieData'] = Movie::with('showing')->whereHas('showing', function ($q) use ($mallID) {
            $q->where('endDate', '>=', Carbon::now()->toDateTimeString())->where('mallID', '=', $mallID);
        })
            ->orderBy('name', 'ASC')
            ->distinct()
            ->get();

        $data['cinemas'] = WhatsShowing::where('whatsShowing.endDate', '>=', date("Y-m-d H:i:s"))
            ->join('shoppingMall', 'shoppingMall.mallID', '=', 'whatsShowing.mallID')
            ->groupBy('whatsShowing.mallID')
            ->select('shoppingMall.name', 'shoppingMall.mallID')
            ->orderBy('shoppingMall.name')
            ->pluck('name', 'mallID');

        $data['mall'] = ShoppingMall::find($mallID)->toArray();

        initSEO(array('title' => pageTitle(array('Movies showing at ' . $data['mall']['name'], Config::get('app.name')))));

        return view('movies.cinema', $data);

    }
}
