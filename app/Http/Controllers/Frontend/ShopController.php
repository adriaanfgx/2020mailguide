<?php

namespace App\Http\Controllers\Frontend;

use App\MapImage;
use App\Shop;
use App\ShopCategory;
use App\Shopfile;
use App\ShoppingMall;
use App\SubCategory;
use App\User;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;

class ShopController extends Controller
{
    public function __construct()
    {
        initSEO(array('title' => 'Shops | ' . Config::get('app.name')));
    }


    public function getIndex($province_id = 0, $mall_id = 0, $category_id = 0)
    {

        //Top 5 malls / shops for provinces
        $country_id = Cookie::get('country_id', 190);
        $expiresAt = Carbon::now()->addDay(1);

//        Cache::forget('topMalls_'.$country_id);
        $topMallByProvinces = Cache::remember('topMalls_' . $country_id, $expiresAt, function () use ($country_id) {
            $provinces = activeProvinces();
            $mallByProvinces = [];
            if (count($provinces)) {
                foreach ($provinces as $province_id => $province) {
                    $malls = ShoppingMall::join('mall_visits', 'mall_visits.mall_id', '=', 'shoppingMall.mallID')
                        ->where('province_id', $province_id)->orderBy('mall_visits.visits', 'DESC')
                        ->take(5)->get();
                    if ($malls->count()) {
                        foreach ($malls as $idx => $mall) {
                            $shops = Shop::join('shop_visits', 'shop_visits.shop_id', '=', 'shop.shopID')
                                ->where('mallID', $mall->mallID)->take(5)->get();

                            $malls[$idx]->shops = $shops;
                        }

                        $mallByProvinces[$province_id]['name'] = $province;
                        $mallByProvinces[$province_id]['malls'] = $malls;

                    }
                }
            }
            return $mallByProvinces;
        });

        $shop_category = array();

        if ($category_id > 0) {
            $shop_category = DB::table('shop_categories')->where('id', $category_id)->first();
        }

        return view('shops.index', compact('province_id', 'mall_id', 'category_id', 'topMallByProvinces', 'shop_category'));
    }

    public function getFilter($id)
    {
        $mall = ShoppingMall::find($id);
        return $mall->shops()->pluck('name', 'shopMGID');
    }

    public function getMallShops($mallID, $categoryID = 0)
    {
        storeSelectedDropdownMallInSession($mallID);
        $alpha = Input::get('alpha');
        $query = Shop::where('shop.mallID', '=', $mallID)->where('display', '=', 'Y')->where('shop.name', 'like', $alpha . '%');
        if ($categoryID > 0) {
            $query->where('shop.category_id', '=', $categoryID);
        }
        $shops = $query->leftjoin('shop_categories', 'shop_categories.id', '=', 'shop.category_id')
            ->join('shoppingMall', 'shoppingMall.mallID', '=', 'shop.mallID')
            //->where('shop_categories.parent_id',0)
            ->select('shop.name', 'shopMGID', 'shoppingMall.name as mall', 'shop.shopNumber', 'shop.telephone', 'shop_categories.name as category', 'shop_categories.id as categoryID', 'shop.logo', 'shop.mallID', 'shop.mapX', 'shop.mapY')
            ->orderBy('shop.name')->get();
        //var_dump(Helpers::getLastQuery());

        return $shops;
    }

    public function getKeyWordSearch($keyWord)
    {
        $stringToSearch = trim($keyWord);
        return DB::select(DB::raw("SELECT shop.name AS name,shop.shopNumber AS shopNumber,shop.category AS category,shop.telephone AS telephone, shoppingMall.name as mall, shop.shopMGID,shop.mallID as mallID FROM shop join shoppingMall on shoppingMall.mallID = shop.mallID WHERE display = 'Y' AND shop.name Like '%" . $stringToSearch . "%';"));
    }

    public function getSiteSearch($keyWord)
    {

        $stringToSearch = trim($keyWord);

        $data['malls'] = DB::select(DB::raw("SELECT * FROM shoppingMall WHERE activate = 'Y' AND name Like '%" . $stringToSearch . "%';"));
        $data['shops'] = DB::select(DB::raw("SELECT * FROM shop WHERE display = 'Y' AND name Like '%" . $stringToSearch . "%';"));

        session()->put('testData', $data);

        return view('pages/searchresult');
    }

    public function getFilterShopsByMall($mallID)
    {

        $data['mall'] = ShoppingMall::find($mallID);
        $data['shops'] = Shop::where('mallID', '=', $mallID)->orderBy('name')->get();

        return view('shops.map', $data);
    }

    /**
     * Front page search
     * Get shop that matches search
     */
    public function getShopByKeyWord(Request $request, $keyWord = NULL, $page_num = 1)
    {
        if ($keyWord != NULL) {
            $keyWord = trim($keyWord);
        } else {
            $keyWord = $request->get('shop_name');
        }


        if ($keyWord) {
            session()->put('shopSearchKeyword', $keyWord);
        }

        $searchResult = Shop::whereHas('mall', function ($q) {
            return $q->where('activate', '=', 'Y');
        })->where('name', 'LIKE', '%' . $keyWord . '%')->where('display', '=', 'Y')
            ->orderBy('shop.name')->paginate(25);

        $searchResult->setPath('/shops/quickfind/' . $keyWord . '/');

        return view('shops.quickfind', array('shops' => $searchResult));
    }

    /**
     * Create Shop in the front end
     */
    public function getCreateFrontEnd()
    {

        $data['categories'] = Shop::distinct('category')->orderBy('category')->pluck('category', 'category');
        $data['subcategories'] = Shop::distinct('subcategory')->orderBy('subcategory')->pluck('subcategory', 'subcategory');

        return view('users.createShop', $data);
    }

    /**
     * Post Create Shop
     */
    public function postCreate(Request $request)
    {

        //to do send validate e-mail to mall manager+ centre manager &
        $input = $request->all();
        $mallID = $input['mallID'];
        $url = $input['url'];

        if ($url) {

            $url = str_replace('https://', '', $url);
            $url = str_replace('http://', '', $url);

            $url = 'http://' . $url;
            $input['url'] = $url;
        }

        $v = Shop::validate($input);

        if ($v->passes()) {
            //$input['display']='N';
            $created = $this->doCreate($input);

            if ($created == true) {

                if (Sentinel::check()) {

                    $user = Sentinel::getUser();

                    if (!session()->has('activeMallID') && !$user->hasAccess('admin')) {
                        $input['display'] = 'N';
                        self::sendAddShopMail($input);
                    }

                    if ($user->hasAccess('admin')) {
                        session()->flash('success', 'Shop was registered successfully!');
                        return redirect('/admin/shops/' . $mallID);

                    } elseif ($user->hasAccess('user')) {
                        session()->flash('success', 'Shop was registered successfully! Shop will display after Mall Manager has approved it!');
                        return redirect('/chainstores');

                    } elseif ($user->hasAccess('manager')) {
                        session()->flash('success', 'Shop was registered successfully!');
                        return redirect('/shops/list');
                    }

                } else {
                    session()->flash('success', 'Shop was registered successfully! Shop will display after Mall Manager has approved it!');

                    self::sendAddShopMail($input);
                    return redirect('/');
                }
            } else {
                session()->flash('error', 'Shop Could not be created');
            }
        } else {
            session()->flash('error', 'Please review the form!');
            return back()->withErrors($v)->withInput();
        }
    }

    private function sendAddShopMail($input)
    {
        $users = User::whereHas('malls', function ($q) use ($input) {
            $q->where('mall_id', '=', $input['mallID']);
        })->get();
        foreach ($users as $user) {
            if (($user->admin_level == 'Mall Manager' || $user->admin_level == "Mall Manager") && $user->email != NULL) {
                $input['to_cc'][$user->email] = $user->email;
            }
        }

        //message for mall managers
        $input['the_message'] = $input['name'] . " has been added as a shop on mallguide. This shop will only display after you have changed the display status to yes on edit.";

        $mall = ShoppingMall::find($input['mallID']);
        $input['mall_name'] = $mall->name;
        $input['user_name'] = $mall->name;

        Mail::send('emails.addShop', $input, function ($m) use ($input) {
            $m->from('webmaster@mallguide.co.za', Config::get('app.name'));
            $m->to("info@mallguide.co.za")->subject('New Shop Registration | ' . Config::get('app.name'));
            if (isset($input['to_cc'])) {
                $m->cc($input['to_cc']);
            }
        });

        if (isset($input['email']) && $input['email'] != "") {
            $input['the_message'] = "Your shop, " . $input['name'] . " has been added successfully on " . Config::get('app.name') . ". This shop will only display after the centre manager has confirmed it. Please use these contact details for further queries: info@mallguide.co.za";
            $input['user_name'] = "";
            $email = $input['email'];
            Mail::send('emails.addShop', $input, function ($m) use ($email) {
                $m->from('webmaster@mallguide.co.za', Config::get('app.name'));
                $m->to($email)->subject('New Shop Registration | ' . Config::get('app.name'));
            });
        }
    }

    public function doCreate($input)
    {

        $shopCreated = false;
        $mallID = isset($input['mallID']) ? $input['mallID'] : 0;
        $data['mallID'] = isset($input['mallID']) ? $input['mallID'] : 0;
        $data['shopID'] = 0;
        $data['name'] = trim($input['name']);
        $data['tradingHours'] = isset($input['tradingHours']) ? $input['tradingHours'] : '';
        $data['keywords'] = isset($input['keywords']) ? $input['keywords'] : '';
        $data['description'] = isset($input['description']) ? $input['description'] : '';
        $data['category'] = isset($input['category']) ? $input['category'] : '';
        if (isset($input['category_id']) && $input['category_id'] != 0) {
            $data['category_id'] = $input['category_id'];
            $data['category'] = ShopCategory::find($input['category_id'])->name;
        } else {
            $data['category_id'] = 0;
        }
        $data['product1'] = isset($input['product1']) ? $input['product1'] : '';
        $data['product2'] = isset($input['product2']) ? $input['product2'] : '';
        $data['product3'] = isset($input['product3']) ? $input['product3'] : '';
        $data['product4'] = isset($input['product4']) ? $input['product4'] : '';
        $data['product5'] = isset($input['product5']) ? $input['product5'] : '';
        $data['product6'] = isset($input['product6']) ? $input['product6'] : '';
        $data['product7'] = isset($input['product7']) ? $input['product7'] : '';
        $data['product8'] = isset($input['product8']) ? $input['product8'] : '';
        $data['product9'] = isset($input['product9']) ? $input['product9'] : '';
        $data['product10'] = isset($input['product10']) ? $input['product10'] : '';
        $data['shopNumber'] = isset($input['shopNumber']) ? $input['shopNumber'] : '';
        $data['landmark'] = isset($input['landmark']) ? $input['landmark'] : '';
        $data['telephone'] = isset($input['telephone']) ? $input['telephone'] : '';
        $data['fax'] = isset($input['fax']) ? $input['fax'] : '';
        $data['cell'] = isset($input['cell']) ? $input['cell'] : '';
        $data['contactPerson'] = isset($input['contactPerson']) ? $input['contactPerson'] : '';
        $data['email'] = isset($input['email']) ? $input['email'] : '';
        $data['url'] = isset($input['url']) ? removeURLProtocol($input['url']) : '';
        $data['faceBookURL'] = isset($input['faceBookURL']) ? removeURLProtocol($input['faceBookURL']) : '';
        $data['newShop'] = isset($input['newShop']) ? $input['newShop'] : '';
        $data['visa'] = isset($input['visa']) ? $input['visa'] : '';
        $data['mastercard'] = isset($input['mastercard']) ? $input['mastercard'] : '';
        $data['amex'] = isset($input['amex']) ? $input['amex'] : '';
        $data['diners'] = isset($input['diners']) ? $input['diners'] : '';
        $data['map'] = isset($input['map']) ? $input['map'] : '';
        $data['mapX'] = isset($input['mapX']) ? $input['mapX'] : '';
        $data['mapY'] = isset($input['mapY']) ? $input['mapY'] : '';
        $data['display'] = isset($input['display']) ? $input['display'] : '';
        $data['subcategory'] = isset($input['subcategory']) ? $input['subcategory'] : '';
        if (isset($input['subcategory_id']) && $input['subcategory_id'] != 0) {
            $data['subcategory_id'] = $input['subcategory_id'];
            $data['subcategory'] = ShopCategory::find($input['subcategory_id'])->name;
        } else {
            $data['subcategory_id'] = 0;
        }
        $data['promoBlock'] = isset($input['promoBlock']) ? $input['promoBlock'] : '';
        $data['ownerName'] = isset($input['ownerName']) ? $input['ownerName'] : '';
        $data['ownerSurname'] = isset($input['ownerSurname']) ? $input['ownerSurname'] : '';
        $data['ownerEmail'] = isset($input['ownerEmail']) ? $input['ownerEmail'] : '';
        $data['ownerCell'] = isset($input['ownerCell']) ? $input['ownerCell'] : '';
        $data['managerName'] = isset($input['managerName']) ? $input['managerName'] : '';
        $data['managerSurname'] = isset($input['managerSurname']) ? $input['managerSurname'] : '';
        $data['managerEmail'] = isset($input['managerEmail']) ? $input['managerEmail'] : '';
        $data['managerCell'] = isset($input['managerCell']) ? $input['managerCell'] : '';
        $data['managerName2'] = isset($input['managerName2']) ? $input['managerName2'] : '';
        $data['managerSurname2'] = isset($input['managerSurname2']) ? $input['managerSurname2'] : '';
        $data['managerCell2'] = isset($input['managerCell2']) ? $input['managerCell2'] : '';
        $data['managerEmail2'] = isset($input['managerEmail2']) ? $input['managerEmail2'] : '';
        $data['managerName3'] = isset($input['managerName3']) ? $input['managerName3'] : '';
        $data['managerSurname3'] = isset($input['managerSurname3']) ? $input['managerSurname3'] : '';
        $data['managerCell3'] = isset($input['managerCell3']) ? $input['managerCell3'] : '';
        $data['managerEmail3'] = isset($input['managerEmail3']) ? $input['managerEmail3'] : '';
        $data['headOfficeName'] = isset($input['headOfficeName']) ? $input['headOfficeName'] : '';
        $data['headOfficeSurname'] = isset($input['headOfficeSurname']) ? $input['headOfficeSurname'] : '';
        $data['headOfficeCell'] = isset($input['headOfficeCell']) ? $input['headOfficeCell'] : '';
        $data['headOfficeEmail'] = isset($input['headOfficeEmail']) ? $input['headOfficeEmail'] : '';
        $data['opsManagerName'] = isset($input['opsManagerName']) ? $input['opsManagerName'] : '';
        $data['opsManagerSurname'] = isset($input['opsManagerSurname']) ? $input['opsManagerSurname'] : '';
        $data['opsManagerCell'] = isset($input['mapY']) ? $input['mapY'] : '';
        $data['opsManagerEmail'] = isset($input['opsManagerEmail']) ? $input['opsManagerEmail'] : '';
        $data['financialName'] = isset($input['financialName']) ? $input['financialName'] : '';
        $data['financialSurname'] = isset($input['financialSurname']) ? $input['financialSurname'] : '';
        $data['financialCell'] = isset($input['financialCell']) ? $input['financialCell'] : '';
        $data['financialEmail'] = isset($input['financialEmail']) ? $input['financialEmail'] : '';
        $data['areaManagerName'] = isset($input['areaManagerName']) ? $input['areaManagerName'] : '';
        $data['areaManagerSurname'] = isset($input['mapY']) ? $input['mapY'] : '';
        $data['areaManagerCell'] = isset($input['areaManagerCell']) ? $input['areaManagerCell'] : '';
        $data['areaManagerEmail'] = isset($input['areaManagerEmail']) ? $input['areaManagerEmail'] : '';
        $data['emergencyName'] = isset($input['emergencySurname']) ? $input['emergencySurname'] : '';
        $data['emergencySurname'] = isset($input['mapY']) ? $input['mapY'] : '';
        $data['emergencyCell'] = isset($input['emergencyCell']) ? $input['emergencyCell'] : '';
        $data['emergencyEmail'] = isset($input['emergencyEmail']) ? $input['emergencyEmail'] : '';
        $data['emergencyName2'] = isset($input['emergencyName2']) ? $input['emergencyName2'] : '';
        $data['emergencySurname2'] = isset($input['emergencySurname2']) ? $input['emergencySurname2'] : '';
        $data['emergencyCell2'] = isset($input['emergencyCell2']) ? $input['emergencyCell2'] : '';
        $data['emergencyEmail2'] = isset($input['emergencyEmail2']) ? $input['emergencyEmail2'] : '';
        $data['location2'] = isset($input['location2']) ? $input['location2'] : '';
        $data['location3'] = isset($input['location3']) ? $input['location3'] : '';
        $data['googleMapX'] = isset($input['googleMapX']) ? $input['googleMapX'] : '';
        $data['googleMapY'] = isset($input['googleMapY']) ? $input['googleMapY'] : '';
        $data['blog'] = isset($input['blog']) ? $input['blog'] : '';
        $data['blog_url'] = isset($input['blog_url']) ? removeURLProtocol($input['blog_url']) : '';
        $reserveWith = isset($input['reserveWith']) ? $input['reserveWith'] : '';

        $data['reserveWith'] = '';
        if (is_array($reserveWith)) {
            $data['reserveWith'] = isset($reserveWith) ? implode($reserveWith, ',') : '';
        }

        $mall = ShoppingMall::find($mallID);

        if (isset($mall->thumbWidth)) {
            $thumbMaxWidth = $mall->thumbWidth;
        } else {
            $thumbMaxWidth = 150;
        }

        $destinationPath = base_path('uploadimages' . '/mall_' . $mallID);


        $logo = isset($input['logo']) ? $input['logo'] : null;
        $image1 = isset($input['image1']) ? $input['image1'] : null;
        $image2 = isset($input['image2']) ? $input['image2'] : null;
        $image3 = isset($input['image3']) ? $input['image3'] : null;
        $image4 = isset($input['image4']) ? $input['image4'] : null;
        $image5 = isset($input['image5']) ? $input['image5'] : null;
        $image6 = isset($input['image6']) ? $input['image6'] : null;
        $image7 = isset($input['image7']) ? $input['image7'] : null;
        $image8 = isset($input['image8']) ? $input['image8'] : null;
        $image9 = isset($input['image9']) ? $input['image9'] : null;
        $image10 = isset($input['image10']) ? $input['image10'] : null;

        if (is_object($logo)) {

            $logo1 = uploadImage($logo, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['logo'] = $logo1['thumbnail'];
            $data['logoBig'] = $logo1['largeImage'];

        }

        if (is_object($image1)) {

            $img1 = uploadImage($image1, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['image1'] = $img1['thumbnail'];
            $data['imageBig1'] = $img1['largeImage'];

        }

        if (is_object($image2)) {

            $img2 = uploadImage($image2, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['image2'] = $img2['thumbnail'];
            $data['imageBig2'] = $img2['largeImage'];

        }

        if (is_object($image3)) {

            $img3 = uploadImage($image3, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['image3'] = $img3['thumbnail'];
            $data['imageBig3'] = $img3['largeImage'];

        }

        if (is_object($image4)) {

            $img4 = uploadImage($image4, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['image4'] = $img4['thumbnail'];
            $data['imageBig4'] = $img4['largeImage'];

        }

        if (is_object($image5)) {

            $img5 = uploadImage($image5, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['image5'] = $img5['thumbnail'];
            $data['imageBig5'] = $img5['largeImage'];

        }

        if (is_object($image6)) {

            $img6 = uploadImage($image6, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['image6'] = $img6['thumbnail'];
            $data['imageBig6'] = $img6['largeImage'];

        }

        if (is_object($image7)) {

            $img7 = uploadImage($image7, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['image7'] = $img7['thumbnail'];
            $data['imageBig7'] = $img7['largeImage'];

        }

        if (is_object($image8)) {

            $img8 = uploadImage($image5, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['image8'] = $img8['thumbnail'];
            $data['imageBig8'] = $img8['largeImage'];

        }

        if (is_object($image9)) {

            $img9 = uploadImage($image9, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['image9'] = $img9['thumbnail'];
            $data['imageBig9'] = $img9['largeImage'];

        }

        if (is_object($image10)) {

            $img10 = uploadImage($image10, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);

            $data['image10'] = $img10['thumbnail'];
            $data['imageBig10'] = $img10['largeImage'];

        }

        $newShopID = Shop::insertGetId($data);

        if (isset($newShopID)) {

            Shop::find($newShopID)->update(array('shopID' => $newShopID));
            $shopCreated = true;

        }

        return $shopCreated;

    }


    /**
     * @param Request $request
     * @return array
     */
    public function getSubcategoryByCategory(Request $request)
    {

        $category = $request->get('category');
        $subcat = [0 => 'No Sub-categories'];
        if (!empty($category)) {
            $req = SubCategory::where('category_id', '=', $category)->distinct('name')->orderBy('name')->pluck('name', 'id');
            if (count($req)) {
                $subcat = $req;
            }
        }
        return $subcat;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function postShopByMalls(Request $request)
    {
        $shops = [];
        $malls = $request->get('mallIDs');
        if (isset($malls)) {
            $shops = Shop::whereIn('mallID', $malls)->distinct('name')->orderBy('name')->pluck('name', 'shopMGID');
        }

        return response($shops)->header('Content-Type', 'application/json');
    }

    public function getViewShop($id)
    {

        $shop = Shop::find($id);
        $mallID = $shop->mallID;
        $data['shop'] = $shop;
        $data['mall'] = ShoppingMall::find($mallID);

        $data['shop']->mapSrc = MapImage::where('mallID', $mallID)->where('location', $shop->map)->pluck('imageSrc');

        initSEO(array('title' => pageTitle(array($data['mall']->name, $shop->name, Config::get('app.name'))), 'description' => metaDescription($shop->description)));


        return view('shops.view_shop', $data);
    }

    public function getShopMap($id)
    {

        $shop = Shop::find($id);
        if ($shop) {
            $image = MapImage::where('mallID', $shop->mallID)->where('location', $shop->map)->pluck('imageSrc');
            if (!$image) {
                App::abort('404');
            }
        } else {
            App::abort('404');
        }
        $image_url = Config::get('app.url') . '/uploadimages/mall_' . $shop->mallID . '/' . $image;

        return response()->make(file_get_contents($image_url), 200);
    }

    public function getShopList($mallID = NULL)
    {
        $input = Input::get();
        if (!Sentinel::check()) {
            session()->flash('error', 'You are not logged in.');
            return redirect('login');
        } else {
            if (session()->has('activeMallID') && $mallID == NULL) {
                $mallID = session()->get('activeMallID');
            } else {
                return redirect('/portal');
            }
            $query = Shop::where('mallID', '=', $mallID)->orderBy('name');
            if (isset($input["display"])) {
                $data["heading"] = "Shops Not Displaying";
                $data["display"] = $input["display"];
                $query->where("display", $input["display"]);
            } else {
                $data["display"] = "";
                $data["heading"] = "Listed Shops";
            }
            $data['shops'] = $query->get();
            $data['categories'] = ShopCategory::where('parent_id', 0)->get();

            return view('portal.shops.list', $data);
        }
    }

    /**
     * User register new shop on the front end
     */
    public function getRegisterNew()
    {
        return view('shops.users.register');
    }














    /***************************************************/
    /*Portal Functions
    /***************************************************/
    /**
     * @param $mallID
     * @return mixed
     */
    public function getRegister($mallID = NULL)
    {
        if (Sentinel::check()) {
            if (session()->has('activeMallID') && $mallID == NULL) {
                $mallID = session()->get('activeMallID');
                $data['mall'] = ShoppingMall::find($mallID);
            }

            $data['categories'] = ShopCategory::where('parent_id', 0)->pluck('name', 'id');
            $data['subcategories'] = ShopCategory::where('parent_id', '!=', 0)->pluck('name', 'id');;
            return view('portal.shops.create', $data);
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not Logged In!');

            return redirect('/');
        }
    }

    public function getUpdate($id)
    {
        if (Sentinel::check()) {
            // Find the user using the user id
            $data['shop'] = Shop::find($id);
            $data['categories'] = ShopCategory::where('parent_id', 0)->pluck('name', 'id');
            //dd($data['shop']->category_id);
            $data['subcategories'] = ShopCategory::where('parent_id', $data['shop']->category_id)->pluck('name', 'id');;

            return view('portal.shops.update', $data);
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function updateShopByExcel()
    {
        if (Sentinel::check()) {
            $user = Sentinel::getUser();
            if ($user->hasAccess('admin')) {
                if (Input::hasFile('updateFile')) {
                    $file = Input::file('updateFile');
                    $path = Config::get('image_upload_path') . '/toUpdate';
                    $uploaded_file = Helpers::uploadFile($file, $path);

                    $spreadsheet = Excel::load($path . '/' . $uploaded_file, function ($reader) {
                        // reader methods
                    })->get();

                    //dd($spreadsheet);
                    $updated = FALSE;

                    //dd($spreadsheet);
                    foreach ($spreadsheet as $row) {
                        if (isset($row->shopMGID) && $row->shopMGID != "" && $row->shopMGID != 0) {
                            $shop = Shop::find($row->shopMGID);
                            $data = $row->toArray();
                            //if category is set get category id
                            if ($row->category != "" && $row->category != NULL) {
                                $category = ShopCategory::where('name', $row->category)->first();
                                if ($category) {
                                    $data['category_id'] = $category->id;
                                    if ($row->subcategory != "" && $row->subcategory != NULL) {
                                        $subcategory = ShopCategory::where('parent_id', $row->category_id)->where('name', $row->subcategory)->first();
                                        if ($subcategory) {
                                            $data['subcategory_id'] = $subcategory->id;
                                        }
                                    }
                                }
                            }

                            $updated = $shop->update($data);

                        }
                    }
                    if ($updated != TRUE) {
                        session()->flash('error', 'Shops could not be updated! Please recheck your file.');
                    } else {
                        session()->flash('success', 'Shops where updated!');
                    }
                    return Redirect::back();
                } else {
                    session()->flash('success', 'Please upload a valid file!');
                    return Redirect::back();
                }
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {

            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function importShopByExcel()
    {
        if (Sentinel::check()) {
            $user = Sentinel::getUser();
            if ($user->hasAccess('admin')) {
                $mallID = Input::get('mallID');
                if (Input::hasFile('updateFile') && $mallID != "" && Input::get('mallID') != NULL) {
                    $file = Input::file('updateFile');
                    $path = Config::get('image_upload_path') . '/toUpdate';
                    $uploaded_file = Helpers::uploadFile($file, $path);

                    $spreadsheet = Excel::load($path . '/' . $uploaded_file, function ($reader) {
                    })->get();

                    $updated = FALSE;
                    $new_data = array();
                    $i = 0;
                    foreach ($spreadsheet as $row) {
                        //dd($row);
                        if ($row->mallID == $mallID) {
                            //$shop=Shop::find($row->shopMGID);
                            $new_data[$i] = $row->toArray();
                            //if category is set get category id
                            if ($row->category != "" && $row->category != NULL) {
                                $category = ShopCategory::where('name', $row->category)->first();
                                if ($category) {
                                    $new_data[$i]['category_id'] = $category->id;
                                    $new_data[$i]['subcategory_id'] = "";
                                    if ($row->subcategory != "" && $row->subcategory != NULL) {
                                        $subcategory = ShopCategory::where('parent_id', $category->id)->where('name', $row->subcategory)->first();
                                        if ($subcategory) {
                                            $new_data[$i]['subcategory_id'] = $subcategory->id;
                                        }
                                    }
                                }
                            } else {
                                $new_data[$i]['category_id'] = "";
                                $new_data[$i]['subcategory_id'] = "";
                            }
                        }
                        $i++;
                    }
                    $new_data = array_filter($new_data);
                    if (isset($new_data) && sizeof($new_data) > 0) {
                        Shop::where('mallID', $mallID)->delete();
                        //dd($new_data);
                        $updated = Shop::insert($new_data);
                        //update shopID field to match shopMGID field
                        $newShops = Shop::where('mallID', $mallID)->where('shopMGID', '!=', 'shopID')->get();
                        foreach ($newShops as $shop) {
                            $shop->shopID = $shop->shopMGID;
                            $shop->save();
                        }
                    }


                    if ($updated != TRUE) {
                        session()->flash('error', 'Shops could not be updated! Please recheck your file.');
                    } else {
                        session()->flash('success', 'Shops where updated!');
                    }
                    return Redirect::back();
                } else {
                    session()->flash('success', 'Please upload a valid file!');
                    return Redirect::back();
                }
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {

            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getCategories($parent_id = 0)
    {
        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('admin')) {
                $data['parent_id'] = $parent_id;
                if ($parent_id != 0) {
                    $data['parentName'] = ShopCategory::find($parent_id)->name;
                }
                $data['categories'] = ShopCategory::orderBy('name')->where('parent_id', $parent_id)->get();
                return view('admins/shops/categories/index', $data);

            } else {

                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {

            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Get create brand page
     */
    public function getCreateCategory($parent_id = 0)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('admin')) {
                $data['parent_id'] = $parent_id;
                if ($parent_id != 0) {
                    $data['parentName'] = ShopCategory::find($parent_id)->name;
                }
                return view('admins/shops/categories/create', $data);

            } else {

                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {

            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Post Create Brand
     */
    public function postCreateCategory()
    {
        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('admin')) {

                $input = Input::all();

                $v = ShopCategory::validate($input);
                if ($v->passes()) {
                    $data = Input::all();
                    ShopCategory::create($data);

                    session()->flash('success', 'The category has been added.');
                    return redirect(route('admin.shop_categories', $data['parent_id']));

                } else {
                    session()->flash('error', 'Please review the form!');
                    return redirect(route('admin.shop_categories.create')->withErrors($v)->withInput());
                }

            } else {

                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {

            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get View for editing shop brands
     */
    public function getEditCategory($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('admin')) {

                $category = ShopCategory::find($id);
                $data = compact('category');
                $data['parent_id'] = $category->parent_id;
                if ($data['parent_id'] != 0) {
                    $data['parentName'] = ShopCategory::find($data['parent_id'])->name;
                }
                return view('admins/shops/categories/create', $data);

            } else {

                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {

            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Delete Category
     */
    public function postDeleteCategory($id)
    {

        ShopCategory::find($id)->delete();
        $subCategories = ShopCategory::where('parent_id', $id);
        foreach ($subCategories as $subCat) {
            $subCat->delete();
        }
        session()->flash('success', 'The category has been deleted.');

    }

    /**
     * Post Edit card
     */
    public function postEditCategory($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('admin')) {

                $category = ShopCategory::find($id);

                $data = Input::all();
                $category->update($data);

                session()->flash('success', 'The category has been updated.');
                return redirect(route('admin.shop_categories', $data['parent_id']));
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getShopDropdown()
    {
        $data = Input::get();

        $data['list'] = Shop::display()->whereHas('mall', function ($q) use ($data) {
            $q->active()->where('mallID', '=', $data['mallID']);
        }
        )->orderBy('name')->pluck('name', 'shopID');

        $html = "";
        if (sizeof($data['list']) > 0) {
            foreach ($data['list'] as $id => $value) {
                $html .= "<option value='$id'>$value</option>";
            }
        } else {
            $html = "<option value=''>no shops found</option>";
        }
        return $html;
    }

    public function getCategoryShops($mallID, $category = "")
    {

        $thisCategory = trim($category);
        if ($thisCategory == 'all' || $thisCategory == '') {
            if (Sentinel::check()) {
                return Shop::where('mallID', '=', $mallID)
                    ->leftjoin('shop_categories', 'shop_categories.id', '=', 'shop.category_id')
                    ->select('shop.name', 'shopMGID', 'shop.shopNumber', 'shop.telephone', 'shop.display', 'shop_categories.name as category', 'shop_categories.name as subcategory', 'shop.mallID')
                    ->orderBy('name')
                    ->get();
            } else {
                return Shop::where('mallID', '=', $mallID)
                    ->where('display', '=', 'Y')
                    ->leftjoin('shop_categories', 'shop_categories.id', '=', 'shop.category_id')
                    ->select('shop.name', 'shopMGID', 'shop.shopNumber', 'shop.telephone', 'shop.display', 'shop_categories.name as category', 'shop_categories.name as subcategory', 'shop.mallID')
                    ->orderBy('name')
                    ->get();
            }
        } else {
            if (Sentinel::check()) {
                return Shop::where('mallID', '=', $mallID)->where('category_id', '=', $thisCategory)->orderBy('name')->get();
            } else {
                return Shop::where('mallID', '=', $mallID)->where('category_id', '=', $thisCategory)->where('display', '=', 'Y')->orderBy('name')->get();
            }
        }

    }

    public function postDelete($id)
    {

        if (Sentinel::check()) {
            Shop::where('shopMGID', '=', $id)->delete();
        } else {
            session()->flash('error', 'You are not logged in !!');
        }

    }

    public function postRemoveImage($id, $image)
    {
        $shop = Shop::where('shopMGID', '=', $id)->get()->first();
        if ($shop) {
            $destinationPath = Config::get('image_upload_path') . '/mall_' . $shop->mallID;
            //get numeric image number
            $imageID = str_ireplace('image', '', $image);
            $imageID = str_ireplace('big', '', $imageID);

            if ($image) {
                removeOldImage($destinationPath, 'image' . $imageID);
                removeOldImage($destinationPath, 'imageBig' . $imageID);
            }

            $shop->update(array('image' . $imageID => '', 'imageBig' . $imageID => ''));
        }
        exit;

    }

    public function getExportMallShops($mallID = NULL)
    {
        if (Sentinel::check()) {
            if (session()->has('activeMall') && $mallID == NULL) {
                $mallID = session()->get('activeMall');
            }
            $shoppingMall = ShoppingMall::find($mallID);
            $shops = Shop::where('mallID', '=', $mallID)->orderBy('name')->get();
            if ($shops) {
                $csv_arr[0] = array('shopMGID', 'shopID', 'mallID', 'name', 'keywords', 'description', 'category', 'product1', 'product2', 'product3', 'product4', 'product5', 'product6', 'product7', 'product8', 'product9', 'product10', 'shopNumber', 'landmark', 'telephone', 'fax', 'cell', 'contactPerson', 'email', 'url', 'image1', 'image2', 'image3', 'image4', 'image5', 'newShop', 'visa', 'mastercard', 'amex', 'diners', 'otherCard1', 'otherCard2', 'otherCard3', 'otherCard4', 'tradingHours', 'map', 'mapX', 'mapY', 'display', 'subcategory', 'promoBlock', 'ownerName', 'ownerSurname', 'ownerEmail', 'ownerCell', 'managerName', 'managerSurname', 'managerEmail', 'managerCell', 'managerName2', 'managerSurname2', 'managerEmail2', 'managerCell2', 'managerName3', 'managerSurname3', 'managerEmail3', 'managerCell3', 'dateAdded', 'headOfficeName', 'headOfficeSurname', 'headOfficeEmail', 'headOfficeCell', 'financialName', 'financialSurname', 'financialEmail', 'financialCell', 'reserveWith', 'imageBig1', 'imageBig2', 'imageBig3', 'imageBig4', 'imageBig5', 'adults', 'teens', 'kids', 'category2', 'subcategory2', 'lastUpdate', 'image6', 'image7', 'image8', 'image9', 'image10', 'imageBig6', 'imageBig7', 'imageBig8', 'imageBig9', 'imageBig10', 'ratingDisplay', 'loyaltyRewardNum', 'logo', 'logoBig', 'altImage1', 'location2', 'location3', 'faceBookURL', 'suburb', 'city', 'province', 'storeType', 'physicalAddress', 'gpsCoords', 'googleMapX', 'googleMapY', 'brand', 'areaManagerName', 'areaManagerSurname', 'areaManagerEmail', 'areaManagerCell', 'opsManagerName', 'opsManagerSurname', 'opsManagerEmail', 'opsManagerCell', 'emergencyName', 'emergencySurname', 'emergencyEmail', 'emergencyCell', 'emergencyName2', 'emergencySurname2', 'emergencyEmail2', 'emergencyCell2', 'lorealProfessional', 'kerastase', 'redken', 'mizani', 'matrix', 'pureology');

                foreach ($shops as $shop) {
                    $csv_arr[$shop->shopMGID] = array(
                        $shop->shopMGID,
                        $shop->shopID,
                        $shop->mallID,
                        $shop->name,
                        $shop->keywords,
                        $shop->description,
                        $shop->category,
                        $shop->subcategory,
                        $shop->product1,
                        $shop->product2,
                        $shop->product3,
                        $shop->product4,
                        $shop->product5,
                        $shop->product6,
                        $shop->product7,
                        $shop->product8,
                        $shop->product9,
                        $shop->product10,
                        $shop->shopNumber,
                        $shop->landmark,
                        $shop->telephone,
                        $shop->fax,
                        $shop->cell,
                        $shop->contactPerson,
                        $shop->email,
                        $shop->url,
                        $shop->image1,
                        $shop->image2,
                        $shop->image3,
                        $shop->image4,
                        $shop->image5,
                        $shop->newShop,
                        $shop->visa,
                        $shop->mastercard,
                        $shop->amex,
                        $shop->diners,
                        $shop->otherCard1,
                        $shop->otherCard2,
                        $shop->otherCard3,
                        $shop->otherCard4,
                        $shop->tradingHours,
                        $shop->map,
                        $shop->mapX,
                        $shop->mapY,
                        $shop->display,
                        $shop->promoBlock,
                        $shop->ownerName,
                        $shop->ownerSurname,
                        $shop->ownerEmail,
                        $shop->ownerCell,
                        $shop->managerName,
                        $shop->managerSurname,
                        $shop->managerEmail,
                        $shop->managerCell,
                        $shop->managerName2,
                        $shop->managerSurname2,
                        $shop->managerEmail2,
                        $shop->managerCell2,
                        $shop->managerName3,
                        $shop->managerSurname3,
                        $shop->managerEmail3,
                        $shop->managerCell3,
                        $shop->dateAdded,
                        $shop->headOfficeName,
                        $shop->headOfficeSurname,
                        $shop->headOfficeEmail,
                        $shop->headOfficeCell,
                        $shop->financialName,
                        $shop->financialSurname,
                        $shop->financialEmail,
                        $shop->financialCell,
                        $shop->reserveWith,
                        $shop->imageBig1,
                        $shop->imageBig2,
                        $shop->imageBig3,
                        $shop->imageBig4,
                        $shop->imageBig5,
                        $shop->adults,
                        $shop->teens,
                        $shop->kids,
                        $shop->category2,
                        $shop->subcategory2,
                        $shop->lastUpdate,
                        $shop->image6,
                        $shop->image7,
                        $shop->image8,
                        $shop->image9,
                        $shop->image10,
                        $shop->imageBig6,
                        $shop->imageBig7,
                        $shop->imageBig8,
                        $shop->imageBig9,
                        $shop->imageBig10,
                        $shop->ratingDisplay,
                        $shop->loyaltyRewardNum,
                        $shop->logo,
                        $shop->logoBig,
                        $shop->altImage1,
                        $shop->location2,
                        $shop->location3,
                        $shop->faceBookURL,
                        $shop->suburb,
                        $shop->city,
                        $shop->province,
                        $shop->storeType,
                        $shop->physicalAddress,
                        $shop->gpsCoords,
                        $shop->googleMapX,
                        $shop->googleMapY,
                        $shop->brand,
                        $shop->areaManagerName,
                        $shop->areaManagerSurname,
                        $shop->areaManagerEmail,
                        $shop->areaManagerCell,
                        $shop->opsManagerName,
                        $shop->opsManagerSurname,
                        $shop->opsManagerEmail,
                        $shop->opsManagerCell,
                        $shop->emergencyName,
                        $shop->emergencySurname,
                        $shop->emergencyEmail,
                        $shop->emergencyCell,
                        $shop->emergencyName2,
                        $shop->emergencySurname2,
                        $shop->emergencyEmail2,
                        $shop->emergencyCell2,
                        $shop->lorealProfessional,
                        $shop->kerastase,
                        $shop->redken,
                        $shop->mizani,
                        $shop->matrix,
                        $shop->pureology
                    );

                }

                Excel::create($shoppingMall->name . '_shops_' . date("Y-m-d H:i:s"))
                    ->sheet('Mall Shops')
                    ->setTitle('mallshops' . $mallID)
                    ->with($csv_arr)
                    ->export('xls');
            }
        } else {
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Front page search
     * Get shop that matches search
     */

    /**************************************************/
    /*FGX Admin Functions
    /***************************************************/

    /**
     * Get Malls that have user selected chain
     * @access public
     * @param $chain
     * @return
     */
    public function getByChainName($chain)
    {
        return Shop::where('shop.name', 'LIKE', '%' . $chain . '%')->join('shoppingMall', 'shoppingMall.mallID', '=', 'shop.mallID')->select('shoppingMall.name as mall')->orderBy('shoppingMall.name')->pluck('mall');
    }


    /*
     * ********************* Downloads *****************8
     */

    public function postFiles()
    {
        if (Input::hasFile('fileUpload')) {
            $input = Input::all();

            $upload = uploadShopFile('fileUpload', $input['shopMGID']);

            $file = new Shopfile();
            $file->name = $input['upload_name'];
            $file->filename = $upload['name'];
            $file->shop_id = $input['shopMGID'];
            $file->filetype = $upload['extension'];
            $file->filesize = $upload['size'];
            $file->save();

            $shop = Shop::find($input['shopMGID']);
            return response()->view('portal.shops.downloads', ['shop' => $shop]);
        } else {
            session()->flash('error', 'You did not select a file!');
            return redirect(URL::previous());
        }
    }

    public function deleteFile($shop_id, $id)
    {
        $shop = Shopfile::find($id);
        $shop->delete();

        $shop = Shop::find($shop_id);
        return response()->view('portal.shops.downloads', ['shop' => $shop]);
    }


    /**
     * @string Post Edit shop
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postEdit($id)
    {
        $input = Input::all();
        $postingUrl = URL::previous();
        $shop = Shop::find($id);
        $url = $input['url'];
        if ($url) {
            $url = removeURLProtocol($url);
            if ($url) {
                $url = 'http://' . $url;
            }
            $input['url'] = $url;
        }

        $v = Shop::validate($input);

        if ($v->passes()) {

            if ($this->doUpdate($id, $input) == 1) {

                session()->flash('success', 'Shop Updated Successfully!!');
            } else {
                session()->flash('error', 'Shop could not be updated!!');

            }

            $user = Sentinel::getUser();
            if ($user->hasAccess('admin')) {

                return redirect('/admin/shops/' . $shop->mallID . '#shop_' . $id);
            }

            if (session()->has('activeMallID')) {
                return redirect('/shops/list');
            } elseif (session()->has('activeChain')) {
                return redirect('chainstores');
            } else {
                return redirect('portal');
            }


        } else {
            session()->flash('error', 'There are errors, please review form!');
            return redirect($postingUrl)->withErrors($v)->withInput();
        }
    }


    public function doUpdate($id, $input)
    {

        $shop = Shop::find($id);
        $mall = ShoppingMall::find($shop->mallID);
        $mallID = $mall->mallID;

        $data['name'] = trim($input['name']);
        $data['tradingHours'] = isset($input['tradingHours']) ? $input['tradingHours'] : '';
        $data['keywords'] = isset($input['keywords']) ? $input['keywords'] : '';
        $data['description'] = isset($input['description']) ? $input['description'] : '';
        $data['category'] = isset($input['category']) ? $input['category'] : '';

        if (isset($input['category_id']) && $input['category_id'] != 0) {
            $data['category_id'] = $input['category_id'];
            $data['category'] = ShopCategory::find($input['category_id'])->name;
            //dd($data['category']);
        } else {
            $data['category_id'] = 0;
        }
        $data['product1'] = isset($input['product1']) ? $input['product1'] : '';
        $data['product2'] = isset($input['product2']) ? $input['product2'] : '';
        $data['product3'] = isset($input['product3']) ? $input['product3'] : '';
        $data['product4'] = isset($input['product4']) ? $input['product4'] : '';
        $data['product5'] = isset($input['product5']) ? $input['product5'] : '';
        $data['product6'] = isset($input['product6']) ? $input['product6'] : '';
        $data['product7'] = isset($input['product7']) ? $input['product7'] : '';
        $data['product8'] = isset($input['product8']) ? $input['product8'] : '';
        $data['product9'] = isset($input['product9']) ? $input['product9'] : '';
        $data['product10'] = isset($input['product10']) ? $input['product10'] : '';
        $data['shopNumber'] = isset($input['shopNumber']) ? $input['shopNumber'] : '';
        $data['landmark'] = isset($input['landmark']) ? $input['landmark'] : '';
        $data['telephone'] = isset($input['telephone']) ? $input['telephone'] : '';
        $data['fax'] = isset($input['fax']) ? $input['fax'] : '';
        $data['cell'] = isset($input['cell']) ? $input['cell'] : '';
        $data['contactPerson'] = isset($input['contactPerson']) ? $input['contactPerson'] : '';
        $data['email'] = isset($input['email']) ? $input['email'] : '';
        $data['url'] = isset($input['url']) ? removeURLProtocol($input['url']) : '';
        $data['faceBookURL'] = isset($input['faceBookURL']) ? removeURLProtocol($input['faceBookURL']) : '';
        $data['newShop'] = isset($input['newShop']) ? $input['newShop'] : '';
        $data['visa'] = isset($input['visa']) ? $input['visa'] : '';
        $data['mastercard'] = isset($input['mastercard']) ? $input['mastercard'] : '';
        $data['amex'] = isset($input['amex']) ? $input['amex'] : '';
        $data['diners'] = isset($input['diners']) ? $input['diners'] : '';
        $data['map'] = isset($input['map']) ? $input['map'] : '';
        $data['mapX'] = isset($input['mapX']) ? $input['mapX'] : '';
        $data['mapY'] = isset($input['mapY']) ? $input['mapY'] : '';
        $data['display'] = isset($input['display']) ? $input['display'] : '';
        $data['subcategory'] = isset($input['subcategory']) ? $input['subcategory'] : '';
        if (isset($input['subcategory_id']) && $input['subcategory_id'] != 0) {
            $data['subcategory_id'] = $input['subcategory_id'];
            $data['subcategory'] = ShopCategory::find($input['subcategory_id'])->name;
        } else {
            $data['subcategory_id'] = 0;
        }
        $data['promoBlock'] = isset($input['promoBlock']) ? $input['promoBlock'] : '';
        $data['ownerName'] = isset($input['ownerName']) ? $input['ownerName'] : '';
        $data['ownerSurname'] = isset($input['ownerSurname']) ? $input['ownerSurname'] : '';
        $data['ownerEmail'] = isset($input['ownerEmail']) ? $input['ownerEmail'] : '';
        $data['ownerCell'] = isset($input['ownerCell']) ? $input['ownerCell'] : '';
        $data['managerName'] = isset($input['managerName']) ? $input['managerName'] : '';
        $data['managerSurname'] = isset($input['managerSurname']) ? $input['managerSurname'] : '';
        $data['managerEmail'] = isset($input['managerEmail']) ? $input['managerEmail'] : '';
        $data['managerCell'] = isset($input['managerCell']) ? $input['managerCell'] : '';
        $data['managerName2'] = isset($input['managerName2']) ? $input['managerName2'] : '';
        $data['managerSurname2'] = isset($input['managerSurname2']) ? $input['managerSurname2'] : '';
        $data['managerCell2'] = isset($input['managerCell2']) ? $input['managerCell2'] : '';
        $data['managerEmail2'] = isset($input['managerEmail2']) ? $input['managerEmail2'] : '';
        $data['managerName3'] = isset($input['managerName3']) ? $input['managerName3'] : '';
        $data['managerSurname3'] = isset($input['managerSurname3']) ? $input['managerSurname3'] : '';
        $data['managerCell3'] = isset($input['managerCell3']) ? $input['managerCell3'] : '';
        $data['managerEmail3'] = isset($input['managerEmail3']) ? $input['managerEmail3'] : '';
        $data['headOfficeName'] = isset($input['headOfficeName']) ? $input['headOfficeName'] : '';
        $data['headOfficeSurname'] = isset($input['headOfficeSurname']) ? $input['headOfficeSurname'] : '';
        $data['headOfficeCell'] = isset($input['headOfficeCell']) ? $input['headOfficeCell'] : '';
        $data['headOfficeEmail'] = isset($input['headOfficeEmail']) ? $input['headOfficeEmail'] : '';
        $data['opsManagerName'] = isset($input['opsManagerName']) ? $input['opsManagerName'] : '';
        $data['opsManagerSurname'] = isset($input['opsManagerSurname']) ? $input['opsManagerSurname'] : '';
        $data['opsManagerCell'] = isset($input['opsManagerCell']) ? $input['opsManagerCell'] : '';
        $data['opsManagerEmail'] = isset($input['opsManagerEmail']) ? $input['opsManagerEmail'] : '';
        $data['financialName'] = isset($input['financialName']) ? $input['financialName'] : '';
        $data['financialSurname'] = isset($input['financialSurname']) ? $input['financialSurname'] : '';
        $data['financialCell'] = isset($input['financialCell']) ? $input['financialCell'] : '';
        $data['financialEmail'] = isset($input['financialEmail']) ? $input['financialEmail'] : '';
        $data['areaManagerName'] = isset($input['areaManagerName']) ? $input['areaManagerName'] : '';
        $data['areaManagerSurname'] = isset($input['areaManagerSurname']) ? $input['areaManagerSurname'] : '';
        $data['areaManagerCell'] = isset($input['areaManagerCell']) ? $input['areaManagerCell'] : '';
        $data['areaManagerEmail'] = isset($input['areaManagerEmail']) ? $input['areaManagerEmail'] : '';
        $data['emergencyName'] = isset($input['emergencyName']) ? $input['emergencyName'] : '';
        $data['emergencySurname'] = isset($input['emergencySurname']) ? $input['emergencySurname'] : '';
        $data['emergencyCell'] = isset($input['emergencyCell']) ? $input['emergencyCell'] : '';
        $data['emergencyEmail'] = isset($input['emergencyEmail']) ? $input['emergencyEmail'] : '';
        $data['emergencyName2'] = isset($input['emergencyName2']) ? $input['emergencyName2'] : '';
        $data['emergencySurname2'] = isset($input['emergencySurname2']) ? $input['emergencySurname2'] : '';
        $data['emergencyCell2'] = isset($input['emergencyCell2']) ? $input['emergencyCell2'] : '';
        $data['emergencyEmail2'] = isset($input['emergencyEmail2']) ? $input['emergencyEmail2'] : '';
        $data['location2'] = isset($input['location2']) ? $input['location2'] : '';
        $data['location3'] = isset($input['location3']) ? $input['location3'] : '';
        $data['googleMapX'] = isset($input['googleMapX']) ? $input['googleMapX'] : '';
        $data['googleMapY'] = isset($input['googleMapY']) ? $input['googleMapY'] : '';
        $data['blog'] = isset($input['blog']) ? $input['blog'] : '';
        $data['blog_url'] = isset($input['blog_url']) ? removeURLProtocol($input['blog_url']) : '';
        $reserveWith = isset($input['reserveWith']) ? $input['reserveWith'] : '';
        $cards = isset($input['cards']) ? $input['cards'] : '';
        $brands = isset($input['brands']) ? $input['brands'] : array();

        //custom thumbnails
        $thumbnails = isset($input['thumbnail']) ? $input['thumbnail'] : array();

        if (is_array($brands)) {
            $shop->brands()->sync($brands);
        }

        if (is_array($cards) && sizeof($cards) > 0) {
            DB::table('shopCards')->where('shopMGID', '=', $id)->delete();
            foreach ($cards as $card) {
                DB::table('shopCards')->insert(array('shopMGID' => $id, 'cardID' => $card));
            }
        }

        $data['reserveWith'] = '';
        if (is_array($reserveWith)) {
            $data['reserveWith'] = isset($reserveWith) ? implode($reserveWith, ',') : '';
        }

        $destinationPath = base_path('/uploadimages') . '/mall_' . $mallID;

        if (isset($mall->thumbWidth)) {
            $thumbMaxWidth = $mall->thumbWidth;
        } else {
            $thumbMaxWidth = 150;
        }

        $logo = isset($input['logo']) ? $input['logo'] : null;
        $image1 = isset($input['image1']) ? $input['image1'] : null;
        $image2 = isset($input['image2']) ? $input['image2'] : null;
        $image3 = isset($input['image3']) ? $input['image3'] : null;
        $image4 = isset($input['image4']) ? $input['image4'] : null;
        $image5 = isset($input['image5']) ? $input['image5'] : null;
        $image6 = isset($input['image6']) ? $input['image6'] : null;
        $image7 = isset($input['image7']) ? $input['image7'] : null;
        $image8 = isset($input['image8']) ? $input['image8'] : null;
        $image9 = isset($input['image9']) ? $input['image9'] : null;
        $image10 = isset($input['image10']) ? $input['image10'] : null;

        if (isset($logo)) {
            $logo1 = uploadImage($logo, $destinationPath, $thumbMaxWidth, true, true);

            $data['logo'] = $logo1['thumbnail'];
            $data['logoBig'] = $logo1['largeImage'];

        }

        if (isset($image1)) {
            $oldImage1 = $shop->image1;
            removeOldImage($destinationPath, $oldImage1);

            $create_thumbnail = true;
            if (isset($thumbnails[1]) && $thumbnails[1] != '') {
                $create_thumbnail = false;
            }

            $img1 = uploadImage($image1, $destinationPath, $thumbMaxWidth, $create_thumbnail, true);

            if ($create_thumbnail) {
                $data['image1'] = $img1['thumbnail'];
            }

            $data['imageBig1'] = $img1['largeImage'];
        }

        if (isset($image2)) {
            $oldImage2 = $shop->image2;
            removeOldImage($destinationPath, $oldImage2);

            $create_thumbnail = true;
            if (isset($thumbnails[2]) && $thumbnails[2] != '') {
                $create_thumbnail = false;
            }

            $img2 = uploadImage($image2, $destinationPath, $thumbMaxWidth, $create_thumbnail, true);

            if ($create_thumbnail) {
                $data['image2'] = $img2['thumbnail'];
            }
            $data['imageBig2'] = $img2['largeImage'];
        }

        if (isset($image3)) {
            $oldImage3 = $shop->image3;
            removeOldImage($destinationPath, $oldImage3);


            $create_thumbnail = true;
            if (isset($thumbnails[3]) && $thumbnails[3] != '') {
                $create_thumbnail = false;
            }

            $img3 = uploadImage($image3, $destinationPath, $thumbMaxWidth, $create_thumbnail, true);

            if ($create_thumbnail) {
                $data['image3'] = $img3['thumbnail'];
            }
            $data['imageBig3'] = $img3['largeImage'];

        }

        if (isset($image4)) {
            $oldImage4 = $shop->image4;
            removeOldImage($destinationPath, $oldImage4);

            $create_thumbnail = true;
            if (isset($thumbnails[4]) && $thumbnails[4] != '') {
                $create_thumbnail = false;
            }

            $img4 = uploadImage($image4, $destinationPath, $thumbMaxWidth, $create_thumbnail, true);

            if ($create_thumbnail) {
                $data['image4'] = $img4['thumbnail'];
            }
            $data['imageBig4'] = $img4['largeImage'];
        }

        if (isset($image5)) {

            $oldImage5 = $shop->image5;
            removeOldImage($destinationPath, $oldImage5);

            $create_thumbnail = true;
            if (isset($thumbnails[5]) && $thumbnails[5] != '') {
                $create_thumbnail = false;
            }

            $img5 = uploadImage($image5, $destinationPath, $thumbMaxWidth, $create_thumbnail, true);

            if ($create_thumbnail) {
                $data['image5'] = $img5['thumbnail'];
            }
            $data['imageBig5'] = $img5['largeImage'];

        }

        if (isset($image6)) {

            $oldImage6 = $shop->image6;

            removeOldImage($destinationPath, $oldImage6);

            $create_thumbnail = true;
            if (isset($thumbnails[6]) && $thumbnails[6] != '') {
                $create_thumbnail = false;
            }

            $img6 = uploadImage($image6, $destinationPath, $thumbMaxWidth, $create_thumbnail, true);

            if ($create_thumbnail) {
                $data['image6'] = $img6['thumbnail'];
            }
            $data['imageBig6'] = $img6['largeImage'];
        }

        if (isset($image7)) {

            $oldImage7 = $shop->image7;

            removeOldImage($destinationPath, $oldImage7);

            $create_thumbnail = true;
            if (isset($thumbnails[7]) && $thumbnails[7] != '') {
                $create_thumbnail = false;
            }

            $img7 = uploadImage($image7, $destinationPath, $thumbMaxWidth, $create_thumbnail, true);

            if ($create_thumbnail) {
                $data['image7'] = $img7['thumbnail'];
            }
            $data['imageBig7'] = $img7['largeImage'];
        }

        if (isset($image8)) {

            $oldImage8 = $shop->image8;

            removeOldImage($destinationPath, $oldImage8);

            $create_thumbnail = true;
            if (isset($thumbnails[8]) && $thumbnails[8] != '') {
                $create_thumbnail = false;
            }

            $img8 = uploadImage($image8, $destinationPath, $thumbMaxWidth, $create_thumbnail, true);

            if ($create_thumbnail) {
                $data['image8'] = $img8['thumbnail'];
            }
            $data['imageBig8'] = $img8['largeImage'];
        }

        if (isset($image9)) {

            $oldImage9 = $shop->image9;

            removeOldImage($destinationPath, $oldImage9);
            $create_thumbnail = true;
            if (isset($thumbnails[9]) && $thumbnails[9] != '') {
                $create_thumbnail = false;
            }

            $img9 = uploadImage($image9, $destinationPath, $thumbMaxWidth, $create_thumbnail, true);

            if ($create_thumbnail) {
                $data['image9'] = $img9['thumbnail'];
            }
            $data['imageBig9'] = $img9['largeImage'];
        }

        if (isset($image10)) {

            $oldImage10 = $shop->image10;

            removeOldImage($destinationPath, $oldImage10);
            $create_thumbnail = true;
            if (isset($thumbnails[10]) && $thumbnails[10] != '') {
                $create_thumbnail = false;
            }

            $img10 = uploadImage($image10, $destinationPath, $thumbMaxWidth, $create_thumbnail, true);

            if ($create_thumbnail) {
                $data['image10'] = $img10['thumbnail'];
            }
            $data['imageBig10'] = $img10['largeImage'];
        }

        // custom thumbnails
        if (count($thumbnails)) {
            foreach ($thumbnails as $idx => $ddata) {
                if ($ddata != '' && substr($ddata, 0, 1) == '{') {
                    if (isset($data['imageBig' . $idx]) && $data['imageBig' . $idx] != '') {
                        $image = $data['imageBig' . $idx];
                    } else {
                        $image = $shop->{'imageBig' . $idx};
                    }
                    $dimension = json_decode($ddata);
                    if ($thumbnail = makeThumbnail($image, $destinationPath, $dimension->x, $dimension->y, $dimension->width, $dimension->height)) {
                        $data['image' . $idx] = $thumbnail;
                    }
                }
            }
        }
        return $shop->update($data);
    }
}
