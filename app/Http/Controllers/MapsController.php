<?php

namespace App\Http\Controllers;

use App\Map;
use App\ShoppingMall;
use Illuminate\Http\Request;

class MapsController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel.auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('admin.maps.index');
    }

    /**
     * @param $mallID
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getMapsByMall($mallID) {
        $data['mallId'] = $mallID;
        $data['mallName'] = ShoppingMall::find($mallID)->name;
        return view('admin.maps.list')->with($data);
    }

    /**
     * @param $mallID
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate($mallID) {
        $data['mall'] = ShoppingMall::find($mallID);
        $data['heading'] = $data['mall']->name .' >> Add Map';
        return view('admin.maps.create')->with($data);
    }

    /**
     * @param Request $request
     * @param $mallID
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCreate(Request $request, $mallID) {

        $request->validate([
            'location' => 'required',
            'imageSrc' => 'required|mimes:jpeg,png,gif|image'
        ]);

        $mapData['location'] = $request->get('location');
        $mapData['mallID'] = $mallID;

        $destinationPath = base_path('uploadimages/mall_'. $mallID);

        $imageSrc = uploadImage($request->file('imageSrc'), $destinationPath);
        $mapData['imageSrc'] = $imageSrc['unresized'];

        Map::create($mapData);
        return redirect('/admin/maps/' . $mallID);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id) {
        $map = Map::find($id);
        $data['mall'] = ShoppingMall::find($map->mallID);
        $data['map'] = $map;
        $data['heading'] = 'Edit >> ' . $map->location;
        return view('admin.maps.create')->with($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postUpdate(Request $request, $id) {

        $request->validate([
            'location' => 'required',
            'imageSrc' => 'mimes:jpeg,png,gif|image'
        ]);

        $map = Map::find($id);
        $map->location = $request->get('location');
        $destinationPath = base_path('uploadimages/mall_'. $map->mallID);

        if ($request->has('imageSrc')) {
            $imageSrc = uploadImage($request->file('imageSrc'), $destinationPath);
            $map->imageSrc = $imageSrc['unresized'];
        }

        $map->save();
        return redirect('/admin/maps/' . $map->mallID);
    }

    /**
     * @param $id
     * @return string
     */
    public function postDelete($id) {
        if (Map::find($id)->delete()) {
            return 'success';
        }
    }
}
