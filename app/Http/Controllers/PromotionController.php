<?php

namespace App\Http\Controllers;

use App\Promotion;
use App\Shop;
use App\ShoppingMall;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PromotionController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel.auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin.promotions.index');
    }

    /**
     * @param $mallId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getMallShops($mallId)
    {
        $shops = callApi('promotions/mall/' . $mallId);
        return response($shops)->header('Content-Type', 'application/json');

    }

    /**
     * @param $shopId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getShopPromotions($shopId)
    {
        $data['shop'] = Shop::find($shopId);
        return view('admin.promotions.shop_promotions')->with($data);
    }

    /**
     * @param $shopId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function shopPromotionsDataPoint($shopId)
    {
        $promotions = callApi('promotions/shop/' . $shopId);
        return response($promotions)->header('Content-Type', 'application/json');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {

        $promotion = Promotion::find($id);
        $shop = Shop::find($promotion->shopMGID);
        $mall = ShoppingMall::find($shop->mallID);

        $data['promotion'] = $promotion;
        $data['shop'] = $shop;
        $data['mall'] = $mall;

        return view('admin.promotions.edit')->with($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postUpdate(Request $request, $id)
    {

        $rules = [
            'title' => 'required',
            'startDate' => 'required|date',
            'endDate' => 'required|date',
            'promotion' => 'required',
            'image1' => 'mimes:jpeg,png,gif|image',
            'image2' => 'mimes:jpeg,png,gif|image'
        ];

        $form = $request->except('_token');

        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {
            $promotion = Promotion::find($id);
            $mallId = $promotion->mallID;
            $uploadPath = base_path('uploadimages/mall_' . $mallId);

            $promotion->title = $request->get('title');
            $promotion->startDate = $request->get('startDate');
            $promotion->endDate = $request->get('endDate');
            $promotion->promotion = str_replace("&nbsp;", "", $request->get('promotion'));

            $thumbMaxWidth = 150;
            $largeMaxWidth = 450;

            if ($request->has('image1')) {
                $file1 = $request->file('image1');
                $image1 = uploadImage($file1, $uploadPath, $thumbMaxWidth, $largeMaxWidth, $thumb = true, $large = true);

                $promotion->thumbnail1 = $image1['thumbnail'];
                $promotion->image1 = $image1['largeImage'];
            }

            if ($request->has('image2')) {
                $file2 = $request->file('image2');
                $image2 = uploadImage($file2, $uploadPath, $thumbMaxWidth, $largeMaxWidth, $thumb = true, $large = true);

                $promotion->thumbnail2 = $image2['thumbnail'];
                $promotion->image2 = $image2['largeImage'];
            }

            if ($promotion->save()) {
                session()->flash('success', 'Promotion details have been successfully updated');
            } else {
                session()->flash('error', 'An error occurred while updating promotion details');
                return back()->withErrors($validation);
            }
        } else {
            session()->flash('error', 'There are some errors, please review the form');
            return back()->withErrors($validation);
        }

        return redirect('/admin/shop-promotions/' . $promotion->shopMGID);
    }

    /**
     * @param $shopId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate($shopId)
    {

        $data['shop'] = Shop::find($shopId);
        return view('admin.promotions.edit')->with($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCreate(Request $request)
    {
        $rules = [
            'title' => 'required',
            'startDate' => 'required|date',
            'endDate' => 'required|date',
            'promotion' => 'required',
            'image1' => 'mimes:jpeg,png,gif|image',
            'image2' => 'mimes:jpeg,png,gif|image'
        ];

        $form = $request->except('_token');

        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {

            $promoData = [
                'title' => $request->get('title'),
                'startDate' => $request->get('startDate'),
                'endDate' => $request->get('endDate'),
                'promotion' => $request->get('promotion'),
                'shopMGID' => $request->get('shopMGID'),
                'shopID' => $request->get('shopMGID'),
                'mallID' => $request->get('mallID'),
                'category' => '',
            ];

            $uploadPath = base_path('uploadimages/mall_' . $request->get('mallID'));
            $thumbMaxWidth = 150;
            $largeMaxWidth = 450;

            if ($request->has('image1')) {
                $file1 = $request->file('image1');
                $image1 = uploadImage($file1, $uploadPath, $thumbMaxWidth, $largeMaxWidth, $thumb = true, $large = true);

                $promoData['thumbnail1'] = $image1['thumbnail'];
                $promoData['image1'] = $image1['largeImage'];
            }

            if ($request->has('image2')) {
                $file2 = $request->file('image2');
                $image2 = uploadImage($file2, $uploadPath, $thumbMaxWidth, $largeMaxWidth, $thumb = true, $large = true);

                $promoData['thumbnail2'] = $image2['thumbnail'];
                $promoData['image2'] = $image2['largeImage'];
            }

            Promotion::create($promoData);

        } else {
            session()->flash('error', 'There are some errors, please review the form');
            return back()->withErrors($validation);
        }

        return redirect('/admin/shop-promotions/' . $request->get('shopMGID'));
    }

    /**
     * @param $mallId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreateMallPromotion($mallId)
    {

        $data['mall'] = ShoppingMall::find($mallId);
        $shops = Shop::where('mallID', $mallId)->where('display', 'Y')->orderBy('name')->select('name', 'shopMGID')->get();
        $shopSelect = [];
        foreach ($shops as $shop) {
            $shopSelect[$shop->shopMGID] = $shop->name;
        }

        $data['shops'] = $shopSelect;

        return view('admin.promotions.create_mall')->with($data);
    }

    /**
     * @param $id
     * @return string
     */
    public function postDelete($id)
    {
        if (Promotion::find($id)->delete()) {
            return 'success';
        }
    }
}
