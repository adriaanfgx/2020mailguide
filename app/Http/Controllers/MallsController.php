<?php

namespace App\Http\Controllers;

use App\Country;
use App\ShoppingMall;
use App\Province;
use App\City;
use App\Category;
use App\TradingHour;
use DateTime;
use App\ShoppingMallDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class MallsController extends Controller
{
    protected $country;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Country $country)
    {
        $this->middleware(['sentinel.auth', 'is_admin']);
        $this->country = $country;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.malls.list');
    }

    public function events()
    {
        return view('events');
    }

    public function getAddMall()
    {
        $data['countries'] = Country::orderBy('name')->get(['id', 'name']);

        $data['categories'] = Category::orderBy('name')->get(['id', 'name']);

        return view('admin.malls.add', $data);
    }

    public function postAddMall(Request $request) {
        $form = $request->except('_token');

        $rules = [
            'name' => 'required|min:4'
        ];

        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {
            $mallId = ShoppingMall::insertGetId($form);

            Session::flash('success', 'The mall details have been successfully updated...');

            return Redirect::to(route('malls.edit.get', ['id' => $mallId]));
        } else {
            Session::put('error', 'There are some errors, please review the form...');
            return Redirect::to(route('mall.add.get'))->withErrors($validation)->withInput();
        }
    }

    public function edit($id)
    {
        $mall = ShoppingMall::with('details')->find($id);

        $data['mall'] = $mall;

        $data['countries'] = Country::orderBy('name')->get(['id', 'name']);

        $data['provinces'] = Province::where('country_id', $mall->country_id)->orderBy('name')->get(['id', 'name']);

        $data['cities'] = City::where('province_id', $mall->province_id)->orderBy('name')->get(['id', 'name']);

        $data['categories'] = Category::orderBy('name')->get(['id', 'name']);

        $data['startTimeRanges'] = array('' => 'Select ...', 'closed' => 'Closed') + $this->hoursRange();

        $data['endTimeRanges'] = array('' => 'Select ...', 'Till Late' => 'Until late', 'optional' => 'Optional') + $this->hoursRange();

        return view('admin.malls.edit', $data);
    }

    public function postMallDetailsForm($id, Request $request)
    {
        $form = $request->except('_token');

        $rules = [
            'name' => 'required|min:4'
        ];

        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {

            $mall = ShoppingMall::find($id);
            $result = $mall->update($form);

            if ($result) {
                Session::flash('success', 'The mall details have been successfully updated...');
            } else {
                Session::flash('error', 'An error occurred while saving...');
            }

            return Redirect::to(route('malls.edit.get', ['id' => $id]));

        } else {
            Session::put('error', 'There are some errors, please review the form...');
            return Redirect::to(route('malls.edit.get', ['id' => $id]))->withErrors($validation)->withInput();
        }
    }

    public function postMallInformationForm($id, Request $request)
    {

        $form = $request->except('_token');

        $mall = ShoppingMall::find($id);

        $mall->mallDescription = $form['mallDescription'];

        $mall->save();

        $information = array(
            'management' => $form['details_management'],
            'marketing' => $form['details_marketing'],
            'parking' => $form['details_parking'],
            'security' => $form['details_security'],
            'facilities' => $form['details_facilities'],
            'leasing' => $form['details_leasing'],
            'giftcards' => $form['details_giftcards'],
            'tourism' => $form['details_tourism']
        );

        $mallDetails = $mall->details;

        if ($mallDetails) {
            $saved = $mallDetails->update($information);
        } else {
            $new = new ShoppingMallDetail($information);

            $saved = $mall->details()->save($new);
        }

        if ($saved) {
            Session::flash('success', 'The mall details have been successfully updated...');
        } else {
            Session::flash('error', 'An error occurred while saving...');
        }

        return Redirect::to(route('malls.edit.get', ['id' => $id]));
    }

    public function postMallImagesForm($id, Request $request)
    {
        $files = $request->allFiles();
        $rules = [
            'logo' => 'mimes:jpeg,png,gif|image',
            'image1' => 'mimes:jpeg,png,gif|image',
            'image2' => 'mimes:jpeg,png,gif|image',
            'image3' => 'mimes:jpeg,png,gif|image',
            'image4' => 'mimes:jpeg,png,gif|image',
            'image5' => 'mimes:jpeg,png,gif|image',
            'image6' => 'mimes:jpeg,png,gif|image',
            'image7' => 'mimes:jpeg,png,gif|image',
            'image8' => 'mimes:jpeg,png,gif|image',
            'image9' => 'mimes:jpeg,png,gif|image',
            'image10' => 'mimes:jpeg,png,gif|image',
        ];

        $validation = Validator::make($files, $rules);

        if ($validation->passes()) {
            $mall = ShoppingMall::find($id);

            $storagePath = base_path('uploadimages/mall_'. $mall->mallID);

            if (isset($Mall->thumbWidth)) {
                $thumbMaxWidth = $mall->thumbWidth;

            } else {
                $thumbMaxWidth = 260;
            }

            $updatedImages = [];
            foreach ($files as $key => $file) {

                if ($key == 'logo') {
                    $image = uploadImage($file, $storagePath, $thumbMaxWidth, $thumb = true, $large = true);
                    $updatedImages['logo'] = $image['thumbnail'];
                } else {
                    $imageNumber = preg_replace('/[^0-9]/', '', $key);

                    $imageColumnName = 'image' . $imageNumber;

                    $imageThumbnailColumnName = 'thumbnail' . $imageNumber;

                    $image = uploadImage($file, $storagePath, $thumbMaxWidth, $thumb = true, $large = true);

                    $updatedImages[$imageColumnName] = $image['largeImage'];
                    $updatedImages[$imageThumbnailColumnName] = $image['thumbnail'];
                }
            }

            $saved = $mall->update($updatedImages);

            if ($saved) {
                Session::flash('success', 'The mall details have been successfully updated...');
            } else {
                Session::flash('error', 'An error occurred while saving mall images...');
            }

            return Redirect::to(route('malls.edit.get', ['id' => $id]));
        } else {
            Session::put('error', 'There are some errors, please review the form...');
            return back()->withErrors($validation)->withInput();
        }
    }

    public function postMallSpecialFeaturesForm($id, Request $request)
    {
        $form = $request->except('_token');

        $mall = ShoppingMall::find($id);

        $saved = $mall->update($form);

        if ($saved) {
            Session::flash('success', 'The mall details have been successfully updated...');
        } else {
            Session::flash('error', 'An error occurred while saving mall images...');
        }

        return Redirect::to(route('malls.edit.get', ['id' => $id]))->withInput();
    }

    public function postMallCentreManagementForm($id, Request $request)
    {
        $form = $request->except('_token');

        $rules = [
            'centreManagerEmail' => 'min:4|max:64|email',
        ];

        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {
            $mall = ShoppingMall::find($id);

            $saved = $mall->update($form);

            if ($saved) {
                Session::flash('success', 'The mall details have been successfully updated...');
            } else {
                Session::flash('error', 'An error occurred while saving mall images...');
            }

            return Redirect::to(route('malls.edit.get', ['id' => $id]));
        } else {
            Session::put('error', 'There are some errors, please review the form...');
            return Redirect::to(route('malls.edit.get', ['id' => $id]))->withErrors($validation)->withInput();
        }
    }

    public function postMallMarketingConsultantForm($id, Request $request)
    {
        $form = $request->except('_token');

        $rules = [
            'marketingManagerEmail' => 'min:4|max:64|email',
            'marketingAssistantEmail' => 'min:4|max:64|email',
        ];

        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {
            $mall = ShoppingMall::find($id);

            $saved = $mall->update($form);

            if ($saved) {
                Session::flash('success', 'The mall details have been successfully updated...');
            } else {
                Session::flash('error', 'An error occurred while saving mall images...');
            }

            return Redirect::to(route('malls.edit.get', ['id' => $id]));
        } else {
            Session::put('error', 'There are some errors, please review the form...');
            return Redirect::to(route('malls.edit.get', ['id' => $id]))->withErrors($validation)->withInput();
        }
    }

    public function postMallLeasingAgentForm($id, Request $request)
    {
        $form = $request->except('_token');

        $rules = [
            'leasingEmail' => 'min:4|max:64|email'
        ];

        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {
            $mall = ShoppingMall::find($id);

            $saved = $mall->update($form);

            if ($saved) {
                Session::flash('success', 'The mall details have been successfully updated...');
            } else {
                Session::flash('error', 'An error occurred while saving mall images...');
            }

            return back();
        } else {
            Session::put('error', 'There are some errors, please review the form...');
            return back()->withErrors($validation)->withInput();
        }
    }

    public function postMallShoppingCouncilForm($id, Request $request)
    {
        $form = $request->except('_token');

        $mall = ShoppingMall::find($id);

        $saved = $mall->update($form);

        if ($saved) {
            Session::flash('success', 'The mall details have been successfully updated...');
        } else {
            Session::flash('error', 'An error occurred while saving mall images...');
        }

        return Redirect::to(route('malls.edit.get', ['id' => $id]));
    }

    public function postMallTradingHoursForm($id, Request $request)
    {
        $form = $request->except('_token');

        $mallTradingHours = ShoppingMall::find($id)->tradinghour;

        if (!isset($mallTradingHours)) {
            $saved = TradingHour::insert(
                [
                    'mallID' => $id,
                    'startHoursMon' => $form['startHoursMon'],
                    'endHoursMon' => $form['endHoursMon'],
                    'startHoursTues' => $form['startHoursTues'],
                    'endHoursTues' => $form['endHoursTues'],
                    'startHoursWed' => $form['startHoursWed'],
                    'endHoursWed' => $form['endHoursWed'],
                    'startHoursThurs' => $form['startHoursThurs'],
                    'endHoursThurs' => $form['endHoursThurs'],
                    'startHoursFri' => $form['startHoursFri'],
                    'endHoursFri' => $form['endHoursFri'],
                    'startHoursSat' => $form['startHoursSat'],
                    'endHoursSat' => $form['endHoursSat'],
                    'startHoursSun' => $form['startHoursSun'],
                    'endHoursSun' => $form['endHoursSun'],
                    'startHoursPublicHolidays' => $form['startHoursPublicHolidays'],
                    'endHoursPublicHolidays' => $form['endHoursPublicHolidays']
                ]
            );
        } else {
            $saved = TradingHour::where('mallID', '=', $id)->update(
                [
                    'startHoursMon' => $form['startHoursMon'],
                    'endHoursMon' => $form['endHoursMon'],
                    'startHoursTues' => $form['startHoursTues'],
                    'endHoursTues' => $form['endHoursTues'],
                    'startHoursWed' => $form['startHoursWed'],
                    'endHoursWed' => $form['endHoursWed'],
                    'startHoursThurs' => $form['startHoursThurs'],
                    'endHoursThurs' => $form['endHoursThurs'],
                    'startHoursFri' => $form['startHoursFri'],
                    'endHoursFri' => $form['endHoursFri'],
                    'startHoursSat' => $form['startHoursSat'],
                    'endHoursSat' => $form['endHoursSat'],
                    'startHoursSun' => $form['startHoursSun'],
                    'endHoursSun' => $form['endHoursSun'],
                    'startHoursPublicHolidays' => $form['startHoursPublicHolidays'],
                    'endHoursPublicHolidays' => $form['endHoursPublicHolidays']
                ]
            );

        }

        if ($saved) {
            Session::flash('success', 'The mall details have been successfully updated...');
        } else {
            Session::flash('error', 'An error occurred while saving mall images...');
        }

        return Redirect::to(route('malls.edit.get', ['id' => $id]));
    }

    public function postMallSettingsForm($id, Request $request)
    {
        $form = $request->except('_token');

        $mall = ShoppingMall::find($id);

        $saved = $mall->update($form);

        if ($saved) {
            Session::flash('success', 'The mall details have been successfully updated...');
        } else {
            Session::flash('error', 'An error occurred while saving mall images...');
        }

        return Redirect::to(route('malls.edit.get', ['id' => $id]));
    }

    /**
     * @param int $lower
     * @param int $upper
     * @param int $step
     * @param string $format
     * @return array dr
     * @throws \Exception
     */
    function hoursRange($lower = 0, $upper = 86400, $step = 1800, $format = '')
    {
        $times = array();

        if (empty($format)) {
            $format = 'g:i A';
        }

        foreach (range($lower, $upper, $step) as $increment) {
            $increment = gmdate('g:iA', $increment);

            list($hour, $minutes) = explode(':', $increment);

            $date = new DateTime($hour . ':' . $minutes);

            $times[(string)$increment] = $date->format($format);
        }

        return $times;
    }

    public function deleteMallRemoveImage($mallId, $imageId)
    {
        ShoppingMall::find($mallId)->update(array($imageId => ''));
    }
}
