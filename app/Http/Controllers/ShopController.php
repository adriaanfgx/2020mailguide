<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Card;
use App\Category;
use App\Exports\ShopsExport;
use App\Map;
use App\MGGeneric;
use App\Shop;
use App\ShopCategory;
use App\Shopfile;
use App\ShoppingMall;
use App\SubCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;
use Maatwebsite\Excel\Excel;

class ShopController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel.auth');
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getCategories() {
        $categories = Category::orderBy('name')->get();
        return response($categories)->header('Content-Type', 'application/json');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('admin.shops.index');
    }

    /**
     * @param $provinceId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getProvinceMalls($provinceId) {
        $malls = callApi('shops/province/'. $provinceId);
        return response($malls)->header('Content-Type', 'application/json');
    }

    /**
     * @param $mallId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getShopsByMall($mallId) {
        $data['mall'] = ShoppingMall::find($mallId);
        return view('admin.shops.mall_shops')->with($data);
    }

    /**
     * @param $mallId
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function mallShopsData($mallId) {
        $shops = callApi('shops/mall/'. $mallId);
        return response($shops)->header('Content-Type', 'application/json');
    }

    /**
     * @param $mallID
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate($mallID) {

        $data['mall'] = ShoppingMall::find($mallID);
        $data['categories'] = Category::orderBy('name')->get();
        $data['subcategories'] = SubCategory::orderBy('name')->pluck('name','id');

        return view('admin.shops.create')->with($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCreate(Request $request) {

        $request->validate([
            'name' => 'required'
        ]);

        $data = [
            'name' => $request->name,
            'mallID' => $request->mallID,
            'category_id' => $request->category_id,
            'tradingHours' => $request->tradingHours,
            'description' => $request->description,
            'keywords' => $request->keywords,
            'faceBookURL' => '',
            'category' => $request->category,
            'subcategory_id' => $request->subcategory_id
        ];

        $shop = Shop::create($data);
        session()->flash('success', 'The shop was successfully created');
        return redirect('/admin/shops/edit/' . $shop->shopMGID);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id) {

        $shop = Shop::find($id);
//        dd($shop->subcategory_id);
        $data['shop'] = $shop;
        $data['mall'] = ShoppingMall::find($data['shop']->mallID);
        $data['categories'] = Category::orderBy('name')->get();
        $data['mapImage'] = Map::where('mallID','=',$data['shop']->mallID)->orderBy('location')->get();
        $data['brands'] = Brand::orderBy('name')->pluck('name', 'id');
        $data['otherCards'] = Card::orderBy('title')->get();
        $data['otherCardsSelected'] = DB::table('shopCards')->where('shopMGID','=',$id)->pluck('cardID')->toArray();
        $data['subcategories'] = SubCategory::where('category_id','=',$shop->category_id)->orderBy('name')->pluck('name','id');


        return view('admin.shops.edit')->with($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postUpdate(Request $request, $id) {
        $request->validate([
            'name' => 'required'
        ]);

        $shop = Shop::find($id);
        $shop->name = $request->get('name');
        $shop->category_id = $request->get('category_id');
        $shop->tradingHours = $request->get('tradingHours');
        $shop->description = $request->get('description');
        $shop->keywords = $request->get('keywords');
        $shop->newShop = $request->get('newShop');
        $shop->display = $request->get('display');
        $shop->blog = $request->get('blog');
        $shop->category = $request->get('category');
        $shop->subcategory_id = $request->get('subcategory_id');

        if ($shop->save()) {
            session()->flash('success', 'The shop details were successfully updated');
            return redirect('/admin/shops/edit/' . $shop->shopMGID);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postProductDetails(Request $request) {

        $shop = Shop::find($request->get('shopId'));
        for ($i = 1; $i <= 10; $i++) {
            $shop->{'product'.$i} = $request->get('product' . $i);
        }

        if ($shop->save()) {
            session()->flash('success', 'The product details were successfully updated');
            session()->flash('product_success', 'The product details were successfully updated');
            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#shop_products');
        }
    }

    public function postContactDetails(Request $request) {

        $shop = Shop::find($request->get('shopId'));
        $validator = Validator::make($request->all(), [
            'email' => 'nullable|email',
            'url' => 'nullable|url'
        ]);

        if ($validator->fails()) {

            session()->flash('error_contact', 'There was an error with your submission, please review the form');

            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#contact_details')
                ->withErrors($validator)
                ->withInput();
        }

        $facebookUrl = $request->get('facebookURL');
        if ($request->get('facebookURL') === null) {
            $facebookUrl = '';
        }
        $shop->shopNumber = $request->get('shopNumber');
        $shop->telephone = $request->get('telephone');
        $shop->cell = $request->get('cell');
        $shop->email = $request->get('email');
        $shop->googleMapX = $request->get('googleMapX');
        $shop->googleMapY = $request->get('googleMapY');
        $shop->fax = $request->get('fax');
        $shop->contactPerson = $request->get('contactPerson');
        $shop->url = $request->get('url');
        $shop->facebookURL = $facebookUrl;
        $shop->landmark = $request->get('landmark');

        if ($shop->save()) {
            session()->flash('success_contact', 'The contact details were successfully updated');
            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#contact_details');
        } else {
            session()->flash('error_contact', 'There was an error with your submission, please review the form');
            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#contact_details');
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEditMapImage($id) {
        $data['map'] = Map::find($id);
        return view('admin.maps.edit_image')->with($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postMapDetails(Request $request) {
        $shop = Shop::find($request->get('shopId'));
        $shop->map = $request->get('map');
        $shop->mapX = $request->get('mapX');
        $shop->mapY = $request->get('mapY');

        if ($shop->save()) {
            session()->flash('success_map', 'The map details were successfully updated');
            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#map_details');
        } else {
            session()->flash('error_map', 'There was an error with your submission, please review the form');
            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#map_details');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postImageDetails(Request $request) {

        $validator = Validator::make($request->all(), [
            'logo' => 'mimes:jpeg,png,gif|image',
            'image1' => 'mimes:jpeg,png,gif|image',
            'image2' => 'mimes:jpeg,png,gif|image',
            'image3' => 'mimes:jpeg,png,gif|image',
            'image4' => 'mimes:jpeg,png,gif|image',
            'image5' => 'mimes:jpeg,png,gif|image',
            'image6' => 'mimes:jpeg,png,gif|image',
            'image7' => 'mimes:jpeg,png,gif|image',
            'image8' => 'mimes:jpeg,png,gif|image',
            'image9' => 'mimes:jpeg,png,gif|image',
            'image10' => 'mimes:jpeg,png,gif|image'
        ]);

        $shop = Shop::find($request->get('shopId'));
        if ($validator->fails()) {

            session()->flash('error_imageupload', 'There was an error with your submission, please review the form');

            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#image_info')
                ->withErrors($validator)
                ->withInput();
        }

        $destinationPath = base_path('/uploadimages/mall_'.$shop->mallID);

        $mall = ShoppingMall::find($shop->mallID);

        if(isset($mall->thumbWidth) ){
            $thumbMaxWidth = $mall->thumbWidth;
        }else{
            $thumbMaxWidth = 150;
        }

        if($request->has('logo')){
            $logo = $request->file('logo');
            $logo1 = uploadImage($logo,$destinationPath,$thumbMaxWidth,$thumb=true,$large=true);

            $shop->logo = $logo1['thumbnail'];
            $shop->logoBig = $logo1['largeImage'];

        }

        for ($i = 1; $i <=10; $i++) {
            if($request->has('image' . $i)){
                $image = $request->file('image' . $i);
                $img1 = uploadImage($image,$destinationPath,$thumbMaxWidth,$thumb=true,$large=true);

                $shop->{'image'.$i} = $img1['thumbnail'];
                $shop->{'imageBig' . $i} = $img1['largeImage'];

            }
        }

        if ($shop->save()) {
            session()->flash('success_imageupload', 'The image details were successfully updated');
            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#image_info');
        } else {
            session()->flash('error_imageupload', 'There was an error with your submission, please review the form');
            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#image_info');
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postShopBrands(Request $request) {

        $shop = Shop::find($request->get('shopId'));
        $brands = $request->get('brands');
        if ($shop->brands()->sync($brands)) {
            session()->flash('brand_success', 'The shop brand details were successfully updated');
            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#brand_details');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postShopCards(Request $request) {

        $shop = Shop::find($request->get('shopId'));


        $shop->visa = $request->get('visa');
        $shop->mastercard = $request->get('mastercard');
        $shop->amex = $request->get('amex');
        $shop->diners = $request->get('diners');

        if ($request->has('otherCard')) {
            $otherCards = $request->get('otherCard');
            if (count($otherCards) > 0) {
                DB::table('shopCards')->where('shopMGID','=',$shop->shopMGID)->delete();
                foreach ($otherCards as $key => $value) {
                    DB::table('shopCards')->insert(
                        [
                            'shopMGID' => $shop->shopMGID,
                            'cardID' => $value
                        ]
                    );
                }
            }
        }

        if ($shop->save()) {
            session()->flash('success_card', 'The shop card details were successfully updated');
            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#card_details');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postOwnerDetails(Request $request) {

        $validator = Validator::make($request->all(), [
            'ownerEmail' => 'nullable|email',
            'managerEmail' => 'nullable|email',
            'managerEmail2' => 'nullable|email',
            'managerEmail3' => 'nullable|email',
            'headOfficeEmail' => 'nullable|email',
            'financialEmail' => 'nullable|email',
            'opsManagerEmail' => 'nullable|email',
            'areaManagerEmail' => 'nullable|email',
            'emergencyEmail' => 'nullable|email',
            'emergencyEmail2' => 'nullable|email',
        ]);

        $shop = Shop::find($request->get('shopId'));
        if ($validator->fails()) {

            session()->flash('error_owner', 'There was an error with your submission, please review the form');

            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#owner_info')
                ->withErrors($validator)
                ->withInput();
        }

        $inputData = $request->except('_token');

        if ($shop->update($inputData)) {
            session()->flash('success_owner', 'The owner & manager details were successfully updated');
            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#owner_info');
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postReservationDetails(Request $request) {
        $shop = Shop::find($request->get('shopId'));
        if (count($request->get('reserveWith')) > 0) {
            $reservation = implode($request->get('reserveWith'),',');
            $shop->reserveWith = $reservation;

            if ($shop->save()) {
                session()->flash('success_res', 'The reservation details were successfully updated');
                return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#res_info');
            }
        } else {
            session()->flash('error_res', 'Please make a selection for reservations');
            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#res_info');
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function postDelete($id)
    {
        if (Shop::find($id)->delete()) {
            return 'success';
        }
    }

    /**
     * @param $mallId
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportToExcel($mallId) {

        $shoppingMall = ShoppingMall::find($mallId);
        $shops = Shop::where('mallID','=',$mallId)->orderBy('name')->get();
        if( $shops ){
            $csv_arr[0] = array('shopMGID','shopID','mallID','name','keywords','description','category','product1','product2','product3','product4','product5','product6','product7','product8','product9','product10','shopNumber','landmark','telephone','fax','cell','contactPerson','email','url','image1','image2','image3','image4','image5','newShop','visa','mastercard','amex','diners','otherCard1','otherCard2','otherCard3','otherCard4','tradingHours','map','mapX','mapY','display','subcategory','promoBlock','ownerName','ownerSurname','ownerEmail','ownerCell','managerName','managerSurname','managerEmail','managerCell','managerName2','managerSurname2','managerEmail2','managerCell2','managerName3','managerSurname3','managerEmail3','managerCell3','dateAdded','headOfficeName','headOfficeSurname','headOfficeEmail','headOfficeCell','financialName','financialSurname','financialEmail','financialCell','reserveWith','imageBig1','imageBig2','imageBig3','imageBig4','imageBig5','adults','teens','kids','category2','subcategory2','lastUpdate','image6','image7','image8','image9','image10','imageBig6','imageBig7','imageBig8','imageBig9','imageBig10','ratingDisplay','loyaltyRewardNum','logo','logoBig','altImage1','location2','location3','faceBookURL','suburb','city','province','storeType','physicalAddress','gpsCoords','googleMapX','googleMapY','brand','areaManagerName','areaManagerSurname','areaManagerEmail','areaManagerCell','opsManagerName','opsManagerSurname','opsManagerEmail','opsManagerCell','emergencyName','emergencySurname','emergencyEmail','emergencyCell','emergencyName2','emergencySurname2','emergencyEmail2','emergencyCell2','lorealProfessional','kerastase','redken','mizani','matrix','pureology');

            foreach( $shops as $shop ){
                $csv_arr[$shop->shopMGID] = array(
                    $shop->shopMGID,
                    $shop->shopID,
                    $shop->mallID,
                    $shop->name,
                    $shop->keywords,
                    $shop->description,
                    $shop->category,
                    $shop->subcategory,
                    $shop->product1,
                    $shop->product2,
                    $shop->product3,
                    $shop->product4,
                    $shop->product5,
                    $shop->product6,
                    $shop->product7,
                    $shop->product8,
                    $shop->product9,
                    $shop->product10,
                    $shop->shopNumber,
                    $shop->landmark,
                    $shop->telephone,
                    $shop->fax,
                    $shop->cell,
                    $shop->contactPerson,
                    $shop->email,
                    $shop->url,
                    $shop->image1,
                    $shop->image2,
                    $shop->image3,
                    $shop->image4,
                    $shop->image5,
                    $shop->newShop,
                    $shop->visa,
                    $shop->mastercard,
                    $shop->amex,
                    $shop->diners,
                    $shop->otherCard1,
                    $shop->otherCard2,
                    $shop->otherCard3,
                    $shop->otherCard4,
                    $shop->tradingHours,
                    $shop->map,
                    $shop->mapX,
                    $shop->mapY,
                    $shop->display,
                    $shop->promoBlock,
                    $shop->ownerName,
                    $shop->ownerSurname,
                    $shop->ownerEmail,
                    $shop->ownerCell,
                    $shop->managerName,
                    $shop->managerSurname,
                    $shop->managerEmail,
                    $shop->managerCell,
                    $shop->managerName2,
                    $shop->managerSurname2,
                    $shop->managerEmail2,
                    $shop->managerCell2,
                    $shop->managerName3,
                    $shop->managerSurname3,
                    $shop->managerEmail3,
                    $shop->managerCell3,
                    $shop->dateAdded,
                    $shop->headOfficeName,
                    $shop->headOfficeSurname,
                    $shop->headOfficeEmail,
                    $shop->headOfficeCell,
                    $shop->financialName,
                    $shop->financialSurname,
                    $shop->financialEmail,
                    $shop->financialCell,
                    $shop->reserveWith,
                    $shop->imageBig1,
                    $shop->imageBig2,
                    $shop->imageBig3,
                    $shop->imageBig4,
                    $shop->imageBig5,
                    $shop->adults,
                    $shop->teens,
                    $shop->kids,
                    $shop->category2,
                    $shop->subcategory2,
                    $shop->lastUpdate,
                    $shop->image6,
                    $shop->image7,
                    $shop->image8,
                    $shop->image9,
                    $shop->image10,
                    $shop->imageBig6,
                    $shop->imageBig7,
                    $shop->imageBig8,
                    $shop->imageBig9,
                    $shop->imageBig10,
                    $shop->ratingDisplay,
                    $shop->loyaltyRewardNum,
                    $shop->logo,
                    $shop->logoBig,
                    $shop->altImage1,
                    $shop->location2,
                    $shop->location3,
                    $shop->faceBookURL,
                    $shop->suburb,
                    $shop->city,
                    $shop->province,
                    $shop->storeType,
                    $shop->physicalAddress,
                    $shop->gpsCoords,
                    $shop->googleMapX,
                    $shop->googleMapY,
                    $shop->brand,
                    $shop->areaManagerName,
                    $shop->areaManagerSurname,
                    $shop->areaManagerEmail,
                    $shop->areaManagerCell,
                    $shop->opsManagerName,
                    $shop->opsManagerSurname,
                    $shop->opsManagerEmail,
                    $shop->opsManagerCell,
                    $shop->emergencyName,
                    $shop->emergencySurname,
                    $shop->emergencyEmail,
                    $shop->emergencyCell,
                    $shop->emergencyName2,
                    $shop->emergencySurname2,
                    $shop->emergencyEmail2,
                    $shop->emergencyCell2,
                    $shop->lorealProfessional,
                    $shop->kerastase,
                    $shop->redken,
                    $shop->mizani,
                    $shop->matrix,
                    $shop->pureology
                );

            }

            $export = new ShopsExport($csv_arr);
            return \Maatwebsite\Excel\Facades\Excel::download($export, $shoppingMall->name.'_shops.xlsx');

        }
    }

    /**]
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getGenericShops() {
        $data['genericShops'] = MGGeneric::orderBy('name')->pluck('name','shopGenericID');
        return response($data)->header('Content-Type', 'application/json');

    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getGenericShopNames() {
        $data['genericShopNames'] = MGGeneric::pluck('name');
        return response($data)->header('Content-Type', 'application/json');
    }

    public function editGenericShop($id) {

        $data['generic'] = MGGeneric::find($id);
        $data['categories'] = MGGeneric::distinct('category')->pluck('category','category');
        return view('admin.shops.edit_generic')->with($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateGenericShop(Request $request, $id) {
        $generic = MGGeneric::find($id);
        $generic->name = $request->get('name');
        $generic->category = $request->get('category');
        $generic->url = $request->get('url');
        $generic->tradingHours = $request->get('tradingHours');
        $generic->description = $request->get('description');
        $generic->keywords = $request->get('keywords');

        if ($generic->save()) {
            session()->flash('success', 'The generic shop details were successfully updated');
            return redirect('/admin/shops/edit-generic/' . $generic->shopGenericID);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateGenericShopProducts(Request $request) {
        $generic = MGGeneric::find($request->get('shopGenericID'));
        for ($i = 1; $i <= 10; $i++) {
            $generic->{'product'.$i} = $request->get('product' . $i);
        }

        if ($generic->save()) {
            session()->flash('success', 'The product details were successfully updated');
            return redirect('/admin/shops/edit-generic/' . $generic->shopGenericID);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateGenericShopImages(Request $request) {

        $request->validate([
            'logo' => 'mimes:jpeg,png,gif|image',
            'image1' => 'mimes:jpeg,png,gif|image',
            'image2' => 'mimes:jpeg,png,gif|image',
            'image3' => 'mimes:jpeg,png,gif|image',
            'image4' => 'mimes:jpeg,png,gif|image',
            'image5' => 'mimes:jpeg,png,gif|image',
            'image6' => 'mimes:jpeg,png,gif|image',
            'image7' => 'mimes:jpeg,png,gif|image',
            'image8' => 'mimes:jpeg,png,gif|image',
            'image9' => 'mimes:jpeg,png,gif|image',
            'image10' => 'mimes:jpeg,png,gif|image'
        ]);

        $generic = MGGeneric::find($request->get('shopGenericID'));
        $destinationPath = base_path('uploadimages/generic');

        $thumbMaxWidth = 150;

        if($request->has('logo')){
            $logo = $request->file('logo');
            $logo1 = uploadImage($logo,$destinationPath,$thumbMaxWidth,$thumb=true,$large=true);

            $generic->logo = $logo1['thumbnail'];
            $generic->logoBig = $logo1['largeImage'];

        }

        for ($i = 1; $i <=10; $i++) {
            if($request->has('image' . $i)){
                $image = $request->file('image' . $i);
                $img1 = uploadImage($image,$destinationPath,$thumbMaxWidth,$thumb=true,$large=true);

                $generic->{'image'.$i} = $img1['thumbnail'];
                $generic->{'imageBig' . $i} = $img1['largeImage'];

            }
        }

        if ($generic->save()) {
            session()->flash('success', 'The image details were successfully updated');
            return redirect('/admin/shops/edit-generic/' . $generic->shopGenericID);
        } else {
            session()->flash('error', 'There was an error with your submission, please review the form');
            return redirect('/admin/shops/edit-generic/' . $generic->shopGenericID);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateGenericShopCards(Request $request) {

        $generic = MGGeneric::find($request->get('shopGenericID'));
        $generic->visa = $request->has('visa') ? $request->get('visa') : '';
        $generic->mastercard = $request->has('mastercard') ? $request->get('mastercard') : '';
        $generic->amex = $request->has('amex') ? $request->get('amex') : '';
        $generic->diners = $request->has('diners') ? $request->get('diners') : '';

        if ($generic->save()) {
            session()->flash('success', 'The shop card details were successfully updated');
            return redirect('/admin/shops/edit-generic/' . $generic->shopGenericID);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function writeToGeneric($id) {

        $shop = Shop::find($id);

        $genericData['name'] = $shop->name;
        $genericData['keywords'] = $shop->keywords;
        $genericData['description'] = $shop->description;
        $genericData['category'] = $shop->category;
        $genericData['product1'] = $shop->product1;
        $genericData['product2'] = $shop->product2;
        $genericData['product3'] = $shop->product3;
        $genericData['product4'] = $shop->product4;
        $genericData['product5'] = $shop->product5;
        $genericData['product6'] = $shop->product6;
        $genericData['product7'] = $shop->product7;
        $genericData['product8'] = $shop->product8;
        $genericData['product9'] = $shop->product9;
        $genericData['product10'] = $shop->product10;
        $genericData['url'] = $shop->url;
        $genericData['tradingHours'] = $shop->tradingHours;
        $genericData['subcategory'] = $shop->subcategory;
        $genericData['visa'] = $shop->visa;
        $genericData['mastercard'] = $shop->mastercard;
        $genericData['amex'] = $shop->amex;
        $genericData['diners'] = $shop->diners;

        $destinationPath = base_path('uploadimages/generic');

        if( !empty($shop->logo) ){
            if (!File::exists($destinationPath.'/'.$shop->logo)) {
                File::copy(base_path('uploadimages/mall_'.$shop->mallID.'/'.$shop->logo),base_path('uploadimages/generic/'.$shop->logo));
            }
            $genericData['logo'] = $shop->logo;
            $genericData['logoBig'] = $shop->logo;
        }  else {
            $genericData['logo'] = '';
            $genericData['logoBig'] = '';
        }

        if( !empty($shop->image1) ){
            if (!File::exists($destinationPath.'/'.$shop->image1)) {
                File::copy(base_path('uploadimages/mall_'.$shop->mallID.'/'.$shop->image1),base_path('uploadimages/generic/'.$shop->image1));
            }
            $genericData['image1'] = $shop->image1;
            $genericData['imageBig1'] = $shop->image1;
        } else {
            $genericData['image1'] = '';
            $genericData['imageBig1'] = '';
        }

        if( !empty($shop->image2) ){
            if (!File::exists($destinationPath.'/'.$shop->image2)) {
                File::copy(base_path('uploadimages/mall_'.$shop->mallID.'/'.$shop->image2),base_path('uploadimages/generic/'.$shop->image2));
            }
            $genericData['image2'] = $shop->image2;
            $genericData['imageBig2'] = $shop->image2;
        } else {
            $genericData['image2'] = '';
            $genericData['imageBig2'] = '';
        }

        if( !empty($shop->image3) ){
            if (!File::exists($destinationPath.'/'.$shop->image3)) {
                File::copy(base_path('uploadimages/mall_'.$shop->mallID.'/'.$shop->image3),base_path('uploadimages/generic/'.$shop->image3));
            }
            $genericData['image3'] = $shop->image3;
            $genericData['imageBig3'] = $shop->image3;
        }  else {
            $genericData['image3'] = '';
            $genericData['imageBig3'] = '';
        }

        if( !empty($shop->image4) ){
            if (!File::exists($destinationPath.'/'.$shop->image4)) {
                File::copy(base_path('uploadimages/mall_'.$shop->mallID.'/'.$shop->image4),base_path('uploadimages/generic/'.$shop->image4));
            }
            $genericData['image4'] = $shop->image4;
            $genericData['imageBig4'] = $shop->image4;
        }  else {
            $genericData['image4'] = '';
            $genericData['imageBig4'] = '';
        }

        if( !empty($shop->image5) ){
            if (!File::exists($destinationPath.'/'.$shop->image5)) {
                File::copy(base_path('uploadimages/mall_'.$shop->mallID.'/'.$shop->image5),base_path('uploadimages/generic/'.$shop->image5));
            }
            $genericData['image5'] = $shop->image5;
            $genericData['imageBig5'] = $shop->image5;
        }  else {
            $genericData['image5'] = '';
            $genericData['imageBig5'] = '';
        }

        if( !empty($shop->image6) ){
            if (!File::exists($destinationPath.'/'.$shop->image6)) {
                File::copy(base_path('uploadimages/mall_'.$shop->mallID.'/'.$shop->image6),base_path('uploadimages/generic/'.$shop->image6));
            }
            $genericData['image6'] = $shop->image6;
            $genericData['imageBig6'] = $shop->image6;
        }  else {
            $genericData['image6'] = '';
            $genericData['imageBig6'] = '';
        }

        if( !empty($shop->image7) ){
            if (!File::exists($destinationPath.'/'.$shop->image7)) {
                File::copy(base_path('uploadimages/mall_'.$shop->mallID.'/'.$shop->image7),base_path('uploadimages/generic/'.$shop->image7));
            }
            $genericData['image7'] = $shop->image7;
            $genericData['imageBig7'] = $shop->image7;
        }  else {
            $genericData['image7'] = '';
            $genericData['imageBig7'] = '';
        }

        if( !empty($shop->image8) ){
            if (!File::exists($destinationPath.'/'.$shop->image8)) {
                File::copy(base_path('uploadimages/mall_'.$shop->mallID.'/'.$shop->image8),base_path('uploadimages/generic/'.$shop->image8));
            }
            $genericData['image8'] = $shop->image8;
            $genericData['imageBig8'] = $shop->image8;
        }  else {
            $genericData['image8'] = '';
            $genericData['imageBig8'] = '';
        }

        if( !empty($shop->image9) ){
            if (!File::exists($destinationPath.'/'.$shop->image9)) {
                File::copy(base_path('uploadimages/mall_'.$shop->mallID.'/'.$shop->image9),base_path('uploadimages/generic/'.$shop->image9));
            }
            $genericData['image9'] = $shop->image9;
            $genericData['imageBig9'] = $shop->image9;
        }  else {
            $genericData['image9'] = '';
            $genericData['imageBig9'] = '';
        }

        if( !empty($shop->image10) ){
            if (!File::exists($destinationPath.'/'.$shop->image10)) {
                File::copy(base_path('uploadimages/mall_'.$shop->mallID.'/'.$shop->image10),base_path('uploadimages/generic/'.$shop->image10));
            }
            $genericData['image10'] = $shop->image10;
            $genericData['imageBig10'] = $shop->image10;
        }  else {
            $genericData['image10'] = '';
            $genericData['imageBig10'] = '';
        }

        MGGeneric::insert($genericData);
        session()->flash('success', 'The shop card details were successfully updated');
        return redirect()->back();
    }

    /**
     * @param $shopId
     * @param $genericId
     * @return \Illuminate\Http\RedirectResponse
     */
    public function copyFromGeneric($shopId, $genericId) {
        $genericShop = MGGeneric::find($genericId);
        $shop = Shop::find($shopId);

        $updateData = [
            'category' => $genericShop->category,
            'subcategory' => $genericShop->subcategory,
            'url' => $genericShop->url,
            'description' => $genericShop->description,
            'keywords' => $genericShop->keywords,
            'tradingHours' => $genericShop->tradingHours,
            'product1' => $genericShop->product1,
            'product2' => $genericShop->product2,
            'product3' => $genericShop->product3,
            'product4' => $genericShop->product4,
            'product5' => $genericShop->product5,
            'product6' => $genericShop->product6,
            'product7' => $genericShop->product7,
            'product8' => $genericShop->product8,
            'product9' => $genericShop->product9,
            'product10' => $genericShop->product10,
            'visa' => $genericShop->visa,
            'mastercard' => $genericShop->mastercard,
            'amex' => $genericShop->amex,
            'diners' => $genericShop->diners,
        ];

        $destinationPath = base_path('/uploadimages/mall_'.$shop->mallID);
        $imgArray = [];

        for($i=2;$i<=30;$i++){
            if($genericShop["image$i"]){
                array_push( $imgArray,$genericShop["image$i"]);
            }
        }

        $newImageArray = [];
        $updateData["image2"] ='';
        $updateData["image3"] ='';
        $updateData["image4"] ='';
        $updateData["image5"] ='';
        $updateData["image6"] ='';
        $updateData["image7"] ='';
        $updateData["image8"] ='';
        $updateData["image9"] ='';
        $updateData["image10"] ='';

        shuffle($imgArray);

        foreach( $imgArray as $imgarr){
            array_push($newImageArray,$imgarr);
        }

        if( !empty($genericShop->logo) ){

            if (File::exists(base_path('/uploadimages/generic/'.$genericShop->logo))) {
                File::copy(base_path('/uploadimages/generic/'.$genericShop->logo),$destinationPath.'/'.$genericShop->logo);
            }
            $updateData["logo"] = $genericShop->logo;
            $updateData["logoBig"] = $genericShop->logoBig;
        }

        if( !empty($newImageArray[0]) )
        {
            if (File::exists(base_path('/uploadimages/generic/'.$newImageArray[0]))) {
                File::copy(base_path('/uploadimages/generic/'.$newImageArray[0]),$destinationPath.'/'.$newImageArray[0]);
            }
            $updateData["image2"] = $newImageArray[0];
            $updateData["imageBig2"] = $newImageArray[0];
        }

        if( !empty($newImageArray[1]) )
        {
            if (File::exists(base_path('/uploadimages/generic/'.$newImageArray[1]))) {
                File::copy(base_path('/uploadimages/generic/'.$newImageArray[1]),$destinationPath.'/'.$newImageArray[1]);
            }
            $updateData["image3"] = $newImageArray[1];
            $updateData["imageBig3"] = $newImageArray[1];
        }

        if( !empty($newImageArray[2]) )
        {
            if (File::exists(base_path('/uploadimages/generic/'.$newImageArray[2]))) {
                File::copy(base_path('/uploadimages/generic/'.$newImageArray[2]),$destinationPath.'/'.$newImageArray[2]);
            }
            $updateData["image4"] = $newImageArray[2];
            $updateData["imageBig4"] = $newImageArray[2];
        }

        if( !empty($newImageArray[3]) )
        {
            if (File::exists(base_path('/uploadimages/generic/'.$newImageArray[3]))) {
                File::copy(base_path('/uploadimages/generic/'.$newImageArray[3]),$destinationPath.'/'.$newImageArray[3]);
            }
            $updateData["image5"] = $newImageArray[3];
            $updateData["imageBig5"] = $newImageArray[3];
        }

        if( isset($newImageArray[4]) )
        {
            if (File::exists(base_path('/uploadimages/generic/'.$newImageArray[4]))) {
                File::copy(base_path('/uploadimages/generic/'.$newImageArray[4]),$destinationPath.'/'.$newImageArray[4]);
            }
            $updateData["image6"] = $newImageArray[4];
            $updateData["imageBig6"] = $newImageArray[4];
        }

        if( isset($newImageArray[5]) )
        {
            if (File::exists(base_path('/uploadimages/generic/'.$newImageArray[5]))) {
                File::copy(base_path('/uploadimages/generic/'.$newImageArray[5]),$destinationPath.'/'.$newImageArray[5]);
            }
            $updateData["image7"] = $newImageArray[5];
            $updateData["imageBig7"] = $newImageArray[5];
        }

        if( isset($newImageArray[6]) )
        {
            if (File::exists(base_path('/uploadimages/generic/'.$newImageArray[6]))) {
                File::copy(base_path('/uploadimages/generic/'.$newImageArray[6]),$destinationPath.'/'.$newImageArray[6]);
            }
            $updateData["image8"] = $newImageArray[6];
            $updateData["imageBig8"] = $newImageArray[6];
        }

        if( isset($newImageArray[7]) )
        {
            if (File::exists(base_path('/uploadimages/generic/'.$newImageArray[7]))) {
                File::copy(base_path('/uploadimages/generic/'.$newImageArray[7]),$destinationPath.'/'.$newImageArray[7]);
            }
            $updateData["image9"] = $newImageArray[7];
            $updateData["imageBig9"] = $newImageArray[7];
        }

        if( isset($newImageArray[8]) )
        {
            if (File::exists(base_path('/uploadimages/generic/'.$newImageArray[8]))) {
                File::copy(base_path('/uploadimages/generic/'.$newImageArray[8]),$destinationPath.'/'.$newImageArray[8]);
            }
            $updateData["image10"] = $newImageArray[8];
            $updateData["imageBig10"] = $newImageArray[8];
        }

        $shop->update($updateData);

        session()->flash('success', 'The shop card details were successfully updated');
        return redirect()->back();
    }

    /**
     * @param $id
     * @param $image
     */
    public function postRemoveImage( $id,$image ){


        $shop = Shop::where('shopMGID','=',$id)->get()->first();
        if($shop) {
            $destinationPath = base_path('uploadimages/mall_' . $shop->mallID);

            //get numeric image number
            $imageID = str_ireplace('image', '', $image);
            $imageID = str_ireplace('big', '', $imageID);

            if ($image) {
                removeOldImage($destinationPath, 'image' . $imageID);
                removeOldImage($destinationPath, 'imageBig' . $imageID);
            }

            $shop->update(array('image'.$imageID => '', 'imageBig'.$imageID => ''));
        }
        exit;

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function uploadFile(Request $request) {

        $validator = Validator::make($request->all(), [
            'fileUpload' => 'required'
        ]);

        $shop = Shop::find($request->get('shopMGID'));
        if ($validator->fails()) {

            session()->flash('error_downloads', 'You did not select a file.');

            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#shop_downloads')
                ->withErrors($validator)
                ->withInput();
        }

        $image = $request->file('fileUpload');
        $file = $image;
        $dr = base_path('download/mall_' . $shop->mallID);

        $file_name_original = $file->getClientOriginalName();
        $file_name_cleaned = str_replace("'", "", $file_name_original);
        $file_name_cleaned = str_replace("/", "_", $file_name_cleaned);
        $file_name_cleaned = str_replace(" ", "_", $file_name_cleaned);

        $new_filename = Carbon::now()->timestamp . '_' .$file_name_cleaned ;
        $file->move($dr, $new_filename);

        $uploadFile = new Shopfile();
        $uploadFile->name = $request->get('upload_name');
        $uploadFile->filename = $new_filename;
        $uploadFile->shop_id = $request->get('shopMGID');
        $uploadFile->filetype = $file->getClientOriginalExtension();
        $uploadFile->filesize = $file->getSize();

        if ($uploadFile->save()) {
            session()->flash('success_downloads', 'The file has been uploaded successfully.');
            return redirect('/admin/shops/edit/' . $shop->shopMGID.'/#shop_downloads');
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function deleteFileUpload($id) {

        $shopFile = Shopfile::find($id);

        if ($shopFile->delete()) {
            return 'success';
        }
    }


}
