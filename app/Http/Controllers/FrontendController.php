<?php

namespace App\Http\Controllers;

use App\BookingSchedule;
use App\City;
use App\Competition;
use App\Content;
use App\Country;
use App\FormRequest;
use App\Job;
use App\MallVisit;
use App\MovieMall;
use App\ShoppingMall;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class FrontendController extends Controller
{
    protected $country;

    /**
     * Create a new controller instance.
     * @param $country
     * @return void
     */
    public function __construct(Country $country)
    {
        $this->country = $country;
        $country = \session()->get('country', 'ZA');
        $country_id = \session()->get('country_id', 190);

        if(!\session()->has('default_country_code'))
        {
            \session()->put('default_country_code', $country);
            \session()->put('default_country_id', $country_id);
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['seo_title'] = '';
        $data['seo_description'] = '';
        $country_id = session()->get('default_country_id');
        if($country_id != 190)
        {
            return redirect('/malls');
        } else {
            $expiresAt = Carbon::now()->addDay(1);
            $data['newsArticles'] = Content::where('activate','=','Y')->orderBy('contentDate','desc')->take(2)->get();


            $data['jobs'] = Cache::remember('jobs_'.$country_id, $expiresAt, function()
            {
                return Job::whereHas('mall', function($q){
                    $q->where('country_id', session()->get('default_country_id'));
                })
                    ->where('endDate','>=',Carbon::now()->toDateTimeString())
                    ->orderBy('dateAdded','desc')
                    ->take(5)
                    ->get();
            });

            $data['movieMalls'] = Cache::remember('movieMalls_'.$country_id, $expiresAt, function() use($country_id)
            {
                return MovieMall::has('whatsShowing')->where('country_id','=',$country_id)->orderBy('name', 'ASC')->display()->pluck('name','mallID');
            });


            $data['leadingMalls'] = Cache::remember('top5malls', $expiresAt, function()
            {
                return MallVisit::with('mall')
                    ->orderBy('visits','desc')
                    ->take(5)
                    ->get();
            });

            $now = Carbon::now()->toDateTimeString();

            //$bookingSchedule = BookingSchedule::where('startDate','<=',$now)->where('endDate','>',$now)->orderBy('startDate','asc');

            $featuredMall = BookingSchedule::where('startDate','<=',$now)->where('endDate','>=',$now)->orderBy('startDate','asc')->where('bookingWhat','=','Mall')->first();
            $featuredComp = BookingSchedule::where('startDate','<=',$now)->where('endDate','>=',$now)->orderBy('startDate','asc')->where('bookingWhat','=','Competition')->first();
            $featuredEvent = BookingSchedule::where('startDate','<=',$now)->where('endDate','>=',$now)->orderBy('startDate','asc')->where('bookingWhat','=','Event')->first();
            $featuredPromotion = BookingSchedule::where('startDate','<=',$now)->where('endDate','>=',$now)->orderBy('startDate','asc')->where('bookingWhat','=','Promotion')->first();
            $featuredShop = BookingSchedule::where('startDate','<=',$now)->where('endDate','>=',$now)->orderBy('startDate','asc')->where('bookingWhat','=','Shop')->first();
            $data['featuredMall'] = null;
            $data['featuredComp'] = null;
            $data['featuredEvent'] = null;
            $data['featuredPromotion'] = null;
            $data['featuredShop'] = null;

            if( $featuredMall != null ){
                $data['featuredMall'] = $featuredMall->mall;
            }

            if( $featuredComp != null ){
                $data['featuredComp'] = $featuredComp->competition;
            }

            if( $featuredEvent != null){
                $data['featuredEvent'] = $featuredEvent->event;
            }

            if( $featuredPromotion != null )
            {
                $data['featuredPromotion'] = $featuredPromotion->promotion;
            }
            if( $featuredShop != null ){
                $data['featuredShop'] = $featuredShop->shop;
            }

            $data['featuredMovies'] = array();
            return view('landing')->with($data);
        }

    }

    /**
     *
     */
    public function countryDropdown()
    {
        $response = callApi('countries');

        $decoded = json_decode($response);

        $countries = [];

        foreach ($decoded as $key => $value) {
            $countries[] = [
                'id' => $key,
                'name' => $value
            ];

        }

        return response($countries)->header('Content-Type', 'application/json');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function changeCountry(Request $request, $id)
    {
        $selectCountry = Country::find($id);

        if ($selectCountry) {
            session()->put('country_iso', strtolower($selectCountry->iso));
            session()->put('country_id', $selectCountry->id);
            session()->put('default_country_code', $selectCountry->id);
        }

        if ($request->server('HTTP_REFERER')) {
            return back();
        }

        return redirect('/');
    }

    /**
     * @return \Psr\Http\Message\StreamInterface
     */
    public function getCountryProvinces($countryId = null)
    {

        $countryId = isset($countryId) ? $countryId : session()->get('country_id', 190);

        $provinces = callApi('provinces/' . $countryId);

        return response($provinces)->header('Content-Type', 'application/json');
    }

    public function getProvinceCities($provinceId){
        $cities = City::where('province_id', $provinceId)->orderBy('name')->get(['name', 'id']);

        return response($cities);
    }

    public function getProvinceMallDropdown($provinceId = null)
    {
        return callApi('malls-dropdown-list/' . $provinceId);
    }

    public function getAutocompleteMalls()
    {
        $malls = callApi('malls-dropdown-list');

        return response($malls)->header('Content-Type', 'application/json');
    }

    /**
     *
     */
    public function getProvinceMalls($id)
    {
        $malls = callApi('malls/province/' . $id);

        return response($malls)->header('Content-Type', 'application/json');
    }

    public function getProvinceEvents($id)
    {
        $events = callApi('events/province/' . $id);

        return response($events)->header('Content-Type', 'application/json');
    }

    public function getExhibitionsEvents($id)
    {
        $exhibitions = callApi('exhibitions/province/' . $id);

        return response($exhibitions)->header('Content-Type', 'application/json');
    }

    public function getMallExhibitions($mallId)
    {
        $exhibitions = callApi('exhibitions/mall/' . $mallId);

        return response($exhibitions)->header('Content-Type', 'application/json');
    }

    public function getMallEvents($mallID)
    {
        $events = callApi('events/mall/' . $mallID);
        return response($events)->header('Content-Type', 'application/json');
    }

    public function getProvinceMaps($provinceId)
    {
        $maps = callApi('maps/province/' . $provinceId);
        return response($maps)->header('Content-Type', 'application/json');
    }

    public function getMallMaps($mallID)
    {
        $maps = callApi('maps/mall/' . $mallID);
        return response($maps)->header('Content-Type', 'application/json');
    }

    public function getProvinceJobs($id)
    {
        $jobs = callApi('jobs/province/' . $id);

        return response($jobs)->header('Content-Type', 'application/json');
    }

    public function getMallJobs($mallId)
    {
        $jobs = callApi('jobs/mall/' . $mallId);

        return response($jobs)->header('Content-Type', 'application/json');
    }

    public function getShopJobs($shopId)
    {
        $jobs = callApi('jobs/shop/' . $shopId);

        return response($jobs)->header('Content-Type', 'application/json');
    }

    public function getMallCompetitions($mallId)
    {
        $formRequests = FormRequest::where('mallID', $mallId)->get();

        $formIds = [];
        foreach ($formRequests as $formRequest) {
            array_push($formIds, $formRequest->formID);
        }


        $forms = Competition::orderBy('endDate', 'desc')->whereIn('formID', $formIds)->get();
        $returnData = [];
        $returnData['mall'] = ShoppingMall::find($mallId)->name;
        foreach ($forms as $form) {
            $returnData['competitions'][] = [
                'id' => $form->formID,
                'subject' => $form->subject,
                'recipient' => $form->recipient,
                'start_date' => $form->startDate,
                'end_date' => $form->endDate,
                'responses' => count($form->formFeedback),
                'linked_to_facebook' => ($form->facebook === 'Y') ? 'Yes' : 'No',
                'current_comp' => ($form->endDate > Carbon::now()) ? 'Yes' : 'No',
                'displayed' => $form->activated
            ];
        }
        return response()->json($returnData);
    }

    public function getPromotionMalls($provinceId)
    {
        $malls = callApi('malls-with-promotions/' . $provinceId);
        return response($malls)->header('Content-Type', 'application/json');
    }
}
