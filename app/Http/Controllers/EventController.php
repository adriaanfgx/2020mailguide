<?php

namespace App\Http\Controllers;

use App\MGEvent;
use App\ShoppingMall;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel.auth');
    }

    /**
     * @param $mallID
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate($mallID)
    {
        $data['mall'] = ShoppingMall::find($mallID);
        $data['categories'] = MGEvent::distinct('category')->where('category', '<>', '')->orderBy('category')->pluck('category', 'category');
        $data['locations'] = MGEvent::distinct('location')->where('mallID', '=', $mallID)->pluck('location', 'location');

        return view('admin.events.create')->with($data);
    }

    /**
     * @param Request $request
     * @param $mallID
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCreate(Request $request, $mallID)
    {

        $request->validate([
            'name' => 'required',
            'startDate' => 'required|date',
            'endDate' => 'required|date',
            'thumbnail1' => 'mimes:jpeg,png,gif|image',
            'altImage1' => 'mimes:jpeg,png,gif|image',
            'altImage2' => 'mimes:jpeg,png,gif|image',
            'altImage3' => 'mimes:jpeg,png,gif|image',
            'fileName' => 'sometimes|mimes:pdf,doc',
        ]);

        $eventData = [
            'name' => $request->get('name'),
            'startDate' => $request->get('startDate'),
            'endDate' => $request->get('endDate'),
            'featured' => 'N',
            'fileSize' => '',
            'mallID' => $mallID,
            'event' => $request->get('event'),
            'display' => $request->display
        ];
        if ($request->get('category_text') !== null) {
            $eventData['category'] = $request->get('category_text');
        } else {
            unset($eventData['category_text']);
        }

        unset($eventData['_token']);

        $destinationPath = base_path('uploadimages/mall_' . $mallID);
        //Upload File
        if ($request->has('fileName')) {
            $eventData['fileName'] = uploadImage($request->file('fileName'), $destinationPath)['largeImage'];
        } else {
            $eventData['fileName'] = '';
        }

        $thumbMaxWidth = 260;

        $thumbnail = $request->file('thumbnail1');

        if ($request->has('thumbnail1')) {
            $image1 = uploadImage($thumbnail, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
            $eventData['thumbnail1'] = $image1['thumbnail'];
            $eventData['image1'] = $image1['largeImage'];
        }

        for ($i = 1; $i <= 3; $i++) {
            $altImage = $request->file('altImage' . $i);
            if ($request->has('altImage' . $i)) {
                $alt = uploadImage($altImage, $destinationPath);
                $eventData['altImage' . $i] = $alt['unresized'];
            }
        }

        MGEvent::create($eventData);
        return redirect('/admin/events/' . $mallID);
    }

    /**
     * @param $mallID
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEventsByMall($mallID)
    {

        $data['mallId'] = $mallID;
        $data['mallName'] = ShoppingMall::find($mallID)->name;
        return view('admin.events.list')->with($data);
    }

    public function getMallBackendEvents($mallID) {
        $selectedMall = ShoppingMall::find($mallID);
        $returnData = [];
        $returnData['mall_name'] = $selectedMall->name;
        $returnData['mall_id'] = $selectedMall->mallID;
        foreach ($selectedMall->events as $value) {
            $returnData['events'][] = [
                'id' => $value->eventsMGID,
                'name' => $value->name,
                'start_date' => $value->startDate,
                'end_date' => $value->endDate,
                'category' => $value->category,
                'venue' => $value->location,
                'display' => ($value->display === 'Y') ? 'Yes' : 'No',
            ];
        }


        return response()->json($returnData);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {

        $event = MGEvent::find($id);
        $data['event'] = $event;
        $data['mall'] = ShoppingMall::find($event->mallID);
        $data['categories'] = MGEvent::distinct('category')->where('category', '<>', '')->orderBy('category')->pluck('category', 'category');
        $data['locations'] = MGEvent::distinct('location')->where('mallID', '=', $event->mallID)->pluck('location', 'location');

        return view('admin.events.create')->with($data);
    }

    public function postUpdate(Request $request, $id)
    {
        $rules = [
            'name' => 'required',
            'startDate' => 'required|date',
            'endDate' => 'required|date',
            'thumbnail1' => 'mimes:jpeg,png,gif|image',
            'altImage1' => 'mimes:jpeg,png,gif|image',
            'altImage2' => 'mimes:jpeg,png,gif|image',
            'altImage3' => 'mimes:jpeg,png,gif|image',
            'fileName' => 'sometimes|mimes:pdf,doc',
        ];

        $form = $request->except('_token');

        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {
            $event = MGEvent::find($id);
            $event->name = $request->get('name');
            $event->startDate = $request->get('startDate');
            $event->endDate = $request->get('endDate');
            $event->event = $request->get('event');
            $event->category = $request->get('category');
            $event->location = $request->get('location');
            $event->display = $request->get('display');

            if ($request->get('category_text') !== null) {
                $event->category = $request->get('category_text');
            }

            $destinationPath = base_path('uploadimages/mall_' . $event->mallID);
            //Upload File
            if ($request->has('fileName')) {
                $event->fileName = uploadImage($request->file('fileName'), $destinationPath)['largeImage'];
            }

            $thumbMaxWidth = 260;

            $thumbnail = $request->file('thumbnail1');

            if ($request->has('thumbnail1')) {
                $image1 = uploadImage($thumbnail, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                $event->thumbnail1 = $image1['thumbnail'];
                $event->image1 = $image1['largeImage'];
            }

            $altImage1 = $request->file('altImage1');
            $altImage2 = $request->file('altImage2');
            $altImage3 = $request->file('altImage3');
            if ($request->has('altImage1')) {
                $alt1 = uploadImage($altImage1, $destinationPath);
                $event->altImage1 = $alt1['unresized'];
            }

            if ($request->has('altImage2')) {
                $alt2 = uploadImage($altImage2, $destinationPath);
                $event->altImage2 = $alt2['unresized'];
            }

            if ($request->has('altImage3')) {
                $alt3 = uploadImage($altImage3, $destinationPath);
                $event->altImage3 = $alt3['unresized'];
            }

            if ($event->save()) {
                Session::flash('success', 'Event details have been successfully updated');
            } else {
                Session::flash('error', 'An error occurred while updating event details');
                return back()->withErrors($validation);
            }
        } else {
            Session::flash('error', 'There are some errors, please review the form');
            return back()->withErrors($validation);
        }

        return redirect('/admin/events/' . $event->mallID);
    }

    /**
     * @param $id
     * @return string
     */
    public function postDelete($id)
    {
        if (MGEvent::find($id)->delete()) {
            return 'success';
        }
    }
}
