<?php

namespace App\Http\Controllers;

use App\FormFieldSpec;
use App\GiftIdea;
use App\Job;
use App\JobApplicant;
use App\MGCrud;
use App\Promotion;
use App\Province;
use App\SEO;
use App\Shop;
use App\ShoppingMall;
use App\Suburb;
use App\User;
use App\UserChain;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class ShopManagerController extends Controller
{
    public function __construct()
    {
        $data = [];

        $uri = request()->path();
        $data['seo_title'] = 'Mallguide ';
        $data['seo_description'] = '';
        $data['seo_keywords'] = '';

        $seo = SEO::where('route', '=', $uri)->first();

        if ($seo) {
            $data['seo_title'] = $seo->title;
            $data['seo_description'] = $seo->description;
            $data['seo_keywords'] = $seo->keywords;
        }
        View::share($data);
    }


    /**
     * Get index page for store manager
     */
    public function getStoreIndex()
    {

        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();

            if ($user->hasAccess('user')) {

                $usr = User::find($user->id);

                $userShops = $usr->shops;

                $shopIDs = array();

                $data['userShops'] = $userShops;

                foreach ($userShops as $shop) {
                    array_push($shopIDs, $shop->shopMGID);
                }
//
//                $shopID = $user->shopMGID;
//                $mallID = $user->mallID;
//                dd($shopIDs[0]);
                $data['user'] = $user;
                if (sizeof($shopIDs) > 1) {

                    $data['shop'] = Shop::whereIn('shopMGID', $shopIDs)->get();
                    $data['promotions'] = Promotion::whereIn('shopMGID', $shopIDs)->where('endDate', '>=', date('Y-m-d H:i:s'))->count();
                    $data['jobs'] = Job::whereIn('shopMGID', $shopIDs)->where('endDate', '>=', date('Y-m-d H:i:s'))->count();

                } else {

                    $data['shop'] = Shop::find($shopIDs[0]);
                    $data['mall'] = ShoppingMall::find($data['shop']->mallID);
                    $data['promotions'] = Promotion::where('shopMGID', '=', $shopIDs[0])->where('endDate', '>=', date('Y-m-d H:i:s'))->count();
                    $data['jobs'] = Job::where('shopMGID', '=', $shopIDs[0])->where('endDate', '>=', date('Y-m-d H:i:s'))->count();
                }


                return view('shopmanagers/shopuser/index', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * List stores for shop manager with multiple stores
     */
    public function getListUserStores()
    {

        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();

            if ($user->hasAccess('user')) {

                $usr = User::find($user->id);

                $userShops = $usr->shops;


                $data['userShops'] = $userShops;

                return view('shopmanagers/shopuser/liststores', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Update Mall Store
     */
    public function getUpdateMallStore($shopID = null)
    {

        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();

            if ($user->hasAccess('user')) {

                if (session()->has('activeShopID') && $shopID == NULL) {
                    $shopID = session()->get('activeShopID');
                }

                $data['user'] = $user;
                $data['shop'] = Shop::find($shopID);

                $data['categories'] = Shop::distinct('category')->orderBy('category')->pluck('category', 'category');
                $data['subcategories'] = Shop::distinct('subcategory')->orderBy('subcategory')->pluck('subcategory', 'subcategory');

                return view('shopmanagers/shopuser/updateshop', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * List Store Gift Ideas
     */
    public function getStoreGiftIdeas()
    {

        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();

            if ($user->hasAccess('user') || $user->hasAccess('manager')) {

                $activeStore = session()->get('activeShopID');
                $activeMallID = session()->get('activeMallID');
                $activeChainName = session()->get('activeChain');

                if (isset($activeMallID)) {

                    $shoppingMall = ShoppingMall::find($activeMallID);
                    $mallShops = $shoppingMall->shops->pluck('shopMGID');

                    $data['giftideas'] = GiftIdea::whereIn('giftIdeas.shopMGID', $mallShops)->join('shop', 'shop.shopMGID', '=', 'giftIdeas.shopMGID')->select('giftIdeas.*', 'shop.name')->orderBy('dateAdded', 'desc')->get();

                } elseif (isset($activeStore)) {

                    $data['giftideas'] = GiftIdea::where('giftIdeas.shopMGID', '=', $activeStore)->join('shop', 'shop.shopMGID', '=', 'giftIdeas.shopMGID')->select('giftIdeas.*', 'shop.name')->orderBy('title')->get();
                } elseif (isset($activeChainName)) {

                    $shopIds = Shop::where('name', '=', $activeChainName)->pluck('shopMGID');
                    $data['giftideas'] = GiftIdea::whereIn('giftIdeas.shopMGID', $shopIds)->join('shop', 'shop.shopMGID', '=', 'giftIdeas.shopMGID')->select('giftIdeas.*', 'shop.name')->orderBy('title')->get();
                }

                return view('shopmanagers.shopuser.giftideas.index', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * List chain store gift ideas
     */
    public function GetChainstoreGiftIdeas()
    {

        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $userChain = UserChain::where('user_id', '=', $user->id)->first()->chain_name;
                $shops = Shop::where('name', '=', $userChain)->pluck('shopMGID');
                $data['giftideas'] = GiftIdea::select('giftIdeas.*', 'shop.name', 'shoppingMall.name as mall')->join('shop', 'shop.shopMGID', '=', 'giftIdeas.shopMGID')->join('shoppingMall', 'shoppingMall.mallID', '=', 'shop.mallID')->whereIn('giftIdeas.shopMGID', $shops)->orderBy('title')->get();

                return view('shopmanagers/chainstores/giftideas/index', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Update Gift Idea Display status
     * @param $id
     * @param $display
     */
    public function postUpdateGiftIdeaDisplayStatus($id, $display)
    {

        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();

            if ($user->hasAccess('user') || $user->hasAccess('manager')) {

                GiftIdea::find($id)->update(array('display' => $display));
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Edit store Gift Idea
     */
    public function getEditStoreGiftIdea($id)
    {

        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();

            if ($user->hasAccess('user') || $user->hasAccess('manager')) {

                $data['giftIdea'] = GiftIdea::find($id);
                $data['categories'] = GiftIdea::distinct('category')->orderBy('category')->pluck('category', 'category');
                $data['price'] = array('Under R50' => 'Under R50', 'R50 - R200' => 'R50 - R200', 'R200 - R500' => 'R200 - R500', 'R500 and above' => 'R500 and above');
                $data['for'] = array('All' => 'All', 'Women' => 'Women', 'Men' => 'Men', 'Teenage girls' => 'Teenage girls', 'Teenage boys' => 'Teenage boys', 'Children - girls' => 'Children - girls', 'Children - boys' => 'Children - boys');

                return view('shopmanagers.shopuser.giftideas.edit', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Add chain store user
     */
    public function getAddChainUser()
    {
        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $userChain = UserChain::where('user_id', '=', $user->id)->first()->chain_name;
                $data['chain_name'] = $userChain;
                //$data['provinces'] = array(''=>'Select an existing province') + DB::table('provinces')->pluck('name','id');
                //$data['cities'] = array('Select an existing city') + City::orderBy('name')->->pluck('name','id');
                $data['suburbs'] = Suburb::distinct('suburb')->orderBy('suburb')->pluck('suburb', 'suburbID');

                return view('shopmanagers/chainstores/adduser', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Tenant add user
     */
    public function getAddStoreUser()
    {

        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();
//            dd(session()->all());
            if ($user->hasAccess('user') || $user->hasAccess('manager')) {

                $usr = User::find($user->id);
                $userShops = $usr->shops;
                $shopIds = array();

                foreach ($userShops as $shop) {
                    array_push($shopIds, $shop->shopMGID);
                }

                $data['provinces'] = Province::pluck('name', 'id');
                $data['cities'] = ShoppingMall::distinct('city')->orderBy('city')->pluck('city', 'city');
                $data['suburbs'] = Suburb::distinct('suburb')->orderBy('suburb')->pluck('suburb', 'suburbID');
                $data['mall'] = ShoppingMall::find($user->mallID);

                return view('shopmanagers/shopuser/adduser', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Post Edit store gift idea
     */
    public function postEditStoreGiftIdea($id)
    {

        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();
//            dd(Input::all());
            if ($user->hasAccess('user') || $user->hasAccess('manager')) {

                $input = Input::all();
                $v = GiftIdea::validate($input);

                if ($v->passes()) {

                    MGCrud::UpdateGiftIdea($id);

                    session()->flash('success', 'The gift idea has been updated!!');
                    return redirect('store/giftideas');

                } else {
                    session()->flash('error', 'Please review the form!');
                    return Redirect::route('store.editStoreGiftIdea', $id)->withErrors($v)->withInput();
                }
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Delete store Gift Idea
     * @param $id
     */
    public function postDeleteStoreGiftIdea($id)
    {

        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();

            if ($user->hasAccess('user') || $user->hasAccess('manager')) {

                GiftIdea::find($id)->delete();

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Add Gift Idea
     */
    public function getAddGiftIdea()
    {

        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();

            if ($user->hasAccess('user') || $user->hasAccess('manager')) {

                $data['shop'] = 0;
                $data['mall'] = 0;
                $shopID = session()->get('activeShopID');
                $mallID = session()->get('activeMallID');
                $chainName = session()->get('activeChain');

                if (isset($shopID)) {
                    $data['shop'] = $shopID;
                }
                if (isset($mallID)) {
                    $data['mall'] = $mallID;
                    $mall = ShoppingMall::find($mallID);
                    $data['mallShops'] = $mall->shops()->where('display', '=', 'Y')->orderBy('name')->pluck('name', 'shopMGID');
                }

                if (isset($chainName)) {
                    $mallIDs = Shop::where('name', '=', $chainName)->pluck('mallID');
                    $data['mallIDs'] = $mallIDs;
                    $data['chainStoreMalls'] = ShoppingMall::whereIn('mallID', $mallIDs)->orderBy('name')->pluck('name', 'mallID');
                }

                $data['categories'] = GiftIdea::distinct('category')->where('category', '!=', '')->orderBy('category')->pluck('category', 'category');
                $data['price'] = array('Under R50' => 'Under R50', 'R50 - R200' => 'R50 - R200', 'R200 - R500' => 'R200 - R500', 'R500 and above' => 'R500 and above');
                $data['for'] = array('All' => 'All', 'Women' => 'Women', 'Men' => 'Men', 'Teenage girls' => 'Teenage girls', 'Teenage boys' => 'Teenage boys', 'Children - girls' => 'Children - girls', 'Children - boys' => 'Children - boys');

                return view('shopmanagers.shopuser.giftideas.create', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Post Add Gift Idea
     */
    public function postAddGiftIdea()
    {
        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();
            $destinationPath = base_path('uploadimages');
            $input = Input::all();

            if ($user->hasAccess('user') || $user->hasAccess('manager')) {

                $v = GiftIdea::validate($input);

                if ($v->passes()) {

                    $malls = Input::get('malls');
                    if (isset($malls)) {
                        $mallArray = explode(",", $malls);
                    }

                    if (isset($mallArray) && is_array($mallArray)) {

                        $shops = Shop::where('name', '=', session()->get('activeChain'))->whereIn('mallID', $mallArray)->pluck('shopMGID');

                        $thumbMaxWidth = 150;
                        $largeMaxWidth = 450;
                        $file1 = Input::file('image1');
                        $uploadDate = date("YmdHis");

                        if (Input::hasFile('image1')) {
                            $file = $file1->getClientOriginalName();
                            $extension = $file1->getClientOriginalExtension();

                            $file1->move($destinationPath, $file);
                        }

                        foreach ($shops as $k => $v) {

                            $shop = Shop::find($shops[$k]);

                            $data['mallID'] = $shop->mallID;
                            $data['shopMGID'] = $shop->shopMGID;
                            $data['dateAdded'] = date("Y-m-d H:i:s");
                            $data['title'] = Input::get('title');
                            $data['manufacturer'] = Input::get('manufacturer');
                            $data['category'] = Input::get('category');
                            $data['price'] = Input::get('price');
                            $data['forWho'] = Input::get('for');
                            $data['description'] = Input::get('description');

                            if (!File::exists($destinationPath)) {
                                File::makeDirectory($destinationPath);
                            }

                            $originPath = $destinationPath;

                            if (isset($file1)) {
                                Image::make($originPath . '/' . $file)->resize($thumbMaxWidth, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                })->save($destinationPath . '/' . $file . '_' . $uploadDate . '_thumbnail.' . $extension);
                                Image::make($originPath . '/' . $file)->resize($largeMaxWidth, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                })->save($destinationPath . '/' . $file . '_' . $uploadDate . '_large.' . $extension);

                                $data['thumbnail1'] = $file . '_' . $uploadDate . '_thumbnail.' . $extension;
                                $data['image1'] = $file . '_' . $uploadDate . '_large.' . $extension;
                            }

                            GiftIdea::insert($data);
                        }
                        session()->flash('success', 'The gift idea has been added !');
                        return redirect('/store/giftideas');

                    } else {
//                        dd(Input::all());
                        $input['mallID'] = Input::get('mallID');
                        $input['shopMGID'] = Input::get('shopMGID');
                        $input['title'] = Input::get('title');
                        $input['manufacturer'] = Input::get('manufacturer');
                        $input['category'] = Input::get('category');
                        $input['price'] = Input::get('price');
                        $input['for'] = Input::get('for');
                        $input['description'] = Input::get('description');
                        $input['image1'] = Input::file('image1');

                        $data['mallID'] = $input['mallID'];
                        $data['shopMGID'] = $input['shopMGID'];
                        $data['dateAdded'] = date("Y-m-d H:i:s");
                        $data['title'] = Input::get('title');
                        $data['manufacturer'] = Input::get('manufacturer');
                        $data['category'] = Input::get('category');
                        $data['price'] = Input::get('price');
                        $data['forWho'] = Input::get('for');
                        $data['description'] = Input::get('description');
                        $image = Input::file('image1');

                        if (isset($image)) {

                            $thumbMaxWidth = 260;

                            $img1 = uploadImage($image, $destinationPath, $thumbMaxWidth, $thumb = true, $large = true);
                            $data['thumbnail1'] = $img1['thumbnail'];
                            $data['image1'] = $img1['largeImage'];
                        }
                        GiftIdea::insert($data);
                        session()->flash('success', 'The gift idea has been added !');
                        return redirect('/store/giftideas');
                    }
                } else {
                    session()->flash('error', 'There are errors, please review the form!');
                    return redirect(route('store.addgiftidea'))->withErrors($v)->withInput();
                }
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Add chain store gift idea
     */
    public function GetCreateChainstoreGiftIdea()
    {
        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $userChain = UserChain::where('user_id', '=', $user->id)->first()->chain_name;
                $mallIDs = Shop::where('name', '=', $userChain)->pluck('mallID');
                $data['malls'] = array('' => 'Select a mall') + ShoppingMall::whereIn('mallID', $mallIDs)->orderBy('name')->pluck('name', 'mallID');
                $data['mallIDs'] = ShoppingMall::whereIn('mallID', $mallIDs)->orderBy('name')->pluck('mallID');
                $data['categories'] = array('' => 'Select a category') + GiftIdea::distinct('category')->orderBy('category')->pluck('category', 'category');
                $data['price'] = array('Under R50' => 'Under R50', 'R50 - R200' => 'R50 - R200', 'R200 - R500' => 'R200 - R500', 'R500 and above' => 'R500 and above');
                $data['for'] = array('All' => 'All', 'Women' => 'Women', 'Men' => 'Men', 'Teenage girls' => 'Teenage girls', 'Teenage boys' => 'Teenage boys', 'Children - girls' => 'Children - girls', 'Children - boys' => 'Children - boys');

                return view('shopmanagers/chainstores/giftideas/create', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get Store Jobs
     */
    public function getStoreJobs()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('user') && session()->has('activeShopID')) {

                $shop = Shop::find(session()->get('activeShopID'));
                $data['jobs'] = $shop->jobs;
                return view('shopmanagers/shopuser/jobs/index', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Store Job Edit page
     */
    public function getEditStoreJob($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('user') || $user->hasAccess('manager')) {

                $data['job'] = Job::find($id);
                return view('shopmanagers/shopuser/jobs/edit', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Post Edit Job
     */
    public function postEditStoreJob($id)
    {
        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('user')) {

                $input = Input::all();
                $v = Job::validate($input);

                if ($v->passes()) {
                    $data['title'] = Input::get('title');
                    $data['salary'] = Input::get('salary');
                    $data['startDate'] = Input::get('startDate');
                    $data['refNum'] = Input::get('refNum');
                    $data['endDate'] = Input::get('endDate');
                    $data['description'] = Input::get('description');
                    $data['contactPerson'] = Input::get('contactPerson');
                    $data['cell'] = Input::get('cell');
                    $data['telephone'] = Input::get('telephone');
                    $data['fax'] = Input::get('fax');
                    $data['email'] = Input::get('email');
                    $data['showInfo'] = Input::get('showInfo');
                    $data['display'] = Input::get('display');

                    Job::find($id)->update($data);
                    session()->flash('success', 'The job has been updated !');
                    return redirect('store/jobs');
                } else {
                    session()->flash('error', 'Please review the form!');
                    return redirect(route('store.editStoreJob', $id))->withErrors($v)->withInput();
                }
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Create Store Job
     */
    public function getCreateStoreJob()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('user')) {
                return view('shopmanagers/shopuser/jobs/create');
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Post Add Job
     */
    public function postCreateStoreJob()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('user')) {

                $input = Input::all();
                $v = Job::validate($input);

                if ($v->passes()) {
                    $data['shopMGID'] = Input::get('shopMGID');
                    $data['mallID'] = Shop::find($data['shopMGID'])->mallID;
                    $data['dateAdded'] = date("Y-m-d H:i:s");
                    $data['title'] = Input::get('title');
                    $data['salary'] = Input::get('salary');
                    $data['startDate'] = Input::get('startDate');
                    $data['refNum'] = Input::get('refNum');
                    $data['endDate'] = Input::get('endDate');
                    $data['description'] = Input::get('description');
                    $data['contactPerson'] = Input::get('contactPerson');
                    $data['cell'] = Input::get('cell');
                    $data['telephone'] = Input::get('telephone');
                    $data['fax'] = Input::get('fax');
                    $data['email'] = Input::get('email');
                    $data['showInfo'] = Input::get('showInfo');

                    Job::insert($data);
                    session()->flash('success', 'The job has been added !');
                    return redirect('store/jobs');
                } else {
                    session()->flash('error', 'Please review the form!');
                    return redirect(route('store.createStoreJob'))->withErrors($v)->withInput();
                }
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Store Marketing Info
     */
    public function getStoreMarketingInfo()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('user')) {

                return view('shopmanagers/shopuser/marketinginformation');

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Store competition terms
     */
    public function getStoreCompetitionTerms()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('user')) {

                return view('shopmanagers/shopuser/competitionterms');

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Store User Update Profile
     */
    public function getEditStoreUserProfile($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('user')) {

                $data['user'] = Sentinel::getUserProvider()->findById($id);
                $data['provinces'] = array('' => 'Select an existing province') + DB::table('provinces')->pluck('name', 'id');
                $data['cities'] = DB::table('adminUsers')->distinct('city')->orderBy('city')->pluck('city', 'city');

                return view('shopmanagers/shopuser/editprofile', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Post Update User Profile
     */
    public function postEditUserProfile($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('user')) {

                $input = Input::all();

                $v = User::validate($input);

                if ($v->passes()) {

                    MGCrud::UpdateUserProfile($user);

                    session()->flash('success', 'Your details have been updated !!');
                    return redirect('/store');
                } else {
                    session()->flash('error', 'Please review the form!');
                    return Redirect::route('store.editprofile', $id)->withErrors($v)->withInput();
                }
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get Update user password
     */
    public function getUpdateStoreUserPassword($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('user')) {

                $data['user'] = $user;
                return view('shopmanagers/shopuser/updatepassword', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Post Update User password
     */
    public function postUpdateStoreUserPassword($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('user')) {

                $input = array(
                    'old_pass' => Input::get('old_pass'),
                    'new_pass' => Input::get('new_pass'),
                    'new_pass_confirmation' => Input::get('new_pass_confirmation')
                );

                // Set Validation Rules
                $rules = array(
                    'old_pass' => 'required',
                    'new_pass' => 'required|confirmed|regex:/\A(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{6,}\z/',
                    'new_pass_confirmation' => 'required|regex:/\A(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{6,}\z/'

                );

                //Run input validation
                $v = Validator::make($input, $rules);

                if ($v->passes()) {

                    if (Hash::check(Input::get('old_pass'), $user->password)) {

                        $user->password = Input::get('new_pass');

                        if ($user->save()) {
                            session()->flash('success', 'Your password has been updated.');
                            return redirect('/store');
                        } else {
                            session()->flash('error', 'Your password could not be updated.');
                            return redirect('/store');
                        }
                    } else {
                        session()->flash('error', 'The password you enterd does not match your current password.');
                        return redirect('store/updatepassword/' . $id)->withErrors($v)->withInput();
                    }

                } else {
                    session()->flash('error', 'Please review the form!');
                    return Redirect::route('store.updatepassword', $id)->withErrors($v)->withInput();
                }
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get Marketing Info for chain shop
     */
    public function getMarketingInfo()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                return view('shopmanagers/chainstores/marketinginformation');

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * List stores for Logged in user
     * @return mixed
     */
    public function getChainShops()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('user') || $user->hasAccess('manager')) {

                $userChain = session()->get('activeChain');
                $data['user'] = $user;
                $country = session()->get('default_country_id');
                $data['provinceList'] = array('' => 'Select Province ...') + Province::where('country_id', '=', $country)->orderBy('id')->pluck('name', 'id');
                $data['userShops'] = Shop::where('shop.name', '=', $userChain)->select('shoppingMall.mallID', 'shoppingMall.name', 'shop.shopMGID', 'shop.name as shop', 'shop.category_id', 'shop.subcategory_id', 'shop.display')->with('ShopCategory')->join('shoppingMall', 'shoppingMall.mallID', '=', 'shop.mallID')->groupBy('shopMGID')->orderBy('shoppingMall.name')->get();

                return view('shopmanagers/chainstores/list', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Filter chain shops by province
     */
    public function getFilterChainByProvince($id)
    {

        if (Sentinel::check()) {

//            dd($id);
            // Find the user using the user id
            $user = Sentinel::getUser();
            $userChain = session()->get('activeChain');
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $data['user'] = $user;
                $country = session()->get('default_country_id');
                $data['provinceList'] = array('' => 'Select Province ...') + Province::where('country_id', '=', $country)->orderBy('id')->pluck('name', 'id');
                $data['userShops'] = Shop::where('shop.name', '=', $userChain)->where('shoppingMall.province_id', '=', $id)->select('shoppingMall.mallID', 'shoppingMall.name', 'shop.shopMGID', 'shop.name as shop', 'shop.category', 'shop.subcategory', 'shop.display')->join('shoppingMall', 'shoppingMall.mallID', '=', 'shop.mallID')->groupBy('shopMGID')->orderBy('shoppingMall.name')->get();
                $data['province'] = $id;
                return view('shopmanagers/chainstores/list', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Delete Store
     */
    public function postDeleteStore($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                MGCrud::DeleteShop($id);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Edit chain store
     */
    public function getEditChainStore($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $data['categories'] = Shop::distinct('category')->orderBy('category')->pluck('category', 'category');
                $data['subcategories'] = Shop::distinct('subcategory')->orderBy('subcategory')->pluck('subcategory', 'subcategory');
                $data['shop'] = Shop::find($id);

                return view('shopmanagers/chainstores/edit', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     *
     */
    public function postEditStore($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $input = Input::all();
                $v = Shop::validate($input);

                if ($v->passes()) {

                    MGCrud::EditShop($id);
                    session()->flash('success', 'The shop has been updated.');
                    return Redirect::route('chainshop.chainstores');

                } else {
                    session()->flash('error', 'Please review the form!');
                    return Redirect::route('chainshop.edit', $id)->withErrors($v)->withInput();
                }
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Create Shop
     */
    public function getCreateShop()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {
                //dd(session()->all());

                if (session()->has('activeChain')) {
                    $userChain = session()->get('activeChain');
                } else {
                    $userChain = UserChain::where('user_id', '=', $user->id)->first()->chain_name;
                }
                $data['shopName'] = $userChain;
                $data['malls'] = array('' => 'Select a mall') + ShoppingMall::orderBy('name')->pluck('name', 'mallID');
                $data['categories'] = Shop::distinct('category')->orderBy('category')->pluck('category', 'category');
                $data['subcategories'] = Shop::distinct('subcategory')->orderBy('subcategory')->pluck('subcategory', 'subcategory');
                $data['name'] = $userChain;

                return view('shopmanagers/chainstores/create', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get Jobs List
     * @return mixed
     */
    public function getJobsList()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $userChain = session()->get('activeChain');

                $shops = Shop::where('name', '=', $userChain)->pluck('shopMGID');
                $shopMalls = Shop::where('name', '=', $userChain)->pluck('mallID');
                $data['malls'] = array('' => 'View all malls...') + ShoppingMall::whereIn('mallID', $shopMalls)->orderBy('name')->pluck('name', 'mallID');

                $data['currentJobs'] = Job::select(DB::raw('jobs.*,shoppingMall.name as mall,shop.name,SUM(CASE WHEN jobApplications.jobID = jobs.jobID THEN 1 ELSE 0 END) as applicants'))
                    ->leftjoin('shop', 'shop.shopMGID', '=', 'jobs.shopMGID')
                    ->leftjoin('jobApplications', 'jobApplications.jobID', '=', 'jobs.jobID')
                    ->join('shoppingMall', 'shoppingMall.mallID', '=', 'jobs.mallID')
                    ->leftjoin('jobApplicants', 'jobApplicants.jobApplicantID', '=', 'jobApplications.jobApplicantID')
                    ->whereIn('jobs.shopMGID', $shops)
                    ->groupBy('jobs.jobID')
                    ->orderBy('jobs.endDate', 'desc')
                    ->get();

                return view('shopmanagers/chainstores/jobs/index', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get Create Job page
     * @return mixed
     */
    public function getCreateJob()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $userChain = UserChain::where('user_id', '=', $user->id)->first()->chain_name;
                $userMalls = Shop::where('name', '=', $userChain)->pluck('mallID');
                $data['malls'] = ShoppingMall::whereIn('mallID', $userMalls)->orderBy('name')->pluck('name', 'mallID');
                $data['mallIDs'] = ShoppingMall::whereIn('mallID', $userMalls)->orderBy('name')->pluck('mallID');

                return view('shopmanagers/chainstores/jobs/create', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Post Create Job
     * @return mixed
     */
    public function postCreateJob()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $input = Input::all();
                $v = Job::validate($input);

                if ($v->passes()) {

                    $selectedMalls = Input::get('mallID');
                    $activeChain = session()->get('activeChain');
                    $shops = Shop::where('name', '=', $activeChain)->pluck('shopMGID');
                    $data['title'] = Input::get('title');
                    $data['salary'] = Input::get('salary');
                    $data['startDate'] = Input::get('startDate');
                    $data['endDate'] = Input::get('endDate');
                    $data['refNum'] = Input::get('refNum');
                    $data['description'] = Input::get('description');
                    $data['contactPerson'] = Input::get('contactPerson');
                    $data['cell'] = Input::get('cell');
                    $data['telephone'] = Input::get('telephone');
                    $data['fax'] = Input::get('fax');
                    $data['email'] = Input::get('email');
                    $data['display'] = Input::get('display');
                    $data['showInfo'] = Input::get('showInfo');
                    $data['dateAdded'] = date('Y-m-d H:i:s');

                    foreach ($selectedMalls as $k => $v) {

                        $data['mallID'] = $v;
                        $data['shopMGID'] = $shops[$k];
                        Job::Insert($data);
                    }

                    return redirect(route('chainshop.jobs'));
                    session()->flash('success', 'The job has successfully been added');

                } else {
                    session()->flash('error', 'Please review the form!');
                    return redirect(route('chainshop.createjob'))->withErrors($v)->withInput();
                }
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Get Promotions List for chain store manager
     * @return mixed
     */
    public function getChainPromotionList()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $userChain = UserChain::where('user_id', '=', $user->id)->first()->chain_name;

                $shops = Shop::where('name', '=', $userChain)->pluck('shopMGID');
                $shopMalls = Shop::where('name', '=', $userChain)->pluck('mallID');

                $data['malls'] = ShoppingMall::whereIn('mallID', $shopMalls)->orderBy('name')->pluck('name', 'mallID');
                $data['currentPromotions'] = Promotion::whereIn('promotions.shopMGID', $shops)
                    ->where('promotions.endDate', '>=', date("Y-m-d H:i:s"))
                    ->join('shop', 'shop.shopMGID', '=', 'promotions.shopMGID')
                    ->join('shoppingMall', 'shoppingMall.mallID', '=', 'promotions.mallID')
                    ->select('shop.name', 'shoppingMall.name as mall', 'promotions.promotion', 'promotions.startDate', 'promotions.endDate', 'promotions.promotionsMGID')
                    ->orderBy('promotions.endDate', 'desc')
                    ->get();


                $data['pastPromotions'] = Promotion::whereIn('promotions.shopMGID', $shops)
                    ->where('promotions.endDate', '<=', date("Y-m-d H:i:s"))
                    ->join('shop', 'shop.shopMGID', '=', 'promotions.shopMGID')
                    ->join('shoppingMall', 'shoppingMall.mallID', '=', 'promotions.mallID')
                    ->select('shop.name', 'shoppingMall.name as mall', 'promotions.promotion', 'promotions.startDate', 'promotions.endDate', 'promotions.promotionsMGID')
                    ->orderBy('promotions.endDate', 'desc')
                    ->get();

                return view('shopmanagers/chainstores/promotions/index', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    public function getListChainPromosByMall($mallID)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $userChain = session()->get('activeChain');
                $shopMalls = Shop::where('name', '=', $userChain)->pluck('mallID');
                $data['malls'] = array('' => 'View all malls...') + ShoppingMall::whereIn('mallID', $shopMalls)->orderBy('name')->pluck('name', 'mallID');
                $data['mallID'] = $mallID;

                $shops = Shop::where('name', '=', $userChain)->pluck('shopMGID');

                $data['currentPromotions'] = Promotion::whereIn('promotions.shopMGID', $shops)
                    ->where('promotions.endDate', '>=', date("Y-m-d H:i:s"))
                    ->where('promotions.mallID', '=', $mallID)
                    ->join('shop', 'shop.shopMGID', '=', 'promotions.shopMGID')
                    ->join('shoppingMall', 'shoppingMall.mallID', '=', 'promotions.mallID')
                    ->select('shop.name', 'shoppingMall.name as mall', 'promotions.promotion', 'promotions.startDate', 'promotions.endDate', 'promotions.promotionsMGID')
                    ->orderBy('promotions.endDate', 'desc')
                    ->get();


                $data['pastPromotions'] = Promotion::whereIn('promotions.shopMGID', $shops)
                    ->where('promotions.endDate', '<=', date("Y-m-d H:i:s"))
                    ->where('promotions.mallID', '=', $mallID)
                    ->join('shop', 'shop.shopMGID', '=', 'promotions.shopMGID')
                    ->join('shoppingMall', 'shoppingMall.mallID', '=', 'promotions.mallID')
                    ->select('shop.name', 'shoppingMall.name as mall', 'promotions.promotion', 'promotions.startDate', 'promotions.endDate', 'promotions.promotionsMGID')
                    ->orderBy('promotions.endDate', 'desc')
                    ->get();
                return view('shopmanagers/chainstores/promotions/mallpromotions', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Filter manager promotions by mallID
     */
    public function getFilterPromotionByMall($mallID)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $shops = Shop::where('name', '=', $user->chain_name)->pluck('shopMGID');
                $currentPromotions = Promotion::whereIn('promotions.shopMGID', $shops)
                    ->where('promotions.endDate', '>=', date("Y-m-d H:i:s"))
                    ->where('shop.mallID', '=', $mallID)
                    ->join('shop', 'shop.shopMGID', '=', 'promotions.shopMGID')
                    ->join('shoppingMall', 'shoppingMall.mallID', '=', 'promotions.mallID')
                    ->select('shop.name', 'shoppingMall.name as mall', 'promotions.promotion', 'promotions.startDate', 'promotions.endDate', 'promotions.promotionsMGID')
                    ->orderBy('promotions.endDate', 'desc')
                    ->get();

                $pastPromotions = Promotion::whereIn('promotions.shopMGID', $shops)
                    ->where('promotions.endDate', '<=', date("Y-m-d H:i:s"))
                    ->where('shop.mallID', '=', $mallID)
                    ->join('shop', 'shop.shopMGID', '=', 'promotions.shopMGID')
                    ->join('shoppingMall', 'shoppingMall.mallID', '=', 'promotions.mallID')
                    ->select('shop.name', 'shoppingMall.name as mall', 'promotions.promotion', 'promotions.startDate', 'promotions.endDate', 'promotions.promotionsMGID')
                    ->orderBy('promotions.endDate', 'desc')
                    ->get();

                $shopMalls = Shop::where('name', '=', $user->chain_name)->pluck('mallID');
                $malls = array('' => 'View all malls...') + ShoppingMall::whereIn('mallID', $shopMalls)->orderBy('name')->pluck('name', 'mallID');

                session()->put('currentPromotions', $currentPromotions);
                session()->put('pastPromotions', $pastPromotions);
                session()->put('malls', $malls);
                session()->put('mallID', $mallID);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getMallPromotions($mallID)
    {
        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {
                return view('shopmanagers.chainstores.promotions.mallpromotions');
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Edit Promotion
     */
    public function getEditPromotion($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $data['promotion'] = Promotion::find($id);
                return view('shopmanagers/chainstores/promotions/edit', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get create Promotion Page
     */
    public function getCreateChainPromotion()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $userChain = UserChain::where('user_id', '=', $user->id)->first()->chain_name;
                if (!is_null(session()->get('activeChain')) && session()->get('activeChain') != '') {
                    $userChain = session()->get('activeChain', $userChain);
                }

                $data['chain_name'] = $userChain;
                $userMalls = Shop::where('name', '=', $userChain)->pluck('mallID');
                $data['malls'] = ShoppingMall::whereIn('mallID', $userMalls)->orderBy('name')->pluck('name', 'mallID');
                $data['mallIDs'] = ShoppingMall::whereIn('mallID', $userMalls)->orderBy('name')->pluck('mallID');

                return view('shopmanagers/chainstores/promotions/create', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Get Competition List
     */
    public function getChainCompetitionList()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $userChain = UserChain::where('user_id', '=', $user->id)->first()->chain_name;

                $malls = Shop::where('name', '=', $userChain)->pluck('mallID');
                $shops = Shop::where('name', '=', $userChain)->pluck('shopMGID');

                $data['currentCompetitions'] = DB::table('forms')->select(DB::raw('forms.formID,pageID,recipient,subject,forms.description,thanksMsg,submitBtnText,forms.dateAdded,activated,startDate,endDate,SUM(CASE WHEN formFeedback.formID = forms.formID THEN 1 ELSE 0 END) as count_resp,(SELECT COUNT(competitionWinners.compWinnerID) FROM competitionWinners WHERE competitionWinners.formID = forms.formID) as winners'))
                    ->leftjoin('formFeedback', 'formFeedback.formID', '=', 'forms.formID')
                    ->leftjoin('competitionWinners', 'competitionWinners.formID', '=', 'formFeedback.formID')
                    ->leftjoin('formRequest', 'formRequest.formID', '=', 'forms.formID')
                    ->where('formType', '=', 'Competition')
                    ->whereIn('formRequest.shopMGID', $shops)
                    ->where('endDate', '>=', date('Y-m-d'))
                    ->groupBy('forms.formID')
                    ->orderBy('endDate', 'desc')
                    ->get();

                $data['pastCompetitions'] = DB::table('forms')->select(DB::raw('forms.formID,pageID,recipient,subject,forms.description,thanksMsg,submitBtnText,forms.dateAdded,activated,startDate,endDate,SUM(CASE WHEN formFeedback.formID = forms.formID THEN 1 ELSE 0 END) as count_resp,(SELECT COUNT(competitionWinners.compWinnerID) FROM competitionWinners WHERE competitionWinners.formID = forms.formID) as winners'))
                    ->leftjoin('formFeedback', 'formFeedback.formID', '=', 'forms.formID')
                    ->leftjoin('competitionWinners', 'competitionWinners.formID', '=', 'formFeedback.formID')
                    ->leftjoin('formRequest', 'formRequest.formID', '=', 'forms.formID')
                    ->where('formType', '=', 'Competition')
                    ->whereIn('formRequest.shopMGID', $shops)
                    ->where('endDate', '<=', date('Y-m-d'))
                    ->groupBy('forms.formID')
                    ->orderBy('endDate', 'desc')
                    ->get();

                return view('shopmanagers/chainstores/competitions/index', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getCreateChainCompetition()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $userChain = UserChain::where('user_id', '=', $user->id)->first()->chain_name;
                $userMalls = Shop::where('name', '=', $userChain)->pluck('mallID');
                $data['malls'] = ShoppingMall::whereIn('mallID', $userMalls)->orderBy('name')->pluck('name', 'mallID');
                $data['mallIDs'] = ShoppingMall::whereIn('mallID', $userMalls)->orderBy('name')->pluck('mallID');

                return view('shopmanagers/chainstores/competitions/create', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    public function postCreatecompetition()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $input = Input::all();
                $v = MGForm::validate($input);

                if ($v->passes()) {

                    $selectedMalls = Input::get('mallID');
                    $shops = Shop::where('name', '=', $user->chain_name)->whereIn('mallID', $selectedMalls)->pluck('shopMGID');

                    $data['recipient'] = Input::get('recipient');
                    $data['subject'] = Input::get('subject');
                    $data['description'] = Input::get('description');
                    $data['thanksMsg'] = Input::get('thanksMsg');
                    $data['submitBtnText'] = Input::get('submitBtnText');
                    $data['dateAdded'] = date("Y-m-d H:i:s");
                    $data['activated'] = Input::get('activated');
                    $data['formType'] = 'Competition';
                    $data['startDate'] = Input::get('startDate');
                    $data['endDate'] = Input::get('endDate');

                    $thumbMaxWidth = 150;
                    $largeMaxWidth = 450;

                    $file1 = Input::file('image');

                    if (isset($file1)) {

                        $destinationPath = Config::get('image_upload_path');
//                        $destinationPath = public_path().'/uploadimages';

                        $img1 = Helpers::uploadImage($file1, $destinationPath, $thumbMaxWidth, $largeMaxWidth, $thumb = true, $large = true);
                        $data['thumbnail'] = $img1['thumbnail'];
                        $data['image'] = $img1['largeImage'];
                    }

                    $form = MGForm::insertGetId($data);

                    if ($form) {

                        foreach ($selectedMalls as $k => $v) {

                            $shop = Shop::find($shops[$k]);

                            $formReqData['formID'] = $form;
                            $formReqData['userID'] = $user->id;
                            $formReqData['mallID'] = $shop->mallID;
                            $formReqData['shopMGID'] = $shop->shopMGID;
                            $formReqData['dateAdded'] = date("Y-m-d H:i:s");
                            $formReqData['display'] = 'Y';

                            //Insert Into formRequest
                            FormRequest::insertGetId($formReqData);
                        }

                        //Insert into the table formFields
                        $formFieldArray = array('E-mail address', 'Name', 'Surname', 'Cell', 'Telephone');
                        foreach ($formFieldArray as $k => $val) {

                            //Insert into the table formFieldSpecs
                            $formFieldData['formID'] = $form;
                            $formFieldData['name'] = $val;
                            $formFieldData['typeOfField'] = 'text';
                            $formFieldData['contentOrder'] = $k + 1;

                            if ($k < 3) {
                                $formFieldData['mandatory'] = 'Y';
                            } else {
                                $formFieldData['mandatory'] = 'N';
                            }

                            if ($k == 0) {
                                $formFieldData['emailField'] = 'Y';
                            } else {
                                $formFieldData['emailField'] = 'X';
                            }

                            $formFieldID = FormField::insertGetId($formFieldData);

                            //Get latest Entry and insert into formFieldSpecs
                            $formFieldSpecData['formFieldID'] = $formFieldID;
                            $formFieldSpecData['defaultValue'] = '';
                            $formFieldSpecData['maxlength'] = '';
                            $formFieldSpecData['size'] = 200;
                            $formFieldSpecData['rows'] = '';
                            $formFieldSpecData['cols'] = '';
                            $formFieldSpecData['wrap'] = '';
                            $formFieldSpecData['multiple'] = '';

                            FormFieldSpec::insert($formFieldSpecData);
                        }

                        return Redirect::route('chainshop.compfields', $form);
                    }

                } else {
                    session()->flash('error', 'Please review the form!');
                    return Redirect::route('chainshop.createcompetition')->withErrors($v)->withInput();
                }

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Get Comp fields (Second Step of comp add process)
     */
    public function getChainCompetitionFields($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $form = MGForm::find($id);
                $formFields = FormField::where('formID', '=', $form->formID)->get();

                return view('shopmanagers/chainstores/competitions/fields', array('form' => $form, 'formFields' => $formFields));
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Delete Competition
     */
    public function postDeleteComp($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                MGForm::find($id)->delete();

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Edit Competition Form
     */
    public function postEditField($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $input = Input::all();
                $v = MGForm::validate($input);

                if ($v->passes()) {

                    $form = MGForm::find($id);

                    $data['recipient'] = Input::get('recipient');
                    $data['subject'] = Input::get('subject');
                    $data['description'] = Input::get('description');
                    $data['startDate'] = Input::get('startDate');
                    $data['endDate'] = Input::get('endDate');
                    $data['activated'] = Input::get('activated');
                    $data['noForm'] = Input::get('noForm');

                    $thumbMaxWidth = 150;
                    $largeMaxWidth = 450;

                    $destinationPath = Config::get('image_upload_path');
//                    $destinationPath = public_path().'/uploadimages';

                    $file1 = Input::file('image');

                    if (isset($file1)) {

                        $oldThumbNail = $form->thumbnail;
                        $oldImage = $form->image;
                        Helpers::removeOldImage($destinationPath, $oldThumbNail);
                        Helpers::removeOldImage($destinationPath, $oldImage);

                        $img1 = Helpers::uploadImage($file1, $destinationPath, $thumbMaxWidth, $largeMaxWidth, $thumb = true, $large = true);
                        $data['thumbnail'] = $img1['thumbnail'];
                        $data['image'] = $img1['largeImage'];
                    }

                    $form->update($data);

                    session()->flash('success', 'The competition has been updated');
                    return Redirect::route('chainshop.competitions');


                } else {
                    session()->flash('error', 'Please review the form!');
                    return Redirect::route('chainshop.compfields', $id)->withErrors($v)->withInput();
                }
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Remove competition field
     * @param $id
     */
    public function postDeleteField($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $fieldSpecID = null;
                $fieldSpec = FormFieldSpec::where('formFieldID', '=', $id)->select('formFieldSpecID')->get();
                foreach ($fieldSpec as $spec) {
                    $fieldSpecID = $spec->formFieldSpecID;
                }

                FormFieldSpec::find($fieldSpecID)->delete();
                FormField::find($id)->delete();
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Edit Form field
     * @param $formID
     * @param $formFieldID
     */
    public function getEditFormField($formID, $formFieldID)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $data['field'] = '';

                $field = FormField::select('formFields.*', 'formFieldSpecs.size', 'formFieldSpecs.maxlength', 'formFieldSpecs.defaultValue')
                    ->join('formFieldSpecs', 'formFieldSpecs.formFieldID', '=', 'formFields.formFieldID')
                    ->where('formFields.formFieldID', '=', $formFieldID)
                    ->groupBy('formFields.formFieldID')->get();

                foreach ($field as $fld) {
                    $data['field'] = $fld;
                }

                $data['size'] = array_combine(range(30, 300), range(30, 300));
                $data['formID'] = $formID;

                return view('shopmanagers/chainstores/competitions/formfield', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Post Edit Form field
     * @param $id
     */
    public function postEditFormField($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $input = Input::all();

                $v = FormField::validate($input);

                if ($v->passes()) {

                    $data['name'] = Input::get('name');
                    $data['defaultValue'] = Input::get('defaultValue');
                    $data['size'] = Input::get('size');
                    $data['maxlength'] = Input::get('maxlength');
                    $data['mandatory'] = Input::get('mandatory');

                    FormField::find($id)->update(array('name' => $data['name'], 'mandatory' => $data['mandatory']));
                    FormFieldSpec::where('formFieldID', '=', $id)->update(array('defaultValue' => $input['defaultValue'], 'size' => $input['size'], 'maxlength' => $input['maxlength']));

                    session()->flash('success', 'The form field has been Updated');
                    return redirect('chainstores/competitions/fields/' . Input::get('formID'));

                } else {
                    return Redirect::route('chainshop.editformfield', array(Input::get('formID'), $id))->withErrors($v)->withInput();
                }
            } elseif ($user->hasAccess('user')) {

                $input = Input::all();

                $v = FormField::validate($input);

                if ($v->passes()) {

                    $data['name'] = Input::get('name');
                    $data['defaultValue'] = Input::get('defaultValue');
                    $data['size'] = Input::get('size');
                    $data['maxlength'] = Input::get('maxlength');
                    $data['mandatory'] = Input::get('mandatory');

                    FormField::find($id)->update(array('name' => $data['name'], 'mandatory' => $data['mandatory']));
                    FormFieldSpec::where('formFieldID', '=', $id)->update(array('defaultValue' => $input['defaultValue'], 'size' => $input['size'], 'maxlength' => $input['maxlength']));

                    session()->flash('success', 'The form field has been Updated');
                    return redirect('store/competitions/competitionfields/' . Input::get('formID'));

                } else {
                    return Redirect::route('store.editformfield', array(Input::get('formID'), $id))->withErrors($v)->withInput();
                }
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Add competion field
     * @param $formID
     */
    public function getAddCompetitionField($formID)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $data['typeOfField'] = array('text' => 'Text field', 'textarea' => 'Textarea', 'radio' => 'Radio button', 'checkbox' => 'Checkbox ', 'select' => 'Drop down list');
                $data['size'] = array_combine(range(30, 300), range(30, 300));
                $data['rows'] = array_combine(range(1, 20), range(1, 20));
                $data['cols'] = array_combine(range(15, 60), range(15, 60));
                $data['numRadioOptions'] = array_combine(range(1, 100), range(1, 100));
                $data['numCheckboxOptions'] = array_combine(range(1, 100), range(1, 100));
                $data['numSelectOptions'] = array_combine(range(1, 100), range(1, 100));
                $data['wrap'] = array('' => 'None', 'soft' => 'Soft', 'hard' => 'Hard');
                $data['formID'] = $formID;
                return view('shopmanagers/chainstores/competitions/addfield', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Post Form Field
     */
    public function postAddField()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $formID = Input::get('formID');

                $prevContentOrder = FormField::select('contentOrder')->where('formID', '=', $formID)->max('contentOrder');

                $formFieldData['formID'] = $formID;
                $formFieldData['name'] = Input::get('name');
                $formFieldData['typeOfField'] = Input::get('typeOfField');
                $formFieldData['contentOrder'] = $prevContentOrder + 1;
                $formFieldData['mandatory'] = Input::get('mandatory');

                //Insert Form Field
                $formFieldID = FormField::insertGetId($formFieldData);

                //Insert Form Field Specs
                $formFieldSpecData['formFieldID'] = $formFieldID;
                $formFieldSpecData['defaultValue'] = Input::get('defaultValue');
                $formFieldSpecData['maxlength'] = Input::get('maxlength');
                $formFieldSpecData['size'] = Input::get('size');
                $formFieldSpecData['rows'] = Input::get('rows');
                $formFieldSpecData['cols'] = Input::get('cols');
                $formFieldSpecData['wrap'] = Input::get('wrap');
                $formFieldSpecData['multiple'] = Input::get('multiple');

                FormFieldSpec::insert($formFieldSpecData);

                session()->flash('success', 'The field has been added successfully');
                return redirect('chainstores/competitions/fields/' . $formID);
            } elseif ($user->hasAccess('user')) {

                $formID = Input::get('formID');

                $prevContentOrder = FormField::select('contentOrder')->where('formID', '=', $formID)->max('contentOrder');

                $formFieldData['formID'] = $formID;
                $formFieldData['name'] = Input::get('name');
                $formFieldData['typeOfField'] = Input::get('typeOfField');
                $formFieldData['contentOrder'] = $prevContentOrder + 1;
                $formFieldData['mandatory'] = Input::get('mandatory');

                //Insert Form Field
                $formFieldID = FormField::insertGetId($formFieldData);

                //Insert Form Field Specs
                $formFieldSpecData['formFieldID'] = $formFieldID;
                $formFieldSpecData['defaultValue'] = Input::get('defaultValue');
                $formFieldSpecData['maxlength'] = Input::get('maxlength');
                $formFieldSpecData['size'] = Input::get('size');
                $formFieldSpecData['rows'] = Input::get('rows');
                $formFieldSpecData['cols'] = Input::get('cols');
                $formFieldSpecData['wrap'] = Input::get('wrap');
                $formFieldSpecData['multiple'] = Input::get('multiple');

                FormFieldSpec::insert($formFieldSpecData);

                session()->flash('success', 'The field has been added successfully');
                return redirect('store/competitions/competitionfields/' . $formID);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get Job Applicants
     */
    public function getJobApplicants($jobID)
    {
        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $applicantID = DB::table('jobApplications')->where('jobID', '=', $jobID)->pluck('jobApplicantID');
                $data['applicants'] = DB::table('jobApplicants')->whereIn('jobApplicantID', $applicantID)->orderBy('dateAdded', 'desc')->get();

                return view('shopmanagers/chainstores/jobs/applicants', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * View Job applicant
     * @param $id
     */
    public function getJobApplicant($id)
    {
        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {
                $data['applicant'] = JobApplicant::find($id);

                return view('shopmanagers.chainstores.jobs.applicant', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    public function getApplicantCv($cv)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $file = Config::get('download_path') . "/$cv";
                $headers = array(
                    'Content-Type: application/pdf',
                );
                return response()->download($file, 'filename.pdf', $headers);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Update User Profile
     * @param $id
     * @return \Illuminate\Contracts\View\View
     */
    public function getEditUserProfile($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $data['user'] = Sentinel::getUserProvider()->findById($id);
                $data['provinces'] = array('' => 'Select an existing province') + DB::table('provinces')->pluck('name', 'id');
                $data['cities'] = DB::table('adminUsers')->distinct('city')->orderBy('city')->pluck('city', 'city');

                return view('shopmanagers/chainstores/editprofile', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Post Edit User Profile
     * @param $id
     */
    public function postEditProfile($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $input = Input::all();

                $v = User::validate($input);

                if ($v->passes()) {
                    MGCrud::UpdateUserProfile($user);

                    session()->flash('success', 'Your details have been updated !!');
                    return redirect('/chainshop');
                } else {
                    session()->flash('error', 'Please review the form!');
                    return redirect(route('chainshop.editprofile', $id))->withErrors($v)->withInput();
                }
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Update User password
     * @param $id
     */
    public function getUpdatePassword($id)
    {
        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $data['user'] = $user;
                return view('shopmanagers/chainstores/updatepassword', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Post Update Password
     * @param $id
     */
    public function postUpdatePassword($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $input = array(
                    'old_pass' => Input::get('old_pass'),
                    'new_pass' => Input::get('new_pass'),
                    'new_pass_confirmation' => Input::get('new_pass_confirmation')
                );

                // Set Validation Rules
                $rules = array(
                    'old_pass' => 'required',
                    'new_pass' => 'required|min:6|alpha_num|confirmed',
                    'new_pass_confirmation' => 'required|alpha_dash'
                );

                //Run input validation
                $v = Validator::make($input, $rules);

                if ($v->passes()) {

                    if (Hash::check(Input::get('old_pass'), $user->password)) {

                        $user->password = Input::get('new_pass');

                        if ($user->save()) {
                            session()->flash('success', 'Your password has been updated.');
                            return redirect('/chainshop');
                        } else {
                            session()->flash('error', 'Your password could not be updated.');
                            return redirect('/chainshop');
                        }

                    } else {
                        session()->flash('error', 'The password you enterd does not match your current password.');
                        return redirect('users/updatepassword/' . $user->id)->withErrors($v)->withInput();
                    }

                } else {
                    session()->flash('error', 'Please review the form!');
                    return redirect(route('chainshop.updatepassword', $id))->withErrors($v)->withInput();
                }

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get Store promotions
     */
    public function getStorePromotions()
    {

        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();

            if ($user->hasAccess('user') && session()->has('activeShopID')) {
                $shopID = session()->get('activeShopID');
                $data['shop'] = Shop::find($shopID);
                $data['promotions'] = $data['shop']->promotions;

                return view('shopmanagers/shopuser/promotions/index', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get edit promotion page
     */
    public function getEditShopPromotion($id)
    {

        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();

            if ($user->hasAccess('user')) {

                $data['promotion'] = Promotion::find($id);

                return view('shopmanagers/shopuser/promotions/edit', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }

        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Add Store Promotion
     */
    public function getCreateStorePromotion()
    {

        if (Sentinel::check()) {
            // Get the current active/logged in user
            $user = Sentinel::getUser();

            if ($user->hasAccess('user') && session()->has('activeShopID')) {

                $data['shop'] = Shop::find(session()->get('activeShopID'));

                return view('shopmanagers/shopuser/promotions/create', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get shopping mall for selected store
     */
    public function getMallByShop($id)
    {

        return Shop::find($id)->mallID;
    }

    /**
     * Filter jobs by mall
     * @param $mallID
     * @return mixed
     */
    public function getFilterJobByMall($mallID)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $activeChain = session()->get('activeChain');
                $shops = Shop::where('name', '=', $activeChain)->pluck('shopMGID');

                $shopMalls = Shop::where('name', '=', $activeChain)->pluck('mallID');
                $data['malls'] = array('' => 'View all malls...') + ShoppingMall::whereIn('mallID', $shopMalls)->orderBy('name')->pluck('name', 'mallID');
                $data['mallID'] = $mallID;

                $data['currentJobs'] = Job::select(DB::raw('jobs.*,shoppingMall.name as mall,shop.name,SUM(CASE WHEN jobApplications.jobID = jobs.jobID THEN 1 ELSE 0 END) as applicants'))
                    ->leftjoin('shop', 'shop.shopMGID', '=', 'jobs.shopMGID')
                    ->leftjoin('jobApplications', 'jobApplications.jobID', '=', 'jobs.jobID')
                    ->join('shoppingMall', 'shoppingMall.mallID', '=', 'jobs.mallID')
                    ->leftjoin('jobApplicants', 'jobApplicants.jobApplicantID', '=', 'jobApplications.jobApplicantID')
                    ->whereIn('jobs.shopMGID', $shops)
                    ->where('jobs.mallID', '=', $mallID)
                    ->where('jobs.endDate', '>=', date("Y-m-d H:i:s"))
                    ->groupBy('jobs.jobID')
                    ->orderBy('jobs.endDate', 'desc')
                    ->get();

                return view('shopmanagers/chainstores/jobs/malljobs', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * List job for selected mall
     */
    public function getMallJobs($mallID)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                return view('shopmanagers/chainstores/jobs/malljobs');
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * List users added by Logged In User
     */
    public function getListUsers()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            $data['user_id'] = $user->user_id;
            $shopID = session()->get('activeShopID');
            $data['users'] = User::whereHas('shops', function ($q) use ($shopID) {
                $q->where('shop_id', '=', $shopID);
            })->orderBy('email')->get();

            if ($user->hasAccess('user') || $user->hasAccess('manager')) {

                return view('shopmanagers/shopuser/listusers', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

    /**
     * Manager Edit User
     */
    public function getEditUser($id)
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('user') || $user->hasAccess('manager')) {

                $data['user'] = User::find($id);
                $data['provinces'] = array('' => 'Select an existing province') + DB::table('provinces')->pluck('name', 'id');
                $data['cities'] = ShoppingMall::distinct('city')->orderBy('city')->pluck('city', 'city');

                return view('/shopmanagers/shopuser/edituser', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Get Users for logged in chain store user
     */
    public function getChainUserList()
    {

        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();

            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $data['users'] = User::where('parent_user_id', '=', $user->id)->orderBy('email')->select('email', 'first_name', 'last_name', 'admin_level', 'created_at', 'activated', 'id')->get();

                return view('/shopmanagers/chainstores/listusers', $data);

            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }

    }

    /**
     * Edit chain store User
     */
    public function getEditChainUser($id)
    {
        if (Sentinel::check()) {
            // Find the user using the user id
            $user = Sentinel::getUser();
            if ($user->hasAccess('manager') || $user->hasAccess('user')) {

                $data['user'] = User::find($id);
                $data['provinces'] = array('' => 'Select an existing province') + DB::table('provinces')->pluck('name', 'id');
                $data['cities'] = ShoppingMall::distinct('city')->orderBy('city')->pluck('city', 'city');

                return view('/shopmanagers/chainstores/edituser', $data);
            } else {
                session()->flash('error', 'You\'re not allowed to be here!');
                return redirect('/');
            }
        } else {
            session()->flush();
            session()->flash('error', 'You\'re not logged in!');
            return redirect('/login');
        }
    }

}
