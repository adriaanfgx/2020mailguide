<?php

namespace App\Http\Controllers;

use App\Category;
use App\ShopCategory;
use App\SubCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('sentinel.auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        return view('admin.categories.index');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id) {
        $data['category'] = Category::find($id);
        $data['heading'] = 'Edit Category';
        return view('admin.categories.edit')->with($data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postUpdate(Request $request, $id) {

        $rules = [
            'name' => 'required'
        ];

        $form = $request->except('_token');
        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {
            $category = Category::find($id);
            $category->name = $request->get('name');

            if ($category->save()) {
                session()->flash('success', 'Event details have been successfully updated');
                return redirect('/admin/categories');
            } else {
                session()->flash('error', 'There was an issue saving your data');
                return back()->withErrors($validation);
            }
        } else {
            session()->flash('error', 'There are some errors, please review the form');
            return back()->withErrors($validation);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate() {
        $data['heading'] = 'Add a new Category';
        return view('admin.categories.edit')->with($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCreate(Request $request) {
        $rules = [
            'name' => 'required'
        ];

        $form = $request->except('_token');
        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {
            if (Category::create($form)) {
                session()->flash('success', 'Category details have been successfully saved');
                return redirect('/admin/categories');
            } else {
                session()->flash('error', 'There was an issue saving your data');
                return back()->withErrors($validation);
            }
        } else {
            session()->flash('error', 'There are some errors, please review the form');
            return back()->withErrors($validation);
        }
    }

    /**
     * @param $id
     * @return string
     */
    public function postDelete($id) {
        if (Category::find($id)->delete()) {
            return 'success';
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function subCategories($id) {
        $data['category'] = Category::find($id);
        return view('admin.categories.sub_categories')->with($data);

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function getSubCategories($id) {
        $subCategories = SubCategory::where('category_id', $id)->orderBy('name')->get();
        return response($subCategories)->header('Content-Type', 'application/json');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addSubCategory($id) {
        $category = Category::find($id);

        $data['category'] = $category;
        $data['heading'] = 'Add Sub Category for >> ' . $category->name;

        return view('admin.categories.add_sub_category')->with($data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postCreateCategory(Request $request) {

        $rules = [
            'name' => 'required'
        ];

        $form = $request->except('_token');
        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {

            if (SubCategory::create($form)) {
                session()->flash('success', 'Sub-category details have been successfully saved');
                return redirect('/admin/sub-categories/' . $request->get('category_id'));
            } else {
                session()->flash('error', 'There was an issue saving your data');
                return back()->withErrors($validation);
            }
        } else {
            session()->flash('error', 'There are some errors, please review the form');
            return back()->withErrors($validation);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editSubCategory($id) {

        $data['subCategory'] = SubCategory::find($id);
        $data['category'] = Category::find($id);
        $data['heading'] = 'Edit Sub-category';
        return view('admin.categories.add_sub_category')->with($data);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function updateSubCategory($id, Request $request) {

        $rules = [
            'name' => 'required'
        ];

        $form = $request->except('_token');
        $validation = Validator::make($form, $rules);

        if ($validation->passes()) {
            $subCategory = SubCategory::find($id);
            $subCategory->name = $request->get('name');

            if ($subCategory->save()) {
                session()->flash('success', 'Sub-category details have been successfully updated');
                return redirect('/admin/sub-categories/' . $subCategory->category_id);
            } else {
                session()->flash('error', 'There was an issue saving your data');
                return back()->withErrors($validation);
            }
        } else {
            session()->flash('error', 'There are some errors, please review the form');
            return back()->withErrors($validation);
        }
    }

    public function deleteSubCategory($id) {
        if (SubCategory::find($id)->delete()) {
            return 'success';
        }
    }
}
