<?php

namespace App\Http\Controllers;

use App\Competition;
use App\CompetitionWinners;
use App\FormFeedback;
use App\FormField;
use App\FormFieldElements;
use App\FormFieldSpec;
use App\FormRequest;
use App\MGForm;
use App\Shop;
use App\ShoppingMall;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use PHPUnit\Framework\Constraint\IsFalse;

class CompetitionsController extends Controller
{
    public function __construct()
    {
        $this->middleware(['sentinel.auth', 'is_admin']);
    }

    public function getCompetitions()
    {
        return view('admin.competitions.list');
    }

    public function getCompetitionResponses($competitionId)
    {
        $data['competitionId'] = $competitionId;

        return view('admin.competitions.responses', $data);
    }

    public function getCompetitionResponseAnswers()
    {
        return view('admin.competitions.answers');
    }

    public function getCompetitionChooseWinner($formFeedbackId)
    {
        $formFeedback = FormFeedback::find($formFeedbackId);

        $formId = $formFeedback->formID;

        $winner = array("formID" => $formId, 'feedbackID' => $formFeedbackId, "prizePosition" => 1);

        $winnerId = CompetitionWinners::create($winner);

        if ($winnerId) {
            session()->flash('success', 'Winner was chosen successfully!');
        } else {
            session()->flash('error', 'Could not select winner!');
        }
        return Redirect::route('competitions.responses.answers.list', $formFeedback);
    }

    public function getCompetitionRemoveWinner($competitionWinnerID)
    {
        $winner = CompetitionWinners::find($competitionWinnerID);

        $formFeedbackId = $winner->feedbackID;

        if ($winner->delete()) {
            session()->flash('success', 'The winner has been removed!');
        } else {
            session()->flash('error', 'The winner could not be removed!');
        }

        return Redirect::route('competitions.responses.answers.list', $formFeedbackId);
    }

    public function getCompetitionChooseRandomWinner($formFeedbackId)
    {
        $winningResponse = FormFeedback::where("formID", "=", $formFeedbackId)->orderBy(DB::raw('RAND()'))->first();

        $winner = array("formID" => $formFeedbackId, 'feedbackID' => $winningResponse->feedbackID, "prizePosition" => 1);

        $winnerID = CompetitionWinners::create($winner);

        if ($winnerID) {
            session()->flash('success', 'Winner was chosen successfully!');
        } else {
            session()->flash('error', 'Could not select winner!');
        }

        return Redirect::route('competitions.responses.list', $formFeedbackId);
    }

    public function getCompetitionResponsesDownloadCSV($formFeedbackId, $page = 1, $perPage = 500)
    {
        $responsesCount = MGForm::find($formFeedbackId)->formFeedback->count();

        $form = MGForm::with(['formFeedback' => function ($query) use ($page, $perPage) {
            $query->skip((($page - 1) * $perPage))->take($perPage);
        }])->find($formFeedbackId);

        $responses = $form->formFeedback;

        $pagesCount = ceil($responsesCount / $perPage);

        $nextPage = $page + 1;

        $formFields = $form->formFields;

        $formFieldIDs = [];

        foreach ($formFields as $formField) {
            $formFieldIDs[] = $formField->formFieldID;
            $firstRow[] = trim($formField->name);
        }

        $firstRow[] = 'Date';

        $uploadDate = date("YmdHis");

        $destinationPath = base_path() . '/uploadimages/';

        $filename = 'comp_responses_' . $formFeedbackId . '_' . $uploadDate . '.csv';

        if (session()->has('filename_' . $formFeedbackId)) {
            $filename = session()->get('filename_' . $formFeedbackId);
        } else {
            session()->put('filename_' . $formFeedbackId, $filename);
        }

        if (!file_exists($destinationPath . $filename)) {
            $file = fopen($destinationPath . $filename, 'wb');
            fputcsv($file, $firstRow);
        } else {
            $file = fopen($destinationPath . $filename, 'a');
        }

        foreach ($responses as $response) {
            $row = [];

            $responseAnswers = $response->formFeedbackValues()->get(['value', 'formFieldID'])->keyBy('formFieldID')->toArray();;

            foreach ($formFieldIDs as $id) {
                if (isset($responseAnswers[$id])) {
                    $row[] = trim($responseAnswers[$id]['value']);
                } else {
                    $row[] = '';
                }
            }

            $row[] = date('l, jS F Y', strtotime($response->dateAdded));

            fputcsv($file, $row);
        }

        if ($nextPage > $pagesCount) {

            session()->forget('filename_' . $formFeedbackId);

            $download_filename = Str::slug($form->subject, '_') . '_responses_' . date('Y-m-d_H.i.s') . '.csv';

            $headers = [
                'Content-Type' => 'text/csv',
                'Content-Disposition' => 'attachment; filename="' . $download_filename . '"',
            ];

            return Response::download($destinationPath . $filename, $download_filename, $headers);

        } else {
            echo 'Exporting page ' . $page . ' of ' . $pagesCount . ' for download.<br /><br />';
            echo 'Please don\'t close this window until your download is ready...<br /><br />';

            return redirect(route('competitions.responses.download-csv', ['responseId' => $formFeedbackId, 'page' => $nextPage, 'perPage' => $perPage]));
        }
    }

    public function getCreateCompetition($mallId = null)
    {
        $data['heading'] = 'CREATE A COMPETITION';

        if ($mallId != null) {
            $data['mallID'] = $mallId;
        }

        $malls = ShoppingMall::where('activate', '=', 'Y')->orderBy('name')->get(['name', 'mallID']);

        $data['malls'] = $malls;

        $data['formFieldArray'] = MGForm::defaultFields();

        return view('admin.competitions.create', $data);
    }

    /**
     * @param $formID
     * @return mixed
     */
    public function getEditCompetition($formID)
    {
        //Get the current user's id.
        $user = Sentinel::getUser();
        //Do they have admin access?
        if ($user->hasAccess('admin')) {
            $data['heading'] = "EDIT A COMPETITION";

            $data['malls'] = [
                "" => "Select a Mall"
            ];
            $data['moves'] = getMoveList();
            $data['competition'] = MGForm::find($formID);
            $malls = ShoppingMall::where('activate', '=', 'Y')->orderBy('name')->get(['name', 'mallID']);
            $data['mallID'] = $data['competition']->formRequest->mallID;
            $data['malls'] = $malls;
            $request = $data['competition']->formRequest;

            //$url=Config::get('app.url');
            if ($data['competition']->thumbnail) {
                $data['imagePath'] = '/uploadimages/' . $data['competition']->thumbnail;
            }
            if ($data['competition']->TsandCsFile) {
                $data['filePath'] = '/uploadimages/mall_' . $request->mallID . '/' . $data['competition']->TsandCsFile;

            }
            session()->flash('mallID', $request->mallID);
            //form fields array

            return view('admin.competitions.create', $data);
        } else {
            session()->flash('error', 'You do not have the required permissions to access that page!');
            return redirect('login');
        }
    }

    /**
     * @param $formID
     * @return mixed
     */
    public function postUpdateCompetition($formID)
    {
        $input = Input::all();

        unset($input['image1']);

        unset($input['TsandCsFile']);

        $user = Sentinel::getUser();
        $userID = $user->userID;
        // Set Validation Rules
        if ($user->hasAccess('admin')) {
            $v = MGForm::validateAdmin($input);
        } else {
            $v = MGForm::validate($input);
        }

        $competition = MGForm::find($formID);
        if ($v->fails()) {
            $response_values = array(
                'validation_failed' => 1,
                'errors' => $v->errors()->toArray()
            );
            session()->flash('error', 'Please review the form!');

            return back()->withErrors($v)->withInput();
        } else {
            $mallID = Input::get('mallID');
            $data = Input::all();
            $data['endDate'] = Carbon::parse($data['endDate'])->toDateString() . ' 23:59:59';
            $data['formType'] = 'Competition';
            $displayFor = Input::get('displayFor');
            $data['displayFor'] = $displayFor ? $displayFor : 'M';
            if (isset($data['facebook'])) {
                //$competition=MGForm::find($formID);
                $data['activated'] = $data['facebook'] == "Y" ? 'N' : $data['activated'];
                $data['imageFB'] = isset($competition->image) ? $competition->image : "";
            }
            $thumbMaxWidth = 150;
            $largeMaxWidth = 450;

            $uploadDate = date("YmdHis");

            //get mallID
            if (!$mallID) {
                $mallID = FormRequest::where('formID', $formID)->pluck('mallID');
            }

            if (Input::hasFile('image1')) {
                $file1 = Input::file('image1');
                $destinationPath = Config::get('image_upload_path');

                $image_array = uploadImage($file1, $destinationPath, $thumbMaxWidth);

                if (isset($data['facebook']) && $data['facebook'] == "Y") {
                    $data['imageFB'] = $image_array['largeImage'];
                }
                $data['image'] = $image_array['largeImage'];
                $data['thumbnail'] = $image_array['thumbnail'];
            }
            if (Input::hasFile('TsandCsFile')) {
                //die("arg");
                $file2 = Input::file('TsandCsFile');
                //ALTER TABLE `forms` ADD COLUMN `TsandCsFile` VARCHAR(255) NULL AFTER `ogDescription`;
                $destinationPath = Config::get('image_upload_path') . '/mall_' . $mallID;
                //$destinationPath = public_path().'\uploadimages\mall_'.$data['mallID'];
                $TsandCsFile = uploadFile($file2, $destinationPath);

                if ($TsandCsFile != "") {
                    $data['TsandCsFile'] = $TsandCsFile;
                } else {
                    unset($data['TsandCsFile']);
                }
            } else {
                unset($data['TsandCsFile']);
            }
            $competitionID = $competition->update($data);

            if ($user->hasAccess('admin')) {
                $formReqData['mallID'] = Input::get('mallID');
                $formReqData['shopMGID'] = Input::get('shopMGID');
                //Insert Into formRequest
                $formRequest = $competition->formRequest;
                $formRequest->update($formReqData);
            }

            $data['competition'] = $competition;

            if (Input::has('mallID')) {
                $mallID = Input::get('mallID');
            } else {
                if (session()->has('activeMallID')) {
                    $mallID = session()->get('activeMallID');
                } elseif (session()->has('activeShopID')) {
                    $shop = Shop::find(session()->get('activeShopID'));
                    $mallID = $shop->mallID;
                }
            }
            $response_values = array(
                'validation_passed' => 1,
                'errors' => array(),
                'status' => 'success',
                'mallID' => $mallID,
                'id' => $competition->formID
            );

            if ($user->hasAccess('admin')) {
                session()->put('success', 'Competition updated successfully.');
                return redirect(route('competitions.list', ['mallID' => $mallID]));
            } elseif (!is_array($mallID)) {
                return Redirect::to('/competitions/list');
            } else {
                return Redirect::to('portal/mcomps');
            }
        }

    }


    public function postCreateCompetition(Request $request)
    {
        //        print_r(json_decode(Input::get('formCustomFields')));exit();
        //if user is logged in
        $user = Sentinel::getUser();

        //Do they have admin access?
        if ($user->hasAccess('admin') || $user->hasAccess('manager') || $user->hasAccess('user') || $user->hasAccess('independent')) {
            $input = Input::all();

            $input['endDate'] = Carbon::parse($input['endDate'])->toDateString() . ' 23:59:59';

            if (Input::hasFile('image1')) {
                $input['image1'] = Input::file('image1');
            } else if (isset($input['image1'])) {
                unset($input['image1']);
            }
            if (Input::hasFile('TsandCsFile')) {
                $input['TsandCsFile'] = Input::file('TsandCsFile');
            } else if (isset($input['TsandCsFile'])) {
                unset($input['TsandCsFile']);
            }

            // Set Validation Rules
            if ($user->hasAccess('admin')) {
                $v = MGForm::validateAdmin($input);
            } else {
                $v = MGForm::validate($input);
            }
            if ($v->fails()) {
                session()->flash('error', 'Please review the form');

                return back()->withErrors($v)->withInput();
            } else {
//					$user = Sentry::getUser();
                $userID = $user->userID;

                if (Input::has('mallID')) {
                    $mallID = Input::get('mallID');
                } else {
                    if (session()->has('activeMallID')) {
                        $mallID = session()->get('activeMallID');
                    }
                    if (session()->has('activeMallID')) {
                        $mallID = session()->get('activeMallID');
                    } elseif (session()->has('activeShopID')) {
                        $shop = Shop::find(session()->get('activeShopID'));
                        $mallID = $shop->mallID;
                    }
                }

                $data = Input::all();
                $data['endDate'] = Carbon::parse($input['endDate'])->toDateString() . ' 23:59:59';
                $data['userID'] = $user->id;
                $data['pageID'] = 0;
                $data['dateAdded'] = time();
                $data['formType'] = 'Competition';
                $data['displayFor'] = Input::get('displayFor', 'M');

                if (isset($data['facebook'])) {
                    $data['activated'] = $data['facebook'] == "Y" ? 'N' : $data['activated'];
                }
                $thumbMaxWidth = 150;
                $largeMaxWidth = 450;

                $uploadDate = date("YmdHis");

                $data['imageFB'] = '';
                if (Input::hasFile('image1')) {
                    $file1 = Input::file('image1');
                    $destinationPath = base_path() . '/uploadimages/mall_' . $mallID;

                    $image_array = uploadImage($file1, $destinationPath, $thumbMaxWidth);

                    $data['image'] = $image_array['largeImage'];
                    if (isset($data['facebook']) && $data['facebook'] == "Y") {
                        $data['imageFB'] = $image_array['largeImage'];
                    }
                    if (isset($image_array['thumbnail'])) {
                        $data['thumbnail'] = $image_array['thumbnail'];
                    }
                }
                if (Input::hasFile('TsandCsFile')) {
                    $file2 = Input::file('TsandCsFile');
                    $destinationPath = base_path() . '/uploadimages/mall_' . $mallID;

                    $data['TsandCsFile'] = uploadImage($file2, $destinationPath);
                }

                $competition = MGForm::create($data);

                $response_values = array(
                    'validation_passed' => 1,
                    'errors' => array(),
                    'status' => 'success',
                    'mallID' => $mallID,
                    'id' => $competition->formID
                );

                if ($competition) {
                    if (is_array($mallID) && session()->has('activeChain')) {
                        $shopMGIDs = Shop::where('name', '=', session()->get('activeChain'))->lists('shopMGID', 'mallID');

                        foreach ($mallID as $cur_mallID) {
                            if (isset($shopMGIDs[$cur_mallID])) {
                                $formReqData['shopMGID'] = $shopMGIDs[$cur_mallID];
                                $formReqData['formID'] = $competition->formID;
                                $formReqData['userID'] = $userID;
                                $formReqData['mallID'] = $cur_mallID;
                                $formReqData['dateAdded'] = time();
                                $formReqData['display'] = 'Y';

                                $formRequest = FormRequest::create($formReqData);
                            }
                        }
                    } else {
                        $formReqData['formID'] = $competition->formID;
                        $formReqData['userID'] = $userID;
                        $formReqData['mallID'] = $mallID;
                        if (session()->has('activeShopID')) {
                            $formReqData['shopMGID'] = session()->get('activeShopID');
                            //dd(Session::get('activeShopID'));
                        } else {
                            $formReqData['shopMGID'] = Input::get('shopMGID');
                        }
                        $formReqData['dateAdded'] = Carbon::now()->format('Y-m-d H:i:s');
                        $formReqData['display'] = 'Y';

                        FormRequest::create($formReqData);

                    }
                    //Insert Into formRequest
                }

                //dd(json_decode($data['formCustomFields'], true)[0]);
                if (!$user->hasAccess('admin')) {
                    $formFieldArray = MGForm::defaultFields();
                } else {
                    $formFieldArray = $data['formCustomFields'];
                    //dd($formFieldArray);
                }
                //Insert into the table formFields

                foreach ($formFieldArray as $k => $val_array) {
                    if ($user->hasAccess('admin')) {
                        $val_array = json_decode($val_array, true);
                    }
                    //Insert into the table formFieldSpecs
                    $val_array['formID'] = $competition->formID;

                    $formField = FormField::create($val_array);

                    //Get latest Entry and insert into formFieldSpecs
                    $formFieldSpecData['formFieldID'] = $formField->formFieldID;
                    if (isset($val_array['size'])) {
                        $formFieldSpecData['size'] = $val_array['size'];
                    }
                    $formFieldSpecsData['defaultValue'] = isset($val_array['defaultValue']) ? $val_array['defaultValue'] : null;
                    $formFieldSpecsData['maxlength'] = isset($val_array['maxlength']) ? $val_array['maxlength'] : '';
                    $formFieldSpecsData['rows'] = isset($val_array['rows']) ? $val_array['rows'] : '';
                    $formFieldSpecsData['cols'] = isset($val_array['cols']) ? $val_array['cols'] : '';
                    $formFieldSpecsData['wrap'] = isset($val_array['wrap']) ? $val_array['wrap'] : '';
                    $formFieldSpecsData['multiple'] = isset($val_array['multiple']) ? $val_array['multiple'] : '';
                    $formFieldElem['multiple'] = isset($val_array['multiple']) ? $val_array['multiple'] : '';
                    if (isset($val_array['fileTypes'])) {
                        $formFieldSpecData['fileTypes'] = $val_array['fileTypes'];
                    }
                    //$formFieldSpecData['fileTypes'] = $val_array['fileTypes[]'];

                    if (isset($val_array['checkName'])) {
                        $names = explode(',', $val_array['checkName']);

                        foreach ($names as $index => $name) {
                            $formElemData['name'] = $name;
                            $formElemData['formFieldID'] = $formField->formFieldID;
                            $checkNum = $index + 1;
                            if (isset($val_array["checkedSelected" . $checkNum])) {
                                $formElemData['checkedSelected'] = 'Y';
                            } else {
                                $formElemData['checkedSelected'] = 'N';
                            }
                            $formElemData['contentOrder'] = $checkNum;
                            FormFieldElements::create($formElemData);

                        }
                    }

                    $formSpecs = FormFieldSpec::create($formFieldSpecData);
                }

                if ($user->hasAccess('admin')) {
                    session()->put('success', 'Competition created successfully');
                    return redirect(route('competitions.list', ['mallId' => $mallID]));
                } else {
                    return Redirect::route('competitions.fields', $competition->formID);
                }
            }
        } else {
            session()->flash('error', 'You do not have the required permissions to access that page!');
            return redirect('login');
        }
    }

    public function getDefaultFormFields()
    {
        $formFieldArray['formFields'] = MGForm::defaultFields();

        return Response::json($formFieldArray);
    }

    public function getResponseAnswers($formFeedbackId)
    {
        $response = FormFeedback::find($formFeedbackId);

        $data["fields"] = $response->formFeedbackValues;

        $data['winner'] = NULL;

        if (sizeof($response->competitionWinners) > 0) {
            $data['winner'] = $response->competitionWinners->first()->compWinnerID;
        }

        $data["form"] = MGForm::find($response->formID);

        $data["response"] = $response;

        return response($data)->header('Content-Type', 'application/json');
    }

    public function getResponses($competitionId)
    {
        $form = MGForm::with(['formFeedback'])->find($competitionId);

        $responses = $form->formFeedback;

        $returnData = [];
        foreach ($responses as $key => $respons) {
            $returnData[$key] = [
                'id' => $respons->feedbackID,
                'date_received' => $respons->dateAdded,
                'email_address' => '',
                'is_winner' => sizeof($respons->competitionWinners) > 0 ? True : False,
                'winner_id' => sizeof($respons->competitionWinners) > 0 ? $respons->competitionWinners->first()->compWinnerID: 0
            ];
            foreach ($respons->formFeedbackValues as $value) {
                if (Str::contains($value->fieldName, 'E-mail address')) {
                    $returnData[$key]['email_address'] = $value->value;
                }
            }
        }
        return response()->json($returnData);
    }

    public function deleteCompetitionResponse($responseId)
    {
        $response = FormFeedback::with('formFeedbackValues')->find($responseId);

        foreach ($response->formFeedbackValues as $answer) {
            $answer->delete();
        }

        if ($response->delete()) {
            session()->flash('success', 'The winner has been removed!');
        } else {
            session()->flash('error', 'The winner could not be removed!');
        }
    }

    public function deleteCompetition($competitionId)
    {
        FormRequest::where('formID', '=', $competitionId)->delete();

        MGForm::find($competitionId)->delete();

        session()->flash('success', 'The Competition has been deleted!');
    }

    public function getChangeOrder($fieldID, $move)
    {
        $field = FormField::find($fieldID);
        $form = MGForm::find($field->formID);
        $fields = $form->formFields->sortBy('contentOrder');
        $contentOrders = $fields->pluck('contentOrder', 'contentOrder');
        $curOrder = $field->contentOrder;
        if ($move == "up") {
            $move_to = $contentOrders[$field->contentOrder] - 1;
            $other_field = FormField::where('contentOrder', '=', $move_to)->where('formID', '=', $field->formID)->first();
            $saved = $other_field->update(array('contentOrder' => $curOrder));
        } elseif ($move == "down") {
            $move_to = $contentOrders[$field->contentOrder] + 1;
            $other_field = FormField::where('contentOrder', '=', $move_to)->where('formID', '=', $field->formID)->first();
            $saved = $other_field->update(array('contentOrder' => $curOrder));
        } elseif ($move == "top") {
            foreach ($fields as $a_field) {
                if ($a_field->formFieldID != $fieldID && $a_field->contentOrder < $field->contentOrder) {
                    $nr = $a_field->contentOrder;
                    $a_field->contentOrder = $nr + 1;
                    $a_field->save();
                }
            }
            $move_to = 1;
        } elseif ($move == "bottom") {
            $other_field = $fields->last();
            $move_to = $other_field->contentOrder;
            foreach ($fields as $a_field) {
                if ($a_field->formFieldID != $fieldID && $a_field->contentOrder > $field->contentOrder) {
                    $nr = $a_field->contentOrder;
                    $a_field->contentOrder = $nr - 1;
                    $a_field->save();
                }
            }
        }

        $saved = $field->update(array('contentOrder' => $move_to));

        if ($saved) {
            session()->flash('success', 'fields have been reordered!');
        } else {
            session()->flash('failed', 'Could not be saved!');
        }
        return $field->formID;
    }

    /**
     * @param $fieldID
     * @return mixed
     */
    public function getEditField($fieldID)
    {
        $data['heading'] = 'Edit Field';

        $field = FormField::find($fieldID);
        $form = MGForm::find($field->formID);

        $mallID = $form->formRequest->mallID;
        $data['prev_type'] = $field->typeOfField;
        $data['field'] = $field;
        $data['hasFields'] = ShoppingMall::hasFieldAccess($mallID);

        $data['fieldTypes'] = MGForm::fieldTypes($data['hasFields']);
        $data['fileTypes'] = MGForm::fileTypes();
        $data['size'] = array_combine(range(30, 300), range(30, 300));

        //amount of checkbox/radio button as well as amount of textbox rows for dropdown
        $data['rows'] = array_combine(range(1, 20), range(1, 20));
        $data['width'] = array_combine(range(15, 60), range(15, 60));
        $data['wrap'] = array('none' => 'None', 'soft' => 'Soft', 'hard' => 'Hard');
        $data['size'] = array_combine(range(30, 300), range(30, 300));

        $user = Sentinel::getUser();
        //var_dump($field);
        if ($user->hasAccess('admin')) {
            return view('admin.competitions.add_field', $data);
        } else {
            return view('portal/competitions/updatecompfield', $data)->nest('add_field', 'admins.competitions.add_field', $data);
        }
    }

    public function getRemoveWinner($compWinnerID)
    {
        //Get the current user's id.
        $user = Sentinel::getUser();

        //Do they have admin access?
        if ($user->hasAccess('admin')) {
            $winner = CompetitionWinners::find($compWinnerID);
            $formID = $winner->formID;
            $bool = $winner->delete();

            if ($bool) {
                //success!
                session()->flash('success', 'The winner has been removed!');
            } else {
                session()->flash('error', 'The winner could not be removed!');
            }

            return back();
        } else {
            session()->flash('error', 'You do not have the required permissions to access that page!');
            return redirect('/login');
        }

    }
}
