<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MallVisit extends Model
{
    protected $table = 'mall_visits';

    public function mall()
    {
        return $this->belongsTo('App\ShoppingMall');
    }
}
