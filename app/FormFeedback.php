<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class FormFeedback extends Model
{
    protected $table = 'formFeedback';

    public $timestamps = false;

    protected $primaryKey = 'feedbackID';

    protected $guarded = array('feedbackID');

    public function formFeedbackValues()
    {
        return $this->hasMany('App\FormFeedbackValues', 'feedbackID');
    }

    public function competitionWinners()
    {
        return $this->hasMany('App\CompetitionWinners', 'feedbackID');
    }
}
