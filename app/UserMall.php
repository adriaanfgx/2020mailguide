<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMall extends Model
{
    protected $table = 'user_malls';

    protected $primaryKey = 'id';

    public $timestamps = false;
}