<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Map extends Model
{
    protected $table = 'mapImage';
    protected $primaryKey = 'mapImageMGID';
    protected $fillable = ['imageSrc', 'location', 'mallID'];
    public $timestamps = false;
}
