<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class Shop extends Model
{

    protected $table = 'shop';
    public $timestamps = false;
    protected $primaryKey = 'shopMGID';
    protected $guarded = array('shopMGID');

    public static function validate($input)
    {

        $rules = array(
            'mallID' => 'sometimes|required',
            'name' => 'required',
            'email' => 'Between:3,64|email',
            'ownerEmail' => 'Between:3,64|email',
            'managerEmail' => 'Between:3,64|email',
            'managerEmail2' => 'Between:3,64|email',
            'managerEmail3' => 'Between:3,64|email',
            'headOfficeEmail' => 'Between:3,64|email',
            'financialEmail' => 'Between:3,64|email',
            'opsManagerEmail' => 'Between:3,64|email',
            'areaManagerEmail' => 'Between:3,64|email',
            'emergencyEmail' => 'Between:3,64|email',
            'emergencyEmail2' => 'Between:3,64|email',
            'url' => 'url',
            'logo' => 'mimes:jpeg,png,gif|image',
            'image1' => 'mimes:jpeg,png,gif|image',
            'image2' => 'mimes:jpeg,png,gif|image',
            'image3' => 'mimes:jpeg,png,gif|image',
            'image4' => 'mimes:jpeg,png,gif|image',
            'image5' => 'mimes:jpeg,png,gif|image',
            'image6' => 'mimes:jpeg,png,gif|image',
            'image7' => 'mimes:jpeg,png,gif|image',
            'image8' => 'mimes:jpeg,png,gif|image',
            'image9' => 'mimes:jpeg,png,gif|image',
            'image10' => 'mimes:jpeg,png,gif|image'
        );

        return Validator::make($input, $rules);
    }

    public function mall()
    {
        return $this->belongsTo('App\ShoppingMall', 'mallID');
    }

    public function competitions()
    {
        return $this->belongsToMany('App\MGForm', 'formRequest', 'shopMGID', 'formID')->withPivot('userID', 'mallID', 'dateAdded', 'display');
    }

    public function curCompetitions()
    {
        return $this->belongsToMany('App\MGForm', 'formRequest', 'shopMGID', 'formID')->withPivot('userID', 'mallID', 'dateAdded', 'display')->where('endDate', '>=', date('Y-m-d H:i:s'));
    }

    public function pastCompetitions()
    {
        return $this->belongsToMany('App\MGForm', 'formRequest', 'shopMGID', 'formID')->withPivot('userID', 'mallID', 'dateAdded', 'display')->where('endDate', '<', date('Y-m-d H:i:s'));
    }

    public function orderPromotions()
    {
        return $this->hasMany('App\Promotion', 'shopMGID')->orderBy("endDate", "desc");
    }

    public function promotions()
    {
        return $this->hasMany('App\Promotion', 'shopMGID');
    }

    public function shopCategory()
    {
        return $this->belongsTo('App\ShopCategory', 'category_id')->where('parent_id', 0);
    }

    public function shopSubCategory()
    {
        return $this->belongsTo('App\ShopCategory', 'subcategory_id')->where('parent_id', '!=', 0);
    }

    public function curPromotions()
    {
        return $this->hasMany('App\Promotion', 'shopMGID')->where('endDate', '>=', date('Y-m-d H:i:s'));
    }

    public function pastPromotions()
    {
        return $this->hasMany('App\Promotion', 'shopMGID')->where('endDate', '<', date('Y-m-d H:i:s'));
    }

    public function jobs()
    {
        return $this->hasMany('App\Job', 'shopMGID', 'shopMGID');
    }

    public function curJobs()
    {
        return $this->hasMany('App\Job', 'shopMGID', 'shopMGID')->where('display', '=', 'Y')->where('endDate', '>', date('Y-m-d'));
    }

    public function events()
    {
        return $this->hasMany('MGEvent', 'shopMGID', 'shopMGID');
    }

    public function curEvents()
    {
        return $this->hasMany('MGEvent', 'shopMGID', 'shopMGID')->where('display', '=', 'Y')->where('endDate', '>=', date('Y-m-d'));
    }

    public function curExhibitions()
    {
        return $this->hasMany('Exhibition', 'shopMGID', 'shopMGID')->where('display', '=', 'Y')->where('endDate', '>=', date('Y-m-d'));
    }

    public function scopeDisplay($query)
    {
        return $query->where('display', '=', 'Y');
    }

    public function generic()
    {

        return $this->hasOne('GenericShop', 'name');
    }

    public function giftIdeas()
    {

        return $this->hasMany('App\GiftIdea', 'shopMGID');
    }

    public function user()
    {
        return $this->belongsToMany('App\User', 'user_shops', 'shop_id', 'user_id');
    }

    public function brands()
    {
        return $this->belongsToMany('App\Brand', 'shop_brands', 'shop_id', 'brand_id');
    }

    public function downloads()
    {
        return $this->hasMany('App\Shopfile', 'shop_id');
    }
}
