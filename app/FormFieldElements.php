<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FormFieldElements extends Model
{
    protected $table = 'formFieldElements';

    public $timestamps = false;
    protected $primaryKey = 'formElementID';
    protected $guarded = array('formElementID');

    public function scopeOrdered($query)
    {
        return $query->orderBy('dateAdded', 'desc');
    }
}