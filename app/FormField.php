<?php
namespace App;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Database\Eloquent\Model;

class FormField extends Model {

    protected $table = 'formFields';

    public $timestamps = false;
    protected $primaryKey = 'formFieldID';
    protected $fillable = array('formID','name','typeOfField','mandatory','contentOrder','emailField');

    public static function validate( $input ){

        $rules = array(
            'name' => 'required'
        );

        return Validator::make( $input, $rules );
    }
    public function formFieldElements()
    {
        return $this->hasMany('App\FormFieldElements','formFieldID')->orderBy('contentOrder');
    }
    public function formFieldSelectedElements()
    {
        return $this->hasMany('App\FormFieldElements','formFieldID')->where('checkedSelected','=','Y')->orderBy('contentOrder');
    }
    public function formFieldSpecs()
    {
        return $this->hasOne('App\FormFieldSpec','formFieldID');
    }
    public function form()
    {
        return $this->hasOne('App\MGForm','formID');
    }
    public function getFieldByOrder($orderNum)
    {
        return $this->where('contentOrder','=',$orderNum);
    }


}
