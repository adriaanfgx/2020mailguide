<?php
/**
 * Created by JetBrains PhpStorm.
 * User: likho
 * Date: 2014/08/05
 * Time: 11:27 AM
 * To change this template use File | Settings | File Templates.
 */
namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsletterMovie extends Model {
    protected $table = 'newslettersMovies';
}
