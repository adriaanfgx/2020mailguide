$(document).ready(function(){

    $(".go_back").click(function(){
        goBack();
    });
    $(".byCinema").change(function(e){

        if( $(this).val() === ''){
            return;

        }else{

            var mallToView = $(this).val();
            var url = '/movies/mallmovies/'+mallToView;

            window.location.assign(url);

        }
    });

    $(".byMovie").change(function(){
        if( $(this).val() === ''){
            return;
        }else{
            var movieID = $(this).val();
            var movieName = $('.byMovie option:selected').text();
            var name = movieName.replace(/ /g, '-');
            window.location.href = 'movies/moreinfo/'+movieID+'/'+encodeURI(name);
        }

    });

});

function goBack() {
    window.history.back();
}