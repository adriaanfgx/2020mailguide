new Vue({
    el: "#mall_events",
    data: {
        fields: [
            {
                'key': 'name',
                'label': 'Event Title',
            },
            {
                'key': 'start_date',
                'label': 'Start Date',
            },
            {
                'key': 'end_date',
                'label': 'End Date',
            },
            {
                'key': 'category',
                'label': 'Category'
            },
            {
                'key': 'venue',
                'label': 'Venue'
            },
            {
                'key': 'display',
                'label': 'Display'
            },
            {
                'key': 'actions',
                'label': ''
            }
        ],
        items: [],
        filter: null,
        totalRows: 1,
        currentPage: 1,
        perPage: 10,
        model: '',
        value: '',
        mall: null,
        malls: [],
        events: [],
        isBusy: false,
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        fetchMallEvents: function () {

            var vm = this;

            this.toggleBusy()

            var mallId = window.location.pathname.split('/').pop()

            url = '/admin/events/get-backend/' + mallId
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        data = response.data;
                        console.log("Data us " + data);
                        if (data.hasOwnProperty('events')) {
                            vm.events = data.events
                            vm.totalRows = vm.events.length
                        }
                    }
                })
        },
        editEventUrl: function (item) {
            return '/admin/events/edit/' + item.id
        },
        deleteEventUrl: function (item) {
            let vm = this;
            let url = '/admin/events/delete/' + item.id
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        window.location.reload();
                    }
                })
        },
        confirmRemove(item) {
            if (confirm('Are you sure you want to remove item?')) {
                this.deleteEventUrl(item)
            } else  {
                return false;
            }
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    mounted: function () {
        this.fetchMallEvents()
    }
})
