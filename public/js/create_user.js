new Vue({
    el: "#create_user",
    data: {
        mallDropdown: []
    },
    methods: {
        provinceChange: function (province) {
            console.log('province changed....')
            let provinceId = province.target.value

            axios
                .get('/cities/' + provinceId)
                .then(function (response) {
                    if (response.status === 200) {
                        let cities = response.data

                        $('#city').children('option:not(:first)').remove();

                        $.each(cities, function (index, city) {
                            $('#city').append($('<option>', {
                                value: city.id,
                                text: city.name
                            }));
                        });
                    }
                })
        }
    },
    mounted: function () {
    }
});
