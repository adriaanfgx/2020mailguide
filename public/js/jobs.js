new Vue({
    el: "#jobs",
    data: {
        fields: [
            {
                'key': 'province',
                'label': 'Province',
            },
            {
                'key': 'city',
                'label': 'City',
            },
            {
                'key': 'name',
                'label': 'Mall',
            },
            {
                'key': 'category',
                'label': 'Category'
            },
            {
                'key': 'jobs',
                'label': 'Job count'
            },
            {
                'key': 'facebook',
                'label': 'Linked to Facebook'
            },
            {
                'key': 'actions',
                'label': ''
            }
        ],
        items: [],
        filter: null,
        totalRows: 1,
        currentPage: 1,
        perPage: 10,
        model: '',
        value: '',
        province: null,
        provinces: [],
        mall: null,
        malls: [],
        jobs: [],
        isBusy: false,
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        fetchProvinceJobs: function () {

            var vm = this;

            this.toggleBusy()

            url = '/jobs/province/' + this.province
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        vm.jobs = response.data;
                        vm.totalRows = response.data.length
                        console.log(response.data)

                    }
                })
        },
        onProvinceChange: function (value) {
            this.fetchProvinceJobs()
        },
        onMallChange: function (mallId) {
            window.location.href = '/admin/jobs/mall/' + mallId
        },
        fetchProvinces: function () {
            var vm = this;

            url = '/provinces';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        data.forEach(province => {
                            vm.provinces.push(
                                {
                                    'value': province.id,
                                    'text': province.name
                                }
                            )
                        });
                    }
                })
        },
        manageMallJobsUrl: function (item) {
            return '/admin/jobs/mall/' + item.mall_id
        },
        deleteExhibitionUrl: function (item) {
            return ''
        },
        fetchAutocompleteMalls: function () {
            var vm = this;

            url = '/malls/autocomplete';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {

                        data = response.data;
                        data.forEach(province => {
                            vm.malls.push(
                                {
                                    'value': province.id,
                                    'text': province.name
                                }
                            )
                        });
                    }
                })
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    mounted: function () {
        this.fetchProvinces()

        this.fetchAutocompleteMalls()
    }
})
