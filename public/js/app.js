
new Vue({
    el: "#malls",
    data: {
        fields: [
            {
                'key': 'name',
                'label': 'Mall',
                'sortable': false,
                'formatter': 'formatMallName'
            },
            {
                'key':'centre_manager_telephone',
                'label': 'Phone',
                'formatter': 'isNotEmpty'
            },
            {
                'key': 'website',
                'label': 'Website',
                'formatter': 'isNotEmpty'
            },
            {
                'key': 'coordinates',
                'label': 'Location',
                'formatter': 'hasCoordinates'
            }, 
            {
                'key': 'images',
                'label': 'Images',
                'formatter': 'formatImages'
            }, 
            {
                'key': 'last_update',
                'label': 'Last Updated',
                'formatter': 'formatLastUpdated'
            },
            {
                'key': 'facebook',
                'label': 'Facebook',
                'formatter': 'isYorN'
            },
            {
                'key': 'activate',
                'label': 'Active',
                'formatter': 'isYorN'
            },
            {
                'key': 'mallguide_display',
                'label': 'Mallguide',
                'formatter': 'isYorN'
            },
            { key: 'actions', label: '' }],
        items: [],
        filter: null,
        totalRows: 1,
        currentPage: 1,
        perPage: 10,
        model: '',
        province: null,
        provinces: [],
        value: '',
        minMatch: 1,
        suggestionAttribute: 'name',
        malls: [],
        autocomplete: [],
        selectedEvent: "",
        isBusy: false
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        formatMallName: function(value, key, item) {
            let address = [item.physical_address, item.province].filter(Boolean).join(", ")
            
            return `
                <a class="mall-title" href="/admin/malls/edit/${item.id}">${value}</a>
                <br /><br />
                <span class="mall-address">${address}</span>
            `
        },
        provinceChange: function(value) {
            this.fetchProvinceMalls()
        },
        clickInput: function() {
            this.selectedEvent = 'click input'
        },
        clickButton: function() {
            this.selectedEvent = 'click button'
        },
        selected: function(value) {
            console.log('selected titty')
            console.log(value)
            this.selectedEvent = 'selection changed'
        },
        enter: function() {
            this.selectedEvent = 'enter'
        },
        keyUp: function() {
            this.selectedEvent = 'keyup pressed'
        },
        keyDown: function() {
            this.selectedEvent = 'keyDown pressed'
        },
        keyRight: function() {
            this.selectedEvent = 'keyRight pressed'
        },
        clear: function() {
            this.selectedEvent = 'clear input'
        },
        escape: function() {
            this.selectedEvent = 'escape'
        },
        changed: function(value) {
            console.log('sdfsdf')
            console.log(value)

        },
        fetchAutocompleteMalls: function() {
            var vm = this;

            url = '/malls/autocomplete';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status == 200) {
                        data = response.data;
                        vm.autocomplete = data
                    }
                })
        },
        fetchProvinceMalls: function () {
            var vm = this;

            this.toggleBusy()
            console.log('=====================')
            if (this.province == null || this.province == '') return

            url = '/malls/province/' + this.province;
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status == 200) {
                        data = response.data;
                        vm.malls = data
                        vm.totalRows = data.length
                    }
                })
        },
        fetchProvinces: function() {
            var vm = this;

            url = '/provinces';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status == 200) {
                        data = response.data;
                        data.forEach(province => {
                            vm.provinces.push(
                                {
                                    'value': province.id,
                                    'text': province.name
                                }
                            )
                        });
                    }
                })
        },
        isYorN: function(value, key, item) {

            return value == 'Y'?
            '<svg class="check" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5L12 5z"></path></svg>':
            '<svg class="cross" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48L7.48 8z"></path></svg>'
        },
        isNotEmpty: function(value, key, item) {

            return value != ''?
            '<svg class="cross" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48L7.48 8z"></path></svg>':
            '<svg class="check" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5L12 5z"></path></svg>'
        },
        hasCoordinates: function(value, key, item) {
            return (typeof value == 'object' && (value.x_coordinate != '' && value.y_coordinate != '')) ?
                '<svg class="check" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5L12 5z"></path></svg>':
                '<svg class="cross viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48L7.48 8z"></path></svg>'
        },
        formatImages: function(value, key, item) {
            return `Logo: <strong>0</strong><br />Images: <strong>${value.length}</strong>`
        },
        formatLastUpdated: function(value, key, item) {
            return value
        },
        editUrl: function(item) {

            return '/admin/malls/edit/' + item.id
        } 
    },
    components: {
        'vue-instant': VueInstant.VueInstant
    },
    mounted: function () {
        this.fetchAutocompleteMalls()

        this.fetchProvinces()
    }
});
