new Vue({
    el: "#mall_exhibitions",
    data: {
        fields: [
            {
                'key': 'name',
                'label': 'Exhibition Title',
            },
            {
                'key': 'start_date',
                'label': 'Start Date',
            },
            {
                'key': 'end_date',
                'label': 'End Date',
            },
            {
                'key': 'category',
                'label': 'Category'
            },
            {
                'key': 'venue',
                'label': 'Venue'
            },
            {
                'key': 'display',
                'label': 'Display'
            },
            {
                'key': 'actions',
                'label': ''
            }
        ],
        items: [],
        filter: null,
        totalRows: 1,
        currentPage: 1,
        perPage: 10,
        model: '',
        value: '',
        mall: null,
        malls: [],
        exhibitions: [],
        isBusy: false,
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        fetchMallExhibitions: function () {

            var vm = this;

            this.toggleBusy()

            var mallId = window.location.pathname.split('/').pop()

            url = '/malls/exhibitions/' + mallId
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        data = response.data;
                        if (data.hasOwnProperty('exhibitions')) {
                            vm.exhibitions = data.exhibitions
                        }
                    }
                })
        },
        editExhibitionUrl: function (item) {
            return '/admin/exhibitions/edit/' + item.id
        }
        ,deleteExhibitionUrl: function (item) {
            let vm = this;
            let url = '/admin/exhibitions/delete/' + item.id
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        window.location.reload();
                    }
                })
        },
        confirmRemove(item) {
            if (confirm('Are you sure you want to remove item?')) {
                this.deleteExhibitionUrl(item)
            } else  {
                return false;
            }
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    mounted: function () {
        this.fetchMallExhibitions()
    }
})
