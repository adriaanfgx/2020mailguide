new Vue({
    el: "#events",
    data: {
        fields: [
            {
                'key': 'province',
                'label': 'Province',
                'sortable': false
            },
            {
                'key':'city',
                'label': 'City',
            },
            {
                'key':'name',
                'label': 'Mall',
            },
            {
                'key': 'events',
                'label': 'Number of events'
            },
            {
                'key': 'facebook',
                'label': 'Linked To Facebook',
                'formatter': 'isYorN'
            }, 
            { key: 'actions', label: '' }],
        items: [],
        filter: null,
        totalRows: 1,
        currentPage: 1,
        perPage: 10,
        model: '',
        province: null,
        provinces: [],
        value: '',
        malls: [],
        isBusy: false,
        mall: null,
        mallDropdown:[]
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        provinceChange: function(value) {
            var vm = this
            this.fetchProvinceEvents();
            vm.mallDropdown = []
            this.fetchMalls(value)
        },
        fetchProvinceEvents: function () {
            var vm = this;

            this.toggleBusy()
            console.log('=====================')
            if (this.province == null || this.province === '') return

            url = '/events/province/' + this.province;
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        data = response.data;
                        vm.malls = data
                        vm.totalRows = data.length
                    }
                })
        },
        fetchProvinces: function() {
            var vm = this;

            url = '/provinces';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        data.forEach(province => {
                            vm.provinces.push(
                                {
                                    'value': province.id,
                                    'text': province.name
                                }
                            )
                        });
                    }
                })
        },
        isYorN: function(value, key, item) {

            return value == 'Yes'?
            '<svg class="check" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5L12 5z"></path></svg>':
            '<svg class="cross" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48L7.48 8z"></path></svg>'
        },
        addEventUrl: function(item) {
            return '/admin/events/create/' + item.item.mall_id
        },
        viewEventsUrl: function (item) {
            return '/admin/events/' + item.item.mall_id
        },
        fetchMalls: function (provinceId) {
            var vm = this;

            url = '/mall-dropdown-list/';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        data.forEach(mall => {
                            vm.mallDropdown.push(
                                {
                                    'value': mall.id,
                                    'text': mall.name
                                }
                            )
                        });
                    }
                })
        },
        mallChange: function (mallId) {
            window.location.href = '/admin/events/' + mallId
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    mounted: function () {
        this.fetchProvinces()
        this.fetchMalls()
    }
});
