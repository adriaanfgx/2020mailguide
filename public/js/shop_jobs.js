new Vue({
    el: "#shop_jobs",
    data: {
        fields: [
            {
                'key': 'position',
                'label': 'Position',
            },
            {
                'key': 'salary',
                'label': 'Salary',
            },
            {
                'key': 'contact_person',
                'label': 'Contact Person',
            },
            {
                'key': 'description',
                'label': 'Description',
            },
            {
                'key': 'starting_date',
                'label': 'Job count'
            },
            {
                'key': 'closing_date',
                'label': 'Closing Date'
            },
            {
                'key': 'show_contact',
                'label': 'Show Job Contact Info'
            },
            {
                'key': 'display',
                'label': 'Display'
            },
            {
                'key': 'actions',
                'label': ''
            }
        ],
        items: [],
        filter: null,
        totalRows: 0,
        currentPage: 1,
        perPage: 10,
        model: '',
        value: '',
        mall: null,
        malls: [],
        jobs: [],
        isBusy: false,
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        fetchMallJobs: function () {

            this.toggleBusy()

            const vm = this;

            let mallId = window.location.pathname.split('/').pop();

            url = '/jobs/shop/' + mallId
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        data = response.data;
                        if (data.hasOwnProperty('jobs')) {
                            vm.jobs = data.jobs
                            vm.totalRows = data.jobs.length
                        }
                    }
                })
        },
        viewJobApplicationsUrl: function (job) {
            return '/admin/jobs/' + job.id + '/applicants/'
        },
        editJobUrl: function (job) {
            return '/admin/jobs/' + job.id + '/edit'
        },
        confirmRemove(item) {
            if (confirm('Are you sure you want to remove item?')) {
                this.deleteJob(item)
            } else {
                return false;
            }
        },
        deleteJob: function (item) {
            let vm = this;
            let url = '/admin/jobs/delete/' + item.id
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        window.location.reload();
                    }
                })
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    mounted: function () {
        this.fetchMallJobs()
    }
})
