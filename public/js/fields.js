function hide_fields() {
    console.log("typf field value is " + $('#typeOfField').val())
    change_field_types($('#typeOfField').val());

    $("#typeOfField").change(function () {
        console.log("hide_fields");
        change_field_types($(this).val());
    });

}

function change_field_types(type) {
    console.log("type is ==> " + type)
    if (type === 'textarea') {
        //textarea fields
        $("#textareafields").show();
        //textfield fields
        $("#textfields").hide();
    } else {
        //textarea fields
        $("#textareafields").hide()
    }
    if (type === 'radio') {
        //alert('hi');
        $("#radiofields").show();
        get_radio_forms();

        $("#textfields").hide();
    } else {
        $("#radiofields").hide();
    }
    if (type === 'checkbox') {
        //checkbox field
        $("#checkfields").show();
        get_check_forms();

        //hide textfield fields
        $("#textfields").hide();
    } else {
        $("#checkfields").hide();
    }
    if (type === 'select') {

        $("#numSelectOptions").show();
        $("#multiple").show();
        //textfield fields
        $("#textfields").hide();
        get_select_form();

    } else {
        $("#numSelectOptions").hide();
        $("#multiple").hide();
    }
    if (type === 'file') {

        $("#filefields").show();
        //textfield fields
        $("#textfields").hide();
    } else {
        $("#filefields").hide();
    }
    if (type === 'dob') {

        $("#textfields").show();
        $("input[name=defaultValue]").addClass('timepicker');
        $("#maxlength").hide();
        $("#defaultValue").hide();
        //alert("hi");
        /*
        $("input[name=defaultValue]").datepicker({
             dateFormat: "yy-mm-dd"
         });*/
        //activate_date();
    } else {
        //$("input[name=defaultValue]").removeClass('timepicker');
        $("#defaultValue").show();
        $("#maxlength").show()
        $("#textfields").hide();
    }
    if (type === 'text') {
        $("#textfields").show();
    }
}

/*functions for add field modal********************/
function get_radio_forms() {
    var fieldID = $('#addField').data('id');
    var type = "radio";
    var amount = $("#numRadioOptions option:selected").val();
    var prev_type = $('#prev_type').val();
    var id_string = '#radios';
    if (fieldID == 'new' || (prev_type != 'radio')) {
        var first_row = getRow(1, amount, type);
        $('#radios').html(first_row);
        for (var ii = 2; ii <= amount; ii++) {
            var row = getRow(ii, amount, type);
            $('#radios tr:last').after(row);
        }

        activateMove();
    } else {
        get_edit_elems('#radios', fieldID, type)
    }

    //$("#numRadioOptions").append("<table><tr><td>Hi"+amount+" <td></tr></table>");

    $("#numRadioOptions").change(function () {

        var old_amount = $('#radios tr').length;
        var new_amount = $("#numRadioOptions option:selected").val();

        if (old_amount > new_amount) {
            for (var ii = old_amount; ii > new_amount; ii--) {
                $('#radios tr:last').remove();
            }
        } else {
            for (var ii = old_amount + 1; ii <= new_amount; ii++) {
                var row = getRow(ii, new_amount, type);
                $('#radios tr:last').after(row);
            }
        }
        activateMove();
    });
}

function get_edit_elems(IDname, fieldID, type) {
    var url = "/admin/getElements/" + fieldID;

    $.getJSON(url,

        function (the_data) {
            if (the_data) {
                var amount = Object.keys(the_data).length;
                $.each(the_data, function (i, item) {
                    //alert(item.formFieldID);
                    var ii = i + 1;
                    var row = getRow(ii, amount, type, item);
                    if (ii == 1) {
                        $(IDname).html(row);
                    } else {
                        $(IDname + ' tr:last').after(row);
                    }
                    //alert(row);

                });

                activateMove();
                //hideMovements();
            }

        });

}

function get_check_forms() {
    //alert("hi");
    var type = "checkbox";
    var amount = $("#numRadioOptions option:selected").val();
    //alert(amount);
    //var stop=amount;
    var the_id = "#checks";
    var fieldID = $('#addField').data('id');

    //alert(fieldID);
    getRows(fieldID, amount, type, the_id);

    $("#numCheckboxOptions").change(function () {

        var old_amount = $('#checks tr').length;
        var new_amount = $("#numCheckboxOptions option:selected").val();

        if (old_amount > new_amount) {
            for (var ii = old_amount; ii > new_amount; ii--) {
                $('#checks tr:last').remove();
            }
        } else {
            for (var ii = old_amount + 1; ii <= new_amount; ii++) {
                var row = getRow(ii, new_amount, type);

                $('#checks tr:last').after(row);
            }
        }

        activateMove();
        //hideMovements();
    });
}

function get_select_form() {
    var type = "select";
    var the_id = "#selects";
    var amount = $("#numSelectOptions option:selected").val();
    var fieldID = $('#addField').data('id');

    getRows(fieldID, amount, type, the_id);

    $("#numSelectOptions").change(function () {

        var old_amount = $(the_id + ' tr').length;
        var new_amount = $("#numSelectOptions option:selected").val();

        if (old_amount > new_amount) {
            for (var ii = old_amount; ii > new_amount; ii--) {
                $(the_id + ' tr:last').remove();
            }
        } else {
            for (var ii = old_amount + 1; ii <= new_amount; ii++) {
                var row = getRow(ii, new_amount, type);

                $(the_id + ' tr:last').after(row);
            }
        }
        activateMove();
    });
}

function getRows(fieldID, amount, type, the_id) {
    var prev_type = $('#prev_type').val();

    //alert(prev_type);
    if (fieldID == 'new' || prev_type != type) {
        var first_row = getRow(1, amount, type);
        $(the_id).html(first_row);
        for (var ii = 2; ii <= amount; ii++) {
            var row = getRow(ii, amount, type);
            $(the_id + ' tr:last').after(row);
        }
        activateMove();
    } else {
        var url = "/admin/getElements/" + fieldID;

        $.getJSON(url,
            function (the_data) {
                if (the_data) {
                    amount = Object.keys(the_data).length;
                    $.each(the_data, function (i, item) {
                        var ii = i + 1;
                        var row = getRow(ii, amount, type, item);
                        if (ii == 1) {
                            $(the_id).html(row);
                        } else {
                            $(the_id + ' tr:last').after(row);
                        }
                    });
                    activateMove();
                }

            });
    }


}

function getRow(num, total, the_type, item) {
    var sel = "<select data-num='" + num + "'>";
    sel += "<option value=''>Move Option...</option>";
    sel += "<option value='up'>Move up one position</option>";
    sel += "<option value='down'>Move down one position</option>";
    sel += "<option value='top'>Move to top</option>";
    sel += "<option value='bottom'>Move to bottom</option>";

    sel += "</select>"

    var selected = ''
    var value = ''
    if (item) {
        if (item.checkedSelected == 'Y') {
            var selected = "checked='checked'";
        }
        var value = item.name;
    }


    var row = '<tr><td><label class="control-label" data-num="' + num + '"  for="add_field">Option Name (' + num + ')</label><input type="text" id="name' + num + '" name="checkName[]" placeholder="name" value="' + value + '"></td>' +
        '<td>' +
        '<label class="control-label" for="checkedSelected">set checked by default?</label> ' +
        '<input type="checkbox" data-num="' + num + '" value="' + num + '" id="check' + num + '" ' + selected + ' name="checkedSelected' + num + '">' +
        '</td>' +

        '<td class="move1">' + sel + '</td>' +
        '</tr>';
    //hideMovements();
    return row;
}

function activateMove() {
    var ref = ".move1 select";
    hideMovements(ref);
    //alert('what');
    $(ref).change(function () {
//			alert('whaty');
        //moving to
        var the_val = $(this).val();

        //this rows number
        var row_num = $(this).data("num");

        if (the_val === "up") {
            //get one ups row number
            var new_num = row_num - 1;
        } else if (the_val === "down") {
            var new_num = row_num + 1;
        } else if (the_val === "top") {
            //get one ups row number
            var new_num = 0;
        } else if (the_val === "bottom") {
            var amount = $(ref).length;
            var new_num = amount;
        }

        var move_id = "#name" + new_num;
        var cur_id = "#name" + row_num;
        var check_move_id = "#check" + new_num;
        var check_cur_id = "#check" + row_num;

        //the added name to move
        var inputVal = $(cur_id).val();
        var swapWithVal = $(move_id).val();
        //alert(swapWithVal);
        $(move_id).val(inputVal);
        $(cur_id).val(swapWithVal);

        var is_checked = $(check_cur_id).is(':checked');
        var is_move_checked = $(check_move_id).is(':checked');
        $(check_cur_id).prop('checked', is_move_checked);
        $(check_move_id).prop('checked', is_checked);

    });
}

function activateFieldMove() {
    $(".to_move").change(function () {
        var move = $(this).val();
        var fieldID = $(this).data('id');
        var the_url = "/admin/changeOrder/" + fieldID + '/' + move;
        $.ajax(
            {
                type: 'GET',
                url: the_url,
                success: function (data) {
                    get_fields(data);
                }
            });
    });
}

function hideMovements(ref) {
    var amount = $(ref).length - 1;
    var amountn = amount - 1;
    //alert(amount);
    if (amount > 0) {
        $(ref).each(function (i, item) {
            //alert(item.formFieldID);
            if (i == 0) {
                $(this).children("option[value='up']").hide();
                $(this).children("option[value='top']").hide();
                $(this).children("option[value='bottom']").show();
                $(this).children("option[value='down']").show();
            } else if (i == 1 && i != amount && i != amountn) {
                //$(".move option[value='up']").hide();
                $(this).children("option[value='top']").hide();
                $(this).children("option[value='bottom']").show();
                $(this).children("option[value='down']").show();
                $(this).children("option[value='up']").show();
            } else if (i == amountn) {
                //$(this).children("option[value='bottom']").hide();
                $(this).children("option[value='top']").show();
                $(this).children("option[value='bottom']").show();
                $(this).children("option[value='down']").hide();
                $(this).children("option[value='up']").show();
            } else if (i == amount) {
                $(this).children("option[value='top']").show();
                $(this).children("option[value='bottom']").hide();
                $(this).children("option[value='down']").hide();
                $(this).children("option[value='up']").show();
            } else {
                $(this).children("option[value='top']").show();
                $(this).children("option[value='bottom']").show();
                $(this).children("option[value='down']").show();
                $(this).children("option[value='up']").show();
            }
        });
    } else {
        $(this).children("option[value='top']").hide();
        $(this).children("option[value='bottom']").hide();
        $(this).children("option[value='down']").hide();
        $(this).children("option[value='up']").hide();
    }


}