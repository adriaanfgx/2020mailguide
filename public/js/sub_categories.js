new Vue({
    el: "#sub_categories",
    data: {
        fields: [
            {
                'key': 'name',
                'label': 'Name',
            },
            {key: 'actions', label: ''}],
        items: [],
        filter: null,
        totalRows: 1,
        currentPage: 1,
        perPage: 10,
        model: '',
        value: '',
        subcategories: [],
        isBusy: false,
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        fetchSubCategories: function () {
            var vm = this;
            this.toggleBusy()

            var categoryId = window.location.pathname.split('/').pop()

            url = '/admin/category/children/' + categoryId;
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status == 200) {
                        console.log("Response is" + response);
                        data = response.data;
                        vm.subcategories = data
                        vm.totalRows = data.length
                    }
                })
        },
        editUrl: function (item) {
            return '/admin/sub-category/edit/' + item.item.id
        },
        deleteUrl: function (item) {
            let vm = this;
            let url = '/admin/sub-category/delete/' + item.item.id
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status == 200) {
                        window.location.reload();
                    }
                })
        },
        confirmRemove(item) {
            if (confirm('Are you sure you want to remove item?')) {
                this.deleteUrl(item)
            } else  {
                return false;
            }
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    mounted: function () {
        this.fetchSubCategories()
    }
});
