new Vue({
    el: "#mall_shops",
    data: {
        fields: [
            {
                'key': 'name',
                'label': 'Name',
                'formatter': 'formatName'
            },
            {
                'key': 'category',
                'label': 'Category',
            },
            {
                'key': 'shop_number',
                'label': 'Shop Number',
            },
            {
                'key': 'telephone',
                'label': 'Telephone'
            },
            {
                'key': 'images',
                'label': 'Images',
                'formatter': 'formatImages'
            },
            {
                'key': 'map',
                'label': 'Map',
                'formatter': 'hasMap'
            },
            {
                'key': 'map_coordinates',
                'label': 'Coordinates',
                'formatter': 'hasCoordinates'
            },
            {
                'key': 'new_shop',
                'label': 'New Shop',
                'formatter': 'isYesOrNo'
            },
            {
                'key': 'display',
                'label': 'Display',
                'formatter': 'isYesOrNo'
            },
            {
                'key': 'last_updated',
                'label': 'Last Updated'
            },
            {
                'key': 'name_lower',
                'label': 'Generic Info',
                'formatter': 'formatGeneric'
            },
            {
                'key': 'actions',
                'label': ''
            }
        ],
        items: [],
        filter: null,
        totalRows: 1,
        currentPage: 1,
        perPage: 10,
        model: '',
        value: '',
        mall: null,
        shops: [],
        genericShops: [],
        genericShopNames: [],
        isBusy: false,
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        fetchMallShops: function () {

            var vm = this;

            this.toggleBusy()

            var mallId = window.location.pathname.split('/').pop()

            url = '/admin/shops/mall/' + mallId
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        data = response.data;
                        if (data.hasOwnProperty('shops')) {
                            vm.shops = data.shops
                            vm.totalRows = vm.shops.length
                        }
                    }
                })
        },
        editShopUrl: function (item) {
            return '/admin/shops/edit/' + item.id
        },
        deleteEventUrl: function (item) {
            let vm = this;
            let url = '/admin/shops/delete/' + item.id
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        window.location.reload();
                    }
                })
        },
        confirmRemove(item) {
            if (confirm('Are you sure you want to remove item?')) {
                this.deleteEventUrl(item)
            } else  {
                return false;
            }
        },
        hasCoordinates: function(value, key, item) {
            return (typeof value == 'object' && (value.x_coordinate !== '' && value.y_coordinate !== '')) ?
                '<svg class="check" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5L12 5z"></path></svg>':
                '<svg class="cross viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48L7.48 8z"></path></svg>'
        },
        hasMap: function(value, key, item) {
            return value === ''?
                '<svg class="cross" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48L7.48 8z"></path></svg>':
                '<svg class="check" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5L12 5z"></path></svg>'
        },
        isYesOrNo: function(value, key, item) {
            return value === 'Y'?
                '<svg class="check" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5L12 5z"></path></svg>':
                '<svg class="cross" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48L7.48 8z"></path></svg>'

        },
        formatName: function(value, key, item) {
            return `
                <a class="mall-title" href="/admin/shops/edit/${item.id}">${value}</a>`
        },
        formatImages: function(value, key, item) {
            let logo = 0
            if (item.logo === 'Yes') {
                logo = 1
            }
            return `Logo: <strong>${logo}</strong><br />Images: <strong>${item.images}</strong>`
        },
        formatGeneric: function (value, key, item) {

            let vm = this;
            for (i in vm.genericShops)  {
                if (vm.genericShops[i].toLowerCase() === value) {
                    return  '<a href="/admin/shops/edit-generic/'+i+'" target="_blank" class="text-info">View Generic</a><br />' +
                         '<a href="/admin/shops/copy-from-generic/'+item.id+'/'+i+'" class="text-success">Copy from Generic</a><br />'
                }
            }

            if (vm.genericShopNames.includes(value.toUpperCase()) === false) {
                return '<a href="/admin/shops/write-to-generic/'+item.id+'" class="text-error">Write to Generic</a><br />'
            }
        },
        getGeneric: function () {
            let vm = this;
            let url = '/admin/shops/get-generic'
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        if (data.hasOwnProperty('genericShops')) {
                            vm.genericShops = data.genericShops
                        }
                    }
                })
        },
        getGenericShopNames: function () {
            let vm = this;
            let url = '/admin/shops/get-generic-shop-names'
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        if (data.hasOwnProperty('genericShopNames')) {
                            vm.genericShopNames = data.genericShopNames
                        }
                    }
                })
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    mounted: function () {
        this.fetchMallShops()
        this.getGeneric()
        this.getGenericShopNames()
    }
})
