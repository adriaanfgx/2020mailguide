new Vue({
    el: "#promotions",
    data: {
        fields: [
            {
                'key': 'name',
                'label': 'Name',
                'sortable': false
            },
            {
                'key':'category',
                'label': 'Category',
            },
            {
                'key':'subcategory',
                'label': 'Sub Category',
            },
            {
                'key': 'promotions',
                'label': 'Number of promotions'
            },
            { key: 'actions', label: '' }],
        items: [],
        filter: null,
        totalRows: 1,
        currentPage: 1,
        perPage: 10,
        model: '',
        province: null,
        provinces: [],
        value: '',
        shops: [],
        isBusy: false,
        mall: null,
        mallDropdown:[]
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        provinceChange: function(value) {
            var vm = this
            // this.fetchMallsWithPromos();
            vm.mallDropdown = []
            this.fetchPromotionMalls(value)
        },
        fetchMalls: function () {
            var vm = this;

            url = '/mall-dropdown-list/';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        data.forEach(mall => {
                            vm.mallDropdown.push(
                                {
                                    'value': mall.id,
                                    'text': mall.name
                                }
                            )
                        });
                    }
                })
        },
        fetchProvinces: function() {
            var vm = this;

            url = '/provinces';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        data.forEach(province => {
                            vm.provinces.push(
                                {
                                    'value': province.id,
                                    'text': province.name
                                }
                            )
                        });
                    }
                })
        },
        isYorN: function(value, key, item) {

            return value == 'Yes'?
            '<svg class="check" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5L12 5z"></path></svg>':
            '<svg class="cross" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48L7.48 8z"></path></svg>'
        },
        addPromotionUrl: function(item) {
            return '/admin/promotions/create/' + item.item.id
        },
        viewPromotionsUrl: function (item) {
            return '/admin/shop-promotions/' + item.item.id
        },
        fetchPromotionMalls: function (provinceId) {
            var vm = this;

            url = '/malls-with-promotions/' + provinceId;
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        data.forEach(mall => {
                            vm.mallDropdown.push(
                                {
                                    'value': mall.id,
                                    'text': mall.name
                                }
                            )
                        });
                    }
                })
        },
        mallChange: function (mallId) {
            this.fetchShopPromotions(mallId);
            // window.location.href = '/admin/events/' + mallId
        },
        fetchShopPromotions(mallId) {
            var vm = this;

            this.toggleBusy()
            console.log('=====================')

            url = '/admin/mall-promotions/' + mallId;
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        data = response.data.shops;
                        vm.shops = data
                        if (data === undefined) {
                            window.location.href = 'create-mall-promotions/' + mallId;
                        }
                        vm.totalRows = data.length
                    }
                })
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    mounted: function () {
        this.fetchProvinces()
        this.fetchMalls()
    }
});
