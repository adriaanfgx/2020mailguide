new Vue({
    el: "#change-country",
    data: {
        countries: []
    },
    name: 'Change Country',
    methods: {
        fetchCountries: function () {
            var vm = this

            axios
                .get('/countries')
                .then(function (response) {
                    if (response.status == 200) {
                        data = response.data;
                        vm.countries = data
                    }
                })
        }
    },
    mounted: function () {
        this.fetchCountries()
    }
})
