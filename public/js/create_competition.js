new Vue({
    el: "#create-competition",
    data: {
        fields: [],
        name: null,
        mandatory: null,
        typeOfField: null,
        defaultValue: null,
        size: null,
        maxlength: null
    },
    methods: {
        fetchCustomFields: function () {
            var vm = this;

            url = '/admin/competitions/page-form-fields';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        vm.fields = response.data.formFields;
                    }
                })
        },
        removeField: function (event) {
            let index = event.target.dataset.index

            this.fields.splice(index, 1)
        },
        addField: function () {
            let field = {
                name: this.name,
                mandatory: this.mandatory,
                typeOfField: this.typeOfField,
                defaultValue: this.defaultValue,
                size: this.size,
                maxlength: this.maxlength,
            }

            this.fields.push(field)

            $('#addFieldModal').modal('hide')
        }
    },
    mounted: function () {
        // this.fetchCustomFields()
    }
})
