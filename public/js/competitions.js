new Vue({
    el: "#competitions",
    data: {
        fields: [
            {
                'key': 'subject',
                'label': 'Subject',
            },
            {
                'key': 'recipient',
                'label': 'Recipient Email',
            },
            {
                'key': 'start_date',
                'label': 'Start',
            },
            {
                'key': 'end_date',
                'label': 'End'
            },
            {
                'key': 'linked_to_facebook',
                'label': 'Facebook',
                'formatter': 'hasFacebook'
            },
            {
                'key': 'responses',
                'label': 'Responses',
                'formatter': 'responsesLink'
            },
            {
                'key': 'actions',
                'label': ''
            }
        ],
        items: [],
        filter: null,
        totalRows: 0,
        currentPage: 1,
        perPage: 10,
        model: '',
        value: '',
        province: null,
        provinces: [],
        mall: "",
        malls: [],
        competitions: [],
        isBusy: false,
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        onProvinceChange: function (provinceId) {
            if (provinceId == null) return

            this.fetchProvinceMalls(provinceId)
        },
        onMallChange: function (mallId) {
            this.fetchMallCompetitions(mallId)
        },
        fetchProvinces: function () {
            var vm = this;

            url = '/provinces';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        data.forEach(province => {
                            vm.provinces.push(
                                {
                                    'value': province.id,
                                    'text': province.name
                                }
                            )
                        });
                    }
                })
        },
        fetchProvinceMalls: function (provinceId) {
            var vm = this;

            url = '/malls/province/' + this.province;
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        vm.malls.length = 0
                        response.data.forEach(mall => {
                            vm.malls.push(
                                {
                                    'value': mall.id,
                                    'text': mall.name
                                }
                            )
                        });
                    }
                })
        },
        fetchAutocompleteMalls: function () {
            var vm = this;

            url = '/malls/autocomplete';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        data.forEach(province => {
                            vm.malls.push(
                                {
                                    'value': province.id,
                                    'text': province.name
                                }
                            )
                        });

                        let mallId = window.location.pathname.split('/').pop()
                        if (!isNaN(parseInt(mallId))) {
                            vm.mall = mallId

                            vm.fetchMallCompetitions(mallId)
                        }
                    }
                })
        },
        fetchMallCompetitions: function (mallId) {
            if (mallId === "" || mallId == null) return

            var vm = this;
            this.toggleBusy()

            url = '/mall/competitions/' + mallId
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        vm.competitions = response.data.competitions
                        vm.totalRows = response.data.competitions.length
                    }
                })
        },
        editCompetitionUrl: function (competition) {
            return '/admin/competitions/edit/' + competition.id
        },
        hasFacebook: function (hasFacebook) {
            return hasFacebook === 'Yes' ?
                '<svg class="check" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M12 5l-8 8-4-4 1.5-1.5L4 10l6.5-6.5L12 5z"></path></svg>' :
                '<svg class="cross" viewBox="0 0 12 16" version="1.1" width="12" height="16" aria-hidden="true"><path fill-rule="evenodd" d="M7.48 8l3.75 3.75-1.48 1.48L6 9.48l-3.75 3.75-1.48-1.48L4.52 8 .77 4.25l1.48-1.48L6 6.52l3.75-3.75 1.48 1.48L7.48 8z"></path></svg>'
        },
        responsesLink: function (response_count, key, response) {
            if (response_count === 0) return `<span class="btn btn-sm btn-light">${response_count}</span>`

            return `<a class="btn btn-sm  btn-outline-success" href="/admin/competition/responses/${response.id}">${response_count}</a>`
        },
        confirmRemove(item) {
            if (confirm('Are you sure you want to remove competition?')) {
                console.log(item)

                let url = '/admin/competitions/delete/' + item.id

                axios
                    .delete(url, {})
                    .then(function (response) {
                        if (response.status === 200) {
                            window.location.reload();
                        }
                    });
            } else  {
                return false;
            }
        },
        createCompetitionUrl(){
            let mallId  = this.mall == null ? "" : this.mall

            return '/admin/competitions/create/' + mallId
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    mounted: function () {
        this.fetchProvinces()

        this.fetchAutocompleteMalls()
    }
})
