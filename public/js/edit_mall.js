new Vue({
    el: "#edit_mall",
    data: {
        mallDropdown: []
    },
    methods: {
        deleteImage(e) {
            e.preventDefault()

            let confirmed = confirm("Are you sure you want to remove image ?")
            let imageId = event.currentTarget.id;

            const mallId = window.location.pathname.split('/').pop();

            if (mallId !== '' && imageId !== '' && confirmed) {

                let url = '/admin/malls/removeimage/' + mallId + '/' + imageId

                axios
                    .delete(url, {})
                    .then(function (response) {
                        if (response.status === 200) {
                            window.location.reload();
                        }
                    });
            }
        },
        countryChange(event) {
            let countryId = event.target.value

            $("#province option").each(function () {
                if ($(this).val() !== '') {
                    $(this).remove();
                }
            });

            $("#city option").each(function () {
                if ($(this).val() !== '') {
                    $(this).remove();
                }
            });

            axios
                .get('/provinces/' + countryId)
                .then(function (response) {
                    if (response.status === 200) {
                        let provinces = response.data

                        $.each(provinces, function (index, province) {
                            $('#province').append($('<option>', {
                                value: province.id,
                                text: province.name
                            }));
                        });

                    }
                })
        },
        provinceChange: function (province) {
            let provinceId = province.target.value

            axios
                .get('/cities/' + provinceId)
                .then(function (response) {
                    if (response.status === 200) {
                        let cities = response.data

                        $.each(cities, function (index, city) {
                            $('#city').append($('<option>', {
                                value: city.id,
                                text: city.name
                            }));
                        });
                    }
                })
        }
    },
    mounted: function () {
    }
});
