new Vue({
    el: "#malls",
    data: {
        fields: [
            {
                'key': 'province',
                'label': 'Province',
                'sortable': false
            },
            {
                'key':'city',
                'label': 'City',
            },
            {
                'key':'name',
                'label': 'Mall',
            },
            {
                'key': 'count_shops',
                'label': 'Number of Shops'
            },
            { key: 'actions', label: '' }],
        items: [],
        filter: null,
        totalRows: 1,
        currentPage: 1,
        perPage: 10,
        model: '',
        province: null,
        provinces: [],
        value: '',
        malls: [],
        isBusy: false,
        mall: null,
        mallDropdown:[],
        autocomplete: [],
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        provinceChange: function(value) {
            var vm = this
            this.fetchProvinceMalls();
            vm.mallDropdown = []
            this.fetchMalls(value)
        },
        fetchProvinceMalls: function () {
            var vm = this;

            this.toggleBusy()
            console.log('=====================')
            if (this.province == null || this.province === '') return

            url = '/admin/shops/province-malls/' + this.province;
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        console.log(response.data.malls);
                        data = response.data.malls;
                        vm.malls = data
                        vm.totalRows = data.length
                    }
                })
        },
        fetchProvinces: function() {
            var vm = this;

            url = '/provinces';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        data.forEach(province => {
                            vm.provinces.push(
                                {
                                    'value': province.id,
                                    'text': province.name
                                }
                            )
                        });
                    }
                })
        },
        addShopUrl: function(item) {
            return '/admin/shops/create/' + item.item.id
        },
        viewShopsUrl: function (item) {
            return '/admin/mall-shops/' + item.item.id
        },
        downloadShopsUrl: function (item) {
            return '/admin/shops/export-to-excel/' + item.item.id
        },
        fetchMalls: function (provinceId) {
            console.log("Province is " + provinceId)
            var vm = this;
            url = '/mall-dropdown-list/';
            if (provinceId > 0) {
                url = '/mall-dropdown-list/' + provinceId;
            }

            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        data.forEach(mall => {
                            vm.mallDropdown.push(
                                {
                                    'value': mall.id,
                                    'text': mall.name
                                }
                            )
                        });
                    }
                })
        },
        mallChange: function (mallId) {
            window.location.href = '/admin/mall-shops/' + mallId
        },
        fetchAutocompleteMalls: function() {
            var vm = this;

            url = '/malls/autocomplete';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status == 200) {
                        data = response.data;
                        vm.autocomplete = data
                    }
                })
        },
        mallSelected: function(val) {
            window.location.href = '/admin/mall-shops/' + val.id
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    components: {
        VueBootstrapTypeahead
    },
    mounted: function () {
        this.fetchProvinces()
        this.fetchMalls()
        this.fetchAutocompleteMalls()
    }
});
