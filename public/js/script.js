



function initGMap(_x, _y, _container_id) {

    if (_x && _y && typeof google != 'undefined') {
        var map;
        var lat = _x;
        var long = _y;

        var latlng = new google.maps.LatLng(lat, long);

        var mapOptions = {
            zoom: 13,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            scrollwheel: false
        };

        map = new google.maps.Map(document.getElementById(_container_id), mapOptions);

        marker = new google.maps.Marker({
            map: map,
            draggable: true,
            position: latlng
        });
    } else {
        return false;
    }
}


function slugify(str)
{
    if(str){
        return str
            .toLowerCase()
            .replace(/[^\w ]+/g,'')
            .replace(/ +/g,'-')
            ;
    }
}

