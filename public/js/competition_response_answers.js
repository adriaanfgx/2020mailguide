new Vue({
    el: "#competition_response_answers",
    data: {
        fields: [
            {
                'key': 'fieldName',
                'label': 'Competition field',
            },
            {
                'key': 'value',
                'label': 'Answer',
            },
        ],
        answers: [],
        winner: null,
        isBusy: false,
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        fetchCompetitionResponses: function () {
            var vm = this;
            this.toggleBusy()

            const formFeedbackID = window.location.pathname.split('/').pop();

            url = '/competition/response/answers/' + formFeedbackID
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        vm.answers = response.data.response.form_feedback_values
                        vm.winner = response.data.winner
                    }
                })
        },
        toggleWinner: function(event) {
            console.log('toggling winner ' + this.winner)
            if (this.winner) {
                window.location.href = `/admin/competition/response/answers/${this.winner}/remove-winner`
            } else {
                const formFeedbackID = window.location.pathname.split('/').pop();
                window.location.href = `/admin/competition/response/answers/${formFeedbackID}/choose-winner`
            }
        },
        deleteResponseUrl: function (competition) {
            return '/admin/competitions/edit/' + competition.id
        },
        viewAnswersButton: function(id) {
            return `<a class="btn btn-sm  btn-outline-secondary" href="/admin/competition/response/answers/${id}">View Answers</a>`
        },
    },
    mounted: function () {
        this.fetchCompetitionResponses()
    }
})
