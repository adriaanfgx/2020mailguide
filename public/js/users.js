new Vue({
    el: "#users",
    data: {
        fields: [
            {
                'key': 'email',
                'label': 'User',
            },
            {
                'key': 'admin_level',
                'label': 'Type Of User',
            },
            {
                'key': 'activated',
                'label': 'Activated',
                'formatter': 'isYorN'
            },
            {
                'key': 'fgx_approved',
                'label': 'FGX Approved',
                'formatter': 'isYorN'
            },
            {
                'key': 'created_at',
                'label': 'Date Added'
            },
            {
                'key': 'actions',
                'label': ''
            }
        ],
        items: [],
        filter: null,
        totalRows: 0,
        currentPage: 1,
        perPage: 10,
        users: [],
        isBusy: false,
        selected_user_type: null,
        user_types: [
            {'value': 'Master', 'text': 'Master'},
            {'value': 'FGX Staff', 'text': 'FGX Staff'},
            {'value': 'Mall Manager', 'text': 'Mall Centre Manager'},
            {'value': 'Marketing Manager', 'text': 'Marketing Manager'},
            {'value': 'Shop Manager', 'text': 'Mall Shop'},
            {'value': 'Chain Manager', 'text': 'Shop Chain'},
        ],
        selected_user_state: null,
        user_states: [
            {'value': null, 'text': 'All users'},
            {'value': 1, 'text': 'Activated'},
            {'value': 0, 'text': 'Not Activated'},
        ]
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        fetchUsers: function () {
            var vm = this;

            this.toggleBusy()

            url = '/admin/user-list'
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        let data = response.data;
                        if (data.hasOwnProperty('users')) {
                            vm.users = data.users
                            vm.totalRows = data.users.length
                        }
                    }
                })
        },
        editUserUrl: function (user) {
            return '/admin/users/edit/' + user.id
        },
        suspendUserUrl: function(user) {
            return '/admin/users/suspend/' + user.id
        },
        unSuspendUserUrl: function(user) {
            return '/admin/users/unsuspend/' + user.id
        },
        approveUserUrl: function(user) {
            return '/admin/users/approve/' + user.id
        },
        resendActivationUrl: function(user) {
            return '/users/resend_activation/' + user.id
        },
        isYorN: function(value) {
            return (value == 1) ? 'Yes': 'No'
        },
        confirmDeleteUser(user) {
            if (confirm('Are you sure you want to delete user?')) {
                let vm = this;
                let url = '/admin/users/delete/' + user.id

                axios
                    .delete(url)
                    .then(function (response) {
                        if (response.status === 200) {
                            window.location.reload();
                        }
                    })
            } else {
                return false;
            }
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    computed: {
        filtered_users: function () {
            if (this.selected_user_type != null || this.selected_user_state != null) {
                let vm = this

                var users = this.users

                if (this.selected_user_type != null) {
                    users = users.filter(function (user) {
                        return user.admin_level == vm.selected_user_type
                    })
                }

                if (this.selected_user_state != null) {
                    users = users.filter(function (user) {
                        return user.activated == vm.selected_user_state
                    })
                }

                return users
            }
            return this.users
        }
    },
    mounted: function () {
        this.fetchUsers()
    }
})
