//$('.wysi-text-area').wysihtml5();
tinymce.init({
                selector:'.wysi-text-area',
                plugins: "image link -mailto ",
                image_advtab: true,
                menubar:false,
                force_br_newlines : true,
                force_p_newlines : false,
                forced_root_block : ''
});

tinymce.create('tinymce.plugins.MailToPlugin', {

    init : function(ed, url) {
        ed.addCommand('mceMailTo', function() {
            var linkText = ed.selection.getContent({format : 'text'});
            var newText = "<a href='mailto:foo@bar.com?subject=testing'>" + linkText + "</a>"
            ed.execCommand('mceInsertContent', false, newText);
        });

        // Register example button
        ed.addButton('mailto', {
            title : 'MailTo',
            cmd : 'mceMailTo',
            image : url + '/images/mailto.gif'
        });
    }
});

// Register plugin with a short name
tinymce.PluginManager.add('mailto', tinymce.plugins.MailToPlugin);


function refreshCountryByContinent(){

    var continent = $("#continent").val();

    $("#country").empty();
    $("#province").empty();

    $.ajax({
        type: "GET",
        url: '/continentcountries/'+continent,
        data: '',
        success: function (data) {

            $('#country').append('<option value="">Please Select a country..</option>');
            $.each(data,function(key,val){

                $('#country').append('<option value="'+key+'">'+val+'</option>');

            });
        }
    });
}

function refreshProvinceList(){

    var country = $('#country').val();
    $('#province').empty();
    $.ajax({
        type: "GET",
        url: '/admin/provinces/country/'+country,
        data: '',
        success: function (data) {

            $('#province').append('<option value="">Please Select a province..</option>');

            $.each(data,function(key,val){
                $('#province').append('<option value="'+key+'">'+val+'</option>');
            });

        }
    });

}

function refreshMallsByCountry(){

    var country = $("#country").val();
    $("#mall").empty();

    $.ajax({
        type: 'GET',
        dataType: "json",
        url: '/admin/malls/filterbycountry/'+country,
        success: function (data) {

            $('#mall').append('<option value="">Please select...</option>');
            $.each(data, function(key, value){
                //alert(value.mallID);
                $('#mall').append('<option value="'+value.mallID+'">'+value.name+'</option>');
            });
        }
    });

}

function filterCityByProvince(){

    var province = $("#province").val();
    $("#city").empty();

    //Refresh city list by province
    $.ajax({
        type: "GET",
        dataType: "json",
        url: '/cities/filterbyprovince/'+province,
        data: '',
        success: function(data){

            $.each(data, function(key, value){
                $('#city').append('<option value="'+value.id+'">'+value.name+'</option>');
            });
        }
    });
}