new Vue({
    el: "#competition_responses",
    data: {
        fields: [
            {
                'key': 'date_received',
                'label': 'Date Received',
            },
            {
                'key': 'email_address',
                'label': 'Email Address',
            },
            {
                'key': 'is_winner',
                'label': 'Winner',
                'formatter': 'isWinner'
            },
            {
                'key': 'id',
                'label': '',
                'formatter': 'removeWinner'
            },
            {
                'key': 'id',
                'label': 'Answers',
                'formatter': 'viewAnswersButton'
            },
            {
                'key': 'actions',
                'label': ''
            }
        ],
        filter: null,
        totalRows: 0,
        currentPage: 1,
        perPage: 10,
        responses: [],
        isBusy: false,
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        fetchCompetitionResponses: function () {
            var vm = this;
            this.toggleBusy()

            const competitionId = window.location.pathname.split('/').pop();

            url = '/competition/responses/' + competitionId
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        vm.responses = response.data
                        vm.totalRows = response.data.length
                    }
                })
        },
        deleteResponse: function (competition) {
            if (confirm('Are you sure you want to remove this response?')) {

                let url = '/admin/competition/responses/delete/' + competition.id

                axios
                    .delete(url, {})
                    .then(function (response) {
                        if (response.status === 200) {
                            window.location.reload();
                        }
                    });
            } else {
                return false;
            }
        },
        viewAnswersButton: function (id) {
            return `<a class="btn btn-sm  btn-outline-secondary" href="/admin/competition/response/answers/${id}">View Answers</a>`
        },
        isWinner: function (is_winner) {
            return is_winner === true ? 'Winner' : '';
        },
        removeWinner: function (value, key, item) {
            if (item.is_winner) {
                return `<a href="/admin/removeWinner/${item.winner_id}" class="btn btn-warning btn-sm">Remove Winner</a>`
            }
            return ''
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    mounted: function () {
        this.fetchCompetitionResponses()
    }
})
