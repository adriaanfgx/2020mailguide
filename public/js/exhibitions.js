new Vue({
    el: "#exhibitions",
    data: {
        fields: [
            {
                'key': 'province',
                'label': 'Province',
                'sortable': false
            },
            {
                'key': 'city',
                'label': 'City',
            },
            {
                'key': 'name',
                'label': 'Mall',
            },
            {
                'key': 'exhibitions',
                'label': 'Number of exhibitions'
            },
            {key: 'actions', label: ''}],
        items: [],
        filter: null,
        totalRows: 1,
        currentPage: 1,
        perPage: 10,
        model: '',
        province: null,
        provinces: [],
        value: '',
        mall: null,
        malls: [],
        exhibitions: [],
        isBusy: false,
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        onProvinceChange: function (provinceId) {
            console.log(provinceId)
            this.fetchProvinceExhibitions(provinceId)
            this.fetchMallsForProvince(provinceId)
        },
        onMallChange: function (mallId) {
            window.location.href = '/admin/exhibitions/' + mallId
        },
        fetchProvinceExhibitions: function (provinceId) {
            var vm = this;

            if (provinceId == null || provinceId === '') return

            this.toggleBusy()

            url = '/exhibitions/province/' + provinceId;
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        data = response.data;
                        vm.exhibitions = data
                        vm.totalRows = data.length
                    }
                })
        },
        fetchProvinces: function () {
            var vm = this;

            url = '/provinces';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        data.forEach(province => {
                            vm.provinces.push(
                                {
                                    'value': province.id,
                                    'text': province.name
                                }
                            )
                        });
                    }
                })
        },
        addExhibitionsUrl: function (item) {
            return '/admin/exhibitions/create/' + item.item.mall_id
        },
        viewExhibitionsUrl: function (item) {
            return '/admin/exhibitions/' + item.item.mall_id
        },
        fetchAutocompleteMalls: function () {
            var vm = this;

            url = '/malls/autocomplete';
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        data.forEach(province => {
                            vm.malls.push(
                                {
                                    'value': province.id,
                                    'text': province.name
                                }
                            )
                        });
                    }
                })
        },
        fetchMallsForProvince: function (provinceId) {
            var vm = this;

            url = '/mall-dropdown-list/' + provinceId;
            axios
                .get(url)
                .then(function (response) {
                    if (response.status === 200) {
                        data = response.data;
                        vm.malls.length = 0
                        data.forEach(mall => {
                            vm.malls.push(
                                {
                                    'value': mall.id,
                                    'text': mall.name
                                }
                            )
                        });
                    }
                })
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    mounted: function () {
        this.fetchProvinces()

        this.fetchAutocompleteMalls()
    }
});
