new Vue({
    el: "#mall_jobs",
    data: {
        fields: [
            {
                'key': 'name',
                'label': 'Name',
            },
            {
                'key': 'category',
                'label': 'Category',
            },
            {
                'key': 'subcategory',
                'label': 'Sub Category',
            },
            {
                'key': 'jobs',
                'label': 'Job count'
            },
            {
                'key': 'actions',
                'label': ''
            }
        ],
        items: [],
        filter: null,
        totalRows: 0,
        currentPage: 1,
        perPage: 10,
        model: '',
        value: '',
        mall: null,
        malls: [],
        jobs: [],
        isBusy: false,
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        fetchMallJobs: function () {

            this.toggleBusy()

            const vm = this;

            let mallId = window.location.pathname.split('/').pop();

            url = '/jobs/mall/' + mallId
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        data = response.data;
                        if (data.hasOwnProperty('shops')) {
                            vm.jobs = data.shops
                            vm.totalRows = data.shops.length
                        }
                    }
                })
        },
        manageMallJobsUrl: function (item) {
            return '/admin/jobs/shop/' + item.id
        },
        deleteExhibitionUrl: function (item) {
            return ''
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    mounted: function () {
        this.fetchMallJobs()
    }
})
