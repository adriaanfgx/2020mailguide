new Vue({
    el: "#add_mall",
    data: {
        provinces: [],
        cities: []
    },
    name: 'Select City',
    methods: {
        fetchCountries: function () {
            var vm = this

            axios
                .get('/countries')
                .then(function (response) {
                    if (response.status == 200) {
                        data = response.data
                        vm.countries = data
                    }
                })
        },
        countryChange: function (country) {
            var vm = this

            let countryId = country.target.value

            axios
                .get('/provinces/' + countryId)
                .then(function (response) {
                    if (response.status == 200) {
                        vm.provinces = response.data
                    }
                })
        },
        provinceChange: function (province) {
            var vm = this

            let provinceId = province.target.value

            axios
                .get('/cities/' + provinceId)
                .then(function (response) {
                    if (response.status == 200) {
                        vm.cities = response.data
                    }
                })
        }
    },
    mounted: function () {
        this.fetchCountries()
    }
})
