new Vue({
    el: "#shop_promotions",
    data: {
        fields: [
            {
                'key': 'start_date',
                'label': 'Start Date',
            },
            {
                'key': 'end_date',
                'label': 'End Date',
            },
            {
                'key': 'promotion',
                'label': 'Promotion',
                formatter: 'formatName'
            },
            {
                'key': 'actions',
                'label': ''
            }
        ],
        items: [],
        filter: null,
        totalRows: 1,
        currentPage: 1,
        perPage: 10,
        model: '',
        value: '',
        mall: null,
        malls: [],
        promotions: [],
        isBusy: false,
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        fetchShopPromotions: function () {
            var vm = this;

            this.toggleBusy()

            var shopId = window.location.pathname.split('/').pop()
            url = '/admin/shop-promotions-data-point/' + shopId
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        data = response.data;

                        if (data.hasOwnProperty('promotions')) {
                            vm.promotions = data.promotions
                            vm.totalRows = vm.promotions.length
                        }
                    }
                })
        },
        editPromotionUrl: function (item) {
            return '/admin/promotions/edit/' + item.id
        },
        deletePromotionsUrl: function (item) {
            let vm = this;
            let url = '/admin/promotions/delete/' + item.id
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status === 200) {
                        window.location.reload();
                    }
                })
        },
        confirmRemove(item) {
            if (confirm('Are you sure you want to remove item?')) {
                this.deletePromotionsUrl(item)
            } else  {
                return false;
            }
        },
        formatName(name) {
            return name.substring(0, 100) + '...';
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }

    },
    mounted: function () {
        this.fetchShopPromotions()
    }
})
