new Vue({
    el: "#add_shop",
    data: {
        categories: [],
        subcategories: []
    },
    name: 'Select Category',
    methods: {
        countryChange: function (country) {
            var vm = this

            let countryId = country.target.value

            axios
                .get('/provinces/' + countryId)
                .then(function (response) {
                    if (response.status == 200) {
                        vm.provinces = response.data
                    }
                })
        },
        categoryChange: function(category) {
            var vm = this
            let categoryId = category.target.value

            axios
                .get('/admin/subcategories/' + categoryId)
                .then(function (response) {
                    if (response.status == 200) {
                        console.log("Data is " + response.data)
                        vm.subcategories = response.data
                    }
                })
        },
        provinceChange: function (province) {
            var vm = this

            let provinceId = province.target.value

            axios
                .get('/cities/' + provinceId)
                .then(function (response) {
                    if (response.status == 200) {
                        vm.cities = response.data
                    }
                })
        }
    },
    mounted: function () {

    }
})
