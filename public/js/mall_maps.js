new Vue({
    el: "#mall_maps",
    data: {
        fields: [
            {
                'key': 'image_source',
                'label': 'Image Source',
            },
            {
                'key': 'location',
                'label': 'Location',
            },
            {
                'key': 'actions',
                'label': ''
            }
        ],
        items: [],
        filter: null,
        totalRows: 1,
        currentPage: 1,
        perPage: 10,
        model: '',
        value: '',
        mall: null,
        malls: [],
        maps: [],
        isBusy: false,
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        fetchMallMaps: function () {

            var vm = this;

            this.toggleBusy()

            var mallId = window.location.pathname.split('/').pop()

            url = '/maps/mall/' + mallId
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status == 200) {
                        data = response.data;
                        if (data.hasOwnProperty('maps')) {
                            vm.maps = data.maps
                        }
                    }
                })
        },
        editMapUrl: function (item) {
            return '/admin/maps/edit/' + item.id
        },
        deleteMapUrl: function (item) {
            let vm = this;
            let url = '/admin/maps/delete/' + item.id
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy()
                    if (response.status == 200) {
                        window.location.reload();
                    }
                })
        },
        confirmRemove(item) {
            if (confirm('Are you sure you want to remove item?')) {
                this.deleteMapUrl(item)
            } else  {
                return false;
            }
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length
            this.currentPage = 1
        }
    },
    mounted: function () {
        this.fetchMallMaps()
    }
})
