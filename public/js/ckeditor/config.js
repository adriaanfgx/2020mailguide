﻿/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
		config.width="200%";
		config.height=100;
		config.skin = 'kama';
		
		config.toolbar =[
		                 ['Bold','Italic'],
		                 ['NewPage','Preview'],
		                 ['Cut','Copy','Paste','PasteText','PasteFromWord','RemoveFormat'],
		                 ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
		                 ['NumberedList','BulletedList','-','Outdent','Indent'],
		                 ['Link','Unlink','Anchor']	                 
		                 ];
};

CKEDITOR.on('instanceReady', function (evt) {
    //editor
    var editor = evt.editor;

    //webkit not redraw iframe correctly when editor's width is < 310px (300px iframe + 10px paddings)
    if (CKEDITOR.env.webkit && parseInt(editor.config.width) < 310) {
        var _ckContainer = document.getElementById('cke_' + editor.name);
        _ckContainer.style.width = '100%';
        var iframe = document.getElementById('cke_contents_' + editor.name).getElementsByTagName('iframe')[0];
        //console.log(iframe);
        iframe.style.display = 'none';
        iframe.style.display = 'block';
    }
});
