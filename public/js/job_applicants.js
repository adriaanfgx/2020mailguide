new Vue({
    el: "#job_applicants",
    data: {
        fields: [
            {
                'key': 'name',
                'label': 'Applicant',
            },
            {
                'key': 'cell',
                'label': 'Cell',
            },
            {
                'key': 'email',
                'label': 'E-mail',
            },
            {
                'key': 'date_applied',
                'label': 'Date Applied'
            },
            {
                'key': 'actions',
                'label': ''
            }
        ],
        items: [],
        filter: null,
        totalRows: 0,
        currentPage: 1,
        perPage: 10,
        model: '',
        value: '',
        mall: null,
        malls: [],
        applicants: [],
        isBusy: false,
    },
    methods: {
        toggleBusy() {
            this.isBusy = !this.isBusy
        },
        fetchMallJobs: function () {

            this.toggleBusy();

            const vm = this;

            let shopId = window.location.pathname.split('/')[3];

            url = '/jobs/' + shopId + '/applicants/';
            axios
                .get(url)
                .then(function (response) {
                    vm.toggleBusy();
                    if (response.status === 200) {
                        data = response.data;
                        if (data.hasOwnProperty('applicants')) {
                            vm.applicants = data.applicants;
                            vm.totalRows = data.applicants.length
                        }
                    }
                })
        },
        viewApplicantUrl: function (job) {
            return '/admin/jobs/applicant/' + job.id
        },
        onFiltered(filteredItems) {
            this.totalRows = filteredItems.length;
            this.currentPage = 1
        }
    },
    mounted: function () {
        this.fetchMallJobs()
    }
});
