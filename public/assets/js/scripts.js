/* menu script*/

function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}

/* load more script*/
$(document).ready(function() {
	$(".stores").slice(0, 4).show();
	$("body").on('click touchstart', '.load-more', function (e) {
		e.preventDefault();
		$(".stores:hidden").slice(0, 4).slideDown();
		if ($(".stores:hidden").length == 0) {
			$(".load-more").css('visibility', 'hidden');
		}
		$('html,body').animate({
			scrollTop: $(this).offset().top
		}, 1000);
	});
});